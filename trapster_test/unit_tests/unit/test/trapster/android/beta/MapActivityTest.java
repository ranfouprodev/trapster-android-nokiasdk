package unit.test.trapster.android.beta;

import org.junit.Test;
import unit.test.TestClass;

public class MapActivityTest extends TestClass<MapActivity>
{
    public MapActivityTest()
    {
        super(MapActivity.class);
    }

    @Test
    public void testSomething()
    {
        String appName = getResources().getString(R.string.action_menu_help);
        assertTrue(appName.equals("Help"));
    }
}
