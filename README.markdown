
Trapster-Android
================

Help and add some clarity!

Requirements
------------

We rely on a few different things to get this working:

The Android SDK    http://developer.android.com/sdk/index.html  

-    Install all of the SDK versions, along with the compatibility packs  

Set your GLOBAL environmental Variable ANDROID_HOME to /path/to/sdk/  

-    On OSX, use /etc/launchd.conf  
-    setenv `ANDROID_HOME=/path/to/sdk/`

Add /path/to/sdk/tools/ and /path/to/sdk/platform-tools/ to your GLOBAL path  

-    On OSX, use /etc/launchd.conf  
-    setenv `PATH=YOURPATH:/path/to/sdk/tools/:/path/to/sdk/platform-tools/`  
-    Note: There is no variable expansion here.  

Maven 3.0.3+

-    This should be availble from your package management system
-    If not: http://maven.apache.org/download.html

Eclipse  

-    EGit Plugin  
-    ADT Plugin for Eclipse  
-    Android Configurator for M2E Version >= 4.1.0  

maven-android-plugin  http://code.google.com/p/maven-android-plugin/  

-    It will install the first time you run the project, all Maven like.
	
-    Usage is easy:  
        `mvn clean install will build it.`
        `mvn android:deploy will install it on device.`    
        `mvn android:run will run the application`
        `mvn clean install android:deploy android:run will do all three at once!`

More Info: http://maven-android-plugin-m2site.googlecode.com/svn/plugin-info.html

maven-android-sdk-deployer
	https://github.com/mosabua/maven-android-sdk-deployer
Not needed if you are on the NVT/NOK network, as the needed jars have been deployed to Nexus
The necessary files and config are in the Trapster-Dependancies repo.
Clone it and set android.sdk.path in the root POM to ${env.ANDRDOID_HOME} and comment out the parent parameters.

Eclipse Setup
-------------

Eclipse  

-	Install it

Add these to your Add On Sources:  

-	ADT Plugin	https://dl-ssl.google.com/android/eclipse/  
-	HEAD Maven Integration for Android Development Tools  http://rgladwell.github.com/m2e-android/updates/master/	  
-	Maven Integration for Android Development Tools       http://rgladwell.github.com/m2e-android/updates/  

Addons needed:  

-	ADT Plugin for Eclipse  
-	Egit  
-	Android Configurator for M2E (Latest is 4.1.0-SNAPSHOT.  Fixes issues with ADT 14)  


I like working with the file systems as opposed to EGit, so check out Trapster-Android and Trapster-Android-Smart-SDK, and switch to the bamboo branch(for now)  

Open eclipse and use EGIT to import the whatever project you want to work on.  Or both.  
To make sure everything is clean, double check that workspace resolution is turned off for the projects. (It's set to that, but you may change it)  
	
You also need to import the HockeyApp project from the git submodule into your workspace.


Todo
----

-    ZipAlign the APKs
-    Proguard
-    Push to HockeyApp from Bamboo
-    DONE! Sign the APKs so we don't get key conflicts on deploys