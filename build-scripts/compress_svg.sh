#!/bin/sh -u
#
# 
#

cd "../trapster/assets/images/convert/"

out_dir="out/"

for infile in *.svg; 
do 
	mkdir -p $out_dir

	python /e/Tools/scour/scour.py  -i $infile -o "${out_dir}${infile}" --disable-simplify-colors --enable-viewboxing --keep-editor-data
	
done