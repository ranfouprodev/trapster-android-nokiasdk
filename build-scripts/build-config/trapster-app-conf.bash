#!/bin/bash

############################
#     Project Settings     #
############################


# Directories!
APP_DIR="trapster"
BUILD_DIR="${APP_DIR}/target"
ARTIFACT_DIR="${APP_DIR}/products"
NATIVE_LIB_DIR="${APP_DIR}/libs"

APP_REPO="git@github.com:Trapster/Trapster-Android-NokiaSDK.git"

# Hockey App
HOCKEY_INTERNAL_TOKEN="da820f7ad80041b583cb6f5cc8648651"
HOCKEY_DOGFOOD_TOKEN="153cd05cd0e247af943ba6222e7e5c86"
HOCKEY_3D_BETA_TOKEN="dd9ab8f3cac740afaa36c44f0a57b458"

HOCKEY_INTERNAL_APP_ID="cc87c2639b9b24f41ad90af8ef2cb00a"
HOCKEY_DOGFOOD_APP_ID="ac0892574e75980540b56377b8256bf3"
HOCKEY_3D_BETA_APP_ID="e0735238c7f5c43729267f2fcce77e56"

# Smartling API Key
SMARTLING_API_KEY="1e9899af-1cd4-418d-aaea-55a4172cec29"
SMARTLING_PROJ="5189a9f7b"
SMARTLING_STRING_FILE="${APP_DIR}/res/values/strings.xml"

###########################
