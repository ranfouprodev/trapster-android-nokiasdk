###################################################
#          TestFlight Related Functions           #
###################################################


function posttotestflight() {
    
    CHANGE_STRING="${1}"

    printtitle "Posting to Test Flight"
    
    curl -v "http://testflightapp.com/api/builds.json" \
     -F file=@"${BUILD_NAME_FINAL}-TestFlight.ipa" \
     -F dsym=@"${BUILD_DSYM_FILE_TF_FINAL}.zip" \
     -F api_token="${APP_TF_API_KEY}" \
     -F team_token="${APP_TF_TEAM_KEY}" \
     -F notes="${CHANGE_STRING}" \
     -F notify=False \
     -F distribution_lists="${APP_TF_CI_BUILD_GROUPS}";

    printtitle "Removing Change Log"
    
    rm -f "${CHANGE_FILE}"
}
