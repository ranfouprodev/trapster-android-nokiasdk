#!/bin/bash

##################################################
#               Git Related Things               #
##################################################

# What branch are we on

function git-get-branch() {

    local BRANCH="$(git branch | grep "*" | sed s/'* '//)"

    echo "${BRANCH}"

}

function git-has-remote() {
    if [ -z "$(git remote)" ]
        then
            false
        else
            true
        fi
}

function git-has-correct-remote() {
    
    if [ -z "$(git remote -v | grep git)" ]
        then
            false
        else
            true
        fi
}

function git-add-remote() {

    local REPO="${1}"

    # Bamboo doesn't add a remote to the local git repo.
    #   This fixes it.

    if !( git-has-remote )
        then
            git remote add origin "${REPO}"
        fi

    # But sometimes, Bamboo adds a remote repo with a " " as the origin url.  I hates it.
    if !( git-has-correct-remote )
        then
            git remote rm origin
            git remote add origin "${REPO}"
        fi
}

function git-tag() {

    print-title "Tagging this in Git"

    GIT_TAG_PREFIX="bamboo/${VERSION_SEMANTIC}"
    GIT_OLD_BUILD_NUMBER=$((${BAMBOO_BUILD_NUMBER}-20))
    
    GIT_TAG="${GIT_TAG_PREFIX}/${BAMBOO_BUILD_NUMBER}"
    GIT_OLD_TAG="${GIT_TAG_PREFIX}/${GIT_OLD_BUILD_NUMBER}"
    
    git tag -m "Bamboo Tag for ${GIT_TAG}" -a ${GIT_TAG}
    git push --tags
    
    if [ "$(git tag -l ${GIT_OLD_TAG})" ]
    then
        git tag -d ${GIT_OLD_TAG}
        git push origin :refs/tags/"${GIT_OLD_TAG}"
    fi
    
    git push --tags
}

function git-reset-hard() {

    git reset --hard
}


function git-setup() {

    local REPO="${1}"
    local BRANCH="${2}"

    git-reset-hard
    git gc 2> /dev/null
    git-add-remote "${REPO}"
    git checkout "${BRANCH}"

}

function generate-change-log-file() {

    # Build Release Notes

    local CHANGE_FILE="${1}"
    local THIS_SHA="${2}"
    local PREV_SHA="${3}"
    
    # Ugly hack due to Bamboo being dumb
    local THIRTY_MIN_AGO=`date -v -30M`
    
    # Change Header
    local CHANGE_HEADER="This build was uploaded via Bamboo. Build: ${BUILD_NUMBER}"
    # Git Change Log
    local CHANGE_LOG="$(git log --decorate --graph --numstat ${THIS_SHA}...${PREV_SHA})" 2> /dev/null
    # Final Change Log File
    
    # Yes, it's hacky.
    echo "${CHANGE_HEADER}" >> "${CHANGE_FILE}"
    echo  "" >> "${CHANGE_FILE}"
    echo "${CHANGE_LOG}" >>"${CHANGE_FILE}"
    
}