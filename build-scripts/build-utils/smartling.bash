#!/bin/bash

##################################################
#          Smartling Related Functions           #
##################################################

function post-strings() {

    local STRINGS_FILE="${1}"

    print-title "Posting Strings to Smartling"

    curl -v "https://api.smartling.com/v1/file/upload" \
     -F apiKey="${SMARTLING_API_KEY}" \
     -F projectId="${SMARTLING_PROJ}" \
     -F fileUri="${STRINGS_FILE}" \
     -F fileType="Android" \
     -F approved="True" \
     -F file=@"${STRINGS_FILE};type=application/xml; charset=utf-8";

}
