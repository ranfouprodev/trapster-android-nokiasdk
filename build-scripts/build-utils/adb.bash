#!/bin/bash

##################################################
#             ADB Related Functions              #
##################################################

function adb-list-devices() {

adb devices | grep 'device$' | cut -f1

}

function have-connected-devices() {

    if [ $(echo "$(adb-list-devices)" | wc -c) -gt 1 ]
    then
        true
    else
        false
    fi

}

function restart-adb() {

    adb kill-server
    adb start-server

}

function hacky-restart-adb() {

    # Oh god this is a hack
    for i in `seq 1 10`
    do
    
        restart-adb
        
        # This gives the devices time to connect
        sleep 1
        
        if have-connected-devices
        then
            echo "WINNER WINNER CHICKEN DINNER"
            break
        fi
    
    done

}



function adb-all-devices() {

    local ADB_CMD="${1}"
    
    for device in $(adb-list-devices)
        do
            eval "adb -s ${device} ${ADB_CMD}"
        done
}

function adb-shell-all-devices() {
    
    local ADB_SHELL_CMD="${1}"
    
    adb-all-devices "shell ${ADB_SHELL_CMD}"
}
