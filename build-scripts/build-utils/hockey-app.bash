#!/bin/bash

##################################################
#           Hockey Related Functions             #
##################################################

function post-to-hockey() {

    local CHANGE_FILE="${1}"

    local CHANGE_LOG="$(<${CHANGE_FILE})"

    local AVAILABLE=2
    local NOT_AVAILABLE=1
    local AVAILABILITY=$AVAILABLE

    # If we end up with a lot more HockeyApp
    #   Projects, we'll change to a switch
    # TODO: Change to a switch
    # Build to upload
    if [ ${HOCKEY_PROJ} = "internal" ]
        then
            local HOCKEY_TOKEN="${HOCKEY_INTERNAL_TOKEN}"
            local HOCKEY_APP_ID="${HOCKEY_INTERNAL_APP_ID}"
            local HOCKEY_BUILD="$(ls ${ARTIFACT_DIR} | grep -v 'Prod' | grep  "\-internal-beta-aligned.apk")"
            print-title "We're posting to Trapster Internal using ${HOCKEY_TOKEN}"
            print-title "We're posting to Trapster Internal"
        else
            local HOCKEY_TOKEN="${HOCKEY_3D_BETA_TOKEN}"
            local HOCKEY_APP_ID="${HOCKEY_3D_BETA_APP_ID}"
            local HOCKEY_BUILD="$(ls ${ARTIFACT_DIR} | grep -v 'Prod' | grep  "\-moderator-beta-aligned.apk")"
            print-title "We're posting to Trapster Dogfood using ${HOCKEY_TOKEN}"
            print-title "We're posting to Trapster Dogfood"
        fi

    HOCKEY_API_URL="https://rink.hockeyapp.net/api/2/apps/${HOCKEY_APP_ID}/app_versions/upload"

    
    # Post the signed-aligned version
    curl -v "${HOCKEY_API_URL}"\
     -F status="${AVAILABILITY}"\
     -F notify="0"\
     -F notes="${CHANGE_STRING}"\
     -F release_type="0"\
     -F ipa=@"${ARTIFACT_DIR}/${HOCKEY_BUILD}"\
     -H "X-HockeyAppToken: ${HOCKEY_TOKEN}";

}

