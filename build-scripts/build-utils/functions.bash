#!/bin/bash

##################################################
#               Generic Functions                #
##################################################


function print-hashes() {

    local HASHES=$((${1}-1))
    
    printf '\n'
    
    for i in $(seq ${HASHES})
    do
        printf '#'
    done
    
    printf '\n'
}

# Print a nicely formatted header
# print-title $STRING
function print-title() {

    local TITLE="# ${1} #"
    local LEN=$(echo "${TITLE}" | wc -m | sed -e 's/^[ \t]*//')
    
    print-hashes ${LEN}
    
    printf "${TITLE}"
    
    print-hashes ${LEN}
    
    printf '\n'
}

# Functions to define and undefine marcos.  
# $1 = macro name $2 = the file to be changed
function define-macro() {

    local MACRO_NAME="${1}"
    local MACRO_FILE="${2}"
    
    sed -i '' -e s"/#undef ${MACRO_NAME}/#define ${MACRO_NAME}/" "${MACRO_FILE}"
}

function undef-macro() {

    local MACRO_NAME="${1}"
    local MACRO_FILE="${2}"
    
    sed -i '' -e s"/#define ${MACRO_NAME}/#undef ${MACRO_NAME}/" "${MACRO_FILE}"

}

function clean_artifact_dir() {

    local ART_DIR="${1}"

    if [ -d "${ART_DIR}" ];
    then
        rm -f "${ART_DIR}"/*.apk
    fi

}

function make_artifact_dir() {

    local ART_DIR="${1}"
    
    mkdir -p "${ART_DIR}"
    
}

function copy_artifacts() {

    local SOURCE_DIR="${1}"
    local ART_DIR="${2}"
    
    if [ ! -d "${ART_DIR}" ];
    then
        make_artifact_dir "${ART_DIR}"
    fi
    
    cp -R "${SOURCE_DIR}"/*aligned.apk "${ART_DIR}"/

}

function clean_native_lib_dir() {

    local NATIVE_DIR="${1}"

    if [ -d "${NATIVE_DIR}" ];
    then
        rm -f "${NATIVE_DIR}"/**/*.so
    fi

}

