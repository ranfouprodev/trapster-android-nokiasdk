#!/bin/bash
#
# Trapster-Android build-script
#
# Usage:
# build-trapster-android-app.sh $1 $2 $3
#
# $1 = build.number
# $2 = True if you want to post to HockeyApp
# $3 = What HockeyApp project to post to.
#
# Internal = internal
# Dogfood = dogfood
# Trapster.Beta = beta
#
# Default = beta 

BUILD_NUMBER=$1
SHOULD_POST=$2
HOCKEY_PROJ=$3
BAMBOO_BRANCH=$4
BAMBOO_COMMIT=$5
BAMBOO_PREV_COMMIT=$6
MAVEN_SKIP_TESTS=$7

# The Build Script location
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PARENT_DIR="${SCRIPT_DIR}/.."

# The conf & utils directories
SCRIPT_CONFIG_DIR="${SCRIPT_DIR}/build-config"
SCRIPT_UTILS_DIR="${SCRIPT_DIR}/build-utils"

# Pull in the main configuration
source "${SCRIPT_CONFIG_DIR}/trapster-app-conf.bash"

# Pull in some helpers
for item in functions git smartling adb hockey-app; do
    source "${SCRIPT_UTILS_DIR}/${item}.bash"
done

# Reset the repo so we know what branch we are on
git-setup "${APP_REPO}" "${BAMBOO_BRANCH}"

GIT_BRANCH="$(git-get-branch)"

CHANGE_LOG_FILE="${PARENT_DIR}/changelog.txt"

if [ -a ${CHANGE_LOG_FILE} ]; then
    rm ${CHANGE_LOG_FILE}
fi

generate-change-log-file "${CHANGE_LOG_FILE}" "${BAMBOO_COMMIT}" "${BAMBOO_PREV_COMMIT}"


# Upload to strings.xml Smartlings if we're on develop
if [ "${GIT_BRANCH}" = "develop" ]

then
    post-strings "${SMARTLING_STRING_FILE}"
fi


print-title "Cleaning out old artifacts and native libs"

clean_artifact_dir "${ARTIFACT_DIR}"

clean_native_lib_dir "${NATIVE_LIB_DIR}"

################################
# Should we install or deploy? #
################################

# Replace the MVN_COMMAND so we don't overwrite release artifacts.
if [ ${GIT_BRANCH} = "master" ]; then
    MVN_COMMAND='deploy'
    print-title "We're deploying to Nexus"
else
    MVN_COMMAND='install'
    print-title "We're installing to the local repo"
fi


print-title "Let's starting building things"


print-title "Updating the POM"
# Update the POM
# test profile so the test projects get updated too and don't point to the wrong pom
#mvn --batch-mode release:update-versions -Ptest -Dbuild.number="${BUILD_NUMBER}" -DautoVersionSubmodules='True' -DdevelopmentVersion='${version.full}'-SNAPSHOT

# Clean the whole project tree
#mvn clean -Ptest

print-title "Building the Debug Version"
# Debug Build
mvn clean "${MVN_COMMAND}" -Dbuild.number="${BUILD_NUMBER}" -Dmaven.test.skip="${MAVEN_SKIP_TESTS}"

copy_artifacts "${BUILD_DIR}" "${ARTIFACT_DIR}"

# Check if there was a failure
if [ "$?" -gt 0 ]
then
  exit 1
fi

if [ ${HOCKEY_PROJ} = "internal" ]
then
    print-title "3D Internal HockeyApp Build"
    # Hockey App Release Build.. trapster.beta namespace
    mvn "${MVN_COMMAND}" -Pthreedeeinternal -Dproduction.signing -Dbuild.number="${BUILD_NUMBER}" -Dmaven.test.skip="${MAVEN_SKIP_TESTS}"
else
    print-title "3D Beta HockeyApp Build"
    # Hockey App Release Build.. trapster.beta namespace
    mvn "${MVN_COMMAND}" -Pthreedeebeta -Dproduction.signing -Dbuild.number="${BUILD_NUMBER}" -Dmaven.test.skip="${MAVEN_SKIP_TESTS}"
fi

# Check if there was a failure
if [ "$?" -gt 0 ]
then
  exit 1
fi

copy_artifacts "${BUILD_DIR}" "${ARTIFACT_DIR}"


print-title "Building the Production Version"

# CLean to make sure there aren't any sneaky gremlins
mvn clean

mvn --batch-mode release:update-versions -Pproduction -Dproduction.signing -Dproduction.build -DautoVersionSubmodules='true' -DdevelopmentVersion='${version.marketing}'-SNAPSHOT -Dversion.full='${version.marketing}' -Dbuild.number="${BUILD_NUMBER}" 

mvn android:manifest-update -Pproduction -Dproduction.signing -Dproduction.build -Dandroid.manifest.versionName='${version.marketing}' -Dversion.full='${version.marketing}' -Dbuild.number="${BUILD_NUMBER}"

# Prod Version

mvn "${MVN_COMMAND}" -Pproduction -Dproduction.signing -Dproduction.build -Dandroid.manifest.versionName='${version.marketing}' -Dandroid.manifest.debuggable='False' -Dversion.full='${version.marketing}' -Dbuild.number="${BUILD_NUMBER}"

copy_artifacts "${BUILD_DIR}" "${ARTIFACT_DIR}"


#####################
# Post to HockeyApp #
#####################


if [[ ${SHOULD_POST} = "True" && ${GIT_BRANCH} = "develop" ]]
then
    post-to-hockey "${CHANGE_LOG_FILE}"
    print-title "Build maybe posted to HockeyApp!"
fi

print-title "We're done building things, boi!"
