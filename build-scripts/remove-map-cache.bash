#!/bin/bash
#
# Trapster-Android 
#
# Script to remove the NokiaSDK's map cache files
#  to 'fix' incompatibilities between sdk versions
#
#

# The Build Script location
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# The conf & utils directories
SCRIPT_UTILS_DIR="${SCRIPT_DIR}/build-utils"

# Pull in some helpers
source "${SCRIPT_UTILS_DIR}/functions.bash"
source "${SCRIPT_UTILS_DIR}/adb.bash"

if have-connected-devices
then
    adb-shell-all-devices "rm -r /sdcard/.nokia*"
    print-title "All connected devices should now be free of the filthy cache"
else
    print-title "No devices found, please correct this error"
fi
