#!/bin/sh -u
#
# Trapster-Android Convert Images Buildscript
#
# Takes all the images in the xhdpi directory and rescales / copies them into hdpi/mdpi/ldpi at the correct resolution
#
# Usage:
# convert-images.sh $1 $2
#
# Requires imagemagick to be installed
#

xhdpi_dir="../res/drawable-xhdpi/"
hdpi_dir="../drawable-hdpi/"
mdpi_dir="../drawable-mdpi/"
ldpi_dir="../drawable-ldpi/"

cd "$xhdpi_dir"

for infile in *.png; 
do 
	
	convert $infile -resize 75%  "${hdpi_dir}${infile}";
	
	convert $infile -resize 50%  "${mdpi_dir}${infile}";
	
	convert $infile -resize 37.5%  "${ldpi_dir}${infile}";
	 
	
done