package com.trapster.android.widget;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.RemoteViews;
import com.google.inject.Inject;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.activity.LoginScreen;
import com.trapster.android.activity.SingleTrapDetailsScreen;
import com.trapster.android.activity.StartScreen;
import com.trapster.android.comms.ITrapsterAPI;
import com.trapster.android.manager.BitmapCacheManager;
import com.trapster.android.manager.FlurryManager;
import com.trapster.android.manager.SessionManager;
import com.trapster.android.manager.TrapManager;
import com.trapster.android.manager.flurry.FLURRY_EVENT_NAME;
import com.trapster.android.manager.flurry.FLURRY_PARAMETER_NAME;
import com.trapster.android.manager.flurry.FlurryEvent;
import com.trapster.android.model.Trap;
import com.trapster.android.model.TrapType;
import com.trapster.android.model.dao.TrapDAO;
import com.trapster.android.service.TrapsterService;
import com.trapster.android.util.GeographicUtils;
import com.vividsolutions.jts.geom.Geometry;
import roboguice.service.RoboService;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.TreeMap;

public class TrapsterWidgetUpdateService extends RoboService implements LocationListener
{
    private long lastPopupTextTime = 0;
    private final long TIME_FOR_POPUP_TEXT = 3 * 1000; // 3 Seconds

    public static final String TRAPINDEX = "TrapsterWidgetUpdateService.TrapIndex";

    public static final String TOGGLESERVICE = "TrapsterWidgetUpdateService.ToggleService";

    public static final String VOTE = "TrapsterWidgetUpdateService.Vote";

    public static final String TRAPDETAIL = "TrapsterWidgetUpdateService.TrapDetail";

    public static final int DISABLE_WIDGET_REQ_CD = 0;
    public static final int VOTE_AGREE_REQ_CD = 1;
    public static final int VOTE_DISAGREE_REQ_CD = 2;
    public static final int PREVIOUS_TRAP_REQ_CD = 3;
    public static final int NEXT_TRAP_REQ_CD = 4;
    public static final int REFRESH_WIDGET_REQ_CD = 5;
    public static final int TRAP_DETAIL_REQ_CD = 6;

    private int trapIndex;

    private boolean activeService;

    boolean controlsDisabled = false;

    private AlarmManager am;

    private LocationManager lm;

    private Location lastLocation;

    private int maxTrapIndex;

    private RemoteViews v;

    @Inject SharedPreferences sharedPreferences;

    TreeMap<Double, Trap> sortTraps = new TreeMap<Double, Trap>();
    ArrayList<Trap> sortedTrapList = new ArrayList<Trap>();

    @Inject ITrapsterAPI api;
    @Inject BitmapCacheManager bitmapCacheManager;
    @Inject TrapManager trapManager;
    @Inject TrapDAO trapDAO;
    @Inject SessionManager sessionManager;

    private void startUp(Intent intent)
    {
        ////Log.d(TrapsterWidget.LOGNAME, "Starting up!");

        long widgetMessageTime = sharedPreferences.getLong(Defaults.SETTING_WIDGET_MESSAGE_TIME, 0);

        if (widgetMessageTime != 0)
        {
            if ((System.currentTimeMillis() - widgetMessageTime) < 3000)
                return;
            else
            {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.remove(Defaults.SETTING_WIDGET_MESSAGE_TIME);
                editor.commit();
            }
        }

        am = (AlarmManager) getSystemService(ALARM_SERVICE);

        // This initiates the location managed
        lm = (LocationManager) getSystemService(LOCATION_SERVICE);

        activeService = verifyTrapsterIsRunning();

        lastLocation = getLastLocation();

        // Trap Index can be passed on any request
        updateTrapIndex(intent);

        // Toggle the service functionality
        if (intent != null && intent.hasExtra(TOGGLESERVICE))
        {
{/* 
            //Log.d(TrapsterWidget.LOGNAME, "Toggling service state, currently: " + activeService); */}
            Intent i = new Intent(this, TrapsterService.class);
            //i.putExtra("BACKGROUND", true);

            if (activeService)
            {
                stopService(i);
            }
            else
            {
                startService(i);
            }

            activeService = !activeService;

//			Logger.d(LOGNAME,
//					"Toggling Trapster Service From Widget. New state:"
//							+ activeService);
        }

//		Logger.d(LOGNAME, "Starting TrapsterWidgetUpdate with trap index:"
//				+ trapIndex);

        v = new RemoteViews(getPackageName(), R.layout.widget_trapster);

        // Default View Visibility
        /*v.setViewVisibility(R.id.layoutWidgetContainer, View.VISIBLE);
        v.setViewVisibility(R.id.layoutWidgetErrorContainer, View.GONE);
        v.setViewVisibility(R.id.layoutVoteButtons, View.VISIBLE);
        v.setViewVisibility(R.id.layoutMessages, View.GONE);*/


        //v.setViewVisibility(R.id.widget_trap_circle_background, View.VISIBLE);

        //v.setImageViewBitmap(R.id.widget_trap_circle_icon, null);
        if (activeService)
        {
            v.setImageViewResource(R.id.widget_trap_circle_background, R.drawable.trapcircle);
            v.setViewVisibility(R.id.widget_trap_circle_icon, View.VISIBLE);
            if (sessionManager.isLoggedIn())
            {
                v.setViewVisibility(R.id.widget_vote_no, View.VISIBLE);
                v.setViewVisibility(R.id.widget_vote_yes, View.VISIBLE);
            }
        }
        else
        {
            v.setImageViewResource(R.id.widget_trap_circle_background, R.drawable.trapcircle_off);
            v.setViewVisibility(R.id.widget_vote_no, View.GONE);
            v.setViewVisibility(R.id.widget_vote_yes, View.GONE);
        }


        Intent i = new Intent(this, TrapsterWidgetUpdateService.class);
        i.putExtra(TOGGLESERVICE, true);
        PendingIntent pendingIntent = PendingIntent.getService(this, DISABLE_WIDGET_REQ_CD, i,PendingIntent.FLAG_CANCEL_CURRENT);

        v.setOnClickPendingIntent(R.id.widget_master_container, pendingIntent);

        //v.setTextViewText(R.id.widget_trap_name_text, getString(R.string.widget_tap_to_start));
        //v.setTextViewText(R.id.widget_trap_distance_text, "");


        if (activeService && intent != null && intent.hasExtra(SingleTrapDetailsScreen.TRAP_BUNDLE_KEY))
        {
            //v.setImageViewResource(R.id.widget_trap_circle_icon, R.drawable.trapster_car);
            //v.setTextViewText(R.id.widget_trap_name_text, getString(R.string.widget_no_nearby_traps));

            if (intent.hasExtra(VOTE))
            {

//				Logger.d(LOGNAME, "Rating Trap");
                if (sessionManager.isLoggedIn())
                {

                    Trap trap = (Trap) intent
                            .getParcelableExtra(SingleTrapDetailsScreen.TRAP_BUNDLE_KEY);

                    boolean rating = intent.getBooleanExtra(VOTE, false);

//					Logger.d(LOGNAME, "Rating Trap:" + rating);

                    rateTrap(trap, rating);

                    v.setTextViewText(R.id.widget_trap_name_text, getString(R.string.widget_rating_thankyou));
                    v.setTextViewText(R.id.widget_trap_distance_text, "");

                    showMessage();

                } else
                {
                    updateErrorNotLoggedIn();

                }
                setRefreshTime(5);

            } else if (intent.hasExtra(TRAPDETAIL))
            {

//				Logger.d(LOGNAME, "Trap Detail");
                if (sessionManager.isLoggedIn())
                {

                    Trap trap = (Trap) intent
                            .getParcelableExtra(SingleTrapDetailsScreen.TRAP_BUNDLE_KEY);

                    Context applicationContext = getApplicationContext();
                    Intent launcherIntent = new Intent(applicationContext, SingleTrapDetailsScreen.class);
                    launcherIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    launcherIntent.putExtra(SingleTrapDetailsScreen.TRAP_BUNDLE_KEY, trap);

                    applicationContext.startActivity(launcherIntent);
                }
            }
        }

        if (lastLocation == null)
        {
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    Defaults.GPS_SECONDS_FOR_UPDATE,
                    Defaults.GPS_DISTANCE_FOR_UPDATE, this);

//			Logger.d(LOGNAME, "No Location Waiting for Update");
            updateNoLocation();
        } else
        {
            updateStandard();
        }

        updateWidget();
    }

    private void updateTrapIndex(Intent intent)
    {

        if (intent != null && intent.hasExtra(TRAPINDEX))
            trapIndex = intent.getIntExtra(TRAPINDEX, 0);
        else
        {
            int widgetTrapIndex = sharedPreferences.getInt(Defaults.SETTING_WIDGET_TRAP_INDEX, -1);
            if (widgetTrapIndex != -1)
                trapIndex = widgetTrapIndex;
            else
                trapIndex = 0;
        }

    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {

        ////Log.d(TrapsterWidget.LOGNAME, "onStartCommand");;
        if (TrapsterWidget.isInstalled(getApplicationContext()))
            startUp(intent);

        stopSelf();
        return START_STICKY;

    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
//		Logger.d(LOGNAME, "Shutting down widget");

    }

    private void setUpdateAndQuit()
    {

        lm.removeUpdates(this);

        //
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putInt(Defaults.SETTING_WIDGET_TRAP_INDEX, trapIndex);
        edit.commit();

        stopSelf();


    }

    private void updateWidget()
    {
{/* 
		//Log.d(TrapsterWidget.LOGNAME, "Updating Widget"); */}

        Intent disableIntent = new Intent(this, TrapsterWidgetUpdateService.class);
        disableIntent.putExtra(TOGGLESERVICE, true);
        PendingIntent disablePendingIntent = PendingIntent.getService(this, DISABLE_WIDGET_REQ_CD,
                disableIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        // Update the service status
        if (activeService)
        {

            /*v.setViewVisibility(R.id.buttonStatusIndicatorOn, View.VISIBLE);
            v.setViewVisibility(R.id.buttonStatusIndicatorOff, View.GONE);

            v.setTextColor(R.id.txtTrapType, Color.WHITE);
            v.setTextColor(R.id.txtDistanceAway, Color.WHITE);
            setRemoveViewEnabled(R.id.layoutWidgetTrap, true);*/

            if (!controlsDisabled)
            {
                v.setImageViewResource(R.id.widget_trap_circle_background, R.drawable.trapcircle);

                if (sessionManager.isLoggedIn())
                {
                    v.setViewVisibility(R.id.widget_vote_no, View.VISIBLE);
                    v.setViewVisibility(R.id.widget_vote_yes, View.VISIBLE);
                }

                //v.setTextViewText(R.id.widget_trap_name_text, getString(R.string.widget_no_nearby_traps));
                //v.setTextViewText(R.id.widget_trap_distance_text, "");

            }
        }
        else
        {
            v.setImageViewResource(R.id.widget_trap_circle_background, R.drawable.trapcircle_off);
            //v.setImageViewResource(R.id.widget_trap_circle_icon, R.drawable.trapster_car);
            v.setViewVisibility(R.id.widget_vote_no, View.GONE);
            v.setViewVisibility(R.id.widget_vote_yes, View.GONE);
            v.setViewVisibility(R.id.widget_trap_circle_icon, View.GONE);

            v.setOnClickPendingIntent(R.id.widget_master_container, disablePendingIntent);

            v.setTextViewText(R.id.widget_trap_name_text, getString(R.string.widget_tap_to_start));
            v.setTextViewText(R.id.widget_trap_distance_text, "");
            v.setOnClickPendingIntent(R.id.widget_trap_circle_background, disablePendingIntent);
            v.setOnClickPendingIntent(R.id.widget_trap_circle_icon, disablePendingIntent);
            v.setOnClickPendingIntent(R.id.widget_container_text_container, disablePendingIntent);
            v.setOnClickPendingIntent(R.id.widget_text_image_background, disablePendingIntent);

        }

        v.setOnClickPendingIntent(R.id.widget_power_button, disablePendingIntent);



        /*Intent enableIntent = new Intent(this, TrapsterService.class);
        PendingIntent enablePendingIntent = PendingIntent.getService(this, 0, enableIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        v.setOnClickPendingIntent(R.id.buttonStatusIndicatorOff,enablePendingIntent);*/

        Intent i = new Intent(this, StartScreen.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, Defaults.PENDING_INTENT_START_SCREEN_FROM_WIDGET_LOGO, i,
                PendingIntent.FLAG_CANCEL_CURRENT);
        //v.setOnClickPendingIntent(R.id.widget_trap_circle_icon, pendingIntent);
        v.setOnClickPendingIntent(R.id.widget_trapster_logo, pendingIntent);

        // Push update for this widget to the home screen
        ComponentName trapsterWidget = new ComponentName(this,
                TrapsterWidget.class);
        AppWidgetManager manager = AppWidgetManager.getInstance(this);
        manager.updateAppWidget(trapsterWidget, v);

        setUpdateAndQuit();
    }

    private void disableControls()
    {
        v.setViewVisibility(R.id.widget_vote_yes, View.GONE);
        v.setViewVisibility(R.id.widget_vote_no, View.GONE);
       /* v.setViewVisibility(R.id.imageNextTrap, View.GONE);
        setRemoveViewEnabled(R.id.buttonAgree, false);
//		v.setBoolean(R.id.buttonAgree, "setEnabled", false);
        setRemoveViewEnabled(R.id.buttonDisagree, false);
//		v.setBoolean(R.id.buttonDisagree, "setEnabled", false);*/
        //
        controlsDisabled = true;
    }

    private void updateStandard()
    {

        // Revise the UI
        buildUpdate();

        // Push update for this widget to the home screen
        ComponentName trapsterWidget = new ComponentName(this,
                TrapsterWidget.class);
        AppWidgetManager manager = AppWidgetManager.getInstance(this);
        manager.updateAppWidget(trapsterWidget, v);

        setUpdateAndQuit();
    }

    private boolean verifyTrapsterIsRunning()
    {
        return TrapsterService.state == TrapsterService.STATE.running;
    }

    private void buildUpdate()
    {
        // Update Trap Details
        updateTrapDetails(v);

        updateControls(v);

    }

    private Location getLastLocation()
    {

        lastLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if (lastLocation == null)
            lastLocation = lm
                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        return lastLocation;

    }


    private void updateTrapDetails(RemoteViews v)
    {
        if (activeService)
        {
            // Request the updates for the current update
            // Since the TrapsterService is required to run, it will handle all the
            // updates
            //tm.requestTrapUpdate(lastLocation.getLatitude(),
            //		lastLocation.getLongitude(),
            //		Defaults.BACKGROUND_TRAPS_UPDATE_DISTANCE);

            double radiusInMeters = GeographicUtils
                    .milesToKilometers(Defaults.BACKGROUND_TRAPS_UPDATE_DISTANCE / 2) * 1000;

            Geometry box = GeographicUtils.createBoundingBox(
                    lastLocation.getLatitude(), lastLocation.getLongitude(),
                    radiusInMeters);

            ArrayList<Trap> traps = trapDAO.getTraps(box);

            int n = traps.size();

            //Added to display the traps from nearest to farthest
            if (sortTraps.size() <= 0)
            {
                for (int i = 0; i < n; i++)
                {

                    double km = (GeographicUtils.geographicDistance(lastLocation.getLatitude(),
                            lastLocation.getLongitude(), traps.get(i).getLatitude(), traps.get(i).getLongitude())) / 1000;

                    sortTraps.put(km, traps.get(i));

                }
                Iterator<Trap> iterator = sortTraps.values().iterator();

                while (iterator.hasNext())
                {
                    sortedTrapList.add(iterator.next());
                }
            }

            maxTrapIndex = n - 1;

            Trap trap = null;
            if (n > 0)
            {
                if (trapIndex < n)
                    trap = sortedTrapList.get(trapIndex);
                else
                {
                    trap = traps.get(0);
                    trapIndex = 0;
                }
                updateWithTrap(v, trap);
            } else
            {
                v.setTextViewText(R.id.txtTrapType, getText(R.string.widget_no_nearby_traps));
                v.setTextViewText(R.id.txtDistanceAway, "");
                //disableControls();
                setRefreshTime(5);
            }

            if (lastPopupTextTime + TIME_FOR_POPUP_TEXT > System.currentTimeMillis())
            {
                v.setTextViewText(R.id.widget_trap_name_text, getString(R.string.widget_rating_thankyou));
                v.setTextViewText(R.id.widget_trap_distance_text, "");
            }
        }



    }

    private Bitmap getMapImage(Trap trap)
    {
        TrapType trapType = trapManager.getTrapType(trap.getTypeid());
        String trapTypeIcon = trapType.getImageFilename();
        try
        {
            Bitmap bitmapIcon = bitmapCacheManager.getBitmapFromImageResource(trapManager.getIconNameForTrap(trap), null);

            if (!activeService)
            {
                Bitmap greyBitmap = toGreyscale(bitmapIcon);
                return greyBitmap;
            }

            return bitmapIcon;
        }
        catch (BitmapCacheManager.InvalidResourceKeyException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public Bitmap toGreyscale(Bitmap bmpOriginal)
    {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        paint.setAlpha(40);

        // ColorMatrix cm = new ColorMatrix();
        // cm.setSaturation(60);
        // ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        // paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }

    private void updateErrorNotLoggedIn()
    {
        v.setViewVisibility(R.id.layoutWidgetContainer, View.GONE);
        v.setViewVisibility(R.id.layoutWidgetErrorContainer, View.VISIBLE);

        v.setTextViewText(R.id.textWidgetErrorMessage,getString(R.string.widget_not_logged_in_title));

        Intent intent = new Intent(this, LoginScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, Defaults.PENDING_INTENT_START_SCREEN_FROM_WIDGET_CONTAINER,
                intent, PendingIntent.FLAG_CANCEL_CURRENT);

        v.setOnClickPendingIntent(R.id.buttonLogin, pendingIntent);

        showMessage();
    }

    private void updateNoLocation()
    {
        v.setViewVisibility(R.id.layoutWidgetContainer, View.GONE);
        v.setViewVisibility(R.id.layoutWidgetErrorContainer, View.VISIBLE);

        v.setTextViewText(R.id.textWidgetErrorMessage, getString(R.string.widget_no_location));

        Intent intent = new Intent(this, StartScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, Defaults.PENDING_INTENT_START_SCREEN_FROM_WIDGET_CONTAINER,
                intent, PendingIntent.FLAG_CANCEL_CURRENT);

        v.setOnClickPendingIntent(R.id.buttonLogin, pendingIntent);

    }

    private void updateWithTrap(RemoteViews v, Trap trap)
    {

        if (trap != null)
        {
            Intent singleTrapIntent = new Intent(this, SingleTrapDetailsScreen.class);
            singleTrapIntent.putExtra(SingleTrapDetailsScreen.TRAP_BUNDLE_KEY, trap);

            PendingIntent startTrapPendingIntent = PendingIntent.getActivity(this, VOTE_AGREE_REQ_CD, singleTrapIntent, PendingIntent.FLAG_CANCEL_CURRENT);

            TrapType type = trapManager.getTrapType(trap.getTypeid());

            if (type != null)
            {
                v.setTextViewText(R.id.widget_trap_name_text, type.getName());

                if (lastLocation != null)
                {
                    double km = (GeographicUtils.geographicDistance( lastLocation.getLatitude(),lastLocation.getLongitude(), trap.getLatitude(),trap.getLongitude())) / 1000;
                    DecimalFormat distFormat = new DecimalFormat("#0.00");
                    String dist = distFormat.format(km) + " " + getString(R.string.traplist_unit_km);
                    if (sharedPreferences.getBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_MILES, true))
                    {
                        dist = distFormat.format(km * 0.62137) + " " + getString(R.string.traplist_unit_mi);
                    }
                    v.setTextViewText(R.id.widget_trap_distance_text, dist);
                }
                v.setViewVisibility(R.id.widget_trap_circle_icon, View.VISIBLE);
                Bitmap icon =  getMapImage(trap);
                if (icon != null)
                    v.setImageViewBitmap(R.id.widget_trap_circle_icon,icon);




                setRemoveViewEnabled(R.id.widget_vote_yes, true);

                v.setOnClickPendingIntent(R.id.widget_text_image_background, startTrapPendingIntent);
                //v.setOnClickPendingIntent(R.id.widget_trap_name_text, startTrapPendingIntent);
                //v.setOnClickPendingIntent(R.id.widget_trap_distance_text, startTrapPendingIntent);
                v.setOnClickPendingIntent(R.id.widget_trap_circle_background, startTrapPendingIntent);
                v.setOnClickPendingIntent(R.id.widget_trap_circle_icon, startTrapPendingIntent);

                Intent intent = new Intent(this, TrapsterWidgetUpdateService.class);
                intent.putExtra(VOTE, true);
                intent.putExtra(SingleTrapDetailsScreen.TRAP_BUNDLE_KEY, trap);

                PendingIntent voteIntent = PendingIntent.getService(this, VOTE_AGREE_REQ_CD,
                        intent, PendingIntent.FLAG_CANCEL_CURRENT);

                v.setOnClickPendingIntent(R.id.widget_vote_yes, voteIntent);

                setRemoveViewEnabled(R.id.widget_vote_no, true);

                Intent intent2 = new Intent(this, TrapsterWidgetUpdateService.class);
                intent2.putExtra(VOTE, false);
                intent2.putExtra(SingleTrapDetailsScreen.TRAP_BUNDLE_KEY, trap);

                PendingIntent voteIntent2 = PendingIntent.getService(this, VOTE_DISAGREE_REQ_CD,
                        intent2, PendingIntent.FLAG_CANCEL_CURRENT);

                v.setOnClickPendingIntent(R.id.widget_vote_no, voteIntent2);

                Intent i = new Intent(this, TrapsterWidgetUpdateService.class);
                i.putExtra(TRAPDETAIL, true);
                i.putExtra(SingleTrapDetailsScreen.TRAP_BUNDLE_KEY, trap);

                PendingIntent pendingIntent = PendingIntent.getService(this, TRAP_DETAIL_REQ_CD, i,
                        PendingIntent.FLAG_CANCEL_CURRENT);

                v.setOnClickPendingIntent(R.id.widget_container_text_container, pendingIntent);
            }

        }
    }

    private void updateControls(RemoteViews v)
    {
        /*int next = trapIndex + 1;
        if (next > maxTrapIndex)
        {
            v.setViewVisibility(R.id.imageNextTrap, View.INVISIBLE);
        } else
        {
            v.setViewVisibility(R.id.imageNextTrap, View.VISIBLE);
            // Trigger the next load
            Intent intent = new Intent(this, TrapsterWidgetUpdateService.class);
            intent.putExtra(TRAPINDEX, next);

            PendingIntent pendingIntent = PendingIntent.getService(this, NEXT_TRAP_REQ_CD, intent,
                    PendingIntent.FLAG_CANCEL_CURRENT);
            v.setOnClickPendingIntent(R.id.imageNextTrap, pendingIntent);
        }


        int prev = trapIndex - 1;
        if (prev < 0)
        {
            v.setViewVisibility(R.id.imagePrevTrap, View.INVISIBLE);
        } else
        {
            v.setViewVisibility(R.id.imagePrevTrap, View.VISIBLE);
            // Trigger the previous load
            Intent intent = new Intent(this, TrapsterWidgetUpdateService.class);
            intent.putExtra(TRAPINDEX, prev);

            PendingIntent pendingIntent = PendingIntent.getService(this, PREVIOUS_TRAP_REQ_CD, intent,
                    PendingIntent.FLAG_CANCEL_CURRENT);
            v.setOnClickPendingIntent(R.id.imagePrevTrap, pendingIntent);
        }*/
    }

    /**
     * Submit trap votes to server. This response is not monitored for errors
     *
     * @param agree (Y or N)
     */
    private void rateTrap(Trap trap, boolean agree)
    {

        //TrapProvider tp = new TrapProvider(this);
        //tp.rateTrap(agree, trap, null);

        api.rateTrap(agree, trap,null, null);
        lastPopupTextTime = System.currentTimeMillis();

        sendFlurryTrapVoteEvent(agree, trap);
    }

    private void setRefreshTime(int seconds)
    {

        // prepare Alarm Service to trigger Widget
        Intent intent = new Intent(this, TrapsterWidgetUpdateService.class);
        PendingIntent pendingIntent = PendingIntent.getService(this, REFRESH_WIDGET_REQ_CD, intent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.SECOND, seconds);
        am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                pendingIntent);
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    @Override
    public void onLocationChanged(Location location)
    {

        lastLocation = location;

        updateWidget();

        lm.removeUpdates(this);

    }

    @Override
    public void onProviderDisabled(String arg0)
    {
    }

    @Override
    public void onProviderEnabled(String arg0)
    {
    }

    @Override
    public void onStatusChanged(String arg0, int arg1, Bundle arg2)
    {
    }

    void setRemoveViewEnabled(int viewId, boolean enabled)
    {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO)
        {
            if (!enabled)
            {
                // cannot disable removeView (unsupported);
                // send NOACTION broadcast instead
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), viewId * -1, new Intent("NOACTION"), PendingIntent.FLAG_CANCEL_CURRENT);
                v.setOnClickPendingIntent(viewId, pendingIntent);
            }
        } else
        {
            v.setBoolean(viewId, "setEnabled", enabled);
        }
    }

    void showMessage()
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(Defaults.SETTING_WIDGET_MESSAGE_TIME, System.currentTimeMillis());
        editor.commit();
        //
        setRefreshTime(3);
    }

    private void sendFlurryTrapVoteEvent(boolean vote, Trap trap)
    {
        FlurryEvent event;

        if (vote)
            event = FlurryManager.createEvent(FLURRY_EVENT_NAME.WIDGET_TRAP_VOTE_AGREE);
        else
            event = FlurryManager.createEvent(FLURRY_EVENT_NAME.WIDGET_TRAP_VOTE_DISAGREE);

        TrapType trapType = trapManager.getTrapType(trap.getTypeid());
        String trapName = trapType.getName();
        String englishName = trapType.getUnlocalizedName();

        event.putParameter(FLURRY_PARAMETER_NAME.TRAP_TYPE, englishName);
        event.putParameter(FLURRY_PARAMETER_NAME.TRAP_TYPE_LANGUAGE, trapName);
        event.putParameter(FLURRY_PARAMETER_NAME.LOCATION_COUNTRY, FlurryManager.getCountryCode());
        event.putParameter(FLURRY_PARAMETER_NAME.LOCATION_STATE, FlurryManager.getStateCode());
        event.putParameter(FLURRY_PARAMETER_NAME.DRIVING_SPEED, FlurryManager.getDrivingSpeed());

        event.send();
    }

}
