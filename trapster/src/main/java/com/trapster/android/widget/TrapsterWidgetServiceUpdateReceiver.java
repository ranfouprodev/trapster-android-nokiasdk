package com.trapster.android.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.trapster.android.manager.SessionManager;
import com.trapster.android.receiver.TrapsterServiceBroadcastReceiver;
import com.trapster.android.util.Log;

/*
 * This class is used to update the widget when the service is running
 */
public class TrapsterWidgetServiceUpdateReceiver extends BroadcastReceiver
{
    //
    public void onReceive(Context context, Intent intent)
    {
        if (TrapsterWidget.isInstalled(context))
        {
            String action= intent.getAction();
            if (!TrapsterServiceBroadcastReceiver.INTENT_BROADCAST.equals(action))
{/*                 //Log.d(TrapsterWidget.LOGNAME, "Received action: " + action); */}
            if (SessionManager.INTENT_BROADCAST_ACTION_LOGON.equals(action))
            {
                context.startService(new Intent(context, TrapsterWidgetUpdateService.class));
            }
            else
            {

                int type= intent.getIntExtra(TrapsterServiceBroadcastReceiver.INTENT_NOTIFICATION_TYPE, -1);
                if (type == TrapsterServiceBroadcastReceiver.INTENT_UPDATE_TRAP_NOTIFICATION || type == TrapsterServiceBroadcastReceiver.INTENT_UPDATE_STATUS)
                {
                    ////Log.d(TrapsterWidget.LOGNAME, "Type received: " + type);
                    context.startService(new Intent(context, TrapsterWidgetUpdateService.class));
                }

/*                switch (type)
                {
                    case TrapsterServiceBroadcastReceiver.INTENT_UPDATE_TRAP_NOTIFICATION:
                    case TrapsterServiceBroadcastReceiver.INTENT_UPDATE_STATUS:
                        // Status running/stopped not changed
                        context.startService(new Intent(context, TrapsterWidgetUpdateService.class));
                        break;
                }*/
            }
        }
    }
}
