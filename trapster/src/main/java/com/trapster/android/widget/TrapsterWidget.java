package com.trapster.android.widget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import com.trapster.android.manager.FlurryManager;
import com.trapster.android.manager.flurry.FLURRY_EVENT_NAME;
import com.trapster.android.manager.flurry.FlurryEvent;

public class TrapsterWidget extends AppWidgetProvider
{
    static final String LOGNAME= "TrapsterWidget";
    //
    public static boolean isInstalled(Context context)
    {
        int[] appWidgetIds= AppWidgetManager.getInstance(context).getAppWidgetIds(new ComponentName(context, TrapsterWidget.class));

        boolean installed = appWidgetIds.length > 0;
        ////Log.d(LOGNAME, "Checking if app is installed, result: " + installed);
        return installed;
    }
    public void onDisabled(Context context)
    {
        super.onDisabled(context);
        sendFlurryWidgetDisabledEvent();
       FlurryManager.endSession(context);
    }
    public void onEnabled(Context context)
    {
        super.onEnabled(context);
        FlurryManager.startSession(context);
        sendFlurryWidgetEnabledEvent();
    }
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds)
    {
        context.startService(new Intent(context, TrapsterWidgetUpdateService.class));
    }

    private void sendFlurryWidgetEnabledEvent()
    {
        FlurryEvent event = FlurryManager.createEvent(FLURRY_EVENT_NAME.WIDGET_ADD_TO_SCREEN);
        event.send();
    }

    private void sendFlurryWidgetDisabledEvent()
    {
        FlurryEvent event = FlurryManager.createEvent(FLURRY_EVENT_NAME.WIDGET_REMOVE_FROM_SCREEN);
        event.send();
    }
}
