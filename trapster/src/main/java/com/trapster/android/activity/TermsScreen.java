package com.trapster.android.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.google.inject.Inject;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.activity.component.listener.ReloadAttributesListener;
import com.trapster.android.comms.ITrapsterAPI;
import com.trapster.android.manager.FlurryManager;
import com.trapster.android.manager.SessionManager;
import com.trapster.android.manager.flurry.FLURRY_EVENT_NAME;
import com.trapster.android.manager.flurry.FLURRY_PARAMETER_NAME;
import com.trapster.android.manager.flurry.FlurryEvent;
import com.trapster.android.model.StringResource;
import com.trapster.android.model.dao.StringResourceDAO;
import com.trapster.android.service.TrapsterService;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.screen_terms)
public class TermsScreen extends Screen {

	private static final String LOGNAME = "TermsScreen";
    private final static String CLASS_NAME = "TermsAndConditions";

	private static boolean termsAccepted = false;

	@Inject
	SessionManager sessionManager;

	@Inject
	SharedPreferences sharedPreferences;

	@InjectView(R.id.textTermsAccept) TextView textTermsAccept;
	
	@InjectView(R.id.textTerms) TextView textTerms;
	
	@InjectView(R.id.layoutLoadingTerms) LinearLayout layoutLoading; 
	
	@InjectView(R.id.buttonAccept) Button buttonAccept;
	
	@InjectView(R.id.buttonDecline) Button buttonDecline;
    @InjectView(R.id.termsScrollView) ScrollView scrollView;

	@Inject
	StringResourceDAO stringResourceDao;
	
	@Inject ITrapsterAPI api;
	
	private Handler handler = new Handler(){

		@Override
		public void handleMessage(Message msg) {
			switch(msg.what){
				case ReloadAttributesListener.UPDATED_TERMS:
					setTerms(msg.obj.toString());
                    //activate the accept button once the terms have loaded
                    enablePositiveButton();
					break;
			}
		}


	};
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		setTitle(R.string.terms_title);
		
		textTermsAccept.setText(Html.fromHtml(getString(R.string.terms_accept)));
		textTermsAccept.setLinksClickable(true);
		textTermsAccept.setMovementMethod(LinkMovementMethod.getInstance());
		
		StringResource terms = stringResourceDao.findByKey(Defaults.ASSET_TERMSANDCONDITIONS);
		if(terms != null)
        {
            setTerms(terms.getText());
            enablePositiveButton();
        }

		else
			reloadTerms(); 
		
		
	}

    @Override
    public void onNewIntent(Intent intent)
    {

    }
    private void enablePositiveButton()
    {
        buttonAccept.setClickable(true);
        buttonAccept.setTextColor(Color.BLACK);
    }
	
	
	
	@Override
	public void onBackPressed() {
		setResult(RESULT_CANCELED);
		finish();
	}




	public void onDeclineClick(View view){
		setResult(RESULT_CANCELED);
        sendFlurryTermsBehaviorEvent(false);
		finish();
	}

    /*
    *  Sam
    *
    *  Accept button begins as being disabled.  Not until the terms text is uploaded and displayed does the
    *  Accept button become clickable.
     */
	
	public void onAcceptClick(View view){

        termsAccepted = true;
        sendFlurryTermsBehaviorEvent(true);
		
		long currentTerms = sharedPreferences.getLong(Defaults.ASSET_TERMSANDCONDITIONS_NEWEST_DATE, 0);

		sharedPreferences.edit().putLong(Defaults.ASSET_TERMSANDCONDITIONS_ACCEPTED_DATE, currentTerms).commit();
		
		setResult(RESULT_OK);
        startService(new Intent(this, TrapsterService.class));
		finish();
	}
	
	
	private void setTerms(String terms){
		layoutLoading.setVisibility(View.GONE);

		textTerms.setVisibility(View.VISIBLE);
		textTerms.setText(Html.fromHtml(terms.trim()));
		textTerms.setLinksClickable(true);
		textTerms.setMovementMethod(LinkMovementMethod.getInstance());


	}
	
	private void reloadTerms() {
		ReloadAttributesListener listener = new ReloadAttributesListener(sharedPreferences, handler, stringResourceDao);
		api.getLegalAttributes(0, listener, listener);
	}

    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }

    private void sendFlurryTermsBehaviorEvent(boolean whichVote)
    {
        FlurryEvent event = FlurryManager.createEvent(FLURRY_EVENT_NAME.TERMS_BEHAVIOR);
        event.putParameter(FLURRY_PARAMETER_NAME.TERMS_VOTE, String.valueOf(whichVote));
        event.putParameter(FLURRY_PARAMETER_NAME.TERMS_SCROLL, String.valueOf(scrollView.getScrollY() > 0)); // Slightly innaccurate, but in general people won't scroll back to the top before submitting
        event.send();
    }

    @Override
    protected boolean shouldUseActionBar()
    {
        return false;
    }

    public static boolean areTermsAccepted()
    {
        return termsAccepted;
    }
}
