package com.trapster.android.activity.fragment;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.here.android.common.GeoBoundingBox;
import com.here.android.common.GeoCoordinate;
import com.here.android.mapping.Map;
import com.here.android.mapping.MapContainer;
import com.here.android.mapping.MapFactory;
import com.here.android.mapping.MapPolyline;
import com.trapster.android.Defaults;
import com.trapster.android.manager.MapController;
import com.trapster.android.manager.PatrolManager;
import com.trapster.android.manager.PositionManager;
import com.trapster.android.model.PatrolPath;
import com.trapster.android.service.PatrolSyncService;
import com.trapster.android.util.BackgroundTask;
import com.trapster.android.util.GeographicUtils;
import com.trapster.android.util.MapUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA. User: snazi Date: 9/20/12 Time: 11:09 AM
 * 
 * 
 */
@Singleton
public class PatrolMapLayer extends AbstractMapLayer {

	private static final int PATROL_LINE_WIDTH = 10;

	private static final String LOGNAME = "PatrolMapLayer";

	//public static final int MAX_PATROL_SIZE = 200;

	@Inject
	MapController mapController;

	@Inject
	PatrolManager patrolManager;

	@Inject
	PositionManager pm;

	@Inject
	SharedPreferences sharedPreferences;

	private MapContainer currentPatrolContainer;
	private MapContainer modifiablePatrolContainer;

	
	private HashMap<PatrolPath, MapPolyline> currentPatrolMap = new HashMap<PatrolPath, MapPolyline>();
	private HashMap<PatrolPath, MapPolyline> modifiablePatrolMap = new HashMap<PatrolPath, MapPolyline>();

	private final Object hashMapLock = new Object();

	private Handler updateHandler = new Handler();
	private Runnable mapUpdateTask = new Runnable() {

		@Override
		public void run() {
			
			if(getMap() != null){
				GeoBoundingBox bbox = getMap().getBoundingBox();
                if (bbox.isValid())
                {
                    double radius = bbox.getBottomRight().distanceTo(bbox.getCenter());

                    double radiusInMiles = GeographicUtils
                            .kilometersToMiles(radius / 1000);

                    radiusInMiles = (radiusInMiles > PatrolSyncService.MAX_QUERY_RADIUS) ? PatrolSyncService.MAX_QUERY_RADIUS
                            : radiusInMiles;

                    GeoCoordinate newCenter = getMap().getCenter();

                    patrolManager.requestPatrolUpdate(newCenter.getLatitude(),
                            newCenter.getLongitude(), radius);
                }

			}
		}

	};

    private Runnable recurringMapUpdateTask = new Runnable() {

        @Override
        public void run() {

            if(getMap() != null){
                GeoBoundingBox bbox = getMap().getBoundingBox();
                if (bbox.isValid())
                {
                    double radius = bbox.getBottomRight().distanceTo(bbox.getCenter());

                    double radiusInMiles = GeographicUtils
                            .kilometersToMiles(radius / 1000);

                    radiusInMiles = (radiusInMiles > PatrolSyncService.MAX_QUERY_RADIUS) ? PatrolSyncService.MAX_QUERY_RADIUS
                            : radiusInMiles;

                    GeoCoordinate newCenter = getMap().getCenter();

                    patrolManager.requestPatrolUpdate(newCenter.getLatitude(),
                            newCenter.getLongitude(), radius);

                    scheduleRefresh();
                }



            }
        }

    };



    @Inject
	public PatrolMapLayer() {
	}

	

	@Override
	public void onAttachMap(Map map) {
        if(currentPatrolContainer == null)
		    currentPatrolContainer = MapFactory.createMapContainer();

        if(modifiablePatrolContainer == null)
		    modifiablePatrolContainer = MapFactory.createMapContainer();


		map.addMapObject(currentPatrolContainer);


        scheduleRefresh(); 
	}



    @Override
	public void onDetachMap(Map map) {
		super.onDetachMap(map);

        if (map != null)
        {
            map.removeMapObject(currentPatrolContainer);
            map.removeMapObject(modifiablePatrolContainer);
        }

	    cancelRefresh();

	}

    private void cancelRefresh() {
        if (updateHandler != null && mapUpdateTask != null)
            updateHandler.removeCallbacks(recurringMapUpdateTask);

    }


    private void scheduleRefresh() {
       updateHandler.postDelayed(recurringMapUpdateTask, ((PatrolSyncService.MAPTILE_EXPIRY_IN_SECONDS/2)*1000));

       runDrawTask();

    }




	protected void startPatrolTransaction() {
		synchronized (hashMapLock) {
			Iterator<PatrolPath> i = currentPatrolMap.keySet().iterator();
			modifiablePatrolMap.clear();
			while (i.hasNext()) {
				PatrolPath path = i.next();
				modifiablePatrolMap.put(path, currentPatrolMap.get(path));
			}
		}

	}

	protected void commit() {

		modifiablePatrolContainer.removeAllMapObjects();
		synchronized (hashMapLock) {
			Iterator<PatrolPath> i = modifiablePatrolMap.keySet().iterator();
			while (i.hasNext()) {
				PatrolPath path = i.next();
				modifiablePatrolContainer.addMapObject(modifiablePatrolMap
						.get(path));

            }

			HashMap<PatrolPath, MapPolyline> tempMap = currentPatrolMap;
			currentPatrolMap = modifiablePatrolMap;
			modifiablePatrolMap = tempMap;

		}

	}

	protected void swapMapContainers() {
		
		if(isMapAttached()){
			Map map = getMap();
			synchronized (map) {
	
				map.addMapObject(modifiablePatrolContainer);
				map.removeMapObject(currentPatrolContainer);
	
				MapContainer tempContainer = currentPatrolContainer;
				currentPatrolContainer = modifiablePatrolContainer;
				modifiablePatrolContainer = tempContainer;
			}
		}
	}

	public void draw(ArrayList<PatrolPath> patrolPaths) {
		startPatrolTransaction();

		if (!sharedPreferences.getBoolean(
				Defaults.PREFERENCE_MAP_DISPLAY_SHOW_PATROL, true)) {
			synchronized (hashMapLock) {
				modifiablePatrolMap.clear();
			}

		} else {

            //Log.v(LOGNAME,"Drawing patrol layer");

			/*int patrolPointSize = (patrolPaths.size() > MAX_PATROL_SIZE) ? MAX_PATROL_SIZE
					: patrolPaths.size();*/

			for (int i = 0; i < patrolPaths.size(); i++) {
				PatrolPath path = patrolPaths.get(i);
				GeoCoordinate[] coordinates = path.getGeoCoordinates();
				MapPolyline mapPolyline = MapUtils.generatePolyline(coordinates);

				try {
					int polylineColor = Color.parseColor("#"
							+ path.getBaseColor());
					mapPolyline.setLineColor(polylineColor);
				} catch (Exception e) {
					mapPolyline.setLineColor(Color.BLUE);
				}


				//
				mapPolyline.setLineWidth(PATROL_LINE_WIDTH);
				mapPolyline.setVisible(true);
				mapPolyline.setZIndex(500);

				synchronized (hashMapLock) {
					modifiablePatrolMap.put(path, mapPolyline);
				}

			}
		}
		commit();
		swapMapContainers();
		// mapController.getMap().redraw();
	}

	@Override
	public void show() {
		super.show();

        runDrawTask();

	}

	private void runDrawTask() {
		new BackgroundTask()
        {

            @Override
            public void onExecute()
            {

                if(isMapAttached())
                {
                    //long start = System.currentTimeMillis();
                    draw(patrolManager.getAllPaths());
                    //long end = System.currentTimeMillis();

                   // Log.i("BBQ", "Draw patrol took: " + (end - start));
                }
            }

            @Override
            public void onTimeout(long runTime)
            {
            }
        }.execute();
	}

	@Override
	public void hide() {
		super.hide();
		if (updateHandler != null && mapUpdateTask != null)
			updateHandler.removeCallbacks(mapUpdateTask);
	}

	@Override
	public void refresh() {
		
		/*GeoBoundingBox bbox = getMap().getBoundingBox();
		GeoCoordinate bottomRight = bbox.getBottomRight();
		GeoCoordinate topLeft = bbox.getTopLeft();

		Envelope envelope = new Envelope(topLeft.getLongitude(),
				bottomRight.getLongitude(), bottomRight.getLatitude(),
				topLeft.getLatitude());
		
		draw(patrolManager.getPaths(envelope));
*/
		runDrawTask();
	}

	@Override
	public void forceUpdate(double lat, double lon, double radius) {
        if (updateHandler != null)
            updateHandler.removeCallbacks(mapUpdateTask);

        updateHandler.postDelayed(mapUpdateTask, 15000);
	}

	@Override
	public void onMapMoveEnd(GeoCoordinate newCenter) {
		if (updateHandler != null)
			updateHandler.removeCallbacks(mapUpdateTask);
	
		/*
		 * requests are sent after 5000ms of map inactivity. This should prevent
		 * the app from going crazy if a user rapidly pans the map. I'm not sure
		 * how this will react with follow-mode on. It should be tested
		 */
		updateHandler.postDelayed(mapUpdateTask, 5000);

		//refresh();

	}


}
