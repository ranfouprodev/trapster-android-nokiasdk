package com.trapster.android.activity.fragment;

import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.inject.Inject;
import com.here.android.common.GeoPosition;
import com.here.android.common.LocationMethod;
import com.here.android.common.LocationStatus;
import com.here.android.common.PositionListener;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.activity.SingleTrapDetailsScreen;
import com.trapster.android.manager.PositionManager;
import com.trapster.android.manager.TrapManager;
import com.trapster.android.model.Trap;
import com.trapster.android.model.dao.TrapCursorAdapter;
import com.trapster.android.model.dao.TrapDAO;
import com.trapster.android.model.provider.TrapProvider;
import com.trapster.android.util.BackgroundTask;
import com.trapster.android.util.GeographicUtils;
import com.vividsolutions.jts.geom.Geometry;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class NearbyTrapListFragment extends AbstractListFragment implements PositionListener {

    private static final String EXTRA_BBOX = "EXTRA_BBOX";

    private static String LOGNAME = "NearbyTrapListFragment";

    @Inject TrapManager tm;

    @Inject TrapDAO trapDao;

    @Inject PositionManager pm;

    protected Cursor cursor = null;

    private TrapCursorAdapter adapter;

    private Geometry area;

    private GeoPosition location;

    private final TrapUpdateObserver trapObserver = new TrapUpdateObserver();

    private QueryNewRange query;
    private boolean isPopulated;

    public static NearbyTrapListFragment newInstance(Geometry area) {
        NearbyTrapListFragment f = new NearbyTrapListFragment();

        Bundle args = new Bundle();
        args.putSerializable(EXTRA_BBOX, area);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        area = (Geometry) getArguments().get(EXTRA_BBOX);

        setHasOptionsMenu(false);

        adapter = new TrapCursorAdapter(getParentActivity());

        location = pm.getLastLocation();

        // Allocate the adapter to the List displayed within this fragment.
        setListAdapter(adapter);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nearbytraplist, null);

        ListView lv = (ListView) view.findViewById(android.R.id.list);

        lv.setEmptyView(view.findViewById(R.id.layoutNoTraps));
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        pm.addPositionListener(this);

    }

    @Override
    public void onStop() {
        super.onStop();
        pm.removePositionListener(this);
        if(query != null)
            query.cancel();

    }

    @Override
    public void onResume() {
        super.onResume();


        if(location != null)
            refresh(location);
        getParentActivity().getContentResolver().registerContentObserver(TrapProvider.CONTENT_URI, true, trapObserver);

    }

    @Override
    public void onPause() {
        super.onPause();
        getParentActivity().getContentResolver().unregisterContentObserver(trapObserver);
    }



    public void refresh(GeoPosition _location) {
        isPopulated = true;

        this.location = _location;

        android.util.Log.v(LOGNAME,"Refreshing location:"+location);

        adapter.updateLocation(location);

        if(query != null)
            query.cancel();

        if(location != null)
            area = GeographicUtils.createBoundingBox(location.getCoordinate().getLatitude(), location.getCoordinate().getLongitude(),  Defaults.NEARBY_TRAP_RADIUS);

        query = new QueryNewRange(area);
        query.execute();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long theid) {



        Trap trap= (Trap) l.getItemAtPosition(position);

        android.util.Log.v(LOGNAME,"Trap click:"+position+":"+trap);

        if (trap != null)
        {
            showTrapView(trap);
        }

    }


    void showTrapView(Trap trap)
    {
        Intent i= new Intent(getParentActivity(), SingleTrapDetailsScreen.class);
        i.putExtra(SingleTrapDetailsScreen.TRAP_BUNDLE_KEY, trap);
        startActivity(i);
    }



    private class QueryNewRange extends BackgroundTask
    {
        private Geometry area;
        private ArrayList<Trap> traps;

        public QueryNewRange(Geometry area) {
            super();
            this.area = area;
        }
        @Override
        public void onExecute()
        {
            traps = (ArrayList<Trap>) trapDao.getNewTraps(area);
            if (location != null && isRunning())
                Collections.sort(traps, new SortByDistanceTrapComparator(location.getCoordinate().getLatitude(), location.getCoordinate().getLongitude()));

            android.util.Log.v(LOGNAME,"Found "+traps.size()+" nearby traps:"+area.toString());
        }

        @Override
        public void onTimeout(long runTime)
        {
        }

        @Override
        public void onPostExecute()
        {
            if (traps != null)
            {
                adapter.clear();

                if (isRunning())
                {
                    for(Trap trap :  traps)
                        adapter.add(trap);

                    adapter.notifyDataSetChanged();
                }

            }

        }

    }

    private class SortByDistanceTrapComparator implements Comparator<Trap> {

        private double lat;
        private double lon;

        public SortByDistanceTrapComparator(double _lat, double _lon) {
            lat = _lat;
            lon = _lon;

        }

        public int compare(Trap t1, Trap t2) {

            double d1 = GeographicUtils.geographicDistance(lat, lon, t1
                    .getLatitude(), t1.getLongitude());
            double d2 = GeographicUtils.geographicDistance(lat, lon, t2.getLatitude(), t2.getLongitude());

            return Double.valueOf(d1).compareTo(Double.valueOf(d2));
        }
    }

    @Override
    public void onPositionFixChanged(LocationMethod arg0, LocationStatus arg1) {
    }

    @Override
    public void onPositionUpdated(LocationMethod arg0, GeoPosition position) {

        if(!isPopulated)
            refresh(position);

        adapter.updateLocation(position);
    }


    private class TrapUpdateObserver extends ContentObserver
    {
        public TrapUpdateObserver()
        {
            super(null);
        }

        @Override
        public void onChange(boolean selfChange)
        {
            super.onChange(selfChange);
            refresh(location);
        }
    }

}