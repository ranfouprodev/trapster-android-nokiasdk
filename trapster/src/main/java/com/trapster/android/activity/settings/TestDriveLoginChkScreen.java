package com.trapster.android.activity.settings;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.google.inject.Inject;
import com.trapster.android.R;
import com.trapster.android.activity.LoginScreen;
import com.trapster.android.activity.Screen;
import com.trapster.android.activity.TestDriveScreen;
import com.trapster.android.manager.SessionManager;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.screen_test_drive_login_chk)
public class TestDriveLoginChkScreen extends Screen
{
    private final static String CLASS_NAME = "TestDriveLoginChkScreen";

    @Inject
    SessionManager sessionManager;

    @InjectView(R.id.joinLoginBtn) Button joinLoginBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        joinLoginBtn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v){
                //not logged in? go to login activity.
                Intent i = new Intent(TestDriveLoginChkScreen.this, LoginScreen.class);
                TestDriveLoginChkScreen.this.startActivity(i);
            }
        });
    }

    @Override

    public void onResume() {
        super.onResume();

        if(!sessionManager.isLoggedIn()){
            //start TestDriveScreen activity, finish this one so back takes you back to the settings menu.
            Intent i = new Intent(this, TestDriveScreen.class);
            startActivity(i);
            this.finish();
        }
    }

    @Override
    protected String getClassName(){
        return CLASS_NAME;
    }
}
