package com.trapster.android.activity;

import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import com.trapster.android.BuildFeatures;
import com.trapster.android.R;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

@ContentView(R.layout.screen_about)
public class AboutScreen extends Screen
{
    private final static String CLASS_NAME = "AboutScreen";
    private final static String ABOUT_FILE_LOCATION = "about.html";

    @InjectView(R.id.aboutWebView) WebView webView;
    @InjectView(R.id.aboutLoadingText) TextView loadingText;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setTitle(R.string.about_title);
        String formattedText;
        try
        {
            InputStream inputStream = null;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            try
            {
                byte[] buffer = new byte[1024 * 4];
                inputStream = getAssets().open(ABOUT_FILE_LOCATION, AssetManager.ACCESS_STREAMING);
                for (int i = inputStream.read(buffer); i != -1; i = inputStream.read(buffer))
                    out.write(buffer, 0, i);
                formattedText = new String(out.toByteArray());
                WebSettings settings = webView.getSettings();
                settings.setJavaScriptEnabled(true);
                settings.setSaveFormData(true);
                formattedText = String.format(formattedText, BuildFeatures.VERSIONNAME);
                webView.loadDataWithBaseURL("", formattedText, "text/html", "utf-8", "");
                webView.setBackgroundColor(0);
                webView.setBackgroundResource(R.drawable.carbon_background);

                webView.setWebViewClient(new WebViewClient()
                {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        Uri uri = Uri.parse(url);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                        return true;
                    }


                    @Override
                    public void onPageFinished(WebView view, String url)
                    {
                        loadingText.setVisibility(View.GONE);
                        webView.setVisibility(View.VISIBLE);
                    }
                });


            }
            finally
            {
                if (inputStream != null)
                    inputStream.close();
            }
        }
        catch (IOException e)
        {
            loadingText.setText(R.string.about_text_error);
        }



    }

    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }
}