package com.trapster.android.activity.fragment;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PointF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.inject.Inject;
import com.here.android.common.GeoCoordinate;
import com.here.android.common.GeoPosition;
import com.here.android.common.ViewObject;
import com.here.android.mapping.Map;
import com.here.android.mapping.*;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.manager.*;
import com.trapster.android.manager.flurry.FLURRY_EVENT_NAME;
import com.trapster.android.manager.flurry.FLURRY_PARAMETER_NAME;
import com.trapster.android.manager.flurry.FlurryEvent;
import com.trapster.android.model.RouteLink;
import com.trapster.android.model.RouteLinkPoint;
import com.trapster.android.model.RouteLinkQuadkey;
import com.trapster.android.model.UpdatedRouteLinkInfo;
import com.trapster.android.service.RouteLinkUpdateMonitor;
import com.trapster.android.util.GeographicUtils;
import com.vividsolutions.jts.geom.*;
import roboguice.inject.InjectFragment;
import roboguice.inject.InjectView;

import java.util.*;

/**
 * Created with IntelliJ IDEA. User: snazi Date: 12/3/12 Time: 4:08 PM Allow the
 * user to select route links in order to update their speed limits
 */
public class ReportSpeedLimitFragment extends AbstractFragment implements MapEventListener {
    private static final String LOGNAME = "ReportSpeedLimitFragment";

    private static final String DISPLAY_THANK_YOU_KEY_BUNDLE = "displaythankyoukeybundle";
    private static final String SELECTED_ROUTE_LINK_BUNDLE = "selectedroutelinkbundle";
    private static final String FIRST_KEY = "firstkey";

    private static final int SPEED_LIMIT_CHANGE_VALUE = 5;
    private static final int SPEED_LIMIT_MIN_VALUE = 5;
    private static final int SPEED_LIMIT_DEFAULT_VALUE = 25;
    private static final int SPEED_LIMIT_MAX_VALUE = 200;
    private static final int ROUTE_LINK_LINE_WIDTH = 5;

    public static final int UPDATE_STREET = 0;

    @InjectFragment(R.id.speedLimitBottomBarFragment)BottomBarFragment bottomBarFragment;

    @InjectView(R.id.textSpeedlimit)TextView textSpeedLimit;
    @InjectView(R.id.speedLimitDecrement)ImageButton minusButton;
    @InjectView(R.id.speedLimitIncrement)ImageButton plusButton;
    @InjectView(R.id.layoutSpeedLimitContainer)RelativeLayout speedLimitContainer;
    @InjectView(R.id.layoutThankYouContainer) RelativeLayout thankYouContainer;
    @InjectView(R.id.textThankYouSpeed) TextView textThankYouSpeed;
    @InjectView(R.id.speedLimitInstructions) LinearLayout instructions;

    @Inject RouteLinkManager routeLinkManager;
    @Inject RouteLinkUpdateMonitor routeLinkUpdateMonitor;
    @Inject SharedPreferences sharedPreferences;
    @Inject UpdatedRouteLinkManager updatedRouteLinkManager;
    @Inject PositionManager positionManager;

    private MapContainer speedLimitMapObjectLayer;
    double currentSpeedLimit = -1;
    private Map map;
    private boolean displayWaypointRouteLink = false;
    private RouteLink firstKey;
    private HashMap<RouteLink, MapPolyline> selectedRouteLinks = new HashMap<RouteLink, MapPolyline>();
    private ArrayList<RouteLink> savedRouteLinks = new ArrayList<RouteLink>();
    private Timer dialogDismissTimer;
    private boolean displayThankYou = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report_speed_limit_default, null);

        return view;
    }



	public void setMap(Map map) {
		
		this.map = map;
        map.setTilt(0);
		
		speedLimitMapObjectLayer = MapFactory.createMapContainer();
		map.addMapObject(speedLimitMapObjectLayer);

        if(savedRouteLinks.size() > 0)
        {
            for (int i = 0; i < savedRouteLinks.size(); i++)
                {
                    drawRouteLinkOverlay(savedRouteLinks.get(i));
                }
            savedRouteLinks.clear();
        }
        else
            drawApproximateRouteLinks();
	}
    
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupBottomBar();

        bottomBarFragment.setNegativeAction(null, null);
        minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(currentSpeedLimit > SPEED_LIMIT_MIN_VALUE)
                    setSpeedLimit(currentSpeedLimit - SPEED_LIMIT_CHANGE_VALUE);
            }
        });

        plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(currentSpeedLimit < SPEED_LIMIT_MAX_VALUE)
                    setSpeedLimit(currentSpeedLimit + SPEED_LIMIT_CHANGE_VALUE);
            }
        });



    }

    @Override
    public void onResume()
    {
        super.onResume();
        GlobalMapEventManager.addListener(this, new GlobalMapEventManager.FUNCTION_CALL[]{GlobalMapEventManager.FUNCTION_CALL.TAP, GlobalMapEventManager.FUNCTION_CALL.MAP_MOVE_END, GlobalMapEventManager.FUNCTION_CALL.MAP_MOVE_START});

        dialogDismissTimer = new Timer("ThankYouDialogDismissTimer");

        if(displayThankYou)
            displayThankYou();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        GlobalMapEventManager.removeListener(this);

        if (dialogDismissTimer != null)
            dialogDismissTimer.cancel();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState)
    {
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putBoolean(DISPLAY_THANK_YOU_KEY_BUNDLE, displayThankYou);
        savedInstanceState.putSerializable(FIRST_KEY, firstKey);

        Iterator<RouteLink> i = selectedRouteLinks.keySet().iterator();
        while (i.hasNext())
        {
            savedRouteLinks.add(i.next());
        }
        savedInstanceState.putSerializable(SELECTED_ROUTE_LINK_BUNDLE, savedRouteLinks);
        removeRouteLinkOverlay();
        map.removeMapObject(speedLimitMapObjectLayer);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        if(savedInstanceState!= null)
        {
            displayThankYou = savedInstanceState.getBoolean(DISPLAY_THANK_YOU_KEY_BUNDLE, false);
            savedRouteLinks = (ArrayList)savedInstanceState.getSerializable(SELECTED_ROUTE_LINK_BUNDLE);
            firstKey = (RouteLink) savedInstanceState.getSerializable(FIRST_KEY);
        }

    }

    private void drawApproximateRouteLinks()
    {
        RouteLink broadcastRouteLink = routeLinkUpdateMonitor.getBroadcastRouteLink();
        Set<RouteLink> broadcastApproximateRouteLinks = routeLinkUpdateMonitor.getBroadcastApproximateRouteLinks();
        if (broadcastRouteLink != null && broadcastApproximateRouteLinks != null)
        {
            firstKey = broadcastRouteLink;
            drawRouteLinkOverlay(broadcastRouteLink);
            joinApproximateRouteLinks(broadcastRouteLink, broadcastApproximateRouteLinks);
        }
    }

    public void setSpeedLimit(double speedLimit)
    {
        if(speedLimit < 0)
        {
            //if current speed limit is not found then use the current speed rounded down
            //to the nearest multiple of 5 and use that as the default speed limit.
            GeoPosition geoPosition = positionManager.getLastLocation();
            speedLimit = geoPosition.getSpeed();

            if (sharedPreferences.getBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_MILES,true))
                speedLimit = GeographicUtils.metersPerSecondToMph(speedLimit);
            else
                speedLimit = GeographicUtils.metersPerSecondToKph(speedLimit);

            if (speedLimit <= SPEED_LIMIT_DEFAULT_VALUE || speedLimit >= SPEED_LIMIT_MAX_VALUE)
                speedLimit = SPEED_LIMIT_DEFAULT_VALUE;
        }

        speedLimit = (int)(speedLimit/SPEED_LIMIT_CHANGE_VALUE);
        speedLimit = speedLimit*SPEED_LIMIT_CHANGE_VALUE;

        currentSpeedLimit = speedLimit;
        textSpeedLimit.setText(String.valueOf((int)speedLimit));
    }


    private void setupBottomBar()
    {

        bottomBarFragment.setPositiveAction(getString(R.string.forgot_submit_button), new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                int numSelectedRouteLinks= selectedRouteLinks.size();

                if (numSelectedRouteLinks > 0)
                {
                    Iterator<RouteLink> iterator = selectedRouteLinks.keySet().iterator();
                    double updatedSpeedLimit= GeographicUtils.convertSpeedToMps(currentSpeedLimit, sharedPreferences);
                    ArrayList<UpdatedRouteLinkInfo> updatedRouteLinkInfoList= new ArrayList<UpdatedRouteLinkInfo>(
                            numSelectedRouteLinks);

                    while(iterator.hasNext())
                    {
                        RouteLink routeLink = iterator.next();
                        UpdatedRouteLinkInfo updatedRouteLinkInfo = new UpdatedRouteLinkInfo();
                        updatedRouteLinkInfo.setRouteLink(routeLink.getLinkId());

                        GeoCoordinate[] geoCoordinates = routeLink.toGeoCoords();

                        updatedRouteLinkInfo.setLat(geoCoordinates[0].getLatitude());
                        updatedRouteLinkInfo.setLng(geoCoordinates[0].getLongitude());
                        updatedRouteLinkInfo.setSpeedLimit(updatedSpeedLimit);
                        updatedRouteLinkInfoList.add(updatedRouteLinkInfo);
                    }

                    updatedRouteLinkManager.pushNewSpeedLimitToServer(updatedRouteLinkInfoList);
                    updatedRouteLinkManager.saveRouteLinkExtraInfo(updatedRouteLinkInfoList);

                    sendFlurrySpeedLimitSubmitEvent(updatedSpeedLimit);
                }

                removeRouteLinkOverlay();
                displayThankYou();
            }
        });
    }



    private void displayThankYou()
    {
        displayThankYou = true;
        speedLimitContainer.setVisibility(View.GONE);
        thankYouContainer.setVisibility(View.VISIBLE);
        textThankYouSpeed.setText(String.valueOf((int)currentSpeedLimit));

        dialogDismissTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                    getParentActivity().finish();
            }
        }, 5000);
    }


    private void removeRouteLinkOverlay()
    {
        Iterator iterator =  selectedRouteLinks.keySet().iterator();
        while(iterator.hasNext())
        {
            speedLimitMapObjectLayer.removeMapObject(selectedRouteLinks.get(iterator.next()));
        }
        selectedRouteLinks.clear();
        firstKey = null;
    }

    private void drawRouteLinkOverlay(RouteLink nearestRouteLink)
    {
        GeoCoordinate[] coordinates = nearestRouteLink.toGeoCoords();

        MapPolyline routeLinkOverlay = MapFactory.createMapPolyline(MapFactory.createGeoPolyline(Arrays.asList(coordinates)));
        routeLinkOverlay.setLineColor(Color.BLUE);
        routeLinkOverlay.setLineWidth(ROUTE_LINK_LINE_WIDTH);
        speedLimitMapObjectLayer.addMapObject(routeLinkOverlay);
        routeLinkOverlay.setVisible(true);

        selectedRouteLinks.put(nearestRouteLink, routeLinkOverlay);
    }

    /*
    *  author Sam
    *
    *  When selecting a route link.  If it is selected then deselect it.  If a route link with a different
    *  street name is chosen then deselect all old route link and select the newly selected one.
    *
    *
     */

    private void toggleOverlay(RouteLink tappedRouteLink)
    {

        if(selectedRouteLinks.size() == 0)
        {
            firstKey = tappedRouteLink;
            drawRouteLinkOverlay(tappedRouteLink);
            return;
        }

        if(selectedRouteLinks.containsKey(tappedRouteLink))
        {
            speedLimitMapObjectLayer.removeMapObject(selectedRouteLinks.get(tappedRouteLink));
            selectedRouteLinks.remove(tappedRouteLink);
            if(selectedRouteLinks.size() == 0)
                firstKey = null;
        }
        else
        {
            //if the user is choosing the route link with a different same street name remove all chosen route links prior to drawing
            if (firstKey != null && tappedRouteLink != null)
            {
                String firstStreetName = firstKey.getAddressInfo().getStreet();
                String tappedStreetName = tappedRouteLink.getAddressInfo().getStreet();
                if (firstStreetName != null && tappedRouteLink != null)
                {
                    if(!firstStreetName.equals(tappedStreetName))
                        removeRouteLinkOverlay();
                }

            }

            drawRouteLinkOverlay(tappedRouteLink);
            firstKey = tappedRouteLink;
        }
    }

    private void joinApproximateRouteLinks(RouteLink nearestRouteLink, Set<RouteLink> approximateRouteLinks)
    {
        String street= nearestRouteLink.getAddressInfo().getStreet();
        double nearestRouteLinkSpeedLimit = nearestRouteLink.getSpeedLimit();

        for (Iterator<RouteLink> q= approximateRouteLinks.iterator(); q.hasNext();)
        {
            RouteLink approximateRouteLink= q.next();
            if (street != null && street.equals(approximateRouteLink.getAddressInfo().getStreet())
                    && nearestRouteLinkSpeedLimit == approximateRouteLink.getSpeedLimit())
                drawRouteLinkOverlay(approximateRouteLink);
        }
    }

    /*
     *  Author Sam
     *
     *  After a database check of the nearest route links a check is perform to find the nearest route link
     *  based on where the user tapped on the screen.  A compare is done from each point on each route link
     *  to the users tapped location.  Lowest distance wins!!!
     */


    private RouteLink findNearestRouteLink(ArrayList<RouteLink> routeLinkArrayList, GeoCoordinate tappedLocation)
    {
        double closestLocation = Double.MAX_VALUE;
        double tempClosestPointLocation = 0.0;
        RouteLink closestRouteLink = null;

        if (routeLinkArrayList.size() == 1)
            return routeLinkArrayList.get(0);
        else
        {
            for (int i = 0; i < routeLinkArrayList.size(); i++)
            {
                RouteLink routeLink = routeLinkArrayList.get(i);
                LineString routeLinkLineString = routeLink.getShapeGeometry();

                Coordinate coordinate = new Coordinate(tappedLocation.getLongitude(), tappedLocation.getLatitude());
                Point myPoint = new GeometryFactory().createPoint(coordinate);

                tempClosestPointLocation = routeLinkLineString.distance(myPoint);

                if(tempClosestPointLocation < closestLocation)
                {
                    closestLocation = tempClosestPointLocation;
                    closestRouteLink = routeLink;
                }
            }
            return closestRouteLink;
        }
    }



    private class GetNearestRouteLinks implements RouteLinkManager.OnNearestRouteLinkUpdateListener
    {
        ReportSpeedLimitFragment mOverlay;

        GetNearestRouteLinks(ReportSpeedLimitFragment overlay)
        {
            mOverlay = overlay;
        }

        @Override
        public void onNearestRouteLinkUpdated(RouteLink nearestRouteLink) {

            if(nearestRouteLink != null && displayWaypointRouteLink)
            {
                mOverlay.toggleOverlay(nearestRouteLink);
            }
        }

    }


    @Override
    public boolean onMapObjectsSelected(List<ViewObject> viewObjects) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onMapMoveStart()
    {
        if (instructions.getVisibility() != View.GONE)
        {
            getParentActivity().runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    instructions.setVisibility(View.GONE);
                }
            });
        }

    }

    @Override
    public void onMapMoveEnd(final GeoCoordinate geoCoordinate)
    {
        final ReportSpeedLimitFragment fragment = this;
        new AsyncTask<Void, Void, Void>()
        {

            @Override
            protected Void doInBackground(Void... voids)
            {
                String currentQuad = routeLinkManager.calculateQuadkey(geoCoordinate.getLatitude(), geoCoordinate.getLongitude());

                RouteLinkQuadkey routeLinkQuadkey = routeLinkManager.getQuadFromDB(currentQuad);

                //if the quad the user moved to isn't in the database then download the new quad.
                if (routeLinkQuadkey == null)
                {
                    //just fetching a new quad.  Do not display any waypoint route links.
                    displayWaypointRouteLink = false;
                    routeLinkManager.requestNearestRouteLinkUpdate(geoCoordinate.getLatitude(), geoCoordinate.getLongitude(), new GetNearestRouteLinks(fragment));
                }
                return null;
            }
        }.execute(null, null);

    }

    @Override
    public void onMapAnimatingStart() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onMapAnimatingEnd() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean onTap(final PointF point) {
        GeoCoordinate tapLocation = map.pixelToGeo(point);
        if (tapLocation != null)
        {
            Coordinate tappedCoordinate = new Coordinate(tapLocation.getLatitude(), tapLocation.getLongitude());

            Envelope envelope = new Envelope(tappedCoordinate);
            ArrayList<RouteLink> routeLinks = routeLinkManager.getRouteLinksByBBox(envelope);

            //check the database first if route links exist.  If not, fetch them from LBSP.
            if (routeLinks.size() > 0)
            {
                RouteLink routeLink = findNearestRouteLink(routeLinks, tapLocation);
                if (routeLink != null)
                    toggleOverlay(routeLink);
            }
            else
            {
                displayWaypointRouteLink = true;
                routeLinkManager.requestNearestRouteLinkUpdate(tapLocation.getLatitude(), tapLocation.getLongitude(), new GetNearestRouteLinks(this));
            }
        }

        return true;
    }

    @Override
    public boolean onDoubleTap(PointF point) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean onPinchZoom(float v, PointF point) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean onRotate(float v) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean onTilt(float v) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean onLongPressed(PointF point) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onLongPressReleased() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean onTwoFingerTap(PointF point) {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onMapSchemeSet() {
        //To change body of implemented methods use File | Settings | File Templates.
    }


    private void sendFlurrySpeedLimitCancelEvent(double speedLimit)
    {
        FlurryEvent event = FlurryManager.createEvent(FLURRY_EVENT_NAME.SPEED_LIMIT_REPORT_CANCELLED);
        String speedLimitChunk = FlurryManager.convertToMphChunk(GeographicUtils.metersPerSecondToMph(speedLimit));

        event.putParameter(FLURRY_PARAMETER_NAME.REPORTED_SPEED, speedLimitChunk);
        event.putParameter(FLURRY_PARAMETER_NAME.DRIVING_SPEED, FlurryManager.getDrivingSpeed());

        event.send();
    }

    private void sendFlurrySpeedLimitSubmitEvent(double speedLimit)
    {
        FlurryEvent event = FlurryManager.createEvent(FLURRY_EVENT_NAME.SPEED_LIMIT_REPORT_SUBMITTED);
        String speedLimitChunk = FlurryManager.convertToMphChunk(GeographicUtils.metersPerSecondToMph(speedLimit));

        event.putParameter(FLURRY_PARAMETER_NAME.REPORTED_SPEED, speedLimitChunk);
        event.putParameter(FLURRY_PARAMETER_NAME.DRIVING_SPEED, FlurryManager.getDrivingSpeed());

        event.send();
    }
}
