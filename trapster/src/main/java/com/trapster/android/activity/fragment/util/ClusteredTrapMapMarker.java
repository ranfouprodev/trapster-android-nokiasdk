package com.trapster.android.activity.fragment.util;

import android.content.res.Resources;
import android.graphics.*;
import com.here.android.common.Image;
import com.here.android.mapping.MapFactory;
import com.here.android.mapping.MapMarker;
import com.trapster.android.R;
import com.trapster.android.TrapsterApplication;
import com.trapster.android.model.Trap;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;

import java.util.List;

public class ClusteredTrapMapMarker extends AbstractMapMarker {

	private static final String LOGNAME = "ClusteredTrapMapMarker";
	private static final double THRESHOLD = 0.00001;
	private List<Trap> traps;
	private MapMarker marker;

	public ClusteredTrapMapMarker(List<Trap> traps) {
		super();
		// RoboGuice.injectMembers(TrapsterApplication.getAppInstance(), this);
		this.traps = traps;
		this.marker = convertTrapToMapObject();
	}

	@Override
	public MapMarker getMarker() {
		return marker;
	}

	@Override
	public Envelope getSpatialIndex() {
		Envelope trapEnvelope = new Envelope(new Coordinate(marker
				.getCoordinate().getLongitude(), marker.getCoordinate()
				.getLatitude()));
		trapEnvelope.expandBy(0.000001);
		return trapEnvelope;
	}

	public List<Trap> getTraps() {
		return traps;
	}


	public boolean containsTrap(int trapId) {
		for (Trap trap : traps)
			if (trap.getId() == trapId)
				return true;
		return false;
	}

	private MapMarker convertTrapToMapObject() {

		// Add the cluster icon into the map
		MapMarker mapObject = MapFactory.createMapMarker();

		double latCentroid = 0;
		double lngCentroid = 0;

		for (Trap trap : traps) {

			// calculate centroid
			latCentroid += trap.getLatitude();
			lngCentroid += trap.getLongitude();
		}


        double centroidLat = (traps.size() == 0)?0:latCentroid/traps.size();
        double centroidLng = (traps.size() == 0)?0:lngCentroid/traps.size();

        mapObject.setCoordinate(MapFactory.createGeoCoordinate(centroidLat, centroidLng));

		Image image = MapFactory.createImage();
		// Overlay the 1..9+ image onto the bitmap
		/*
		 * try { image.setImageResource(R.drawable.icon_cluster,
		 * TrapsterApplication.getAppInstance()); } catch (IOException e) {
		 * //Log.e(LOGNAME, "Unable to load cluster icon:" + e.getMessage()); }
		 */

		int number = (traps.size() > 9) ? 9 : traps.size();
		image.setBitmap(overlayNumber(BitmapFactory.decodeResource(
				TrapsterApplication.getAppInstance().getResources(),
				R.drawable.icon_cluster), number));

		// @TODO do we load the cluster png or the cluster svg and scale
		// it?
		mapObject.setIcon(image);

		/*
		 * Anchor points reference the center of the image.
		 * 
		 * The pinX and pinY in the trap type reference the top left.
		 */

		mapObject.setAnchorPoint(new PointF(32, 63));
		
		mapObject.setZIndex(100);

		return mapObject;
	}

	private Bitmap overlayNumber(Bitmap icon, int number) {

		String numberText = (number >= 9) ? "9+" : String.valueOf(number);

		Resources res = TrapsterApplication.getAppInstance().getResources();

		Paint paint = new Paint();
		paint.setColor(Color.WHITE);
		paint.setTextSize(res.getDimension(R.dimen.text_large));

		float textSize = paint.measureText(numberText);

		float x = icon.getWidth() / 2 - textSize / 2;
		float y = icon.getHeight() / 2 + 2; // Adjusted to look correct

		Bitmap bmOverlay = Bitmap.createBitmap(icon.getWidth(),
				icon.getHeight(), icon.getConfig());
		Canvas canvas = new Canvas(bmOverlay);
		canvas.drawBitmap(icon, new Matrix(), null);
		canvas.drawText(numberText, x, y, paint);
		return bmOverlay;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		ClusteredTrapMapMarker other = (ClusteredTrapMapMarker) obj;

		// Must have the same number of traps
		if (other.traps == null || other.traps.size() != traps.size())
			return false;

		if (getSpatialIndex().distance(other.getSpatialIndex()) > THRESHOLD)
			return false;

		return true;
	}

}
