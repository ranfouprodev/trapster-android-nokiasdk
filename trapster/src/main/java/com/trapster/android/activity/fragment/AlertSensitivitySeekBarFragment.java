package com.trapster.android.activity.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.trapster.android.activity.fragment.AbstractFragment;
import com.trapster.android.R;
import roboguice.inject.InjectView;

public class AlertSensitivitySeekBarFragment extends AbstractFragment
{
    @InjectView(R.id.itemSeekBarTitle) private TextView title;
    @InjectView(R.id.itemSeekBarSlider) private SeekBar seekBar;
    @InjectView(R.id.itemSeekBarMinimumValue) private TextView minimumValue;
    @InjectView(R.id.itemSeekBarMaximumValue) private TextView maximumValue;
    @InjectView(R.id.itemSeekBarQuantity) private TextView seekValue;
    @InjectView(R.id.itemSeekBarQuantityUnit) private TextView seekUnits;

    private double sizePerPiece;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_seek_bar, container, false);
        return view;
    }

    public void setTitle(String title)
    {
        this.title.setText(title);
    }

    public void setMinimumValue(int minimumValue)
    {
        this.minimumValue.setText(String.valueOf(minimumValue));
    }

    public void setMaximumValue(int maximumValue, int numPieces)
    {
        this.maximumValue.setText(String.valueOf(maximumValue));
        seekBar.setMax(numPieces);
        sizePerPiece = maximumValue / (double)numPieces;
    }

    public void setSeekLocation(int seekLocation)
    {
        seekBar.setProgress(seekLocation);
    }

    public void setSeekUnits(String units)
    {
        this.seekUnits.setText(units);
    }

    public double getSeekValue()
    {
        double num = seekBar.getProgress() * sizePerPiece * 100;
        num = Math.round(num);
        num = num / 100;
        return num;
    }

    public void setSeekListener(SeekBar.OnSeekBarChangeListener listener)
    {
        seekBar.setOnSeekBarChangeListener(listener);
    }

    public void setSeekValue(String seekValue)
    {
        this.seekValue.setText(seekValue);
    }

    public int getProgress()
    {
        return seekBar.getProgress();
    }
}
