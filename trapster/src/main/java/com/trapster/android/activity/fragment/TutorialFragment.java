package com.trapster.android.activity.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class TutorialFragment extends AbstractFragment {
	
	private static final String BUNDLE_BACKGROUND = "BUNDLE_BACKGROUND";
	
	public static TutorialFragment newInstance( int backgroundResourceId) {
		TutorialFragment f = new TutorialFragment();
		Bundle args = new Bundle();
		args.putInt(BUNDLE_BACKGROUND,backgroundResourceId);
		f.setArguments(args);
		return f;
	}

	private int background;

	@Override
	public void onCreate(Bundle bundle) {
		 super.onCreate(bundle);

		 background = getArguments() != null ? getArguments().getInt(BUNDLE_BACKGROUND) : 0;
	}
	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        View v = inflater.inflate(background, container, false);
        v.setEnabled(false);

        return v;
    }
	
	
	
}

