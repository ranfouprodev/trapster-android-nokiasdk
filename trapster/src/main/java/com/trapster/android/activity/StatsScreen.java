package com.trapster.android.activity;

import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.inject.Inject;
import com.trapster.android.R;
import com.trapster.android.activity.fragment.BottomBarFragment;
import com.trapster.android.activity.fragment.StatsFragmentEmpty;
import com.trapster.android.activity.fragment.StatsFragmentFilled;
import com.trapster.android.manager.SessionManager;
import com.trapster.android.model.Profile;
import com.trapster.android.model.UserStatistics;
import roboguice.inject.ContentView;
import roboguice.inject.InjectFragment;
import roboguice.inject.InjectView;

@ContentView(R.layout.screen_stats_new)
public class StatsScreen extends Screen {

    private static final String CLASS_NAME = "StatsScreen";
    @Inject SessionManager sessionManager;

    @InjectView(R.id.statsUserName) TextView userName;
    @InjectView(R.id.statsMemberSinceDate) TextView memberDate;
    @InjectView(R.id.statsDriversHelped) TextView driversHelped;

    @InjectView(R.id.statsUserIcon) ImageView profileIcon;

    @InjectView(R.id.statsContentContainer) LinearLayout contentContainer;

    @InjectFragment(R.id.statsNavFragment) BottomBarFragment bottomBarFragment;



    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setTitle(R.string.profile_title);
        setupContent();
    }

    private void setupContent()
    {
        Profile profile = sessionManager.getProfile();
        UserStatistics userStatistics = profile.getUserStatistics();

        userName.setText(profile.getName());
        memberDate.setText(getString(R.string.stats_member_since, DateFormat.getMediumDateFormat(this).format(profile.getSignUp())));
        driversHelped.setText(getString(R.string.stats_drivers_helped, userStatistics.getTrapStatistics().getTrapReportCount()));
        if (userStatistics == null || userStatistics.isEmpty())
        {
            getSupportFragmentManager().beginTransaction().add(contentContainer.getId(), new StatsFragmentEmpty()).setCustomAnimations(R.anim.slide_in_right, R.anim.slide_in_right, R.anim.slide_in_right, R.anim.slide_in_right).commit();
        }
        else
        {
            getSupportFragmentManager().beginTransaction().add(contentContainer.getId(), new StatsFragmentFilled()).setCustomAnimations(R.anim.slide_in_right, R.anim.slide_in_right, R.anim.slide_in_right, R.anim.slide_in_right).commit();
        }

        setupBottomBar();
    }

    private void setupBottomBar()
    {
        bottomBarFragment.setPositiveAction(getString(R.string.profile_logout_button), new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                sessionManager.logout();
                finish();
            }
        });
        bottomBarFragment.setNegativeAction(null, null);
    }

    @Override
    public void onPause()
    {
        super.onPause();
    }


    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }


}
