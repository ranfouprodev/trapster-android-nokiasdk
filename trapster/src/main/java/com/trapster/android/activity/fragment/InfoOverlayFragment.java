
package com.trapster.android.activity.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.trapster.android.R;
import com.trapster.android.activity.BecomeAModeratorScreen;
import com.trapster.android.activity.GetSatScreen;
import com.trapster.android.activity.LoginScreen;
import com.trapster.android.activity.StatsScreen;
import com.trapster.android.manager.SessionManager;
import roboguice.inject.InjectFragment;
import roboguice.inject.InjectView;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.Random;

public class InfoOverlayFragment extends AbstractFragment
{
    @Inject SessionManager sessionManager;
    private static final int INFO_ELEMENTS = 11;
    private static final String CLASS_NAME = "InfoOverlayFragment";
    public static final String INFO_SCREEN_HAS_DISPLAYED = "infoScreenHasDisplayed";

    private LinkedList<AbstractFragment> fragmentsOnScreen = new LinkedList<AbstractFragment>();

    @InjectView(R.id.mainText) TextView mainText;
    @InjectView(R.id.subText) TextView subText;
    @InjectView(R.id.container)ViewGroup container;
    @InjectView(R.id.chevronIcon) ImageView chevronIcon;

    @InjectFragment(R.id.loadingFragment) InfoOverlayLoadingFragment loadingFragment;

    private boolean hasDisplayed = false;
    private Runnable onContinueRunnable;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_info_container, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState == null || !savedInstanceState.getBoolean(INFO_SCREEN_HAS_DISPLAYED, false))
        {
            Random rng = new Random();
            setText(rng.nextInt(INFO_ELEMENTS)); // Yes this is hacky

            loadingFragment.setOnContinueClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    hide();
                    if (onContinueRunnable != null)
                        onContinueRunnable.run();
                }
            });
        }
        else
            hide();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState)
    {
        savedInstanceState.putBoolean(INFO_SCREEN_HAS_DISPLAYED, hasDisplayed);
    }

    private void setText(int whichText)
    {
        switch (whichText)
        {
            case 0:
            {
                mainText.setText(R.string.whats_new_tip_0_main_text);
                subText.setText(R.string.whats_new_tip_0_sub_text);
                setClickAction(GetSatScreen.class, false);
                break;
            }
            case 1:
            {
                mainText.setText(R.string.whats_new_tip_1_main_text);
                subText.setText(R.string.whats_new_tip_1_sub_text);
                setClickAction(BecomeAModeratorScreen.class, false);
                break;
            }
            case 2:
            {
                mainText.setText(R.string.whats_new_tip_2_main_text);
                subText.setText(R.string.whats_new_tip_2_sub_text);
                setClickAction(BecomeAModeratorScreen.class, true);
                break;
            }
            case 3:
            {
                mainText.setText(R.string.whats_new_tip_3_main_text);
                subText.setText(R.string.whats_new_tip_3_sub_text);
                setClickAction(null, false);
                break;
            }
            case 4:
            {
                mainText.setText(R.string.whats_new_tip_4_main_text);
                subText.setText(R.string.whats_new_tip_4_sub_text);
                setClickAction(null, false);
                break;
            }
            case 5:
            {
                mainText.setText(R.string.whats_new_tip_5_main_text);
                subText.setText(R.string.whats_new_tip_5_sub_text);
                setClickAction(null, false);
                break;
            }
            case 6:
            {
                mainText.setText(R.string.whats_new_tip_6_main_text);
                subText.setText(R.string.whats_new_tip_6_sub_text);
                setClickAction(null, false);
                break;
            }
            case 7:
            {
                mainText.setText(R.string.whats_new_tip_7_main_text);
                subText.setText(R.string.whats_new_tip_7_sub_text);
                chevronIcon.setVisibility(View.VISIBLE);
                container.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {

                        try
                        {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/54235437416"));
                            startActivity(intent);
                        }
                        catch(Exception e) // This will be thrown if the Facebook app isn't available on the device, in which case, go to a browser page
                        {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/54235437416")));
                        }
                    }
                });

                break;
            }
            case 8:
            {
                mainText.setText(R.string.whats_new_tip_8_main_text);
                subText.setText(R.string.whats_new_tip_8_sub_text);
                setClickAction(null, false);
                break;
            }
            case 9:
            {
                mainText.setText(R.string.whats_new_tip_9_main_text);
                subText.setText(R.string.whats_new_tip_9_sub_text);
                setClickAction(null, false);
                break;
            }
            case 10:
            {
                mainText.setText(R.string.whats_new_tip_10_main_text);
                subText.setText(R.string.whats_new_tip_10_sub_text);
                setClickAction(StatsScreen.class, true);
                break;
            }

        }
    }

    private void setClickAction(final Class<?> clazz, final boolean needsLogin)
    {
        if (clazz != null)
        {
            chevronIcon.setVisibility(View.VISIBLE);
            container.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    if (needsLogin && !sessionManager.isLoggedIn())
                    {
                        Intent intent = new Intent(getParentActivity(), LoginScreen.class);
                        intent.putExtra(LoginScreen.CLASS_EXTRA, clazz);
                        startActivity(intent);
                    }
                    else
                    {
                        Intent intent = new Intent(getParentActivity(), clazz);
                        startActivity(intent);


                    }

                }
            });

        }
        else
        {
            chevronIcon.setVisibility(View.GONE);
            container.setOnClickListener(null);
        }

    }

    @Override
    public void onPause()
    {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        for (AbstractFragment fragment : fragmentsOnScreen)
        {
            transaction.detach(fragment);
        }

        transaction.commit();

        super.onPause();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        for (AbstractFragment fragment : fragmentsOnScreen)
        {
            transaction.attach(fragment);
        }

        transaction.commit();

    }

    public void setOnContinueRunnable(final Runnable onContinueRunnable)
    {
        this.onContinueRunnable = onContinueRunnable;
    }

    @Override
    public void hide()
    {
        hasDisplayed = true;
        visible = false;
        FragmentManager manager = getFragmentManager();
        if (manager != null)
        {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
            ft.remove(this);
            ft.commitAllowingStateLoss();
        }
    }

    public void onMapLoaded()
    {
        loadingFragment.onMapLoaded();
    }

}
