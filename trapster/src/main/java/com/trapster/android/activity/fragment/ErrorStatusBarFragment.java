package com.trapster.android.activity.fragment;

import android.content.*;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.inject.Inject;
import com.here.android.common.GeoPosition;
import com.here.android.common.LocationMethod;
import com.here.android.common.LocationStatus;
import com.here.android.common.PositionListener;
import com.trapster.android.R;
import com.trapster.android.comms.CommunicationManager;
import com.trapster.android.manager.PositionManager;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;
import roboguice.inject.InjectView;

public class ErrorStatusBarFragment extends AbstractFragment implements CommunicationStatusListener, PositionListener {

    public static final String INTENT_NO_CONNECTION = "com.trapster.android.ERROR_NO_CONNECTION";
    public static final String INTENT_NO_GPS = "com.trapster.android.ERROR_NO_GPS";

	/**
	 * Number of ms to keep the error active for
	 */
	private static final long ERROR_VISIBLE_DURATION_MS = 5000L;

	private static final long ERROR_VISIBLE_LONG_DURATION_MS = 25000L;

	private static final String LOGNAME = "ErrorStatusBarFragment";

    private NoGpsStatusBroadcastReceiver noGpsStatusReceiver;

    private NoInternetStatusBroadcastReceiver noInternetStatusReceiver;

    private int animationIn;

    private int animationOut;

    @Inject SharedPreferences pref;

    @InjectView(R.id.imageErrorStatusIcon) ImageView statusImage;

    @InjectView(R.id.textErrorStatusText) TextView statusText;

    @InjectView(R.id.layoutErrorStatus) LinearLayout statusLayout;

    private boolean visible;

    @Inject CommunicationManager cm;
    @Inject PositionManager pm;
	@Inject
	LocationManager locationManager;

    private volatile boolean mapLoaded = false;
    private volatile boolean locationStatusAvailable = true;
    private volatile boolean locationFound = true;
    private volatile boolean hasConnection = true;

    private Handler handler = new Handler(Looper.getMainLooper());
	private Runnable dismissRunnable = new Runnable() {

		@Override
		public void run() {
			hide();
		}

	};



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        noGpsStatusReceiver = new NoGpsStatusBroadcastReceiver();
        noInternetStatusReceiver = new NoInternetStatusBroadcastReceiver();

        animationIn = R.anim.slide_in_top;

        animationOut = R.anim.slide_out_top_with_fade;


    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(INTENT_NO_CONNECTION);
        getParentActivity().registerReceiver(noInternetStatusReceiver, intentFilter);


        intentFilter = new IntentFilter();
        intentFilter.addAction(INTENT_NO_GPS);
        getParentActivity().registerReceiver(noGpsStatusReceiver, intentFilter);

        cm.setGlobalCommunicationStatusListener(this);

        pm.addPositionListener(this);

        checkStatus();
    }

    @Override
    public void onPause() {
        super.onPause();
        getParentActivity().unregisterReceiver(noInternetStatusReceiver);

        getParentActivity().unregisterReceiver(noGpsStatusReceiver);

        cm.removeGlobalCommunicationStatusListener();

        pm.removePositionListener(this);

        handler.removeCallbacks(dismissRunnable);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_statusbar, container, false);

        return v;
    }

    public void setStatus(final int statusResource, final int iconResource)
    {
        handler.post(new Runnable()
        {
            @Override
            public void run()
            {
                statusText.setText(statusResource);
                statusImage.setImageResource(iconResource);
                ErrorStatusBarFragment.this.show(0L);
            }
        });

    }


    private void show(long delay) {
        visible = true;

        FragmentTransaction fragmentTransaction = getParentActivity()
                .getSupportFragmentManager().beginTransaction();

        fragmentTransaction.setCustomAnimations(animationIn, animationOut);

        fragmentTransaction.show(this);

       try
       {
           fragmentTransaction.commit();

           if(delay != 0){

               if (handler != null)
                   handler.removeCallbacks(dismissRunnable);

               handler.postDelayed(dismissRunnable, delay);
           }
       }
       catch (IllegalStateException e)
       {
           if (handler != null)
               handler.removeCallbacks(dismissRunnable);
       }




    }

    public void hide() {

        handler.post(new Runnable()
        {
            @Override
            public void run()
            {
                visible = false;
                FragmentTransaction fragmentTransaction = getParentActivity()
                        .getSupportFragmentManager().beginTransaction();

                fragmentTransaction.setCustomAnimations(animationIn, animationOut);

                fragmentTransaction.hide(ErrorStatusBarFragment.this);

                try{fragmentTransaction.commitAllowingStateLoss();}catch (IllegalStateException e){} // If the activity dies
            }
        });
    }

    private void showMapLoadingStatus()
    {
        handler.post(new Runnable()
        {
            @Override
            public void run()
            {
                setStatus(R.string.map_loading, R.drawable.map);
            }
        });
    }

    private void showNoConnectionError(){
    	handler.post(new Runnable(){

			@Override
			public void run() {

		    	statusImage.setImageResource(R.drawable.no_connection_icon);
		    	statusText.setText(R.string.error_no_connection);
		    	show(ERROR_VISIBLE_DURATION_MS);
		    	statusLayout.setEnabled(false);
			}

    	});
    }

    private void showNoGpsError(){

      	handler.post(new Runnable(){

    			@Override
    			public void run() {

    			 	statusImage.setImageResource(R.drawable.no_location_icon);
    		    	statusText.setText(R.string.error_no_gps);
    		    	show(ERROR_VISIBLE_LONG_DURATION_MS);
    		    	statusLayout.setEnabled(false);
    			}

        	});
      }

    private void showGpsOffError(){

      	handler.post(new Runnable(){

    			@Override
    			public void run() {

    			 	statusImage.setImageResource(R.drawable.no_location_icon);
    		    	statusText.setText(R.string.error_gps_off);
                    ErrorStatusBarFragment.this.show(0L);

    		    	statusLayout.setEnabled(true);
    		    	statusLayout.setOnClickListener(new OnClickListener(){

						@Override
						public void onClick(View v) {
							Intent gpsOptionsIntent = new Intent(
									android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
							getParentActivity().startActivity(gpsOptionsIntent);
						}

    		    	});
    			}

        	});
      }


    private boolean isGpsEnabled(){
    	return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }


    private class NoGpsStatusBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
        	locationStatusAvailable = false;
        }
    }


    private class NoInternetStatusBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
        	hasConnection = false;
            checkStatus();
        }
    }


    @Override
    public void onPositionFixChanged(LocationMethod arg0, LocationStatus status)
    {
        locationStatusAvailable = (pm.getStatus() == LocationStatus.TEMPORARILY_UNAVAILABLE);
        if (pm.getStatus() == LocationStatus.TEMPORARILY_UNAVAILABLE)
            locationStatusAvailable = false;
        else if (pm.getStatus() == LocationStatus.OUT_OF_SERVICE)
        {
            locationStatusAvailable = false;
        }
        else if (status == LocationStatus.AVAILABLE)
        {
            locationStatusAvailable = true;
        }

        checkStatus();
    }

	@Override
	public void onPositionUpdated(LocationMethod arg0, GeoPosition arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onOpenConnection() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCloseConnection() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConnectionError(String errorMessage) {
		if(errorMessage.equalsIgnoreCase(CommunicationManager.NODATACONNECTION)){
			//showNoConnectionError();
            hasConnection = false;
            checkStatus();
		}
	}

    public void onMapLoaded()
    {
        mapLoaded = true;
        checkStatus();
    }

    private void checkStatus()
    {
        if (!locationStatusAvailable)
            showNoGpsError();
        else if (!isGpsEnabled())
            showGpsOffError();
        else if (!mapLoaded)
            showMapLoadingStatus();
        else if (!locationFound)
            showNoGpsError();
        else if (!hasConnection)
            showNoConnectionError();
        else
            hide();

        //Log.i("BBQ", toString());
    }

    public String toString()
    {
        return "locationStatusAvailable: " + locationStatusAvailable +  ", mapLoaded: " + mapLoaded + ", isGpsEnabled: " + isGpsEnabled() + ", locationFound: " + locationFound + ", hasConnection: " + hasConnection;
    }

}
