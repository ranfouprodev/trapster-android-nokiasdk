package com.trapster.android.activity.fragment;

import roboguice.inject.InjectView;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.trapster.android.R;

/**
 * 
 * Creates the pagination elements for the trap selector pages
 * 
 * @author John
 *
 */
public class PaginationFragment extends AbstractFragment {

	private int activePage;
	private int numberOfPages;

	@InjectView(R.id.layoutPaginationContainer) LinearLayout paginationLayout;
	
	private OnPaginationListener onPaginationListener; 
	
	
	public interface OnPaginationListener{
		void onNextPage(); 
		void onPreviousPage(); 
	}
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Defaults
		activePage = 1; 
		numberOfPages = 1;
	}
	
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater
                .inflate(R.layout.fragment_pagination, container, false);

        return v;
    }


	
	

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		refreshUi(); 
	}


	private void refreshUi(){
		
		/*
		 * Add the pagination views
		 */
		paginationLayout.removeAllViews();
		
		for(int i = 1; i < numberOfPages+1; i++){
			
			ImageView pager = new ImageView(getParentActivity());
			
			if(i == activePage)
				pager.setImageResource(R.drawable.pagination_dot_on);
			else
				pager.setImageResource(R.drawable.pagination_dot_off);
			
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			lp.setMargins(5, 0, 5,0);
						
			pager.setLayoutParams(lp);
			
			paginationLayout.addView(pager);
			
		}
	
		
	}
	

	public void setActivePage(int page){
		this.activePage = page; 
		refreshUi();
	}
	
	public void setNumberOfPages(int numberOfPages){
		this.numberOfPages = numberOfPages;
	}


	public OnPaginationListener getOnPaginationListener() {
		return onPaginationListener;
	}


	public void setOnPaginationListener(OnPaginationListener onPaginationListener) {
		this.onPaginationListener = onPaginationListener;
	}
	
}
