package com.trapster.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.google.inject.Inject;
import com.trapster.android.R;
import com.trapster.android.activity.fragment.BottomBarFragment;
import com.trapster.android.comms.ITrapsterAPI;
import com.trapster.android.manager.SessionManager;
import com.trapster.android.model.Profile;
import roboguice.inject.ContentView;
import roboguice.inject.InjectFragment;

@ContentView(R.layout.screen_become_a_mod)
public class BecomeAModeratorScreen extends Screen
{
    private static final String CLASS_NAME = "Become A Moderator";
    private static final int CLOSE_ACTIVITY_TIMEOUT = 5000;

    @Inject ITrapsterAPI api;
    @Inject SessionManager sessionManager;

    @InjectFragment(R.id.becomeAModBottomBar) BottomBarFragment bottomBarFragment;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        bottomBarFragment.setNegativeAction(null, null);
        bottomBarFragment.setPositiveAction(getString(R.string.become_a_mod_button_text), new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                if (!sessionManager.isLoggedIn())
                    goToLogin();
                else
                {
                    Profile profile = sessionManager.getUser().getProfile();
                    if (profile != null && profile.getGlobalModerator() == 0) // -1 indicates they've already made a request, any other value would be lost if the request is submitted
                    {
                        submitModeratorRequest();
                        showToastMessage(getString(R.string.become_a_mod_thank_you_text));
                        finish();
                    }

                }
            }
        });
    }

    private void submitModeratorRequest()
    {
        api.submitBecomeModeratorRequest();
    }

    private void goToLogin()
    {
        Intent i = new Intent(this, LoginScreen.class);
        startActivity(i);
    }

    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }
}
