package com.trapster.android.activity.settings.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.trapster.android.activity.fragment.AbstractFragment;

public abstract class AbstractSettingFragment extends AbstractFragment
{
    private boolean isHidden = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(getLayoutResource(), null,false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        init();
    }

    public boolean wasTouchIntercepted()
    {
        return false;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        if (isHidden)
            super.hide();
    }

    @Override
    public void hide()
    {
        isHidden = true;
        if (getFragmentManager() != null)
            super.hide();
    }

    @Override
    public void show()
    {
        isHidden = false;
        if (getFragmentManager() != null)
            super.show();
    }

    public boolean wantsToHide()
    {
        return isHidden;
    }

    protected abstract void init();
    protected abstract int getLayoutResource();
    public abstract void setOnClickListener(View.OnClickListener listener);
}
