package com.trapster.android.activity.fragment.login;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.widget.EditText;

import com.trapster.android.activity.LoginScreen;
import com.trapster.android.activity.fragment.AbstractFragment;
import com.trapster.android.model.User;

/**
 * Created by John on 8/15/13.
 */
public abstract class AbstractLoginFragment extends AbstractFragment{

    private Status status;
    private LoginScreen loginActivity;

    public enum Status{Busy, Error, Available}

    abstract void disable();

    abstract void enable();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(! (getActivity() instanceof LoginScreen))
            throw new RuntimeException("LoginFragment must be attached to LoginScreen");

        loginActivity = (LoginScreen)getActivity();

        setRetainInstance(true);
    }

    public LoginScreen getLoginActivity(){
        return loginActivity;
    }

    public void setStatus(Status status){
        this.status = status;
    }

    public Status getStatus(){return status;}

    public void showDialog(DialogFragment dialogFragment){

    }

    public void showDialog(int id){
        getParentActivity().showDialog(id);
    }

    public void showDialog(Dialog dialog){
        dialog.show();
    }

    public void startRegister(User user){
        loginActivity.startRegister(user);
    }

    @Override
    public void onPause() {
        super.onPause();
        fadeOutHandler.removeMessages(0);
    }

    public void showHighlightedEditText(int editTextId)
    {
        EditText editText = (EditText) getView().findViewById(editTextId);
        if(editText != null){
            editText.selectAll();
            Message msg = fadeOutHandler.obtainMessage(0);
            msg.obj = editText;
            fadeOutHandler.sendMessageDelayed(msg,5000);
        }
    }

    private static Handler fadeOutHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            EditText editText = (EditText) msg.obj;
            if(editText != null){
                editText.setSelection(0);
            }
         }
    };
}
