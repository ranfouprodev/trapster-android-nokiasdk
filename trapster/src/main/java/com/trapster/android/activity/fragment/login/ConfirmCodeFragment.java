package com.trapster.android.activity.fragment.login;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.inject.Inject;
import com.trapster.android.R;
import com.trapster.android.activity.LoginScreen;
import com.trapster.android.activity.component.PopupMessage;
import com.trapster.android.manager.SessionManager;

import roboguice.inject.InjectView;

/**
 * Created by John on 8/15/13.
 */
public class ConfirmCodeFragment extends AbstractLoginFragment {

    private static final String LOGNAME = "ConfirmCodeFragment";

    public static ConfirmCodeFragment newInstance(Bundle args){
        ConfirmCodeFragment f = new ConfirmCodeFragment();
        f.setArguments(args);
        return f;
    }

    @Inject
    SessionManager sm;

    @InjectView(R.id.buttonConfirm)
    Button buttonConfirm;

    @InjectView(R.id.buttonResend)
    Button buttonResend;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatus(Status.Available);

        getParentActivity().setTitle(R.string.confirm_title);

        // @todo do we have to check for a valid user? They shouldn't get here if they don't

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login_confcode,null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLoginActivity().show(LoginScreen.SCREEN_FORGOT);
            }
        });

        buttonResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resend();
            }
        });

    }

    private void resend() {
        sm.resendLogin(sm.getUser());
        PopupMessage.makeText(getLoginActivity(),getString(R.string.enter_confirmation_text),PopupMessage.STYLE_ALERT).show();
    }

    @Override
    void disable() {
        buttonResend.setEnabled(false);
        buttonConfirm.setEnabled(false);
    }

    @Override
    void enable() {
        buttonResend.setEnabled(true);
        buttonConfirm.setEnabled(true);
    }



}
