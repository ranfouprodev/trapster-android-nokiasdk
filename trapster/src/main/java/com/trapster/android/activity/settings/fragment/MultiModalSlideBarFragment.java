package com.trapster.android.activity.settings.fragment;

import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.trapster.android.R;
import roboguice.inject.InjectView;

public class MultiModalSlideBarFragment extends AbstractSettingFragment
{
    public static final long ANIMATION_INSTANT = 1;
    public static final long ANIMATION_QUICK = 200;
    public static final long ANIMATION_SMOOTH = 350;

    private static final String BUNDLE_KEY = "bundleKeyMultiModalPosition";

    @InjectView(R.id.multiModalSliderObject) ImageView multiModalSliderObject;
    @InjectView(R.id.multiModalBackground) ImageView multiModalBackground;

    @InjectView(R.id.multiModalTextLeft) TextView textLeft;
    @InjectView(R.id.multiModalTextRight) TextView textRight;
    @InjectView(R.id.multiModalTextCenter) TextView textCenter;

    @InjectView(R.id.multiModalDotLeft) ImageView dotLeft;
    @InjectView(R.id.multiModalDotRight) ImageView dotRight;
    @InjectView(R.id.multiModalDotCenter) ImageView dotCenter;

    @InjectView(R.id.multiModalTitle) TextView titleText;

    @InjectView(R.id.multiModalSliderContainer) RelativeLayout sliderContainer;

    private int backgroundLeftBound;
    private int backgroundRightBound;
    private int objectCenter;

    private int leftTextCenter;
    private int rightTextCenter;
    private int centerTextCenter;

    private int lastObjectLocation;

    private SLIDER_SELECTION selection = SLIDER_SELECTION.LEFT;
    private OnSliderSelectionChangedListener selectionChangedListener;

    private int titleResource;

    private int leftTextResource;
    private int rightTextResource;
    private int centerTextResource;

    @Override
    protected void init()
    {
        setTitle(getString(titleResource));

        textLeft.setText(getString(leftTextResource));
        textRight.setText(getString(rightTextResource));
        if (centerTextResource > 0)
        {
            textCenter.setText(getString(centerTextResource));
            textCenter.setVisibility(View.VISIBLE);
        }

        else
            textCenter.setVisibility(View.GONE);

        setupSlider();

        textLeft.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                moveToSelection(SLIDER_SELECTION.LEFT, ANIMATION_QUICK);
            }
        });
        textRight.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                moveToSelection(SLIDER_SELECTION.RIGHT, ANIMATION_QUICK);
            }
        });
        textCenter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                moveToSelection(SLIDER_SELECTION.CENTER, ANIMATION_QUICK);
            }
        });
    }

    @Override
    protected int getLayoutResource()
    {
        return R.layout.fragment_multi_modal_slide_bar;
    }

    @Override
    public void setOnClickListener(View.OnClickListener listener)
    {
    }

    @Override
    public void onHiddenChanged(boolean isHidden)
    {
        super.onHiddenChanged(isHidden);
        if (!isHidden)
        {
            setupSlider();
        }
    }

    public void setDescription(int resource)
    {
        titleResource = resource;
    }

    private void setTitle(String title)
    {
        titleText.setText(title);
    }

    public void setOptionText(int leftText, int rightText)
    {
        setText(leftText, rightText, -1);
    }

    public void setOptionText(int leftText, int rightText, int centerText)
    {
        setText(leftText, rightText, centerText);
    }

    private void setText(int leftText, int rightText, int centerText)
    {
        leftTextResource = leftText;
        rightTextResource = rightText;
        centerTextResource = centerText;
    }

    private void setupSlider()
    {
        if (multiModalBackground != null)
        {
            multiModalBackground.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener()
            {
                @Override
                public boolean onPreDraw()
                {
                    multiModalBackground.getViewTreeObserver().removeOnPreDrawListener(this);

                    objectCenter = (multiModalSliderObject.getWidth() / 2);

                    backgroundLeftBound = (int) (multiModalBackground.getLeft() - (dotLeft.getWidth() * 0.75));
                    backgroundRightBound = multiModalBackground.getRight() - objectCenter;


                    dotLeft.setVisibility(View.VISIBLE);
                    dotRight.setVisibility(View.VISIBLE);

                    leftTextCenter = dotLeft.getLeft() - objectCenter;
                    rightTextCenter = dotRight.getLeft();
                    if (textCenter.getVisibility() == View.VISIBLE)
                    {
                        dotCenter.setVisibility(View.VISIBLE);
                        centerTextCenter = dotCenter.getRight() - objectCenter;

                    }
                    else
                    {
                        centerTextCenter = 0;
                        dotCenter.setVisibility(View.GONE);
                    }

                    lastObjectLocation = multiModalSliderObject.getLeft() + objectCenter;

                    moveToSelection(selection, 0);
                    multiModalSliderObject.setVisibility(View.VISIBLE);



                    return false;
                }
            });

            multiModalBackground.setOnTouchListener(new SliderBarMotionListener());
        }

    }

    public void setStartSelection(SLIDER_SELECTION selection)
    {
        this.selection = selection;
    }

    private void setSelection(SLIDER_SELECTION selection)
    {
        this.selection = selection;
        if (selectionChangedListener != null)
            selectionChangedListener.onSliderSelectionChanged(selection);

    }

    public void moveToSelection(SLIDER_SELECTION selection, long animationTime)
    {
        this.selection = selection;
        //setSelection(selection);

        switch (selection)
        {
            case LEFT:
            {
                moveToTouch(leftTextCenter, animationTime);
                break;
            }
            case CENTER:
            {
                if (textCenter.getVisibility() != View.VISIBLE)
                    throw new RuntimeException("Invalid slider selection, there is no center text");
                moveToTouch(centerTextCenter, animationTime);
                break;
            }
            case RIGHT:
            {
                moveToTouch(rightTextCenter - objectCenter, animationTime);
                break;
            }
        }


    }

    public void setSelectionChangedListener(OnSliderSelectionChangedListener listener)
    {
        selectionChangedListener = listener;
    }

    private boolean checkForTextClick(float xLocation)
    {
        boolean wasTextClick = false;
        float distanceToLeftText = Math.abs(leftTextCenter - xLocation);
        float distanceToRightText = Math.abs(rightTextCenter - xLocation);
        float distanceToCenterText = Float.MAX_VALUE;
        if (textCenter.getVisibility() == View.VISIBLE)
            distanceToCenterText = Math.abs(centerTextCenter - xLocation);


        float validLeftTextDistance = Math.abs((float) (textLeft.getRight() - textLeft.getLeft()));
        float validRightTextDistance = Math.abs((float) (textRight.getRight() - textRight.getLeft()));
        float validCenterTextDistance = Math.abs((float) (textCenter.getRight() - textCenter.getLeft()));

        if (distanceToLeftText <= validLeftTextDistance)
        {
            moveToTouch(leftTextCenter, ANIMATION_QUICK);
            setSelection(SLIDER_SELECTION.LEFT);
            wasTextClick = true;
        }
        else if (distanceToRightText <= validRightTextDistance)
        {
            moveToTouch(rightTextCenter, ANIMATION_QUICK);
            setSelection(SLIDER_SELECTION.RIGHT);
            wasTextClick = true;
        }
        else if (distanceToCenterText <= validCenterTextDistance)
        {
            moveToTouch(centerTextCenter, ANIMATION_QUICK);
            setSelection(SLIDER_SELECTION.CENTER);
            wasTextClick = true;
        }

        return wasTextClick;
    }

    private void snapToText(float xLocation)
    {
        float start = multiModalSliderObject.getLeft() + objectCenter;
        float destination = (xLocation + objectCenter) - start;

        if (destination < backgroundLeftBound)
            destination = backgroundLeftBound;
        else if (destination + objectCenter > backgroundRightBound)
            destination = backgroundRightBound - objectCenter;

        float distanceToLeftText = Math.abs(leftTextCenter - destination);
        float distanceToRightText = Math.abs(rightTextCenter - destination);
        float distanceToCenterText = Float.MAX_VALUE;
        if (textCenter.getVisibility() == View.VISIBLE)
            distanceToCenterText = Math.abs(centerTextCenter - destination);

        if (distanceToLeftText < distanceToRightText && distanceToLeftText < distanceToCenterText)
        {
            moveToTouch(leftTextCenter, ANIMATION_QUICK);
            setSelection(SLIDER_SELECTION.LEFT);
        }
        else if (distanceToRightText < distanceToLeftText && distanceToRightText < distanceToCenterText)
        {
            moveToTouch(rightTextCenter - objectCenter, ANIMATION_QUICK);
            setSelection(SLIDER_SELECTION.RIGHT);
        }
        else
        {
            moveToTouch(centerTextCenter, ANIMATION_QUICK);
            setSelection(SLIDER_SELECTION.CENTER);
        }

    }

    private void moveToTouch(float xLocation, long animationTime)
    {
        float start = multiModalSliderObject.getLeft() + objectCenter;
        float destination = (xLocation + objectCenter) - start;

        if (destination < backgroundLeftBound)
            destination = backgroundLeftBound;
        else if (destination + objectCenter > backgroundRightBound)
            destination = backgroundRightBound - objectCenter;


        multiModalSliderObject.startAnimation(new SliderMoveTranslationAnimation(lastObjectLocation, destination, 0, 0, animationTime));
        lastObjectLocation = (int) destination;
    }

    private class SliderMoveTranslationAnimation extends TranslateAnimation
    {
        public SliderMoveTranslationAnimation(float fromXDelta, float toXDelta, float fromYDelta, float toYDelta, long animationTime)
        {
            super(fromXDelta, toXDelta, fromYDelta, toYDelta);
            setDuration(animationTime);
            setFillAfter(true);
        }
    }

    private boolean wasTouchedIntercepted = false;

    @Override
    public boolean wasTouchIntercepted()
    {
        boolean oldState = wasTouchedIntercepted;
        wasTouchedIntercepted = false;
        return oldState;
    }

    private class SliderBarMotionListener implements View.OnTouchListener
    {
        private boolean isDown;

        @Override
        public boolean onTouch(View view, MotionEvent event)
        {

            switch (event.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                {
                    moveToTouch(event.getX(), ANIMATION_QUICK);
                    isDown = true;
                    wasTouchedIntercepted = true;
                    break;
                }
                case MotionEvent.ACTION_CANCEL:
                {
                    isDown = false;
                    moveToSelection(selection, ANIMATION_QUICK);
                    //snapToText(event.getX());
                    wasTouchedIntercepted = true;
                    break;
                }
                case MotionEvent.ACTION_UP:
                {
                    isDown = false;
                    if (!checkForTextClick(event.getX()))
                        snapToText(event.getX());
                    wasTouchedIntercepted = true;

                    break;
                }
                case MotionEvent.ACTION_MOVE:
                {
                    if (isDown)
                    {
                        moveToTouch(event.getX(), ANIMATION_INSTANT);
                    }
                    wasTouchedIntercepted = true;
                    break;
                }
            }
            return wasTouchedIntercepted;
        }
    }

    public enum SLIDER_SELECTION
    {
        LEFT,
        CENTER,
        RIGHT
    }

    public interface OnSliderSelectionChangedListener
    {
        public void onSliderSelectionChanged(SLIDER_SELECTION selection);
    }
}
