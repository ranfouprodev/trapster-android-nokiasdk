package com.trapster.android.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.TaskStackBuilder;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.inject.Inject;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.activity.fragment.PaginationFragment;
import com.trapster.android.activity.fragment.TutorialFragment;
import com.trapster.android.service.TrapsterService;

import java.util.LinkedList;

import roboguice.inject.ContentView;
import roboguice.inject.InjectFragment;
import roboguice.inject.InjectView;

@ContentView(R.layout.screen_tutorial)
public class TutorialScreen extends Screen {

	private static final String LOGNAME = "TutorialScreen";
	private final static String CLASS_NAME = "Tutorial";
    public static final String EXTRA_SHOW_FOOTER_BUTTONS = "extrashowfooterbuttons";

    @InjectView(R.id.buttonNext)
	Button buttonNext;

	@InjectView(R.id.buttonSkip)
	Button buttonSkip;

    @InjectView(R.id.layoutButtonControl)
    LinearLayout layoutButtons;

	
	@InjectFragment(R.id.fragmentPagination) PaginationFragment paginationFragment;

	public static enum Animation {
		forward, backward, none
	}

	private LinkedList<Fragment> fragmentList = new LinkedList<Fragment>();

	private int currentScreen = 0;

	@Inject
	SharedPreferences sharedPreferences;
	
	
	private GestureDetector gestureDetector;

	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);

        disableActionBar();

		// Gesture detection
		gestureDetector = new GestureDetector(this, new CustomGestureListener());
		
		TutorialFragment fragment = TutorialFragment
				.newInstance(R.layout.fragment_startup_adjusting_map);
		addScreen(fragment);

		fragment = TutorialFragment
				.newInstance(R.layout.fragment_startup_howitworks);
		addScreen(fragment);

		if (bundle != null && bundle.containsKey("screen")) {
			int screen = bundle.getInt("screen");
			show(screen, Animation.none);
		} else
			show(0, Animation.none);

        if (getIntent().hasExtra(EXTRA_SHOW_FOOTER_BUTTONS)) {
            boolean showButtons = getIntent().getBooleanExtra(EXTRA_SHOW_FOOTER_BUTTONS,true);
            if(!showButtons)
                buttonSkip.setVisibility(View.GONE);
        }


		paginationFragment.setNumberOfPages(fragmentList.size());

		TrapsterService.detectionActive = false; 
		
	}




	public void onSignupClick(View view) {
		// Don't reshow the tutorial next startup
		sharedPreferences.edit()
				.putBoolean(Defaults.PREFERENCE_SHOW_TUTORIAL, true)
				.commit();

		Intent intent = new Intent(this, MapScreen.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		// startActivity(intent);

		TaskStackBuilder.from(this).addNextIntent(intent)
				.startActivities();

		finish();
	}

	public void onNextClick(View view) {
		if (!showNext())
			continueToMain();
	}
	
	public void onSkipClick(View view){
		continueToMain(); 
	}

	@Override
	public void onBackPressed() {
        super.onBackPressed();
		//continueToMain();

	}

	private void continueToMain() {

		//TrapsterService.detectionActive = true;
		
		//sharedPreferences.edit()
		//		.putBoolean(Defaults.PREFERENCE_SHOW_TUTORIAL, true)
		//		.commit();

		//Intent intent = new Intent(this, MapScreen.class);
		//intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		//intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		//startActivity(intent);

		finish();

	}

	public void addScreen(Fragment fragment) {
		
		fragmentList.addLast(fragment);
	}

	public boolean showNext() {
		if (currentScreen != fragmentList.size() - 1)
			show(currentScreen + 1, Animation.forward);
		else
			return false;
		return true;
	}

	public boolean showPrevious() {

		if (currentScreen != 0)
			show(currentScreen - 1, Animation.backward);
		else
			return false;
		return true;
	}

	public void show(int screenIndex, Animation animation) {

		View view = findViewById(R.id.wizard_container);

		if (view == null || !(view instanceof ViewGroup))
			throw new MissingContainerException();

		if (screenIndex >= fragmentList.size() - 1)
        {
			screenIndex = fragmentList.size() - 1;
            buttonNext.setText(R.string.tutorial_button_done);
        }
        else
            buttonNext.setText(R.string.tutorial_button_next);

		if (screenIndex <= 0)
			screenIndex = 0;
{/* 
		//Log.d(LOGNAME, "Showing Screen:" + screenIndex); */}

		currentScreen = screenIndex;

		Fragment newFragment = fragmentList.get(screenIndex);

		FragmentTransaction ft = this.getSupportFragmentManager()
				.beginTransaction();

		if (animation != null) {
			switch (animation) {
			case forward:
				ft.setCustomAnimations(R.anim.slide_in_right,
						R.anim.slide_out_left);
				break;
			case backward:
				ft.setCustomAnimations(R.anim.slide_in_left,
						R.anim.slide_out_right);
				break;
			}
		}

		ft.replace(R.id.wizard_container, newFragment, "wizardfragment");

		ft.commit();
		
		paginationFragment.setActivePage(currentScreen+1);

	}

	public class MissingContainerException extends RuntimeException {

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putInt("screen", currentScreen);
		super.onSaveInstanceState(outState);
	}

	@Override
	protected String getClassName() {
		return CLASS_NAME;
	}


    @Override
	public boolean onTouchEvent(MotionEvent event) {
		
		if (gestureDetector.onTouchEvent(event))
			return true;
		else
			return false;
	}

	private class CustomGestureListener extends
			GestureDetector.SimpleOnGestureListener {

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			
			if (velocityX < 0) {
				if (!showNext())
					continueToMain();
			} else
				showPrevious();

			return true;
		}

		@Override
		public boolean onSingleTapUp(MotionEvent e) {

			if (!showNext())
				continueToMain();
			return true;
		}

	}
}
