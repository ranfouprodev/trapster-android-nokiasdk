package com.trapster.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import com.trapster.android.R;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.screen_expanded_information)
public class ExpandedInformationScreen extends Screen
{
    public static String BUNDLE_KEY_IMAGE = "bundleKeyImage";
    public static String BUNDLE_KEY_HEADLINE = "bundleKeyHeadline";
    public static String BUNDLE_KEY_BODY_TEXT = "bundleKeyBodyText";

    @InjectView(R.id.headlineImage) ImageView headlineImage;
    @InjectView(R.id.headlineText) TextView headlineText;
    @InjectView(R.id.bodyText) TextView bodyText;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        headlineImage.setImageDrawable(getResources().getDrawable(intent.getIntExtra(BUNDLE_KEY_IMAGE, R.drawable.icon)));
        headlineText.setText(intent.getStringExtra(BUNDLE_KEY_HEADLINE));
        bodyText.setText(intent.getStringExtra(BUNDLE_KEY_BODY_TEXT));
        setTitle(intent.getStringExtra(BUNDLE_KEY_HEADLINE));
    }

    @Override
    protected String getClassName()
    {
        return null;
    }
}
