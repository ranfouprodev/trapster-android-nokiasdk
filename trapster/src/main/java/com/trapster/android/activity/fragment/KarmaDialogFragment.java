package com.trapster.android.activity.fragment;

import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.trapster.android.activity.StatsScreen;
import com.trapster.android.R;
import roboguice.inject.InjectView;

public class KarmaDialogFragment extends AbstractFragment
{
    @InjectView(R.id.karmaDescriptionText)
    TextView descriptionText;
    @InjectView (R.id.karmaEarnText) TextView earnText;
    @InjectView (R.id.karmaVoteEarnText) TextView voteEarnText;
    @InjectView (R.id.karmaLinkText) TextView hyperlinkText;
    @InjectView (R.id.karmaCloseButton) Button closeButton;

    private StatsScreen statsScreen;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_karma_dialog, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        descriptionText.setText(getString(R.string.karma_how_to_earn, getString(R.string.karma_points_name)));
        earnText.setText(getString(R.string.karma_points_description, getString(R.string.karma_points_name)));
        voteEarnText.setText(getString(R.string.karma_earn_trap_vote, getString(R.string.karma_points_name)));

        hyperlinkText.setText(Html.fromHtml(getString(R.string.karma_hyperlink, getString(R.string.karma_points_name))));
        hyperlinkText.setMovementMethod(LinkMovementMethod.getInstance());





        final KarmaDialogFragment fragment = this;

        closeButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
              //  statsScreen.hideHelpDialog();
            }
        });
    }

    public void show(StatsScreen statsScreen)
    {
        this.statsScreen = statsScreen;
        super.show();
    }


}