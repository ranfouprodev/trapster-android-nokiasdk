package com.trapster.android.activity.fragment.component;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.google.inject.Inject;
import com.trapster.android.manager.BitmapCacheManager;
import com.trapster.android.manager.BitmapCacheManager.BitmapLoadedListener;
import com.trapster.android.manager.TrapManager;
import com.trapster.android.model.Trap;
import com.trapster.android.model.TrapType;

import roboguice.RoboGuice;

public class TrapMapImageView extends RelativeLayout {

	private static final int COMPLETE = 0;
	private static final int FAILED = 1;
    private static final String LOGNAME = "TrapMapImageView";

    private Context mContext;
	
	private ProgressBar mSpinner;
	private ImageView mImage;
	private ImageView mTrapIconImage;

	private int width, height;
	private boolean sizeKnown;
	
	private Trap trap;
	
	private Bitmap mapBitmap;
	
	
	@Inject TrapManager tm;
	
	@Inject BitmapCacheManager bm;
	private TrapType trapType;
	
	private final Handler imageLoadedHandler = new Handler(
			new Handler.Callback() {
				@Override
				public boolean handleMessage(Message msg) {
					switch (msg.what) {
					default:
						mImage.setImageBitmap(mapBitmap);
						mImage.setVisibility(View.VISIBLE);
						
						mTrapIconImage.setImageBitmap(tm.getMapIconForTrap(trap));
						mTrapIconImage.setPadding(0, 0, 0, mTrapIconImage.getHeight()/2);
						
						mTrapIconImage.setVisibility(View.VISIBLE);
						mSpinner.setVisibility(View.GONE);
						
						break;
					}
					return true;
				}
			});
	
	
	public TrapMapImageView(final Context context, final AttributeSet attrSet) {
		super(context, attrSet);
		initialize(context);
	}

	public TrapMapImageView(final Context context) {
		super(context);
		initialize(context);
	}

	private void initialize(final Context context) {
		
		RoboGuice.injectMembers(context, this);
		
		mContext = context;

		setGravity(Gravity.CENTER);

		mImage = new ImageView(mContext);
		mImage.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT));

		mTrapIconImage = new ImageView(mContext);
		
		LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.CENTER_IN_PARENT, 1);
		
		mTrapIconImage.setLayoutParams(params);
		
		
		mSpinner = new ProgressBar(mContext);
		//mSpinner.setIndeterminateDrawable(new BitmapDrawable(BitmapFactory.decodeResource(getResources(), R.drawable.spinner)));
		mSpinner.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT));

		mSpinner.setIndeterminate(true);

		addView(mSpinner);
		addView(mImage);
		addView(mTrapIconImage);
		

		getViewTreeObserver().addOnGlobalLayoutListener(
				new ViewTreeObserver.OnGlobalLayoutListener() {

					private boolean sizeKnown;

					@Override
					public void onGlobalLayout() {
						if (!sizeKnown) {
							width = getWidth();
							height = getHeight();
							sizeKnown = true;
							loadImage();
						}
					}
				});
	}


	private void loadImage() {

		if(width != 0 && height != 0)
    //        bm.getMapImageSnapshot(trap.getLatitude(), trap.getLongitude(), 14, width, height,new TrapMapMarker(trap), new MapLoaderListener(trap.getId()));
         bm.getMapImageSnapshot(trap.getLatitude(), trap.getLongitude(), 14, width, height, new MapLoaderListener(trap.getId()));
		
	}

	public void setTrap(Trap trap) {
		this.trap = trap; 
		this.trapType = tm.getTrapType(trap.getTypeid());
		
		mSpinner.setVisibility(View.VISIBLE);
		mImage.setVisibility(View.GONE);
		mTrapIconImage.setVisibility(View.GONE);

        loadImage();
	}


	

	private class MapLoaderListener implements BitmapLoadedListener{
		
		private int trapId; 
		
		
		public MapLoaderListener(int trapId) {
			super();
			this.trapId = trapId;
		}


		@Override
		public void onBitmapLoaded(Bitmap bitmap) {

			if(trapId == trap.getId()){
			
				mapBitmap = bitmap;
				
				imageLoadedHandler.sendEmptyMessage(COMPLETE);
			}else{
                // TrapMapImageView has been recycled
                // loadImage();
                //Log.e(LOGNAME,"ImageLoader reset:"+trap.getId()+":"+trapId);
            }
		}
		
	}
	

}
