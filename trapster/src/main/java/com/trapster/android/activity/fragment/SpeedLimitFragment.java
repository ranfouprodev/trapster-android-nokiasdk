package com.trapster.android.activity.fragment;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.inject.Inject;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.activity.LoginScreen;
import com.trapster.android.activity.ReportSpeedLimitScreen;
import com.trapster.android.manager.FlurryManager;
import com.trapster.android.manager.SessionManager;
import com.trapster.android.manager.flurry.FLURRY_EVENT_NAME;
import com.trapster.android.manager.flurry.FLURRY_PARAMETER_NAME;
import com.trapster.android.manager.flurry.FlurryEvent;
import com.trapster.android.model.RouteLink;
import com.trapster.android.service.RouteLinkBroadcastReceiver;
import com.trapster.android.service.RouteLinkNotificationListener;
import com.trapster.android.service.RouteLinkUpdateMonitor;
import com.trapster.android.util.GeographicUtils;

import java.util.Set;

import roboguice.inject.InjectView;

public class SpeedLimitFragment extends AbstractFragment implements RouteLinkNotificationListener
{
    private final static int NO_KNOWN_SPEED_LIMIT_COUNT = 20;

    @Inject SharedPreferences sharedPreferences;
    @Inject SessionManager sessionManager;
    @Inject RouteLinkUpdateMonitor routeLinkUpdateMonitor;
    @InjectView(R.id.layoutSpeedLimit) LinearLayout container;
    @InjectView(R.id.textSpeedlimit) TextView textViewSpeedLimit;

    private double currentSpeedLimit = -1;
    double lastKnownSpeedLimit = -1;
    int numRepeatedNoSpeedLimit;

    private final Handler handler = new Handler();
    private RouteLinkBroadcastReceiver routeLinkBroadcastReceiver;
    private OnSpeedLimitUpdatedListener onSpeedLimitUpdatedListener;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_speed_limit, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        setup();
    }

    @Override
    public void onStart()
    {
        super.onStart();
        IntentFilter routeLinkBroadcastFilter = new IntentFilter();
        routeLinkBroadcastFilter.addAction(RouteLinkBroadcastReceiver.INTENT_BROADCAST);
        routeLinkBroadcastReceiver = new RouteLinkBroadcastReceiver(this);
        getParentActivity().registerReceiver(routeLinkBroadcastReceiver, routeLinkBroadcastFilter);
    }

    @Override
    public void onStop()
    {
        getParentActivity().unregisterReceiver(routeLinkBroadcastReceiver);
        super.onStop();
    }

    public double getCurrentSpeedLimit()
    {
        return GeographicUtils.convertSpeedToUnits(currentSpeedLimit, sharedPreferences);
    }

    private void setup()
    {
        container.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent;
                if (sessionManager.getProfile() != null && sessionManager.getUser() != null)
                {
                    sendFlurrySpeedLimitStartedEvent();
                    intent = new Intent(getParentActivity(), ReportSpeedLimitScreen.class);
                    intent.putExtra(ReportSpeedLimitScreen.SPEED_LIMIT_EXTRA, currentSpeedLimit);
                } else
                    intent = new Intent(getParentActivity(), LoginScreen.class);

                startActivity(intent);
            }
        });
    }

    public void updateCurrentSpeedLimit()
    {
        RouteLink broadcastRouteLink = routeLinkUpdateMonitor.getBroadcastRouteLink();
        if (broadcastRouteLink != null)
        {
            double newSpeedLimit = broadcastRouteLink.getSpeedLimit();

            if (newSpeedLimit != 0 && newSpeedLimit != Defaults.UNLIMITED_SPEED_998 && newSpeedLimit != Defaults.UNLIMITED_SPEED_999)
                currentSpeedLimit = newSpeedLimit;
        }
        if (currentSpeedLimit != -1)
            handler.post(new Runnable()
            {
                @Override
                public void run()
                {
                    textViewSpeedLimit.setText(String.valueOf(GeographicUtils.convertSpeedToUnits(currentSpeedLimit, sharedPreferences)));
                }
            });
    }

    private void sendFlurrySpeedLimitStartedEvent()
    {
        FlurryEvent event = FlurryManager.createEvent(FLURRY_EVENT_NAME.SPEED_LIMIT_REPORT_STARTED);
        event.putParameter(FLURRY_PARAMETER_NAME.DRIVING_SPEED, FlurryManager.getDrivingSpeed());

        event.send();
    }

    private void updateSpeedLimit(Message msg)
    {
        final double[] speedLimitObjs = (double[]) msg.obj;
        //
        float fromYDelta1;
        float toYDelta1;
        final float fromYDelta2;
        final float toYDelta2;
        int speedLimitSoundResId;
        if (speedLimitObjs[0] > speedLimitObjs[1])
        {
            fromYDelta1 = 0;
            toYDelta1 = 32;
            fromYDelta2 = -32;
            toYDelta2 = 0;
            speedLimitSoundResId = R.raw.speed_up;
        } else
        {
            fromYDelta1 = 0;
            toYDelta1 = -32;
            fromYDelta2 = 32;
            toYDelta2 = 0;
            speedLimitSoundResId = R.raw.speed_down;
        }
        Animation rollAnim1 = new TranslateAnimation(0, 0, fromYDelta1, toYDelta1);
        rollAnim1.setDuration(1000);
        rollAnim1.setAnimationListener(new Animation.AnimationListener()
        {
            public void onAnimationStart(Animation animation)
            {
            }

            public void onAnimationRepeat(Animation animation)
            {
            }

            public void onAnimationEnd(Animation animation)
            {
                textViewSpeedLimit.setText(String.valueOf(GeographicUtils.convertSpeedToUnits(speedLimitObjs[0], sharedPreferences)));
                //
                Animation rollAnim2 = new TranslateAnimation(0, 0, fromYDelta2, toYDelta2);
                rollAnim2.setDuration(1000);
                textViewSpeedLimit.startAnimation(rollAnim2);
            }
        });
        textViewSpeedLimit.startAnimation(rollAnim1);

        /*if (sharedPreferences.getBoolean(Defaults.PREFERENCE_MAP_DISPLAY_ALERT_SPEED_LIMIT, false) &&
                speedLimitObjs[0] != speedLimitObjs[1])
            soundManager.playSound(speedLimitSoundResId);*/

    }

    private void resetSpeedLimit()
    {
        float fromYDelta1 = 0;
        float toYDelta1 = -32;
        final float fromYDelta2 = 32;
        final float toYDelta2 = 0;
        Animation rollAnim1 = new TranslateAnimation(0, 0, fromYDelta1, toYDelta1);
        rollAnim1.setDuration(1000);
        rollAnim1.setAnimationListener(new Animation.AnimationListener()
        {
            public void onAnimationStart(Animation animation)
            {
            }

            public void onAnimationRepeat(Animation animation)
            {
            }

            public void onAnimationEnd(Animation animation)
            {
                textViewSpeedLimit.setText("--");
                //
                Animation rollAnim2 = new TranslateAnimation(0, 0, fromYDelta2, toYDelta2);
                rollAnim2.setDuration(1000);
                textViewSpeedLimit.startAnimation(rollAnim2);
            }
        });
        textViewSpeedLimit.startAnimation(rollAnim1);
    }


    @Override
    public void onRouteLinkUpdate(RouteLink routeLink, Set<RouteLink> approximateRouteLinks, int resultCode)
    {
        double newSpeedLimit = 0;

        if (resultCode == RouteLinkUpdateMonitor.RESULT_CODE_SUCCESS | resultCode == RouteLinkUpdateMonitor.RESULT_CODE_NO_LINK)
        {
            if (routeLink != null)
                newSpeedLimit = routeLink.getSpeedLimit();

            if (newSpeedLimit != 0 && newSpeedLimit != Defaults.UNLIMITED_SPEED_998 && newSpeedLimit != Defaults.UNLIMITED_SPEED_999)
            {
                updateCurrentSpeedLimit();
                if (newSpeedLimit != currentSpeedLimit)
                {
                    currentSpeedLimit = newSpeedLimit;
                    lastKnownSpeedLimit = currentSpeedLimit;
                    if (onSpeedLimitUpdatedListener != null)
                        onSpeedLimitUpdatedListener.onSpeedLimitUpdated(currentSpeedLimit);
                }
                numRepeatedNoSpeedLimit = 0;
            } else
                numRepeatedNoSpeedLimit++;

            if (numRepeatedNoSpeedLimit > NO_KNOWN_SPEED_LIMIT_COUNT && currentSpeedLimit != -1)
            {
                handler.post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        resetSpeedLimit();
                    }
                });
                currentSpeedLimit = -1;
            }

        }
    }

    public void setOnSpeedLimitUpdatedListener(OnSpeedLimitUpdatedListener onSpeedLimitUpdatedListener)
    {
        this.onSpeedLimitUpdatedListener = onSpeedLimitUpdatedListener;
    }

    public interface OnSpeedLimitUpdatedListener
    {
        public void onSpeedLimitUpdated(double newLimit);
    }
}
