package com.trapster.android.activity.fragment.login;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.inject.Inject;
import com.trapster.android.PHPDefaults;
import com.trapster.android.R;
import com.trapster.android.comms.UsernameAvailableListener;
import com.trapster.android.comms.rest.IUserAPI;
import com.trapster.android.model.User;
import com.trapster.android.util.Log;
import com.trapster.android.util.UsernameUtils;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;

import roboguice.inject.InjectView;

/**
 * Used to create a new username when the login comes from an oauth source
 */
public class MergeFacebookFragment extends AbstractLoginFragment {

    private static final String LOGNAME = "MergeFacebookFragment";

    @InjectView(R.id.editPassword)
    EditText editPassword;

    @InjectView(R.id.buttonDone)
    Button buttonDone;

    @InjectView(R.id.textForgotPassword)
    TextView textForgotPassword;

    @InjectView(R.id.layoutCheckPassword)
    LinearLayout layoutCheckPassword;

    @Inject
    IUserAPI api;

    private User user;


    public static MergeFacebookFragment newInstance(Bundle args){
        MergeFacebookFragment f = new MergeFacebookFragment();
        f.setArguments(args);
        return f;
    }

    public static MergeFacebookFragment newInstance(User user){
        MergeFacebookFragment f = new MergeFacebookFragment();
        Bundle args = new Bundle();
        args.putParcelable("USER",user);
        f.setArguments(args);
        return f;
    }




    @Override
    void disable() {

    }

    @Override
    void enable() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null && getArguments().containsKey("USER")){
            user = getArguments().getParcelable("USER");
        }else{
            user = getLoginActivity().getRegisterUser();
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register_mergefacebook,null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String password =editPassword.getText().toString().trim();

                getLoginActivity().sendMergeFacebook(password);

            }
        });

        // setup handler for forgot password button
        textForgotPassword.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                final WebView webView = (WebView) getParentActivity().findViewById(R.id.forgotPasswordWebView);

                webView.loadUrl(PHPDefaults.PHP_API_FORGOT_PASSWORD);
                webView.setWebViewClient(new WebViewClient()
                {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url)
                    {
                        view.loadUrl(url);
                        return true;
                    }

                    @Override
                    public void onPageFinished(WebView view, String url)
                    {
                        webView.setVisibility(View.VISIBLE);
                    }
                });
                WebSettings webSettings = webView.getSettings();
                webSettings.setJavaScriptEnabled(true);
            }
        });

        layoutCheckPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                switch (event.getAction() & MotionEvent.ACTION_MASK) {

                    case MotionEvent.ACTION_DOWN:
                        editPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_OUTSIDE:
                        editPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        break;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        break;
                    case MotionEvent.ACTION_MOVE:
                        break;
                }

                return true;

            }
        });


    }




}
