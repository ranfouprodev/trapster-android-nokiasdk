package com.trapster.android.activity.fragment;

import android.app.Activity;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.google.inject.Inject;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.activity.MapScreen;
import com.trapster.android.activity.fragment.component.CompassTextView;
import com.trapster.android.activity.fragment.component.CustomTextView;
import com.trapster.android.manager.MapController;
import com.trapster.android.model.RouteLink;
import com.trapster.android.model.Trap;
import com.trapster.android.receiver.TrapsterServiceBroadcastReceiver;
import com.trapster.android.service.RouteLinkNotificationListener;
import com.trapster.android.service.RouteLinkUpdateMonitor;
import com.trapster.android.service.TrapsterServiceNotificationListener;
import com.trapster.android.util.GeographicUtils;
import roboguice.inject.InjectView;

import java.util.ArrayList;
import java.util.Set;

public class SpeedometerFragment extends AbstractFragment
{

	protected static final String LOGNAME = "SpeedometerFragment";

    static final int AUTO_ZOOM_MIN_LEVEL = 15;
    static final int AUTO_ZOOM_MAX_LEVEL = 20;
    static final int AUTO_ZOOM_MIN_SPEED_IN_MPS = 5;

	@InjectView(R.id.textCompass) CompassTextView compassView;
	@InjectView(R.id.textSpeed) CustomTextView speedView;
	@InjectView(R.id.imageDial) ImageView mapDial;

	@Inject MapController mapController;
	@Inject SharedPreferences sharedPreferences;
    @Inject RouteLinkUpdateMonitor routeLinkUpdateMonitor;
	
	private int bearing = 0; 
	private int speed = 0;
    private double currentLimit = 0;
	
	private Handler handler = new Handler(Looper.getMainLooper());

	
	
	private Runnable updateCompass = new Runnable(){

		@Override
		public void run() {
			// Strangely on some devices we are getting MAX_INT for a value
			if(speed > 999 ||speed < 5)
				speed = 0; 
			
			speedView.setText(String.valueOf(speed));
			compassView.setBearing(bearing);
		}
		
	};

	private TrapsterServiceBroadcastReceiver receiver;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_speedometer, null);

		return view; 
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		handler.postDelayed(updateCompass, 250);
        mapDial.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
            }
        });
	}

	@Override
	public void onPause() {
		super.onPause();

		if(receiver != null)
			getParentActivity().unregisterReceiver(receiver);

	}

	@Override
	public void onResume() {
		super.onResume();

		receiver= new TrapsterServiceBroadcastReceiver(new SpeedUpdateListener());
		IntentFilter filter= new IntentFilter();
		filter.addAction(TrapsterServiceBroadcastReceiver.INTENT_BROADCAST);
		getParentActivity().registerReceiver(receiver, filter);

	}


    /*
      * Receiver for Speed/Bearing Updates
      */
	private class SpeedUpdateListener implements TrapsterServiceNotificationListener{

        private static final String SERVICE_LISTENER_OWNER_NAME = "SpeedometerFragment.SpeedUpdateListener";

		@Override
		public void onActiveTrapUpdate(ArrayList<Trap> traps) {
		}

		@Override
		public void onUpdateSpeedAndDirection(double _speed, int direction) {

			/*
			 * I suspect it is possible that the receiver gets updated information while the fragment is inthe process of onCreate or onDestroy, 
			 * this might be the reason why sharedPreferences == null
			 */
			if(sharedPreferences != null){
			
				bearing = direction; 
	
				speed = (int) GeographicUtils.convertSpeedToUnits(_speed, sharedPreferences);

                if(routeLinkUpdateMonitor.getBroadcastRouteLink() != null)
                    currentLimit = GeographicUtils.convertSpeedToUnits(routeLinkUpdateMonitor.getBroadcastRouteLink().getSpeedLimit(), sharedPreferences);
				
	
	            /*if(sharedPreferences.getBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_AUTO_ZOOM, false) && _speed > AUTO_ZOOM_MIN_SPEED_IN_MPS)
	                setAutoZoomLevel(_speed);*/

                Activity activity = getParentActivity();
                if (activity != null)
                {
                    activity.runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            setSpeedometerColors();
                        }
                    });
                }

				
				handler.post(updateCompass);
			}
		}

        private void setSpeedometerColors() {

          MapScreen mapScreen =  (MapScreen)getParentActivity();


            // Although ridiculous, 2.3 devices allow fragments to not be attached to an activity onResume, causing getParentActivity() to return null
            if (mapScreen != null)
            {

                double overageLimit = currentLimit + (currentLimit*.1);

                if(currentLimit == 0 || speed < 5){
                    mapDial.setImageResource(R.drawable.main_dial);
                    speedView.setTextColor(Color.BLACK);
                }
                else if(speed < currentLimit || currentLimit <= 0){
                    mapDial.setImageResource(R.drawable.main_dial_green);
                    speedView.setTextColor(Color.BLACK);
                }else if(speed < overageLimit){
                    mapDial.setImageResource(R.drawable.main_dial_yellow);
                    speedView.setTextColor(Color.BLACK);
                }else{
                    mapDial.setImageResource(R.drawable.main_dial_red);
                    speedView.setTextColor(Color.RED);
                }
            }
		}

		private void setAutoZoomLevel(double speedInMPS)
        {
            double zoomLevel =  AUTO_ZOOM_MAX_LEVEL - ((speedInMPS - AUTO_ZOOM_MIN_LEVEL)/3);
            zoomLevel = Math.ceil(zoomLevel);

            if (zoomLevel <= AUTO_ZOOM_MIN_LEVEL)
                zoomLevel = AUTO_ZOOM_MIN_LEVEL;
            if (zoomLevel > AUTO_ZOOM_MAX_LEVEL)
                zoomLevel = AUTO_ZOOM_MAX_LEVEL;

        }


		@Override
		public void onError(int errorCode, String message) {
		}

        @Override
        public String getOwner()
        {
            return SERVICE_LISTENER_OWNER_NAME;
        }
    }

    public void setCurrentLimit(double limit)
    {
        currentLimit = limit;
    }
}
