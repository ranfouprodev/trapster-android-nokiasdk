package com.trapster.android.activity.fragment;

import android.content.SharedPreferences;
import com.google.inject.Inject;
import com.here.android.common.GeoPosition;
import com.here.android.mapping.Map;
import com.here.android.mapping.MapAnimation;
import com.here.android.mapping.MapFactory;
import com.trapster.android.Defaults;
import com.trapster.android.manager.MapController;
import com.trapster.android.manager.PositionManager;

public class BlankMapFragment extends AbstractMapFragment {

	private static final String LOGNAME = "BlankMapFragment";

	@Inject MapController mapController;
	
	@Inject SharedPreferences sharedPreferences;

	@Inject PositionManager pm;

    private static final int DEFAULT_ZOOM_LEVEL = 17;

	private Map map;
	 
	public interface OnBlankMapReadyListener{
 		void onMapReady(Map map);
 	}
	
	private OnBlankMapReadyListener onBlankMapReadyListener;
	
	@Override
	protected void onMapCreated(Map map)
    {
		this.map = map; 
		GeoPosition position = pm.getLastLocation();
		
		/*
		 * Load the map to zoom level 15
		 */
		getMap().setCenter(MapFactory.createGeoCoordinate(position.getCoordinate().getLatitude(), position.getCoordinate().getLongitude()),MapAnimation.NONE);
		getMap().setZoomLevel(Defaults.DEFAULT_MAP_ZOOM_LEVEL);
        
        if(onBlankMapReadyListener != null)
        	onBlankMapReadyListener.onMapReady(map);
    }

    @Override
    protected void restoreMap(MapConfig mapConfig)
    {
        mapConfig.restoreMap(map);
    }

    @Override
    protected void setupDefaultValues()
    {
        GeoPosition lastLocation = positionManager.getLastLocation();

        if(lastLocation.getCoordinate().isValid() && lastLocation.getCoordinate().getLatitude() != 0)
            map.setCenter(lastLocation.getCoordinate(),MapAnimation.NONE);

        //mapController.setDefaultTilt(map);
    }

    public OnBlankMapReadyListener getOnBlankMapReadyListener() {
		return onBlankMapReadyListener;
	}

	public void setOnBlankMapReadyListener(OnBlankMapReadyListener onBlankMapReadyListener) {
		this.onBlankMapReadyListener = onBlankMapReadyListener;
	}
	
}
