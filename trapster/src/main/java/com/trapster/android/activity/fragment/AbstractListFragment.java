package com.trapster.android.activity.fragment;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import roboguice.fragment.RoboListFragment;

public class AbstractListFragment extends RoboListFragment
{
    private FragmentActivity parentActivity;

    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        parentActivity = (FragmentActivity) activity;
    }

    public final FragmentActivity getParentActivity()
    {
        if (parentActivity != null)
            return parentActivity;
        else
            return super.getActivity();
    }
}
