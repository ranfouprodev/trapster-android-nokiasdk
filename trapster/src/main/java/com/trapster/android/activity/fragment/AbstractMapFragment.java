package com.trapster.android.activity.fragment;

import android.app.Activity;
import android.graphics.PointF;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import com.google.inject.Inject;
import com.here.android.common.GeoPosition;
import com.here.android.common.LocationMethod;
import com.here.android.common.LocationStatus;
import com.here.android.common.PositionListener;
import com.here.android.mapping.*;
import com.trapster.android.Defaults;
import com.trapster.android.activity.fragment.util.MapMotionDetector;
import com.trapster.android.manager.GlobalMapEventManager;
import com.trapster.android.manager.MapController;
import com.trapster.android.manager.PositionManager;
import roboguice.RoboGuice;

/**
 * @author John
 */
public abstract class AbstractMapFragment extends com.here.android.mapping.MapCompatibilityFragment
{
    protected static final String LOGNAME = "AbstractMapFragment";
    protected Map map;
    private MapConfig currentMapState;

    @Inject PositionManager positionManager;
    @Inject GlobalMapEventManager globalMapEventManager;
    @Inject MapController mapController;
    private OneTimePositionChangedListener listener;
    private FragmentActivity parentActivity;

    private final ZoomLimiter zoomLimiter = new ZoomLimiter();

    protected abstract void onMapCreated(Map map);
    protected abstract void restoreMap(MapConfig mapConfig);
    protected abstract void setupDefaultValues();

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        RoboGuice.getInjector(getActivity()).injectMembersWithoutViews(this);

        if (android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.HONEYCOMB)
            MapSettings.setAntiAliasingSupportEnabled(false);

        setRetainInstance(true);
    }

    /**
     * These are the map settings that are universal across all fragments
     */
    private void initializeMapItems()
    {
        setCopyrightLogoPosition(MapCopyrightLogoPosition.TOP_RIGHT);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        RoboGuice.getInjector(getActivity()).injectViewMembers(this);

    }


    @Override
    public void onResume()
    {
        super.onResume();
        initializeAndDisplayMap();
        addMapEventListener(zoomLimiter);
    }

    @Override
    public void onPause()
    {
        super.onPause();

        if (listener != null)
            positionManager.removePositionListener(listener);
        removeMapEventListener(globalMapEventManager);
        removeMapEventListener(zoomLimiter);

    }

    private boolean hasInitialized = false;

    private void initializeAndDisplayMap()
    {
        if (!hasInitialized)
        {
            if (positionManager.hasFirstLocation() || positionManager.isLocationEnabled())
            {
                init(new FragmentInitListener()
                {

                    @Override
                    public void onFragmentInitializationCompleted(InitError initError)
                    {
                        if (initError == InitError.NONE)
                        {
                            setup();
                        }
                        else
                        {
                            map = null;
                            throw new RuntimeException("Error initializing map fragment, init error: " + initError);
                        }
                    }
                });

            }
            else
            {
                listener = new OneTimePositionChangedListener();
                positionManager.addPositionListener(listener);
            }
        }
        else
        {
            setup();
        }


    }

    private void setup()
    {
        // now the map is ready to be used
        map = getMap();
        addMapEventListener(globalMapEventManager);
        onMapCreated(map);
        mapController.updateMapScheme(map);
        if (currentMapState != null)
            restoreMap(currentMapState);
        else
            setupDefaultValues();

        initializeMapItems();

        show();
    }


    @Override
    public void onSaveInstanceState(Bundle bundle)
    {
        super.onSaveInstanceState(bundle);
        if (map != null)
        {
            currentMapState = new MapConfig(map);
            bundle.putParcelable("State", currentMapState);
        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        // Restore map state
        if (savedInstanceState != null && savedInstanceState.containsKey("State"))
        {
            currentMapState = (MapConfig) savedInstanceState.getParcelable("State");
        }
    }

    public MapConfig getCurrentMapConfig(){
        return new MapConfig(map);
    }


    public boolean isMapAttached()
    {
        return map != null;
    }

    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        parentActivity = (FragmentActivity) activity;
    }

    public final FragmentActivity getParentActivity()
    {
        if (parentActivity != null)
            return parentActivity;
        else
            return super.getActivity();
    }

    public void show()
    {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.show(this);
        ft.commit();
    }

    public void hide()
    {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.hide(this);
        ft.commit();
    }

    public void setCurrentMapState(MapConfig currentMapState)
    {
        this.currentMapState = currentMapState;
    }

    public static class MapConfig implements Parcelable
    {
        double lat;
        double lng;
        float tilt;
        double zoom;
        float orientation;
        String scheme;

        public MapConfig(Parcel in)
        {
            readFromParcel(in);
        }

        public MapConfig(Map map)
        {
            lat = map.getCenter().getLatitude();
            lng = map.getCenter().getLongitude();
            tilt = map.getTilt();
            zoom = map.getZoomLevel();
            orientation = map.getOrientation();
            scheme = map.getMapScheme();

           // Log.v(LOGNAME,"Storing map:"+toString());
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder();
            sb.append("Lat: ");
            sb.append(lat);
            sb.append(" Lng: ");
            sb.append(lng);
            sb.append(" Tilt: ");
            sb.append(tilt);
            sb.append(" Zoom: ");
            sb.append(zoom);
            sb.append(" Orientation: ");
            sb.append(orientation);
            sb.append(" Scheme: ");
            sb.append(scheme);

            return sb.toString();
        }

        private void readFromParcel(Parcel in)
        {
            lat = in.readDouble();
            lng = in.readDouble();
            tilt = in.readFloat();
            zoom = in.readDouble();
            orientation = in.readFloat();
            scheme = in.readString();
        }


        public void restoreMap(Map map)
        {
            map.setCenter(MapFactory.createGeoCoordinate(lat, lng), MapAnimation.NONE);
            map.setTilt(tilt);
            map.setZoomLevel(zoom);
            map.setOrientation(orientation);

            if(scheme != null)
                map.setMapScheme(scheme);
        }

        @Override
        public int describeContents()
        {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int flags)
        {
            parcel.writeDouble(lat);
            parcel.writeDouble(lng);
            parcel.writeFloat(tilt);
            parcel.writeDouble(zoom);
            parcel.writeFloat(orientation);
            parcel.writeString(scheme);
        }


        public static final Parcelable.Creator CREATOR = new Parcelable.Creator()
        {
            public MapConfig createFromParcel(Parcel in)
            {
                return new MapConfig(in);
            }

            public MapConfig[] newArray(int size)
            {
                return new MapConfig[size];
            }
        };

    }

    protected class OneTimePositionChangedListener implements PositionListener
    {

        @Override
        public void onPositionUpdated(LocationMethod locationMethod, GeoPosition geoPosition)
        {
            positionManager.removePositionListener(listener);
            initializeAndDisplayMap();
        }
        @Override public void onPositionFixChanged(LocationMethod locationMethod, LocationStatus locationStatus){ }
    }

    private class ZoomLimiter extends MapMotionDetector
    {
        @Override
        public boolean onPinchZoom(float scaleFactor, PointF center)
        {

            if (map != null && map.getZoomLevel() < Defaults.MAP_MAX_ZOOM_LEVEL)
                return true;
            else
                return false;
        }
    }

}
