package com.trapster.android.activity.settings.fragment;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.trapster.android.BuildFeatures;
import com.trapster.android.R;
import roboguice.inject.InjectView;

public class IconSettingFragment extends TextSettingFragment
{
    @InjectView(R.id.settingsIcon) ImageView icon;

    private int sideLength;
    private int imageResource;
    private Drawable imageDrawable;
    private View.OnClickListener listener;


    @Override
    protected int getLayoutResource()
    {
        return R.layout.fragment_settings_icon;
    }
    @Override
    protected void init()
    {
        super.init();

        if (imageDrawable != null)
            setupDrawable();
        else if (imageResource > 0)
            icon.setImageResource(imageResource);
        else
            icon.setVisibility(View.GONE);
        icon.setOnClickListener(listener);
    }

    public void setImageResource(int resource)
    {
        if (icon != null)
            icon.setImageResource(resource);
        imageResource = resource;
    }

    public void setImageDrawable(Drawable drawable, int sideLength)
    {
        this.sideLength = sideLength;
        imageDrawable = drawable;
        if (this.icon != null)
        {
            setupDrawable();
        }
    }

    public void setIconClickListener(View.OnClickListener listener)
    {
        if (icon != null)
            icon.setOnClickListener(listener);
        this.listener = listener;
    }

    private void setupDrawable()
    {
        if (sideLength > 0)
        {
            ViewGroup.LayoutParams params = icon.getLayoutParams();
            params.width = sideLength;
            params.height = sideLength;
            icon.setLayoutParams(params);

            if (BuildFeatures.SUPPORTS_HONEYCOMB)
                icon.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        icon.setImageDrawable(imageDrawable);
        icon.setVisibility(View.VISIBLE);


    }
}
