package com.trapster.android.activity.fragment.login;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.trapster.android.PHPDefaults;
import com.trapster.android.R;
import com.trapster.android.model.User;
import roboguice.inject.InjectView;

/**
 * Created by John on 8/15/13.
 */
public class LoginFragment extends AbstractLoginFragment {

    private static final String LOGNAME = "LoginFragment";

    @InjectView(R.id.textForgotPassword) TextView textForgotPassword;

    @InjectView(R.id.editName) EditText usernameText;
    @InjectView(R.id.editPasswd) EditText passwordText;

    @InjectView(R.id.buttonLogin)
    Button loginButton;


    public static LoginFragment newInstance(Bundle args){
        LoginFragment f = new LoginFragment();
        f.setArguments(args);
        return f;
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatus(Status.Available);

        getParentActivity().setTitle(R.string.signup_title);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login_normal,null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        // setup handler for forgot password button
        textForgotPassword.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                final WebView webView = (WebView) getParentActivity().findViewById(R.id.forgotPasswordWebView);

                webView.loadUrl(PHPDefaults.PHP_API_FORGOT_PASSWORD);
                webView.setWebViewClient(new WebViewClient()
                {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url)
                    {
                        view.loadUrl(url);
                        return true;
                    }

                    @Override
                    public void onPageFinished(WebView view, String url)
                    {
                        webView.setVisibility(View.VISIBLE);
                    }
                });
                WebSettings webSettings = webView.getSettings();
                webSettings.setJavaScriptEnabled(true);
            }
        });

        passwordText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {

                if (actionId== EditorInfo.IME_ACTION_SEND || actionId == EditorInfo.IME_ACTION_DONE){
                    login();
                    return true;
                }
                return false;
            }
        });


       // usernameText.setText(UsernameUtils.getPrimaryEmailAccount(getLoginActivity()));

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });


    }


    private void login(){

        String username= usernameText.getText().toString().trim();
        String password= passwordText.getText().toString().trim();

        if(validateFields(username,password)){
            getLoginActivity().sendLogin(new User(username,password));
        }

    }

    private boolean validateFields(String username, String password){


        if (username == null || password == null || username.length() == 0 || password.length() == 0)
        {
            showAlertMessage( getString(R.string.invalid_credentials_error));
            showHighlightedEditText();
            return false;
        }

        return true;

    }


    public void showHighlightedEditText()
    {
        usernameText.selectAll();
        usernameText = null;
        passwordText.selectAll();
        passwordText = null;
    }



    @Override
    void disable() {
        loginButton.setEnabled(false);

    }

    @Override
    void enable() {
        loginButton.setEnabled(true);

    }



}
