package com.trapster.android.activity;

import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.SparseArray;
import android.view.View;
import android.view.WindowManager;
import com.google.inject.Inject;
import com.trapster.android.R;
import com.trapster.android.activity.component.ConfirmationCodeDialog;
import com.trapster.android.activity.component.PopupMessage;
import com.trapster.android.activity.fragment.login.*;
import com.trapster.android.comms.BasicResponseListener;
import com.trapster.android.comms.ConfirmationListener;
import com.trapster.android.comms.EmailAvailableListener;
import com.trapster.android.comms.SessionListener;
import com.trapster.android.comms.rest.RESTUserAPI;
import com.trapster.android.comms.rest.StatusCode;
import com.trapster.android.manager.FlurryManager;
import com.trapster.android.manager.SessionManager;
import com.trapster.android.manager.UpdatedRouteLinkManager;
import com.trapster.android.manager.flurry.FLURRY_EVENT_NAME;
import com.trapster.android.manager.flurry.FlurryEvent;
import com.trapster.android.model.User;
import com.trapster.android.util.Log;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

import java.util.Stack;


@ContentView(R.layout.screen_login)
public class LoginScreen extends Screen implements SessionListener {

    private static final String LOGNAME = "LoginScreen";

    public static final String CLASS_EXTRA = "classextra";

    public static final String EXTRA_IS_ADDING = "EXTRA_IS_ADDING";

    public static final String EXTRA_SHOW_SCREEN = "EXTRA_SHOW_SCREEN";

    public static final int SCREEN_LOGIN = 0;
    public static final int SCREEN_LOGIN_MAIN = 1;
    public static final int SCREEN_CONFIRM = 2;
    public static final int SCREEN_FORGOT = 3;
    public static final int SCREEN_CHOOSENAME = 4;
    public static final int SCREEN_MERGE = 5;


    private AccountAuthenticatorResponse mAccountAuthenticatorResponse = null;

    private Bundle mResultBundle = null;


    @Inject
    RESTUserAPI api;


    @Inject
    SessionManager sessionManager;

    @Inject
    UpdatedRouteLinkManager updatedRouteLinkManager;

    @InjectView(R.id.wizard_container)
    View fragmentContainer;

    ProgressDialog progressDialog;

    ConfirmationCodeDialog confDialog;

    private static boolean isConfDialogDisplayed;

    private final static String CLASS_NAME = "Login";


    private SparseArray<Fragment> fragmentList = new SparseArray<Fragment>();
    private int currentScreen;

    private Stack<Integer> backstack = new Stack<Integer>();

    private User registerUser;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        mAccountAuthenticatorResponse =
                getIntent().getParcelableExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE);

        if (mAccountAuthenticatorResponse != null) {
            mAccountAuthenticatorResponse.onRequestContinued();
        }

        addScreens(savedInstanceState);

        if (getIntent() != null && getIntent().hasExtra(EXTRA_SHOW_SCREEN))
            show(getIntent().getIntExtra(EXTRA_SHOW_SCREEN, SCREEN_LOGIN_MAIN));
        else
            show(SCREEN_LOGIN_MAIN);

     /*   // setup handler for forgot password button
        TextView textForgotPassword= (TextView) findViewById(R.id.forgotPassword);
        textForgotPassword.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                //startActivity(new Intent(getApplicationContext(), ForgotPasswordScreen.class));
                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(PHPDefaults.PHP_API_FORGOT_PASSWORD));
                startActivity(myIntent);
            }
        });*/

    }

    private void addScreens(Bundle savedInstanceState) {
        addScreen(SCREEN_LOGIN_MAIN, StandardLoginFragment.newInstance(savedInstanceState));
        addScreen(SCREEN_LOGIN, LoginFragment.newInstance(savedInstanceState));
        addScreen(SCREEN_CONFIRM, ConfirmCodeFragment.newInstance(savedInstanceState));
        addScreen(SCREEN_FORGOT, ForgotPasswordFragment.newInstance(savedInstanceState));
        addScreen(SCREEN_CHOOSENAME, GenerateUsernameFragment.newInstance(savedInstanceState));
        addScreen(SCREEN_MERGE,MergeFacebookFragment.newInstance(savedInstanceState));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (progressDialog != null && progressDialog.isShowing())
            outState.putBoolean("SHOWING_PROGRESS", true);

        if (confDialog != null && confDialog.isShowing())
            outState.putBoolean("SHOWING_CONF", true);

        outState.putInt("screen", currentScreen);

    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        // Always call the superclass so it can restore the view hierarchy
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState.containsKey("SHOWING_PROGRESS"))
            showProgressDialog();

        if (savedInstanceState.containsKey("SHOWING_CONF"))
            confDialog.show();

        if (savedInstanceState.containsKey("screen")) {
            int screen = savedInstanceState.getInt("screen");
            show(screen);
        }

    }

    private void addScreen(int index, Fragment fragment) {

        fragmentList.put(index, fragment);
    }

    public void show(int screenIndex) {

        currentScreen = screenIndex;

        backstack.push(currentScreen);

        Fragment newFragment = fragmentList.get(screenIndex);

        FragmentTransaction ft = this.getSupportFragmentManager()
                .beginTransaction();


        ft.replace(R.id.wizard_container, newFragment, "wizardfragment");

        ft.commit();

    }

    public void sendResend(User user) {
        sessionManager.resendLogin(user);
    }


    public void sendLogin(User user) {

        showProgressDialog();
        sessionManager.login(user, null, this);
    }

    private void sendConfirmationCode(User user, String confcode) {
        showProgressDialog();
        sessionManager.login(user, confcode, this);
    }

    public void sendRegisterFacebook(User user) {

        sessionManager.registerFacebook(user, this);

    }

    public void sendRegister(String email, String username, String password, boolean terms){
        showProgressDialog();
        sessionManager.register(email,username,password,this, terms);
    }

    public void sendChooseUsername(User user){
        this.registerUser = user;
        show(SCREEN_CHOOSENAME);
    }


    public void sendMergeFacebook(String password){
        showProgressDialog();
        registerUser.setPassword(password);


        MergeFacebookListener mergeFacebookListener = new MergeFacebookListener();
        api.mergeWithFacebook(registerUser, registerUser.getAccessToken(), mergeFacebookListener,mergeFacebookListener);
    }

    private class MergeFacebookListener implements BasicResponseListener,CommunicationStatusListener{

        @Override
        public void onComplete() {
            sessionManager.completeLogin(registerUser,LoginScreen.this);
        }

        @Override
        public void onOpenConnection() {

        }

        @Override
        public void onCloseConnection() {
            hideProgressDialog();
        }

        @Override
        public void onConnectionError(String errorMessage) {
            LoginScreen.this.onLoginFail(registerUser,StatusCode.UNKNOWN);
        }

        @Override
        public void onError(TrapsterError error) {
            LoginScreen.this.onLoginFail(registerUser,StatusCode.PASS_CONFIRM_ERROR);
        }
    }



    public void sendLoginFacebook(User user) {
        showProgressDialog();

        this.registerUser = user;

        sessionManager.loginFacebook(user, new SessionListener() {
            @Override
            public void onLoginSuccess(User user, String optionalMessage) {

                LoginScreen.this.onLoginSuccess(user, optionalMessage);
            }

            @Override
            public void onLoginFail(User user, StatusCode code) {

                switch (code) {
                    // No Account
                    case INVALID_USERNAME:
                        // CheckUsername First
                        checkEmailAvailable(user.getProfile().getEmail());

                        //show(SCREEN_CHOOSENAME);
                        break;
                    default:
                        if (progressDialog != null)
                            progressDialog.dismiss();
                        break;
                }

            }

            @Override
            public void onError(TrapsterError error) {
                if (progressDialog != null)
                    progressDialog.dismiss();
                Log.v(LOGNAME, "LOGIN ERROR." + error.getDetails());
            }
        });
    }

    private void checkEmailAvailable(String email){
        EmailTakenListener emailTakenListener = new EmailTakenListener();
        api.isEmailTaken(email, emailTakenListener, emailTakenListener);
    }

    private class EmailTakenListener implements EmailAvailableListener,CommunicationStatusListener {

        @Override
        public void onOpenConnection() {
            com.trapster.android.util.Log.v(LOGNAME, "Open connection");
        }

        @Override
        public void onCloseConnection() {
            com.trapster.android.util.Log.v(LOGNAME, "Closed connection");
            if (progressDialog != null)
                progressDialog.dismiss();
        }

        @Override
        public void onConnectionError(String errorMessage) {
            com.trapster.android.util.Log.e(LOGNAME, "Connection Error:" + errorMessage);
            // Not sure if this is the way to handle this
        }


        @Override
        public void onEmailAvailable(String email, String displayname, long id, boolean available) {
            com.trapster.android.util.Log.v(LOGNAME, "Email available:" + available);
            if(available){
                show(SCREEN_CHOOSENAME);
            }else{
                registerUser.setUserName(displayname);

                show(SCREEN_MERGE);
            }

        }
    }





    @Override
    protected void onPause() {
        super.onPause();
        //
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
        if (confDialog != null) {
            confDialog.dismiss();
            confDialog = null;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //
        sessionManager.removeSessionListener(this);
    }


    @Override
    public void onLoginSuccess(User user, String message) {


        if (confDialog != null)
            sessionManager.setFirstRegister(true);
        else
            sessionManager.setFirstLogin(true);
        //
        if (progressDialog != null)
            progressDialog.dismiss();

        updatedRouteLinkManager.getSpeedLimitFromServer();

        sendFlurryLoginSuccessEvent();

        if (isConfDialogDisplayed)
            showToastMessage(getString(R.string.login_account_successfully_created));

        isConfDialogDisplayed = false;

                /*Class clazz = (Class)getIntent().getSerializableExtra(CLASS_EXTRA);
                if (clazz != null)
                {
                    startActivity(new Intent(LoginScreen.this, clazz));
                    finish();
                }
                else
                    returnToMap();*/

        finish();


    }

    @Override
    public void onLoginFail(final User user, StatusCode code) {
        if (progressDialog != null)
            progressDialog.dismiss();

        switch (code) {
            case REQUIRES_CONFIRMATION_CODE:
                runOnUiThread(new Runnable() {
                    public void run() {
                        showConfirmationCodeDialog(user);
                    }
                });

                if (sessionManager.isFirstRegister()) {
                    sendFlurrySignupSuccessEvent();
                    sessionManager.setFirstRegister(false);
                }
                break;

            default:
                runOnUiThread(new Runnable() {
                    public void run() {
                        PopupMessage.makeText(LoginScreen.this, getString(R.string.invalid_credentials_error), PopupMessage.STYLE_ALERT).show();
                        // standardLoginFragment.showHighlightedEditText();
                    }
                });
        }


    }

    @Override
    public void onError(TrapsterError error) {
        if (progressDialog != null)
            progressDialog.dismiss();
        runOnUiThread(new Runnable() {
            public void run() {
                PopupMessage.makeText(LoginScreen.this, getString(R.string.login_unknown_error), PopupMessage.STYLE_ALERT).show();
                //  showErrorMessage(R.id.textLoginError,);
                // standardLoginFragment.showHighlightedEditText();
            }
        });

    }


    public void sendForgotPassword(String email) {
        sessionManager.forgotPassword(email,null,null);

    }


    void showConfirmationCodeDialog(final User user) {
        isConfDialogDisplayed = true;
        if (progressDialog != null)
            progressDialog.dismiss();

        confDialog = ConfirmationCodeDialog.show(this, user, new ConfirmationListener() {
            public void onLogin(User user, String code) {
                sendConfirmationCode(user, code);

                if (confDialog != null)
                    confDialog.dismiss();
            }

            public void onResend(User user) {
                sendResend(user);
            }
        });
        confDialog.show();
    }

    public void showProgressDialog() {
        // Display progress dialog
        progressDialog = ProgressDialog
                .show(this, getString(R.string.please_wait), getString(R.string.logging_in), true);
        progressDialog.setCancelable(true);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
            }
        });

    }

    public void hideProgressDialog(){
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }

    public ConfirmationCodeDialog getConfirmationDialog() {
        return confDialog;
    }

    @Override
    protected String getClassName() {
        return CLASS_NAME;
    }


    public void startRegister(User user) {
        Intent intent = new Intent(this, CreateNewUserScreen.class);
        startActivity(intent);
    }

    /*
    From AccountAuthenticatorActivity Class
    */
    public final void setAccountAuthenticatorResult(Bundle result) {
        mResultBundle = result;
    }

    /**
     * Sends the result or a Constants.ERROR_CODE_CANCELED error if a result isn't present.
     */
    @Override
    public void finish() {
        if (mAccountAuthenticatorResponse != null) {
            // send the result bundle back if set, otherwise send an error.
            if (mResultBundle != null) {
                mAccountAuthenticatorResponse.onResult(mResultBundle);
            } else {
                mAccountAuthenticatorResponse.onError(AccountManager.ERROR_CODE_CANCELED,
                        "canceled");
            }
            mAccountAuthenticatorResponse = null;
        }
        super.finish();
    }


    private void sendFlurryLoginSuccessEvent() {
        FlurryEvent event = FlurryManager.createEvent(FLURRY_EVENT_NAME.LOGGED_IN);

        event.send();
    }

    private void sendFlurrySignupSuccessEvent() {
        FlurryEvent event = FlurryManager.createEvent(FLURRY_EVENT_NAME.SIGNUP_SUCCESSFUL);
        event.send();
    }

    /*@Override
    public void onBackPressed() {


        if (backstack.size() > 1) {
            backstack.pop(); // remove the current screen
            show(backstack.pop()); // show the back screen
        } else
            super.onBackPressed();
    }*/

    public User getRegisterUser() {
        return registerUser;
    }




}
