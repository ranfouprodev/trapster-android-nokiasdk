package com.trapster.android.activity.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.inject.Inject;
import com.trapster.android.R;
import com.trapster.android.activity.UserFeedbackExplanationScreen;
import com.trapster.android.manager.SessionManager;
import com.trapster.android.model.UserStatistics;
import com.trapster.android.model.dao.UpdatedRouteLinkDAO;
import roboguice.inject.InjectView;

public class StatsFragmentFilled extends AbstractFragment
{
    private static final double TIME_SAVED_FACTOR = 6.0;
    @Inject SessionManager sessionManager;
    @Inject UpdatedRouteLinkDAO routeLinkDAO;

    @InjectView(R.id.statsSaferPeople) TextView statsSaferPeople;
    @InjectView(R.id.statsUnwrittenTickets) TextView statsUnwrittenTickets;
    @InjectView(R.id.statsTimeSaved) TextView statsTimeSaved;

    @InjectView(R.id.statstrapReports) TextView statstrapReports;
    @InjectView(R.id.statsSpeedReports) TextView statsSpeedReports;
    @InjectView(R.id.statsTrapVotes) TextView statsTrapVotes;

    @InjectView(R.id.statsExplanationIcon) ImageView statsExplanationIcon;

    private final Handler handler = new Handler();
    private final Runnable updateStatsRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            if (sessionManager.isLoggedIn())
            {
                populateView();
                handler.postDelayed(this, 1000);
            }

        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_stats_filled, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        statsExplanationIcon.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                handler.removeCallbacks(null);
                startActivity(new Intent(getParentActivity(), UserFeedbackExplanationScreen.class));
            }
        });
        populateView();

        handler.postDelayed(updateStatsRunnable, 1000);

    }

    @Override
    public void onPause()
    {
        handler.removeCallbacks(null);
        super.onPause();
    }


    private void populateView()
    {
        UserStatistics userStatistics = sessionManager.getProfile().getUserStatistics();

        UserStatistics.TrapStatistics trapStatistics = userStatistics.getTrapStatistics();
        UserStatistics.MyVoteStatistics myVoteStatistics = userStatistics.getMyVoteStatistics();
        UserStatistics.VotesOnMyTrapsStatistics votesOnMyTrapsStatistics = userStatistics.getVotesOnMyTrapsStatistics();

        statsSaferPeople.setText(String.valueOf(trapStatistics.getSafeDriverCount() + myVoteStatistics.getSafeDriverCount() + votesOnMyTrapsStatistics.getSafeDriverCount()));
        statsUnwrittenTickets.setText(String.valueOf(trapStatistics.getNoTicketCount() + myVoteStatistics.getNoTicketCount() + votesOnMyTrapsStatistics.getNoTicketCount()));
        statsTimeSaved.setText(String.valueOf((int)((trapStatistics.getTimeSavedCount() + myVoteStatistics.getTimeSavedCount() + votesOnMyTrapsStatistics.getTimeSavedCount()) * TIME_SAVED_FACTOR)));

        statstrapReports.setText(String.valueOf(userStatistics.getTrapStatistics().getTrapReportCount()));
        statsTrapVotes.setText(String.valueOf(userStatistics.getMyVoteStatistics().getNumVotes()));

        statsSpeedReports.setText(String.valueOf(routeLinkDAO.getSize()));
    }
}
