package com.trapster.android.activity.settings.fragment;

import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import com.trapster.android.R;
import roboguice.inject.InjectView;

public class RadioButtonSettingFragment extends IconSettingFragment
{
    @InjectView(R.id.settingsRadio)
    CheckBox radioButton;

    private CompoundButton.OnCheckedChangeListener listener;
    private boolean isChecked = false;

    @Override
    public int getLayoutResource()
    {
        return R.layout.fragment_settings_radio;
    }
    @Override
    public void init()
    {
        super.init();

        View.OnClickListener onClickListener = (new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                boolean newState;
                // The order of the onClick events isn't the same for the parent container and the checkbox
                // The state of the box will change in one BEFORE this event, but not in the other
                // It's a nightmare
                if (view.equals(radioButton))
                    newState = !radioButton.isChecked();
                else
                    newState = radioButton.isChecked();

                radioButton.setChecked(newState);
                listener.onCheckedChanged(radioButton, !newState);

            }
        });
        radioButton.setOnClickListener(onClickListener);
        super.setOnClickListener(onClickListener);
        radioButton.setChecked(isChecked);
    }

    public void setCheckBoxListener(CompoundButton.OnCheckedChangeListener listener)
    {
        if (radioButton != null)
            radioButton.setOnCheckedChangeListener(listener);
        this.listener = listener;
    }

    public void setChecked(boolean isChecked)
    {
        if (radioButton != null)
            radioButton.setChecked(isChecked);
        this.isChecked = isChecked;

    }
}
