package com.trapster.android.activity.fragment.component;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.google.inject.Inject;
import com.trapster.android.BuildFeatures;
import com.trapster.android.manager.BitmapCacheManager;
import com.trapster.android.model.ImageResource;
import com.trapster.android.model.ImageSpec;
import com.trapster.android.model.TrapType;
import com.trapster.android.model.dao.ImageResourceDAO;

import roboguice.RoboGuice;

/**
 * 
 * [Nov 29, 2012 John] Moved the bitmap create and lookup from the onDraw function for performance 
 * 
 * @author John
 *
 */
public class ReportTrapIconImageView extends ImageView
{
	
	@Inject BitmapCacheManager bitmapCacheManager;
	
	@Inject
	ImageResourceDAO imageResourceDao;
	
	private TrapType trapType;

	private int pinX;

	private int pinY;

	private Paint paint;

	private Bitmap scaledIcon;

	private int scaledIconSize;


    private static final double ICON_SIZE_SCREEN_PERCENTAGE = 0.1;
    private ImageSpec imageSpec;

    public ReportTrapIconImageView(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		init(); 
	}


	public ReportTrapIconImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(); 
	}



	public ReportTrapIconImageView(Context context) {
		super(context);
		init(); 
	}


	private void init(){
		RoboGuice.injectMembers(getContext(), this);
	}
	

	public void setTrapType(TrapType trapType) {
		this.trapType = trapType;
		initializeDraw(); 
	}


	@SuppressLint("NewApi")
	private void initializeDraw() {
		paint = new Paint();
		
		String trapTypeIcon = trapType.getImageFilename();
		
		ImageResource imageResource = imageResourceDao.findByKey(trapTypeIcon);
		
		if (imageResource != null)
        {
          
            ImageSpec spec = imageResource.getSpec();
            
            /*DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            int portraitPixels;
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
                portraitPixels = displayMetrics.heightPixels;
            else
                portraitPixels = displayMetrics.widthPixels;
            
            scaledIconSize = (int) (portraitPixels * ICON_SIZE_SCREEN_PERCENTAGE);

            
            float scalingFactorX = (float) scaledIconSize / (float) spec.getWidth();
            float scalingFactorY = (float) scaledIconSize / (float) spec.getHeight();

            // Scaled PinX values
            pinX = (int)(scalingFactorX * spec.getPinx());

            pinY = (int)(scalingFactorY * spec.getPiny());

            
            // Adjusted for the center of the icon
            pinX -= scaledIconSize/2; 
            pinY -= scaledIconSize/2;*/
            
            
           // Log.d("ReportTrapIcon", "Scaled:"+scaledIconSize+" pinx:"+pinX+" piny:"+pinY+" spec:"+spec.getWidth()+","+spec.getHeight());
            
         /*   if (scaledIconSize > 0)
            {
                ViewGroup.LayoutParams params = this.getLayoutParams();
                params.width = scaledIconSize;
                params.height = scaledIconSize;
                setLayoutParams(params);
            }*/


            imageSpec = imageResource.getSpec();

            
            Drawable drawable = bitmapCacheManager.getDrawableFromSvg(imageResource);

            if (drawable == null)
            {
                Bitmap icon = bitmapCacheManager.getBitmapFromCache(trapTypeIcon);
                setImageBitmap(icon);
            }
            else
            {
            	
            	//drawable.setBounds(0,0,10,10);
            	setImageDrawable(drawable);
                
                if (BuildFeatures.SUPPORTS_HONEYCOMB)
                    setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            }

            setPadding(imageSpec.getPinx(),-imageSpec.getPiny(),0,0);
			
		}
	}


	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		setMeasuredDimension(imageSpec.getWidth()+(imageSpec.getPinx()*2),imageSpec.getHeight()+(imageSpec.getPiny()*2));
	}

	
	
	@Override
	protected void onDraw(Canvas canvas) {
		int save = canvas.save();
		canvas.translate(pinX, -pinY);
		super.onDraw(canvas);
		canvas.restore();
	}


	
	
}
