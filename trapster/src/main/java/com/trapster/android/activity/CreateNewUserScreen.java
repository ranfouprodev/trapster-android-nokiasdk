package com.trapster.android.activity;

import android.os.Bundle;

import com.trapster.android.R;

import roboguice.inject.ContentView;

@ContentView(R.layout.screen_generatename)
public class CreateNewUserScreen extends Screen {

    private final static String LOGNAME = "CreateNewUserScreen";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        enableActionBar();
    }

    @Override
    protected String getClassName()
    {
        return LOGNAME;
    }

}
