package com.trapster.android.activity.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.inject.Inject;
import com.trapster.android.R;
import com.trapster.android.activity.LoginScreen;
import com.trapster.android.activity.MapScreen;
import com.trapster.android.activity.SelectTrapTypeScreen;
import com.trapster.android.activity.settings.DashMenuSettingsScreen;
import com.trapster.android.manager.SessionManager;

import roboguice.inject.InjectFragment;
import roboguice.inject.InjectView;

public class DashboardFragment extends AbstractFragment {

    static final int REQUEST_CODE_MAP_ACTIVITY= 0;

    static final int RESPONSE_CODE_QUIT_APPLICATION= 1001;

	private static final String LOGNAME = "DashboardFragment";

	@InjectView(R.id.buttonActionMenu)ImageView buttonActionMenu;
	@InjectView(R.id.buttonActionTraps)ImageView buttonActionTraps;

    @InjectFragment(R.id.fragmentSpeedometer) SpeedometerFragment speedometerFragment;

	@Inject SessionManager sessionManager; 
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_dashboard, null);
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		//Report Traps Menu
		buttonActionTraps.setOnClickListener(new ReportTrapsMenuClickListener());
		
		// Action Menu
		buttonActionMenu.setOnClickListener(new OpenMenuClickListener());

		
	}

    @Override
    public void onResume()
    {
        if (!sessionManager.isLoggedIn())
            buttonActionTraps.setImageDrawable(getResources().getDrawable(R.drawable.button_login_key));
        else
            buttonActionTraps.setImageDrawable(getResources().getDrawable(R.drawable.button_menu_report));

        super.onResume();
    }


	class OpenMenuClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
            Intent intent = new Intent(getParentActivity(), DashMenuSettingsScreen.class);
            startActivityForResult(intent, MapScreen.RESPONSE_CODE_QUIT_APPLICATION);
		}

	}

	
	class ReportTrapsMenuClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			
			if(!sessionManager.isLoggedIn())
            {
                Intent i = new Intent(getParentActivity(), LoginScreen.class);
                //i.putExtra(LoginScreen.CLASS_EXTRA, SelectTrapTypeScreen.class);
                getParentActivity().startActivity(i);
            } else
            {
                AbstractMapFragment.MapConfig mapConfig = ((MapScreen) getParentActivity()).getCurrentMapConfig();
                Intent intent = new Intent(getParentActivity(), SelectTrapTypeScreen.class);
                if (mapConfig != null)
                    intent.putExtra(SelectTrapTypeScreen.EXTRA_MAP_CONFIG, mapConfig);
                getParentActivity().startActivity(intent);

            }

        }

	}
	
	

}
