package com.trapster.android.activity.fragment;

import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.inject.Inject;
import com.trapster.android.R;
import roboguice.inject.InjectFragment;

public class MapLoadingFragment extends AbstractFragment
{
    @InjectFragment (R.id.gpsErrorFragment) GPSLoadErrorFragment gpsErrorFragment;
    @Inject LocationManager locationManager;

    private final Handler handler = new Handler();
    private final Runnable showGPSErrorRunnable = new ShowGPSErrorRunnable();
    private final static long TIME_TO_SHOW_GPS_ERROR = 30 * 1000; // 30 seconds

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map_loading, null);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String provider = LocationManager.GPS_PROVIDER;

        gpsErrorFragment.hide();
        gpsErrorFragment.setRetryRunnable(new Runnable()
        {
            @Override
            public void run()
            {
                handler.postDelayed(this, TIME_TO_SHOW_GPS_ERROR);
            }
        });
        if (provider != null && locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            handler.postDelayed(showGPSErrorRunnable, TIME_TO_SHOW_GPS_ERROR);
    }

    @Override
    public void onPause()
    {
        handler.removeCallbacks(showGPSErrorRunnable);
        super.onPause();
    }

    @Override
    public void show()
    {
        super.show();
    }

    @Override
    public void hide()
    {
        handler.removeCallbacks(showGPSErrorRunnable);
        super.hide();
    }

    @Override
    public void onBackPressed()
    {
        handler.removeCallbacks(showGPSErrorRunnable);
        super.onBackPressed();
    }

    private class ShowGPSErrorRunnable implements Runnable
    {
        public void run()
        {
            gpsErrorFragment.show();
        }
    }


}
