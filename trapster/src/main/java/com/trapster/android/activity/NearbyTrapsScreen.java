package com.trapster.android.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabHost.TabContentFactory;

import com.google.inject.Inject;
import com.here.android.common.GeoPosition;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.activity.fragment.NearbyTrapListFragment;
import com.trapster.android.activity.fragment.NewestTrapListFragment;
import com.trapster.android.activity.fragment.component.CustomTabView;
import com.trapster.android.manager.PositionManager;
import com.trapster.android.manager.SessionManager;
import com.trapster.android.util.GeographicUtils;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

public class NearbyTrapsScreen extends Screen implements
		TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener {

	
	@Inject PositionManager pm;
    @Inject SessionManager sessionManager;
	
	public static final String EXTRA_DATEFROM = "EXTRA_DATEFROM";
	public static final String EXTRA_BBOX = "EXTRA_BBOX";
	private static final String LOGNAME = "NewNearbyTabScreen";

	private TabHost tabHost;
	private ViewPager viewPager;
	private HashMap<String, TabInfo> mapTabInfo = new HashMap<String, TabInfo>();
	private TabFragmentsAdapter tabFragmentsAdapter;
	private Date from;
	private Geometry area;
	private CustomTabView nearbyTabView;
	private CustomTabView newTabView;

    private final static String CLASS_NAME = "NearbyTraps";

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        disableActionBar();

		// Inflate the layout
		setContentView(R.layout.screen_newnearby);

		Intent intent = getIntent();
		if (intent != null && intent.hasExtra(EXTRA_DATEFROM)) {
			if (intent.getSerializableExtra(EXTRA_DATEFROM) instanceof Date) {
				from = (Date) intent.getSerializableExtra(EXTRA_DATEFROM);
			}
		}

		if (intent != null && intent.hasExtra(EXTRA_BBOX)) {
			if (intent.getSerializableExtra(EXTRA_BBOX) instanceof Envelope) {
				area = (Geometry) intent.getSerializableExtra(EXTRA_BBOX);
			}
		}

		
		if(area == null){
			GeoPosition position = pm.getLastLocation(); 
			area = GeographicUtils.createBoundingBox(position.getCoordinate().getLatitude(), position.getCoordinate().getLongitude(), Defaults.NEARBY_TRAP_RADIUS);
		}
		
		// Intialise ViewPager
        initialiseTabs(savedInstanceState);
		
		if (savedInstanceState != null) {
			tabHost.setCurrentTabByTag(savedInstanceState.getString("tab")); 
		}


	}
	
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
        intialiseViews(null);
	}


	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}
	
	public void setNewTrapsCount(int count){
		newTabView.setCount(count);
	}

	protected void onSaveInstanceState(Bundle outState) {
		outState.putString("tab", tabHost.getCurrentTabTag()); // save the tab
																// selected
		super.onSaveInstanceState(outState);
	}

    private void initialiseTabs(Bundle savedInstanceState){
        tabHost = (TabHost)findViewById(android.R.id.tabhost);
        tabHost.setup();
        nearbyTabView = new CustomTabView(this, 0,getResources().getString(R.string.nearby_traps_tab));
        addTabHost(NearbyTrapListFragment.class,"Tab1",nearbyTabView,savedInstanceState );

        newTabView = new CustomTabView(this, 0, getResources().getString(R.string.nearby_traps_new_tab));
        addTabHost(NewestTrapListFragment.class,"Tab2",newTabView,savedInstanceState );
        tabHost.setOnTabChangedListener(this);
    }


	private void intialiseViews(Bundle savedInstanceState) {
	    	
			List<Fragment> fragments = new Vector<Fragment>();
			


	    	fragments.add(NearbyTrapListFragment.newInstance(area));

	    	
			fragments.add(NewestTrapListFragment.newInstance(from, area));   	

			
			tabFragmentsAdapter  = new TabFragmentsAdapter(super.getSupportFragmentManager(), fragments);
			viewPager = (ViewPager)super.findViewById(R.id.viewpager);
			viewPager.setAdapter(this.tabFragmentsAdapter);
			viewPager.setOnPageChangeListener(this);
			

	    }

	/**
	 * Initialise the Tab Host
	 */
	private void addTabHost(Class fragmentClass, String tabName,View view, Bundle args) {
		TabInfo tabInfo = null;
		NearbyTrapsScreen.addTab(this, this.tabHost,
				this.tabHost.newTabSpec(tabName).setIndicator(view),
				(tabInfo = new TabInfo(tabName, fragmentClass, args)));
		
		this.mapTabInfo.put(tabInfo.tag, tabInfo);

	}
	


	/**
	 * Add Tab content to the Tabhost
	 */
	private static void addTab(NearbyTrapsScreen activity, TabHost tabHost,
			TabHost.TabSpec tabSpec, TabInfo tabInfo) {
		// Attach a Tab view factory to the spec
		tabSpec.setContent(activity.new TabFactory(activity));
		
		tabHost.addTab(tabSpec);
		
		
	}

	
	
	
	/**
	 * (non-Javadoc)
	 * 
	 * @see android.widget.TabHost.OnTabChangeListener#onTabChanged(java.lang.String)
	 */
	public void onTabChanged(String tag) {
		// TabInfo newTab = this.mapTabInfo.get(tag);
		int pos = this.tabHost.getCurrentTab();
		this.viewPager.setCurrentItem(pos);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.support.v4.view.ViewPager.OnPageChangeListener#onPageScrolled
	 * (int, float, int)
	 */
	@Override
	public void onPageScrolled(int position, float positionOffset,
			int positionOffsetPixels) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.support.v4.view.ViewPager.OnPageChangeListener#onPageSelected
	 * (int)
	 */
	@Override
	public void onPageSelected(int position) {
		tabHost.setCurrentTab(position);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.view.ViewPager.OnPageChangeListener#
	 * onPageScrollStateChanged(int)
	 */
	@Override
	public void onPageScrollStateChanged(int state) {
	}

	private class TabFragmentsAdapter extends FragmentPagerAdapter {

		private List<Fragment> fragments;

		/**
		 * @param fm
		 * @param fragments
		 */
		public TabFragmentsAdapter(FragmentManager fm, List<Fragment> fragments) {
			super(fm);
			this.fragments = fragments;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.support.v4.app.FragmentPagerAdapter#getItem(int)
		 */
		@Override
		public Fragment getItem(int position) {
			return this.fragments.get(position);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.support.v4.view.PagerAdapter#getCount()
		 */
		@Override
		public int getCount() {
			return this.fragments.size();
		}
	}

	private class TabInfo {
		private String tag;
		private Class<?> clss;
		private Bundle args;
		private Fragment fragment;

		TabInfo(String tag, Class<?> clazz, Bundle args) {
			this.tag = tag;
			this.clss = clazz;
			this.args = args;
		}

	}

	/**
	 * A simple factory that returns dummy views to the Tabhost
	 */
	class TabFactory implements TabContentFactory {

		private final Context mContext;

		/**
		 * @param context
		 */
		public TabFactory(Context context) {
			mContext = context;
		}

		/**
		 * (non-Javadoc)
		 * 
		 * @see android.widget.TabHost.TabContentFactory#createTabContent(java.lang.String)
		 */
		public View createTabContent(String tag) {
			View v = new View(mContext);
			v.setMinimumWidth(0);
			v.setMinimumHeight(0);
			return v;
		}

	}

    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }


}
