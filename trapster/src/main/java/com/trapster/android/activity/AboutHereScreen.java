package com.trapster.android.activity;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import com.trapster.android.R;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;


@ContentView(R.layout.screen_about)
public class AboutHereScreen extends Screen {

    private final static String CLASS_NAME = "AboutHereScreen";
    String aboutFileLocationFull = "file:///android_asset/nokia-sdk-notices.html";
    @InjectView(R.id.aboutWebView) WebView webView;
    @InjectView(R.id.aboutLoadingText) TextView loadingText;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set title
        setTitle(R.string.about_here_title);

        webView.loadUrl(aboutFileLocationFull);
        webView.setWebViewClient(new WebViewClient()
        {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url)
            {
                loadingText.setVisibility(View.GONE);
                webView.setVisibility(View.VISIBLE);
            }
        });
        //Manages settings state for a WebView. When a WebView is first created, it obtains a set of default settings.
        WebSettings webSettings = webView.getSettings();
        //WebView to enable JavaScript execution
        webSettings.setJavaScriptEnabled(true);


    }


    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }
}
