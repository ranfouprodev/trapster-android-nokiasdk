package com.trapster.android.activity.settings;

import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.activity.settings.fragment.MultiModalSlideBarFragment;

public class DisplayOptionsSettingsScreen extends AbstractSettingsScreen
{
    private static final String CLASS_NAME = "Display Options Screen";

    @Override
    protected void setupFragments()
    {
        setupDayNightSlider();
        setupUnitsSlider();
        setupViewSlider();
        //setupFollowSlider();
        //setupZoomSlider();
        setupScreenOrientationSlider();
    }


    private void setupDayNightSlider()
    {
        MultiModalSlideBarFragment slideBarFragment = (MultiModalSlideBarFragment) createSettingsFragment(FRAGMENT_TYPE.MULTI_MODAL_SLIDE_BAR);

        slideBarFragment.setDescription(R.string.settings_menu_map_mode);
        slideBarFragment.setOptionText(R.string.setting_mapmode_auto, R.string.setting_mapmode_day, R.string.setting_mapmode_night);
        slideBarFragment.setSelectionChangedListener(new MultiModalSlideBarFragment.OnSliderSelectionChangedListener()
        {
            @Override
            public void onSliderSelectionChanged(MultiModalSlideBarFragment.SLIDER_SELECTION selection)
            {
                switch (selection)
                {
                    case LEFT:
                    {
                        editor.putInt(Defaults.PREFERENCE_MAP_MODE, Defaults.PREFERENCE_MAP_MODE_AUTO);
                        break;
                    }
                    case RIGHT:
                    {
                        editor.putInt(Defaults.PREFERENCE_MAP_MODE, Defaults.PREFERENCE_MAP_MODE_DAY);
                        break;
                    }
                    case CENTER:
                    {
                        editor.putInt(Defaults.PREFERENCE_MAP_MODE, Defaults.PREFERENCE_MAP_MODE_NIGHT);
                        break;
                    }
                }
            }
        });

        int selectedMapMode = sharedPreferences.getInt(Defaults.PREFERENCE_MAP_MODE, Defaults.PREFERENCE_MAP_MODE_AUTO);
        switch (selectedMapMode)
        {
            case Defaults.PREFERENCE_MAP_MODE_AUTO:
            {
                slideBarFragment.setStartSelection(MultiModalSlideBarFragment.SLIDER_SELECTION.LEFT);
                break;
            }
            case Defaults.PREFERENCE_MAP_MODE_DAY:
            {
                slideBarFragment.setStartSelection(MultiModalSlideBarFragment.SLIDER_SELECTION.RIGHT);
                break;
            }
            case Defaults.PREFERENCE_MAP_MODE_NIGHT:
            {
                slideBarFragment.setStartSelection(MultiModalSlideBarFragment.SLIDER_SELECTION.CENTER);
                break;
            }
        }
    }

    private void setupUnitsSlider()
    {
        MultiModalSlideBarFragment slideBarFragment = (MultiModalSlideBarFragment) createSettingsFragment(FRAGMENT_TYPE.MULTI_MODAL_SLIDE_BAR);

        slideBarFragment.setDescription(R.string.setting_mapoptions_units_subtitle);
        slideBarFragment.setOptionText(R.string.setting_mapoptions_miles, R.string.setting_mapoptions_km);
        slideBarFragment.setSelectionChangedListener(new MultiModalSlideBarFragment.OnSliderSelectionChangedListener()
        {
            @Override
            public void onSliderSelectionChanged(MultiModalSlideBarFragment.SLIDER_SELECTION selection)
            {
                switch (selection)
                {
                    case LEFT:
                    {
                        editor.putBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_MILES, true);
                        break;
                    }
                    case RIGHT:
                    {
                        editor.putBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_MILES, false);
                        break;
                    }
                }
            }
        });

        if(sharedPreferences.getBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_MILES, true))
            slideBarFragment.setStartSelection(MultiModalSlideBarFragment.SLIDER_SELECTION.LEFT);
        else
            slideBarFragment.setStartSelection(MultiModalSlideBarFragment.SLIDER_SELECTION.RIGHT);
    }

    private void setupViewSlider()
    {
        MultiModalSlideBarFragment slideBarFragment = (MultiModalSlideBarFragment) createSettingsFragment(FRAGMENT_TYPE.MULTI_MODAL_SLIDE_BAR);

        slideBarFragment.setDescription(R.string.setting_mapoptions_mapdisplay_subtitle);
        slideBarFragment.setOptionText(R.string.setting_mapoptions_3d, R.string.setting_mapoptions_2d);
        slideBarFragment.setSelectionChangedListener(new MultiModalSlideBarFragment.OnSliderSelectionChangedListener()
        {
            @Override
            public void onSliderSelectionChanged(MultiModalSlideBarFragment.SLIDER_SELECTION selection)
            {
                switch (selection)
                {
                    case LEFT:
                    {
                        editor.putBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_3D, true);
                        break;
                    }
                    case RIGHT:
                    {
                        editor.putBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_3D, false);
                        break;
                    }
                }
            }
        });

        if(sharedPreferences.getBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_3D, true))
            slideBarFragment.setStartSelection(MultiModalSlideBarFragment.SLIDER_SELECTION.LEFT);
        else
            slideBarFragment.setStartSelection(MultiModalSlideBarFragment.SLIDER_SELECTION.RIGHT);
    }

    private void setupFollowSlider()
    {
        MultiModalSlideBarFragment slideBarFragment = (MultiModalSlideBarFragment) createSettingsFragment(FRAGMENT_TYPE.MULTI_MODAL_SLIDE_BAR);

        slideBarFragment.setDescription(R.string.setting_mapoptions_mapfollow);
        slideBarFragment.setOptionText(R.string.settings_mapoptions_follow_mode_rotate, R.string.settings_mapoptions_follow_mode_north);
        slideBarFragment.setSelectionChangedListener(new MultiModalSlideBarFragment.OnSliderSelectionChangedListener()
        {
            @Override
            public void onSliderSelectionChanged(MultiModalSlideBarFragment.SLIDER_SELECTION selection)
            {
                switch (selection)
                {
                    case LEFT:
                    {
                        editor.putBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_AUTO_ZOOM, true);
                        break;
                    }
                    case RIGHT:
                    {
                        editor.putBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_AUTO_ZOOM, false);
                        break;
                    }
                }
            }
        });

        if(sharedPreferences.getBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_AUTO_ZOOM, true))
            slideBarFragment.setStartSelection(MultiModalSlideBarFragment.SLIDER_SELECTION.LEFT);
        else
            slideBarFragment.setStartSelection(MultiModalSlideBarFragment.SLIDER_SELECTION.RIGHT);
    }

    private void setupZoomSlider()
    {
        MultiModalSlideBarFragment slideBarFragment = (MultiModalSlideBarFragment) createSettingsFragment(FRAGMENT_TYPE.MULTI_MODAL_SLIDE_BAR);

        slideBarFragment.setDescription(R.string.setting_mapoptions_autozoom);
        slideBarFragment.setOptionText(R.string.widget_status_off, R.string.widget_status_on);
        slideBarFragment.setSelectionChangedListener(new MultiModalSlideBarFragment.OnSliderSelectionChangedListener()
        {
            @Override
            public void onSliderSelectionChanged(MultiModalSlideBarFragment.SLIDER_SELECTION selection)
            {
                switch (selection)
                {
                    case LEFT:
                    {
                        editor.putBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_FOLLOW, true);
                        break;
                    }
                    case RIGHT:
                    {
                        editor.putBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_FOLLOW, false);
                        break;
                    }
                }
            }
        });

        if(sharedPreferences.getBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_FOLLOW, true))
            slideBarFragment.setStartSelection(MultiModalSlideBarFragment.SLIDER_SELECTION.LEFT);
        else
            slideBarFragment.setStartSelection(MultiModalSlideBarFragment.SLIDER_SELECTION.RIGHT);
    }

    public void setupScreenOrientationSlider()
    {
        MultiModalSlideBarFragment slideBarFragment = (MultiModalSlideBarFragment) createSettingsFragment(FRAGMENT_TYPE.MULTI_MODAL_SLIDE_BAR);

        slideBarFragment.setDescription(R.string.setting_orientation_title);
        slideBarFragment.setOptionText(R.string.setting_orientation_auto, R.string.setting_orientation_portrait, R.string.setting_orientation_landscape);
        slideBarFragment.setSelectionChangedListener(new MultiModalSlideBarFragment.OnSliderSelectionChangedListener()
        {
            @Override
            public void onSliderSelectionChanged(MultiModalSlideBarFragment.SLIDER_SELECTION selection)
            {
                switch (selection)
                {
                    case LEFT:
                    {
                        editor.putInt(Defaults.PREFERENCE_ORIENTATION, Defaults.SCREEN_ORIENTATION_SENSOR);
                        break;
                    }
                    case RIGHT:
                    {
                        editor.putInt(Defaults.PREFERENCE_ORIENTATION, Defaults.SCREEN_ORIENTATION_PORTRAIT);
                        break;
                    }
                    case CENTER:
                    {
                        editor.putInt(Defaults.PREFERENCE_ORIENTATION, Defaults.SCREEN_ORIENTATION_LANDSCAPE);
                        break;
                    }
                }

                editor.commit();
                setScreenOrientation();


            }
        });


        int selectedOrientation = sharedPreferences.getInt(Defaults.PREFERENCE_ORIENTATION, Defaults.SETTING_DEFAULT_SCREEN_ORIENTATION);
        switch (selectedOrientation)
        {
            case Defaults.SCREEN_ORIENTATION_SENSOR:
            {
                slideBarFragment.setStartSelection(MultiModalSlideBarFragment.SLIDER_SELECTION.LEFT);
                break;
            }
            case Defaults.SCREEN_ORIENTATION_PORTRAIT:
            {
                slideBarFragment.setStartSelection(MultiModalSlideBarFragment.SLIDER_SELECTION.RIGHT);
                break;
            }
            case Defaults.SCREEN_ORIENTATION_LANDSCAPE:
            {
                slideBarFragment.setStartSelection(MultiModalSlideBarFragment.SLIDER_SELECTION.CENTER);
                break;
            }
        }
    }

    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }
}
