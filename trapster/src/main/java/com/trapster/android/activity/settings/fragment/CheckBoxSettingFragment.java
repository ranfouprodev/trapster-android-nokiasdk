package com.trapster.android.activity.settings.fragment;

import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import com.trapster.android.R;
import roboguice.inject.InjectView;

public class CheckBoxSettingFragment extends IconSettingFragment
{
    @InjectView(R.id.settingsCheckBox) CheckBox checkBox;

    private CompoundButton.OnCheckedChangeListener listener;
    private boolean isChecked;


    @Override
    public int getLayoutResource()
    {
        return R.layout.fragment_settings_checkbox;
    }
    @Override
    public void init()
    {
        super.init();

        checkBox.setChecked(isChecked);
        super.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                isChecked = !isChecked;
                checkBox.setChecked(isChecked);
                listener.onCheckedChanged(checkBox,isChecked);
            }
        });

        checkBox.setOnCheckedChangeListener(listener);

    }

    public void setCheckBoxListener(CompoundButton.OnCheckedChangeListener listener)
    {
        if (checkBox != null)
            checkBox.setOnCheckedChangeListener(listener);
        this.listener = listener;
    }

    public void setChecked(boolean isChecked)
    {
        if (checkBox != null)
        {
            checkBox.setChecked(isChecked);
            listener.onCheckedChanged(checkBox, isChecked);
        }

        this.isChecked = isChecked;
    }

    public boolean isChecked()
    {
        return isChecked;
    }
}
