package com.trapster.android.activity.fragment;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.PointF;
import android.os.Handler;
import android.util.SparseArray;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.here.android.common.GeoBoundingBox;
import com.here.android.common.GeoCoordinate;
import com.here.android.mapping.Map;
import com.trapster.android.activity.ClusterTrapScreen;
import com.trapster.android.activity.SingleTrapDetailsScreen;
import com.trapster.android.activity.fragment.util.AbstractMapMarker;
import com.trapster.android.activity.fragment.util.AbstractMapMarker.OnMapMarkerClick;
import com.trapster.android.activity.fragment.util.ClusteredTrapMapMarker;
import com.trapster.android.activity.fragment.util.TrapMapMarker;
import com.trapster.android.manager.MapController;
import com.trapster.android.manager.SoundManager;
import com.trapster.android.manager.SoundThemeManager;
import com.trapster.android.manager.TrapManager;
import com.trapster.android.model.Trap;
import com.trapster.android.model.dao.MapBoundedCursorLoader;
import com.trapster.android.model.dao.TrapDAO;
import com.trapster.android.model.provider.TrapProvider;
import com.trapster.android.service.TrapSyncService;
import com.trapster.android.util.GeographicUtils;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.index.quadtree.Quadtree;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Responsible for displaying the traps on the map. Traps are pull from the
 * database and populated with the traploader
 * 
 * Also performs clustering whenever a new trap is added to the map. Clusters
 * are recomputed at each zoom level by adjusting the Quadtree for that zoom
 * level
 * 
 * 
 * @author John
 * 
 */
@Singleton
public class TrapMapLayer extends AbstractMapLayer {

	private static final String LOGNAME = "TrapMapLayer";

	// Don't cluster if the zoom level is greater than this
	private static final int MINIMUM_CLUSTER_ZOOM = 20;

	// Don't show traps after this
	private static final int MAXIMUM_CLUSTER_ZOOM = 8;

    private static final long STARTUP_TRAP_LOAD_DELAY = 5000;

	@Inject
	Application application;

	@Inject
	TrapManager trapManager;

    @Inject
    SharedPreferences sharedPreferences;

	@Inject
	MapController mapController;

	@Inject
	TrapDAO trapDao;

    @Inject
    SoundManager soundManager;
    @Inject
    SoundThemeManager soundThemeManager;

	private MapBoundedCursorLoader cursorLoader;

	private SparseArray<TrapMapMarker> trapCache = new SparseArray<TrapMapMarker>();

	private Handler updateHandler = new Handler();

	private static final int CORE_POOL_SIZE = 1;
	private static final int MAXIMUM_POOL_SIZE = 1;
	private static final long KEEP_ALIVE = 10; // seconds
    private static final long TIME_BETWEEN_REDRAWS = 2000;
    private static final long CENTER_ON_ME_DRAW_DELAY = 15 * 1000;

    private long lastDrawTime;

	private LinkedBlockingQueue<Runnable> backgroundTaskQueue = new LinkedBlockingQueue<Runnable>();
	private ThreadPoolExecutor backgroundExecutor = new ThreadPoolExecutor(
			CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, KEEP_ALIVE, TimeUnit.SECONDS,
			backgroundTaskQueue);

	private Runnable mapUpdateTask = new Runnable() {

        @Override
        public void run()
        {


            //virtualRadarManager.checkZoom();

            // Reload the traps in the area
            if (getMap() != null)
            {

                GeoBoundingBox bbox = getMap().getBoundingBox();
                if (bbox.isValid())
                {
                    double radius = bbox.getBottomRight().distanceTo(bbox.getCenter());

                    double radiusInMiles = GeographicUtils
                            .kilometersToMiles(radius / 1000);

                    radiusInMiles = (radiusInMiles > TrapSyncService.MAX_QUERY_RADIUS) ? TrapSyncService.MAX_QUERY_RADIUS : radiusInMiles;

                    GeoCoordinate newCenter = getMap().getCenter();

                    trapManager.requestTrapUpdate(newCenter.getLatitude(),
                            newCenter.getLongitude(), radiusInMiles);
                }


            }
        }

    };


	// Cache of clusters at each zoom level
	private SparseArray<Quadtree> clusterLayerIndex = new SparseArray<Quadtree>();

	/*
	 * Based on 320dpi phone. Other phones will see different values but it
	 * shouldn't impact the clustering. It will cause some traps to overlap.
	 */
	private static final double[] meterPerPixelAtZoom = new double[] { 98304,
		49152, 24576, 12288, 6144, 3072, 1536, 768, 384, 192, 96, 48, 24,
		12, 6, 3, 1.5, 0.75, 0.37, 0.16, 0.08 };

	private int previousZoomLevel = 0;
    private long lastRedrawTime = 0;

	/*
	 * For handling redrawing in the background
	 */
	private Handler initialDrawHandler = new Handler();
	private Runnable redrawRunnable = new Runnable() {

		@Override
		public void run() {
/*            if (!isVisible())
                show();*/

			//new RedrawAsyncTask().execute();


		}

	};

	@Inject
	public TrapMapLayer() {
	}
	
	@Inject 
	public void init(){
		cursorLoader = new MapBoundedCursorLoader(null,
				application.getApplicationContext(), TrapProvider.CONTENT_URI,
				null, null, null, "_id desc", trapManager, sharedPreferences);
	}

	@Override
	public void onAttachMap(Map map) {

		cursorLoader.setMap(map);

		// Preload the traps
        updateHandler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                new BackgroundAsyncTask().execute();
            }
        }, STARTUP_TRAP_LOAD_DELAY);


	}
	
	@Override
	public void onDetachMap(Map map) {
		super.onDetachMap(map);

/*       	if (redrawHandler != null)
			redrawHandler.removeCallbacks(redrawRunnable);*/
	}

	private Quadtree getQuadtreeForZoom(int zoom) {
		int index = zoom;

		if (zoom >= MINIMUM_CLUSTER_ZOOM)
			index = MINIMUM_CLUSTER_ZOOM;
		else if (zoom <= MAXIMUM_CLUSTER_ZOOM)
			return new Quadtree(); // no clusters

		if (clusterLayerIndex.indexOfKey(index) < 0) {
			clusterLayerIndex.put(index, new Quadtree());
		}
		return clusterLayerIndex.get(index);

	}

	private void setQuadtreeForZoom(Quadtree quad, int zoom) {
		int index = zoom;

		if (zoom >= MINIMUM_CLUSTER_ZOOM)
			index = MINIMUM_CLUSTER_ZOOM;

		if (index >= MAXIMUM_CLUSTER_ZOOM)
			clusterLayerIndex.put(index, quad);

	}

	/*
	 * To avoid too many refresh calls and to allow the screen to draw a 16ms
	 * delay is added.
	 */
	private void scheduleRedraw() {

		/*
		 * Trigger clustering to start
		 */
    	/*if (redrawHandler != null)
			redrawHandler.removeCallbacks(redrawRunnable);

		redrawHandler.postDelayed(redrawRunnable, 150);*/

        long currentTime = System.currentTimeMillis();
        if (currentTime - lastRedrawTime >= TIME_BETWEEN_REDRAWS)
        {
            lastRedrawTime = currentTime;
            new BackgroundAsyncTask().execute();
        }


	}

	@Override
	public void hide() {
		super.hide();
		if (updateHandler != null && mapUpdateTask != null)
			updateHandler.removeCallbacks(mapUpdateTask);
	}

	/*
	 * Until .show() is called, the trap layer won't display. The mapView has to
	 * be configured first
	 * 
	 * (non-Javadoc)
	 * 
	 * @see com.trapster.android.activity.fragment.MapLayer#show()
	 */
	@Override
	public void show() {
		super.show();

        scheduleRedraw();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.trapster.android.activity.fragment.MapLayer#forceUpdate(double,
	 * double, double)
	 */
	@Override
	public void forceUpdate(double lat, double lon, double radius) {
		//trapManager.requestTrapUpdate(lat, lon, radius);

        if (updateHandler != null)
            updateHandler.removeCallbacks(mapUpdateTask);
		/*
		 * requests are sent after X ms of map inactivity. This should prevent
		 * the app from going crazy if a user rapidly pans the map. I'm not sure
		 * how this will react with follow-mode on. It should be tested
		 */
        updateHandler.postDelayed(mapUpdateTask, 2500);
	}

	@Override
	public void refresh() {
		super.refresh();
		scheduleRedraw();
	}

    public void requestRedraw()
    {
        if (System.currentTimeMillis() - lastDrawTime >= CENTER_ON_ME_DRAW_DELAY)
        {
            refresh();
        }
    }

	@Override
	public void onMapMoveEnd(GeoCoordinate newCenter) {
		if (updateHandler != null)
			updateHandler.removeCallbacks(mapUpdateTask);
		/*
		 * requests are sent after X ms of map inactivity. This should prevent
		 * the app from going crazy if a user rapidly pans the map. I'm not sure
		 * how this will react with follow-mode on. It should be tested
		 */
		updateHandler.postDelayed(mapUpdateTask, 500);
	}

	/*
	 * This is only required when we filter traps out on the top of the tilt
	 * portion of the map
	 * 
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.trapster.android.activity.fragment.AbstractMapLayer#onTilt(float)
	 */
	@Override
	public boolean onTilt(float tileAngle) {
		return false;
	}

	@Override
	public boolean onPinchZoom(float zoomLevel, PointF arg1) {
		scheduleRedraw();

		return false;
	}

	private void showClusterTrapPopup(List<Trap> traps) {

		Intent i = new Intent(application, ClusterTrapScreen.class);
		i.putExtra(ClusterTrapScreen.CLUSTER_TRAP_TRAP_EXTRAS,
				(Serializable) traps);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		application.getApplicationContext().startActivity(i);
	}

	private void showSingleTrapPopup(Trap trap) {
		Intent intent = new Intent(application, SingleTrapDetailsScreen.class);
		intent.putExtra(SingleTrapDetailsScreen.TRAP_BUNDLE_KEY, trap);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		application.getApplicationContext().startActivity(intent);

	}

	private class BackgroundAsyncTask implements Runnable {

		private MapTransaction transaction;

		public void execute() {
			if(isMapAttached())
                backgroundExecutor.execute(this);
		}

		@Override
		public void run() {

       		transaction = startTransaction();

			Cursor cursor = cursorLoader.loadInBackground();

            ArrayList<Trap> trapsToCluster = new ArrayList<Trap>();
            if (cursor != null)
            {
                int trapCount = 0;
                if (cursor.moveToFirst()) {
                    do {
                        try {

                            int trapId = cursor.getInt(cursor
                                    .getColumnIndex(TrapProvider.FIELD_ID));

                            if (trapCache.get(trapId) == null) {

                                Trap trap = TrapProvider.fromCursor(cursor);

                                trapCount++;

                                addTrap(trap);

                                trapsToCluster.add(trap);

                            }

                        } catch (Exception e) {

						  //Log.e(LOGNAME, "Unable to load trap:" +  Log.getStackTraceString(e));

                        }
                    } while (cursor.moveToNext());
                }

                cursor.close();
            }



			transaction.commit();


            transaction = startTransaction();
            clusterTraps(trapsToCluster);
            transaction.commit();

			scheduleRedraw();
		}

        private void addTrap(Trap trap) {
            // If the trap isn't already loaded into the map
            if (trapCache.get(trap.getId()) == null) {

               // Log.v(LOGNAME,"Adding trap to map:"+trap.getId());

                // Create the MapMarker class, loads the bitmap and sets the
                // offset
                TrapMapMarker marker = new TrapMapMarker(trap);

                /*
                If the attributes aren't loaded the marker image will be unavailable. Adding it to the map now will make it invisible
                 */
                if(marker.getMarker() != null){

                    // Sets the OnClick handler
                    marker.setOnMarkerClick(new OnMapMarkerClick() {

                        @Override
                        public void onMapMarkerClick(AbstractMapMarker marker) {
                            if (marker instanceof TrapMapMarker) {
                                showSingleTrapPopup(((TrapMapMarker) marker)
                                        .getTrap());

                                //Trap trap = ((TrapMapMarker) marker).getTrap();
                                //TrapType type = trapManager.getTrapType(trap.getTypeid());
                                //soundManager.playSound(type.getAudioid(), soundThemeManager.getCurrentTheme());

                            }
                        }

                    });

                    // Adds the trap to the cache (avoids duplicates being added)
                    trapCache.put(marker.getTrapId(), marker);

                    /*
                     * Adds the new trap to every zoom level layer.
                     */
                    for (int zoomLevel = MINIMUM_CLUSTER_ZOOM; zoomLevel >= MAXIMUM_CLUSTER_ZOOM; zoomLevel--) {

                        Quadtree clusterIndex = getQuadtreeForZoom(zoomLevel);

                        clusterIndex.insert(marker.getSpatialIndex(), marker);

                        setQuadtreeForZoom(clusterIndex, zoomLevel);

                    }


                    /*
                     * Adds the marker on each level
                     */
                    transaction.addMapMarker(marker, MAXIMUM_CLUSTER_ZOOM + 1,
                            AbstractMapMarker.MAP_MAXIMUM_ZOOM);

                }else android.util.Log.e(LOGNAME,"Attributes not loaded for trap skipping:"+trap.getTypeid());
            }
        }

        private void clusterTraps(ArrayList<Trap> trapsToCluster) {

            for (Trap trap : trapsToCluster) {

                // If the trap isn't already loaded into the map
                if (trapCache.get(trap.getId()) != null) {

                    TrapMapMarker marker = trapCache.get(trap.getId());

				/*
				 * Iterates through each layer, starting from the highest zoom
				 * level and checks if the new trap needs to be clustered.
				 */
                    for (int zoomLevel = MINIMUM_CLUSTER_ZOOM - 1; zoomLevel > MAXIMUM_CLUSTER_ZOOM; zoomLevel--) {

                        Envelope iconEnvelope = getIconEnvelopeForZoomLevel(marker,
                                zoomLevel);

                        Quadtree testClusterIndex = getQuadtreeForZoom(zoomLevel);

                        @SuppressWarnings("unchecked")
                        List<AbstractMapMarker> testCluster = testClusterIndex
                                .query(iconEnvelope);

					/*
					 * If the testCluster is non-zero it means that there are
					 * traps that are close to this point and should be
					 * clustered together
					 */
                        if (testCluster.size() > 1) {
                            {/*
						 * //Log.d(LOGNAME, "Found " + testCluster.size() +
						 * " points to cluster on zoom level:" + zoomLevel);
						 */
                            }

                            // prevents duplicate traps from being added to a
                            // cluster
                            HashSet<Trap> trapList = new HashSet<Trap>();

                            Iterator<AbstractMapMarker> iter = testCluster
                                    .iterator();
                            while (iter.hasNext()) {
                                AbstractMapMarker trapHolder = iter.next();

                                hideMapMarker(trapHolder, zoomLevel);

							/*
							 * Trap to be clustered is another cluster so all
							 * the traps need to be added
							 */
                                if (trapHolder instanceof ClusteredTrapMapMarker) {

                                    ClusteredTrapMapMarker clusteredTrapHolder = (ClusteredTrapMapMarker) trapHolder;

                                    // Add the traps to the cluster (Avoid adding
                                    // duplicates)
                                    trapList.addAll(clusteredTrapHolder.getTraps());

                                } else if (trapHolder instanceof TrapMapMarker) {

                                    TrapMapMarker tempMarker = (TrapMapMarker) trapHolder;

                                    trapList.add(tempMarker.trap);

                                }

                                // Remove the trap from spatialIndex
                                testClusterIndex.remove(
                                        trapHolder.getSpatialIndex(), trapHolder);

                            }

                            ClusteredTrapMapMarker clusteredTrapMapMarker = new ClusteredTrapMapMarker(
                                    new ArrayList<Trap>(trapList));

                            clusteredTrapMapMarker
                                    .setOnMarkerClick(new OnMapMarkerClick() {
                                        @Override
                                        public void onMapMarkerClick(
                                                AbstractMapMarker marker) {
                                            if (marker instanceof ClusteredTrapMapMarker) {
                                                showClusterTrapPopup(((ClusteredTrapMapMarker) marker)
                                                        .getTraps());
                                            }
                                        }
                                    });

                            testClusterIndex.insert(
                                    clusteredTrapMapMarker.getSpatialIndex(),
                                    clusteredTrapMapMarker);

						/*
						 * testing
						 */
                            transaction.addMapMarker(clusteredTrapMapMarker,
                                    zoomLevel);

                            setQuadtreeForZoom(testClusterIndex, zoomLevel);

                        }

                    }


                }
            }
        }

    }

    public void addTrap(Trap trap){
        MapTransaction transaction = startTransaction();
        addTrap(trap,transaction);
        transaction.commit();
    }

    private void addTrap(Trap trap, MapTransaction transaction) {

        android.util.Log.v(LOGNAME,"Adding trap:"+trap.getId());
        // If the trap isn't already loaded into the map
        if (trapCache.get(trap.getId()) == null) {

            // Log.v(LOGNAME,"Adding trap to map:"+trap.getId());

            // Create the MapMarker class, loads the bitmap and sets the
            // offset
            TrapMapMarker marker = new TrapMapMarker(trap);

                /*
                If the attributes aren't loaded the marker image will be unavailable. Adding it to the map now will make it invisible
                 */
            if(marker.getMarker() != null){

                // Sets the OnClick handler
                marker.setOnMarkerClick(new OnMapMarkerClick() {

                    @Override
                    public void onMapMarkerClick(AbstractMapMarker marker) {
                        if (marker instanceof TrapMapMarker) {
                            showSingleTrapPopup(((TrapMapMarker) marker)
                                    .getTrap());

                            //Trap trap = ((TrapMapMarker) marker).getTrap();
                            //TrapType type = trapManager.getTrapType(trap.getTypeid());
                            //soundManager.playSound(type.getAudioid(), soundThemeManager.getCurrentTheme());

                        }
                    }

                });

                // Adds the trap to the cache (avoids duplicates being added)
                trapCache.put(marker.getTrapId(), marker);

                    /*
                     * Adds the new trap to every zoom level layer.
                     */
                for (int zoomLevel = MINIMUM_CLUSTER_ZOOM; zoomLevel >= MAXIMUM_CLUSTER_ZOOM; zoomLevel--) {

                    Quadtree clusterIndex = getQuadtreeForZoom(zoomLevel);

                    clusterIndex.insert(marker.getSpatialIndex(), marker);

                    setQuadtreeForZoom(clusterIndex, zoomLevel);

                }


                    /*
                     * Adds the marker on each level
                     */
                transaction.addMapMarker(marker, MAXIMUM_CLUSTER_ZOOM + 1,
                        AbstractMapMarker.MAP_MAXIMUM_ZOOM);

            }//else Log.v(LOGNAME,"Attributes not loaded for trap skipping");
        }
    }

    public void removeTrap(int trapId){
        TrapMapMarker marker = trapCache.get(trapId);
        removeTrap(marker);
    }

	public void removeTrap(Trap trap) {

		TrapMapMarker marker = trapCache.get(trap.getId());
        removeTrap(marker);
    }

    private void removeTrap(TrapMapMarker marker){
		if (marker != null) {
			
			// Remove the trap from the cache
			trapCache.remove(marker.getTrapId());

			hideMapMarker(marker);
			
			MapTransaction transaction = startTransaction();
		

			transaction.removeMapMarker(marker);
			
			/*
			 * Remove the trap from each zoom level
			 */
			for (int zoomLevel = MINIMUM_CLUSTER_ZOOM; zoomLevel >= MAXIMUM_CLUSTER_ZOOM; zoomLevel--) {

				Quadtree clusterIndex = getQuadtreeForZoom(zoomLevel);

				boolean removed = clusterIndex.remove(marker.getSpatialIndex(),
						marker);



				setQuadtreeForZoom(clusterIndex, zoomLevel);
			}

			/*
			 * Iterates through each layer and checks if the trap is in a
			 * cluster.
			 */
			for (int zoomLevel = MINIMUM_CLUSTER_ZOOM; zoomLevel >= MAXIMUM_CLUSTER_ZOOM; zoomLevel--) {

				Quadtree testClusterIndex = getQuadtreeForZoom(zoomLevel);

				@SuppressWarnings("unchecked")
				List<AbstractMapMarker> allMarkers = testClusterIndex
						.queryAll();

				for (AbstractMapMarker am : allMarkers) {
					if ((am instanceof ClusteredTrapMapMarker)) {

						ClusteredTrapMapMarker clusteredTrapHolder = (ClusteredTrapMapMarker) am;
						

						if (clusteredTrapHolder
								.containsTrap(marker.getTrapId())) {
						

							// Remove the trap from spatialIndex
							testClusterIndex.remove(
									clusteredTrapHolder.getSpatialIndex(),
									clusteredTrapHolder);

							transaction.removeMapMarker(clusteredTrapHolder);
							
							
							HashSet<Trap> trapList = new HashSet<Trap>();

							trapList.addAll(clusteredTrapHolder.getTraps());
							trapList.remove(marker.trap);

							if (trapList.size() == 1) {

								Trap remainingTrap = new ArrayList<Trap>(
										trapList).get(0);

								TrapMapMarker tmm = new TrapMapMarker(
										remainingTrap);

								// Sets the OnClick handler
								tmm.setOnMarkerClick(new OnMapMarkerClick() {

									@Override
									public void onMapMarkerClick(
											AbstractMapMarker marker) {
										if (marker instanceof TrapMapMarker) {
											showSingleTrapPopup(((TrapMapMarker) marker)
													.getTrap());
										}
									}

								});

								testClusterIndex.insert(tmm.getSpatialIndex(),
										tmm);

								/*
								 * testing
								 */
								transaction.addMapMarker(tmm, zoomLevel);

							} else {
								ClusteredTrapMapMarker clusteredTrapMapMarker = new ClusteredTrapMapMarker(
										new ArrayList<Trap>(trapList));

								clusteredTrapMapMarker
										.setOnMarkerClick(new OnMapMarkerClick() {
											@Override
											public void onMapMarkerClick(
													AbstractMapMarker marker) {
												if (marker instanceof ClusteredTrapMapMarker) {
													showClusterTrapPopup(((ClusteredTrapMapMarker) marker)
															.getTraps());
												}
											}
										});

								testClusterIndex.insert(clusteredTrapMapMarker
										.getSpatialIndex(),
										clusteredTrapMapMarker);

								/*
								 * testing
								 */
								transaction.addMapMarker(
										clusteredTrapMapMarker, zoomLevel);
							}

							setQuadtreeForZoom(testClusterIndex, zoomLevel);

						}
					}

				}
			}

			transaction.commit();

			// This forces a redraw
			previousZoomLevel = 0;
			scheduleRedraw();
		}

	}

	/**
	 * This function generates a geographic area at a zoom level where we
	 * suspect the traps will be overlapping. Because we are using a 3D map with
	 * tilt function, it is very difficult to know exactly when the traps
	 * overlap on the graphics layer.
	 * 
	 * The parameters below can be tweaked to get the best possible user
	 * experience.
	 * 
	 * @param marker
	 * @param zoomLevel
	 * @return
	 */
	private Envelope getIconEnvelopeForZoomLevel(TrapMapMarker marker,
			int zoomLevel) {

		double mpp = meterPerPixelAtZoom[zoomLevel];



		/*
		 * Feel free to adjust this based on how the clusters look. 35 is a
		 * reasonable value and seems to work. A higher value will give you more
		 * clusters and less overlap
		 */
		double iconSizeInMeters = 35 * mpp;

		double deltaY = GeographicUtils
				.calculateLatitudeSpanOnSameLongitudialLine((float) iconSizeInMeters);
		double deltaX = GeographicUtils
				.calculateLongitudeSpanOnSameLatitudeLevel(
						(float) iconSizeInMeters, marker.getTrap()
								.getLatitude(), marker.getTrap().getLongitude());

		Envelope trapEnvelope = new Envelope(marker.getSpatialIndex());
		trapEnvelope.expandBy(deltaX, deltaY);

		return trapEnvelope;

	}

	// This will cause ALL traps to be removed and then rebuilt into clusters.
	public void wipeTrapCache() {
		trapCache.clear();
		clusterLayerIndex.clear();
		removeAllMapMarkers();

	}

	

}
