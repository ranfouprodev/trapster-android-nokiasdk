package com.trapster.android.activity.settings.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

public class InterceptableScrollView extends ScrollView
{
    private boolean scrollable = true;

    private OnTouchInterceptListener listener;

    public InterceptableScrollView(Context context)
    {
        super(context);
    }

    public InterceptableScrollView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public InterceptableScrollView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    public void setOnTouchInterceptListener(OnTouchInterceptListener listener)
    {
        this.listener = listener;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event)
    {
        if (listener != null && listener.shouldScroll())
            return super.onInterceptTouchEvent(event);
        else
            return false;
    }

    public interface OnTouchInterceptListener
    {
        public boolean shouldScroll();
    }
}
