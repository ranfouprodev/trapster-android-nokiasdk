package com.trapster.android.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.*;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.facebook.Settings;
import com.google.inject.Inject;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.TrapsterApplication;
import com.trapster.android.comms.ITrapsterAPI;
import com.trapster.android.comms.SessionListener;
import com.trapster.android.comms.rest.StatusCode;
import com.trapster.android.manager.SessionManager;
import com.trapster.android.model.User;
import com.trapster.android.model.dao.StringResourceDAO;
import com.trapster.android.util.BackgroundTask;
import com.trapster.android.util.TrapsterError;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.screen_start)
public class StartScreen extends Screen implements SessionListener {

	private static final String LOGNAME = "StartScreen";

	// Request Codes
	private static final int REQUEST_CODE_LOGIN_ACTIVITY = 1;
	private static final int REQUEST_CODE_CONFIGURE_GPS_ACTIVITY = 2;
	private static final int REQUEST_CODE_TERMS_ACTIVITY = 3;


	// Dialogs
	private static final int GPS_MISSING_DIALOG = 0;

    private final static String CLASS_NAME = "StartScreen";

	
	@Inject
	SessionManager sessionManager;

	@Inject
	LocationManager locationManager;


    @Inject ITrapsterAPI api;
    @Inject StringResourceDAO stringResourceDAO;

	//private AttributesLoadedReceiver receiver;

	private IntentFilter intentFilter;

	private boolean isBusy = false; 

	@Inject SharedPreferences settings;

    //private static Runnable launchMapRunnable;

    private boolean hasStartedMap = false;

    @InjectView(R.id.spinner)
    ImageView spinner;

    private final Handler handler = new Handler();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        /*launchMapRunnable = new Runnable()
        {
            @Override
            public void run()
            {
                launchMapActivity();
            }
        };*/
        spinner.startAnimation(AnimationUtils.loadAnimation(this, R.anim.spinner_animation));

	}
	
	void launchMapActivity() {


		/*if (!runChecks())
        {
            if (!hasStartedMap)
            {
                hasStartedMap = true;

            }
		}*/

        Intent intent = new Intent(this, MapScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();

	}
	
	protected void onPause() {
		super.onPause();
		
		// Hide the splash dialog if showing
		hideDialog();
        handler.removeCallbacks(null);
		
	}

	@Override
	public void onStart() {
		super.onStart();
		
		//receiver = new AttributesLoadedReceiver();
		//intentFilter = new IntentFilter(AttributesSyncService.ATTRIBUTES_LOADED_LISTENER);
		//registerReceiver(receiver, intentFilter);


	
    }

    @Override
    public void onResume()
    {
        super.onResume();
        publishFacebookInstall();

        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {

                if (!checkAttributes())
                {
                    if (!checkForLegal())
                    {
                        if (!checkForAutoLogin())
                        {
                            launchMapActivity();
                        }
                    }
                    else
                    {
                        startActivityForResult(new Intent(StartScreen.this, TermsScreen.class),
                                REQUEST_CODE_TERMS_ACTIVITY);
                    }
                }
            }
        }, 2000);


        //handler.postDelayed(launchMapRunnable, 5000);
        //launchMapActivity();
    }
	@Override
	public void onStop() {
		super.onStop();

        //try{unregisterReceiver(receiver);}catch (IllegalArgumentException e){}
    }
	
	
	/*
	 * These checks run for each startup in the order below. 
	 * 
	 */
	boolean runChecks() {

        boolean checkattributes = checkAttributes();
        boolean checklegal = checkForLegal();
        boolean checkGPS = checkForGpsEnabled();
        boolean checklogin = checkForAutoLogin();

        return checkattributes || checklegal || checkGPS || checklogin;

		//return  checkAttributes() || checkForLegal() || checkForGpsEnabled() || checkForAutoLogin();
	}


	/**
	 * Checks for the preloaded attributes are present and installed
	 * 
	 * 
	 * @return true if the attributes need to be loaded 
	 */
	private boolean checkAttributes(){
		long lastUpdateTimeAttributes = settings.getLong(
				Defaults.PREFERENCE_LAST_UPDATE_TIME_ALLATTRIBUTES, 0);
		
		//return lastUpdateTimeAttributes == 0;
        return false; // Temporary hack, because we need to get this hotfix out.  The logic being that we preload attributes, so we'll have SOMETHING
	}
	
	/**
	 * Checks the validity of the user credentials stored on the device
	 * 
	 * @return true if there is a login in process
	 */
	private boolean checkForAutoLogin() {

        boolean isLoggedIn = sessionManager.isLoggedIn();
        if (isLoggedIn)
            sessionManager.requestUserStatistics();

		if (sessionManager.hasCredentials() && !isLoggedIn && !isBusy)
        {
            isBusy  = true;
            sessionManager.autoLogin(this);
            return true;
		}
        else if (!sessionManager.hasCredentials() && !isLoggedIn && !isBusy)
            sessionManager.sendFlurryAutoLoginEvent(false);
		return false;
	}

	/**
	 * Checks if the Location Services (GPS) is disabled
	 * 
	 * @return true if GPS is disabled in the system
	 */
	private boolean checkForGpsEnabled() {
		
		String provider = LocationManager.GPS_PROVIDER;

		if (provider == null
				|| !locationManager
						.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			TrapsterApplication trapsterApp = (TrapsterApplication) getApplication();

			if (!trapsterApp.hasGpsWarningDisplayed()) {

				showDialog(GPS_MISSING_DIALOG);
				trapsterApp.setGpsWarningDisplayed(true);
				return true;
			} else{
				return false;
			}
		}
		return false;
	}
	

	private boolean checkForLegal() {

        long lastAcceptedTerms = settings.getLong(Defaults.ASSET_TERMSANDCONDITIONS_ACCEPTED_DATE, 0);
        long currentTerms = settings.getLong(Defaults.ASSET_TERMSANDCONDITIONS_NEWEST_DATE, 0);


        if(currentTerms == 0 || lastAcceptedTerms == 0 || lastAcceptedTerms != currentTerms )
        {
			return true;	
		}
		
		return false; 

	}
	
	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog dialog;

		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		switch (id) {
		case GPS_MISSING_DIALOG:
			builder.setTitle(R.string.gps_error_title);
			builder.setCancelable(true);
			builder.setNegativeButton(R.string.gps_cancel_button,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							launchMapActivity();
							dialog.dismiss();
						}
					});
			builder.setPositiveButton(R.string.gps_ok_button,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							Intent gpsOptionsIntent = new Intent(
									android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
							startActivityForResult(gpsOptionsIntent,
									REQUEST_CODE_CONFIGURE_GPS_ACTIVITY);
							dialog.dismiss();
						}
					});
			builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
				public void onCancel(DialogInterface dialog) {
					finish();
				}
			});
			builder.setMessage(R.string.gps_error_text);
			return builder.create();
			
		
			
		default:
			return super.onCreateDialog(id);
		}

	}

	private boolean checkForTutorial() {
		/*if (!settings.getBoolean(Defaults.PREFERENCE_SHOW_TUTORIAL, false)) {
			
			Intent intent = new Intent(this, TutorialScreen.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			 
			finish(); 
			
			return true;
		}
		return false;*/

        return false;
	}


	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
			case REQUEST_CODE_CONFIGURE_GPS_ACTIVITY:
				launchMapActivity();
				break;
			case REQUEST_CODE_TERMS_ACTIVITY:
				if(resultCode == RESULT_OK)
					launchMapActivity();
				else
					finish();
				break;
			default:
				finish();
		}
	}



	public void onError(TrapsterError error) {
		showMessage(getString(R.string.login_error));
	}


    @Override
	public void onLoginFail(final User user, StatusCode code)
    {

		startActivityForResult(new Intent(this, LoginScreen.class),REQUEST_CODE_LOGIN_ACTIVITY);
	}

	public void onLoginSuccess(User user,String message) {
		if (message != null)
			showMessage(message);
		launchMapActivity(); 
	}



    /*private class AttributesLoadedReceiver extends RoboBroadcastReceiver
    {

        @Override
		public void handleReceive(Context context, Intent intent)
        {
            launchMapRunnable.run();
		}
		
	}*/

    @Override
    protected boolean shouldUseActionBar()
    {
        return false;
    }

    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }

    private void publishFacebookInstall()
    {
        final Context context = this;
        new BackgroundTask()
        {
            @Override
            public void onExecute()
            {
                try
                {
                    Settings.publishInstallAndWait(context, Defaults.FACEBOOK_APP_ID);
                }
                catch (Exception e)
                {
                }
            }

            @Override public void onTimeout(long runTime){}
        }.execute();

    }

}
