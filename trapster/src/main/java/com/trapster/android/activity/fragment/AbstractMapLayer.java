package com.trapster.android.activity.fragment;

import android.graphics.PointF;
import android.util.Log;
import com.here.android.common.GeoCoordinate;
import com.here.android.common.ViewObject;
import com.here.android.mapping.Map;
import com.here.android.mapping.MapContainer;
import com.here.android.mapping.MapFactory;
import com.here.android.mapping.MapObject;
import com.trapster.android.activity.fragment.util.AbstractMapMarker;
import com.trapster.android.activity.fragment.util.ClusteredTrapMapMarker;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public abstract class AbstractMapLayer implements MapLayer {

	private static final String LOGNAME = "AbstractMapLayer";

    /*
    MapFactory cannot be in static constructor since the factory needs to be initialized first
     */
	private MapContainer currentObjectLayer = null;

	private MapContainer modifiableObjectLayer = null;

	private HashMap<MapObject, AbstractMapMarker> visibleObjects = new HashMap<MapObject, AbstractMapMarker>();

	private final Object hashMapLock = new Object();

	private boolean inTransaction = false;

	private LinkedList<MapContainer> bufferQueue = new LinkedList<MapContainer>();

	private Map map;

	private boolean attached;

	public void onAttachMap(Map map){};
	public void onDetachMap(Map map){};
	
	
	public void attach(final Map map) {

        if(currentObjectLayer == null)
            currentObjectLayer = MapFactory.createMapContainer();

        if(modifiableObjectLayer == null)
            modifiableObjectLayer = MapFactory.createMapContainer();
        

		this.map = map; 

		map.addMapObject(currentObjectLayer);

		bufferQueue = new LinkedList<MapContainer>();
		bufferQueue.addFirst(currentObjectLayer);
		bufferQueue.addLast(modifiableObjectLayer);
		
		attached = true; 
		
		onAttachMap(map);

	}
	
	public void detach(){
		if(map != null){
            //currentObjectLayer.removeAllMapObjects();
            //modifiableObjectLayer.removeAllMapObjects();
			map.removeMapObject(currentObjectLayer);
			map.removeMapObject(modifiableObjectLayer);
		}

        onDetachMap(map);

        this.map = null;
        attached = false;

    }
	
	public boolean isMapAttached(){
		return attached; 
	}

	protected Map getMap(){
		return map; 
	}
	
	/*
	 * public void addMapObject(MapObject mo){
	 * markerObjectLayer.addMapObject(mo); }
	 */

	@Override
	public boolean onDoubleTap(PointF arg0) {
		return false;
	}

	@Override
	public void onLongPressReleased() {
	}

	@Override
	public boolean onLongPressed(PointF arg0) {
		return false;
	}

	@Override
	public void onMapAnimatingEnd() {
	}

	@Override
	public void onMapAnimatingStart() {
	}

	@Override
	public void onMapMoveEnd(GeoCoordinate arg0) {
	}

	@Override
	public void onMapMoveStart() {
	}

	@Override
	public boolean onMapObjectsSelected(List<ViewObject> viewObjects) {

		boolean consume = false;

		AbstractMapMarker mostImportantMarker = null;

		for (ViewObject viewObject : viewObjects) {
			synchronized (hashMapLock) {
				AbstractMapMarker marker = visibleObjects.get(viewObject);
				if (marker != null && marker.getOnMarkerClick() != null) {
					if (mostImportantMarker != null) {
						if (marker instanceof ClusteredTrapMapMarker) {
							if (mostImportantMarker instanceof ClusteredTrapMapMarker) {
								if (((ClusteredTrapMapMarker) mostImportantMarker)
										.getTraps().size() < ((ClusteredTrapMapMarker) marker)
										.getTraps().size()) {
									mostImportantMarker = marker;
								}
							}
						}
					} else
						mostImportantMarker = marker;
				}
			}

		}

		if (mostImportantMarker != null) {
			mostImportantMarker.getOnMarkerClick().onMapMarkerClick(
					mostImportantMarker);
			consume = true;
		}

		return consume;
	}

	@Override
	public void onMapSchemeSet() {
	}

	@Override
	public boolean onPinchZoom(float arg0, PointF arg1) {
		return false;
	}

	@Override
	public boolean onRotate(float arg0) {
		return false;
	}

	@Override
	public boolean onTap(PointF arg0) {
		return false;
	}

	@Override
	public boolean onTilt(float arg0) {
		return false;
	}

	@Override
	public boolean onTwoFingerTap(PointF arg0) {
		return false;
	}

	public boolean isVisible() {
		if (currentObjectLayer == null)
			return true;
		return currentObjectLayer.isVisible();
	}

	@Override
	public void hide() {
		//currentObjectLayer.setVisible(false);
	}

	@Override
	public void show() {
		//currentObjectLayer.setVisible(true);
	}

	@Override
	public void refresh() {
	}

	protected AbstractMapMarker getMarker(MapObject mapObject) {
		synchronized (hashMapLock) {
			return visibleObjects.get(mapObject);
		}

	}

	/*
	 * Adding a transaction function to add map changes in bulk instead of
	 * individual
	 */
	protected MapTransaction startTransaction() {
		inTransaction = true;

		MapTransaction transaction = new MapTransaction();

		return transaction;
	}

	private void swapMapContainers() {
		if(map != null){
			synchronized (map) {

				MapContainer foreground = bufferQueue.poll();
				map.removeMapObject(foreground);
				//bufferQueue.offer(foreground);
				bufferQueue.addLast(foreground);
	
				MapContainer background = bufferQueue.peek();
				map.addMapObject(background);
	
				inTransaction = false;
			}
            long end = System.currentTimeMillis();
		}
	
	}

	private MapContainer getBackgroundContainer() {
		return bufferQueue.getLast();
	}

	private HashMap<MapObject, AbstractMapMarker> getVisibleObjectsHashSet() {

		return visibleObjects;
	}

	protected boolean addMapMarker(AbstractMapMarker marker,
			int visibleAtZoomLevel) {

		HashMap<MapObject, AbstractMapMarker> visibleHashset = getVisibleObjectsHashSet();

		if (marker == null)
			return false;

		if (marker.getMarker() == null)
			return false;

		if (visibleHashset.containsKey(marker.getMarker())) {
			marker.getMarker().setVisible(visibleAtZoomLevel, true);
			return true;
		}

		marker.getMarker().setVisible(0, AbstractMapMarker.MAP_MAXIMUM_ZOOM,
				false);
		marker.getMarker().setVisible(visibleAtZoomLevel, true);

		currentObjectLayer.addMapObject(marker.getMarker());

		return true;

	}

	protected boolean addMapMarker(AbstractMapMarker marker, int minZoom,
			int maxZoom) {

		HashMap<MapObject, AbstractMapMarker> visibleHashset = getVisibleObjectsHashSet();

		if (marker == null)
			return false;

		if (marker.getMarker() == null)
			return false;

		if (visibleHashset.containsKey(marker.getMarker())) {
			marker.getMarker().setVisible(minZoom, maxZoom, true);
			return true;
		}

		marker.getMarker().setVisible(0, AbstractMapMarker.MAP_MAXIMUM_ZOOM,
				false);
		marker.getMarker().setVisible(minZoom, maxZoom, true);

		currentObjectLayer.addMapObject(marker.getMarker());

		return true;
	}

	protected boolean addMapMarker(AbstractMapMarker marker) {

		HashMap<MapObject, AbstractMapMarker> visibleHashset = getVisibleObjectsHashSet();

		if (marker == null)
			return false;

		if (marker.getMarker() == null)
			return false;

		if (visibleHashset.containsKey(marker.getMarker())) {
			marker.getMarker().setVisible(true);
			return true;
		}

		currentObjectLayer.addMapObject(marker.getMarker());

		return true;
	}

	protected void removeMapMarker(AbstractMapMarker marker) {
		startTransaction().removeMapMarker(marker).commit();
	}

	protected void hideMapMarker(AbstractMapMarker marker, int hiddenAtZoomLevel) {

		if (marker != null && marker.getMarker() != null) {

			marker.getMarker().setVisible(hiddenAtZoomLevel, false);
		}

	}

	protected void hideMapMarker(AbstractMapMarker marker) {

		if (marker != null && marker.getMarker() != null) {

			marker.getMarker().setVisible(false);
		}

	}

	protected void showMapMarker(AbstractMapMarker marker) {

		if (marker != null && marker.getMarker() != null) {

			marker.getMarker().setVisible(true);
		}

	}

	protected void removeAllMapMarkers() {
        if(currentObjectLayer != null)
            currentObjectLayer.removeAllMapObjects();

        if(modifiableObjectLayer != null)
            modifiableObjectLayer.removeAllMapObjects();

    }

	public class MapTransaction extends Thread {

		private HashMap<MapObject, AbstractMapMarker> addedOrChangedMarkers = new HashMap<MapObject, AbstractMapMarker>();
		private HashMap<MapObject, AbstractMapMarker> removedMarkers = new HashMap<MapObject, AbstractMapMarker>();

		public MapTransaction() {

		}

		public MapTransaction addMapMarker(AbstractMapMarker marker,
				int visibleAtZoomLevel) {

			if (marker == null || marker.getMarker() == null)// exception?
				return this;

			if (visibleObjects.containsKey(marker.getMarker())) {
				marker.getMarker().setVisible(visibleAtZoomLevel, true);
				return this;
			}

			marker.getMarker().setVisible(0,
					AbstractMapMarker.MAP_MAXIMUM_ZOOM, false);
			marker.getMarker().setVisible(visibleAtZoomLevel, true);

			addedOrChangedMarkers.put(marker.getMarker(), marker);

			return this;

		}

		public MapTransaction addMapMarker(AbstractMapMarker marker,
				int minZoom, int maxZoom) {

			if (marker == null || marker.getMarker() == null)// exception?
				return this;

			if (visibleObjects.containsKey(marker.getMarker())) {
				marker.getMarker().setVisible(minZoom, maxZoom, true);
				return this;
			}

			marker.getMarker().setVisible(0,
					AbstractMapMarker.MAP_MAXIMUM_ZOOM, false);
			marker.getMarker().setVisible(minZoom, maxZoom, true);

			addedOrChangedMarkers.put(marker.getMarker(), marker);

			return this;
		}

		public MapTransaction addMapMarker(AbstractMapMarker marker) {

			if (marker == null || marker.getMarker() == null)// exception?
				return this;

			if (visibleObjects.containsKey(marker.getMarker())) {
				marker.getMarker().setVisible(true);
				return this;
			}

			addedOrChangedMarkers.put(marker.getMarker(), marker);

			return this;
		}

		public MapTransaction removeMapMarker(AbstractMapMarker marker) {

			if (marker != null && marker.getMarker() != null) {
				removedMarkers.put(marker.getMarker(), marker);
			}

			return this;

		}


		public void rollback() {
			inTransaction = false;
		}

		public void commit() {

			start();

		}

		@Override
		public void run() {

			int size = addedOrChangedMarkers.size() + removedMarkers.size();

			if (size > 0) {

				long startTime = System.currentTimeMillis();

				synchronized (hashMapLock) {
			
					visibleObjects.putAll(addedOrChangedMarkers);

					Iterator<MapObject> iterRemoved = removedMarkers.keySet()
							.iterator();
					while (iterRemoved.hasNext()) {
						MapObject mo = iterRemoved.next();
						visibleObjects.remove(mo);
					}
								
					MapContainer background = getBackgroundContainer();
	
					commitChanges(background, addedOrChangedMarkers, removedMarkers);
	
					swapMapContainers();
	
					MapContainer newBackground = getBackgroundContainer();
	
					commitChanges(newBackground, addedOrChangedMarkers, removedMarkers);
	
					{/*Log.d(LOGNAME,
							"Loaded " + addedOrChangedMarkers.size()
									+ " new traps from database: Traps in Memory:"
									+ visibleObjects.size() + ":render time:"
									+ (System.currentTimeMillis() - startTime)
									+ " ms"); */}
				
				}
			}

		}


    }

	private void commitChanges(MapContainer container,
			HashMap<MapObject, AbstractMapMarker> addedOrChangedMarkers,
			HashMap<MapObject, AbstractMapMarker> removedMarkers) {

		Iterator<MapObject> iterAdded = addedOrChangedMarkers.keySet()
				.iterator();
		while (iterAdded.hasNext()) {
			MapObject mo = iterAdded.next();
			container.addMapObject(mo);
		}

		Iterator<MapObject> iterRemoved = removedMarkers.keySet().iterator();
		while (iterRemoved.hasNext()) {
			MapObject mo = iterRemoved.next();
			boolean removed = container.removeMapObject(mo);
			if(!removed)
				Log.d(LOGNAME, "Unable to remove marker:"+removedMarkers.get(mo).getMarker());
		}

	}

}
