package com.trapster.android.activity;

import android.os.Bundle;
import com.trapster.android.R;
import com.trapster.android.activity.fragment.AbstractMapFragment;
import com.trapster.android.activity.fragment.ReportTrapsMenuFragment;
import roboguice.inject.ContentView;
import roboguice.inject.InjectFragment;

@ContentView(R.layout.screen_select_traptype)
public class SelectTrapTypeScreen extends Screen
{

	private static final String LOGNAME = "SelectTrapTypeScreen";
    private final static String CLASS_NAME = "SelectTrapType";
    public static final String EXTRA_MAP_CONFIG = "EXTRA_MAP_CONFIG";

    @InjectFragment(R.id.fragmentSelectTrapType)
    ReportTrapsMenuFragment reportTrapsMenuFragment;

    private AbstractMapFragment.MapConfig mapConfig;



    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(R.string.selecttrap_title);

        if(getIntent() != null && getIntent().hasExtra(EXTRA_MAP_CONFIG)){
            reportTrapsMenuFragment.setMapConfig((AbstractMapFragment.MapConfig) getIntent().getParcelableExtra(EXTRA_MAP_CONFIG));
        }
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }


}
