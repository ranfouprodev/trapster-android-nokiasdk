package com.trapster.android.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.SeekBar;
import com.google.inject.Inject;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.activity.fragment.AlertSensitivitySeekBarFragment;
import com.trapster.android.manager.FlurryManager;
import com.trapster.android.manager.flurry.FLURRY_EVENT_NAME;
import com.trapster.android.manager.flurry.FLURRY_PARAMETER_NAME;
import com.trapster.android.manager.flurry.FlurryEvent;
import roboguice.inject.ContentView;

@ContentView(R.layout.screen_alert_sensitivity)
public class AlertSensitivityScreen extends Screen
{
    private static final int TIME_INTERVAL = 5;
    private static final int DEFAULT_TIME_VALUE = 0;


    private AlertSensitivitySeekBarFragment distanceBar;
    private AlertSensitivitySeekBarFragment reminderBar;
    private double distanceValue;
    private double timeValue;

    private final static String BUNDLE_KEY_DISTANCE = "bundleKeyDistance";
    private final static String BUNDLE_KEY_TIME = "bundleKeyTime";

    private final static String CLASS_NAME = "AlertSensitivity";

    @Inject
	SharedPreferences sharedPreferences;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        loadSettings();
    }

    @Override
    public void onPause()
    {
        saveSettings();
        super.onPause();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        setupDistanceBar();
        setupRemindereBar();
    }

    @Override
    public void onSaveInstanceState(Bundle bundle)
    {
        bundle.putDouble(BUNDLE_KEY_DISTANCE, distanceValue);
        bundle.putDouble(BUNDLE_KEY_TIME, timeValue);
    }

    @Override
    public void onRestoreInstanceState(Bundle bundle)
    {
        distanceValue = bundle.getDouble(BUNDLE_KEY_DISTANCE);
        timeValue = bundle.getDouble(BUNDLE_KEY_TIME);
    }

    @Override
    public void onOrientationChanged()
    {

    }

    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }


    private void loadSettings()
    {
        distanceValue = sharedPreferences.getFloat(Defaults.PREFERENCE_TRAP_ALERT_DISTANCE, (float)Defaults.DEFAULT_ALERT_DISTANCE);
        timeValue = sharedPreferences.getInt(Defaults.PREFERENCE_TRAP_ALERT_REPEAT_AUDIBLE, Defaults.DEFAULT_ALERT_REPEAT_MINUTES);
    }

    private void saveSettings()
    {
        sharedPreferences.edit().putFloat(Defaults.PREFERENCE_TRAP_ALERT_DISTANCE, distanceBar.getProgress()).commit();
        sharedPreferences.edit().putInt(Defaults.PREFERENCE_TRAP_ALERT_REPEAT_AUDIBLE, reminderBar.getProgress()*TIME_INTERVAL).commit();

        sendFlurryAlertNotificationChangeEvent();
    }

    private void setupDistanceBar()
    {
        distanceBar = (AlertSensitivitySeekBarFragment) getSupportFragmentManager().findFragmentById(R.id.alertSensitivityDistanceSeekView);
        distanceBar.setMaximumValue(2, 20);
        distanceBar.setMinimumValue(0);
        distanceBar.setTitle(getString(R.string.alert_sensitivity_distance_title));
        distanceBar.setSeekLocation((int) distanceValue);

        if (sharedPreferences.getBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_MILES, true))  // true = miles, false = km
            distanceBar.setSeekUnits(getString(R.string.alert_sensitivity_units_miles));
        else
            distanceBar.setSeekUnits(getString(R.string.alert_sensitivity_units_km));
        distanceBar.setSeekValue(String.valueOf(distanceBar.getSeekValue()));


        distanceBar.setSeekListener(new SeekBar.OnSeekBarChangeListener()
        {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b)
            {
                distanceBar.setSeekValue(String.valueOf(distanceBar.getSeekValue()));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar)
            {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar)
            {
                distanceValue = distanceBar.getProgress();
            }
        });
    }

    private void setupRemindereBar()
    {
        reminderBar = (AlertSensitivitySeekBarFragment) getSupportFragmentManager().findFragmentById(R.id.alertSensitivitySleepSeekView);

        final int maxInterval = 60;
        final int intervals = 12;
        final int intervalSize = maxInterval / intervals;

        reminderBar.setMaximumValue(maxInterval, intervals);
        reminderBar.setMinimumValue(0);
        reminderBar.setTitle(getString(R.string.alert_sensitivity_repeat_title));

        int timeValueInPieces = (int)timeValue / intervalSize;
        reminderBar.setSeekLocation((int) timeValueInPieces); // Read from settings
        reminderBar.setSeekUnits(getString(R.string.alert_sensitivity_units_seconds));
        reminderBar.setSeekValue(String.valueOf((int) reminderBar.getSeekValue()));

        reminderBar.setSeekListener(new SeekBar.OnSeekBarChangeListener()
        {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b)
            {
                reminderBar.setSeekValue(String.valueOf((int) reminderBar.getSeekValue()));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar)
            {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar)
            {
                timeValue = reminderBar.getProgress();
            }
        });
    }

    private void sendFlurryAlertNotificationChangeEvent()
    {
        FlurryEvent event = FlurryManager.createEvent(FLURRY_EVENT_NAME.TRAP_ALERT_NOTIFICATION_SETTINGS_CHANGE);
        event.putParameter(FLURRY_PARAMETER_NAME.ALERT_SETTINGS_DISTANCE, String.valueOf(distanceBar.getSeekValue()));
        event.putParameter(FLURRY_PARAMETER_NAME.ALERT_SETTINGS_REPEAT_DELAY, String.valueOf(reminderBar.getSeekValue()));

        event.send();
    }
}
