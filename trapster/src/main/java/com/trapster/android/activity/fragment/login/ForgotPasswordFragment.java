package com.trapster.android.activity.fragment.login;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.inject.Inject;
import com.trapster.android.R;
import com.trapster.android.activity.component.PopupMessage;
import com.trapster.android.comms.ITrapsterAPI;
import com.trapster.android.util.UsernameUtils;

import roboguice.inject.InjectView;

/**
 * Created by John on 8/15/13.
 */
public class ForgotPasswordFragment extends AbstractLoginFragment {

    private static final String LOGNAME = "ForgotPasswordFragment";

    public static ForgotPasswordFragment newInstance(Bundle args){
        ForgotPasswordFragment f = new ForgotPasswordFragment();
        f.setArguments(args);
        return f;
    }

    @Inject
    ITrapsterAPI api;

    @InjectView(R.id.buttonForgotPassword)
    Button buttonForgotPassword;

    @InjectView(R.id.editResendEmail)
    EditText textResendEmail;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatus(Status.Available);

        getParentActivity().setTitle(R.string.forgot_title);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login_forgot,null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        buttonForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendForgotPassword();
            }
        });

        textResendEmail.setText(UsernameUtils.getPrimaryEmailAccount(getLoginActivity()));
    }


    private void sendForgotPassword(){

        String email = textResendEmail.getText().toString();

        if(!LoginUtil.isEmailValid(email)){
            PopupMessage.makeText(getLoginActivity(),getString(R.string.signup_email_error),PopupMessage.STYLE_ALERT).show();
        }else{

            getLoginActivity().sendForgotPassword(email);

            PopupMessage msg =  PopupMessage.makeText(getLoginActivity(), getString(R.string.forgot_popup), PopupMessage.STYLE_ALERT);
            msg.setOnPopupClosedListener(new PopupMessage.OnPopupClosedListener() {
                @Override
                public void onPopupClosed() {
                   getLoginActivity().finish();
                }
            });
            msg.show();


        }
    }



    @Override
    void disable() {
        buttonForgotPassword.setEnabled(false);
    }

    @Override
    void enable() {
        buttonForgotPassword.setEnabled(true);
    }



}
