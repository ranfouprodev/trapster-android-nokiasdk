package com.trapster.android.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.util.DisplayMetrics;
import android.view.*;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.MenuItem;
import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockFragmentActivity;
import com.google.inject.Inject;
import com.trapster.android.BuildFeatures;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.activity.fragment.TrapsterDialogFragment;
import com.trapster.android.manager.FlurryManager;
import com.trapster.android.manager.flurry.FLURRY_EVENT_NAME;
import com.trapster.android.manager.flurry.FLURRY_PARAMETER_NAME;
import com.trapster.android.manager.flurry.FlurryEvent;
import com.trapster.android.service.TrapsterService;

import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;




public abstract class Screen extends RoboSherlockFragmentActivity
{

    @Inject SharedPreferences sharedPreferences;
  

    // Private Handlers Methods
    private String errorMessage;
    private final Handler mHandler = new Handler();
    final Runnable mShowMessage = new Runnable() {
        public void run() {
            if(!isFinishing())
                showDialog(null,errorMessage,null, null, null, null, 5000);
        }
    };

    protected final Runnable defaultHomeAction = new Runnable()
    {
        @Override
        public void run()
        {
            returnToMap();
        }
    };

    private Timer dialogDismissTimer;
    private boolean postSaveInstanceState;
    TextView textErrorMessage;

    private boolean backPressed = false;

    /**
	 * Shows a single dialog fragment. Any existing dialogs are closed when this is called
	 * 
	 * @param dialogFragment
	 */
    protected void showDialog(DialogFragment dialogFragment) {
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
	    Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
	    if (prev != null) {
	        ft.remove(prev);
	    }
	    ft.addToBackStack(null);

	    dialogFragment.setRetainInstance(true);
	    dialogFragment.show(ft, "dialog");
	}

	protected void hideDialog(){
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
	    Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
	    if (prev != null) {
            ft.hide(prev);
	        ft.remove(prev);

	    }
	    ft.addToBackStack(null);

        TrapsterDialogFragment dialogFragment= (TrapsterDialogFragment) getSupportFragmentManager().findFragmentByTag("dialog");
        if (dialogFragment != null)
            dialogFragment.dismiss();
	}

    protected void hideFragment(Fragment fragment){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        if (fragment != null) {
            ft.hide(fragment);
            ft.remove(fragment);
        }
        ft.addToBackStack(null);

    }

    protected void showDialog(String title, String description,String positiveButton, String negativeButton,
                                 View.OnClickListener posListener, View.OnClickListener negListener, int timeoutInMs) {

        if(!postSaveInstanceState){
            hideDialog();

            FragmentTransaction ft= getSupportFragmentManager().beginTransaction();
            ft.addToBackStack(null);
            final TrapsterDialogFragment trapsterDialogFragment =
                    TrapsterDialogFragment.newInstance(title, description, positiveButton,
                            negativeButton, posListener, negListener);
            trapsterDialogFragment.show(ft, "dialog");

            if (timeoutInMs > 0) {
                if (dialogDismissTimer != null)
                    dialogDismissTimer.cancel();
                dialogDismissTimer = new Timer("DialogDismissTimer");
                dialogDismissTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        try
                        {
                            trapsterDialogFragment.dismiss();
                        }
                        catch (NullPointerException npe)
                        {
                            // There appears to be a bug in the support library, causing a function BELOW dismiss() to throw an exception.  We catch it here so the app doesn't crash
                        }

                    }
                }, timeoutInMs);
            }
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        if (!BuildFeatures.SUPPORTS_HONEYCOMB)
            requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (shouldUseActionBar())
            enableActionBar();
         else
            disableActionBar();


        super.onCreate(savedInstanceState);


        setVolumeControlStream(AudioManager.STREAM_MUSIC);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        FlurryManager.startSession(this);

        sendFlurryScreenChangeEvent();
    }

    @Override
    public void onStop()
    {
        super.onStop();
        FlurryManager.endSession(this);
    }

    private void sendFlurryScreenChangeEvent()
    {
        String className = getClassName();
        if (className != null)
        {
            FlurryEvent event = FlurryManager.createEvent(FLURRY_EVENT_NAME.SCREEN_CHANGE);
            event.putParameter(FLURRY_PARAMETER_NAME.SCREEN_NAME, className);
            event.send();
        }
    }


    @Override
    public void onResume()
    {
        super.onResume();
        setScreenOrientation();
/*        if (sharedPreferences.getLong(Defaults.ASSET_TERMSANDCONDITIONS_ACCEPTED_DATE, 0) > 0)
            notifyBackgroundTimeout(false);*/
        //Settings.publishInstallAsync(this, Defaults.FACEBOOK_APP_ID);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        postSaveInstanceState = true;
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        postSaveInstanceState = false;
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onPause()
    {
        super.onPause();
/*        if (!backPressed && (sharedPreferences.getLong(Defaults.ASSET_TERMSANDCONDITIONS_ACCEPTED_DATE, 0) > 0))
            notifyBackgroundTimeout(true);*/
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        backPressed = true;
    }

    // Local Methods
    public void showMessage(String s) {
        errorMessage = s;
        mHandler.post(mShowMessage);
    }

    public void showMessage(int resId) {
        showMessage(getString(resId));
    }
    
    public void showMessageAndCloseActivity(int resourceId, int timeoutInMs){
    
    	String description = getString(resourceId); 
    	
    	 hideDialog();

         FragmentTransaction ft= getSupportFragmentManager().beginTransaction();
         ft.addToBackStack(null);
         TrapsterDialogFragment.newInstance(null, description, null, null, null, null).show(ft, "dialog");

         if (timeoutInMs > 0) {
             if (dialogDismissTimer != null)
                 dialogDismissTimer.cancel();
             dialogDismissTimer = new Timer("DialogDismissTimer");
             dialogDismissTimer.schedule(new TimerTask() {
                 @Override
                 public void run() {
                     hideDialog();
                     finish(); 
                 }
             }, timeoutInMs);
         }
    }

    public void showToastMessage(String message)
    {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_layout,
                (ViewGroup) findViewById(R.id.toast_layout_root));

        TextView text = (TextView) layout.findViewById(R.id.toastText);
        text.setText(message);

        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    public void showErrorMessage(int textViewId, String errorMessage)
    {
        textErrorMessage = (TextView) findViewById(textViewId);
        textErrorMessage.setText(errorMessage);
        textErrorMessage.setVisibility(View.VISIBLE);
        textErrorMessage = null;

    }

    public void setScreenOrientation()
    {
    	
        setRequestedOrientation(sharedPreferences.getInt(Defaults.PREFERENCE_ORIENTATION,
                Defaults.SETTING_DEFAULT_SCREEN_ORIENTATION));
    }
    protected void onOrientationChanged()
    {

    }



    protected void returnToMap()
    {
        MapScreen.returnToMap(this);
    }

    private void notifyBackgroundTimeout(boolean isPausing)
    {
        Intent intent = new Intent(this, TrapsterService.class);
        intent.putExtra(TrapsterService.BACKGROUND_TIMER_EVENT, isPausing);
        startService(intent);
    }

    protected void enableActionBar()
    {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
        {
            if (!BuildFeatures.SUPPORTS_HONEYCOMB)
            {
                CharSequence title = getTitle();
                actionBar.setIcon(R.drawable.map);
                setTitle(null);
                actionBar.setTitle(title);
                actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.redtitle_bar));
            }

            actionBar.setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    public void setTitle(CharSequence title)
    {
        super.setTitle(title);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
            actionBar.setTitle(title);
    }

    protected void disableActionBar()
    {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
        {
            actionBar.hide();
        }
    }

    protected boolean shouldUseActionBar()
    {
        return true;
    }

    //Ninja'd from StackOverflow - http://stackoverflow.com/questions/10380989/how-do-i-get-the-current-orientation-activityinfo-screen-orientation-of-an-a
    protected int getScreenOrientation()
    {
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        int orientation;
        // if the device's natural orientation is portrait:
        if ((rotation == Surface.ROTATION_0
                || rotation == Surface.ROTATION_180) && height > width ||
                (rotation == Surface.ROTATION_90
                        || rotation == Surface.ROTATION_270) && width > height)
        {
            switch (rotation)
            {
                case Surface.ROTATION_0:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
                case Surface.ROTATION_90:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
                case Surface.ROTATION_180:
                    orientation =
                            ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                    break;
                case Surface.ROTATION_270:
                    orientation =
                            ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                    break;
                default:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
            }
        }
        // if the device's natural orientation is landscape or if the device
        // is square:
        else
        {
            switch (rotation)
            {
                case Surface.ROTATION_0:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
                case Surface.ROTATION_90:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
                case Surface.ROTATION_180:
                    orientation =
                            ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                    break;
                case Surface.ROTATION_270:
                    orientation =
                            ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                    break;
                default:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
            }
        }

        return orientation;
    }

    protected abstract String getClassName();
}