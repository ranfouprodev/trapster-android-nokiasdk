package com.trapster.android.activity.fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.inject.Inject;
import com.here.android.common.GeoPosition;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.activity.Screen;
import com.trapster.android.activity.SingleTrapDetailsScreen;
import com.trapster.android.manager.FlurryManager;
import com.trapster.android.manager.PositionManager;
import com.trapster.android.manager.SessionManager;
import com.trapster.android.manager.TrapManager;
import com.trapster.android.manager.flurry.FLURRY_EVENT_NAME;
import com.trapster.android.manager.flurry.FLURRY_PARAMETER_NAME;
import com.trapster.android.manager.flurry.FlurryEvent;
import com.trapster.android.model.Trap;
import com.trapster.android.model.TrapType;
import com.trapster.android.util.GeographicUtils;
import roboguice.inject.InjectView;

import java.io.Serializable;
import java.text.DecimalFormat;

/**
 * Generates the list of trap types in a category
 * 
 * @author John
 * 
 */
public class TrapPopupFragment extends AbstractFragment {

	static final String LOGNAME = "Trapster.TrapPopupFragment";
	//
	private final String FLURRY_CLASS_NAME = "TrapAlertPopup";
	//
	Trap trap;
	OnVoteListener listener;
	private LinearLayout layoutTrapContainer;
    private TextView distanceText;
    private TextView trapTypeText;
	@Inject TrapManager trapManager;
	@Inject SessionManager sessionManager;

	@Inject PositionManager positionManager;
    @Inject SharedPreferences sharedPreferences;

    @InjectView(R.id.layoutTrapNotLoggedInContainer) LinearLayout loggedOutContainer;
    @InjectView(R.id.notLoggedInImageTrapIcon) ImageView loggedOutIcon;
    @InjectView(R.id.loggedOutDistanceText) TextView loggedOutdistanceText;
    @InjectView(R.id.loggedOutTrapTypeName) TextView loggedOutTrapTypeText;

    @InjectView(R.id.layoutTrapLoggedInContainer) LinearLayout loggedInContainer;
    @InjectView(R.id.loggedInImageTrapIcon) ImageView loggedInIcon;
    @InjectView(R.id.loggedInDistanceText) TextView loggedInDistanceText;
    @InjectView(R.id.loggedInTrapTypeName) TextView loggedInTrapTypeText;

    @InjectView(R.id.buttonAgree) ImageButton agreeButton;
    @InjectView(R.id.buttonDisagree) ImageButton disagreeButton;

    private final Handler handler = new Handler();

	//
	public interface OnVoteListener extends Serializable {
		void onVote(boolean vote, Trap trap);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_trappopup, container, false);
		return view;
	}

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        loggedOutContainer.setVisibility(View.VISIBLE);

        loggedInContainer.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent i = new Intent(getParentActivity(), SingleTrapDetailsScreen.class);
                i.putExtra(SingleTrapDetailsScreen.TRAP_BUNDLE_KEY, trap);
                getParentActivity().startActivity(i);
            }
        });

        agreeButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                showPopupMessage();
                listener.onVote(true, trap);
                sendFlurryTrapVote(true, trap);
            }
        });
        disagreeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showPopupMessage();
                listener.onVote(false, trap);
                sendFlurryTrapVote(false, trap);
            }
        });

        loggedInContainer.setVisibility(View.GONE);
        //
        if (trap != null)
            setContentView();
        //
    }

    private void showPopupMessage()
    {
        Activity activity = getParentActivity();
        if (activity != null && activity instanceof Screen)
            ((Screen) activity).showToastMessage(getString(R.string.single_trap_message_vote_received));
    }

	void setContentView() {
		ImageView icon;
		if (sessionManager.isLoggedIn())
        {
            loggedOutContainer.setVisibility(View.GONE);
			icon = loggedInIcon;
            layoutTrapContainer = loggedInContainer;
            distanceText = loggedInDistanceText;
            trapTypeText = loggedInTrapTypeText;
		}
        else
        {
			loggedInContainer.setVisibility(View.GONE);
            layoutTrapContainer = loggedOutContainer;
            icon = loggedOutIcon;
            distanceText = loggedOutdistanceText;
            trapTypeText = loggedOutTrapTypeText;
		}
		layoutTrapContainer.setVisibility(View.VISIBLE);
		//
		Bitmap bitmapIcon = trapManager.getMapIconForTrap(trap);

		if (bitmapIcon != null && icon != null) // Causes a crash if the asset
												// can't be found
		{
			Matrix matrix = new Matrix();
			matrix.setScale(1.30f, 1.30f);
			Bitmap mBitmap = Bitmap
					.createBitmap(bitmapIcon, 0, 0, bitmapIcon.getWidth(),
							bitmapIcon.getHeight(), matrix, true);
			icon.setImageBitmap(mBitmap);
			// Distance Away
			GeoPosition position = positionManager.getLastLocation();
			setDistanceValue(position);
			// Trap Type
			TrapType trapType = trapManager.getTrapType(trap.getTypeid());
            trapTypeText.setText(trapType.getName());
		}

	}

    void setDistanceValue(GeoPosition location)
    {
        String distanceString;
        if (location != null)
        {
            double km = (GeographicUtils.geographicDistance(location.getCoordinate().getLatitude(), location.getCoordinate().getLongitude(), trap.getLatitude(), trap.getLongitude())) / 1000;
            DecimalFormat distFormat = new DecimalFormat("#0.0");
            String distanceText;
            if (sharedPreferences.getBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_MILES, true))
                distanceText = " " + distFormat.format(GeographicUtils.kilometersToMiles(km)) + " mi ";
            else
                distanceText = " " + distFormat.format(km) + " km ";
            distanceString = distanceText.toUpperCase();
        }
        else
            distanceString = "";
        distanceText.setText(distanceString);
    }

	public void setOnVoteListener(OnVoteListener listener) {
		this.listener = listener;
	}

	public void setTrap(Trap trap) {
		this.trap = trap;
        handler.post(new Runnable()
        {
            @Override
            public void run()
            {
                setContentView();
            }
        });

	}

	public Trap getTrap() {
		return trap;
	}

	private void sendFlurryTrapVote(boolean whichVote, Trap trap) {
		FlurryEvent event;
		if (whichVote)
			event = FlurryManager
					.createEvent(FLURRY_EVENT_NAME.TRAP_VOTE_AGREE);
		else
			event = FlurryManager
					.createEvent(FLURRY_EVENT_NAME.TRAP_VOTE_DISAGREE);

		String trapName = trapManager.getTrapType(trap.getTypeid()).getName();
		String englishName = trapManager.getTrapType(trap.getTypeid())
				.getUnlocalizedName();

		event.putParameter(FLURRY_PARAMETER_NAME.TRAP_TYPE, englishName);
		event.putParameter(FLURRY_PARAMETER_NAME.TRAP_TYPE_LANGUAGE, trapName);
		event.putParameter(FLURRY_PARAMETER_NAME.LOCATION_COUNTRY,
				FlurryManager.getCountryCode());
		event.putParameter(FLURRY_PARAMETER_NAME.LOCATION_STATE,
				FlurryManager.getStateCode());
		event.putParameter(FLURRY_PARAMETER_NAME.DRIVING_SPEED,
				FlurryManager.getDrivingSpeed());
		event.putParameter(FLURRY_PARAMETER_NAME.VOTING_SCREEN,
				FLURRY_CLASS_NAME);

		event.send();
	}

}
