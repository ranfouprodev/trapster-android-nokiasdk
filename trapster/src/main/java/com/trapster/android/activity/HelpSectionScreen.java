package com.trapster.android.activity;

import android.content.Intent;
import android.os.Bundle;

import com.trapster.android.Defaults;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 11/17/12
 * Time: 5:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class HelpSectionScreen extends Screen {

    private final static String CLASS_NAME = "HelpSection";
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Retrieve the view
        Intent i = getIntent();

        if(!i.hasExtra(Defaults.INTENT_HELP_VIEW_ID))
            finish();

        setContentView(i.getExtras().getInt(Defaults.INTENT_HELP_VIEW_ID));
        this.setTitle(i.getStringExtra(Defaults.INTENT_HELP_VIEW_TITLE));


    }

    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }
}
