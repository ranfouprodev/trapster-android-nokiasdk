package com.trapster.android.activity.fragment.login;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by John on 9/29/13.
 */
public class LoginUtil {

    /**
     *  method for determining if the password length is at minimum
     *  six characters
     *
     */
    public static boolean isPasswordMinLength(String password)
    {
        if (password.length() < 6)
            return false;
        else
            return true;

    }

    /**
     * method is used for checking valid email id format.
     *
     * @param email
     * @return boolean true for valid false for invalid
     */
    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^.+\\@.+\\..{2,}$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    /**
     * method is used for checking valid username id format.
     *
     * @param username
     * @return boolean true for valid false for invalid
     */
    public static boolean isUserNameValid(String username) {
        boolean isValid = false;

        String expression = "[\\w]*";
        CharSequence inputStr = username;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }
}
