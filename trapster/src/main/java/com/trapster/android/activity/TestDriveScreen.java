package com.trapster.android.activity;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.trapster.android.R;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.screen_test_drive)
public class TestDriveScreen extends Screen
{
    private final static String CLASS_NAME = "TestDriveScreen";

    @InjectView(R.id.link1) TextView link1;
    @InjectView(R.id.link2) TextView link2;
    @InjectView(R.id.test_driveSw1) ToggleButton test_driveSw1;
    @InjectView(R.id.sec1Body) TextView tiv;

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        link1.setMovementMethod(LinkMovementMethod.getInstance());
        link2.setMovementMethod(LinkMovementMethod.getInstance());

        tiv.setText("twerks!");
    }


    @Override
    protected String getClassName(){
        return CLASS_NAME;
    }

}
