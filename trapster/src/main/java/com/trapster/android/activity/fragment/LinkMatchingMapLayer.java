package com.trapster.android.activity.fragment;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.here.android.common.GeoCoordinate;
import com.here.android.common.GeoPosition;
import com.here.android.mapping.*;
import com.trapster.android.model.RouteLink;
import com.trapster.android.util.GeographicUtils;
import com.trapster.android.util.MapUtils;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 10/24/13
 * Time: 4:25 PM
 * To change this template use File | Settings | File Templates.
 */
@Singleton
public class LinkMatchingMapLayer extends AbstractMapLayer {
    @Override
    public void forceUpdate(double lat, double lon, double radius) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    private static final int MAX_STORED_GEOPOSITIONS = 5;
    private static final int MAX_STORED_ROUTELINKS = 3;
    private static final int MIN_BOUNDING_BOX_SIZE_IN_METERS = 40;

    MapPolyline routeLinkOverlay;
    MapContainer linkMatchingRouteLinkContainer;

    MapContainer myLocationContainer;
    MapCircle myLocationOverlay;

    MapContainer boundingBoxContainer;
    MapPolyline boundingBoxPolyLine;

    MapContainer projectedRouteContainer;
    MapPolyline projectedRouteOverlay;

    LinkedList<MapCircle> locations = new LinkedList<MapCircle>();
    LinkedList<MapPolyline> routelinkPolyLines = new LinkedList<MapPolyline>();
    LinkedList<RouteLink> routeLinks = new LinkedList<RouteLink>();

    Map map;

    @Inject
    public LinkMatchingMapLayer() {
    }

    @Override
    public void onAttachMap(Map map) {

        this.map = map;

        linkMatchingRouteLinkContainer = MapFactory.createMapContainer();

        myLocationContainer = MapFactory.createMapContainer();

        boundingBoxContainer = MapFactory.createMapContainer();

        projectedRouteContainer = MapFactory.createMapContainer();
    }

    public void drawRouteLink(RouteLink routeLink, int color)
    {
        if(linkMatchingRouteLinkContainer != null)
        {
            routeLinkOverlay = MapUtils.generatePolyline(routeLink.toGeoCoords());
            if(updateMapPolylineLinkedList(routeLinkOverlay, routeLink))
            {
                routeLinkOverlay.setLineColor(color);
                routeLinkOverlay.setLineWidth(10);
                linkMatchingRouteLinkContainer.addMapObject(routeLinkOverlay);
                routeLinkOverlay.setVisible(true);
                map.addMapObject(linkMatchingRouteLinkContainer);
            }
        }
    }

    private boolean updateMapPolylineLinkedList(MapPolyline routelinkLine, RouteLink routeLink)
    {
        boolean routlinkAdded = false;
        for (int i = 0; i < routeLinks.size(); i++)
        {
            if (routeLinks.get(i).getLinkId().compareTo(routeLink.getLinkId()) == 0)
                return routlinkAdded;
        }

        if(routelinkPolyLines.size() >= MAX_STORED_ROUTELINKS)
        {
            linkMatchingRouteLinkContainer.removeMapObject(routelinkPolyLines.get(0));
            routelinkPolyLines.remove(0);
            routeLinks.remove(0);
        }

        routlinkAdded = true;
        routeLinks.add(routeLink);
        routelinkPolyLines.add(routelinkLine);
        return routlinkAdded;
    }

    private void updateMapCircleLinkedList(MapCircle myLocationCircle)
    {
        if(locations.size() >= MAX_STORED_GEOPOSITIONS)
        {
            myLocationContainer.removeMapObject(locations.get(0));
            locations.remove(0);
        }

        locations.add(myLocationCircle);

    }

    public void drawMyLocation(GeoCoordinate location, int color)
    {
        if(myLocationContainer != null)
        {
            myLocationOverlay = MapFactory.createMapCircle(6.0, location);
            updateMapCircleLinkedList(myLocationOverlay);

            myLocationContainer.addMapObject(myLocationOverlay);
            myLocationOverlay.setFillColor(color);

            myLocationOverlay.setVisible(true);
            map.addMapObject(myLocationContainer);
        }
    }

    public void drawProjectedRoute(ArrayList<RouteLink> projectRoutes, int color)
    {
        RouteLink routeLink;
        GeoCoordinate [] geoCoordinates;
        ArrayList<GeoCoordinate> geoCoordinateArrayList = new ArrayList<GeoCoordinate>();
        if(projectedRouteContainer != null)
        {
            //i is equal to 1 so you do not draw over your current route link
            for (int i = 1; i < projectRoutes.size(); i++)
            {
                routeLink = projectRoutes.get(i);
                geoCoordinates = routeLink.toGeoCoords();
                for (int j = 0; j < geoCoordinates.length; j++)
                {
                    geoCoordinateArrayList.add(geoCoordinates[j]);
                }
            }

            if(geoCoordinateArrayList.size() > 0)
            {
                projectedRouteOverlay = MapUtils.generatePolyline(geoCoordinateArrayList);

                projectedRouteOverlay.setLineColor(color);
                projectedRouteOverlay.setLineWidth(10);
                projectedRouteContainer.addMapObject(projectedRouteOverlay);
                projectedRouteOverlay.setVisible(true);
                map.addMapObject(projectedRouteContainer);
            }
        }
    }

    public void drawBoundingBox(Envelope bbox, int color)
    {
        ArrayList<GeoCoordinate> geoCoordinateBoundingBox = new ArrayList<GeoCoordinate>();

        if(boundingBoxContainer != null)
        {
            geoCoordinateBoundingBox.add(MapFactory.createGeoCoordinate(bbox.getMinY(), bbox.getMinX()));
            geoCoordinateBoundingBox.add(MapFactory.createGeoCoordinate(bbox.getMaxY(), bbox.getMinX()));
            geoCoordinateBoundingBox.add(MapFactory.createGeoCoordinate(bbox.getMaxY(), bbox.getMaxX()));
            geoCoordinateBoundingBox.add(MapFactory.createGeoCoordinate(bbox.getMinY(), bbox.getMaxX()));
            geoCoordinateBoundingBox.add(MapFactory.createGeoCoordinate(bbox.getMinY(), bbox.getMinX()));

            if(geoCoordinateBoundingBox.size() > 0)
            {
                boundingBoxPolyLine = MapUtils.generatePolyline(geoCoordinateBoundingBox);

                boundingBoxPolyLine.setLineColor(color);
                boundingBoxPolyLine.setLineWidth(5);
                boundingBoxContainer.addMapObject(boundingBoxPolyLine);
                boundingBoxPolyLine.setVisible(true);
                map.addMapObject(boundingBoxContainer);
            }
        }
    }

    public void drawBoundingBox(GeoPosition location,int color)
    {
        ArrayList<GeoCoordinate> geoCoordinateBoundingBox = new ArrayList<GeoCoordinate>();

        Coordinate center = new Coordinate(location.getCoordinate().getLongitude(),
                location.getCoordinate().getLatitude());

        double accuracy = 0.0;
        if (location.getLatitudeAccuracy() >= MIN_BOUNDING_BOX_SIZE_IN_METERS)
            accuracy = location.getLatitudeAccuracy();
        else
            accuracy = MIN_BOUNDING_BOX_SIZE_IN_METERS;


        Envelope bbox = GeographicUtils.createBoundingBox(center.y,
                center.x, accuracy).getEnvelopeInternal();

        if(boundingBoxContainer != null)
        {
            geoCoordinateBoundingBox.add(MapFactory.createGeoCoordinate(bbox.getMinY(), bbox.getMinX()));
            geoCoordinateBoundingBox.add(MapFactory.createGeoCoordinate(bbox.getMaxY(), bbox.getMinX()));
            geoCoordinateBoundingBox.add(MapFactory.createGeoCoordinate(bbox.getMaxY(), bbox.getMaxX()));
            geoCoordinateBoundingBox.add(MapFactory.createGeoCoordinate(bbox.getMinY(), bbox.getMaxX()));
            geoCoordinateBoundingBox.add(MapFactory.createGeoCoordinate(bbox.getMinY(), bbox.getMinX()));

            if(geoCoordinateBoundingBox.size() > 0)
            {
                boundingBoxPolyLine = MapUtils.generatePolyline(geoCoordinateBoundingBox);

                boundingBoxPolyLine.setLineColor(color);
                boundingBoxPolyLine.setLineWidth(5);
                boundingBoxContainer.addMapObject(boundingBoxPolyLine);
                boundingBoxPolyLine.setVisible(true);
                map.addMapObject(boundingBoxContainer);
            }
        }
    }


}
