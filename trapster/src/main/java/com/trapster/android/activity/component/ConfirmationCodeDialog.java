package com.trapster.android.activity.component;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.trapster.android.R;
import com.trapster.android.comms.ConfirmationListener;
import com.trapster.android.model.User;

public class ConfirmationCodeDialog extends Dialog implements View.OnClickListener{


    public static ConfirmationCodeDialog show(Context _context, User user, ConfirmationListener _listener){
        ConfirmationCodeDialog dialog = new ConfirmationCodeDialog(_context,android.R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // set the default dialog border visiblity to be transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        dialog.listener = _listener;
        dialog.setOnDismissListener(new OnDismissListener() {
            public void onDismiss(DialogInterface dialog) {
                ConfirmationCodeDialog confirmDialog= (ConfirmationCodeDialog) dialog;
                if (confirmDialog.listener != null)
                    confirmDialog.listener= null;
            }
        });

        dialog.user = user;
        return dialog;
    }

    private User user;
    private ConfirmationListener listener;
    private static boolean buttonOkSelected;

    private ConfirmationCodeDialog(Context context, int theme) {
        super(context, theme);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init(){
        setContentView(R.layout.dialog_confirmation);
        ((Button) findViewById(R.id.btnConfCancel)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnOk)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnResend)).setOnClickListener(this);

        setErrorVisibility();

        LinearLayout r = (LinearLayout) findViewById(R.id.dlgConf);
        Animation anim = AnimationUtils.loadAnimation(getContext(), R.anim.calloutfromright);
        r.setAnimation(anim);

    }

    public void setErrorVisibility()
    {
        if (buttonOkSelected)
            ((TextView)findViewById(R.id.confirmationError)).setVisibility(View.VISIBLE);
        buttonOkSelected = false;
    }


    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnOk:
                String code = ((EditText)findViewById(R.id.editConf)).getText().toString();
                buttonOkSelected = true;
                listener.onLogin(user,code);
                break;
            case R.id.btnConfCancel:
                cancel();
                break;
            case R.id.btnResend:
                listener.onResend(user);
                break;
        }
    }



}
