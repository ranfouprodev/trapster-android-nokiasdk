package com.trapster.android.activity.settings;

import android.widget.CompoundButton;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.activity.settings.fragment.CheckBoxSettingFragment;
import com.trapster.android.activity.settings.fragment.DividerSettingFragment;
import com.trapster.android.activity.settings.fragment.MultiModalSlideBarFragment;

public class MapOptionsSettingsScreen extends AbstractSettingsScreen
{

    private static final String CLASS_NAME = "Map Options Screen";
    @Override

    // Break these up into separate setup functions
    protected void setupFragments()
    {
        MultiModalSlideBarFragment mapStyleFragment = (MultiModalSlideBarFragment) createSettingsFragment(FRAGMENT_TYPE.MULTI_MODAL_SLIDE_BAR);

        mapStyleFragment.setDescription(R.string.setting_maptype_title);
        mapStyleFragment.setOptionText(R.string.setting_maptype_map, R.string.setting_maptype_satellite, R.string.setting_maptype_terrain);
        mapStyleFragment.setSelectionChangedListener(new MapStyleSelectionListener());

        DividerSettingFragment divider = (DividerSettingFragment) createSettingsFragment(FRAGMENT_TYPE.DIVIDER);
        divider.setDescription(R.string.settings_menu_map_display);

        final CheckBoxSettingFragment trafficFragment = (CheckBoxSettingFragment) createSettingsFragment(FRAGMENT_TYPE.CHECK_BOX);
        final CheckBoxSettingFragment patrolLineFragment = (CheckBoxSettingFragment) createSettingsFragment(FRAGMENT_TYPE.CHECK_BOX);
        //CheckBoxSettingFragment speedLimitFragment = (CheckBoxSettingFragment) createSettingsFragment(FRAGMENT_TYPE.CHECK_BOX);
        final CheckBoxSettingFragment virtualRadarFragment = (CheckBoxSettingFragment) createSettingsFragment(FRAGMENT_TYPE.CHECK_BOX);

        trafficFragment.setImageResource(R.drawable.traffic_icon);
        patrolLineFragment.setImageResource(R.drawable.patrollines_icon);
        //speedLimitFragment.setImageResource(R.drawable.speedlimitalerting_icon);
        virtualRadarFragment.setImageResource(R.drawable.virtualradar_icon);

        trafficFragment.setDescription(R.string.setting_mapitems_traffic);
        patrolLineFragment.setDescription(R.string.setting_mapitems_patrol);
        //speedLimitFragment.setDescription(R.string.setting_mapitems_speed_limit);
        virtualRadarFragment.setDescription(R.string.setting_mapitems_vitrual_radar);

        trafficFragment.setCheckBoxListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b)
            {
                editor.putBoolean(Defaults.PREFERENCE_MAP_DISPLAY_SHOW_TRAFFIC, b);
            }
        });

        patrolLineFragment.setCheckBoxListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b)
            {
                editor.putBoolean(Defaults.PREFERENCE_MAP_DISPLAY_SHOW_PATROL, b);
            }
        });

/*        speedLimitFragment.setCheckBoxListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b)
            {
                editor.putBoolean(Defaults.PREFERENCE_MAP_DISPLAY_ALERT_SPEED_LIMIT, b);
            }
        });*/

        virtualRadarFragment.setCheckBoxListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b)
            {
                editor.putBoolean(Defaults.PREFERENCE_MAP_DISPLAY_SHOW_VIRTUAL_RADAR, b);
            }
        });

        // Set starting values
        int mapType = sharedPreferences.getInt(Defaults.PREFERENCE_MAP_TYPE, Defaults.PREFERENCE_MAP_TYPE_MAP);
        switch (mapType)
        {
            case Defaults.PREFERENCE_MAP_TYPE_MAP:
            {
                mapStyleFragment.setStartSelection(MultiModalSlideBarFragment.SLIDER_SELECTION.LEFT);
                break;
            }
            case Defaults.PREFERENCE_MAP_TYPE_SATELLITE:
            {
                mapStyleFragment.setStartSelection(MultiModalSlideBarFragment.SLIDER_SELECTION.RIGHT);
                break;
            }
            case Defaults.PREFERENCE_MAP_TYPE_TERRAIN:
            {
                mapStyleFragment.setStartSelection(MultiModalSlideBarFragment.SLIDER_SELECTION.CENTER);
                break;
            }
        }

        trafficFragment.setChecked(sharedPreferences.getBoolean(Defaults.PREFERENCE_MAP_DISPLAY_SHOW_TRAFFIC, true));
        patrolLineFragment.setChecked(sharedPreferences.getBoolean(Defaults.PREFERENCE_MAP_DISPLAY_SHOW_PATROL, true));
        //speedLimitFragment.setChecked(sharedPreferences.getBoolean(Defaults.PREFERENCE_MAP_DISPLAY_ALERT_SPEED_LIMIT, true));
        virtualRadarFragment.setChecked(sharedPreferences.getBoolean(Defaults.PREFERENCE_MAP_DISPLAY_SHOW_VIRTUAL_RADAR, true));

/*        trafficFragment.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                trafficFragment.setChecked(!trafficFragment.isChecked());
            }
        });

        patrolLineFragment.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                patrolLineFragment.setChecked(!patrolLineFragment.isChecked());
            }
        });

        virtualRadarFragment.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                virtualRadarFragment.setChecked(!virtualRadarFragment.isChecked());
            }
        });*/
    }

    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }

    private class MapStyleSelectionListener implements MultiModalSlideBarFragment.OnSliderSelectionChangedListener
    {

        @Override
        public void onSliderSelectionChanged(MultiModalSlideBarFragment.SLIDER_SELECTION selection)
        {
            switch (selection)
            {
                case LEFT:
                {
                    editor.putInt(Defaults.PREFERENCE_MAP_TYPE, Defaults.PREFERENCE_MAP_TYPE_MAP);
                    break;
                }
                case RIGHT:
                {
                    editor.putInt(Defaults.PREFERENCE_MAP_TYPE, Defaults.PREFERENCE_MAP_TYPE_SATELLITE);
                    break;
                }
                case CENTER:
                {
                    editor.putInt(Defaults.PREFERENCE_MAP_TYPE, Defaults.PREFERENCE_MAP_TYPE_TERRAIN);
                    break;
                }
            }
        }
    }
}
