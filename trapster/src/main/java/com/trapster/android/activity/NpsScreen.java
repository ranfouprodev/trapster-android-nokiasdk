package com.trapster.android.activity;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.activity.fragment.BottomBarFragment;
import com.trapster.android.activity.fragment.login.LoginUtil;
import com.trapster.android.comms.ITrapsterAPI;
import com.trapster.android.manager.FlurryManager;
import com.trapster.android.manager.flurry.FLURRY_EVENT_NAME;
import com.trapster.android.manager.flurry.FLURRY_PARAMETER_NAME;
import com.trapster.android.manager.flurry.FlurryEvent;

import javax.inject.Inject;

import roboguice.inject.ContentView;
import roboguice.inject.InjectFragment;
import roboguice.inject.InjectView;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 11/17/12
 * Time: 6:02 PM
 * To change this template use File | Settings | File Templates.
 */

@ContentView(R.layout.screen_nps)
public class NpsScreen extends Screen
{

    private final static String CLASS_NAME = "NpsScreen";
    private final String CURRENT_VOTE_KEY = "CURRENT_VOTE_KEY";
    private final String LAST_VOTE_KEY = "LAST_VOTE_KEY";
    private final String HAS_SELECTED_KEY = "HAS_SELECTED_KEY";

    /*static final int[] answers = {R.id.answer0, R.id.answer1, R.id.answer2,
            R.id.answer3, R.id.answer4, R.id.answer5, R.id.answer6, R.id.answer7, R.id.answer8,
            R.id.answer9, R.id.answer10};*/

    @InjectView(R.id.textFeedback) EditText mFeedback;
    @InjectView(R.id.textAddress)EditText mEmailAddress;
    //@InjectView(R.id.slider) SeekBar sb;
    @InjectView(R.id.npsFeedbackBlock) LinearLayout linearLayout;
    @InjectView(R.id.npsOpacity) ImageView imageView;
    @InjectView(R.id.npsPrivacy) TextView privacy;
    @InjectView(R.id.textAddressError) TextView emailError;
    @Inject ITrapsterAPI api;
    @InjectFragment(R.id.statsNavFragment)BottomBarFragment bottomBarFragment;
    @Inject SharedPreferences sharedPreferences;

    @InjectView (R.id.npsCheckBox0) Button button0;
    @InjectView (R.id.npsCheckBox1) Button button1;
    @InjectView (R.id.npsCheckBox2) Button button2;
    @InjectView (R.id.npsCheckBox3) Button button3;
    @InjectView (R.id.npsCheckBox4) Button button4;
    @InjectView (R.id.npsCheckBox5) Button button5;
    @InjectView (R.id.npsCheckBox6) Button button6;
    @InjectView (R.id.npsCheckBox7) Button button7;
    @InjectView (R.id.npsCheckBox8) Button button8;
    @InjectView (R.id.npsCheckBox9) Button button9;
    @InjectView (R.id.npsCheckBox10) Button button10;

    private Button[] buttons;

    boolean isDialogVisible = false;
    private int lastSelectedBox = -1;
    private int currentVote = 0;
    private boolean hasSelected = false;



    private View.OnClickListener onClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            currentVote = (Integer)view.getTag();
            if (currentVote != lastSelectedBox || !hasSelected)
            {
                view.setSelected(true);
                if (lastSelectedBox > -1)
                    buttons[lastSelectedBox].setSelected(false);
                lastSelectedBox = currentVote;
                hasSelected = true;

                linearLayout.setVisibility(View.VISIBLE);
                bottomBarFragment.enable();
            }

        }
    };

    TextView vote;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        buttons = new Button[]{button0, button1, button2, button3, button4, button5, button6, button7, button8, button9, button10};

        for (int i = 0; i < buttons.length; i++)
        {

            Button button = buttons[i];
            button.setTag(new Integer(i));
            button.setText(String.valueOf(i));
            button.setOnClickListener(onClickListener);
        }



        /*sb.setMax(answers.length - 1);
        sb.setProgress(5);
        sb.setOnSeekBarChangeListener(this);*/

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        setPrivacyText();

        setupBottomBar();
        bottomBarFragment.disablePositiveButton();
    }

    private void setPrivacyText()
    {
        privacy.setText(Html.fromHtml(getApplicationContext().getString(R.string.nps_privacy_policy)), TextView.BufferType.SPANNABLE);


        String npsPrivacy = getApplicationContext().getString(R.string.nps_privacy_policy);
        String nps = privacy.getText().toString();

        int startIndex = npsPrivacy.indexOf("\">");
        String hyperlinkText = npsPrivacy.substring(startIndex+2, npsPrivacy.length()-4);
        startIndex = nps.indexOf(hyperlinkText);
        int endIndex = nps.length();


        Spannable privacyText= (Spannable) privacy.getText();
        privacyText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.pal_orangelinks)), startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        privacyText.setSpan(new StyleSpan(Typeface.BOLD), startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        privacy.setText(privacyText);

        privacy.setLinksClickable(true);
        privacy.setMovementMethod(LinkMovementMethod.getInstance());
    }


    private String getNPSFeedback()
    {
        return mFeedback.getText().toString();
    }

    private String getNPSEmailAddress()
    {
        return mEmailAddress.getText().toString();
    }


    private void setDialogActions()
    {
        View.OnClickListener negAction = new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hideDialog();
                imageView.setVisibility(View.GONE);
                bottomBarFragment.enable();
            }
        };
        View.OnClickListener posAction = new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hideDialog();
                finish();
            }
        };

        showDialog(getString(R.string.nps_cancel_title), getString(R.string.nps_cancel_description),
                getString(R.string.preference_yes), getString(R.string.preference_go_back), posAction, negAction, 0);
    }


    private void setupBottomBar()
    {
        bottomBarFragment.setNegativeAction(getString(R.string.preference_cancel), new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                bottomBarFragment.disable();
                isDialogVisible = true;
                imageView.setVisibility(View.VISIBLE);
                setDialogActions();
            }
        });

        bottomBarFragment.setPositiveAction(getString(R.string.preference_send), new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {


                String email = getNPSEmailAddress();
                if (email == null || email.equals("") || LoginUtil.isEmailValid(email))
                {
                    String npsVote = String.valueOf(currentVote);
                    api.sendNPSResults(npsVote, getNPSFeedback(), getNPSEmailAddress(), null);
                    showMessageAndCloseActivity(R.string.nps_thank_you, 5000);
                    sendFlurryNPSVoteEvent(npsVote);
                    sharedPreferences.edit().putLong(Defaults.NPS_LAST_RATING_TIME, System.currentTimeMillis()).commit();
                } 
                else
                {
                    emailError.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public void onStart()
    {
        super.onStart();
    }

    public void onStop()
    {
        super.onStop();

    }

    protected void onDestroy()
    {
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle bundle)
    {
        super.onSaveInstanceState(bundle);
        bundle.putInt(CURRENT_VOTE_KEY, currentVote);
        bundle.putInt(LAST_VOTE_KEY, lastSelectedBox);
        bundle.putBoolean(HAS_SELECTED_KEY, hasSelected);
    }

    @Override
    public void onRestoreInstanceState(Bundle bundle)
    {
        super.onRestoreInstanceState(bundle);
        currentVote = bundle.getInt(CURRENT_VOTE_KEY, 0);
        lastSelectedBox = bundle.getInt(LAST_VOTE_KEY);
        hasSelected = bundle.getBoolean(HAS_SELECTED_KEY);
        if (hasSelected)
        {
            buttons[currentVote].setSelected(true);
            linearLayout.setVisibility(View.VISIBLE);
            bottomBarFragment.enable();
        }


    }


    @Override
    public void onBackPressed()
    {
        if (isDialogVisible)
        {
            bottomBarFragment.enable();
            isDialogVisible = false;
            hideDialog();
            imageView.setVisibility(View.GONE);
        }
        else
            super.onBackPressed();
    }


    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }

    private void sendFlurryNPSVoteEvent(String vote)
    {
        FlurryEvent event = FlurryManager.createEvent(FLURRY_EVENT_NAME.NPS_SCREEN_VOTE);
        event.putParameter(FLURRY_PARAMETER_NAME.SCORE, vote);
    }
}
