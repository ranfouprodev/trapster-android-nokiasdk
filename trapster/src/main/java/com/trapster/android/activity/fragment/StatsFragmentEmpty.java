package com.trapster.android.activity.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.trapster.android.R;
import com.trapster.android.activity.UserFeedbackExplanationScreen;
import roboguice.inject.InjectView;

public class StatsFragmentEmpty extends AbstractFragment
{
    @InjectView(R.id.statsExplanationIcon2) ImageView statsExplanationIcon;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_stats_empty, container, false);
        return view;
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        if (statsExplanationIcon == null)
            statsExplanationIcon = (ImageView) view.findViewById(R.id.statsExplanationIcon2);
        statsExplanationIcon.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startActivity(new Intent(getParentActivity(), UserFeedbackExplanationScreen.class));
            }
        });
    }
}
