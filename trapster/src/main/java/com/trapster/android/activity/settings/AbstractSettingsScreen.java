package com.trapster.android.activity.settings;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.LinearLayout;
import com.google.inject.Inject;
import com.trapster.android.R;
import com.trapster.android.activity.LoginScreen;
import com.trapster.android.activity.Screen;
import com.trapster.android.activity.StatsScreen;
import com.trapster.android.activity.settings.component.InterceptableScrollView;
import com.trapster.android.activity.settings.fragment.*;
import com.trapster.android.manager.BitmapCacheManager;
import com.trapster.android.model.ImageResource;
import com.trapster.android.model.TrapType;
import com.trapster.android.model.dao.ImageResourceDAO;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

import java.util.Iterator;
import java.util.LinkedList;

@ContentView(R.layout.screen_abstract_settings)
public abstract class AbstractSettingsScreen extends Screen implements InterceptableScrollView.OnTouchInterceptListener
{
    private static final double ICON_SIZE_SCREEN_PERCENTAGE = 0.15;
    private static final String LOGNAME = "AbstractSettingsScreen";

    @Inject SharedPreferences sharedPreferences;
    @Inject ImageResourceDAO imageResourceDAO;
    @Inject BitmapCacheManager bitmapCacheManager;

    @InjectView(R.id.settingsContainer) LinearLayout container;
    @InjectView(R.id.screenScrollView) InterceptableScrollView scrollView;
    //protected LinkedHashMap<String, AbstractSettingFragment> settingsMap;
    private LinkedList<AbstractSettingFragment> settingsList;

    protected SharedPreferences.Editor editor;

    public enum FRAGMENT_TYPE
    {
        MULTI_MODAL_SLIDE_BAR,
        TEXT_ONLY,
        ICON,
        CHECK_BOX,
        RADIO_BUTTON,
        DIVIDER,
        SUB_TEXT,
        INFO_LAYOUT_NO_IMAGE,
        INFO_LAYOUT_IMAGE_LEFT,
        INFO_LAYOUT_IMAGE_RIGHT
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        editor = sharedPreferences.edit();

            //settingsMap = new LinkedHashMap<String, AbstractSettingFragment>();
            settingsList = new LinkedList<AbstractSettingFragment>();

            container.removeAllViews();

            setupFragments();

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            Iterator<AbstractSettingFragment> i = settingsList.iterator();

            while (i.hasNext())
            {
                AbstractSettingFragment fragment = i.next();

                if(savedInstanceState == null)
                    transaction.add(container.getId(), fragment);

                if (fragment.wantsToHide())
                    transaction.hide(fragment);
            }

            transaction.commit();

        scrollView.setOnTouchInterceptListener(this);

    }

    @Override
    public void onPause()
    {
        super.onPause();
        //FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        editor.apply();

       /* Iterator<AbstractSettingFragment> i = settingsList.iterator();

        while (i.hasNext())
        {
                transaction.detach(i.next());
        }

        transaction.commitAllowingStateLoss();*/
    }

    @Override
    public void onResume()
    {
        super.onResume();

        /*FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        Iterator<AbstractSettingFragment> i = settingsList.iterator();

        while (i.hasNext())
        {
            transaction.attach(i.next());
        }

        try
        {
            transaction.commitAllowingStateLoss();
        }
        catch (IllegalStateException ise)
        {
            finish(); // We won't have anything in this window anyway, so let's go ahead and kick them out, when they go back in, it'll work
        }*/
    }

    protected AbstractSettingFragment createSettingsFragment(FRAGMENT_TYPE fragmentType, final Class<?> clazz)
    {
        AbstractSettingFragment fragment = createSettingsFragment(fragmentType);
        fragment.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(AbstractSettingsScreen.this, clazz);
                intent.putExtra(LoginScreen.CLASS_EXTRA, StatsScreen.class);
                try
                {
                    startActivity(intent);
                }
                catch (ActivityNotFoundException e){}  //Bizarre case might cause this to happen, no idea why though
            }
        });

        return fragment;
    }

    protected AbstractSettingFragment createSettingsFragment(FRAGMENT_TYPE fragmentType)
    {
        AbstractSettingFragment fragment;
        switch (fragmentType)
        {
            case MULTI_MODAL_SLIDE_BAR:
            {
                fragment = new MultiModalSlideBarFragment();
                break;
            }
            case TEXT_ONLY:
            {
                fragment = new TextSettingFragment();
                break;
            }
            case ICON:
            {
                fragment = new IconSettingFragment();
                break;
            }
            case CHECK_BOX:
            {
                fragment = new CheckBoxSettingFragment();
                break;
            }
            case RADIO_BUTTON:
            {
                fragment = new RadioButtonSettingFragment();
                break;
            }
            case DIVIDER:
            {
                fragment = new DividerSettingFragment();
                break;
            }
            case SUB_TEXT:
            {
                fragment = new SubTextSettingFragment();
                break;
            }
            default:
            {
                throw new RuntimeException("Invalid fragment type specified");
            }
        }

        //settingsMap.put(key, fragment);
        settingsList.add(fragment);

        return fragment;
    }

    protected void setupIcon(final IconSettingFragment fragment, TrapType trapType) throws IndexOutOfBoundsException
    {


        String trapTypeIcon = trapType.getImageFilename();
        ImageResource imageResource = imageResourceDAO.findByKey(trapTypeIcon);

        Drawable drawable = bitmapCacheManager.getDrawableFromSvg(imageResource);

        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int portraitPixels;
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            portraitPixels = displayMetrics.heightPixels;
        else
            portraitPixels = displayMetrics.widthPixels;
        final int sideLength = (int) (portraitPixels * ICON_SIZE_SCREEN_PERCENTAGE);

        if (drawable == null)
        {
            try
            {
                Bitmap bitmap = bitmapCacheManager.getBitmapFromImageResource(trapTypeIcon, new BitmapCacheManager.BitmapLoadedListener()
                {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap)
                    {
                        fragment.setImageDrawable(new BitmapDrawable(bitmap),sideLength);
                    }
                });
                fragment.setImageDrawable(new BitmapDrawable(bitmap), sideLength);
            }
            catch (BitmapCacheManager.InvalidResourceKeyException e)
            {

            }
        }
        else
        {

            fragment.setImageDrawable(drawable, sideLength);
        }
    }

    protected void showFragment(AbstractSettingFragment fragment)
    {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(container.getId(), fragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public boolean shouldScroll()
    {
        for (AbstractSettingFragment fragment : settingsList)
        {
            if (fragment.wasTouchIntercepted())
                return false;
        }

        return true;
    }

    protected abstract void setupFragments();



}
