package com.trapster.android.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import com.google.inject.Inject;
import com.here.android.mapping.Map;
import com.trapster.android.Defaults;
import com.trapster.android.activity.fragment.BlankMapFragment;
import com.trapster.android.activity.fragment.ReportSpeedLimitFragment;
import com.trapster.android.activity.fragment.SpeedLimitTutorialFragment;
import com.trapster.android.activity.fragment.BlankMapFragment.OnBlankMapReadyListener;
import com.trapster.android.R;
import com.trapster.android.manager.GlobalMapEventManager;
import com.trapster.android.util.GeographicUtils;

import roboguice.inject.ContentView;
import roboguice.inject.InjectFragment;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 12/3/12
 * Time: 1:13 PM
 * Allows the user to report the speed limit of selected route links
 */

@ContentView(R.layout.screen_report_speed_limit)
public class ReportSpeedLimitScreen extends Screen{

    public static final String SPEED_LIMIT_EXTRA = "speed_limit_extra";
    private static final String CLASS_NAME = "ReportSpeedLimit";



    @InjectFragment(R.id.fragmentMap)BlankMapFragment mapFragment;
    @InjectFragment(R.id.fragmentReportSpeedLimit) ReportSpeedLimitFragment speedLimitFragment;
    @InjectFragment(R.id.fragmentReportSpeedLimitTutorial)SpeedLimitTutorialFragment speedLimitTutorialFragment;

    @Inject SharedPreferences sharedPreferences;

    @Inject GlobalMapEventManager globalMapEventManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(R.string.speed_limit_reporting_title);

        double speedLimit = GeographicUtils.convertSpeedToUnits(getIntent().getDoubleExtra(SPEED_LIMIT_EXTRA, -1), sharedPreferences);
        speedLimitFragment.setSpeedLimit(speedLimit);
        // Turn off the traps and patrol
        //mapFragment.hideLayers();

        if(sharedPreferences.getBoolean(Defaults.PREFERENCE_HIDE_SPEED_LIMIT_TUTORIAL, false))
            speedLimitTutorialFragment.hide();
        
        mapFragment.setOnBlankMapReadyListener(new OnBlankMapReadyListener(){

			@Override
			public void onMapReady(Map map) {
				speedLimitFragment.setMap(map);
			}
			
		});


    }

    /**
     * MapController.getMap will return the master map. Report Speed Limit uses a blank map so it will have to be pulled from the
     * blank map fragment
     *
     * @return
     */
    public Map getMap(){
        return mapFragment.getMap();
    }

    @Override
    public void onResume() {
        super.onResume();

        mapFragment.addMapEventListener(globalMapEventManager);
        GlobalMapEventManager.addListener(speedLimitFragment, GlobalMapEventManager.FUNCTION_CALL.MAP_MOVE_END);
    }

    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }

    @Override
    protected void onPause() {
        super.onPause();

        mapFragment.removeMapEventListener(globalMapEventManager);
        GlobalMapEventManager.removeListener(speedLimitFragment);
    }


    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }

}
