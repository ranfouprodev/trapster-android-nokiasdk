package com.trapster.android.activity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.trapster.android.R;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 11/17/12
 * Time: 5:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class GetSatScreen extends Screen {

    private WebView webView;
    private ProgressBar progressBar;
    private final static String CLASS_NAME = "GetSat";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        setContentView(R.layout.screen_webview);

        webView = (WebView) findViewById(R.id.webView);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        webView.getSettings()
                .setUserAgentString(
                        "Mozilla/5.0 (iPhone; CPU iPhone OS 5_0_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A405 Safari/7534.48.3");

        webView.getSettings().setJavaScriptEnabled(true);

        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }

            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {
                // Error Loading it.
                Toast.makeText(
                        getBaseContext(),
                        "Sorry, there was an error loading the page. Visit trapster.com for more help",
                        Toast.LENGTH_SHORT).show();

            }
        });

        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress > 0 && progress < 100)
                    progressBar.setVisibility(View.VISIBLE);
                else
                    progressBar.setVisibility(View.GONE);
            }
        });

        webView.loadUrl("http://m.getsatisfaction.com/trapster");
    }

    public void onStart() {
        super.onStart();

        // @TODO flurry
    }

    public void onStop() {
        super.onStop();

    }

    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }

    @Override
    protected boolean shouldUseActionBar()
    {
        return false;
    }
}
