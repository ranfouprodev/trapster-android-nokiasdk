package com.trapster.android.activity.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import com.google.inject.Inject;
import com.here.android.mapping.MapScheme;
import com.trapster.android.R;
import com.trapster.android.manager.MapController;
import roboguice.inject.InjectView;

/**
 * Created by John on 7/19/13.
 */
public class CenterOnMeFragment  extends AbstractFragment implements MapController.OnMapUpdateListener {

    @Inject
    MapController mapController;


    @InjectView(R.id.buttonCenterOnMe)
    ImageButton centerOnMeButton;

    private int animationIn;
    private int animationOut;
    private boolean visible;

    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            boolean animate = (Boolean) msg.obj;
            internalHide(animate);
        }
    };


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        animationIn = R.anim.slide_in_right;

        animationOut = R.anim.slide_out_left;

        if (savedInstanceState != null && savedInstanceState.containsKey("visibility")) {
            boolean show = savedInstanceState.getBoolean("visibility");
            if(show)
                show(0,false);
            else
                hide(false);
        }

        String mapScheme = mapController.getExpectedMapScheme();

        if(mapScheme.equalsIgnoreCase(MapScheme.NORMAL_NIGHT))
           centerOnMeButton.setImageResource(R.drawable.button_center_on_me_night);

        centerOnMeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mapController.toggleCenterOnMe();
            }
        });

        setRetainInstance(true);

        super.onActivityCreated(savedInstanceState);

    }

    private void checkVisibility() {
        if(mapController.isCenterOnMe())
            hide(false);
        else
            show(0,false);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_center_on_me,null);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("visibility", visible);
    }

    @Override
    public void onResume() {
        super.onResume();

        mapController.addMapUpdateListener(this);

        checkVisibility();
    }

    @Override
    public void onPause() {
        super.onPause();

        handler.removeCallbacksAndMessages(null);

        mapController.removeMapUpdateListener(this);

    }

    public void show(int delay, boolean animate) {
        visible = true;

        FragmentTransaction fragmentTransaction = getActivity()
                .getSupportFragmentManager().beginTransaction();

        if (animate)
            fragmentTransaction.setCustomAnimations(animationIn, animationOut);

        fragmentTransaction.show(this);

        fragmentTransaction.commit();

        if (delay != 0) {

            handler.removeCallbacksAndMessages(null);
            Message msg = handler.obtainMessage();
            msg.obj = animate;
            handler.sendMessageDelayed(msg,delay);
        }

    }

    public void hide(boolean animate) {

        handler.removeCallbacksAndMessages(null);

        Message msg = handler.obtainMessage();
        msg.obj = animate;
        handler.sendMessage(msg);


    }

    private void internalHide(boolean animate) {

        visible = false;
        FragmentTransaction fragmentTransaction = getActivity()
                .getSupportFragmentManager().beginTransaction();

        if (animate)
            fragmentTransaction.setCustomAnimations(animationOut, animationIn);

        fragmentTransaction.hide(this);

        fragmentTransaction.commit();


    }

    @Override
    public void onCenterOnMeChanged(boolean centerOnMe) {
        if(centerOnMe)
            hide(false);
        else
            show(0,true);

    }
}