package com.trapster.android.activity.settings;

import android.content.Intent;
import android.view.View;
import com.google.inject.Inject;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.activity.settings.fragment.MultiModalSlideBarFragment;
import com.trapster.android.activity.settings.fragment.SubTextSettingFragment;
import com.trapster.android.manager.TrapManager;
import com.trapster.android.model.TrapType;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class TrapsAndAlertsSettingsScreen extends AbstractSettingsScreen
{



    private final static String CLASS_NAME = "Traps and Alerts Screen";
    @Inject TrapManager trapManager;

    private List<TrapType> trapTypes;
    private HashMap<TrapType, SubTextSettingFragment> subTextMap;

    private boolean useGlobalValues = false;
    private boolean globalVisible = false;
    private boolean globalAudible = false;
    private boolean globalVibrate = false;

    @Override
    protected void setupFragments()
    {
        trapTypes = trapManager.getSortedTrapTypes();

        MultiModalSlideBarFragment slideBarFragment = (MultiModalSlideBarFragment) createSettingsFragment(FRAGMENT_TYPE.MULTI_MODAL_SLIDE_BAR);
        slideBarFragment.setDescription(R.string.trap_alert_notification_slide_bar_title);
        slideBarFragment.setOptionText(R.string.trap_alert_notification_slide_bar_visual, R.string.trap_alert_notification_slide_bar_visual_audible_vibrate, R.string.trap_alert_notification_slide_bar_visual_audible);

        subTextMap = new HashMap<TrapType, SubTextSettingFragment>();
        for (TrapType trapType : trapTypes)
        {
            SubTextSettingFragment fragment = (SubTextSettingFragment) createSettingsFragment(FRAGMENT_TYPE.SUB_TEXT);
            fragment.setDescription(trapType.getName());
            fragment.setSubText(createSelectedSettingsString(trapType));
            try
            {
                setupIcon(fragment, trapType);
            }
            catch (IndexOutOfBoundsException e){}


            fragment.setOnClickListener(new SpecificTrapTypeLauncher(trapType));

            subTextMap.put(trapType, fragment);
        }

        slideBarFragment.setSelectionChangedListener(new MultiModalSlideBarFragment.OnSliderSelectionChangedListener()
        {
            @Override
            public void onSliderSelectionChanged(MultiModalSlideBarFragment.SLIDER_SELECTION selection)
            {

                editor.putInt(Defaults.PREFERENCE_ALERTS_GLOBAL_SETTINGS, selection.ordinal());
                useGlobalValues = true;
                Iterator<TrapType> i = subTextMap.keySet().iterator();
                while (i.hasNext())
                {
                    TrapType type = i.next();
                    switch (selection)
                    {
                        case RIGHT:
                        {
                            editor.putBoolean(type.getAudioid() + Defaults.SETTING_VISIBLE, true);
                            editor.putBoolean(type.getAudioid() + Defaults.SETTING_AUDIBLE, true);
                            editor.putBoolean(type.getAudioid() + Defaults.SETTING_VIBRATE, true);

                            globalVisible = true;
                            globalAudible = true;
                            globalVibrate = true;


                            break;
                        }
                        case CENTER:
                        {
                            editor.putBoolean(type.getAudioid() + Defaults.SETTING_VISIBLE, true);
                            editor.putBoolean(type.getAudioid() + Defaults.SETTING_AUDIBLE, true);
                            editor.putBoolean(type.getAudioid() + Defaults.SETTING_VIBRATE, false);

                            globalVisible = true;
                            globalAudible = true;
                            globalVibrate = false;
                            break;
                        }
                        case LEFT:
                        {
                            editor.putBoolean(type.getAudioid() + Defaults.SETTING_VISIBLE, true);
                            editor.putBoolean(type.getAudioid() + Defaults.SETTING_AUDIBLE, false);
                            editor.putBoolean(type.getAudioid() + Defaults.SETTING_VIBRATE, false);

                            globalVisible = true;
                            globalAudible = false;
                            globalVibrate = false;
                            break;
                        }
                    }

                    SubTextSettingFragment fragment = subTextMap.get(type);
                    fragment.setSubText(createSelectedSettingsString(type));
                }


            }

        });

        int selection = sharedPreferences.getInt(Defaults.PREFERENCE_ALERTS_GLOBAL_SETTINGS, MultiModalSlideBarFragment.SLIDER_SELECTION.RIGHT.ordinal());
        slideBarFragment.setStartSelection(MultiModalSlideBarFragment.SLIDER_SELECTION.values()[selection]);


    }

    @Override
    public void onResume()
    {
        for (TrapType trapType : trapTypes)
        {
            SubTextSettingFragment fragment = subTextMap.get(trapType);
            fragment.setSubText(createSelectedSettingsString(trapType));
        }
        super.onResume();
    }

    private String createSelectedSettingsString(TrapType type)
    {
        StringBuilder sb = new StringBuilder();

        boolean usesAlerts = sharedPreferences.getBoolean(type.getAudioid() + Defaults.SETTING_ALERTING, true);
        if (usesAlerts)
        {
            // Is the trap on (not disabled in settings)
            boolean visible = sharedPreferences.getBoolean(type.getAudioid() + Defaults.SETTING_VISIBLE, true);
            // Does the trap allow audible?
            boolean audible = sharedPreferences.getBoolean(type.getAudioid() + Defaults.SETTING_AUDIBLE,true);
            // Does the trap allow vibrate?
            boolean vibrate = sharedPreferences.getBoolean(type.getAudioid() + Defaults.SETTING_VIBRATE,true);

            if (useGlobalValues)
            {
                vibrate = globalVibrate;
                audible = globalAudible;
                visible = globalVisible;
            }

            if (visible)
                sb.append("Visual");
            if (vibrate)
            {
                if (sb.length() > 0)
                {
                    sb.append(" + " + "Vibrate");
                }
            }

            if (audible)
            {
                if (sb.length() > 0)
                {
                    sb.append(" + " + "Audible Alert");
                }
            }
        }
        else
            sb.append("Disabled");

        return sb.toString();
    }

    private class SpecificTrapTypeLauncher implements View.OnClickListener
    {
        private TrapType trapType;

        public SpecificTrapTypeLauncher(TrapType trapType)
        {
            this.trapType = trapType;
        }

        @Override
        public void onClick(View view)
        {
            Intent i = new Intent(TrapsAndAlertsSettingsScreen.this, SpecificTrapSettingsScreen.class);
            i.putExtra(SpecificTrapSettingsScreen.TRAP_TYPE, trapType.getId());
            startActivity(i);
            useGlobalValues = false;
        }
    }

    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }
}
