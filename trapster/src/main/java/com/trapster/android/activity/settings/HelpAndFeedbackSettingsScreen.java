package com.trapster.android.activity.settings;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.activity.*;
import com.trapster.android.activity.settings.fragment.TextSettingFragment;
import com.trapster.android.manager.SessionManager;
import com.trapster.android.model.Profile;

import javax.inject.Inject;

public class HelpAndFeedbackSettingsScreen extends AbstractSettingsScreen
{
    private static final long RATE_US_MINIMUM_INTERVAL = 1000 * 60 * 60 * 24;// 24 hours
    @Inject SessionManager sessionManager;


    @Override
    protected void setupFragments()
    {
        TextSettingFragment tutorialsFragment = (TextSettingFragment) createSettingsFragment(FRAGMENT_TYPE.TEXT_ONLY, TutorialsSettingsScreen.class);
        TextSettingFragment supportFragment = (TextSettingFragment) createSettingsFragment(FRAGMENT_TYPE.TEXT_ONLY, GetSatScreen.class);
        TextSettingFragment feedbackFragment = (TextSettingFragment) createSettingsFragment(FRAGMENT_TYPE.TEXT_ONLY);
        TextSettingFragment rateUsFragment = (TextSettingFragment) createSettingsFragment(FRAGMENT_TYPE.TEXT_ONLY);
        TextSettingFragment aboutFragment = (TextSettingFragment) createSettingsFragment(FRAGMENT_TYPE.TEXT_ONLY, AboutScreen.class);
        TextSettingFragment aboutHereFragment = (TextSettingFragment) createSettingsFragment(FRAGMENT_TYPE.TEXT_ONLY, AboutHereScreen.class);
        if (eligibleToBecomeAMod())
        {
            TextSettingFragment becomeModFragment = (TextSettingFragment) createSettingsFragment(FRAGMENT_TYPE.TEXT_ONLY, BecomeAModeratorScreen.class);
            becomeModFragment.setDescription(R.string.become_a_mod_title);
        }

        tutorialsFragment.setDescription(R.string.help_feedback_tutorials);
        supportFragment.setDescription(R.string.help_feedback_get_sat);
        feedbackFragment.setDescription(R.string.help_feedback_nps);
        rateUsFragment.setDescription(R.string.help_feedback_play_store);
        aboutFragment.setDescription(R.string.dash_menu_about);
        aboutHereFragment.setDescription(R.string.dash_menu_about_here);

        feedbackFragment.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                long lastRateTime = sharedPreferences.getLong(Defaults.NPS_LAST_RATING_TIME, 0L);
                long currentTime = System.currentTimeMillis();
                if (lastRateTime + RATE_US_MINIMUM_INTERVAL < currentTime)
                {
                    startActivity(new Intent(getApplication(), NpsScreen.class));
                } else
                {
                    showToastMessage(getString(R.string.nps_timeout_popup));
                }
            }
        });

        rateUsFragment.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.trapster.android");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
    }

    @Override
    protected String getClassName()
    {
        return null;
    }

    private boolean eligibleToBecomeAMod()
    {
        if (sessionManager.isLoggedIn())
        {
            Profile profile = sessionManager.getUser().getProfile();
            if (profile != null && profile.getGlobalModerator() == 0) // -1 indicates they've already made a request, any other value would be lost if the request is submitted
            {
                return true;
            }
        }

        return false;
    }
}
