package com.trapster.android.activity.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.google.inject.Inject;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import roboguice.inject.InjectView;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 12/9/12
 * Time: 1:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class SpeedLimitTutorialFragment extends AbstractFragment {


    @InjectView (R.id.buttonOkSpeedLimitTutorial) Button tutorialOkButton;
    @InjectView (R.id.speedLimitTutorialContainer)RelativeLayout speedLimitTutorialContainer;

    @Inject SharedPreferences sharedPreferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_speed_limit_tutorial, null);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        tutorialOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                speedLimitTutorialContainer.setVisibility(View.GONE);
                sharedPreferences.edit().putBoolean(Defaults.PREFERENCE_HIDE_SPEED_LIMIT_TUTORIAL, true).commit();
            }
        });



    }

}
