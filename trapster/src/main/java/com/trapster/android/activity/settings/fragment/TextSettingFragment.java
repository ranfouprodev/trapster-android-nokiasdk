package com.trapster.android.activity.settings.fragment;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.trapster.android.R;
import roboguice.inject.InjectView;

public class TextSettingFragment extends AbstractSettingFragment
{
    @InjectView(R.id.settingsTextDescription) TextView description;
    @InjectView(R.id.settingsContainer) LinearLayout container;

    private String textDescription;
    private View.OnClickListener listener;
    private int descriptionResource;

    @Override
    protected void init()
    {
        if (descriptionResource > 0)
            textDescription = getString(descriptionResource);
        description.setText(textDescription);
        container.setOnClickListener(listener);
    }

    @Override
    protected int getLayoutResource()
    {
        return R.layout.fragment_settings_text;
    }

    public void setDescription(int description)
    {
        if (isAdded())
            setDescription(getString(description));
        descriptionResource = description;
    }

    public void setDescription(String description)
    {
        if (this.description != null)
            this.description.setText(description);
        this.textDescription = description;
    }

    @Override
    public void setOnClickListener(View.OnClickListener listener)
    {
        if (container != null)
            container.setOnClickListener(listener);
        this.listener = listener;
    }
}
