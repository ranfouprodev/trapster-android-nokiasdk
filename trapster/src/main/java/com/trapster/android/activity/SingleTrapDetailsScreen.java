package com.trapster.android.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import com.google.inject.Inject;
import com.here.android.common.GeoCoordinate;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.activity.fragment.TrapMapLayer;
import com.trapster.android.activity.fragment.component.TrapMapImageView;
import com.trapster.android.comms.ITrapsterAPI;
import com.trapster.android.manager.FlurryManager;
import com.trapster.android.manager.PositionManager;
import com.trapster.android.manager.SessionManager;
import com.trapster.android.manager.TrapManager;
import com.trapster.android.manager.flurry.FLURRY_EVENT_NAME;
import com.trapster.android.manager.flurry.FLURRY_PARAMETER_NAME;
import com.trapster.android.manager.flurry.FlurryEvent;
import com.trapster.android.model.Trap;
import com.trapster.android.model.TrapType;
import com.trapster.android.model.dao.TrapDAO;
import com.trapster.android.service.TrapAddOrUpdateService;
import com.trapster.android.util.GeographicUtils;
import com.trapster.android.util.PrettyTime;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

import java.security.InvalidParameterException;
import java.text.DecimalFormat;
import java.util.Date;

@ContentView(R.layout.screen_single_trap_details)
public class SingleTrapDetailsScreen extends Screen implements CommunicationStatusListener
{
    private final static String CLASS_NAME = "SingleTrapDetails";
    public final static String TRAP_BUNDLE_KEY = "trapBundleKey";

    private Trap currentTrap;

    @Inject TrapManager trapManager;
    @Inject SessionManager sessionManager;
    @Inject ITrapsterAPI api;
    @Inject PositionManager positionManager;
    @Inject SharedPreferences sharedPreferences;
    @Inject TrapDAO trapDAO;
    @Inject TrapMapLayer trapMapLayer;

    @InjectView (R.id.singleTrapDetailsButtonContainer) LinearLayout buttonContainer;
    @InjectView (R.id.singleTrapDetailsAgreeButton) ImageButton agreeButton;
    @InjectView (R.id.singleTrapDetailsDisagreeButton) ImageButton disagreeButton;

    @InjectView (R.id.singleTrapDetailsIgnoreButton) Button ignoreButton;
    @InjectView (R.id.singleTrapDetailsDeleteButton) Button deleteButton;
    @InjectView (R.id.singleTrapDetailsShareButton) ImageButton shareButton;

    @InjectView (R.id.singleTrapDetailsSnapshot) TrapMapImageView mapSnapshot;
    @InjectView (R.id.singleTrapDetailsDistanceText)TextView distanceText;
    @InjectView (R.id.singleTrapDetailsLocationText)TextView locationText;
    @InjectView (R.id.singleTrapDetailsInfoTextOriginalReport)TextView originalReportText;
    @InjectView (R.id.singleTrapDetailsInfoTextLastRatedBy)TextView lastRatedText;
    @InjectView (R.id.singleTrapDetailsInfoTextReportedTime)TextView lastVotedTimeText;
    @InjectView (R.id.singleTrapDetailsInfoExpireTime)TextView expireTime;

    @InjectView (R.id.singleTrapDetailsOverlay) ImageView overlayView;



    private final static double KILOMETERS_PER_MILE = 0.62137;
    private static final double DEFAULT_ZOOM_LEVEL = 16;

    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Intent callingIntent = getIntent();

        if (callingIntent.hasExtra(TRAP_BUNDLE_KEY)&& callingIntent.getParcelableExtra(TRAP_BUNDLE_KEY) != null)
            currentTrap = callingIntent.getParcelableExtra(TRAP_BUNDLE_KEY);
        else
            throw new InvalidParameterException("SingleTrapDetailScreen REQUIRES a trap be passed by the intent that starts it!");

        if (sessionManager.isLoggedIn())
        {
            agreeButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    sendVote(true);
                }
            });

            disagreeButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    sendVote(false);
                }
            });
        }
        else
        {
            buttonContainer.setVisibility(View.GONE);
        }

        this.setTitle(trapManager.getTrapType(currentTrap.getTypeid()).getName());

        deleteButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                confirmTrapDelete();
            }
        });
        shareButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dispatchShareIntent();
                sendFlurryShareTrap();
            }
        });
        setupSnapshot();
        setupTrapDetails();
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    private void setupSnapshot()
    {
        mapSnapshot.setTrap(currentTrap);
    }



    private void setupTrapDetails()
    {
    	
    	
        if (sessionManager.isLoggedIn())
        {            	
        	if ((sessionManager.getUser().getUserName().equalsIgnoreCase(currentTrap.getOuname()) ) || (sessionManager.getUser().getProfile().getGlobalModerator() > 0))
                deleteButton.setVisibility(View.VISIBLE);
            else
                deleteButton.setVisibility(View.GONE);
        }
        else
            deleteButton.setVisibility(View.GONE);

        distanceText.setText(getString(R.string.single_trap_distance_text, getTextDistance()));
        locationText.setText(currentTrap.getAddress());
        originalReportText.setText(getString(R.string.single_trap_original_report_text, currentTrap.getOuname()));
        lastRatedText.setText(getString(R.string.single_trap_last_rated_by_text, currentTrap.getLuname()));
        lastVotedTimeText.setText(getString(R.string.single_trap_last_voted_text, PrettyTime.formatRelativeTime(getResources(), new Date(currentTrap.getLvoteSec() * 1000))));

        TrapType trapType = trapManager.getTrapType(currentTrap.getTypeid());
        if (trapType.getLifetime() > 0)
        {
            // Figure out how long until this trap expires.
            // Per Matt's instructions, we don't worry about this text once the trap is expired since the
            // trap gets removed from the map.
            long expiry = (currentTrap.getLvoteSec() * 1000) + (trapType.getLifetime() * 1000);
            Date now = new Date();
            long deltaTime = (expiry - now.getTime())/1000;
            expireTime.setText(getString(R.string.single_trap_expire_true_text, PrettyTime.formatRelativeTime(getResources(), deltaTime)));
        }
        else
        {
            expireTime.setText(getString(R.string.single_trap_expire_false_text));
        }

    }

    private String getTextDistance()
    {
        String distanceText;
        GeoCoordinate coordinate = positionManager.getLastLocation().getCoordinate();

        double distanceKilometers = GeographicUtils.geographicDistance(coordinate.getLatitude(), coordinate.getLongitude(), currentTrap.getLatitude(), currentTrap.getLongitude()) / 1000;

        DecimalFormat distanceFormat = new DecimalFormat("#0.00");

        if (sharedPreferences.getBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_MILES, true))
        {
            double distanceMiles = distanceKilometers * KILOMETERS_PER_MILE;
            distanceText = distanceFormat.format(distanceMiles) + " " + getString(R.string.alert_sensitivity_units_miles);
        }
        else
            distanceText = distanceFormat.format(distanceKilometers) + " " + getString(R.string.alert_sensitivity_units_km);


        return distanceText;
    }

    private void sendVote(boolean whichVote)
    {
        showToastMessage(getString(R.string.single_trap_message_vote_received));
        //Toast.makeText(getApplicationContext(), , Toast.LENGTH_SHORT).show();
        sendFlurryTrapVote(whichVote, currentTrap);

       // @TODO shouldn't we check to make sure the trap is successful
        api.rateTrap(whichVote, currentTrap,null, null);
        returnToMap();
    }

    private void confirmTrapDelete()
    {
        String rawDescription = getString(R.string.single_trap_popup_description_delete);
        View.OnClickListener postiveListener = new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hideDialog();
                disableOverlay();
                deleteTrap();
            }
        };
        View.OnClickListener negativeListener = new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hideDialog();
                disableOverlay();
            }
        };


        enableOverlay();


        showDialog(null, rawDescription, getString(R.string.single_trap_popup_button_delete), getString(R.string.cancel_button), postiveListener, negativeListener, 0);
    }

    private void deleteTrap()
    {
       /* api.deleteTrap(currentTrap, this);
        disableOverlay();
        trapDAO.delete(currentTrap);
       
        trapMapLayer.forceUpdate(currentTrap.getLatitude(), currentTrap.getLongitude(), 1);*/
    	Intent intent = new Intent(getApplicationContext(), TrapAddOrUpdateService.class);
		intent.putExtra(TrapAddOrUpdateService.INTENT_EXTRA_DELETE_TRAP, currentTrap);
		startService(intent);
    	
    	
        showToastMessage(getString(R.string.single_trap_delete_confirmation));

        returnToMap();
        finish();
    }

    private void enableOverlay()
    {
        // Show the overlay, disable all buttons
        overlayView.setVisibility(View.VISIBLE);

        agreeButton.setClickable(false);
        disagreeButton.setClickable(false);
        shareButton.setClickable(false);
        ignoreButton.setClickable(false);
        deleteButton.setClickable(false);
    }

    private void disableOverlay()
    {
        // Hide th overlay, enable all buttons
        overlayView.setVisibility(View.GONE);

        agreeButton.setClickable(true);
        disagreeButton.setClickable(true);
        shareButton.setClickable(true);
        ignoreButton.setClickable(true);
        deleteButton.setClickable(true);
    }

    private void dispatchShareIntent()
    {

        String title = getString(R.string.single_trap_share_title);

        String trapName = trapManager.getTrapType(currentTrap.getTypeid()).getName();
        String trapAddress = currentTrap.getAddress();

        String url = "http://trap.st/" + Long.toString(currentTrap.getId(), 36);  // Moves into base-36

        String description = getString(R.string.single_trap_share_description, trapName, trapAddress, url);

        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, title);
        shareIntent.putExtra(Intent.EXTRA_TEXT, description);


        startActivityForResult(Intent.createChooser(shareIntent, getString(R.string.single_trap_share_chooser_title)), 0);
    }

    private void sendFlurryTrapVote(boolean whichVote, Trap trap)
    {
        FlurryEvent event;
        if (whichVote)
            event = FlurryManager.createEvent(FLURRY_EVENT_NAME.TRAP_VOTE_AGREE);
        else
            event = FlurryManager.createEvent(FLURRY_EVENT_NAME.TRAP_VOTE_DISAGREE);

        String trapName = trapManager.getTrapType(trap.getTypeid()).getName();
        String englishName = trapManager.getTrapType(trap.getTypeid()).getUnlocalizedName();

        event.putParameter(FLURRY_PARAMETER_NAME.TRAP_TYPE, englishName);
        event.putParameter(FLURRY_PARAMETER_NAME.TRAP_TYPE_LANGUAGE, trapName);
        event.putParameter(FLURRY_PARAMETER_NAME.LOCATION_COUNTRY, FlurryManager.getCountryCode());
        event.putParameter(FLURRY_PARAMETER_NAME.LOCATION_STATE, FlurryManager.getStateCode());
        event.putParameter(FLURRY_PARAMETER_NAME.DRIVING_SPEED, FlurryManager.getDrivingSpeed());
        event.putParameter(FLURRY_PARAMETER_NAME.VOTING_SCREEN, getClassName());

        event.send();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void sendFlurryShareTrap()
    {
        FlurryEvent event = FlurryManager.createEvent(FLURRY_EVENT_NAME.SOCIAL_SHARE_SINGLE_TRAP);

        String trapName = trapManager.getTrapType(currentTrap.getTypeid()).getName();
        String englishName = trapManager.getTrapType(currentTrap.getTypeid()).getUnlocalizedName();

        event.putParameter(FLURRY_PARAMETER_NAME.TRAP_TYPE, englishName);
        event.putParameter(FLURRY_PARAMETER_NAME.TRAP_TYPE_LANGUAGE, trapName);
        event.putParameter(FLURRY_PARAMETER_NAME.LOCATION_COUNTRY, FlurryManager.getCountryCode());
        event.putParameter(FLURRY_PARAMETER_NAME.LOCATION_STATE, FlurryManager.getStateCode());
        event.putParameter(FLURRY_PARAMETER_NAME.DRIVING_SPEED, FlurryManager.getDrivingSpeed());

        event.send();
    }

	@Override
	public void onOpenConnection() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCloseConnection() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnectionError(String errorMessage) {
		showToastMessage(errorMessage);
	}
}
