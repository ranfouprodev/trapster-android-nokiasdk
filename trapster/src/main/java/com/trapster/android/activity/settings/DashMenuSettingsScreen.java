package com.trapster.android.activity.settings;

import android.content.Intent;
import android.view.View;
import com.google.inject.Inject;
import com.trapster.android.R;
import com.trapster.android.activity.LoginScreen;
import com.trapster.android.activity.MapScreen;
import com.trapster.android.activity.NearbyTrapsScreen;
import com.trapster.android.activity.StatsScreen;
import com.trapster.android.activity.settings.fragment.IconSettingFragment;
import com.trapster.android.manager.SessionManager;

public class DashMenuSettingsScreen extends AbstractSettingsScreen
{
    @Inject SessionManager sessionManager;
    private final static String CLASS_NAME = "MainMenu";
    IconSettingFragment profileFragment;

    @Override
    protected void setupFragments()
    {
        setupProfileFragment();
        IconSettingFragment settingsFragment = ((IconSettingFragment)createSettingsFragment(FRAGMENT_TYPE.ICON, SettingsScreen.class));
        IconSettingFragment mapOptionsFragment = ((IconSettingFragment)createSettingsFragment(FRAGMENT_TYPE.ICON, MapOptionsSettingsScreen.class));
        IconSettingFragment nearbyTrapsFragment = ((IconSettingFragment)createSettingsFragment(FRAGMENT_TYPE.ICON, NearbyTrapsScreen.class));
        IconSettingFragment helpFragment = ((IconSettingFragment)createSettingsFragment(FRAGMENT_TYPE.ICON, HelpAndFeedbackSettingsScreen.class));
        IconSettingFragment quitFragment = ((IconSettingFragment)createSettingsFragment(FRAGMENT_TYPE.ICON, HelpAndFeedbackSettingsScreen.class));

        settingsFragment.setImageResource(R.drawable.settings);
        mapOptionsFragment.setImageResource(R.drawable.map_options);
        nearbyTrapsFragment.setImageResource(R.drawable.nearby_traps);
        helpFragment.setImageResource(R.drawable.help);
        quitFragment.setImageResource(R.drawable.quit);

        settingsFragment.setDescription(R.string.dash_menu_settings);
        mapOptionsFragment.setDescription(R.string.settings_menu_map_options);
        nearbyTrapsFragment.setDescription(R.string.dash_menu_nearby_traps);
        helpFragment.setDescription(R.string.dash_menu_help);
        quitFragment.setDescription(R.string.dash_menu_quit);

        quitFragment.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                setResult(MapScreen.RESPONSE_CODE_QUIT_APPLICATION);
                finish();
            }
        });
    }

    @Override
    public void onResume()
    {
        updateProfileFragment();
        super.onResume();
    }

    private void updateProfileFragment()
    {
        if (sessionManager.getUser() != null && sessionManager.getUser().getProfile() != null )
        {
            profileFragment.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    Intent i = new Intent(DashMenuSettingsScreen.this, StatsScreen.class);
                    startActivity(i);
                }
            });
            profileFragment.setDescription(R.string.dash_menu_profile);
            profileFragment.setImageResource(R.drawable.profile);
        }
        else
        {
            profileFragment.setDescription(R.string.login_or_signup);
            profileFragment.setImageResource(R.drawable.profile);
            profileFragment.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    Intent intent = new Intent(DashMenuSettingsScreen.this, LoginScreen.class);
                    intent.putExtra(LoginScreen.CLASS_EXTRA, StatsScreen.class);
                    startActivity(intent);
                }
            });

        }
    }

    private void setupProfileFragment()
    {
        if (sessionManager.getUser() != null && sessionManager.getUser().getProfile() != null )
        {
            profileFragment = (IconSettingFragment) createSettingsFragment(FRAGMENT_TYPE.ICON, StatsScreen.class);
            profileFragment.setDescription(R.string.dash_menu_profile);
            profileFragment.setImageResource(R.drawable.profile);
        }
        else
        {
            profileFragment = (IconSettingFragment) createSettingsFragment(FRAGMENT_TYPE.ICON);
            profileFragment.setDescription(R.string.login_or_signup);
            profileFragment.setImageResource(R.drawable.profile);
            profileFragment.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    Intent intent = new Intent(DashMenuSettingsScreen.this, LoginScreen.class);
                    intent.putExtra(LoginScreen.CLASS_EXTRA, StatsScreen.class);
                    startActivity(intent);
                }
            });

        }
    }

    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }
}
