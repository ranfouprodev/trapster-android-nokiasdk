
package com.trapster.android.activity.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.LinearLayout;
import com.trapster.android.R;
import roboguice.inject.InjectView;

public class BottomBarFragment extends AbstractFragment
{
    @InjectView(R.id.bottomBarNegativeButton) Button negativeButton;
    @InjectView(R.id.bottomBarPositiveButton) Button positiveButton;
    @InjectView(R.id.bottomBarLayout) LinearLayout layout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_bottom_bar, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

    }

    public void disableBackground()
    {
        layout.setBackgroundDrawable(null);
    }

    public void setNegativeAction(String buttonText, View.OnClickListener listener)
    {
        negativeButton.setText(buttonText);
        if (listener != null)
            negativeButton.setOnClickListener(listener);
        else
            negativeButton.setVisibility(View.GONE);
            //throw new NullPointerException("Cannot set null listener to negative button.");
    }

    public void setPositiveAction(String buttonText, View.OnClickListener listener)
    {
        positiveButton.setText(buttonText);
        if (listener != null)
            positiveButton.setOnClickListener(listener);
        else
            positiveButton.setVisibility(View.GONE);
            //throw new NullPointerException("Cannot set null listener to positive button.");
    }

    public void disable()
    {
        positiveButton.setClickable(false);
        positiveButton.setFocusable(false);

        negativeButton.setClickable(false);
        negativeButton.setFocusable(false);


    }

    public void disablePositiveButton()
    {
        positiveButton.setTextColor(Color.GRAY);
        positiveButton.setClickable(false);
        positiveButton.setFocusable(false);
    }

    public void enable()
    {
        positiveButton.setTextColor(Color.BLACK);
        positiveButton.setClickable(true);
        positiveButton.setFocusable(true);

        negativeButton.setClickable(true);
        negativeButton.setFocusable(true);


    }

    public void playAnimation(Animation animation)
    {
        layout.startAnimation(animation);
    }
}
