package com.trapster.android.activity.component.listener;

import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.trapster.android.Defaults;
import com.trapster.android.comms.AttributesListener;
import com.trapster.android.model.StringResource;
import com.trapster.android.model.TrapsterResources;
import com.trapster.android.model.dao.StringResourceDAO;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;

public class ReloadAttributesListener implements AttributesListener, CommunicationStatusListener
{

    private static final String LOGNAME = "ReloadAttributesListener";
    public static final int UPDATED_TERMS = 1;
    private SharedPreferences sharedPreferences;
    private Handler handler;
    private StringResourceDAO stringResourceDAO;

    public ReloadAttributesListener(SharedPreferences sharedPreferences, Handler handler, StringResourceDAO stringResourceDAO)
    {
        this.sharedPreferences = sharedPreferences;
        this.handler = handler;
        this.stringResourceDAO = stringResourceDAO;
    }

    @Override
    public void onError(TrapsterError error)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void onOpenConnection()
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void onCloseConnection()
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void onConnectionError(String errorMessage)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void onAttributes(TrapsterResources resources)
    {
        for (StringResource resource : resources.getStrings())
        {

            if (resource.getKey().equalsIgnoreCase(Defaults.ASSET_TERMSANDCONDITIONS))
            {
                stringResourceDAO.save(resource);

                ////Log,.v(LOGNAME, "New Terms:" + resource.getText());

                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putLong(Defaults.ASSET_TERMSANDCONDITIONS_NEWEST_DATE, System.currentTimeMillis());
                edit.commit();

                if (handler != null)
                {
                    Message message = new Message();
                    message.what = UPDATED_TERMS;
                    message.obj = resource.getText();

                    handler.sendMessage(message);
                }

            }

        }
    }

}