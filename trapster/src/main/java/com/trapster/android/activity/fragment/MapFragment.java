package com.trapster.android.activity.fragment;

import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.graphics.PointF;
import android.os.Handler;
import android.view.WindowManager;
import com.google.inject.Inject;
import com.here.android.common.*;
import com.here.android.mapping.Map;
import com.here.android.mapping.MapAnimation;
import com.here.android.mapping.MapRenderListener;
import com.trapster.android.Defaults;
import com.trapster.android.activity.fragment.util.MapMotionDetector;
import com.trapster.android.manager.CompositePositionIndicatorManager;
import com.trapster.android.manager.GlobalMapEventManager;
import com.trapster.android.manager.MapController;
import com.trapster.android.manager.PositionManager;
import com.trapster.android.model.provider.PatrolPathProvider;
import com.trapster.android.model.provider.TrapProvider;


public class MapFragment extends AbstractMapFragment
{
    private double MINIMUM_SPEED_FOR_UPDATE = 2.2352;
    private static final double RADIUS_TO_FIRST_UPDATE_TRAPS_IN_MILES = 5;
    private static final double MIN_SPEED_TO_SET_ORIENTATION_IN_KPH = 5;
    private static final long GPS_LISTENER_DELAY = 3000;

    @Inject WindowManager windowManager;
    @Inject PositionManager positionManager;
    @Inject MapController mapController;
    @Inject SharedPreferences sharedPreferences;

    @Inject TrapMapLayer trapLayer;
    @Inject PatrolMapLayer patrolMapLayer;
    @Inject LinkMatchingMapLayer linkMatchingMapLayer;

    @Inject CompositePositionIndicatorManager positionIndicatorManager;

    private final CenterOnMeListener centerOnMeListener = new CenterOnMeListener();
    private final PositionUpdateListener positionUpdateListener = new PositionUpdateListener();
    private final TrapUpdateObserver trapObserver = new TrapUpdateObserver();
    private final PatrolUpdateObserver patrolObserver = new PatrolUpdateObserver();
    private final MapMovementDetector movementDetector = new MapMovementDetector();
    private final MapTiltDetector tiltDetector = new MapTiltDetector();

    private Runnable onMapLoadedRunnable;
    private boolean isCenterOnMe;

    @Override
    protected void restoreMap(MapConfig mapConfig)
    {
        mapConfig.restoreMap(map);
        boolean display3d = sharedPreferences.getBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_3D, true);

        if (!display3d)
            map.setTilt(0);
        mapController.updateMapScheme(map);
    }

    @Override
    protected void onMapCreated(Map map)
    {
        this.map = map;

        setupMapLayers();

        positionIndicatorManager.attach(this);

        mapController.addMapUpdateListener(centerOnMeListener);
        mapController.addMapUpdateListener(positionIndicatorManager.getAnimationListener());
        mapController.setCenterOnMe(mapController.isCenterOnMe()); // Update the center after rotation

        // Add a few second delay to improve the startup experience, processing GPS updates/setCenter() on the map is a heavy task.
        // Doing it at the same time as all the startup stuff makes for a very ugly experience
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                positionManager.addPositionListener(positionUpdateListener);
                positionManager.addPositionListener(positionIndicatorManager);
            }
        }, GPS_LISTENER_DELAY);

        GlobalMapEventManager.addListener(movementDetector);
        GlobalMapEventManager.addListener(positionIndicatorManager.getAnimationListener());



        GlobalMapEventManager.addListener(trapLayer);
        trapLayer.attach(map);
        patrolMapLayer.attach(map);
        linkMatchingMapLayer.attach(map);

        addMapEventListener(tiltDetector);
        addRenderListener(new MapRenderListener()
        {
            @Override
            public void onDrawEnd(boolean b, long l)
            {
               // Log.i("BBQ" , "Rendering took: " + l);
            }

            @Override
            public void onSizeChanged(int i, int i2)
            {
            }
        });

        if (onMapLoadedRunnable != null)
            onMapLoadedRunnable.run();

    }

    @Override
    protected void setupDefaultValues()
    {
        GeoPosition lastLocation = positionManager.getLastLocation();

        if(lastLocation.getCoordinate().isValid() && lastLocation.getCoordinate().getLatitude() != 0)
            map.setCenter(lastLocation.getCoordinate(),MapAnimation.NONE);

        mapController.updateDayMapScheme(map);

        mapController.setDefaultTilt(map);

        if (!sharedPreferences.getBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_FOLLOW, true))
            map.setOrientation(Defaults.MAP_ORIENTATION_DEFAULT);
    }

    private void setupMapLayers()
    {
        //Set the traffic map layer
        ((com.here.android.restricted.mapping.Map)map).setTrafficInfoVisible(sharedPreferences.getBoolean(Defaults.PREFERENCE_MAP_DISPLAY_SHOW_TRAFFIC, true));
        ((com.here.android.restricted.mapping.Map)map).setMapEmbeddedPOIsVisible(false);

        // Sets the 3D buildings visible
        ((com.here.android.restricted.mapping.Map)map).setLandmarksVisible(true);

        //set Patrol lines Layer
        if(sharedPreferences.getBoolean(Defaults.PREFERENCE_MAP_DISPLAY_SHOW_PATROL, true))
            patrolMapLayer.show();
        else
            patrolMapLayer.hide();

        GeoCoordinate centerMap = map.getCenter();
        trapLayer.forceUpdate(centerMap.getLatitude(), centerMap.getLongitude(), RADIUS_TO_FIRST_UPDATE_TRAPS_IN_MILES);
        patrolMapLayer.forceUpdate(centerMap.getLatitude(), centerMap.getLongitude(), RADIUS_TO_FIRST_UPDATE_TRAPS_IN_MILES);

        setupContentObservers();
    }

    private void setupContentObservers()
    {
        // Listen for changes in the trap database
        getParentActivity().getContentResolver().registerContentObserver(TrapProvider.CONTENT_URI, true, trapObserver);
        getParentActivity().getContentResolver().registerContentObserver(PatrolPathProvider.PATH_CONTENT_URI, true, patrolObserver);
    }

    @Override
    public void onPause()
    {
        mapController.removeMapUpdateListener(positionIndicatorManager.getAnimationListener());
        mapController.removeMapUpdateListener(centerOnMeListener);
        positionManager.removePositionListener(positionUpdateListener);
        positionManager.removePositionListener(positionIndicatorManager);
        GlobalMapEventManager.removeListener(movementDetector);
        GlobalMapEventManager.removeListener(positionIndicatorManager.getAnimationListener());

        removeMapEventListener(tiltDetector);

        getParentActivity().getContentResolver().unregisterContentObserver(trapObserver);
        getParentActivity().getContentResolver().unregisterContentObserver(patrolObserver);

        trapLayer.detach();
        patrolMapLayer.detach();
        linkMatchingMapLayer.detach();
        positionIndicatorManager.detach();

        super.onPause();
    }

    private void centerOnMe(GeoPosition position)
    {
        if (position.isValid())
        {
            int midWidth = windowManager.getDefaultDisplay().getWidth() / 2;
            int midHeight = windowManager.getDefaultDisplay().getHeight() - (windowManager.getDefaultDisplay().getHeight() / 3);
            map.setTransformCenter(new PointF(midWidth, midHeight));

            map.setCenter(position.getCoordinate(), MapAnimation.NONE);
            map.setOrientation((float)position.getHeading(), MapAnimation.NONE);

            if (map.getZoomLevel() < Defaults.MAP_MAX_CENTER_ON_ME_ZOOM_LEVEL)
                map.setZoomLevel(Defaults.DEFAULT_MAP_ZOOM_LEVEL);

            trapLayer.requestRedraw();
        }

    }

    public void setOnMapLoadedRunnable(Runnable runnable)
    {
        if (map == null)
            onMapLoadedRunnable = runnable;
        else
            runnable.run();
    }

    private class CenterOnMeListener implements MapController.OnMapUpdateListener
    {
        @Override
        public void onCenterOnMeChanged(boolean isCentered)
        {
            isCenterOnMe = isCentered;
            if (isCentered)
                centerOnMe(positionManager.getLastLocation());
        }
    }

    private class PositionUpdateListener implements PositionListener
    {

        @Override
        public void onPositionUpdated(LocationMethod locationMethod, GeoPosition geoPosition)
        {
            if (isCenterOnMe  && geoPosition.getSpeed() >= MINIMUM_SPEED_FOR_UPDATE)
            {
                centerOnMe(geoPosition);
            }
        }

        @Override public void onPositionFixChanged(LocationMethod locationMethod, LocationStatus locationStatus){}
    }

    private class TrapUpdateObserver extends ContentObserver
    {
        public TrapUpdateObserver()
        {
            super(null);
        }

        @Override
        public void onChange(boolean selfChange)
        {
            super.onChange(selfChange);

            trapLayer.refresh();
        }
    }

    private class PatrolUpdateObserver extends ContentObserver
    {
        public PatrolUpdateObserver()
        {
            super(null);
        }

        @Override
        public void onChange(boolean selfChange)
        {
            super.onChange(selfChange);
            patrolMapLayer.refresh();
        }
    }

    private class MapMovementDetector extends MapMotionDetector
    {
        @Override
        public void onMapMoveStart()
        {
            mapController.setCenterOnMe(false);
        }

        @Override
        public boolean onDoubleTap(PointF point)
        {
            mapController.setCenterOnMe(false);
            return false;
        }

        @Override
        public boolean onTilt(float f)
        {
            return false;
        }
    }

    private class MapTiltDetector extends MapMotionDetector
    {
        @Override
        public boolean onTilt(float arg0)
        {
            if(!sharedPreferences.getBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_3D, true))
               return true;
            float tilt = map.getTilt();
            if (tilt + arg0 >= Defaults.MAP_MAX_TILT && arg0 > 0 )
                return true;
            else
                return false;
        }
    }
}
