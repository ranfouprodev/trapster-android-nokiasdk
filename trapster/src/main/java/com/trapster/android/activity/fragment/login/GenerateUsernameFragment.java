package com.trapster.android.activity.fragment.login;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.inject.Inject;
import com.trapster.android.R;
import com.trapster.android.comms.UsernameAvailableListener;
import com.trapster.android.comms.rest.IUserAPI;
import com.trapster.android.model.User;
import com.trapster.android.util.Log;
import com.trapster.android.util.UsernameUtils;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;

import roboguice.inject.InjectView;

/**
 * Used to create a new username when the login comes from an oauth source
 */
public class GenerateUsernameFragment extends AbstractLoginFragment {

    private static final String LOGNAME = "GenerateUsernameFragment";

    @InjectView(R.id.editGeneratedName)
    EditText editText;

    @InjectView(R.id.buttonGenerateName)
    Button generateButton;

    @InjectView(R.id.imageCheck)
    ImageView imageCheckIndicator;

    @InjectView(R.id.progressLoading)
    ProgressBar loadingIndicator;

    @InjectView(R.id.checkNewsletter)
    CheckBox checkNewsletter;

    @Inject
    IUserAPI api;



    private User user;


    public static GenerateUsernameFragment newInstance(Bundle args){
        GenerateUsernameFragment f = new GenerateUsernameFragment();
        f.setArguments(args);
        return f;
    }

    public static GenerateUsernameFragment newInstance(User user){
        GenerateUsernameFragment f = new GenerateUsernameFragment();
        Bundle args = new Bundle();
        args.putParcelable("USER",user);
        f.setArguments(args);
        return f;
    }


    private Handler regenerateName = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            editText.setText(UsernameUtils.generateRandomDrivingName(user.getProfile().getFirstName()));
        }
    };

    private static final int USERNAME_TAKEN = 0;
    private static final int USERNAME_AVAILABLE = 1;

    private Handler setCheckHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what){
                case USERNAME_TAKEN:
                    imageCheckIndicator.setImageResource(R.drawable.red_x);
                    imageCheckIndicator.setVisibility(View.VISIBLE);
                    break;
                case USERNAME_AVAILABLE:
                    imageCheckIndicator.setImageResource(R.drawable.green_check);
                    imageCheckIndicator.setVisibility(View.VISIBLE);
                    break;
            }
        }
    };


    private UsernameTakenListener usernameTakenListener;
    private Handler checkUserNameHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            usernameTakenListener = new UsernameTakenListener();
            api.isUsernameTaken(editText.getText().toString().trim(),usernameTakenListener,usernameTakenListener);
        }
    };

    @Override
    void disable() {

    }

    @Override
    void enable() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null && getArguments().containsKey("USER")){
            user = getArguments().getParcelable("USER");
        }else{
            user = getLoginActivity().getRegisterUser();
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register_generatename,null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        regenerateName.sendEmptyMessage(0);

        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(!hasFocus){
                    checkUserNameHandler.removeMessages(0);
                    checkUserNameHandler.sendEmptyMessageDelayed(0,250);
                }
            }
        });

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                checkUserNameHandler.removeMessages(0);
                checkUserNameHandler.sendEmptyMessageDelayed(0,1500);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        generateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String username =editText.getText().toString().trim();


                user.setUserName(username);

                if(user.isFacebook())
                    getLoginActivity().sendRegisterFacebook(user);
                else{
                    getLoginActivity().sendRegister(user.getProfile().getEmail(),username,user.getTempPassword(),checkNewsletter.isChecked());
                }
            }
        });

        imageCheckIndicator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                regenerateName.sendEmptyMessage(0);
            }
        });

    }

    private class UsernameTakenListener implements UsernameAvailableListener,CommunicationStatusListener{

        @Override
        public void onOpenConnection() {
            Log.v(LOGNAME,"Open connection");
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    imageCheckIndicator.setVisibility(View.GONE);
                    loadingIndicator.setVisibility(View.VISIBLE);
                }
            });
        }

        @Override
        public void onCloseConnection() {
            Log.v(LOGNAME,"Closed connection");
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingIndicator.setVisibility(View.GONE);
                }
            });
        }

        @Override
        public void onConnectionError(String errorMessage) {
            Log.e(LOGNAME,"Connection Error:"+errorMessage);
            // Not sure if this is the way to handle this
            //setCheckHandler.sendEmptyMessage(USERNAME_AVAILABLE);
        }

        @Override
        public void onUsernameAvailable(String name, final boolean available) {
            Log.v(LOGNAME,"Username available:"+available);
            setCheckHandler.sendEmptyMessage((available)?USERNAME_AVAILABLE:USERNAME_TAKEN);

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    generateButton.setEnabled(available);
                }
            });

        }
    }


}
