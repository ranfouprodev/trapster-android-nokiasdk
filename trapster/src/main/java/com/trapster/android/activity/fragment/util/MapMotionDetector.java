package com.trapster.android.activity.fragment.util;

import android.graphics.PointF;
import com.here.android.common.GeoCoordinate;
import com.here.android.common.ViewObject;
import com.here.android.mapping.MapEventListener;

import java.util.List;

/*
 * Convenience class so that we don't have to implement all the MapEventListener methods for each call
 * 
 */
public abstract class MapMotionDetector implements MapEventListener {

	@Override
	public boolean onDoubleTap(PointF arg0) {
		return false;
	}

	@Override
	public void onLongPressReleased() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onLongPressed(PointF arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onMapAnimatingEnd() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMapAnimatingStart() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMapMoveEnd(GeoCoordinate arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMapMoveStart() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onMapObjectsSelected(List<ViewObject> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onMapSchemeSet() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onPinchZoom(float scaleFactor, PointF center) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onRotate(float arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onTap(PointF arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onTilt(float arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onTwoFingerTap(PointF arg0) {
		// TODO Auto-generated method stub
		return false;
	}

}
