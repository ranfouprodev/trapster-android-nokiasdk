package com.trapster.android.activity.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import com.trapster.android.R;
import roboguice.inject.InjectView;

public class InfoOverlayLoadingFragment extends AbstractFragment
{
    private static final long SPINNER_TIMEOUT = 7 * 1000; // 7 seconds

    @InjectView(R.id.infoLoadingButton) Button continueButton;
    @InjectView(R.id.infoLoadingSpinner)ImageView spinner;

    private View.OnClickListener listener;
    private final Handler handler = new Handler();

    private volatile boolean timeoutCompleted = false;
    private volatile boolean mapLoadCompleted = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_info_loading, container, false);
        return view;
    }


    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        Animation animation = AnimationUtils.loadAnimation(getParentActivity(), R.anim.spinner_animation);
        spinner.startAnimation(animation);
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
        {
            @Override
            public void onGlobalLayout()
            {
                view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                handler.postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {

                        timeoutCompleted = true;
                        checkReady();
                    }
                }, SPINNER_TIMEOUT);
            }
        });


        if (listener != null)
            continueButton.setOnClickListener(listener);

    }

    public void setOnContinueClickListener(View.OnClickListener listener)
    {
        if (continueButton != null)
            continueButton.setOnClickListener(listener);
        this.listener = listener;
    }

    // Returns true if all conditions are met and we're ready to go to the map
    private void checkReady()
    {
        if (mapLoadCompleted && timeoutCompleted)
        {
            if (listener != null)
                listener.onClick(continueButton);
        }
        else if(mapLoadCompleted || timeoutCompleted)
        {
            enableButton();
        }
    }

    private void enableButton()
    {
        handler.post(new Runnable()
        {
            @Override
            public void run()
            {
                spinner.clearAnimation();
                spinner.setVisibility(View.INVISIBLE);
                //loadingText.setVisibility(View.INVISIBLE);
                continueButton.setVisibility(View.VISIBLE);
            }
        });
    }

    public void onMapLoaded()
    {
        mapLoadCompleted = true;
        checkReady();
    }
}
