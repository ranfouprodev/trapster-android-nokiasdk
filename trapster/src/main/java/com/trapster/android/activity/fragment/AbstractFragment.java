package com.trapster.android.activity.fragment;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.trapster.android.activity.component.PopupMessage;

import roboguice.fragment.RoboFragment;

/**
 * Using both the Roboguice and Actionbarsherlock classes will require an
 * extensible fragment.
 * 
 * 
 * @author John
 * 
 */
public abstract class AbstractFragment extends RoboFragment {

    private FragmentActivity parentActivity;
    protected boolean visible = true;
	
	/*
	 * Used for cleanup
	 */

    public AbstractFragment()
    {
        this.visible = true;
    }

    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        parentActivity = (FragmentActivity) activity;
    }

    public final FragmentActivity getParentActivity()
    {
        if (parentActivity != null)
            return parentActivity;
        else
            return super.getActivity();
    }

	public void onBackPressed(){
		
	}

    public void showWithAnimation(int animationResource)
    {
        try
        {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.setCustomAnimations(animationResource, animationResource);
            ft.show(this);
            ft.commitAllowingStateLoss();
            visible = true;
        }
        catch (Exception e){}


    }


    public void show()
    {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.show(this);
        ft.commitAllowingStateLoss();
        visible = true;
    }

    public void hide()
    {
        FragmentManager manager = getFragmentManager();
        if (manager != null)
        {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.hide(this);
            ft.commitAllowingStateLoss();
            visible = false;
        }

    }

    public void removeWithAnimation(int animationResource)
    {
        FragmentManager manager = getFragmentManager();
        if (manager != null)
        {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.setCustomAnimations(animationResource, animationResource, animationResource, animationResource);
            ft.remove(this);
            ft.commitAllowingStateLoss();
            visible = false;
        }

    }

    // isVisible() is obnoxiously final, so we're instead going to get the state of whether or not it has been set to show or hide,
    // which isn't reflected until fragment transaction that sets it has completed
    public boolean getVisibleState()
    {
        return visible;
    }

    /**
     * User for error messages
     */

    public void showAlertMessage(String message){
        PopupMessage.makeText(getActivity(),message,PopupMessage.STYLE_ALERT).show();

    }
}
