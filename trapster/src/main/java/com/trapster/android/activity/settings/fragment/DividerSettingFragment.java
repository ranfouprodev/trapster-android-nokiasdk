package com.trapster.android.activity.settings.fragment;

import android.view.View;
import android.widget.TextView;
import com.trapster.android.R;
import roboguice.inject.InjectView;

public class DividerSettingFragment extends AbstractSettingFragment
{
    @InjectView (R.id.dividerFragmentText) TextView dividerText;

    private int descriptionResource;

    @Override
    protected void init()
    {
        if (descriptionResource != 0)
            dividerText.setText(getString(descriptionResource));
    }

    @Override
    protected int getLayoutResource()
    {
        return R.layout.fragment_settings_divider;
    }

    @Override
    public void setOnClickListener(View.OnClickListener listener)
    {
    }

    public void setDescription(int description)
    {
        if (isAdded())
            setDescription(getString(description));
        descriptionResource = description;
    }

    private void setDescription(String description)
    {
        if (this.dividerText != null)
            this.dividerText.setText(description);
    }
}
