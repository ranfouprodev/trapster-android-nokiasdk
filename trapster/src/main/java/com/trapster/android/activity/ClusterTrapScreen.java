package com.trapster.android.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.google.inject.Inject;
import com.trapster.android.activity.fragment.component.TrapMapImageView;
import com.trapster.android.R;
import com.trapster.android.manager.MapController;
import com.trapster.android.manager.TrapManager;
import com.trapster.android.model.Trap;
import com.trapster.android.model.TrapType;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

import java.util.List;

/**
 * Launched when a user clicks on a cluster
 * 
 * 
 * [Nov 24, 2012 john] 
 * Updated the trapAdapter to use a ViewHolder for efficiency. 
 * Fixed bug where views weren't repopulated when the listview recycled the view.
 * Removed the TrapItem list and moved the Trap into the Adapter. Doesn't do anything but makes it easier to read 
 * Added TrapMapSnapshot and cleaned up UI to match
 * Renamed view_trap_cluster_item to item_trap_cluster_view for consistency
 * 
 *
 * [Nov 29, 2012 john]
 * adapter.addAll only available on api > 11. Changed to add
 */
@ContentView(R.layout.screen_trap_cluster)
public class ClusterTrapScreen extends Screen
{

	private static final String LOGNAME = "ClusterTrap";
	
    public static final String CLUSTER_TRAP_TRAP_EXTRAS = "cluster_trap_extras";
	
    private TrapAdapter trapAdapter;

    @InjectView(R.id.clusterTrapListView) ListView lv;
    
    @Inject private TrapManager trapManager;
    
    @Inject MapController mapController;

    private final static String CLASS_NAME = "ClusterTrapScreen";
    

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
                     
        Intent i = getIntent();
        if(!i.hasExtra(CLUSTER_TRAP_TRAP_EXTRAS) || !(i.getExtras().get(CLUSTER_TRAP_TRAP_EXTRAS) instanceof List))
        	finish(); 
        
        @SuppressWarnings("unchecked")
		List<Trap> traps = (List<Trap>)i.getExtras().get(CLUSTER_TRAP_TRAP_EXTRAS);
        
       
        trapAdapter = new TrapAdapter(this, traps);
        lv.setAdapter(trapAdapter);
        
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
            {
            	Trap trap= (Trap) adapterView.getItemAtPosition(position);
            	if (trap != null){
            		showTrapView(trap);
            	}
            }
        });



        String numTraps = String.valueOf(traps.size());
        if (traps.size() == 1)
        {
            this.setTitle(getString(R.string.cluster_trap_title_singular, numTraps));
        }
        else
            this.setTitle(getString(R.string.cluster_trap_title, numTraps));

    }

    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }


    void showTrapView(Trap trap)
	{
		Intent i= new Intent(this, SingleTrapDetailsScreen.class);
		i.putExtra(SingleTrapDetailsScreen.TRAP_BUNDLE_KEY, trap);
		startActivity(i);
	}

    public void clearCluster()
    {
        trapAdapter.clear();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        clearCluster();
    }
    
    private class TrapAdapter extends ArrayAdapter<Trap>
    {
      
        public TrapAdapter (Screen screen, List<Trap> trapList)
        {
            super(screen, R.id.clusterTrapListView);
            for(Trap trap:trapList)
            	add(trap);
            
        }

        public View getView(final int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder = null;

    		View v = convertView;

    		if (v == null) {
    			LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
    					Context.LAYOUT_INFLATER_SERVICE);
    			holder = new ViewHolder();

    			v = vi.inflate(R.layout.item_trap_cluster_view, null);
   
                holder.trapTitle = (TextView) v.findViewById(R.id.clusterTrapNameText);
                holder.trapDescription = (TextView) v.findViewById(R.id.clusterTrapNameDescription);
    			holder.mapSnapshot = (TrapMapImageView) v.findViewById(R.id.imageMapSnapshot);
    			
    			v.setTag(holder);
    		} else {
    			holder = (ViewHolder) v.getTag();
    		}

    		final Trap item = getItem(position);
    		if (item != null) {
    			bindView(holder, item);
    		}

    		return v;

        }
        
        private void bindView(ViewHolder holder, Trap trap) {

    		TrapType trapType = trapManager.getTrapType(trap.getTypeid());

    		holder.mapSnapshot.setTrap(trap);

    		
            holder.trapTitle.setText(trapType.getName());
            holder.trapDescription.setText(trap.getAddress());
    		
    	}
        
        
    }
    
    
	private static class ViewHolder {
		public TextView trapTitle;
		public TextView trapDescription;
	
		public TrapMapImageView mapSnapshot;
		
		
	}
    
}