package com.trapster.android.activity.fragment.util;

import android.graphics.Bitmap;
import android.graphics.PointF;
import com.google.inject.Inject;
import com.here.android.common.Image;
import com.here.android.mapping.MapFactory;
import com.here.android.mapping.MapMarker;
import com.here.android.mapping.MapOverlayType;
import com.trapster.android.TrapsterApplication;
import com.trapster.android.manager.BitmapCacheManager;
import com.trapster.android.manager.TrapManager;
import com.trapster.android.model.ImageSpec;
import com.trapster.android.model.Trap;
import com.vividsolutions.jts.geom.Envelope;
import roboguice.RoboGuice;

public class TrapMapMarker extends AbstractMapMarker{

	@Inject
	protected TrapManager trapManager;

    @Inject
    BitmapCacheManager bitmapCacheManager;
	
	public Trap trap; 


	protected MapMarker marker; 
	
	protected boolean visible;

	protected ImageSpec imageSpec; 
	
	
	/**
	 * Used to enable the clustertrap marker. Don't use this as a instantiation as
	 * it won't have any objects populated 
	 */
	protected TrapMapMarker(){
	}
	
	public TrapMapMarker(Trap trap){
		RoboGuice.injectMembers(TrapsterApplication.getAppInstance(), this);
		setTrap(trap);
	}

    public Trap getTrap() {
        return trap;
    }

    public void setTrap(Trap trap) {
        this.trap = trap;
        marker = convertTrapToMapObject(trap);
    }
	
	public Envelope getSpatialIndex(){
		Envelope trapEnvelope = new Envelope(trap.getGeometry()
				.getEnvelopeInternal());
		trapEnvelope.expandBy(0.000001);
		return trapEnvelope; 
	}
	
	public MapMarker getMarker() {
		return marker;
	}


	private MapMarker convertTrapToMapObject(Trap trap) {
		
		MapMarker mapObject = MapFactory.createMapMarker();

		mapObject.setCoordinate(MapFactory.createGeoCoordinate(
				trap.getLatitude(), trap.getLongitude()));

        String key = trapManager.getIconNameForTrap(trap);

        if(key== null)
            android.util.Log.e("BBQ","Unable to find key for trap:"+trap.getTypeid()+":"+trap.getLevel());


        Image image = bitmapCacheManager.getImageFromCache(key);

        if(image == null){
            image = MapFactory.createImage();

            Bitmap mapIcon = trapManager.getMapIconForTrap(trap);
            /*
             * This will occur when the resources aren't loaded yet, or the image
             * isn't ready
             */
            if (mapIcon == null)
                return null;

            image.setBitmap(mapIcon);
            bitmapCacheManager.addMapImageToCache(key,image);
        }
		mapObject.setIcon(image);

		/*
		 * Anchor points reference the center of the image.
		 * 
		 * The pinX and pinY in the trap type reference the top left.
		 */
		imageSpec = trapManager.getImageSpecForTrap(trap);

		int offsetX = imageSpec.getPinx();

		int offsetY = imageSpec.getPiny();

		mapObject.setAnchorPoint(new PointF(offsetX, offsetY));
		
		mapObject.setZIndex(100);
        mapObject.setOverlayType(MapOverlayType.BACKGROUND_OVERLAY);

		return mapObject;
	}


	public boolean isVisible() {
		return visible;
	}


	public void setVisible(boolean visible) {
		this.visible = visible;
	}


	public int getTrapId() {
		return (trap != null)?trap.getId():-1; 
	}


}
