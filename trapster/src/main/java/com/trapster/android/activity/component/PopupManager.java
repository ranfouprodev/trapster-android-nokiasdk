package com.trapster.android.activity.component;

import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.google.inject.Singleton;
import com.trapster.android.R;

import java.util.LinkedList;
import java.util.Queue;

@Singleton
public class PopupManager extends Handler {

    private static final int MESSAGE_DISPLAY = 0xc2007;
    private static final int MESSAGE_ADD_VIEW = 0xc20074dd;
    private static final int MESSAGE_REMOVE = 0xc2007de1;

    private Queue<PopupMessage> msgQueue =  new LinkedList<PopupMessage>();

    private Animation inAnimation, outAnimation;

    public PopupManager() {
    }

    void add(PopupMessage appMsg) {

        msgQueue.add(appMsg);
        if (inAnimation == null) {
            inAnimation = AnimationUtils.loadAnimation(appMsg.getActivity(),
                    R.anim.slide_in_top);
        }
        if (outAnimation == null) {
            outAnimation = AnimationUtils.loadAnimation(appMsg.getActivity(),
                    R.anim.slide_out_top_with_fade);
        }
        displayMsg();
    }

    /**
     * Removes all {@link PopupMessage} from the queue.
     */
    void clearMsg(PopupMessage appMsg) {
        if(msgQueue.contains(appMsg)){
            // Avoid the message from being removed twice.
            removeMessages(MESSAGE_REMOVE);
            msgQueue.remove(appMsg);
            removeMsg(appMsg);
        }
    }

    /**
     * Removes all {@link PopupMessage} from the queue.
     */
    void clearAllMsg() {
        if (msgQueue != null) {
            msgQueue.clear();
        }
        removeMessages(MESSAGE_DISPLAY);
        removeMessages(MESSAGE_ADD_VIEW);
        removeMessages(MESSAGE_REMOVE);
    }

    /**
     * Displays the next {@link PopupMessage} within the queue.
     */
    private void displayMsg() {
        if (msgQueue.isEmpty()) {
            return;
        }
        // First peek whether the AppMsg is being displayed.
        final PopupMessage appMsg = msgQueue.peek();
        // If the activity is null we throw away the AppMsg.
        if (appMsg.getActivity() == null) {
            msgQueue.poll();
        }
        final Message msg;
        if (!appMsg.isShowing()) {
            // Display the AppMsg
            msg = obtainMessage(MESSAGE_ADD_VIEW);
            msg.obj = appMsg;
            sendMessage(msg);
        } else {
            msg = obtainMessage(MESSAGE_DISPLAY);
            sendMessageDelayed(msg, appMsg.getDuration()
                    + inAnimation.getDuration() + outAnimation.getDuration());
        }
    }

    /**
     * Removes the {@link PopupMessage}'s view after it's display duration.
     *
     * @param appMsg The {@link PopupMessage} added to a {@link ViewGroup} and should be removed.s
     */
    private void removeMsg(final PopupMessage appMsg) {
        ViewGroup parent = ((ViewGroup) appMsg.getView().getParent());
        if (parent != null) {
            outAnimation.setAnimationListener(new OutAnimationListener(appMsg));
            appMsg.getView().startAnimation(outAnimation);
            // Remove the AppMsg from the queue.
            msgQueue.poll();
            if (appMsg.isFloating()) {
                // Remove the AppMsg from the view's parent.
                parent.removeView(appMsg.getView());
            } else {
                appMsg.getView().setVisibility(View.INVISIBLE);
            }

            Message msg = obtainMessage(MESSAGE_DISPLAY);
            sendMessage(msg);
        }
    }

    private void addMsgToView(PopupMessage appMsg) {
        View view = appMsg.getView();
        if (view.getParent() == null) {
            appMsg.getActivity().addContentView(
                    view,
                    appMsg.getLayoutParams());
        }
        view.startAnimation(inAnimation);
        if (view.getVisibility() != View.VISIBLE) {
            view.setVisibility(View.VISIBLE);
        }
        final Message msg = obtainMessage(MESSAGE_REMOVE);
        msg.obj = appMsg;
        sendMessageDelayed(msg, appMsg.getDuration());
    }

    @Override
    public void handleMessage(Message msg) {
        final PopupMessage appMsg;
        switch (msg.what) {
            case MESSAGE_DISPLAY:
                displayMsg();
                break;
            case MESSAGE_ADD_VIEW:
                appMsg = (PopupMessage) msg.obj;
                addMsgToView(appMsg);
                break;
            case MESSAGE_REMOVE:
                appMsg = (PopupMessage) msg.obj;
                removeMsg(appMsg);
                break;
            default:
                super.handleMessage(msg);
                break;
        }
    }

    private static class OutAnimationListener implements Animation.AnimationListener {

        private PopupMessage appMsg;

        private OutAnimationListener(PopupMessage appMsg) {
            this.appMsg = appMsg;
        }

        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            if (!appMsg.isFloating()) {
                appMsg.getView().setVisibility(View.GONE);
            }

            if(appMsg.getOnPopupClosedListener() != null)
                appMsg.getOnPopupClosedListener().onPopupClosed();
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    }
}