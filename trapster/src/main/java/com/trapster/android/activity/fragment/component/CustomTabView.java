package com.trapster.android.activity.fragment.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.trapster.android.R;


public class CustomTabView extends LinearLayout{

	private int count;
	
	private String text;

	private TextView textView;

	private TextView textCountView;
	
	public CustomTabView(final Context context, final AttributeSet attrSet) {
		super(context, attrSet);

		TypedArray a = context.obtainStyledAttributes(attrSet, R.styleable.CustomTabView, 0, 0);
	
		text = a.getString(R.styleable.CustomTabView_text);
		
		count = a.getInt(R.styleable.CustomTabView_count, 0);
				
		initialize(context);

		a.recycle();
		
	}

	public CustomTabView(final Context context, int count, String text) {
		super(context);
		this.text = text;
		this.count = count; 
		initialize(context);
	}

	private void initialize(final Context context) {

		
		LayoutInflater layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.item_tab, this);
		
        
        textCountView = (TextView) findViewById(R.id.textTabCount);
		
		textView = (TextView) findViewById(R.id.textTabDetail);
		
			
		update(); 
	}

	public void update() {
		
		textCountView.setVisibility((count==0)?View.INVISIBLE:View.VISIBLE);
		
		if(count != 0)
			textCountView.setText(String.valueOf(count));
		
		textView.setText(text);
		
	}
	
	public void setCount(int count){
		this.count = count; 
		update(); 
	}
	
}
