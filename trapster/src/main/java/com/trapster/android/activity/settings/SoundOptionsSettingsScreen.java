package com.trapster.android.activity.settings;

import android.util.SparseArray;
import android.view.View;
import android.widget.CompoundButton;
import com.google.inject.Inject;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.activity.settings.fragment.DividerSettingFragment;
import com.trapster.android.activity.settings.fragment.RadioButtonSettingFragment;
import com.trapster.android.manager.SoundManager;
import com.trapster.android.manager.SoundThemeManager;
import com.trapster.android.manager.TrapManager;
import com.trapster.android.model.SoundTheme;

import java.util.ArrayList;

public class SoundOptionsSettingsScreen extends AbstractSettingsScreen
{
    private static final int REDLIGHT_CAMERA_TRAP_TYPE = 1;
    private static final int DEFAULT_SOUNDTHEME_ID = 0;
    private static final String CLASS_NAME = "Sound Options Screen";
    @Inject SoundThemeManager soundThemeManager;
    @Inject SoundManager soundManager;
    @Inject TrapManager trapManager;

    private ArrayList<SoundTheme> languageThemes = new ArrayList<SoundTheme>();
    private ArrayList<SoundTheme> specialThemes = new ArrayList<SoundTheme>();
    private SparseArray<RadioButtonSettingFragment> themeMap = new SparseArray<RadioButtonSettingFragment>();

    private RadioButtonSettingFragment lastSelectedFragment;

    private DividerSettingFragment specialThemesDivider;


    @Override
    public void onResume()
    {
        super.onResume();
        SoundTheme currentTheme = soundThemeManager.getCurrentTheme();

        if (currentTheme == null || currentTheme.getId() == DEFAULT_SOUNDTHEME_ID || isSpecialTheme(currentTheme.getId()))
            showSpecialThemes();
        else
            hideSpecialThemes();
    }

    @Override
    protected void setupFragments()
    {
        organizeThemes();

        ((DividerSettingFragment)createSettingsFragment(FRAGMENT_TYPE.DIVIDER)).setDescription(R.string.setting_soundthemes_lang_title);
        addThemes(languageThemes);

        specialThemesDivider =((DividerSettingFragment)createSettingsFragment(FRAGMENT_TYPE.DIVIDER));
        specialThemesDivider.setDescription(R.string.setting_soundthemes_themes_title);
        addThemes(specialThemes);

        uncheckRadioButtons();
        setStartingRadioButton();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        soundThemeManager.downloadTheme(soundThemeManager.getCurrentTheme(), false);
    }

    private void addThemes(ArrayList<SoundTheme> themes)
    {
        for (final SoundTheme theme : themes)
        {
            final RadioButtonSettingFragment fragment = (RadioButtonSettingFragment) createSettingsFragment(FRAGMENT_TYPE.RADIO_BUTTON);
            fragment.setDescription(theme.getName());
            fragment.setImageResource(R.drawable.play);

            fragment.setCheckBoxListener(new CompoundButton.OnCheckedChangeListener()
            {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isSelected)
                {
                    if (isSelected)
                    {
                        soundThemeManager.setCurrentTheme(theme);
                        editor.putInt(Defaults.PREFERENCE_SOUND_THEME_CHOSEN, theme.getId());

                        if (theme.getId() == DEFAULT_SOUNDTHEME_ID || isSpecialTheme(theme.getId()))
                            showSpecialThemes();
                        else
                            hideSpecialThemes();

                        if (theme.getName().equals("Mr Flamboyant"))
                            editor.putBoolean(Defaults.PREFERENCE_SOUND_THEME_FABULOUS_EASTER_EGG, true);
                    }
                    uncheckRadioButtons();
                    fragment.setChecked(true);
                }
            });

            View.OnClickListener playDemoListener = new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    playDemo(theme.getId());
                }
            };

            fragment.setIconClickListener(playDemoListener);
            fragment.setOnClickListener(playDemoListener);

            themeMap.put(theme.getId(), fragment);
        }
    }

    private void hideSpecialThemes()
    {
        specialThemesDivider.hide();

        for (SoundTheme theme : specialThemes)
        {
            themeMap.get(theme.getId()).hide();
        }
    }

    private void showSpecialThemes()
    {
        specialThemesDivider.show();

        for (SoundTheme theme : specialThemes)
        {
            themeMap.get(theme.getId()).show();
        }
    }


    private void setStartingRadioButton()
    {
        int selectedTheme = sharedPreferences.getInt(Defaults.PREFERENCE_SOUND_THEME_CHOSEN, DEFAULT_SOUNDTHEME_ID);
        RadioButtonSettingFragment fragment = themeMap.get(selectedTheme);
        if (fragment == null)
            fragment = themeMap.get(0);

        fragment.setChecked(true);
        lastSelectedFragment = fragment;
    }

    private void organizeThemes()
    {
        // This is a major hack until we can streamline the way we organize sound themes
        // These fortunately don't need to be localized since it makes sense that they're in English
        SoundTheme englishTheme = new SoundTheme();
        englishTheme.setName("Default (English)");
        englishTheme.setDescription("English");
        englishTheme.setAuthor("trapster");
        englishTheme.setId(DEFAULT_SOUNDTHEME_ID);

        languageThemes.add(englishTheme);
        ArrayList<SoundTheme> soundThemes = soundThemeManager.getSoundThemes();
        for (SoundTheme theme : soundThemes)
        {
            if (isLanguageTheme(theme))
                languageThemes.add(theme);
            else
                specialThemes.add(theme);
        }
    }

    private boolean isLanguageTheme(SoundTheme theme)
    {
        // If author == Trapster || author == Unspecified
        String author = theme.getAuthor();
        if(author.equalsIgnoreCase(getString(R.string.setting_soundthemes_author_trapster))
                || author.equalsIgnoreCase(getString(R.string.setting_soundthemes_author_unspecified)))
        {
            return true;
        }
        return false;
    }

    private void uncheckRadioButtons()
    {
        for (int i = 0; i < themeMap.size(); i++)
        {
            themeMap.valueAt(i).setChecked(false);
        }
    }

    private void playDemo(int soundThemeID)
    {
        SoundTheme theme = soundThemeManager.getTheme(soundThemeID);
        soundManager.playSound(trapManager.getTrapType(REDLIGHT_CAMERA_TRAP_TYPE).getAudioid(), theme);
    }

    private boolean isSpecialTheme(int themeID)
    {
        for (SoundTheme theme : specialThemes)
        {
            if (theme.getId() == themeID)
                return true;
        }

        return false;
    }

    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }
}
