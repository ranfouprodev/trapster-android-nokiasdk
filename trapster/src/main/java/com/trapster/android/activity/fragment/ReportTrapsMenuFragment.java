package com.trapster.android.activity.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import com.google.inject.Inject;
import com.trapster.android.BuildFeatures;
import com.trapster.android.R;
import com.trapster.android.activity.ReportTrapScreen;
import com.trapster.android.manager.BitmapCacheManager;
import com.trapster.android.manager.BitmapCacheManager.BitmapLoadedListener;
import com.trapster.android.manager.BitmapCacheManager.InvalidResourceKeyException;
import com.trapster.android.manager.PositionManager;
import com.trapster.android.manager.TrapManager;
import com.trapster.android.model.ImageResource;
import com.trapster.android.model.TrapType;
import com.trapster.android.model.dao.CategoryDAO;
import com.trapster.android.model.dao.ImageResourceDAO;
import roboguice.inject.InjectView;

import java.util.List;


public class ReportTrapsMenuFragment extends AbstractFragment {

	public static final String LOGNAME = "ReportTrapsFragment";

	@Inject
	TrapManager trapManager;

    @Inject CategoryDAO categoryDAO;
	
	@Inject
	BitmapCacheManager bitmapCacheManager;

    @Inject
    PositionManager positionManager;
	
	@InjectView(R.id.gridMainTraps) GridView mainGridView;

	
	@InjectView(R.id.layoutReportTrapGrid)
	LinearLayout layoutDashboard;

    @Inject ImageResourceDAO imageResourceDAO;

	private ReportTrapCursorAdapter trapAdapter;
    private static final double ICON_SIZE_SCREEN_PERCENTAGE = 0.25;
    Handler handler = new Handler(Looper.getMainLooper());

    private double lat;
    private double lng;
    private AbstractMapFragment.MapConfig mapConfig;

    public static ReportTrapsMenuFragment newInstance(){
		ReportTrapsMenuFragment fragment = new ReportTrapsMenuFragment();
		return fragment; 
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_dash_report_traps, null);
		return view; 
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		
		List<TrapType> types = trapManager.getSortedReportableOnlyTrapTypes();


		trapAdapter = new ReportTrapCursorAdapter(getParentActivity(),types);
		mainGridView.setAdapter(trapAdapter);
		
		mainGridView.setOnItemClickListener(new TrapClickListener());
		
	}

    public void setMapConfig(AbstractMapFragment.MapConfig mapConfig) {
        this.mapConfig = mapConfig;
    }


    private class TrapClickListener implements OnItemClickListener{

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			
			TrapType type = trapAdapter.getItem(position);
			Intent intent = new Intent(getParentActivity(), ReportTrapScreen.class);
			intent.putExtra(ReportTrapScreen.INTENT_EXTRA_TRAPTYPE_ID, type.getId());
            intent.putExtra(ReportTrapScreen.INTENT_EXTRA_TRAP_LOCATION_LAT, lat);
            intent.putExtra(ReportTrapScreen.INTENT_EXTRA_TRAP_LOCATION_LNG, lng);
            if(mapConfig != null)
                intent.putExtra(ReportTrapScreen.INTENT_EXTRA_MAP_CONFIG,mapConfig);
			//intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			
			getParentActivity().startActivity(intent);

			getParentActivity().finish();

		}
		
	}
	
	
	private class ReportTrapCursorAdapter extends ArrayAdapter<TrapType> {

		private List<TrapType> entries;
		private Location location;

		public ReportTrapCursorAdapter(Context context, List<TrapType> objects) {
			super(context, R.layout.item_dash_report_traps, objects);
			this.entries = objects;
		}
		
		@Override
		public TrapType getItem(int position) {
			return entries.get(position);
		}

		@Override
		public long getItemId(int position) {
			return getItem(position).getId();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;

			View v = convertView;

			if (v == null) {
				LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
						Context.LAYOUT_INFLATER_SERVICE);
				holder = new ViewHolder();

				v = vi.inflate(R.layout.item_dash_report_traps, null);
				holder.trapTypeText = (TextView) v.findViewById(R.id.textTrapType);
				holder.icon = (ImageView) v.findViewById(R.id.imageTrapIcon);
				
				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}

			final TrapType item = getItem(position);
			if (item != null) {
				bindView(holder, item);
			}

            String imageKey = item.getImageFilename();

            /*
                * 102x102 is the default HDPI size for the menu popup icons. It will scale automatically to different resolutions
                *
                * Dimen should be loaded from @dimen/dashboard_menu_image
                */
                ImageResource imageResource = imageResourceDAO.findByKey(imageKey);



                Drawable drawable = bitmapCacheManager.getDrawableFromSvg(imageResource);

                DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                int portraitPixels;
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
                    portraitPixels = displayMetrics.heightPixels;
                else
                    portraitPixels = displayMetrics.widthPixels;
                int sideLength = (int) (portraitPixels * ICON_SIZE_SCREEN_PERCENTAGE);

                if (sideLength > 0)
                {
                    ViewGroup.LayoutParams params = holder.icon.getLayoutParams();
                    params.width = sideLength;
                    params.height = sideLength;
                    holder.icon.setLayoutParams(params);
                } if (sideLength > 0)
            {
                ViewGroup.LayoutParams params = holder.icon.getLayoutParams();
                params.width = sideLength;
                params.height = sideLength;
                holder.icon.setLayoutParams(params);
            }

                if (BuildFeatures.SUPPORTS_HONEYCOMB)
                    holder.icon.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

                if (drawable == null)
                {
                    Bitmap image = null;
                    try {
                        image = bitmapCacheManager.getBitmapFromImageResource(imageKey, new UpdateGridListener(holder.icon));
                        //image = bitmapCacheManager.getScaledBitmapFromImageResource(imageKey, 102,102,new UpdateGridListener(holder.icon));
                    } catch (InvalidResourceKeyException e) {
                        {/*                     //Log.e(LOGNAME, "Invalid imageKey:"+imageKey); */}
                    }

                    // @todo if the icon is loading from a url we should populate it accordingly instead of passing null
                    holder.icon.setImageDrawable(new BitmapDrawable(image));
                }
                else
                {
                    holder.icon.setImageDrawable(drawable);
                }

            // location
            holder.trapTypeText.setText(item.getName());

			return v;
		}

		public void bindView(ViewHolder holder, TrapType trapType) {

            try
            {
                // Default image key
                //String imageKey = trapType.getLevels().get(0).getIcon();
                String imageKey = trapType.getImageFilename();


			/*
			 * 102x102 is the default HDPI size for the menu popup icons. It will scale automatically to different resolutions
			 *
			 * Dimen should be loaded from @dimen/dashboard_menu_image
			 */
                Bitmap image = null;
			/*try {
				image = bitmapCacheManager.getScaledBitmapFromImageResource(imageKey, 102,102,new UpdateGridListener(holder.icon));
			} catch (InvalidResourceKeyException e) {
			}*/

                ImageResource imageResource = imageResourceDAO.findByKey(imageKey);
                Drawable drawable = bitmapCacheManager.getDrawableFromSvg(imageResource);
                if (drawable == null)
                {
                    try {
                        image = bitmapCacheManager.getBitmapFromImageResource(imageKey, new UpdateGridListener(holder.icon));
                    } catch (InvalidResourceKeyException e) {
                        {/*                     //Log.e(LOGNAME, "Invalid imageKey:"+imageKey); */}
                    }

                    // @todo if the icon is loading from a url we should populate it accordingly instead of passing null
                    holder.icon.setImageBitmap(image);
                }
                else
                {
                    DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                    int portraitPixels;
                    if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
                        portraitPixels = displayMetrics.heightPixels;
                    else
                        portraitPixels = displayMetrics.widthPixels;
                    int sideLength = (int) (portraitPixels * ICON_SIZE_SCREEN_PERCENTAGE);

                    if (sideLength > 0)
                    {
                        ViewGroup.LayoutParams params = holder.icon.getLayoutParams();
                        params.width = sideLength;
                        params.height = sideLength;
                        holder.icon.setLayoutParams(params);
                    }

                    holder.icon.setImageDrawable(drawable);
                }
            }
            catch (IndexOutOfBoundsException e){}

					
			// location
			holder.trapTypeText.setText(trapType.getName());



			
			
		}

		private class ViewHolder {
			TextView trapTypeText;
			ImageView icon;
		}

	}
	
		
	private class UpdateGridListener implements BitmapLoadedListener{

		private ImageView view; 
		
		public UpdateGridListener(ImageView view){
			this.view = view; 
		}
		
		@Override
		public void onBitmapLoaded(final Bitmap bitmap) {
			if(bitmap != null && view != null){
				handler.post(new Runnable(){

					@Override
					public void run() {
                        view.setVisibility(View.VISIBLE);
						view.setImageBitmap(bitmap);
					}
					
				});
			}
			
		}
		
	}
	
	
		

	
		
}
