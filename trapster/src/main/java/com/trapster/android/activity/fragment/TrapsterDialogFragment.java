package com.trapster.android.activity.fragment;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.trapster.android.R;

public class TrapsterDialogFragment extends DialogFragment{

    private static final String EXTRA_TITLE = "EXTRA_TITLE";
    private static final String EXTRA_DESCRIPTION = "EXTRA_DESCRIPTION";
    private static final String EXTRA_NEGATIVE_BUTTON = "EXTRA_NEG_BUTTON";
    private static final String EXTRA_POSITIVE_BUTTON = "EXTRA_POS_BUTTON";

    private String title;
    private String description;
    private String positiveButtonText;
    private String negativeButtonText;

    private TextView titleText;
    private TextView descriptionText;
    private Button posButton;
    private Button negButton;
    private LinearLayout linearLayout;

    private static View.OnClickListener mNegListener;
    private static View.OnClickListener mPosListener;

    public static TrapsterDialogFragment newInstance( String title, String description, String positiveButtonText,
                                                       String negativeButtonText, View.OnClickListener posListener,
                                                       View.OnClickListener negListener) {
        TrapsterDialogFragment f = new TrapsterDialogFragment();

        Bundle args = new Bundle();
        args.putString(EXTRA_TITLE , title);
        args.putString(EXTRA_DESCRIPTION, description);
        args.putString(EXTRA_NEGATIVE_BUTTON , negativeButtonText);
        args.putString(EXTRA_POSITIVE_BUTTON , positiveButtonText);
        f.setArguments(args);

        mPosListener = posListener;
        mNegListener = negListener;
        return f;
    }





    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(DialogFragment.STYLE_NO_FRAME,0);

    }





    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getArguments() != null) {
            title = (String) getArguments().get(EXTRA_TITLE);
            description = (String) getArguments().get(EXTRA_DESCRIPTION);

            positiveButtonText = (String) getArguments().get(EXTRA_POSITIVE_BUTTON);
            negativeButtonText = (String) getArguments().get(EXTRA_NEGATIVE_BUTTON);
            // populate
            init();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup groupContainer,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_dialog, groupContainer);

        titleText = (TextView) v.findViewById(R.id.textMapPopupTitle);
        descriptionText = (TextView) v.findViewById(R.id.textMapPopupDescription);

        posButton = (Button) v.findViewById(R.id.dialogPositiveButton);
        negButton = (Button) v.findViewById(R.id.dialogNegativeButton);

        linearLayout = (LinearLayout) v.findViewById(R.id.dialogButtonsContainer);


        setPositiveClickListener(mPosListener);
        setNegativeClickListener(mNegListener);


        return v;
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void init() {

        setTitle(title);
        setDescription(description);
        setPositiveButton(positiveButtonText);
        setNegativeButton(negativeButtonText);

        if (negativeButtonText == null && positiveButtonText == null)
             linearLayout.setVisibility(View.GONE);
        else
            linearLayout.setVisibility(View.VISIBLE);
    }


    public void setDescription(String _description) {
        description = _description;
        descriptionText.setText(Html.fromHtml(_description));

        descriptionText.setVisibility( (description!=null) ? View.VISIBLE : View.GONE);
    }

    public void setTitle(String _title) {
        title = _title;
        titleText.setText(_title);

        titleText.setVisibility( (title!=null) ? View.VISIBLE : View.GONE);

    }

    public void setPositiveButton(String _posButtonText) {
        positiveButtonText = _posButtonText;
        posButton.setText(_posButtonText);

        posButton.setVisibility( (positiveButtonText!=null) ? View.VISIBLE : View.GONE);
    }

    public void setNegativeButton(String _negButtonText) {
        negativeButtonText = _negButtonText;
        negButton.setText(_negButtonText);

        negButton.setVisibility( (negativeButtonText!=null) ? View.VISIBLE : View.GONE);
    }

    private void setPositiveClickListener(View.OnClickListener listener)
    {
        if (listener != null)
            posButton.setOnClickListener(listener);

    }

    private void setNegativeClickListener(View.OnClickListener listener)
    {
        if (listener != null)
            negButton.setOnClickListener(listener);

    }


}