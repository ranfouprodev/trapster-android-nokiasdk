package com.trapster.android.activity;

import android.os.Bundle;

import com.google.inject.Inject;
import com.here.android.mapping.Map;
import com.trapster.android.R;
import com.trapster.android.activity.fragment.AbstractMapFragment;
import com.trapster.android.activity.fragment.BlankMapFragment;
import com.trapster.android.activity.fragment.BlankMapFragment.OnBlankMapReadyListener;
import com.trapster.android.activity.fragment.ReportTrapFragment;
import com.trapster.android.manager.FlurryManager;
import com.trapster.android.manager.GlobalMapEventManager;
import com.trapster.android.manager.MapController;
import com.trapster.android.manager.TrapManager;
import com.trapster.android.manager.flurry.FLURRY_EVENT_NAME;
import com.trapster.android.manager.flurry.FLURRY_PARAMETER_NAME;
import com.trapster.android.manager.flurry.FlurryEvent;
import com.trapster.android.model.TrapType;

import roboguice.inject.ContentView;
import roboguice.inject.InjectFragment;

/**
 * Allows the user to create and position a new trap
 * 
 * The traptype needs to be passed in as a parameter otherwise armageddon
 * 
 * @author John
 *
 */
@ContentView(R.layout.screen_report_trap)
public class ReportTrapScreen extends Screen {

	// The trap type ID needs to be passed in
	public static final String INTENT_EXTRA_TRAPTYPE_ID = "INTENTTRAPTYPE";
    public static final String INTENT_EXTRA_TRAP_LOCATION_LAT = "INTENTTRAPLOCATIONLAT";
    public static final String INTENT_EXTRA_TRAP_LOCATION_LNG = "INTENTTRAPLOCATIONLNG";

	private static final String LOGNAME = "ReportTrap";
    public static final String INTENT_EXTRA_MAP_CONFIG ="INTENTMAPCONFIG"; ;

    @InjectFragment(R.id.fragmentReportTrap) ReportTrapFragment reportTrapFragment;
	@InjectFragment(R.id.fragmentMap) BlankMapFragment mapFragment;

	
	@Inject TrapManager trapManager;
	
	@Inject MapController mapController;

    @Inject GlobalMapEventManager globalMapEventManager;

	private TrapType trapType;

    private final static String CLASS_NAME = "ReportTrapScreen";
    private AbstractMapFragment.MapConfig mapConfig;

    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if(getIntent() == null || !getIntent().hasExtra(INTENT_EXTRA_TRAPTYPE_ID)){
		}
		
		setTitle(R.string.report_trap_placement_title);

		trapType = trapManager.getTrapType(getIntent().getIntExtra(INTENT_EXTRA_TRAPTYPE_ID, 1));
		
		reportTrapFragment.setTrapType(trapType);
        reportTrapFragment.setLatitude(getIntent().getDoubleExtra(INTENT_EXTRA_TRAP_LOCATION_LAT, 0.0));
        reportTrapFragment.setLongitude(getIntent().getDoubleExtra(INTENT_EXTRA_TRAP_LOCATION_LNG, 0.0));
		
		sendFlurryNewTrapStartedEvent(trapType);


        mapConfig = getIntent().getParcelableExtra(INTENT_EXTRA_MAP_CONFIG);
        if (mapConfig != null)
            mapFragment.setCurrentMapState(mapConfig);
       // Log.v(LOGNAME,"Map:"+mapConfig.toString());

		mapFragment.setOnBlankMapReadyListener(new OnBlankMapReadyListener(){

			@Override
			public void onMapReady(Map map) {
				reportTrapFragment.setMap(map);
			}
			
		});

	}
	

	@Override
	public void onResume() {
		super.onResume();
		
		mapFragment.addMapEventListener(globalMapEventManager);
 	
	}

	@Override
	protected void onPause() {
		super.onPause();
		
		//mapFragment.getMapView().removeMapViewEventListener(reportTrapFragment);
        mapFragment.removeMapEventListener(globalMapEventManager);
 	}

	
	
	/**
	 * MapController.getMap will return the master map. Report Trap uses a blank map so it will have to be pulled from the 
	 * blank map fragment
	 * 
	 * @return
	 */
	public Map getMap(){
		return mapFragment.getMap(); 
	}

    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }

    private void sendFlurryNewTrapStartedEvent(TrapType trapType)
    {
        FlurryEvent event = FlurryManager.createEvent(FLURRY_EVENT_NAME.NEW_TRAP_REPORT);

        String trapName = trapType.getName();
        String englishName = trapType.getUnlocalizedName();

        event.putParameter(FLURRY_PARAMETER_NAME.TRAP_TYPE, englishName);
        event.putParameter(FLURRY_PARAMETER_NAME.TRAP_TYPE_LANGUAGE, trapName);
        event.putParameter(FLURRY_PARAMETER_NAME.LOCATION_COUNTRY, FlurryManager.getCountryCode());
        event.putParameter(FLURRY_PARAMETER_NAME.LOCATION_STATE, FlurryManager.getStateCode());
        event.putParameter(FLURRY_PARAMETER_NAME.DRIVING_SPEED, FlurryManager.getDrivingSpeed());

        event.send();
    }
}
