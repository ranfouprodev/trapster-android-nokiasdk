package com.trapster.android.activity.fragment.login;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.google.inject.Inject;
import com.trapster.android.R;
import com.trapster.android.activity.LoginScreen;
import com.trapster.android.comms.EmailAvailableListener;
import com.trapster.android.comms.rest.RESTUserAPI;
import com.trapster.android.model.User;
import com.trapster.android.util.UsernameUtils;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;

import java.util.Arrays;

import roboguice.inject.InjectView;

/**
 * Created by John on 8/15/13.
 */
public class StandardLoginFragment extends AbstractLoginFragment {

    private static final String LOGNAME = "StandardLoginFragment";
    private static final int EMAIL_TAKEN = 0;
    private static final int EMAIL_AVAILABLE = 1;

    @Inject
    RESTUserAPI api;

   @InjectView(R.id.editPasswd)
    EditText editPassword;

    @InjectView(R.id.editEmail)
    EditText editEmail;

    @InjectView(R.id.layoutEmailError)
    LinearLayout layoutEmailError;


    @InjectView(R.id.layoutCheckPassword)
    LinearLayout layoutCheckPassword;



    @InjectView(R.id.textLoginWhyJoin) TextView textWhyJoin;

    @InjectView(R.id.buttonRegister)
    Button buttonJoin;


    @InjectView(R.id.buttonLogin)
    Button loginButton;

    @InjectView(R.id.buttonFacebookLogin)
    private LoginButton fbLoginButton;

    @InjectView(R.id.progressLoading)
    ProgressBar loadingIndicator;

    private UiLifecycleHelper uiHelper;

    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    private Handler setCheckHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what){
                case EMAIL_TAKEN:
                     layoutEmailError.setVisibility(View.VISIBLE);
                     editEmail.setBackgroundResource(R.drawable.text_field_error);
                    break;
                case EMAIL_AVAILABLE:
                    layoutEmailError.setVisibility(View.INVISIBLE);
                    editEmail.setBackgroundResource(R.drawable.text_field);
                    break;
            }
        }
    };

    private EmailTakenListener emailTakenListener;
    private Handler checkUserNameHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            emailTakenListener = new EmailTakenListener(false);
            api.isEmailTaken(editEmail.getText().toString().trim(), emailTakenListener, emailTakenListener);
        }
    };
    private boolean isValidEmail;


    public static StandardLoginFragment newInstance(Bundle args){
        StandardLoginFragment f = new StandardLoginFragment();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatus(Status.Available);

        getParentActivity().setTitle(R.string.login_title);

        Session session = new Session(getParentActivity().getApplicationContext());

        uiHelper = new UiLifecycleHelper(getActivity(), callback);
        uiHelper.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login_standard,null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        buttonJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              signupUser();
            }
        });

        editEmail.setText(UsernameUtils.getPrimaryEmailAccount(getLoginActivity()));

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLoginActivity().show(LoginScreen.SCREEN_LOGIN);
            }
        });

        fbLoginButton.setReadPermissions(Arrays.asList("email"));
        fbLoginButton.setFragment(this);

        editEmail.setText(UsernameUtils.getPrimaryEmailAccount(getLoginActivity()));

        layoutCheckPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                    switch (event.getAction() & MotionEvent.ACTION_MASK) {

                        case MotionEvent.ACTION_DOWN:
                            editPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                            break;
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_OUTSIDE:
                            editPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                            break;
                        case MotionEvent.ACTION_POINTER_DOWN:
                            break;
                        case MotionEvent.ACTION_POINTER_UP:
                            break;
                        case MotionEvent.ACTION_MOVE:
                            break;
                    }

                    return true;

            }
        });


        editEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(!hasFocus){
                    checkUserNameHandler.removeMessages(0);
                    checkUserNameHandler.sendEmptyMessageDelayed(0,250);
                }
            }
        });

        editEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                // resets the errors
                setCheckHandler.sendEmptyMessage(EMAIL_AVAILABLE);

                checkUserNameHandler.removeMessages(0);
                checkUserNameHandler.sendEmptyMessageDelayed(0,1500);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }

    private void signupUser() {

        if(!isValidEmail){
            checkUserNameHandler.removeMessages(0);
            emailTakenListener = new EmailTakenListener(true);
            api.isEmailTaken(editEmail.getText().toString().trim(), emailTakenListener, emailTakenListener);
        }else{

            String email = editEmail.getText().toString().trim();
            String password = editPassword.getText().toString().trim();


            if(validationChecks(email,  password)){

                User user = new User();
                user.setEmailCredentials(email,password);

                getLoginActivity().sendChooseUsername(user);
            }
        }


    }


    private boolean validationChecks(String email, String password)
    {

        //Validation checks prior to data call.
        if(!LoginUtil.isEmailValid(email))
        {
            showHighlightedEditText(R.id.editEmail);
            showAlertMessage(getString(R.string.signup_email_error));
            return false;
        }

        if (!LoginUtil.isPasswordMinLength(password))
        {
            showHighlightedEditText(R.id.editPasswd);
            showAlertMessage(getString(R.string.signup_password_error));
            return false;
        }

        return true;
    }



    @Override
    void disable() {
        buttonJoin.setEnabled(false);
        fbLoginButton.setEnabled(false);
    }

    @Override
    void enable() {
        buttonJoin.setEnabled(true);
        fbLoginButton.setEnabled(true);
    }


    @Override
    public void onResume() {
        super.onResume();

        // For scenarios where the main activity is launched and user
        // session is not null, the session state change notification
        // may not be triggered. Trigger it if it's open/closed.
       /* Session session = Session.getActiveSession();
        if (session != null &&
                (session.isOpened() || session.isClosed()) ) {
            onSessionStateChange(session, session.getState(), null);
        }
*/
        uiHelper.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

    private void onSessionStateChange(final Session session, SessionState state, Exception exception) {
        if (state.isOpened()) {

            // make request to the /me API
            Request request = Request.newMeRequest(session,
                    new Request.GraphUserCallback() {
                        // callback after Graph API response with user object

                        @Override
                        public void onCompleted(GraphUser gUser,
                                                Response response) {

                            if (gUser != null) {
                                Log.v(LOGNAME,"FB:"+gUser.getName() + ","
                                        + gUser.getUsername() + ","
                                        + gUser.getId() + "," + gUser.getLink()
                                        + "," + gUser.asMap().get("email"));

                            }

                            User user = new User();

                            user.setFacebookCredentials(gUser.getName(),gUser.asMap().get("email").toString(), session.getAccessToken(),session.getExpirationDate().getTime() );

                            // Check token with server
                            getLoginActivity().sendLoginFacebook(user);

      //                      startRegister(user);
                        }
                    });
            Request.executeBatchAsync(request);


        } else if (state.isClosed()) {
            Log.i(LOGNAME, "Logged out...");
            showAlertMessage(getString(R.string.login_facebook_error));
        }
    }

    private class EmailTakenListener implements EmailAvailableListener,CommunicationStatusListener {

        private EmailTakenListener(boolean forwardRequest) {
            this.forwardRequest = forwardRequest;
        }

        private boolean forwardRequest;



        @Override
        public void onOpenConnection() {
            com.trapster.android.util.Log.v(LOGNAME, "Open connection");
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingIndicator.setVisibility(View.VISIBLE);
                }
            });
        }

        @Override
        public void onCloseConnection() {
            com.trapster.android.util.Log.v(LOGNAME, "Closed connection");
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingIndicator.setVisibility(View.GONE);
                }
            });
        }

        @Override
        public void onConnectionError(String errorMessage) {
            com.trapster.android.util.Log.e(LOGNAME, "Connection Error:" + errorMessage);
            // Not sure if this is the way to handle this
            //setCheckHandler.sendEmptyMessage(USERNAME_AVAILABLE);
        }


        @Override
        public void onEmailAvailable(String email, String displayName, long id, boolean available) {
            com.trapster.android.util.Log.v(LOGNAME, "Email available:" + available);
            setCheckHandler.sendEmptyMessage((available)?EMAIL_AVAILABLE:EMAIL_TAKEN);
            isValidEmail = available;

            if(isValidEmail && forwardRequest){
                signupUser();
            }

        }
    }

}
