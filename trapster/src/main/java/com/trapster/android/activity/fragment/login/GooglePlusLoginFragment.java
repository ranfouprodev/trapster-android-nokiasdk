package com.trapster.android.activity.fragment.login;

/**
 * Shell class for Google plus logins. Will require the google play services library to be included in the pom
 */
public class GooglePlusLoginFragment extends AbstractLoginFragment {
    @Override
    void disable() {

    }

    @Override
    void enable() {

    }/*implements View.OnClickListener,
        PlusClient.ConnectionCallbacks, PlusClient.OnConnectionFailedListener, PlusClient.OnPersonLoadedListener {

    private static final String LOGNAME = "GooglePlusLoginFragment";

    private static final int REQUEST_CODE_SIGN_IN = 1;

    private static final int REQUEST_CODE_GET_GOOGLE_PLAY_SERVICES = 2;

    @InjectView(R.id.sign_in_button)
    private SignInButton signInButton;

    private PlusClient mPlusClient;

    private ConnectionResult mConnectionResult;


    public static GooglePlusLoginFragment newInstance(Bundle args){
        GooglePlusLoginFragment f = new GooglePlusLoginFragment();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPlusClient = new PlusClient.Builder(getActivity(), this, this)
                .setVisibleActivities("http://schemas.google.com/AddActivity")
                .setScopes(Scopes.PLUS_PROFILE, Scopes.PLUS_LOGIN)
                .build();

        setStatus(Status.Available);
     }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login_googleplus,null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        signInButton.setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        mPlusClient.connect();
    }

    @Override
    public void onStop() {
        mPlusClient.disconnect();
        super.onStop();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.sign_in_button:

                setStatus(Status.Busy);

                int available = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());

                if (available != ConnectionResult.SUCCESS) {

                    if (GooglePlayServicesUtil.isUserRecoverableError(available)) {
                       showDialog( GooglePlayServicesUtil.getErrorDialog(
                                available, getActivity(), REQUEST_CODE_GET_GOOGLE_PLAY_SERVICES) );
                    }else{
                        // Show generic error dialog
                        Log.e(LOGNAME,"Unable to start play services:code:"+available);
                        setStatus(Status.Error);
                    }
                    return;
                }

                try {
                    Log.v(LOGNAME,"Requesting Signin");
                    mConnectionResult.startResolutionForResult(getActivity(), REQUEST_CODE_SIGN_IN);
                } catch (IntentSender.SendIntentException e) {
                    // Fetch a new result to start.
                    mPlusClient.connect();
                }
                break;
            *//*case R.id.sign_out_button:
                if (mPlusClient.isConnected()) {
                    mPlusClient.clearDefaultAccount();
                    mPlusClient.disconnect();
                    mPlusClient.connect();
                }
                break;*//*

        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_SIGN_IN
                || requestCode == REQUEST_CODE_GET_GOOGLE_PLAY_SERVICES) {
            if (resultCode == Activity.RESULT_OK && !mPlusClient.isConnected()
                    && !mPlusClient.isConnecting()) {

                Log.v(LOGNAME,"G+ login callback");

                // This time, connect should succeed.
                mPlusClient.connect();
            }
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {

        Log.v(LOGNAME,"Loading user profile");

        setStatus(Status.Busy);

        mPlusClient.loadPerson(this,"me");
    }

    @Override
    public void onDisconnected() {
        mPlusClient.connect();
  }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        mConnectionResult = result;
        // @TODO handle this error
    }

    @Override
    public void onPersonLoaded(ConnectionResult connectionResult, Person person) {

        Log.e(LOGNAME,"Connection:"+connectionResult.getErrorCode()+" message:"+connectionResult.toString());

        String currentPersonName = person != null && person.getDisplayName() != null
                ? person.getDisplayName()
                : "Unknown";
        Log.v(LOGNAME,"Signed in as:"+currentPersonName);
    }

    @Override
    void disable() {
        signInButton.setEnabled(false);
    }

    @Override
    void enable() {
       signInButton.setEnabled(true);
    }*/
}

