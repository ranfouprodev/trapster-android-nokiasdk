package com.trapster.android.activity.fragment.component;

import com.trapster.android.R;
import com.trapster.android.util.Log;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Class to handle the rendering and clipping of the compass dial
 * 
 * 
 * @author John
 * 
 */
public class CompassTextView extends TextView {

	private static final String LOGNAME = "CompassTextView";

	private int bearing;
	
	String[] bearings = new String[]{"N","NE","E","SE","S","SW","W","NW"};
	
	public CompassTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public CompassTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public CompassTextView(Context context) {
		super(context);
		init();
	}

	private void init() {
	}

/*	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(heightMeasureSpec, widthMeasureSpec);
		setMeasuredDimension(getMeasuredHeight(), getMeasuredWidth());
	}

	@Override
	protected boolean setFrame(int l, int t, int r, int b) {
		return super.setFrame(l, t, l + (b - t), t + (r - l));
	}
*/
	@Override
	public void onDraw(Canvas canvas) {
		
	      canvas.save();

	     // Translate the text to adjust for the +/- 45 degrees
	     float incremental = (((float)bearing) % 45) / 45; 
	      
	     float characterWidth = getPaint().measureText("..");
	     
	     int nearestBearing = (int)(Math.floor(bearing / 45)); 
	     float currentDirectionWidth = getPaint().measureText(bearings[nearestBearing]);
	     
	     float pixelWidth = (characterWidth) + (0.5f*currentDirectionWidth);
	     
	     float translate = (pixelWidth * incremental);
	     
	     canvas.translate(-translate, 0);
	      
	      super.onDraw(canvas);
	      
	      canvas.restore();
	}

	public int getBearing() {
		return bearing;
	}

	
	
	
	public void setBearing(int _bearing) {
		
		
		this.bearing = (_bearing % 360);
		
		int nearestBearing = (int)(Math.floor(bearing / 45)); 
		
		StringBuilder compassText = new StringBuilder(); 
		
		
		compassText.append((nearestBearing<=1)?bearings[6]:bearings[nearestBearing-2]);
		
		compassText.append("..");
		
		compassText.append((nearestBearing==0)?bearings[7]:bearings[nearestBearing-1]);
		
		compassText.append("..");
		
		compassText.append(bearings[nearestBearing]);
		
		compassText.append("..");
		
		compassText.append((nearestBearing==7)?bearings[0]:bearings[nearestBearing+1]);
		
		compassText.append("..");
		
		compassText.append((nearestBearing>=6)?bearings[1]:bearings[nearestBearing+2]);
		
		
		String compass = compassText.toString(); 
		int length = compass.length(); 
		
		String shortCompass = compass.substring((length/2)-6, (length/2)+6);
		
		
		setText(shortCompass);
		
		invalidate();
	}

}
