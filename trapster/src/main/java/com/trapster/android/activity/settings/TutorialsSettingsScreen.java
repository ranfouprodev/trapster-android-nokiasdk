package com.trapster.android.activity.settings;

import android.content.Intent;
import android.view.View;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.activity.HelpSectionScreen;
import com.trapster.android.activity.TutorialScreen;
import com.trapster.android.activity.settings.fragment.TextSettingFragment;

public class TutorialsSettingsScreen extends AbstractSettingsScreen
{
    private static final String CLASS_NAME = "Tutorials Screen";

    @Override
    protected void setupFragments()
    {
        // How Does Trapster Work should be an option here
        TextSettingFragment tutorialFragment = (TextSettingFragment) createSettingsFragment(FRAGMENT_TYPE.TEXT_ONLY, TutorialScreen.class);
        setupHelpLink((TextSettingFragment) createSettingsFragment(FRAGMENT_TYPE.TEXT_ONLY), R.string.help_feedback_report_traps, R.layout.screen_help_report_traps, R.string.report_trap_title);
        setupHelpLink((TextSettingFragment) createSettingsFragment(FRAGMENT_TYPE.TEXT_ONLY), R.string.help_feedback_rate_traps, R.layout.screen_rate_trap, R.string.rate_trap_title);
        setupHelpLink((TextSettingFragment) createSettingsFragment(FRAGMENT_TYPE.TEXT_ONLY), R.string.help_feedback_trap_colors, R.layout.screen_trap_color, R.string.trap_color_title);
        setupHelpLink((TextSettingFragment) createSettingsFragment(FRAGMENT_TYPE.TEXT_ONLY), R.string.help_feedback_trap_types, R.layout.screen_trap_types, R.string.trap_types_title);
        setupHelpLink((TextSettingFragment) createSettingsFragment(FRAGMENT_TYPE.TEXT_ONLY), R.string.help_feedback_trap_filters, R.layout.screen_trap_filters, R.string.trap_filter_title);
        setupHelpLink((TextSettingFragment) createSettingsFragment(FRAGMENT_TYPE.TEXT_ONLY), R.string.help_feedback_speed_limit, R.layout.screen_speed_limit, R.string.speed_limit_title);
        setupHelpLink((TextSettingFragment) createSettingsFragment(FRAGMENT_TYPE.TEXT_ONLY), R.string.help_feedback_bg_mode, R.layout.screen_bg_mode, R.string.bg_mode_title);

        tutorialFragment.setDescription(R.string.help_feedback_trapster_tutorial);
    }

    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }

    private void setupHelpLink(TextSettingFragment fragment, int descriptionResource, final int layout, final int helpTitle)
    {
        fragment.setDescription(descriptionResource);
        fragment.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(getApplicationContext(), HelpSectionScreen.class);
                intent.putExtra(Defaults.INTENT_HELP_VIEW_ID, layout);
                intent.putExtra(Defaults.INTENT_HELP_VIEW_TITLE, getString(helpTitle));
                startActivity(intent);
            }
        });
    }
}
