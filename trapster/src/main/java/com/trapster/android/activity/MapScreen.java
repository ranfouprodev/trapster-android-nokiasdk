package com.trapster.android.activity;

import android.content.*;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


import com.google.inject.Inject;
import com.trapster.android.BuildFeatures;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.activity.fragment.*;
import com.trapster.android.activity.fragment.*;

import com.trapster.android.activity.fragment.DashboardFragment;
import com.trapster.android.activity.fragment.DashboardTutorialFragment;
import com.trapster.android.activity.fragment.InfoOverlayFragment;
import com.trapster.android.activity.fragment.MapFragment;
import com.trapster.android.activity.fragment.RateUsFragment;

import com.trapster.android.activity.fragment.AbstractMapFragment;
import com.trapster.android.activity.fragment.CenterOnMeFragment;
import com.trapster.android.activity.fragment.ErrorStatusBarFragment;
import com.trapster.android.activity.fragment.SpeedLimitFragment;
import com.trapster.android.activity.fragment.TrapPopupFragment;
import com.trapster.android.activity.settings.DashMenuSettingsScreen;
import com.trapster.android.comms.ITrapsterAPI;
import com.trapster.android.comms.LaunchMessageListener;
import com.trapster.android.manager.*;
import com.trapster.android.manager.*;
import com.trapster.android.manager.CompositePositionIndicatorManager;
import com.trapster.android.manager.MapController;
import com.trapster.android.manager.PositionManager;
import com.trapster.android.manager.TrapManager;
import com.trapster.android.manager.TrapUpdateManager;

import com.trapster.android.model.LaunchMessage;
import com.trapster.android.model.Trap;
import com.trapster.android.model.TrapType;
import com.trapster.android.opengl.CompositeGLView;
import com.trapster.android.opengl.CompositeTextureView;
import com.trapster.android.opengl.CompositeTextureView44;
import com.trapster.android.opengl.CompositeView;

import com.trapster.android.service.TrapsterService;

import com.trapster.android.util.BackgroundTask;
import com.trapster.android.util.MapLoadCompleteListener;
import com.trapster.android.util.TrapsterError;
import com.trapster.android.widget.TrapsterWidgetUpdateService;
import roboguice.inject.ContentView;
import roboguice.inject.InjectFragment;
import roboguice.inject.InjectView;


@ContentView(R.layout.screen_map)
public class MapScreen extends Screen
{
    private double MAXIMUM_SPEED_FOR_FEEDBACK_POPUP = 2.2352;
    private long TIME_BETWEEN_FEEDBACK_POPUPS = 1000 * 30; // 30 Seconds
    private static final String CLASS_NAME = "MapScreen";
    public static final int RESPONSE_CODE_QUIT_APPLICATION= 1001;
    private static final double MAX_CURRENT_SPEED = 225.0; //in Meters per Second. equivalent to 500 MPH
    private static final long MAP_STARTUP_DELAY = 1500;
    private static final long COMPONENT_STARTUP_DELAY = 250;
    private static final int NUMBER_OF_RUNS_PER_RATE_US_DISPLAY = 5;
    private static final double MINIMUM_SPEED_FOR_POPUP = 5.0;
    private static final long TRAP_POPUP_DURATION = 7 * 1000;

    private static final String BUNDLE_KEY_REQUIRE_COMPONENT_LOAD = "requireComponentLoad";
    private static final String BUNDLE_KEY_REQUIRE_MAP_LOAD = "requireMapLoad";

    private static final String MAP_FRAGMENT_TAG = "mapFragment";
    private static final String ERROR_FRAGMENT_TAG = "errorFragment";
    private static final String TRAP_POPUP_FRAGMENT_TAG = "trapPopupFragment";
    private static final String MAP_WAKE_LOCK_NAME = "com.trapster.android.TRAPSTER_MAP";

    @Inject CompositePositionIndicatorManager positionIndicatorManager;
    @Inject PositionManager positionManager;
    @Inject ITrapsterAPI api;
    @Inject SharedPreferences sharedPreferences;
    @Inject MapController mapController;
    @Inject TrapManager trapManager;
    @Inject SessionManager sessionManager;

    @InjectFragment(R.id.speedLimitFragment) SpeedLimitFragment speedLimitFragment;
    private ErrorStatusBarFragment errorStatusBarFragment;
    private MapFragment mapFragment; // Intentionally not injected
    private InfoOverlayFragment infoOverlayFragment;
    private TrapPopupFragment trapPopupFragment;
    private RateUsFragment rateUsFragment;
    private DashboardTutorialFragment dashboardTutorialFragment;
    private UserFeedbackFragment userFeedbackFragment;

    @InjectView(R.id.compositeViewContainer) LinearLayout compositeViewContainer;
    @InjectView(R.id.fullPageContainer) RelativeLayout fullPageContainer;
    @InjectView(R.id.mapContainer) LinearLayout mapContainer;
    @InjectView(R.id.dashboardContainer) LinearLayout dashboardContainer;
    @InjectView(R.id.centerOnMeContainer) LinearLayout centerOnMeContainer;
    @InjectView(R.id.trapPopupContainer) LinearLayout trapPopupContainer;
    @InjectView(R.id.mapParentContainer) RelativeLayout parentContainer;

    private CompositeView compositeView;

    private boolean bound;
    private long startTime;
    private long endTime;
    private boolean shouldShowLaunchMessage = true;
    private boolean hasCheckedForPopups = false;

    private final Handler handler = new Handler();
    private final Handler trapPopupHandler = new Handler();
    private final TrapUpdater trapUpdater = new TrapUpdater();
    private final UserFeedbackPopupListener userFeedbackPopupListener = new UserFeedbackPopupListener();
    private PowerManager.WakeLock wakeLock;

    private boolean needsComponentLoad = true;
    private boolean needsMapLoad = true;

    private final CheckCompositeOverlayRunnable checkCompositeOverlayRunnable = new CheckCompositeOverlayRunnable();


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        startTime = System.currentTimeMillis();

        wakeLock = ((PowerManager)getSystemService(Context.POWER_SERVICE)).newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, MAP_WAKE_LOCK_NAME);
        wakeLock.acquire();
        long start = startTime;
        super.onCreate(savedInstanceState);

        if (BuildFeatures.SUPPORTS_KITKAT)
            compositeView = new CompositeTextureView44(this);
        else if (BuildFeatures.SUPPORTS_ICS)
            compositeView = new CompositeTextureView(this);
        else
            compositeView = new CompositeGLView(this);
        compositeViewContainer.addView((View) compositeView);

        if (savedInstanceState == null)
        {
            showMap();
        }
        else
        {
            needsComponentLoad = savedInstanceState.getBoolean(BUNDLE_KEY_REQUIRE_COMPONENT_LOAD);
            needsMapLoad = savedInstanceState.getBoolean(BUNDLE_KEY_REQUIRE_MAP_LOAD);
            //loadComponents();
        }


        long end = System.currentTimeMillis();

        //Log.i("BBQ", "onCreate took: " + (end - start));
    }

    @Override
    public void onStart()
    {
        super.onStart();
        // This ensures the Trapster Service is running
        Intent i= new Intent(this, TrapsterService.class);
        startService(i);

        bindService(i, mConnection, Context.BIND_AUTO_CREATE);

    }

    @Override
    public void onStop() {
        super.onStop();
        handler.removeCallbacks(null);
        // Unbind from the service
        unbindService(mConnection);

    }


    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // avoids the service being killed. which shouldn't happen but does
            bound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            bound = false;
        }
    };


    @Override
    public void onResume()
    {
        super.onResume();

        sessionManager.addUserFeedbackListener(userFeedbackPopupListener);

        mapController.setCenterOnMe(mapController.isCenterOnMe()); // Update the center after rotation

        speedLimitFragment.updateCurrentSpeedLimit();
        TrapUpdateManager.addListener(trapUpdater);

        loadComponents();

        checkCompositeViewVisibility();
    }

    @Override
    public void onPause()
    {
        long start = System.currentTimeMillis();
        super.onPause();
        TrapUpdateManager.removeListener(trapUpdater);
        sessionManager.removeUserFeedbackListener(userFeedbackPopupListener);

        handler.removeCallbacksAndMessages(null);

        // Since the loading time after a fresh launch (after installing for the first time) can take a really
        // long time for some reason, we temporarily hide it when this activity goes away (it comes back
        // when the user returns).
        ErrorStatusBarFragment errorStatusBarFragment = (ErrorStatusBarFragment) getSupportFragmentManager().findFragmentByTag(ERROR_FRAGMENT_TAG);
        if (errorStatusBarFragment != null)
        {
            errorStatusBarFragment.hide();
        }
    }

    @Override
    public void onPostResume()
    {
        super.onPostResume();

        endTime = System.currentTimeMillis();

    }

    @Override
    public void onDestroy()
    {
        wakeLock.release();
        super.onDestroy();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        shutdownApp();

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent e)
    {
        switch (keyCode)
        {
            case KeyEvent.KEYCODE_MENU:
            {
                Intent intent = new Intent(this, DashMenuSettingsScreen.class);
                startActivityForResult(intent, MapScreen.RESPONSE_CODE_QUIT_APPLICATION);
                return true;
            }
        }

        return super.onKeyDown(keyCode, e);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState)
    {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putBoolean(BUNDLE_KEY_REQUIRE_COMPONENT_LOAD, needsComponentLoad);
        savedInstanceState.putBoolean(BUNDLE_KEY_REQUIRE_MAP_LOAD, needsMapLoad);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState)
    {
        if (savedInstanceState != null)
        {
            trapPopupFragment = (TrapPopupFragment) getSupportFragmentManager().findFragmentByTag(TRAP_POPUP_FRAGMENT_TAG);
            if (trapPopupFragment != null)
                trapPopupFragment.hide();
            mapFragment = (MapFragment) getSupportFragmentManager().findFragmentByTag(MAP_FRAGMENT_TAG);
            positionIndicatorManager.setupForView(compositeView, mapFragment);
            errorStatusBarFragment = (ErrorStatusBarFragment) getSupportFragmentManager().findFragmentByTag(ERROR_FRAGMENT_TAG);
            if (errorStatusBarFragment != null)
            {
                errorStatusBarFragment.onMapLoaded();
                errorStatusBarFragment.hide();
            }
        }
    }

    private void shutdownApp()
    {
        shutdownService();
        incrementStartCount();
        finish();
    }

    private void shutdownService() {

        sharedPreferences.edit().putBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_FOLLOW, true).commit();

        Intent disableIntent = new Intent(this, TrapsterWidgetUpdateService.class);
        disableIntent.putExtra(TrapsterWidgetUpdateService.TOGGLESERVICE, true);

        startService(disableIntent);
        stopService(new Intent(this, TrapsterService.class));
    }

    private void incrementStartCount()
    {
        new BackgroundTask()
        {

            @Override
            public void onExecute()
            {
                SharedPreferences sharedPreferences = getSharedPreferences(Defaults.PREF_SETTINGS, Context.MODE_PRIVATE);
                int numStarts = sharedPreferences.getInt(Defaults.RATE_US_APP_START_COUNT, 0);
                numStarts++;
                sharedPreferences.edit().putInt(Defaults.RATE_US_APP_START_COUNT, numStarts).commit();
            }

            @Override public void onTimeout(long runTime){}
        }.execute();

    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESPONSE_CODE_QUIT_APPLICATION)
            shutdownApp();
    }

    public static void returnToMap(Context context)
    {
        Intent i = new Intent(context, MapScreen.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(i);
    }

    private void showMap()
    {
        showInfoOverlay();
        //loadComponents();
    }

    private void loadComponents()
    {
        if (needsComponentLoad)
        {
            handler.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

                    transaction.add(centerOnMeContainer.getId(), new CenterOnMeFragment());
                    transaction.add(dashboardContainer.getId(), new DashboardFragment());
                    errorStatusBarFragment = new ErrorStatusBarFragment();
                    transaction.add(parentContainer.getId(), errorStatusBarFragment, ERROR_FRAGMENT_TAG);
                    transaction.hide(errorStatusBarFragment); // This will allow it to run, but not be initially visible
                    transaction.commit();
                    needsComponentLoad = false;
                }
            }, COMPONENT_STARTUP_DELAY);
        }

        if (needsMapLoad)
        {
            handler.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    mapFragment = new MapFragment();
                    transaction.add(mapContainer.getId(), mapFragment, MAP_FRAGMENT_TAG);
                    positionIndicatorManager.setupForView(compositeView, mapFragment);
                    trapPopupFragment = new TrapPopupFragment();
                    transaction.add(trapPopupContainer.getId(), trapPopupFragment, TRAP_POPUP_FRAGMENT_TAG);
                    transaction.hide(trapPopupFragment);

                    transaction.commit();

                    new MapLoadCompleteListener(mapFragment, new MapLoadCompleteListener.OnMapLoadCompleted()
                    {
                        @Override
                        public void onMapLoadCompleted()
                        {
                            if (infoOverlayFragment  != null)
                                infoOverlayFragment.onMapLoaded();
                            if (errorStatusBarFragment != null)
                                errorStatusBarFragment.onMapLoaded();
                        }
                    });

                    needsMapLoad = false;
                }
            }, MAP_STARTUP_DELAY);
        }
    }

    private void showInfoOverlay()
    {
        try
        {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            infoOverlayFragment = new InfoOverlayFragment();
            transaction.add(fullPageContainer.getId(), infoOverlayFragment);
            transaction.setCustomAnimations(0, R.anim.fade_out);

            infoOverlayFragment.setOnContinueRunnable(checkCompositeOverlayRunnable);
            transaction.commitAllowingStateLoss();
        }
        catch (Exception e){}

    }

    private void showRateUs()
    {
        try
        {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            rateUsFragment = new RateUsFragment();
            transaction.add(fullPageContainer.getId(), rateUsFragment);
            transaction.commitAllowingStateLoss();
        }
        catch (Exception e){}
    }

    private void showTutorial()
    {
        try
        {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            dashboardTutorialFragment = new DashboardTutorialFragment();
            dashboardTutorialFragment.setOnDismissRunnable(checkCompositeOverlayRunnable);
            transaction.add(fullPageContainer.getId(), dashboardTutorialFragment);
            transaction.setCustomAnimations(0, R.anim.fade_out);
            transaction.commitAllowingStateLoss();
        }
        catch (Exception e){}
    }

    private void showLaunchMessage(final LaunchMessage launchMessage)
    {
        shouldShowLaunchMessage = false;

        if(launchMessage == null)
            return;
        if (launchMessage.getUrl() == null)
            return;

        if(launchMessage.getDisplayText().equals("WEBVIEW"))
        {
            Uri uri = Uri.parse(launchMessage.getUrl());
            Intent intent = new Intent (Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }
        else
        {
            View.OnClickListener negAction = new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    hideDialog();
                }
            };
            View.OnClickListener posAction = new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    Uri uri = Uri.parse(launchMessage.getUrl());
                    Intent intent = new Intent (Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                    hideDialog();
                }
            };

            showDialog(launchMessage.getTitle(), launchMessage.getMessage(), launchMessage.getButtonText(),
                    getString(R.string.preference_cancel), posAction, negAction, 0);
        }
    }

    private void showTrapPopup(final Trap trap)
    {
        final TrapPopupFragment trapPopupFragment = getTrapPopupFragment();

        if (trapPopupFragment != null)
        {
            Trap currentTrap = trapPopupFragment.getTrap();
            if (currentTrap != null)
                trapManager.addToDismissTraps(currentTrap.getId());
            trapPopupFragment.setTrap(trap);
            trapPopupFragment.setOnVoteListener(new TrapPopupFragment.OnVoteListener()
            {
                @Override
                public void onVote(boolean vote, Trap trap)
                {
                    api.rateTrap(vote, trap, null, null);
                    dismissTrapPopup();
                }
            });

            trapPopupFragment.showWithAnimation(R.anim.slide_in_right);
            //
            trapPopupHandler.removeCallbacks(null);
            trapPopupHandler.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    dismissTrapPopup();
                }
            }, TRAP_POPUP_DURATION);
        }


    }

    private void dismissTrapPopup()
    {

        Trap trap = trapPopupFragment.getTrap();
        if (trap != null)
            trapManager.addToDismissTraps(trap.getId());
        trapPopupFragment.hide();
    }

    private TrapPopupFragment getTrapPopupFragment()
    {
        if (trapPopupFragment != null)
            return trapPopupFragment;
        trapPopupFragment = (TrapPopupFragment) getSupportFragmentManager().findFragmentByTag(TRAP_POPUP_FRAGMENT_TAG);

        return trapPopupFragment;
    }

    private void checkForPopups()
    {
        if (!hasCheckedForPopups)
        {
            hasCheckedForPopups = true;
            double speed = positionManager.getLastLocation().getSpeed();
            //if you are travelling greater than 224 MPH you are really standing still.
            if(speed > MAX_CURRENT_SPEED)
                speed = 0;

            if(!sharedPreferences.getBoolean(Defaults.PREFERENCE_HIDE_DASHBOARD_TUTORIAL, false))
            {
                showTutorial();
            }
            else if (speed <= MINIMUM_SPEED_FOR_POPUP)
            {
                if (shouldShowLaunchMessage)
                {
                    api.getLaunchMessage(new LaunchMessageListener()
                    {
                        @Override
                        public void onLaunchMessage(LaunchMessage launchMessage)
                        {
                            showLaunchMessage(launchMessage);
                        }
                        @Override public void onError(TrapsterError error) {}
                    }, null);
                }
                else
                {
                    boolean hasSentFeedback = sharedPreferences.getBoolean(Defaults.RATE_US_DIALOG_FEEDBACK_SENT, false);
                    if (!hasSentFeedback)
                    {
                        int numStarts = sharedPreferences.getInt(Defaults.RATE_US_APP_START_COUNT, 0);
                        if (numStarts >= NUMBER_OF_RUNS_PER_RATE_US_DISPLAY && speed <= MINIMUM_SPEED_FOR_POPUP)
                        {
                            showRateUs();
                        }

                    }
                }
            }
        }

        checkCompositeViewVisibility();
    }

    public void checkCompositeViewVisibility()
    {
        handler.post(new Runnable()
        {
            @Override
            public void run()
            {
                boolean rateUsIsVisible = rateUsFragment != null && rateUsFragment.getVisibleState();
                boolean infoOverlayIsVisible = infoOverlayFragment != null && infoOverlayFragment.getVisibleState();
                boolean dashboardIsVisible = dashboardTutorialFragment != null && dashboardTutorialFragment.getVisibleState();

                if (!rateUsIsVisible && !infoOverlayIsVisible && !dashboardIsVisible)
                    compositeView.setVisibility(View.VISIBLE);
                else
                    compositeView.setVisibility(View.INVISIBLE);
            }
        });

    }

    public AbstractMapFragment.MapConfig getCurrentMapConfig()
    {
        if (mapFragment != null)
            return mapFragment.getCurrentMapConfig();
        else
            return null;
    }

    @Override
    public boolean shouldUseActionBar()
    {
        return false;
    }

    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }

    private class TrapPopupRunnable implements Runnable
    {
        private Trap trap;
        public TrapPopupRunnable(Trap trap)
        {
            this.trap = trap;
        }

        public void run()
        {
            if (trap != null)
            {
                TrapType trapType = trapManager.getTrapType(trap.getTypeid());
                if (trapType != null && trapType.isAlertable())
                {
                    //Log.i("BBQ", "Showing notification at " + System.currentTimeMillis());

                    if(sharedPreferences.getBoolean(Defaults.PREFERENCE_SHOW_TUTORIAL,true))
                    {
                        showTrapPopup(trap);
                    }
                }
            }

        }
    }

    private class TrapUpdater implements TrapUpdateManager.TrapUpdater
    {
        @Override
        public void onTrapUpdate(Trap trap)
        {
            handler.post(new TrapPopupRunnable(trap));
        }
    }

    private class CheckCompositeOverlayRunnable implements Runnable
    {

        @Override
        public void run()
        {
            checkForPopups();
        }
    }

    private class UserFeedbackPopupListener implements SessionManager.UserFeedbackListener
    {
        @Override
        public void onUsersHelpedChanged(final int oldCount, final int newCount)
        {
            if (newCount > oldCount)
            {
                handler.post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        boolean rateUsIsVisible = rateUsFragment != null && rateUsFragment.getVisibleState();
                        boolean infoOverlayIsVisible = infoOverlayFragment != null && infoOverlayFragment.getVisibleState();
                        boolean dashboardIsVisible = dashboardTutorialFragment != null && dashboardTutorialFragment.getVisibleState();
                        double currentSpeed = positionManager.getLastLocation().getSpeed();

                        if (!rateUsIsVisible && !infoOverlayIsVisible && !dashboardIsVisible && (currentSpeed <= MAXIMUM_SPEED_FOR_FEEDBACK_POPUP))
                        {
                            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                            userFeedbackFragment = new UserFeedbackFragment();
                            Bundle arguments = new Bundle();
                            arguments.putInt(UserFeedbackFragment.BUNDLE_KEY_VOTE_COUNT, newCount - oldCount);
                            userFeedbackFragment.setArguments(arguments);
                            transaction.add(fullPageContainer.getId(), userFeedbackFragment);
                            transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
                            transaction.commitAllowingStateLoss();
                        }
                        else
                            handler.postDelayed(this, TIME_BETWEEN_FEEDBACK_POPUPS);
                    }
                });
            }
        }
    }


}
