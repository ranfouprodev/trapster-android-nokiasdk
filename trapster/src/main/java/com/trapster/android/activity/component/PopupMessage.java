package com.trapster.android.activity.component;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.inject.Inject;
import com.trapster.android.R;

import roboguice.RoboGuice;

/**
 * Created by John on 9/27/13.
 */
public class PopupMessage {

    /**
     * Show the view or text notification for a short period of time. This time
     * could be user-definable. This is the default.
     *
     * @see #setDuration
     */
    public static final int LENGTH_SHORT = 3000;

    /**
     * Show the view or text notification for a long period of time. This time
     * could be user-definable.
     *
     * @see #setDuration
     */
    public static final int LENGTH_LONG = 5000;

    /**
     * Show the text notification for a long period of time with a negative style.
     */
    public static final Style STYLE_ALERT = new Style(LENGTH_LONG, R.drawable.error_bar, R.drawable.alerting);
    public static final Style STYLE_INFO = new Style(LENGTH_SHORT, R.drawable.error_bar, R.drawable.about);

    @Inject
    PopupManager popupManager;

    private final Activity mContext;
    private int mDuration = LENGTH_SHORT;
    private View mView;
    private LayoutParams mLayoutParams;
    private boolean mFloating;

    /**
     * Construct an empty PopupMessage object. You must call {@link #setView} before
     * you can call {@link #show}.
     *
     * @param context The context to use. Usually your
     *                {@link android.app.Activity} object.
     */
    public PopupMessage(Activity context) {
        mContext = context;
        RoboGuice.injectMembers(context,this);
    }

    public static PopupMessage makeText(Activity context, CharSequence text, Style style) {
        return makeText(context, text, style.iconId, R.layout.fragment_statusbar,style);
    }

    public static PopupMessage makeText(Activity context, CharSequence text, int iconId, Style style) {
        return makeText(context, text, iconId, R.layout.fragment_statusbar,style);
    }

    public static PopupMessage makeText(Activity context, CharSequence text, int iconId, int layoutId, Style style) {
        LayoutInflater inflate = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflate.inflate(layoutId, null);

        return makeText(context, text,iconId, style, v, true);
    }

    public static PopupMessage makeText(Activity context, CharSequence text, int iconId, Style style, View customView) {
        return makeText(context, text,iconId, style, customView, false);
    }

    private static PopupMessage makeText(Activity context, CharSequence text, int iconId, Style style, View view, boolean floating) {
        PopupMessage result = new PopupMessage(context);

        view.setBackgroundResource(style.background);

        TextView tv = (TextView) view.findViewById(R.id.textErrorStatusText);
        tv.setText(text);

        ImageView icon = (ImageView) view.findViewById(R.id.imageErrorStatusIcon);
        icon.setImageResource(iconId);

        result.mView = view;
        result.mDuration = style.duration;
        result.mFloating = floating;

        return result;
    }


    public void show() {
        popupManager.add(this);
    }

    public boolean isShowing() {
        if (mFloating) {
            return mView != null && mView.getParent() != null;
        } else {
            return mView.getVisibility() == View.VISIBLE;
        }
    }

    public void cancel() {
        popupManager.clearMsg(this);
    }

    public void cancelAll() {
        popupManager.clearAllMsg();
    }

    /**
     * Return the activity.
     */
    public Activity getActivity() {
        return mContext;
    }

    /**
     * Set the view to show.
     *
     * @see #getView
     */
    public void setView(View view) {
        mView = view;
    }

    /**
     * Return the view.
     *
     * @see #setView
     */
    public View getView() {
        return mView;
    }

    /**
     * Set how long to show the view for.
     *
     * @see #LENGTH_SHORT
     * @see #LENGTH_LONG
     */
    public void setDuration(int duration) {
        mDuration = duration;
    }

    /**
     * Return the duration.
     *
     * @see #setDuration
     */
    public int getDuration() {
        return mDuration;
    }

    /**
     * Update the text in a AppMsg that was previously created using one of the makeText() methods.
     *
     * @param resId The new text for the AppMsg.
     */
    public void setText(int resId) {
        setText(mContext.getText(resId));
    }

    /**
     * Update the text in a AppMsg that was previously created using one of the makeText() methods.
     *
     * @param s The new text for the AppMsg.
     */
    public void setText(CharSequence s) {
        if (mView == null) {
            throw new RuntimeException("This AppMsg was not created with AppMsg.makeText()");
        }
        TextView tv = (TextView) mView.findViewById(android.R.id.message);
        if (tv == null) {
            throw new RuntimeException("This AppMsg was not created with AppMsg.makeText()");
        }
        tv.setText(s);
    }

    /**
     * Gets the crouton's layout parameters, constructing a default if necessary.
     *
     * @return the layout parameters
     */
    public LayoutParams getLayoutParams() {
        if (mLayoutParams == null) {
            mLayoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        }
        return mLayoutParams;
    }

    /**
     * Sets the layout parameters which will be used to display the crouton.
     *
     * @param layoutParams The layout parameters to use.
     * @return <code>this</code>, for chaining.
     */
    public PopupMessage setLayoutParams(LayoutParams layoutParams) {
        mLayoutParams = layoutParams;
        return this;
    }

    /**
     * Constructs and sets the layout parameters to have some gravity.
     *
     * @param gravity the gravity of the Crouton
     * @return <code>this</code>, for chaining.
     * @see android.view.Gravity
     */
    public PopupMessage setLayoutGravity(int gravity) {
        mLayoutParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, gravity);
        return this;
    }

    /**
     * Return the value of floating.
     *
     * @see #setFloating(boolean)
     */
    public boolean isFloating() {
        return mFloating;
    }


    public OnPopupClosedListener getOnPopupClosedListener() {
        return onPopupClosedListener;
    }

    public void setOnPopupClosedListener(OnPopupClosedListener onPopupClosedListener) {
        this.onPopupClosedListener = onPopupClosedListener;
    }

    private OnPopupClosedListener onPopupClosedListener;

    public interface OnPopupClosedListener{
        void onPopupClosed();
    }

    /**
     * Sets the value of floating.
     *
     * @param mFloating
     */
    public void setFloating(boolean mFloating) {
        this.mFloating = mFloating;
    }

    public static class Style {

        private final int duration;
        private final int background;
        private final int iconId;

        public Style(int duration, int resId, int iconId) {
            this.duration = duration;
            this.background = resId;
            this.iconId = iconId;
        }

        /**
         * Return the duration in milliseconds.
         */
        public int getDuration() {
            return duration;
        }

        /**
         * Return the resource id of background.
         */
        public int getBackground() {
            return background;
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof PopupMessage.Style)) {
                return false;
            }
            Style style = (Style) o;
            return style.duration == duration
                    && style.background == background;
        }

    }

}