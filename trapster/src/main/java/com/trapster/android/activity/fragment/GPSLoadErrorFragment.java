package com.trapster.android.activity.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.trapster.android.R;
import roboguice.inject.InjectFragment;

public class GPSLoadErrorFragment extends AbstractFragment
{
    @InjectFragment(R.id.errorGPSBottomBar) BottomBarFragment bottomBarFragment;
    private Runnable runnable;

    public GPSLoadErrorFragment()
    {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gps_load_error, null);

        return view;
    }

    public void setRetryRunnable(Runnable runnable)
    {
        this.runnable = runnable;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        bottomBarFragment.setPositiveAction(getString(R.string.error_no_gps_fragment_retry), new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                GPSLoadErrorFragment.this.hide();
                if (runnable != null)
                    runnable.run();
            }
        });

        bottomBarFragment.setNegativeAction(getString(R.string.action_menu_power), new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                getParentActivity().onBackPressed();
            }
        });
    }
}