package com.trapster.android.activity.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.google.inject.Inject;
import com.trapster.android.R;
import com.trapster.android.manager.SessionManager;
import com.trapster.android.model.Profile;
import roboguice.inject.InjectView;

import java.text.SimpleDateFormat;

/**
 * Created by John on 6/4/13.
 */
public class ProfileFragment extends  AbstractFragment{

    @InjectView(R.id.textProfileName) TextView profileName;
    @InjectView(R.id.textProfileEmail) TextView profileEmail;
    @InjectView(R.id.textProfileUserSince) TextView profileUserSince;

    @Inject
    SessionManager sessionManager;
    private Profile profile;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        profile = sessionManager.getUser().getProfile();
        profileName.setText(sessionManager.getUser().getUserName());

        if(profile != null){
            populateProfile();
        }else{
            // @TODO restart profile download?
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, null);
        return view;
    }

    private void populateProfile(){
        // Parsing user name


        //Parsing Email
        String emailText =  (profile == null || profile.getEmail()== null)?"":profile.getEmail();
        profileEmail.setText(emailText);

        //Parsing Member Since date
        SimpleDateFormat df = new SimpleDateFormat("MMM dd, yyyy");
        String sinceDate = (profile == null || profile.getSignUp() == null)? "" : getString(R.string.profile_since) +" "+ df.format(profile.getSignUp());
        profileUserSince.setText(sinceDate);
    }

}
