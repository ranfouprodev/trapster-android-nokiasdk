package com.trapster.android.activity.fragment;

import com.here.android.mapping.MapEventListener;

public interface MapLayer extends MapEventListener{
	
	
	/**
	 * Hides the MapLayer from view 
	 */
	void hide();
	
	
	/**
	 * Shows the MapLayer  
	 */
	void show(); 
	
	/*
	 * Forces a redraw of the existing MapLayer
	 */
	void refresh(); 
	
	/*
	 * Forces an update of the MapLayer. This will typically cause the MapLayer to call back to the server and update
	 * it results
	 */
	void forceUpdate(double lat, double lon, double radius);

}
