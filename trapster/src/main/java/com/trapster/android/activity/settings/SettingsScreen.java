package com.trapster.android.activity.settings;

import com.trapster.android.R;
import com.trapster.android.activity.AlertSensitivityScreen;
import com.trapster.android.activity.settings.fragment.IconSettingFragment;

public class SettingsScreen extends AbstractSettingsScreen
{
    private static final String CLASS_NAME = "Settings Screen";

    @Override
    protected void setupFragments()
    {
        IconSettingFragment trapSettingsFragment = (IconSettingFragment) createSettingsFragment(FRAGMENT_TYPE.ICON, TrapsAndAlertsSettingsScreen.class);
        IconSettingFragment alertOptionsFragment = (IconSettingFragment) createSettingsFragment(FRAGMENT_TYPE.ICON, AlertSensitivityScreen.class);
        IconSettingFragment displayOptionsFragment = (IconSettingFragment) createSettingsFragment(FRAGMENT_TYPE.ICON, DisplayOptionsSettingsScreen.class);
        IconSettingFragment soundOptionsFragment = (IconSettingFragment) createSettingsFragment(FRAGMENT_TYPE.ICON, SoundOptionsSettingsScreen.class);
        IconSettingFragment testDriveFragment = (IconSettingFragment) createSettingsFragment(FRAGMENT_TYPE.ICON, TestDriveLoginChkScreen.class);

        trapSettingsFragment.setDescription(R.string.settings_menu_trap);
        alertOptionsFragment.setDescription(R.string.settings_menu_alerts);
        displayOptionsFragment.setDescription(R.string.settings_menu_display_options);
        soundOptionsFragment.setDescription(R.string.settings_menu_audio);
        testDriveFragment.setDescription(R.string.settings_menu_test_drive);

        trapSettingsFragment.setImageResource(R.drawable.trap_settings);
        alertOptionsFragment.setImageResource(R.drawable.alerting);
        displayOptionsFragment.setImageResource(R.drawable.items_displayed);
        soundOptionsFragment.setImageResource(R.drawable.audio);
        testDriveFragment.setImageResource(R.drawable.test_drive);
    }

    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }
}
