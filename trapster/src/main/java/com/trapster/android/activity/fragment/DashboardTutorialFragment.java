package com.trapster.android.activity.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.google.inject.Inject;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import roboguice.inject.InjectView;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 1/18/13
 * Time: 10:19 AM
 * To change this template use File | Settings | File Templates.
 */
public class DashboardTutorialFragment extends AbstractFragment
{
    @Inject SharedPreferences sharedPreferences;

    @InjectView (R.id.dashboardTutorialContainer)RelativeLayout dashboardTutorialContainer;

    private Runnable onDismissRunnable;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_startup_dash, null);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        dashboardTutorialContainer.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                sharedPreferences.edit().putBoolean(Defaults.PREFERENCE_HIDE_DASHBOARD_TUTORIAL, true).commit();
                if (onDismissRunnable != null)
                    onDismissRunnable.run();
                removeWithAnimation(R.anim.fade_out);

            }
        });

    }

    public void setOnDismissRunnable(Runnable onDismissRunnable)
    {
        this.onDismissRunnable = onDismissRunnable;
    }
}
