package com.trapster.android.activity.fragment;

import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.google.inject.Inject;
import com.here.android.common.GeoPosition;
import com.here.android.common.LocationMethod;
import com.here.android.common.LocationStatus;
import com.here.android.common.PositionListener;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.activity.NearbyTrapsScreen;
import com.trapster.android.activity.SingleTrapDetailsScreen;
import com.trapster.android.manager.PositionManager;
import com.trapster.android.manager.TrapManager;
import com.trapster.android.model.Trap;
import com.trapster.android.model.dao.TrapCursorAdapter;
import com.trapster.android.model.dao.TrapDAO;
import com.trapster.android.model.provider.TrapProvider;
import com.trapster.android.util.BackgroundTask;
import com.trapster.android.util.GeographicUtils;
import com.vividsolutions.jts.geom.Geometry;

import java.util.*;

/**
 * 
 * [Nov 29, 2012 john]
 * adapter.addAll only available on api > 11. Changed to add
 * 
 * @author John
 *
 */
public class NewestTrapListFragment extends AbstractListFragment implements PositionListener {

	private static final String EXTRA_DATEFROM = "EXTRA_DATEFROM";

	private static final String EXTRA_BBOX = "EXTRA_BBOX";

	private static String LOGNAME = "NewestTrapListFragment";

	protected Cursor cursor = null;
	
	@Inject TrapManager tm;
	
	@Inject TrapDAO trapDao; 
	
	@Inject PositionManager pm;
	
	private Date from;

	private Geometry area;
	
	private TrapCursorAdapter adapter;
	
	private GeoPosition location;

	private QueryNewRange query;


    private final TrapUpdateObserver trapObserver = new TrapUpdateObserver();

	public static NewestTrapListFragment newInstance(Date from, Geometry area) {
		NewestTrapListFragment f = new NewestTrapListFragment();

		Bundle args = new Bundle();

		if (from == null) {

			Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
			calendar.add(Calendar.HOUR, -1 * Defaults.AGE_FOR_NEW_TRAPS);

			from = calendar.getTime();

		}

		args.putSerializable(EXTRA_DATEFROM, from);
		args.putSerializable(EXTRA_BBOX, area);
		f.setArguments(args);

		return f;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		from = (Date) getArguments().get(EXTRA_DATEFROM);


		area = (Geometry) getArguments().get(EXTRA_BBOX);

		location = pm.getLastLocation(); 
		
		adapter = new TrapCursorAdapter(getParentActivity());
		
		if(location != null)
			adapter.updateLocation(location);
		
		// Allocate the adapter to the List displayed within this fragment.
		setListAdapter(adapter);


	}

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_newesttraplist, null);

		ListView lv = (ListView) view.findViewById(android.R.id.list);

		lv.setEmptyView(view.findViewById(R.id.layoutNoTraps));
		return view;
	}

	@Override
	public void onStart() {
		super.onStart();
		pm.addPositionListener(this);

	}

	@Override
	public void onStop() {
		super.onStop();
		pm.addPositionListener(this);
		if (query != null)
			query.cancel();

	}



    @Override
    public void onResume() {
        super.onResume();
        if(location != null)
            refresh(location);
        getParentActivity().getContentResolver().registerContentObserver(TrapProvider.CONTENT_URI, true, trapObserver);

    }

    @Override
    public void onPause() {
        super.onPause();
        getParentActivity().getContentResolver().unregisterContentObserver(trapObserver);
    }


	public void refresh(GeoPosition location) {

		if (query != null)
			query.cancel();

		if (location != null)
			area = GeographicUtils.createBoundingBox(location.getCoordinate().getLatitude(),
					location.getCoordinate().getLongitude(), Defaults.NEARBY_TRAP_RADIUS);


		query = new QueryNewRange(from, area);
		query.execute();
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long theid) {

		Trap trap= (Trap) l.getItemAtPosition(position);
		if (trap != null)
		{
			showTrapView(trap);
		}
		
	}
	
	
	void showTrapView(Trap trap)
	{
		Intent i= new Intent(getParentActivity(), SingleTrapDetailsScreen.class);
		i.putExtra(SingleTrapDetailsScreen.TRAP_BUNDLE_KEY, trap);
		startActivity(i);
	}


	private class QueryNewRange extends BackgroundTask
    {

		Date from;
		Geometry area;
        private List<Trap> traps;

		public QueryNewRange(Date from, Geometry area) {
			super(getParentActivity());
			this.from = from;
			this.area = area;
		}

		@Override
		public void onExecute()
        {
            traps = trapDao.getNewTraps(from, area);

				/*
				 * Sorting by distance in the query doesn't yield the exact correct
				 * results due to the roundness nature of the world and the
				 * flatness nature of the pythagorem calcs.
				 */
            if (location != null && isRunning())
                Collections.sort(traps, new SortByDistanceTrapComparator(location.getCoordinate().getLatitude(), location.getCoordinate().getLongitude()));
		}

		@Override
		public void onPostExecute()
        {

			if (traps != null)
            {
				adapter.clear();
				
				int trapCount = 0; 
				for(Trap trap : traps)
                {
					// Add only the new traps, not updated or voted ones
                  	//if(trap.getOuname().equalsIgnoreCase(trap.getLuname())){
						trapCount++; 
						adapter.add(trap);
					//}
				}
				
				adapter.notifyDataSetChanged();
				// Update the tab counts

			}
		}

        @Override
        public void runOnUIThread()
        {
            if(getParentActivity() instanceof NearbyTrapsScreen && traps.size() > 0){
                ((NearbyTrapsScreen)getParentActivity()).setNewTrapsCount(traps.size());
            }
        }

        @Override
        public void onTimeout(long runTime)
        {
        }


    }

	private class SortByDistanceTrapComparator implements Comparator<Trap> {

		private double lat;
		private double lon;

		public SortByDistanceTrapComparator(double _lat, double _lon) {
			lat = _lat;
			lon = _lon;
		}

		public int compare(Trap t1, Trap t2) {

			double d1 = GeographicUtils.geographicDistance(lat, lon,
					t1.getLatitude(), t1.getLongitude());
			double d2 = GeographicUtils.geographicDistance(lat, lon,
					t2.getLatitude(), t2.getLongitude());

			return Double.valueOf(d1).compareTo(Double.valueOf(d2));
		}
	}

	@Override
	public void onPositionFixChanged(LocationMethod arg0, LocationStatus arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPositionUpdated(LocationMethod arg0, GeoPosition position) {
		adapter.updateLocation(position);
	}

    private class TrapUpdateObserver extends ContentObserver
    {
        public TrapUpdateObserver()
        {
            super(null);
        }

        @Override
        public void onChange(boolean selfChange)
        {
            super.onChange(selfChange);
            refresh(location);
        }
    }

}