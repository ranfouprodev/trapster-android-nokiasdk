package com.trapster.android.activity.settings.fragment;

import android.widget.TextView;
import com.trapster.android.R;
import roboguice.inject.InjectView;

public class SubTextSettingFragment extends IconSettingFragment
{
    @InjectView(R.id.settingsSubTextDescription) TextView subText;
    private int subTextResource;
    private String subTextString;

    @Override
    protected void init()
    {
        super.init();
        if (subTextResource > 0)
            subTextString = getString(subTextResource);
        subText.setText(subTextString);
    }

    @Override
    protected int getLayoutResource()
    {
        return R.layout.fragment_settings_subtext;
    }

    public void setSubText(int subText)
    {
        if (this.subText != null)
            this.subText.setText(getString(subText));
        subTextResource = subText;
    }

    public void setSubText(String subTextString)
    {
        if (subText != null)
            subText.setText(subTextString);
        this.subTextString = subTextString;
    }
}
