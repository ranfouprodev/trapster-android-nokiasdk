package com.trapster.android.activity.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PointF;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.inject.Inject;
import com.here.android.common.GeoCoordinate;
import com.here.android.common.ViewObject;
import com.here.android.mapping.*;
import com.trapster.android.R;
import com.trapster.android.activity.Screen;
import com.trapster.android.activity.fragment.component.ReportTrapIconImageView;
import com.trapster.android.manager.*;
import com.trapster.android.manager.RouteLinkManager.OnNearestRouteLinkUpdateListener;
import com.trapster.android.manager.flurry.FLURRY_EVENT_NAME;
import com.trapster.android.manager.flurry.FLURRY_PARAMETER_NAME;
import com.trapster.android.manager.flurry.FlurryEvent;
import com.trapster.android.model.RouteLink;
import com.trapster.android.model.Trap;
import com.trapster.android.model.TrapType;
import com.trapster.android.model.dao.TrapDAO;
import com.trapster.android.service.TrapAddOrUpdateService;
import com.trapster.android.util.BackgroundTask;
import com.trapster.android.util.GeographicUtils;
import com.trapster.android.util.MapUtils;
import roboguice.inject.InjectFragment;
import roboguice.inject.InjectView;

import java.util.LinkedList;
import java.util.List;


public class ReportTrapFragment extends AbstractFragment implements MapEventListener {

	private static final String LOGNAME = "ReportTrapFragment";

	public static final int UPDATE_STREET = 0;
    private static final int ROUTE_LINK_LINE_WIDTH = 5;
    private static final int IMAGE_OFFSET = 30;
    private static final String UNKNOWN_STREET = " ";
	
	private TrapType trapType;

    @InjectFragment(R.id.bottomBarFragment) BottomBarFragment bottomBarFragment;

    @InjectView (R.id.reportTrapInstructions) LinearLayout instructionLayout;
	
	@InjectView(R.id.imageTrapIcon) ReportTrapIconImageView trapIcon;
	
	//@InjectView(R.id.imageTrapIconHeader) ImageView trapIconHeader;
	
	//@InjectView(R.id.textTrapType) TextView trapTypeText;
	
	@InjectView(R.id.textTrapAddress) TextView trapAddress;
	
	//@InjectView(R.id.buttonOk) Button buttonOk;
	
	//@InjectView(R.id.buttonCancel) Button buttonCancel;

    @InjectView (R.id.reportTrapAddressBackground) LinearLayout addressBackground;
	
	@Inject SessionManager sessionManager;
	@Inject RouteLinkManager routeLinkManager;
	@Inject MapController mapController;
    @Inject TrapMapLayer trapMapLayer;
    @Inject TrapManager trapManager;

    @Inject
    TrapDAO trapDao;

	private double latitude;
    private double longitude;

    private MapContainer routeLinkMapObjectLayer;
    MapPolyline routeLinkOverlay;
	
	private static Handler updateHandler = new Handler();
	private Runnable routeLinkUpdateTask = new Runnable(){
		@Override
		public void run() {
			displayListOfNearbyRoadlinks();
		}
		
	};
	
	private Handler updateUIHandler = new Handler(){
		 @Override
	        public void handleMessage(Message msg) {
	            switch (msg.what) {
	            	case UPDATE_STREET:
	            		String streetName = (String) msg.obj;
                        if (UNKNOWN_STREET.equals(streetName))
                            addressBackground.setVisibility(View.INVISIBLE);
                        else
                        {
                            addressBackground.setVisibility(View.VISIBLE);
                            trapAddress.setText(streetName);
                        }

	            		break;
	            }
		 }
	};

	private Map map;
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_report_trap, null);
		return view; 
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {


		super.onViewCreated(view, savedInstanceState);

        bottomBarFragment.setNegativeAction(null, null);
        bottomBarFragment.setPositiveAction(getString(R.string.forgot_submit_button), new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                instructionLayout.setVisibility(View.GONE);
                submit();
                bottomBarFragment.hide();
            }
        });

		updateHandler.postDelayed(routeLinkUpdateTask, 500);

        GlobalMapEventManager.addListener(this, new GlobalMapEventManager.FUNCTION_CALL[]{GlobalMapEventManager.FUNCTION_CALL.MAP_MOVE_END, GlobalMapEventManager.FUNCTION_CALL.MAP_MOVE_START});
		
	}

	
	@Override
	public void onPause() {
		super.onPause();
        GlobalMapEventManager.removeListener(this);
		if (updateHandler != null)
			updateHandler.removeCallbacks(routeLinkUpdateTask);
	}

	public void setTrapType(TrapType trapType) {
		this.trapType = trapType;
        trapIcon.setTrapType(trapType);
	}

    public void setLatitude(double latitude)
    {
        this.latitude = latitude;
    }
    public void setLongitude(double longitude)
    {
        this.longitude = longitude;
    }

	private void updateView() {
		// Not sure if we should do null checking here or not
		String trapTypeIcon = trapType.getImageFilename();
		
		/*ImageResource imageResource = imageResourceDao.findByKey(trapTypeIcon);
		
		if (imageResource != null){		
		
			Bitmap icon = bitmapCacheManager.getBitmapFromCache(trapTypeIcon);
			
			trapIconHeader.setImageBitmap(icon);
		}
		
		trapTypeText.setText(trapType.getName());*/
		
	}

    @Inject CompositePositionIndicatorManager compositePositionIndicatorManager;
	
	public void setMap(Map map){
		this.map = map;
        mapController.updateMapScheme(map);
        //map.setTilt(0);
        //map.setOrientation(0); // This is pretty much a hack in the interests of time
		map.addMapObject(routeLinkMapObjectLayer);
        //map.addMapObject(compositePositionIndicatorManager.replicatePinMarker());
	}
	
		
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
   
        
		routeLinkMapObjectLayer = MapFactory.createMapContainer();
		
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
        if (map != null)
        {
            outState.putDouble("MAPLAT", map.getCenter().getLatitude());
            outState.putDouble("MAPLNG", map.getCenter().getLongitude());
        }

		
	}

    @Override
    public void onBackPressed()
    {
        sendFlurryNewTrapCancelledEvent(trapType);
        super.onBackPressed();
    }

	public Trap getShellTrap()
	{

		GeoCoordinate center = map.getCenter(); 
		
		Trap trap= new Trap();
        trap.setId(-1);
		trap.setTypeid(trapType.getId());
		trap.setGeometry(center.getLatitude(),center.getLongitude());
        trap.setLevel(4);
        trap.setOuname(sessionManager.getUser().getUserName());
        trap.setLuname(sessionManager.getUser().getUserName());
        trap.setLvoteSec((System.currentTimeMillis()/1000));

		return trap;

	}
	
	private void displayListOfNearbyRoadlinks(){

        if (map != null)
        {
            final GeoCoordinate center = map.getCenter();
            new BackgroundTask()
            {

                @Override
                public void onExecute()
                {


                    routeLinkManager.requestNearestRouteLinkUpdate(center.getLatitude(), center.getLongitude(), new GetNearestRouteLinks());
                }

                @Override
                public void onTimeout(long runTime)
                {
                }
            }.execute();

        }
		
	}
	
	
	private class GetNearestRouteLinks implements OnNearestRouteLinkUpdateListener{

	
		public void onNearestRouteLinkUpdated(RouteLink nearestRouteLink) {
			
			if(nearestRouteLink != null)
            {
                if(nearestRouteLink.getAddressInfo().getStreet() != null)
                {
                    Message msg = new Message();
                    msg.what = UPDATE_STREET;
                    msg.obj = nearestRouteLink.getAddressInfo().getStreet();
                    updateUIHandler.sendMessage(msg);

                    //snapToRouteLink(link);
                }
			}
		}

/*        private void snapToRouteLink (RouteLink routeLink)
        {
            if (map != null)
            {
                GeoCoordinate center = map.getCenter();
                GeoCoordinate nearestPoint = null;
                GeoCoordinate tempPoint = null;
                double distanceFromPoints = 0.0;

                removeRouteLinkOverlay();

                LinkedList<GeoCoordinate> coordinates = new LinkedList<GeoCoordinate>();
                for (int i = 0; i < routeLink.getNumPoints(); i++)
                {
                    coordinates.add(routeLink.getPointAt(i).toGeoPoint());
                    if(nearestPoint == null)
                    {
                        nearestPoint = routeLink.getPointAt(i).toGeoPoint();
                        distanceFromPoints = nearestPoint.distanceTo(center);
                    }
                    else
                    {
                        tempPoint = routeLink.getPointAt(i).toGeoPoint();
                        if(distanceFromPoints > tempPoint.distanceTo(center))
                        {
                            distanceFromPoints = tempPoint.distanceTo(center);
                            nearestPoint = tempPoint;
                        }
                    }
                }

                routeLinkOverlay = MapUtils.generatePolyline(coordinates);
            }

            //turn off the snapping for now.  Just leave the drawing portion
            //Point centerPoint = map.geoToPixel(nearestPoint);
            //nearestPoint = map.pixelToGeo(centerPoint.getX(), centerPoint.getY());
            //map.setCenter(nearestPoint, MapAnimation.ANIMATION_LINEAR);

            drawRouteLinkOverlay();
        }*/

        private void removeRouteLinkOverlay()
        {
            routeLinkMapObjectLayer.removeMapObject(routeLinkOverlay);
            routeLinkOverlay = null;
        }

        private void drawRouteLinkOverlay()
        {
            if (routeLinkOverlay != null)
            {
                routeLinkOverlay.setLineColor(Color.BLUE);
                routeLinkOverlay.setLineWidth(ROUTE_LINK_LINE_WIDTH);
                routeLinkMapObjectLayer.addMapObject(routeLinkOverlay);
                routeLinkOverlay.setVisible(true);
            }

        }

		
	}

    private void submit()
    {
        sendFlurryNewTrapSubmittedEvent(trapType);

        // Remove dummy traps
        //removeDummyTrap();

        Trap trap = getShellTrap();

        Intent intent = new Intent(getParentActivity().getApplicationContext(), TrapAddOrUpdateService.class);
        intent.putExtra(TrapAddOrUpdateService.INTENT_EXTRA_TRAP, trap);
        getParentActivity().startService(intent);

        // trapManager.addToDismissTraps(-1);
        // trapDao.save(trap);

        // Dialog
        ((Screen)getParentActivity()).showMessageAndCloseActivity(R.string.dialog_report_trap_success, 2500);
    }

    private void removeDummyTrap() {
        trapMapLayer.removeTrap(-1);
        trapDao.delete(-1);
    }


    @Override
	public boolean onDoubleTap(PointF arg0) {
		return false;
	}

	@Override
	public void onLongPressReleased() {
	}

	@Override
	public boolean onLongPressed(PointF arg0) {
		return false;
	}

	@Override
	public void onMapAnimatingEnd() {
	}

	@Override
	public void onMapAnimatingStart() {
	}

	@Override
	public void onMapMoveEnd(GeoCoordinate arg0) {

        new BackgroundTask()
        {
            @Override
            public void onExecute()
            {
                 /*if (updateHandler != null)
                    updateHandler.removeCallbacks(routeLinkUpdateTask);*/

                //updateHandler.postDelayed(routeLinkUpdateTask, 100);
                routeLinkUpdateTask.run();

                if (isRunning())
                {
                    Message msg = new Message();
                    msg.what = UPDATE_STREET;
                    msg.obj = UNKNOWN_STREET;
                    updateUIHandler.sendMessage(msg);
                }

            }

            @Override
            public void onTimeout(long runTime)
            {

            }
        }.execute();

	}

	@Override
	public void onMapMoveStart()
    {
        if (instructionLayout.getVisibility() != View.GONE)
        updateUIHandler.post(new Runnable()
        {
            @Override
            public void run()
            {
                instructionLayout.setVisibility(View.GONE);
            }
        });
	}

	@Override
	public boolean onMapObjectsSelected(List<ViewObject> arg0) {
		return false;
	}

	@Override
	public void onMapSchemeSet() {
	}

	@Override
	public boolean onPinchZoom(float arg0, PointF arg1) {
		return false;
	}

	@Override
	public boolean onRotate(float arg0) {
		return false;
	}

	@Override
	public boolean onTap(PointF arg0) {
		return false;
	}

	@Override
	public boolean onTilt(float arg0) {
		return false;
	}

	@Override
	public boolean onTwoFingerTap(PointF arg0) {
		return false;
	}

    private void sendFlurryNewTrapCancelledEvent(TrapType trapType)
    {
        FlurryEvent event = FlurryManager.createEvent(FLURRY_EVENT_NAME.NEW_TRAP_REPORT_CANCELLED);

        String trapName = trapType.getName();
        String englishName = trapType.getUnlocalizedName();

        event.putParameter(FLURRY_PARAMETER_NAME.TRAP_TYPE, englishName);
        event.putParameter(FLURRY_PARAMETER_NAME.TRAP_TYPE_LANGUAGE, trapName);
        event.putParameter(FLURRY_PARAMETER_NAME.LOCATION_COUNTRY, FlurryManager.getCountryCode());
        event.putParameter(FLURRY_PARAMETER_NAME.LOCATION_STATE, FlurryManager.getStateCode());
        event.putParameter(FLURRY_PARAMETER_NAME.DRIVING_SPEED, FlurryManager.getDrivingSpeed());

        event.send();
    }

    private void sendFlurryNewTrapSubmittedEvent(TrapType trapType)
    {

        FlurryEvent event = FlurryManager.createEvent(FLURRY_EVENT_NAME.NEW_TRAP_REPORT_SUBMITTED);

        String trapName = trapType.getName();
        String englishName = trapType.getUnlocalizedName();
        double distanceBetweenTrapAndUser = GeographicUtils.geographicDistance(latitude, longitude, getShellTrap().getLatitude(), getShellTrap().getLongitude());

        event.putParameter(FLURRY_PARAMETER_NAME.TRAP_TYPE, englishName);
        event.putParameter(FLURRY_PARAMETER_NAME.TRAP_TYPE_LANGUAGE, trapName);
        event.putParameter(FLURRY_PARAMETER_NAME.LOCATION_COUNTRY, FlurryManager.getCountryCode());
        event.putParameter(FLURRY_PARAMETER_NAME.LOCATION_STATE, FlurryManager.getStateCode());
        event.putParameter(FLURRY_PARAMETER_NAME.DRIVING_SPEED, FlurryManager.getDrivingSpeed());
        event.putParameter(FLURRY_PARAMETER_NAME.DISTANCE_FROM_USER, FlurryManager.getDistanceBetweenTrapAndUser(distanceBetweenTrapAndUser));

        event.send();
    }



	
	
	
}
