package com.trapster.android.activity.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.google.inject.Inject;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.activity.GetSatScreen;
import com.trapster.android.activity.MapScreen;
import com.trapster.android.manager.FlurryManager;
import com.trapster.android.manager.flurry.FLURRY_EVENT_NAME;
import com.trapster.android.manager.flurry.FLURRY_PARAMETER_NAME;
import com.trapster.android.manager.flurry.FlurryEvent;
import roboguice.inject.InjectView;

public class RateUsFragment extends AbstractFragment
{
    @Inject SharedPreferences sharedPreferences;

    @InjectView(R.id.rateUsShowLoveButton) Button showLoveButton;
    @InjectView(R.id.rateUsContactButton) Button contactButton;
    @InjectView(R.id.rateUsAskLaterButton) Button askLaterButton;

    private final static String FLURRY_CLICK_NOT_NOW = "Ask Later";
    private final static String FLURRY_CLICK_LOVE = "Send Love";
    private final static String FLURRY_CLICK_CONTACT= "Contact Us";

    private final static String TRAPSTER_MARKET_LINK = "market://details?id=com.trapster.android";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_rate_us, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        showLoveButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                sendFlurryRateUsLoveEvent(sharedPreferences.getInt(Defaults.RATE_US_DIALOG_IGNORE_COUNT, 0));
                sendFlurryRateUsClickEvent(FLURRY_CLICK_LOVE);
                launchMarketLink();
                removeWithAnimation(R.anim.slide_out_right);
            }
        });

        contactButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                sendFlurryRateUsContactEvent(sharedPreferences.getInt(Defaults.RATE_US_DIALOG_IGNORE_COUNT, 0));
                sendFlurryRateUsClickEvent(FLURRY_CLICK_CONTACT);
                launchGetSat();
                removeWithAnimation(R.anim.slide_out_right);
            }
        });

        askLaterButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                sendFlurryRateUsClickEvent(FLURRY_CLICK_NOT_NOW);
                incrementNumberIgnores();
                setupAskLater();
                removeWithAnimation(R.anim.slide_out_right);
            }
        });
    }

    private void setupAskLater()
    {
        sharedPreferences.edit().putInt(Defaults.RATE_US_APP_START_COUNT, 0).commit();
    }

    private void launchGetSat()
    {
        disableAskAgain();

        Intent i = new Intent(getParentActivity(), GetSatScreen.class);
        startActivity(i);
    }

    private void disableAskAgain()
    {
        sharedPreferences.edit().putBoolean(Defaults.RATE_US_DIALOG_FEEDBACK_SENT, true).commit();
    }

    private void launchMarketLink()
    {
        disableAskAgain();
        Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(TRAPSTER_MARKET_LINK));
        marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        startActivity(marketIntent);
    }

    private void sendFlurryRateUsContactEvent(int numPreviousIgnores)
    {
        FlurryEvent event = FlurryManager.createEvent(FLURRY_EVENT_NAME.RATE_US_SCREEN_CLICK_CONTACT);
        event.putParameter(FLURRY_PARAMETER_NAME.NUMBER_PREVIOUS_IGNORES, String.valueOf(numPreviousIgnores));

        event.send();
    }

    private void sendFlurryRateUsLoveEvent(int numPreviousIgnores)
    {
        FlurryEvent event = FlurryManager.createEvent(FLURRY_EVENT_NAME.RATE_US_SCREEN_CLICK_LOVE);
        event.putParameter(FLURRY_PARAMETER_NAME.NUMBER_PREVIOUS_IGNORES, String.valueOf(numPreviousIgnores));

        event.send();
    }

    private void sendFlurryRateUsClickEvent(String whichClick)
    {
        FlurryEvent event = FlurryManager.createEvent(FLURRY_EVENT_NAME.RATE_US_SCREEN_CLICK);
        event.putParameter(FLURRY_PARAMETER_NAME.SELECTED_OPTION, whichClick);

        event.send();
    }

    private void incrementNumberIgnores()
    {
        int ignoreCount = sharedPreferences.getInt(Defaults.RATE_US_DIALOG_IGNORE_COUNT, 0);
        ignoreCount++;
        sharedPreferences.edit().putInt(Defaults.RATE_US_DIALOG_IGNORE_COUNT, ignoreCount).commit();
    }

    @Override
    public void hide()
    {
        ((MapScreen)getParentActivity()).checkCompositeViewVisibility();
        super.removeWithAnimation(R.anim.fade_out);
    }
}
