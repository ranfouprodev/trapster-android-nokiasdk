package com.trapster.android.activity;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.trapster.android.R;
import roboguice.inject.ContentView;

@ContentView(R.layout.screen_why_join)
public class WhyJoinScreen extends Screen {

    Button buttonGotIt;

    private final static String CLASS_NAME = "WhyJoin";

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        //
        // setup handler for sign up button
        buttonGotIt = (Button) findViewById(R.id.btnGotIt);
        buttonGotIt.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                finish();
            }
        });


    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume(){
        super.onResume();

    }

    @Override
    protected void onPause(){
        super.onPause();

    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }

    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }
}
