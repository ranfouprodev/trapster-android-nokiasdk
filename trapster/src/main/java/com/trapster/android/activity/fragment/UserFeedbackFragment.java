package com.trapster.android.activity.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import com.trapster.android.R;
import com.trapster.android.activity.MapScreen;
import com.trapster.android.activity.StatsScreen;
import roboguice.inject.InjectFragment;
import roboguice.inject.InjectView;

public class UserFeedbackFragment extends AbstractFragment
{
    public static final String BUNDLE_KEY_VOTE_COUNT = "bundleKeyVoteCount";
    @InjectView(R.id.statsOptOutCheckbox) CheckBox statsOptOutCheckbox;
    @InjectView(R.id.statsPopupBody) TextView statsBody;
    @InjectFragment(R.id.statsNavFragment) BottomBarFragment bottomBarFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_user_feedback_popup, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null)
        {
            int value = arguments.getInt(BUNDLE_KEY_VOTE_COUNT);
            statsBody.setText(getString(R.string.stats_popup_body, value));
        }

        bottomBarFragment.setNegativeAction(getString(R.string.stats_popup_close), new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                removeWithAnimation(R.anim.fade_out);
                hide();
                ((MapScreen)getParentActivity()).checkCompositeViewVisibility();
            }
        });
        bottomBarFragment.setPositiveAction(getString(R.string.stats_popup_more_info), new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hide();
                startActivity(new Intent(getParentActivity(), StatsScreen.class));
            }
        });
    }
}
