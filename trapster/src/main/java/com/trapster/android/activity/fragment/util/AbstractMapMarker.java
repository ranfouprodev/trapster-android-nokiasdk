package com.trapster.android.activity.fragment.util;

import com.here.android.mapping.MapMarker;
import com.vividsolutions.jts.geom.Envelope;

public abstract class AbstractMapMarker {

	/*
	 * We need a UDID ?
	 */
	public static final int MAP_MAXIMUM_ZOOM = 20; 
	public static final int MAP_MINIMUM_ZOOM = 1;
	
	
	private OnMapMarkerClick onMarkerClick; 
	
	public abstract MapMarker getMarker();
	public abstract Envelope getSpatialIndex(); 
	
	private int minimumZoomLevel; 
	private int maximumZoomLevel; 
	
	
	public OnMapMarkerClick getOnMarkerClick() {
		return onMarkerClick;
	}


	public void setOnMarkerClick(OnMapMarkerClick onMarkerClick) {
		this.onMarkerClick = onMarkerClick;
	}


	public int getMinimumZoomLevel() {
		return minimumZoomLevel;
	}
	public void setMinimumZoomLevel(int minimumZoomLevel) {
		this.minimumZoomLevel = minimumZoomLevel;
	}


	public int getMaximumZoomLevel() {
		return maximumZoomLevel;
	}
	public void setMaximumZoomLevel(int maximumZoomLevel) {
		this.maximumZoomLevel = maximumZoomLevel;
	}


	public interface OnMapMarkerClick{
		void onMapMarkerClick(AbstractMapMarker marker);
	}
}
