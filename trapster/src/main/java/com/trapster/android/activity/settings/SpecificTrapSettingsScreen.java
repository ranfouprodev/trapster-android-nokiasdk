package com.trapster.android.activity.settings;

import android.widget.CompoundButton;

import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.activity.fragment.TrapMapLayer;
import com.trapster.android.activity.settings.fragment.CheckBoxSettingFragment;
import com.trapster.android.activity.settings.fragment.IconSettingFragment;
import com.trapster.android.activity.settings.fragment.MultiModalSlideBarFragment;
import com.trapster.android.manager.TrapManager;
import com.trapster.android.model.TrapType;

import javax.inject.Inject;

public class SpecificTrapSettingsScreen extends AbstractSettingsScreen
{
    private static final String CLASS_NAME = "Specific Trap Settings";

    @Inject TrapManager trapManager;

    @Inject
    TrapMapLayer trapMapLayer;

    public static final String TRAP_TYPE = "trapType";
    private TrapType trapType;
    private boolean didChangeSettings = false;

    private CheckBoxSettingFragment alertFragment;
    private CheckBoxSettingFragment showOnMapFragment;
    private MultiModalSlideBarFragment alertTypeFragment;

    private boolean useAlerts = true;

    @Override
    protected void setupFragments()
    {
        int trapTypeID = getIntent().getExtras().getInt(TRAP_TYPE);
        this.trapType = trapManager.getTrapType(trapTypeID);

        useAlerts = sharedPreferences.getBoolean(trapType.getAudioid() + Defaults.SETTING_ALERTING, true);

        IconSettingFragment iconSettingFragment = (IconSettingFragment) createSettingsFragment(FRAGMENT_TYPE.ICON);
        try
        {
            setupIcon(iconSettingFragment, trapType);
        }
        catch (IndexOutOfBoundsException e){}
        iconSettingFragment.setDescription(trapType.getName());

        setupCheckBoxes();

        alertTypeFragment = (MultiModalSlideBarFragment) createSettingsFragment(FRAGMENT_TYPE.MULTI_MODAL_SLIDE_BAR);
        alertTypeFragment.setDescription(R.string.settings_menu_alerts);
        alertTypeFragment.setOptionText(com.trapster.android.R.string.trap_alert_notification_slide_bar_visual, com.trapster.android.R.string.trap_alert_notification_slide_bar_visual_audible_vibrate, com.trapster.android.R.string.trap_alert_notification_slide_bar_visual_audible);

        alertTypeFragment.setSelectionChangedListener(new MultiModalSlideBarFragment.OnSliderSelectionChangedListener()
        {
            @Override
            public void onSliderSelectionChanged(MultiModalSlideBarFragment.SLIDER_SELECTION selection)
            {
                switch (selection)
                {
                    case RIGHT:
                    {
                        editor.putBoolean(trapType.getAudioid() + Defaults.SETTING_VISIBLE, true);
                        editor.putBoolean(trapType.getAudioid() + Defaults.SETTING_AUDIBLE, true);
                        editor.putBoolean(trapType.getAudioid() + Defaults.SETTING_VIBRATE, true);
                        break;
                    }
                    case CENTER:
                    {
                        editor.putBoolean(trapType.getAudioid() + Defaults.SETTING_VISIBLE, true);
                        editor.putBoolean(trapType.getAudioid() + Defaults.SETTING_AUDIBLE, true);
                        editor.putBoolean(trapType.getAudioid() + Defaults.SETTING_VIBRATE, false);
                        break;
                    }
                    case LEFT:
                    {
                        editor.putBoolean(trapType.getAudioid() + Defaults.SETTING_VISIBLE, true);
                        editor.putBoolean(trapType.getAudioid() + Defaults.SETTING_AUDIBLE, false);
                        editor.putBoolean(trapType.getAudioid() + Defaults.SETTING_VIBRATE, false);
                        break;
                    }
                }
            }

        });

        setStartSliderPosition(alertTypeFragment);

        if (!sharedPreferences.getBoolean(trapType.getAudioid() + Defaults.SETTING_SHOW_ON_MAP, true))
        {
            hideSettingsFragments();
        }
        else if (!useAlerts)
        {
            alertTypeFragment.hide();
        }

    }

    private void setupCheckBoxes()
    {
        showOnMapFragment = (CheckBoxSettingFragment) createSettingsFragment(FRAGMENT_TYPE.CHECK_BOX);
        alertFragment = (CheckBoxSettingFragment) createSettingsFragment(FRAGMENT_TYPE.CHECK_BOX);

        showOnMapFragment.setDescription(R.string.settings_menu_map_display);
        alertFragment.setDescription(R.string.settings_menu_trap_alerts);

        alertFragment.setCheckBoxListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b)
            {
                editor.putBoolean(trapType.getAudioid() + Defaults.SETTING_ALERTING, b);

                if (!b)
                    alertTypeFragment.hide();
                else
                    alertTypeFragment.show();

                useAlerts = !useAlerts;
            }
        });

        showOnMapFragment.setCheckBoxListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b)
            {
                editor.putBoolean(trapType.getAudioid() + Defaults.SETTING_SHOW_ON_MAP, b);

                if (b)
                    showSettingsFragments();
                else
                {
                    useAlerts = false;
                    alertFragment.setChecked(false);
                    hideSettingsFragments();
                }


                didChangeSettings = true;
            }
        });

        showOnMapFragment.setChecked(sharedPreferences.getBoolean(trapType.getAudioid() + Defaults.SETTING_SHOW_ON_MAP, true));
        alertFragment.setChecked(sharedPreferences.getBoolean(trapType.getAudioid() + Defaults.SETTING_ALERTING, true));
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (didChangeSettings)
            trapMapLayer.wipeTrapCache();
    }

    private void hideSettingsFragments()
    {
        alertFragment.hide();
        alertTypeFragment.hide();
    }

    private void showSettingsFragments()
    {
        alertFragment.show();
        if (useAlerts)
            alertTypeFragment.show();
    }

    private void setStartSliderPosition(MultiModalSlideBarFragment fragment)
    {
        boolean visible = sharedPreferences.getBoolean(trapType.getAudioid() + Defaults.SETTING_VISIBLE, true);
        // Does the trap allow audible?
        boolean audible = sharedPreferences.getBoolean(trapType.getAudioid() + Defaults.SETTING_AUDIBLE,true);
        // Does the trap allow vibrate?
        boolean vibrate = sharedPreferences.getBoolean(trapType.getAudioid() + Defaults.SETTING_VIBRATE,true);

        if (visible)
        {
            fragment.setStartSelection(MultiModalSlideBarFragment.SLIDER_SELECTION.LEFT);
            if (audible)
            {
                fragment.setStartSelection(MultiModalSlideBarFragment.SLIDER_SELECTION.CENTER);

                if (vibrate)
                    fragment.setStartSelection(MultiModalSlideBarFragment.SLIDER_SELECTION.RIGHT);
            }

        }
    }

    @Override
    protected String getClassName()
    {
        return CLASS_NAME;
    }


}
