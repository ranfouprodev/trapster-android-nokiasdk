package com.trapster.android.activity.fragment;

/**
 * Created by John on 6/4/13.
 */
/*
public class StatsFragment extends AbstractFragment implements StatisticsListener {

    private static final long STATS_UPDATE_DELAY = 1000;
    @InjectView(R.id.textStatsTrapReports)
    TextView trapReports;
    @InjectView(R.id.textStatsTrapVotes)
    TextView trapVotes;
    @InjectView(R.id.textStatsSpeedLimitReports)
    TextView speedLimitReports;

    @Inject
    SessionManager sessionManager;

    @Inject
    UpdatedRouteLinkDAO routeLinkDAO;

    private Profile profile;

    private final Handler updateProfileHandler = new Handler();

    private final Runnable updateProfileRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            populateProfile();
            updateProfileHandler.postDelayed(this, STATS_UPDATE_DELAY);
        }
    };

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        profile = sessionManager.getUser().getProfile();

        sessionManager.requestUserStatistics(this);

        populateProfile();


        sessionManager.requestUserStatistics(this);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stats, null);
        return view;
    }

    private void populateProfile() {
        if (profile != null && profile.getUserStatistics() != null) {
            profile.getUserStatistics().getMyVoteStatistics().
            trapVotes.setText(String.valueOf(profile.getUserStatistics().getMyVoteStatistics().getUserVoteCount()));
            speedLimitReports.setText(String.valueOf(routeLinkDAO.getSize() ));
            trapReports.setText(String.valueOf(profile.getUserStatistics().getTrapStatistics().getTrapReportCount()));
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        updateProfileHandler.removeCallbacks(null);
    }

    @Override
    public void onTrapStatisticsLoaded(UserStatistics userStatistics) {
        updateProfileHandler.post(updateProfileRunnable);
    }

    @Override
    public void onError(TrapsterError error)
    {
    }
}
*/
