package com.trapster.android.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import com.trapster.android.service.ProbeDataUpdateService;
import roboguice.service.RoboIntentService;

public class ProbeDataLocationChangedReceiver extends BroadcastReceiver
{

    public static final String LOCATION_UPDATE_PROVIDER_DISABLED = "com.trapster.android.PROBE_DATA_DISABLED";
    private static final String LOGNAME = "ProbeDataLocationChangedReceiver";

    @Override
    public void onReceive(Context context, Intent intent)
    {
        intent.setClass(context, ProbeDataLocationChangedIntentService.class);
        context.startService(intent);
    }

    public static class ProbeDataLocationChangedIntentService extends RoboIntentService
    {
        public ProbeDataLocationChangedIntentService()
        {
            super("ProbeDataLocationChangedIntentService");
        }

        @Override
        public void onHandleIntent(Intent intent)
        {
            if (intent.hasExtra(LocationManager.KEY_PROVIDER_ENABLED))
            {
                if (!intent.getBooleanExtra(LocationManager.KEY_PROVIDER_ENABLED, true))
                {
                    Intent providerDisabledIntent = new Intent(LOCATION_UPDATE_PROVIDER_DISABLED);
                    getApplicationContext().sendBroadcast(providerDisabledIntent);
                }
            }

            if (intent.hasExtra(LocationManager.KEY_LOCATION_CHANGED))
            {
                Location location = (Location) intent.getExtras().get(LocationManager.KEY_LOCATION_CHANGED);
                Intent updateServiceIntent = new Intent(getApplicationContext(), ProbeDataUpdateService.class);
                updateServiceIntent.putExtra(ProbeDataUpdateService.EXTRA_KEY_LOCATION, location);
                getApplicationContext().startService(updateServiceIntent);
            }
        }
    }

}
