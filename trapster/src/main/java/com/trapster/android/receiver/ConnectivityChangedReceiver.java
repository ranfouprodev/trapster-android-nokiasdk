package com.trapster.android.receiver;


import roboguice.receiver.RoboBroadcastReceiver;

import com.google.inject.Inject;
import com.trapster.android.comms.CommunicationManager;
import com.trapster.android.util.Log;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectivityChangedReceiver extends RoboBroadcastReceiver {
	private static final String LOGNAME = "ConnectivityChangedReceiver";

	
	@Inject ConnectivityManager cm; 
	@Inject CommunicationManager communicationManager;
	
	@Override
	public void handleReceive(Context context, Intent intent) {
		
		// Check if we are connected to an active data network.
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

		boolean isConnected = activeNetwork != null
				&& activeNetwork.isConnectedOrConnecting();

		boolean isRoaming = activeNetwork != null
		&& activeNetwork.isRoaming();

		NetworkInfo wifiNetwork = cm
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		boolean isWifi = wifiNetwork == null ? false : wifiNetwork
				.isConnected();

		boolean runUpdate = false;
	
		if (isConnected)
			runUpdate = true;
{/* 
		//Log.d(LOGNAME, "Triggering Connectivity Changed:"+runUpdate); */}
		
		
		if (runUpdate) {
			communicationManager.clearPausedQueue();
		} 
		
		
	}

}
