package com.trapster.android.receiver;

import android.content.Context;
import android.content.Intent;

import com.trapster.android.model.Traps;
import com.trapster.android.service.TrapsterServiceNotificationListener;

import java.util.HashMap;

import roboguice.receiver.RoboBroadcastReceiver;
import roboguice.service.RoboIntentService;


public class TrapsterServiceBroadcastReceiver extends RoboBroadcastReceiver
{

    public static final String INTENT_BROADCAST = "com.trapster.android.TRAPSTER_SERVICE";

    public static final int INTENT_UPDATE_TRAP_NOTIFICATION = 1;
    public static final int INTENT_UPDATE_VALUES = 2;
    public static final int INTENT_UPDATE_ERROR = 3;
    public static final int INTENT_UPDATE_STATUS = 4;

    public static final String INTENT_NOTIFICATION_TYPE = "intentnotificationtype";
    public static final String INTENT_UPDATE_TRAPS = "intentupdatetraps";
    public static final String INTENT_SPEED = "intentspeed";
    public static final String INTENT_DIRECTION = "intentdirection";
    public static final String INTENT_ERROR_ID = "intenterrorid";
    public static final String INTENT_ERROR_MESSAGE = "intenterrormessage";
    public static final String INTENT_STATUS = "intentstatus";
    public static final String INTENT_LISTENER = "intentListener";
    public static final String INTENT_OWNER = "intentOwner";


    private static final String LOGNAME = "Trapster.TrapsterServiceBroadcastReceiver";
    private String owner;


    private static HashMap<String, TrapsterServiceNotificationListener> listeners = new HashMap<String, TrapsterServiceNotificationListener>();

    public TrapsterServiceBroadcastReceiver()
    {

    }

    public TrapsterServiceBroadcastReceiver(TrapsterServiceNotificationListener listener)
    {
        owner = listener.getOwner();
        if (owner != null)
            listeners.put(owner, listener);
    }

    @Override
    protected void handleReceive(Context context, Intent intent)
    {
/*        Intent newIntent = new Intent(context, TrapsterServiceBroadcastIntentService.class);
        if (intent.hasExtra(INTENT_NOTIFICATION_TYPE))
            newIntent.putExtra(INTENT_NOTIFICATION_TYPE, intent.getIntExtra(INTENT_NOTIFICATION_TYPE, -1));
        if (intent.hasExtra(INTENT_UPDATE_TRAPS))
            newIntent.putExtra(INTENT_UPDATE_TRAPS, intent.getSerializableExtra(INTENT_UPDATE_TRAPS));
        if (intent.hasExtra(INTENT_SPEED))
            newIntent.putExtra(INTENT_SPEED, intent.getDoubleExtra(INTENT_SPEED, 0.0));
        if (intent.hasExtra(INTENT_DIRECTION))
            newIntent.putExtra(INTENT_DIRECTION, intent.getIntExtra(INTENT_DIRECTION, 0));
        if (intent.hasExtra(INTENT_ERROR_MESSAGE))
            newIntent.putExtra(INTENT_ERROR_MESSAGE, intent.getStringExtra(INTENT_ERROR_MESSAGE));
        if (intent.hasExtra(INTENT_ERROR_ID))
            newIntent.putExtra(INTENT_ERROR_ID, intent.getIntExtra(INTENT_ERROR_ID, 0));

        context.startService(newIntent);*/

        intent.setClass(context, TrapsterServiceBroadcastIntentService.class);
        intent.putExtra(INTENT_OWNER, owner);
        context.startService(intent);
    }

    public static class TrapsterServiceBroadcastIntentService extends RoboIntentService
    {

        public TrapsterServiceBroadcastIntentService()
        {
            super("TrapsterServiceBroadcastIntentService");
        }

        @Override
        protected void onHandleIntent(Intent intent)
        {
        	
        	// for some reason intent.hasExtra in this class is taking a long time
            if (intent.getStringExtra(INTENT_OWNER) != null)
            {
                TrapsterServiceNotificationListener listener = listeners.get(intent.getStringExtra(INTENT_OWNER));
                if (listener != null)
                {
                    int type = intent.getIntExtra(INTENT_NOTIFICATION_TYPE, -1);


                    switch (type)
                    {
                        case INTENT_UPDATE_TRAP_NOTIFICATION:

                            if (intent.getParcelableExtra(INTENT_UPDATE_TRAPS) instanceof Traps)
                            {
                                Traps traps = (Traps) intent.getParcelableExtra(INTENT_UPDATE_TRAPS);
                                listener.onActiveTrapUpdate(traps.getTraps());
                            }
                            break;
                        case INTENT_UPDATE_VALUES:
                            double speed = intent.getDoubleExtra(INTENT_SPEED, 0.0f);
                            int direction = intent.getIntExtra(INTENT_DIRECTION, 0);
                            listener.onUpdateSpeedAndDirection(speed, direction);
                            break;
                        case INTENT_UPDATE_ERROR:
                            int errorCode = intent.getIntExtra(INTENT_ERROR_ID, -1);
                            String errorMessage = intent.getStringExtra(INTENT_ERROR_MESSAGE);
                            listener.onError(errorCode, errorMessage);
                            break;
                        case INTENT_UPDATE_STATUS:
                            // Status running/stopped not changed
                            break;
                    }
                }
            }

        }
    }
}

