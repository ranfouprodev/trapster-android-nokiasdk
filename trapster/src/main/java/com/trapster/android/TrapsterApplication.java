package com.trapster.android;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Intent;
import android.os.StrictMode;

import com.trapster.android.service.AttributesSyncService;


public class TrapsterApplication extends Application {

	protected static final String LOGNAME = "TrapsterApplication";

	static TrapsterApplication appInstance;

	public static TrapsterApplication getAppInstance() {
		if (appInstance == null)
			throw new IllegalArgumentException(
					"TrapsterApplication not initialized");
		return appInstance;
	}

	/*
	 * Session Variables
	 */
	private boolean gpsWarningDisplayed = false;

    private boolean rateAppDisplayed = false;


	@Override
	public void onCreate() {

		appInstance = this;

       // MapSettings.setAntiAliasingAllowed(false);
{/* 
		//Log.d(LOGNAME, "-----------------------------"); */}
{/* 		//Log.d(LOGNAME, "Starting Trapster Application"); */}
{/* 		//Log.d(LOGNAME, "Build:" + BuildFeatures.BUILD); */}
{/* 		//Log.d(LOGNAME, "Features:" + BuildFeatures.getFeatureSet()); */}
{/* 		//Log.d(LOGNAME, "Market:" + BuildFeatures.MARKET); */}
{/* 		//Log.d(LOGNAME, "Device:" + BuildFeatures.getDeviceId()); */}
{/* 		//Log.d(LOGNAME, "Type:" + BuildFeatures.getDeviceType()); */}
{/* 		//Log.d(LOGNAME, "TrapsterKey:" + BuildFeatures.TRAPSTERKEY); */}
{/* 		//Log.d(LOGNAME, "-----------------------------"); */}

		runStartupService();
		
		// for development
		//enableStrictMode();

    }

	/**
	 * Runs a background service to set the defaults
	 */
	private void runStartupService() {

        // Doesn't do anything yet
		//Intent syncService = new Intent(this, StartupService.class);
		//startService(syncService);

		// Preload the attributes
		Intent attributesLoaderintent = new Intent(this,
				AttributesSyncService.class);
		startService(attributesLoaderintent);

	}


	public boolean hasGpsWarningDisplayed() {
		return gpsWarningDisplayed;
	}

	public void setGpsWarningDisplayed(boolean gpsWarningDisplayed) {
		this.gpsWarningDisplayed = gpsWarningDisplayed;
	}

	@SuppressLint("NewApi")
	private void enableStrictMode(){
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
	     .detectAll()
	     .penaltyLog()
	     .build());
		
		 StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
         .detectActivityLeaks()
         .detectLeakedSqlLiteObjects()
         .penaltyLog()
         .build());
	}

	
   
	
}
