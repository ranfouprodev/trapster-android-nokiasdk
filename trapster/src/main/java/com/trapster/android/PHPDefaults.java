package com.trapster.android;

public class PHPDefaults
{

    // Application specific values
    public static final String PHP_APP_APPID = "TPST-AND-" + Defaults.getLanguage()+"-" + BuildFeatures.VERSIONNAME;
    public static final String PHP_APP_CAPABILITIES = "3";

    // php API Locations
    private static final String PHP_BASE_URL = "trapster.com"; // Change this to affect the URL path for all outgoing communication
    private static final String PHP_APIURL = "http://www." + PHP_BASE_URL + "/api/";
    private static final String PHP_SECURE_APIURL = "https://www." + PHP_BASE_URL + "/api/";
    public static final String PHP_API_NEW_TRAP_WITH_ID = PHP_APIURL + "trapservid.php";
    public static final String PHP_API_TRAPS_IN_RANGE = PHP_APIURL + "alrtserv2.php";
    public static final String PHP_API_TRAP_VOTE = PHP_APIURL + "vote.php";
    public static final String PHP_API_REMOVE_TRAP = PHP_APIURL + "deltrap.php";
    public static final String PHP_API_IGNORE_TRAP = PHP_APIURL + "trapblacklist.php";
    public static final String PHP_API_CHECK_LOGIN = PHP_APIURL + "checkLogin.php";
    public static final String PHP_API_FORGOT_PASSWORD = "http://www." + PHP_BASE_URL + "/forgot-password.php";
    public static final String PHP_API_SIGNUP = PHP_SECURE_APIURL + "signup.php";
    public static final String PHP_API_RESEND = PHP_APIURL + "resend.php";
    public static final String PHP_API_IAMHERE = PHP_APIURL + "locationdata.php";
    public static final String PHP_API_LAUNCHMESSAGE = PHP_APIURL + "launchmessage.php";
    public static final String PHP_API_SOUNDTHEME = PHP_APIURL + "themes2.php";
    public static final String PHP_API_PATROLPATH = PHP_APIURL + "patrol.php";
    public static final String PHP_API_ATTRIBUTES = PHP_APIURL + "appattributes.php";
    public static final String PHP_API_TYPES = PHP_APIURL + "types.php";
    public static final String PHP_API_REPORT_NEW_SPEED_LIMIT = PHP_APIURL + "speedlimit.php";

    public static final String REPORT_NPS = "https://cpq.nokia.com/api.php";

    //  This is a TEMPORARY URL for retrieving stats.  This is part of the REST services and should not be used in the PHP api
    public static final String API_TEMPORARY_STATS_URL = "http://" + PHP_BASE_URL + "/rest/web.php?service=userstats";

    // XML TAGS
    public static final String XML_TRAPSTER = "trapster";
    public static final String XML_RSP_STATUS = "status";
    public static final String XML_RSP_ERROR = "error";
    public static final String XML_RSP_DATA = "data";
    public static final String XML_RSP_SHOWMESSAGE = "showmessage";
    public static final String XML_RSP_MESSAGE = "message";

    public static final String XML_META = "meta";
    public static final String XML_META_OS = "os";

    public static final String XML_AUTH = "auth";
    public static final String XML_AUTH_KEY = "key";
    public static final String XML_AUTH_UNAME = "uname";
    public static final String XML_AUTH_HASH = "hash";
    public static final String XML_AUTH_DEVICETYPE = "devicetype";
    public static final String XML_AUTH_DEVICEID = "deviceid";
    public static final String XML_AUTH_CODE = "code";
    public static final String XML_AUTH_CAP = "capabilities";
    public static final String XML_AUTH_APPID = "appid";

    public static final String XML_PARAMS = "params";
    public static final String XML_PARAMS_LAT = "lat";
    public static final String XML_PARAMS_LON = "lng";
    public static final String XML_PARAMS_OBJ = "obj";
    public static final String XML_PARAMS_ADDR = "addr";
    public static final String XML_PARAMS_RADIUS = "radius";
    public static final String XML_PARAMS_GPS_ACCURACY_HORIZONTAL = "hacc"; // X/Y axis (lat/lon)  accuracy
    public static final String XML_PARAMS_GPS_ACCURACY_VERTICAL = "vacc";  // For Z-axis, when we retrieve elevation from GPS
    public static final String XML_PARAMS_ALTITUDE = "alt";
    public static final String XML_PARAMS_SATELLITE_COUNT = "slites";

    public static final String XML_PARAMS_TIME = "time";
    public static final String XML_PARAMS_POINT = "point";
    public static final String XML_PARAMS_POINTS = "points";
    public static final String XML_PARAMS_HEADING = "head";
    public static final String XML_PARAMS_SPEED = "spd";

    public static final String XML_PARAMS_EMAIL = "email";
    public static final String XML_PARAMS_UNAME = "uname";
    public static final String XML_PARAMS_PWD = "pwd";
    public static final String XML_PARAMS_CPWD = "cpwd";
    public static final String XML_PARAMS_AGREE = "agree";
    public static final String XML_PARAMS_STAY_IN_TOUCH = "stayintouch";
    public static final String XML_PARAMS_SMS = "smsaddr";
    public static final String XML_PARAMS_CARRIER = "carrier";
    public static final String XML_PARAMS_TRAP_ID = "trapid";
    public static final String XML_PARAMS_VOTE = "vote";
    public static final String XML_PARAMS_CMD = "cmd";

    public static final String XML_PARAMS_ROUTELINKID = "linkid";
    public static final String XML_PARAMS_SPEEDLIMIT = "speed_limit";

    public static final String XML_PARAMS_IF_CHANGED_AFTER = "if_changed_after";
    public static final String XML_PARAMS_HAS_KEY = "has_key";

    // Trap Response
    public static final String XML_RSP_MARKER = "marker";
    public static final String XML_RSP_TRAP_ID = "id";
    public static final String XML_RSP_TRAP_LAT = "lat";
    public static final String XML_RSP_TRAP_LON = "lng";
    public static final String XML_RSP_TRAP_TYPE_ID = "typeid";
    public static final String XML_RSP_TRAP_NUM = "num";
    public static final String XML_RSP_TRAP_OUNAME = "ouname";
    public static final String XML_RSP_TRAP_LUNAME = "luname";
    public static final String XML_RSP_TRAP_LVOTE = "lvote";
    public static final String XML_RSP_TRAP_LVOTE_SEC= "lvote_sec";
    public static final String XML_RSP_TRAP_ADDRESS = "address";
    public static final String XML_RSP_TRAP_LEVEL= "level";
    public static final String XML_RSP_TRAP_BADGEKEY= "badgeKey";

    // Speed Limit Report Response
    public static final String XML_RSP_ATTRIB_LINK = "link";
    public static final String XML_RSP_ATTRIB_LINKID = "linkid";
    public static final String XML_RSP_ATTRIB_SPEED = "speed";

    // My Trips Response
    public static final String XML_RSP_PATH = "path";
    public static final String XML_RSP_POINT = "point";
    public static final String XML_RSP_ID = "id";
    public static final String XML_RSP_MSG = "msg";
    public static final String XML_RSP_POINT_LAT = "lat";
    public static final String XML_RSP_POINT_LON = "lng";
    public static final String XML_RSP_POINT_COLOR = "color";

    // Sound themes response
    public static final String XML_RSP_THEME_ADDED = "added";
    public static final String XML_RSP_THEME_REMOVED = "removed";
    public static final String XML_RSP_THEME_ID = "id";
    public static final String XML_RSP_THEME_NAME = "name";
    public static final String XML_RSP_THEME_DESC = "description";
    public static final String XML_RSP_THEME_AUTHOR = "author";
    public static final String XML_RSP_THEME_VERSION = "version";
    public static final String XML_RSP_THEME_LINK = "link";
    public static final String XML_RSP_THEME_LID = "lid";
    public static final String XML_RSP_THEME_PATH = "path";
    public static final String XML_RSP_THEME_FORMATS = "formats";
    public static final String XML_RSP_THEME_AIFF = "aiff";
    public static final String XML_RSP_THEME_MP3 = "mp3";

    // Attributes response
    public static final String XML_RSP_ATTRIB_KEY= "key";
    public static final String XML_RSP_ATTRIB_COLOR= "color";
    public static final String XML_RSP_ATTRIB_RGBA= "rgba";
    public static final String XML_RSP_ATTRIB_IMAGE= "image";
    public static final String XML_RSP_ATTRIB_PATH= "path";
    public static final String XML_RSP_ATTRIB_SPEC= "spec";
    public static final String XML_RSP_ATTRIB_WIDTH= "width";
    public static final String XML_RSP_ATTRIB_HEIGHT= "height";
    public static final String XML_RSP_ATTRIB_PINX= "pinx";
    public static final String XML_RSP_ATTRIB_PINY= "piny";
    public static final String XML_RSP_ATTRIB_STRING= "string";
    public static final String XML_RSP_ATTRIB_TEXT= "text";

    // Trap Types response
    public static final String XML_RSP_TRAP_TYPE= "type";
    public static final String XML_RSP_TRAP_TYPE_CATEGORY= "category";
    public static final String XML_RSP_TRAP_TYPE_NAME= "name";
    public static final String XML_RSP_TRAP_TYPE_DESCRIPTION= "description";
    public static final String XML_RSP_TRAP_TYPE_ICONS= "icons";
    public static final String XML_RSP_TRAP_TYPE_ALT2= "alt2";
    public static final String XML_RSP_TRAP_TYPE_ALT1= "alt1";
    public static final String XML_RSP_TRAP_TYPE_BASE= "base";
    public static final String XML_RSP_TRAP_TYPE_REPORTABLE= "reportable";
    public static final String XML_RSP_TRAP_TYPE_ALERTABLE= "alertable";
    public static final String XML_RSP_TRAP_TYPE_VOTABLE= "votable";
    public static final String XML_RSP_TRAP_TYPE_AUDIOID= "audioid";
    public static final String XML_RSP_TRAP_TYPE_ANNOUNCES= "announces";
    public static final String XML_RSP_TRAP_TYPE_NOTIFIABLE= "notifiable";
    public static final String XML_RSP_TRAP_TYPE_TYPE_ID= "id";
    public static final String XML_RSP_TRAP_TYPE_LIFETIME= "lifetime";
    public static final String XML_RSP_TRAP_TYPE_LEVEL= "level";
    public static final String XML_RSP_TRAP_TYPE_BIT= "bit";
    public static final String XML_RSP_TRAP_TYPE_LEVEL_NAME= "level_name";
    public static final String XML_RSP_TRAP_TYPE_ICON= "icon";
    public static final String XML_RSP_CATEGORY_SEQUENCE = "sequence";

    // Category response
    public static final String XML_RSP_CATEGORY= "category";
    public static final String XML_RSP_CATEGORY_NAME= "name";
    public static final String XML_RSP_CATEGORY_ID= "id";

    public static final String XML_PARAMS_NPS_EMAIL = "email";
    public static final String XML_PARAMS_NPS_FEEDBACK = "feedback";
    public static final String XML_PARAMS_NPS_SCORE = "score";
    public static final String XML_PARAMS_NPS_PROJID = "projectId";
    public static final String XML_PARAMS_NPS_SOURCEID = "sourceId";
    public static final String XML_PARAMS_NPS_DEVICE = "device";
    public static final String XML_PARAMS_NPS_VERSION = "version";
    public static final String XML_PARAMS_NPS_RELEASE_VERSION = "releaseVersion";

    // Statistics Reponse
    public final static String XML_RSP_STATISTICS_DATA_SECTION = "data";
    public final static String XML_RSP_STATISTICS_TRAPS_SECTION = "reported";
    public final static String XML_RSP_STATISTICS_MY_VOTES_SECTION = "my_trap_votes";
    public final static String XML_RSP_STATISTICS_BREAKDOWN = "breakdown";
    public final static String XML_RSP_STATISTICS_VOTES_ON_MY_TRAPS_SECTION = "votes_on_my_traps";
    public final static String XML_RSP_STATISTICS_COUNT = "count";
    public final static String XML_RSP_STATISTICS_MY_VOTES_YES = "Y";

    public final static String XML_RSP_STATISTICS_TRAP_BRUSH_FIRE = "Brush_Fire";
    public final static String XML_RSP_STATISTICS_TRAP_CHILDREN_AT_PLAY = "Children_at_Play";
    public final static String XML_RSP_STATISTICS_TRAP_COMBO_CAMERA = "Combo_Camera";
    public final static String XML_RSP_STATISTICS_TRAP_CONSTRUCTION_ZONE = "Construction_Zone";
    public final static String XML_RSP_STATISTICS_TRAP_DANGEROUS_CURVE = "Dangerous_Curve";
    public final static String XML_RSP_STATISTICS_TRAP_DANGEROUS_INTERSECTION = "Dangerous_Intersection";
    public final static String XML_RSP_STATISTICS_TRAP_EV_CHARGING_STATION = "EV_Charging_Station";
    public final static String XML_RSP_STATISTICS_TRAP_FIXED_SPEED_CAMERA = "Fixed_Speed_Camera";
    public final static String XML_RSP_STATISTICS_TRAP_FLOODED_ROAD = "Flooded_Road";
    public final static String XML_RSP_STATISTICS_TRAP_ICE_ON_ROAD = "Ice_On_Road";
    public final static String XML_RSP_STATISTICS_TRAP_KNOWN_ENFORCEMENT_POINT = "Known_Enforcement_Point";
    public final static String XML_RSP_STATISTICS_TRAP_LIVE_POLICE = "Live_Police";
    public final static String XML_RSP_STATISTICS_TRAP_MSC_OFTEN_SEEN_HERE = "MSC_Often_Seen_Here";
    public final static String XML_RSP_STATISTICS_TRAP_MOBILE_SPEED_CAMERA = "Mobile_Speed_Camera";
    public final static String XML_RSP_STATISTICS_TRAP_NARROW_BRIDGE_AHEAD = "Narrow_Bridge_Ahead";
    public final static String XML_RSP_STATISTICS_TRAP_RED_LIGHT_CAMERA = "Red_Light_Camera";
    public final static String XML_RSP_STATISTICS_TRAP_ROAD_CLOSED_AHEAD = "Road_Closed_Ahead";
    public final static String XML_RSP_STATISTICS_TRAP_ROAD_HAZARD = "Road_Hazard";
    public final static String XML_RSP_STATISTICS_TRAP_ROAD_KILL = "Road_Kill";
    public final static String XML_RSP_STATISTICS_TRAP_SCHOOL_ZONE = "School_Zone";
    public final static String XML_RSP_STATISTICS_TRAP_TOLL_BOOTH = "Toll_Booth";
    public final static String XML_RSP_STATISTICS_TRAP_TRAFFIC_JAM = "Traffic_Jam";
    public final static String XML_RSP_STATISTICS_TRAP_ACCIDENT = "Accident";


    // Launch Message Response
    public final static String XML_RSP_LAUNCH_MESSAGE_DISPLAY = "display";
    public final static String XML_RSP_LAUNCH_MESSAGE_TITLE = "title";
    public final static String XML_RSP_LAUNCH_MESSAGE_MESSAGE = "message";
    public final static String XML_RSP_LAUNCH_MESSAGE_URL = "url";
    public final static String XML_RSP_LAUNCH_MESSAGE_BUTTON = "button";
}
