package com.trapster.android.model.dao;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.CursorLoader;

import com.here.android.common.GeoBoundingBox;
import com.here.android.common.GeoCoordinate;
import com.here.android.mapping.Map;
import com.trapster.android.Defaults;
import com.trapster.android.manager.TrapManager;
import com.trapster.android.model.TrapType;
import com.trapster.android.model.provider.TrapProvider;

import java.util.ArrayList;

/**
 * This class is designed to query a database with a SpatialElement and return
 * those elements that are bounded by the current map.
 * <p/>
 * It also monitors the URI for any changes and automatically updates the map
 *
 * @author John
 */
public class MapBoundedCursorLoader extends CursorLoader {

    private static final String LOGNAME = "MapBoundedCursorLoader";

    private Map map;

    private TrapManager trapManager;

    private SharedPreferences sharedPreferences;

    /**
     * Not really sure why the injectors wouldn't work. Likely it is because the TrapMapLayer calls this from an injected method.
     *
     *
     * @param map
     * @param context
     * @param uri
     * @param projection
     * @param selection
     * @param selectionArgs
     * @param sortOrder
     */
    public MapBoundedCursorLoader(Map map, Context context, Uri uri,
                                  String[] projection, String selection, String[] selectionArgs,
                                  String sortOrder, TrapManager trapManager, SharedPreferences sharedPreferences) {
        super(context, uri, projection, selection, selectionArgs, sortOrder);
        this.trapManager = trapManager;
        this.sharedPreferences = sharedPreferences;

        this.map = map;

           /*
         * This prevents calls from happening more often than every n second(s).
		 */
        setUpdateThrottle(500);
    }


    public void setMap(Map map) {
        this.map = map;
    }


    @Override
    public Cursor loadInBackground() {

        String where = null;

        if (map != null
                && map.getBoundingBox() != null) {
            GeoBoundingBox bbox = map.getBoundingBox();

            if (bbox.isValid())
            {
                GeoCoordinate bottomRight = bbox.getBottomRight();
                GeoCoordinate topLeft = bbox.getTopLeft();

                StringBuilder sb = new StringBuilder();

              /*  sb.append(TrapProvider.FIELD_LATITUDE
                        + ">" + bottomRight.getLatitude() + " and " + TrapProvider.FIELD_LATITUDE + "<" + topLeft.getLatitude() + " and "
                        + TrapProvider.FIELD_LONGITUDE + ">" + topLeft.getLongitude() + " and "
                        + TrapProvider.FIELD_LONGITUDE + "<" + bottomRight.getLongitude());*/

                sb.append(TrapProvider.FIELD_LATITUDE
                        + ">" + Math.min(bottomRight.getLatitude(),topLeft.getLatitude()) + " and " + TrapProvider.FIELD_LATITUDE + "<" + Math.max(bottomRight.getLatitude(),topLeft.getLatitude()) + " and "
                        + TrapProvider.FIELD_LONGITUDE + ">" + Math.min(topLeft.getLongitude(),bottomRight.getLongitude()) + " and "
                        + TrapProvider.FIELD_LONGITUDE + "<" + Math.max(bottomRight.getLongitude(),topLeft.getLongitude()));

                //+ " and "+TrapProvider.FIELD_EXPIRY + ">" + System.currentTimeMillis();

                // Add filtering by visible trap type
                ArrayList<TrapType> hiddenTrapTypes = new ArrayList<TrapType>();
                if (trapManager.getSortedTrapTypes() != null)
                {
                    for (TrapType trapType : trapManager.getSortedTrapTypes()) {
                        if (!sharedPreferences.getBoolean(trapType.getAudioid() + Defaults.SETTING_SHOW_ON_MAP, true)) {
                            hiddenTrapTypes.add(trapType);
                        }
                    }
                }

                if (hiddenTrapTypes.size() > 0) {
                    sb.append(" and " + TrapProvider.FIELD_TYPE + " NOT IN (");

                    for (TrapType trapType : hiddenTrapTypes) {
                        sb.append(trapType.getId());
                        sb.append(",");

                    }

                    sb.deleteCharAt(sb.length() - 1);// remove last comma
                    sb.append(")");
                }
                where = sb.toString();

                //android.util.Log.v(LOGNAME,"Setting query string to:"+where);
            }
            setSelection(where);

            //setSortOrder("ROWID LIMIT 50");
            }

        return super.loadInBackground();

    }


    @Override
    public void deliverResult(Cursor cursor) {
        super.deliverResult(cursor);
    }


}
