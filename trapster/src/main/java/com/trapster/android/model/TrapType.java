package com.trapster.android.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;



public class TrapType {

    @SerializedName("id")
	private int id;

    @SerializedName("name")
	private String name;

    @SerializedName("description")
	private String description;

    @SerializedName("reportable")
	private boolean reportable;

    @SerializedName("alertable")
	private boolean alertable;

    @SerializedName("votable")
	private boolean votable;

    @SerializedName("audioid")
	private String audioid;

    @SerializedName("announces")
	private boolean announces;

    @SerializedName("notifiable")
	private boolean notifiable;

    @SerializedName("lifetime")
	private long lifetime;

    @SerializedName("category")
	private int categoryId;

    @SerializedName("icons")
	private TrapIcon icon;
	
	/**
	 * Categories are linked when the Trap is accessed from the database. No link is stored 
	 */

    private transient Category category;

    @SerializedName("levels")
    private TrapLevels trapLevels = new TrapLevels();

	private transient List<TrapLevel> levels = new ArrayList<TrapLevel>();

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	
	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public TrapIcon getIcon() {
		return icon;
	}

	public void setIcon(TrapIcon icon) {
		this.icon = icon;
	}

	public boolean isReportable() {
		return reportable;
	}

	public void setReportable(boolean reportable) {
		this.reportable = reportable;
	}

	public boolean isAlertable() {
		return alertable;
	}

	public void setAlertable(boolean alertable) {
		this.alertable = alertable;
	}

	public boolean isVotable() {
		return votable;
	}

	public void setVotable(boolean voteable) {
		this.votable = voteable;
	}

	public String getAudioid() {
		return audioid;
	}

	public void setAudioid(String audioid) {
		this.audioid = audioid;
	}

	public boolean isAnnounces() {
		return announces;
	}

	public void setAnnounces(boolean announces) {
		this.announces = announces;
	}

	public boolean isNotifiable() {
		return notifiable;
	}

	public void setNotifiable(boolean notifiable) {
		this.notifiable = notifiable;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<TrapLevel> getLevels() {

        if (trapLevels.getLevels().size() > 0)
		    return trapLevels.getLevels();//levels;
        else
            return levels;
	}

	public void setLevels(List<TrapLevel> levels2) {
		this.trapLevels.setLevels(levels2);
	}

	public void setLifetime(long lifetime) {
		this.lifetime = lifetime;
	}

	public long getLifetime() {
		return lifetime;
	}

    // This allows us to retrieve the english name, regardless of localization - used for Flurry
    public String getUnlocalizedName()
    {
        for (TRAP_TYPE_NAMES name : TRAP_TYPE_NAMES.values())
        {
            if (name.id == getId())
                return name.name;
        }

        return null;
    }

    public String getImageFilename()
    {
        String imageKey = null;
        if (getLevels().size() > 0)
            imageKey = getLevels().get(0).getIcon();
        if (imageKey == null && getIcon() != null)
        {
            if (getIcon().getBase() != null)
                imageKey = getIcon().getBase();
            else if (getIcon().getAlt1() != null)
                imageKey = getIcon().getAlt1();
            else if (getIcon().getAlt2() != null)
                imageKey = getIcon().getAlt2();
        }
        if (imageKey == null)
            imageKey = getAudioid();

        return imageKey;
    }

    public List<String> getImageFilenames() {
        ArrayList<String> iconNames = new ArrayList<String>();
        for(TrapLevel level:getLevels()){
            if(!iconNames.contains(level.getIcon()))
                iconNames.add(level.getIcon());
        }

       return iconNames;
    }

    private enum TRAP_TYPE_NAMES
    {

        LIVE_POLICE("Live Police", 0),
        KNOWN_ENFORCEMENT_POINT("Known Enforcement Point", 3),
        FIXED_SPEED_CAMERA("Fixed Speed Camera", 2),
        MOBILE_SPEED_CAMERA("Mobile Speed Camera", 5),
        RED_LIGHT_CAMERA("Red Light Camera", 1),
        COMBO_CAMERA("Combo Camera", 4),
        ACCIDENT("Accident", 21),
        BRUSH_FIRE("Brush Fire", 22),
        CONSTRUCTION_ZONE("Construction Zone", 31),
        FLOODED_ROAD("Flooded Road", 25),
        MSC_OFTEN_SEEN_HERE("MSC Often Seen Here", 7),
        CHILDREN_AT_PLAY("Children At Play", 20),
        DANGEROUS_INTERSECTION("Dangerous Intersection", 23),
        DANGEROUS_CURVE("Dangerous Curve", 24),
        ICE_ON_ROAD("Ice On Road", 26),
        NARROW_BRIDGE("Narrow Bridge", 27),
        ROAD_CLOSED_AHEAD("Road Closed Ahead", 28),
        ROAD_KILL("Road Kill", 29),
        TOLL_BOOTH("Toll Booth", 30),
        SCHOOL_ZONE("School Zone", 32),
        ROAD_HAZARD("Road Hazard", 33),
        TRAFFIC_JAM("Traffic Jam", 34),
        EV_CHARGING_STATION("EV Charging Station", 40);

        int id;
        String name;
        TRAP_TYPE_NAMES(String name, int id)
        {
            this.id = id;
            this.name = name;
        }
    }
	
	
	
}
