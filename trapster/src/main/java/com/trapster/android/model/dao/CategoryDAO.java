package com.trapster.android.model.dao;

import roboguice.inject.ContextSingleton;
import android.content.Context;
import android.database.Cursor;

import com.google.inject.Inject;
import com.trapster.android.model.Category;
import com.trapster.android.model.provider.CategoryProvider;

@ContextSingleton
public class CategoryDAO  {

	@Inject
	Context context;
	
	public CategoryDAO() {
	}
	
	public Category findById(int id){
		
		Category ir = null;

		String where = CategoryProvider.KEY_ID+"="+id;
		String[] whereArgs = null;

		Cursor cursor = null;

		try {
			cursor = context.getContentResolver().query(CategoryProvider.CONTENT_URI,
					null, where, whereArgs, null);

			if (cursor != null && cursor.moveToFirst()) {

				ir = CategoryProvider.fromCursor(cursor);

			}
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return ir;
	}
	
	
	public void save(Category r) {

		context.getContentResolver().insert(CategoryProvider.CONTENT_URI, CategoryProvider.toContentValues(r));

	}


}