package com.trapster.android.model;

import com.here.android.common.GeoCoordinate;
import com.here.android.mapping.MapFactory;
import com.google.gson.annotations.SerializedName;
import com.trapster.android.RESTDefaults;


public class PatrolPoint {

    @SerializedName(RESTDefaults.REST_PARAMETER_PATROL_LATITUDE)
    private double lat;

    @SerializedName(RESTDefaults.REST_PARAMETER_PATROL_LONGITUDE)
    private double lon;

    @SerializedName(RESTDefaults.REST_PARAMETER_PATROL_COLOR)
    private String color;

    private GeoCoordinate geoCoordinate;

    public double getLat() {
        return lat;
    }
    public void setLat(double lat) {
        this.lat = lat;
    }
    public double getLon() {
        return lon;
    }
    public void setLon(double lon) {
        this.lon = lon;
    }
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }

    public GeoCoordinate getGeoCoordinate() {
        if(geoCoordinate == null)
            geoCoordinate = MapFactory.createGeoCoordinate(lat, lon);

        return geoCoordinate;
    }

    @Override
    public boolean equals(Object o)
    {
        PatrolPoint patrolPoint= (PatrolPoint) o;
        return
                this.lat == patrolPoint.lat &&
                        this.lon == patrolPoint.lon &&
                        this.color != null &&
                        patrolPoint.color != null &&
                        this.color.equals(patrolPoint.color);
    }
    @Override
    public int hashCode()
    {
        return toString().hashCode();
    }
    @Override
    public String toString()
    {
        return this.lat + "|" + this.lon + "|" + this.color;
    }
}

