package com.trapster.android.model.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import com.trapster.android.model.MapTileUpdateResource;
import com.trapster.android.model.PatrolPath;
import com.trapster.android.model.PatrolPoint;

import java.util.ArrayList;


public class PatrolPathProvider extends ContentProvider {

    private static final String LOGNAME = "PatrolPathProvider";

    private static final String PATROL_DATABASE_NAME = "PatrolPath.db";
    private static final String PATH_TABLE = "Paths";

    private static final int DATABASE_VERSION = 3;
    /*
     * Version 3 - added delete trigger		
     */
    		

    // Column Names
    public static final String PATH_FIELD_ID = "_id";
    public static final String PATH_FIELD_TILEREF = "tileref";
 
    private static final String POINTS_TABLE = "points";

    private static final String POINTS_FIELD_ID = "_id";
    private static final String POINTS_FIELD_LATITUDE = "latitude";
    private static final String POINTS_FIELD_LONGITUDE = "longitude";
    public static final String POINTS_FIELD_PATHID = "pathid";
    public static final String POINTS_FIELD_SEQUENCE = "sequence";
    private static final String POINTS_FIELD_COLOR = "color";

    private static final String PATH_DATABASE_CREATE = "create table "
            + PATH_TABLE + " (" + PATH_FIELD_ID
            + " INTEGER primary key autoincrement, "
            + PATH_FIELD_TILEREF + " TEXT); ";

    private static final String POINTS_DATABASE_CREATE = "create table "
            + POINTS_TABLE + " (" + POINTS_FIELD_ID
            + " INTEGER primary key autoincrement, "
            + POINTS_FIELD_LATITUDE + " FLOAT, " + POINTS_FIELD_LONGITUDE
            + " FLOAT, " + POINTS_FIELD_PATHID + " INT, "
            + POINTS_FIELD_SEQUENCE + " INT, " + POINTS_FIELD_COLOR
            + " TEXT); ";

    private static final int PATROLPATH = 1;
    private static final int PATROLPOINT = 2;

    public static final String PROVIDER_NAME = "com.trapster.android.patrolpath";

    public static final Uri PATH_CONTENT_URI = Uri.parse("content://"
            + PROVIDER_NAME + "/patrolpath");

    public static final Uri POINT_CONTENT_URI = Uri.parse("content://"
            + PROVIDER_NAME + "/patrolpoint");

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "patrolpath", PATROLPATH);
        uriMatcher.addURI(PROVIDER_NAME, "patrolpoint", PATROLPOINT);
    }

    // Variable to hold the database instance
    private SQLiteDatabase db;
    private PatrolDatabaseHelper dbHelper;
    static private PatrolPath mPatrolPath;

    @Override
    public boolean onCreate() {
        this.dbHelper = new PatrolDatabaseHelper(getContext(), PATROL_DATABASE_NAME, null,
                DATABASE_VERSION);
        try {
            db = dbHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            db = null;
{/*             //Log.d(LOGNAME, "Database Opening exception"); */}
        }

        return (db == null) ? false : true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String where, String[] whereArgs, String sort) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        String orderBy = null;

        switch (uriMatcher.match(uri)) {
            case PATROLPATH:
                qb.setTables(PATH_TABLE);
                if (TextUtils.isEmpty(sort)) {
                    orderBy = PATH_FIELD_TILEREF + " ASC";
                } else {
                    orderBy = sort;
                }

                break;
            case PATROLPOINT:
                qb.setTables(POINTS_TABLE);
                if (TextUtils.isEmpty(sort)) {
                    orderBy = POINTS_FIELD_SEQUENCE + " ASC";
                } else {
                    orderBy = sort;
                }
                break;
            default:
                throw new SQLException("Failed to insert row into " + uri);
        }
{/* 
        //Log.d(LOGNAME, "URI:" + uri.toString() + ":" + uriMatcher.match(uri)); */}
{/*         //Log.d(LOGNAME,
                "Running:"
                        + qb.buildQueryString(false, qb.getTables(),
                        projection, where, null, null, orderBy, null)); */}

        // Apply the query to the underlying database.
        Cursor c = qb.query(db, projection, where, whereArgs, null, null,
                orderBy);
{/* 
        //Log.d(LOGNAME, "Query results:" + c.getCount()); */}

        // Register the contexts ContentResolver to be notified if
        // the cursor result set changes.
        c.setNotificationUri(getContext().getContentResolver(), uri);

        // Return a cursor to the query result.
        return c;
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case PATROLPATH:
                return "vnd.android.cursor.dir/vnd.trapster.patrolpath";
            case PATROLPOINT:
                return "vnd.android.cursor.dir/vnd.trapster.patrolpoint";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        switch (uriMatcher.match(uri)) {
            case PATROLPATH:
                // Insert the new row, will return the row number if successful.
                long rowID = db.insert(PATH_TABLE, "nullhack", values);
                if (rowID > 0) {
                    Uri _uri = ContentUris.withAppendedId(PATH_CONTENT_URI, rowID);
                    getContext().getContentResolver().notifyChange(uri, null);
                    addPoints((int)rowID);
                    //Log,.v(LOGNAME, "Inserting PATROLPATH:"+rowID);
                    return _uri;
              }
                break;
            case PATROLPOINT:
                rowID = db.insert(POINTS_TABLE, "nullhack", values);
                if (rowID > 0) {
                    Uri _uri = ContentUris.withAppendedId(POINT_CONTENT_URI, rowID);
                    getContext().getContentResolver().notifyChange(uri, null);
                    return _uri;
                }
                break;
            default:
                throw new SQLException("Failed to insert row into " + uri);
        }

        // Return a URI to the newly inserted row on success.
        throw new SQLException("Failed to insert row into " + uri);
    }

    @Override
    public int delete(Uri uri, String where, String[] whereArgs) {
        int count = 0;
        switch (uriMatcher.match(uri)) {
            case PATROLPATH:
                count = db.delete(PATH_TABLE, where, whereArgs);

                if(count > 0)
                    getContext().getContentResolver().notifyChange(uri, null);
                //else
                //    Log.v(LOGNAME,"No Patrol paths to delete. Not sending update");

                break;
            case PATROLPOINT:
            	count = db.delete(POINTS_TABLE, where, whereArgs);
                break;

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String where, String[] whereArgs) {
        int count;
        switch (uriMatcher.match(uri)) {
            case PATROLPATH:
                count = db.update(PATH_TABLE, values, where, whereArgs);
                //Log,.v(LOGNAME, "Updating TrapType:"+count);
                break;
            case PATROLPOINT:
                count = db.update(POINTS_TABLE, values, where, whereArgs);
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        if(count > 0)
            getContext().getContentResolver().notifyChange(uri, null);

        return count;
    }

    public static int getPatrolPathIdFromCursor(Cursor cursor)
    {
       return cursor.getInt(cursor.getColumnIndex(PATH_FIELD_ID));
    }

    public static ArrayList<PatrolPoint> getPointsFromCursor(Cursor cursor)
    {
        ArrayList<PatrolPoint> points = new ArrayList<PatrolPoint>();

        if (cursor != null && cursor.moveToFirst()) {

            do {

                PatrolPoint point = new PatrolPoint();

                point.setColor(cursor.getString(cursor
                        .getColumnIndex(POINTS_FIELD_COLOR)));
                point.setLat(cursor.getFloat(cursor
                        .getColumnIndex(POINTS_FIELD_LATITUDE)));
                point.setLon(cursor.getFloat(cursor
                        .getColumnIndex(POINTS_FIELD_LONGITUDE)));

                points.add(point);

            } while (cursor.moveToNext());

        }
        return points;
    }

    
    public static ContentValues toContentValues(MapTileUpdateResource tile, PatrolPath path) {
        ContentValues v = new ContentValues();

        ContentValues cv = new ContentValues();
        cv.put(PATH_FIELD_TILEREF, tile.getReference());
        setPatrolPath(path);

        return cv;
    }

    private static void setPatrolPath(PatrolPath path)
    {
        mPatrolPath = path;
    }

    /**
     *
     *
     * @param pathId
     */
    private void addPoints(int pathId) {

        String defaultColour = mPatrolPath.getBaseColor();

        int sequence = 0;

        for (PatrolPoint point : mPatrolPath.getPoints()) {
            ContentValues cv = new ContentValues();
            cv.put(POINTS_FIELD_COLOR,
                    (point.getColor() == null) ? defaultColour : point
                            .getColor());
            cv.put(POINTS_FIELD_LATITUDE, point.getLat());
            cv.put(POINTS_FIELD_LONGITUDE, point.getLon());
            cv.put(POINTS_FIELD_PATHID, pathId);
            cv.put(POINTS_FIELD_SEQUENCE, sequence++);

            db.insert(POINTS_TABLE, "null", cv);

        }

    }


    // Helper class for opening, creating, and managing database version control
    private static class PatrolDatabaseHelper extends SQLiteOpenHelper {



        public PatrolDatabaseHelper(Context context, String name,
                                    SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(PATH_DATABASE_CREATE);
            db.execSQL(POINTS_DATABASE_CREATE);
            addTriggers(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            db.execSQL("DROP TABLE IF EXISTS " + PATH_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + POINTS_TABLE);
            onCreate(db);
        }
        
        private void addTriggers(SQLiteDatabase db) {

			String databaseTrigger = "CREATE TRIGGER fkd_" + PATH_TABLE + "_"
					+ POINTS_TABLE + " BEFORE DELETE ON " + PATH_TABLE
					+ " FOR EACH ROW BEGIN " + " DELETE FROM " + POINTS_TABLE
					+ " WHERE " + POINTS_FIELD_PATHID  + " = OLD." +  PATH_FIELD_ID + "; "
					+ " END;";

			db.execSQL(databaseTrigger);
		}
        
    }
}
