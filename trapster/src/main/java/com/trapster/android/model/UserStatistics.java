package com.trapster.android.model;

import java.util.Arrays;
import java.util.List;

public class UserStatistics
{
    private TrapStatistics trapStatistics;
    private MyVoteStatistics myVoteStatistics;
    private VotesOnMyTrapsStatistics votesOnMyTrapsStatistics;

    private static final List<Integer> safeDriverTraps = Arrays.asList(new Integer[]{33, 24, 23, 26, 20, 17, 29, 32});
    private static final List<Integer> moneyTraps = Arrays.asList(new Integer[]{0, 2, 4, 5, 1, 7, 3});
    private static final List<Integer> timeTraps = Arrays.asList(new Integer[] {21, 22, 31, 25, 28, 30, 34});

    public UserStatistics()
    {
        trapStatistics = new TrapStatistics();
        myVoteStatistics = new MyVoteStatistics();
        votesOnMyTrapsStatistics = new VotesOnMyTrapsStatistics();
    }

    public TrapStatistics getTrapStatistics()
    {
        return trapStatistics;
    }

    public MyVoteStatistics getMyVoteStatistics()
    {
        return myVoteStatistics;
    }

    public VotesOnMyTrapsStatistics getVotesOnMyTrapsStatistics()
    {
        return votesOnMyTrapsStatistics;
    }


    public static class TrapStatistics
    {
        private int safeDriverCount = 0;
        private int noTicketCount = 0;
        private int timeSavedCount = 0;

        public int getTrapReportCount()
        {
            return safeDriverCount + noTicketCount + timeSavedCount;
        }

        public int getSafeDriverCount()
        {
            return safeDriverCount;
        }

        public int getNoTicketCount()
        {
            return noTicketCount;
        }
        public int getTimeSavedCount()
        {
            return timeSavedCount;
        }

        public void addTrapStat(String trapTypeID, int count)
        {
            int trapType = Integer.valueOf(trapTypeID);
            if (safeDriverTraps.contains(trapType))
            {
                safeDriverCount += count;
            }
            else if (moneyTraps.contains(trapType))
            {
                noTicketCount += count;
            }
            else if (timeTraps.contains(trapType))
            {
                timeSavedCount += count;
            }
        }
    }

    public static class MyVoteStatistics
    {
        private int safeDriverCount = 0;
        private int noTicketCount = 0;
        private int timeSavedCount = 0;

        public int getNumVotes()
        {
            return safeDriverCount + noTicketCount + timeSavedCount;
        }

        public int getSafeDriverCount()
        {
            return safeDriverCount;
        }

        public int getNoTicketCount()
        {
            return noTicketCount;
        }
        public int getTimeSavedCount()
        {
            return timeSavedCount;
        }

        public void addTrapStat(String trapTypeID, int count)
        {
            int trapType = Integer.valueOf(trapTypeID);
            if (safeDriverTraps.contains(trapType))
            {
                safeDriverCount += count;
            }
            else if (moneyTraps.contains(trapType))
            {
                noTicketCount += count;
            }
            else if (timeTraps.contains(trapType))
            {
                timeSavedCount += count;
            }
        }
    }

    public static class VotesOnMyTrapsStatistics
    {
        private int safeDriverCount = 0;
        private int noTicketCount = 0;
        private int timeSavedCount = 0;

        public int getNumVotes()
        {
            return safeDriverCount + noTicketCount + timeSavedCount;
        }

        public int getSafeDriverCount()
        {
            return safeDriverCount;
        }

        public int getNoTicketCount()
        {
            return noTicketCount;
        }
        public int getTimeSavedCount()
        {
            return timeSavedCount;
        }

        public void addTrapStat(String trapTypeID, int count)
        {
            int trapType = Integer.valueOf(trapTypeID);
            if (safeDriverTraps.contains(trapType))
            {
                safeDriverCount += count;
            }
            else if (moneyTraps.contains(trapType))
            {
                noTicketCount += count;
            }
            else if (timeTraps.contains(trapType))
            {
                timeSavedCount += count;
            }
        }
    }

    public boolean isEmpty()
    {
        return  votesOnMyTrapsStatistics.getNumVotes() == 0
                && trapStatistics.getTrapReportCount() == 0
                && myVoteStatistics.getNumVotes() == 0;
    }
}
