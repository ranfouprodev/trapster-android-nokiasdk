package com.trapster.android.model;

import com.google.gson.annotations.SerializedName;

public class Formats {

    @SerializedName("aiff")
    private String aiff;

    @SerializedName("mp3")
    private String mp3;

    public String getAiff() {
        return aiff;
    }
    public void setAiff(String aiff) {
        this.aiff = aiff;
    }
    public String getMp3() {
        return mp3;
    }
    public void setMp3(String mp3) {
        this.mp3 = mp3;
    }

}
