package com.trapster.android.model;


import com.google.gson.annotations.SerializedName;
import com.here.android.common.GeoCoordinate;
import com.here.android.mapping.MapFactory;
import com.trapster.android.RESTDefaults;
import com.trapster.android.util.GeographicUtils;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.simplify.DouglasPeuckerSimplifier;

import java.util.ArrayList;
import java.util.HashMap;


public class PatrolPath {

    private static final String LOGNAME = "PatrolPath";

    @SerializedName(RESTDefaults.REST_PARAMETER_PATROL_PATROLPOINTS)
    private ArrayList<PatrolPoint> points;

    private String baseColor;


    public PatrolPath(){
        points = new ArrayList<PatrolPoint>();
    }

    public void add(PatrolPoint point){
        points.add(point);
    }


    public ArrayList<PatrolPoint> getPoints() {
        return points;
    }

    public GeoCoordinate[] getGeoCoordinates() {
/*        int size;
        if (points.size() >= PatrolMapLayer.MAX_PATROL_SIZE)
            size = PatrolMapLayer.MAX_PATROL_SIZE;
        else
            size = points.size();*/

        int size = points.size();
        GeoCoordinate[] geoCoordinates = new GeoCoordinate[size];

        for (int i = 0; i < size; i++)
        {
            PatrolPoint point = points.get(i);
            GeoCoordinate geoCoordinate = MapFactory.createGeoCoordinate(point.getLat(), point.getLon());
            geoCoordinates[i] = geoCoordinate;
        }
/*        int counter = 0;
        for(PatrolPoint point: points){
            GeoCoordinate geoCoordinate = MapFactory.createGeoCoordinate(point.getLat(), point.getLon());
            geoCoordinates[counter] = geoCoordinate;
            counter++;
        }*/

        return geoCoordinates;
    }


    public void setPoints(ArrayList<PatrolPoint> points) {
        this.points = points;
    }


    private void setBaseColor(String baseColor) {
        // Base color is sent RGBA from the server, needs to be ARGB for the internal parser
        if(baseColor.length() > 6){ // Ignore if the Alpha is missing
            String alpha = baseColor.substring(6);
            String color = baseColor.substring(0,6);

            this.baseColor = alpha + color;
        }else
            this.baseColor = baseColor;
    }


    public String getBaseColor() {
        if(baseColor != null)
            return baseColor;

        for(PatrolPoint point:points){
            if(point.getColor() != null && !point.getColor().equals("")){
                setBaseColor(point.getColor());
                return baseColor;
            }

        }

        return null;
    }

    @Override
    public boolean equals(Object o)
    {
        PatrolPath patrolPath= (PatrolPath) o;
        ArrayList<PatrolPoint> patrolPathPoints= patrolPath.points;
        int n= points.size();
        if (n == patrolPathPoints.size())
        {
            for (int i= 0; i < n; i++)
            {
                PatrolPoint patrolPoint= points.get(i);
                if (!patrolPoint.equals(patrolPathPoints.get(i)))
                    return false;
            }
            return true;
        }
        return false;
    }
    @Override
    public int hashCode()
    {
        return toString().hashCode();
    }
    @Override
    public String toString()
    {
        StringBuilder string= new StringBuilder("[");
        int n= points.size();
        for (int i= 0; i < n; i++)
        {
            if (i > 0)
                string.append(",");
            string.append("[");
            string.append(points.get(i).toString());
            string.append("]");
        }
        string.append("]");
        return string.toString();
    }

    public boolean isValid()
    {
        return points.size() > 0;
    }

    public PatrolPath simplify(){

        int size = points.size();

        Coordinate[] pointList = new Coordinate[size];

        HashMap<Coordinate,PatrolPoint> pointReference = new HashMap<Coordinate,PatrolPoint>();

        for (int i = 0; i < size; i++)
        {
            PatrolPoint point = points.get(i);

            Coordinate coord = new Coordinate(point.getLon(),point.getLat());
            pointList[i] = coord;
            pointReference.put(coord,point);

        }

        Geometry geometry = new GeometryFactory().createLineString(pointList);

        double simplifyLevel = GeographicUtils.calculateLatitudeSpanOnSameLongitudialLine(5);

        Geometry simplified = DouglasPeuckerSimplifier.simplify(geometry, simplifyLevel);//0.00015);

        //Log.v(LOGNAME,"Simplifying linestring original:"+size+" new:"+simplified.getCoordinates().length);

        points = new ArrayList<PatrolPoint>();
        for(int j=0; j < simplified.getCoordinates().length; j++){
            points.add(pointReference.get(simplified.getCoordinates()[j]));
        }

        return this;

    }

}
