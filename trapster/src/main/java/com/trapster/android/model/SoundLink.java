package com.trapster.android.model;

import com.google.gson.annotations.SerializedName;

public class SoundLink {
    @SerializedName("lid")
    private String lid;

    @SerializedName("path")
    private String path;

    @SerializedName("formats")
    private Formats formats;

    public String getLid() {
        return lid;
    }
    public void setLid(String lid) {
        this.lid = lid;
    }
    public String getPath() {
        return path;
    }
    public void setPath(String path) {
        this.path = path;
    }
    public Formats getFormats() {
        return formats;
    }
    public void setFormats(Formats formats) {
        this.formats = formats;
    }


}
