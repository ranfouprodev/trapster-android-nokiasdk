package com.trapster.android.model;

import com.google.gson.annotations.SerializedName;
import com.trapster.android.RESTDefaults;

import java.util.ArrayList;

public class PatrolPaths {

    @SerializedName(RESTDefaults.REST_PARAMETER_PATROL_PATROLLINES)
    private ArrayList<PatrolPath> paths = new ArrayList<PatrolPath>();


    public void setPaths(ArrayList<PatrolPath> path) {
        this.paths = path;
    }

    public ArrayList<PatrolPath> getPaths() {
        return paths;
    }
}
