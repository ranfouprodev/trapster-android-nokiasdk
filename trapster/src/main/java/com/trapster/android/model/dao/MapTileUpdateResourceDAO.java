package com.trapster.android.model.dao;

import java.util.ArrayList;

import android.content.ContentValues;
import roboguice.inject.ContextSingleton;
import android.content.Context;
import android.database.Cursor;

import com.google.inject.Inject;
import com.trapster.android.model.MapTileUpdateResource;
import com.trapster.android.model.PatrolPoint;
import com.trapster.android.model.Trap;
import com.trapster.android.model.provider.MapTileUpdateResourceProvider;
import com.trapster.android.model.provider.PatrolPathProvider;
import com.trapster.android.model.provider.TrapProvider;

@ContextSingleton
public class MapTileUpdateResourceDAO{

	@Inject
	Context context;
	
	public MapTileUpdateResourceDAO() {
	}
	
	
	public void save(MapTileUpdateResource r, long updateTime) {

		context.getContentResolver().insert(MapTileUpdateResourceProvider.CONTENT_URI, MapTileUpdateResourceProvider.toContentValues(r, updateTime));

	}

	public MapTileUpdateResource getMapTileUpdate(String layer,
			int tileX, int tileY, int zoom) {
		
		MapTileUpdateResource mapTile = null;

		MapTileUpdateResource mt = new MapTileUpdateResource(layer, tileX, tileY, zoom);
		
		String where = MapTileUpdateResourceProvider.FIELD_LAYER+" = ? and "+MapTileUpdateResourceProvider.FIELD_REFERENCE+" = ?";
		String[] whereArgs = new String[]{layer, mt.getReference()};

		Cursor cursor = null;

		try {
			cursor = context.getContentResolver().query(MapTileUpdateResourceProvider.CONTENT_URI,
					null, where, whereArgs, null);

			if (cursor != null && cursor.moveToFirst()) {

				mapTile = MapTileUpdateResourceProvider.fromCursor(cursor);

			}
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return mapTile;

	}

	
	public ArrayList<MapTileUpdateResource> getExpiredTiles(String layer, long ageInMs) {

		long expiry = System.currentTimeMillis() - ageInMs; 
		
		String where = MapTileUpdateResourceProvider.FIELD_LAYER+" = ? and "+MapTileUpdateResourceProvider.FIELD_LAST_UPDATE+"<"+expiry;
		String[] whereArgs = new String[]{layer};

		ArrayList<MapTileUpdateResource> points = new ArrayList<MapTileUpdateResource>();

		Cursor cursor = null;
try{
			cursor = context.getContentResolver().query(MapTileUpdateResourceProvider.CONTENT_URI,
					null, where, whereArgs, null);

		

		if (cursor != null && cursor.moveToFirst()) {

			do {
				points.add(MapTileUpdateResourceProvider.fromCursor(cursor));
			} while (cursor.moveToNext());
		}
		
	} finally {
		if (cursor != null)
			cursor.close();
	}
		
		return points;

	}
	
}