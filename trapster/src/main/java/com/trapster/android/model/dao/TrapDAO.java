package com.trapster.android.model.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.google.inject.Inject;
import com.trapster.android.model.MapTileUpdateResource;
import com.trapster.android.model.Trap;
import com.trapster.android.model.provider.TrapProvider;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import roboguice.inject.ContextSingleton;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@ContextSingleton
public class TrapDAO {

	private static final String LOGNAME = "TrapDAO";

	@Inject
	protected Context context;
	
	private static final String MBR_WHERE = TrapProvider.FIELD_LATITUDE
			+ ">? and " + TrapProvider.FIELD_LATITUDE + "<? and "
			+ TrapProvider.FIELD_LONGITUDE + ">? and "
			+ TrapProvider.FIELD_LONGITUDE + "<? ";

	
	public List<Trap> getNewTraps(Date since) {
		return getNewTraps( since, null);
	}

	public List<Trap> getNewTraps(Geometry area) {
		return getNewTraps( new Date(0), area);
	}

	public List<Trap> getNewTraps(Date since, Geometry area) {

		Cursor cursor = null;

		ArrayList<Trap> traps = new ArrayList<Trap>();

		try {

			String where = TrapProvider.FIELD_LVOTE_SEC
					+ " > "
					+ String.valueOf((since != null) ? (since.getTime() / 1000)
							: 0);
			String[] whereArgs = null;

			// Choose only the traps within the boundingbox
			if (area != null) {
				where += " and " + MBR_WHERE;
				whereArgs = getMbrWhereArgs(area);
			}

			cursor = context.getContentResolver().query(
					TrapProvider.CONTENT_URI, null, where, whereArgs, null);
			
			
			if (cursor != null && cursor.moveToFirst()) {

				do {
					Trap trap = TrapProvider.fromCursor(cursor);

					if (area != null && trap.getGeometry() != null
							&& area.contains(trap.getGeometry()))
						traps.add(trap);
					else
						traps.add(trap);
				} while (cursor.moveToNext());
			}

		} finally {
			if (cursor != null)
				cursor.close();
		}

		return traps;

	}

	public int clearAllTraps() {
		int rowsDeleted = context.getContentResolver().delete(
				TrapProvider.CONTENT_URI, null, null);
{/* 		//Log.d(LOGNAME, "Clean up traps:  Removed " + rowsDeleted + " rows"); */}
		return rowsDeleted;
	}
	
	
	public ArrayList<Trap> getListOfDeletedTraps(MapTileUpdateResource tile,
			List<Integer> trapsInTile){

		StringBuilder sb = new StringBuilder();

		sb.append(MBR_WHERE);
		
		if(trapsInTile != null && trapsInTile.size() > 0){
			sb.append(" and " + TrapProvider.FIELD_ID + " NOT IN (");
			for (Integer trapId : trapsInTile) {
				sb.append(trapId);
				sb.append(",");
			}
			
			sb.deleteCharAt(sb.length() - 1);// remove last comma
			sb.append(")");
		}
		
		String where = sb.toString();
		
		String[] whereArgs = getMbrWhereArgs(tile.getBoundingBox());
		
		Cursor cursor = null;
		
		ArrayList<Trap> traps = new ArrayList<Trap>();
		
		try {
		
			cursor = context.getContentResolver().query(
					TrapProvider.CONTENT_URI, null, where, whereArgs, null);
			if (cursor != null && cursor.moveToFirst()) {

				do {
					Trap trap = TrapProvider.fromCursor(cursor);

					traps.add(trap);
					
				} while (cursor.moveToNext());
			}

		} finally {
			if (cursor != null)
				cursor.close();
		}

		return traps;
		
	}

	public void clearOldTrapsFromMapTile(MapTileUpdateResource tile,
			List<Integer> trapsInTile) {

		StringBuilder sb = new StringBuilder();

		sb.append(MBR_WHERE);
		
		if(trapsInTile != null && trapsInTile.size() > 0){
			sb.append(" and " + TrapProvider.FIELD_ID + " NOT IN (");
			for (Integer trapId : trapsInTile) {
				sb.append(trapId);
				sb.append(",");
			}
			
			sb.deleteCharAt(sb.length() - 1);// remove last comma
			sb.append(")");
		}
		
		String where = sb.toString();
 
		/*Log.d(LOGNAME,
				"Removing expired traps from tile " + tile.getReference() + ":"
						+ where); */

		String[] whereArgs = getMbrWhereArgs(tile.getBoundingBox());

		int rowsDeleted = context.getContentResolver().delete(
				TrapProvider.CONTENT_URI, where, whereArgs);
{/* 
		//Log.d(LOGNAME, "Clean up traps:  Removed " + rowsDeleted + " rows"); */}

	}

	public void expireTrapsInArea(Geometry area) {

		String where = MBR_WHERE;
		String[] whereArgs = getMbrWhereArgs(area);

		int rowsDeleted = context.getContentResolver().delete(
				TrapProvider.CONTENT_URI, where, whereArgs);
{/* 
		//Log.d(LOGNAME, "Clean up traps:  Removed " + rowsDeleted + " rows"); */}

	}

	public ArrayList<Trap> getTraps(Geometry area) {

		Cursor cursor = null;

		ArrayList<Trap> traps = new ArrayList<Trap>();

		try {

			String where = null;

			String[] whereArgs = null;

			// Choose only the traps within the geometry
			if (area != null) {
				where = MBR_WHERE;
				whereArgs = getMbrWhereArgs(area);
			}

			cursor = context.getContentResolver().query(
					TrapProvider.CONTENT_URI, null, where, whereArgs, null);
			if (cursor != null && cursor.moveToFirst()) {

				do {
					Trap trap = TrapProvider.fromCursor(cursor);

/*					if (area != null && trap.getGeometry() != null && area.contains(trap.getGeometry()))
						traps.add(trap);
					else*/
						traps.add(trap);
				} while (cursor.moveToNext());
			}

		} finally {
			if (cursor != null)
				cursor.close();
		}

		return traps;

	}

    public ArrayList<Trap> getTrapsByTile(String tileIndex) {

        Cursor cursor = null;

        ArrayList<Trap> traps = new ArrayList<Trap>();

        try {

            String where = TrapProvider.FIELD_TILEINDEX +"=?";

            String[] whereArgs = new String[]{tileIndex};

            // Choose only the traps with the tile index
            cursor = context.getContentResolver().query(
                    TrapProvider.CONTENT_URI, null, where, whereArgs, null);
            if (cursor != null && cursor.moveToFirst()) {

                do {
                    Trap trap = TrapProvider.fromCursor(cursor);
                    traps.add(trap);
                } while (cursor.moveToNext());
            }

        } finally {
            if (cursor != null)
                cursor.close();
        }

        return traps;

    }

	public ArrayList<Trap> getAllTraps() {
		return getTraps(null);
	}

	/**
	 * This will bulkInsert (or update) traps in the database. The content change broadcast
	 * will only happen after the last trap is added so it should be used on all bulk updates
	 * 
	 * @param traps
	 * @return
	 */
	public boolean save(ArrayList<Trap> traps) {
		// Bulk inserts
		ContentValues[] insertValues = new ContentValues[traps.size()];
		int i = 0;

        synchronized (traps)
        {
            for (Trap trap : traps) {
                insertValues[i] = TrapProvider.toContentValues(trap);
                i++;
            }
        }

		if (context.getContentResolver().bulkInsert(TrapProvider.CONTENT_URI,
				insertValues) > 0)
			return true;

		return false;

	}

	/**
	 * Insert or Update a trap. Based on the trap_id field
	 * 
	 * @param trap
	 * @return
	 */
	public void save(Trap trap) {

		context.getContentResolver().insert(TrapProvider.CONTENT_URI, TrapProvider.toContentValues(trap));
		
	}

	public void delete(long trapId) {

        String where = TrapProvider.FIELD_ID + "=?";
        String[] whereArgs = new String[]{String.valueOf(trapId)};

		context.getContentResolver().delete(TrapProvider.CONTENT_URI, where,whereArgs);
	}



    public List<Trap> getListOfExpiredTraps(){

        Cursor cursor = null;

        ArrayList<Trap> traps = new ArrayList<Trap>();

        try {

            long now = System.currentTimeMillis() / 1000;

            String where = TrapProvider.FIELD_EXPIRY + " < "+now +" and "+TrapProvider.FIELD_EXPIRY +" > 0";
            String[] whereArgs = null;

            cursor = context.getContentResolver().query(
                    TrapProvider.CONTENT_URI, null, where, whereArgs, null);
            if (cursor != null && cursor.moveToFirst()) {

                do {
                    Trap trap = TrapProvider.fromCursor(cursor);
                    traps.add(trap);
                } while (cursor.moveToNext());
            }

        } finally {
            if (cursor != null)
                cursor.close();
        }

        return traps;

    }

	public void delete(Trap trap) {
		delete(trap.getId());
	}

	public Trap findById(long trapId) {

		Trap trap = null;

		String where = TrapProvider.FIELD_ID + " = " + trapId;

		Cursor cursor = null;

		try {
			cursor = context.getContentResolver().query(TrapProvider.CONTENT_URI,
					null, where, null, null);

			if (cursor != null && cursor.moveToFirst()) {

				trap = TrapProvider.fromCursor(cursor);

			}
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return trap;

	}

    private String[] getMbrWhereArgs(Envelope mbr) {
        //The order of the string array are set to MinLat, MaxLat, MinLng, MaxLng
        //This is used for Trap Alerting.  Update with caution!!!

        String[] whereArgs = new String[] { String.valueOf(mbr.getMinY()),
            String.valueOf(mbr.getMaxY()), String.valueOf(mbr.getMinX()),
            String.valueOf(mbr.getMaxX())};
        return whereArgs;
    }

	private String[] getMbrWhereArgs(Geometry geometry) {

		Envelope mbr = geometry.getEnvelopeInternal();

		return getMbrWhereArgs(mbr);
	}

}
