package com.trapster.android.model.dao;

import android.content.Context;
import android.database.Cursor;
import com.google.inject.Inject;
import com.trapster.android.model.UpdatedRouteLinkInfo;
import com.trapster.android.model.provider.UpdatedRouteLinkProvider;

import roboguice.inject.ContextSingleton;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 12/8/12
 * Time: 1:04 PM
 * To change this template use File | Settings | File Templates.
 */

@ContextSingleton
public class UpdatedRouteLinkDAO {


    @Inject Context context;

    public UpdatedRouteLinkDAO ()
    {
    }

    public UpdatedRouteLinkInfo findByRouteLink(String linkId)
    {
        UpdatedRouteLinkInfo updatedRouteLinkInfo = null;

        String where = UpdatedRouteLinkProvider.ROUTE_LINK_ID+"=?";
        String[] whereArgs = new String[]{linkId};

        Cursor cursor = null;

        try {
            cursor = context.getContentResolver().query(UpdatedRouteLinkProvider.CONTENT_URI,
                    null, where, whereArgs, null);

            if (cursor != null && cursor.moveToFirst()) {

                updatedRouteLinkInfo = UpdatedRouteLinkProvider.fromCursor(cursor);

            }
        } finally {
            if (cursor != null)
                cursor.close();
        }

        return updatedRouteLinkInfo;
    }

    public void save(ArrayList<UpdatedRouteLinkInfo> routeLinkInfoList) {

        for (int i = 0; i < routeLinkInfoList.size(); i++)
        {
            UpdatedRouteLinkInfo updatedRouteLinkInfo = routeLinkInfoList.get(i);
            if (findByRouteLink(updatedRouteLinkInfo.getRouteLinkId()) != null)
            {
                context.getContentResolver().update(UpdatedRouteLinkProvider.CONTENT_URI,
                        UpdatedRouteLinkProvider.toContentValues(updatedRouteLinkInfo),
                        UpdatedRouteLinkProvider.ROUTE_LINK_ID + "=" + updatedRouteLinkInfo.getRouteLinkId(),
                        null);
            }
            else
            {
                context.getContentResolver().insert(UpdatedRouteLinkProvider.CONTENT_URI,
                        UpdatedRouteLinkProvider.toContentValues(updatedRouteLinkInfo));
            }
        }

    }

    public int getSize()
    {
        Cursor cursor = context.getContentResolver().query(UpdatedRouteLinkProvider.CONTENT_URI, null, null, null, null);
       try{
        
        if (cursor != null && cursor.moveToLast())
        {
            return cursor.getPosition();
        }

       } finally {
			if (cursor != null)
				cursor.close();
		}
        
        return 0;
    }

    public int clearCache() {

        int rowsDeleted = context.getContentResolver().delete(UpdatedRouteLinkProvider.CONTENT_URI, null, null);
        return rowsDeleted;
    }
}
