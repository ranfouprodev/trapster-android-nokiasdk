package com.trapster.android.model;

import com.google.gson.annotations.SerializedName;

public class StringResource extends TrapsterResource {

    private transient int id;

    @SerializedName("id")
	private String key;

    @SerializedName("text")
    private String text;



    public void setKey(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}


	public void setText(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	} 
	
}
