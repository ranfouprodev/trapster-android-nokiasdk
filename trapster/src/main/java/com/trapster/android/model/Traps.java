package com.trapster.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Traps implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3738196600183474685L;

    @SerializedName("marker")
	private ArrayList<Trap> traps = new ArrayList<Trap>();
	
	private transient double lat;

	private transient double lon;

	private transient double radius;

	public void setTraps(ArrayList<Trap> traps) {
		this.traps = traps;
	}

	public ArrayList<Trap> getTraps() {
		return traps;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	
	
}
