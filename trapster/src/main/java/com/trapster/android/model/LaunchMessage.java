package com.trapster.android.model;


public class LaunchMessage {

    private String displayText;
    private String title;
    private String message;
    private String buttonText;
    private String url;

    public void setDisplayText(String displayText)
    {
        this.displayText = displayText;
    }
    public String getDisplayText()
    {
        return this.displayText;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }
    public String getTitle()
    {
        return title;
    }

    public void setButtonText(String buttonText)
    {
        this.buttonText = buttonText;
    }
    public String getButtonText()
    {
        return this.buttonText;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }
    public String getUrl()
    {
        return this.url;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
    public String getMessage()
    {
        return this.message;
    }

}
