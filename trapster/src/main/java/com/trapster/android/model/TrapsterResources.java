package com.trapster.android.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TrapsterResources {

    @SerializedName("string")
	private ArrayList<StringResource> strings = new ArrayList<StringResource>();

    @SerializedName("image")
    private ArrayList<ImageResource> images = new ArrayList<ImageResource>();

    @SerializedName("color")
	private ArrayList<ColorResource> colors = new ArrayList<ColorResource>();


	public ArrayList<StringResource> getStrings() {
		return strings;
	}
	public void setStrings(ArrayList<StringResource> strings) {
		this.strings = strings;
	}
	public ArrayList<ImageResource> getImages() {
		return images;
	}
	public void setImages(ArrayList<ImageResource> images) {
		this.images = images;
	}
	public ArrayList<ColorResource> getColors() {
		return colors;
	}
	public void setColors(ArrayList<ColorResource> colors) {
		this.colors = colors;
	}

	
}
