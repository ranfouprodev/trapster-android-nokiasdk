package com.trapster.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.trapster.android.util.SecureHash;

import java.util.Date;

public class User implements Parcelable
{
    public String getTempPassword() {
        return tempPassword;
    }

    public void setTempPassword(String tempPassword) {
        this.tempPassword = tempPassword;
    }

    private String tempPassword;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPassword(String password) {
        if(userName == null)
            throw new IllegalArgumentException("Unknown username");


        this.password = generatePassswordHash(password);
    }


    public static enum Provider{Standard,Google,Facebook};

    private Provider provider;

    private int id;


    private String userName;

    private String password;

    private Profile profile;

    public long getAccessTokenExpiry() {
        return accessTokenExpiry;
    }

    public String getAccessToken() {
        return accessToken;
    }

    private String accessToken;

    private long accessTokenExpiry;

    public User()
	{
        this.profile = new Profile();
	}
	public User(String userName, String password)
	{
		this.userName= userName;
		this.password= generatePassswordHash(password);
        this.provider = Provider.Standard;
        this.profile = new Profile();
	}


    public void setUserName(String userName) {
        this.userName = userName;
        this.profile.setName(userName);
    }

    public void setFacebookCredentials(String name, String email, String accessToken, long tokenExpiry){
        this.provider = Provider.Facebook;

        this.accessToken = accessToken;

        this.accessTokenExpiry = tokenExpiry;

        profile.setName(name);

        profile.setEmail(email);

    }

    public void setEmailCredentials(String email, String password){
        this.profile.setEmail(email);
        this.tempPassword= password; // Temporary hack since the password hash needs the username
        this.provider = Provider.Standard;
    }

    public void setEmail(String email){
        this.profile.setEmail(email);
    }

    public void setSignUpDate(Date signup){
        this.profile.setSignUp(signup);
    }

    public void setCredentials(String userName, String password){
        this.userName= userName;
        this.password= generatePassswordHash(password);
        this.provider = Provider.Standard;
    }

	public String getUserName()
	{
		return userName;
	}
	public String getPassword()
	{
		return password;
	}


    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    private String generatePassswordHash(String password)
    {
        String hashalg = userName + "TRAPSTER" + password;
        try
        {
            return SecureHash.secureHash("MD5", hashalg);
        }catch (Exception e){}
        return null;
    }

    public void generatePassswordHash(){
        this.password= generatePassswordHash(tempPassword);
    }

    public boolean isFacebook() {
        return provider == Provider.Facebook;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeString(userName);
        out.writeString(password);
        out.writeString(provider.toString());
        out.writeString(accessToken);
        out.writeLong(accessTokenExpiry);
        out.writeInt(id);
        out.writeParcelable(profile,0);

    }

    public transient static final Parcelable.Creator<User> CREATOR
            = new Parcelable.Creator<User>() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };

    private User(Parcel in){
        this.userName = in.readString();
        this.password = in.readString();
        this.provider = Provider.valueOf(in.readString());
        this.accessToken = in.readString();
        this.accessTokenExpiry = in.readLong();
        this.id = in.readInt();
        this.profile = in.readParcelable(Profile.class.getClassLoader());
    }

}
