package com.trapster.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.trapster.android.service.TrapSyncService;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;

public class Trap implements Parcelable {

   // private static final long serialVersionUID = -6143752393786782710L;

    @SerializedName("id")
	private int id;

    @SerializedName("traptypeid")
	private int typeid;

    @SerializedName("numvotes")
	private int num;

    @SerializedName("origdisplayname")
	private String ouname;

    @SerializedName("lastupdatedisplayname")
	private String luname;

	private transient String lvote;

    @SerializedName("lastvoteepochtime")
	private long lvoteSec;

    @SerializedName("trapaddress")
	private String address;

    @SerializedName("badgekey")
	private String badgeKey;

    @SerializedName("trapcredlevel")
	private int level;


    @SerializedName("lifetime")
    private long lifetime;

    /**
     * The index of the map tile for the trap.
     */
    private transient String tileIndex;

    private transient long expiry;
	
	private transient Geometry geometry;

    public Trap(){

    }

	public Geometry getGeometry(){
		return geometry;
	}
	
	public void setGeometry(double lat, double lng){
		geometry = new GeometryFactory().createPoint(new Coordinate(lng,lat));
        tileIndex = MapTileUpdateResource.createFromPoint(TrapSyncService.MAPTILE_CACHE_LAYER, lat,lng, TrapSyncService.MAPTILE_CACHE_ZOOM_LEVEL).getReference();
	}
	
	public double getLatitude(){
		return  ((geometry != null) ? geometry.getCoordinate().y:0f);
	}
	
	public double getLongitude(){
		return  ((geometry != null) ? geometry.getCoordinate().x:0f);
	}
	
	public int getTypeid() {
		return typeid;
	}

	public void setTypeid(int typeid) {
		this.typeid = typeid;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public String getOuname() {
		return ouname;
	}

	public void setOuname(String ouname) {
		this.ouname = ouname;
	}

	public String getLuname() {
		return luname;
	}

	public void setLuname(String luname) {
		this.luname = luname;
	}

	public String getLvote() {
		return lvote;
	}

	public void setLvote(String lvote) {
		this.lvote = lvote;
	}

	public long getLvoteSec() {
		return lvoteSec;
	}

	public void setLvoteSec(long lvoteSec) {
		this.lvoteSec = lvoteSec;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setBadgeKey(String badgeKey) {
		this.badgeKey = badgeKey;
	}

	public String getBadgeKey() {
		return badgeKey;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getLevel() {
		return level;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Trap))
			return false;
		Trap other = (Trap) obj;
        if (address == null && other.address != null)
            return false;
		if (id == other.id && lvoteSec == other.lvoteSec && num == other.num && address.equals(other.address) && level == other.level)
            return true;

			return false;

	}

	public long getExpiry() {
		return expiry;
	}

	public void setExpiry(long expiry) {
		this.expiry = expiry;
	}


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeInt(id);
        out.writeInt(typeid);
        out.writeInt(num);
        out.writeString(ouname);
        out.writeString(luname);
        out.writeLong(lvoteSec);
        out.writeString(address);
        out.writeString(badgeKey);
        out.writeInt(level);
        out.writeLong(getExpiry());
        out.writeDouble(getLatitude());
        out.writeDouble(getLongitude());
    }

    public static final Parcelable.Creator<Trap> CREATOR
            = new Parcelable.Creator<Trap>() {
        public Trap createFromParcel(Parcel in) {
            return new Trap(in);
        }

        public Trap[] newArray(int size) {
            return new Trap[size];
        }
    };
    private Trap(Parcel in){
        this.id = in.readInt();
        this.typeid = in.readInt();
        this.num = in.readInt();
        this.ouname = in.readString();
        this.luname = in.readString();
        this.lvoteSec = in.readLong();
        this.address = in.readString();
        this.badgeKey = in.readString();
        this.level = in.readInt();
        this.expiry = in.readLong();
        double lat = in.readDouble();
        double lng = in.readDouble();
        setGeometry(lat,lng);
    }

    public long getLifetime() {
        return lifetime;
    }

    public void setLifetime(long lifetime) {
        this.lifetime = lifetime;
    }


    public String getTileIndex() {
        return tileIndex;
    }

    public void setTileIndex(String tileIndex) {
        this.tileIndex = tileIndex;
    }
}
