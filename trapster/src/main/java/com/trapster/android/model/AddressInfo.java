package com.trapster.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 10/24/13
 * Time: 2:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class AddressInfo implements Serializable {

    @SerializedName("Label")
    private String label;

    @SerializedName("Country")
    private String country;

    @SerializedName("State")
    private String state;

    @SerializedName("County")
    private String county;

    @SerializedName("City")
    private String city;

    @SerializedName("Street")
    private String street;


    public void setLabel(String label){
        this.label = label;
    }

    public String getLabel(){
        return this.label;
    }

    public void setCountry(String country){
        this.country = country;
    }

    public String getCountry(){
        return this.country;
    }

    public void setState(String state){
        this.state = state;
    }

    public String getState(){
        return this.state;
    }

    public void setCounty(String county){
        this.county = county;
    }

    public String getCounty(){
        return this.county;
    }

    public void setCity(String city){
        this.city = city;
    }

    public String getCity(){
        return this.city;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreet(){
        return this.street;
    }

}
