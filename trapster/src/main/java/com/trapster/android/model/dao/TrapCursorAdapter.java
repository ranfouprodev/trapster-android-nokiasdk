package com.trapster.android.model.dao;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.google.inject.Inject;
import com.here.android.common.GeoPosition;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.activity.fragment.component.TrapMapImageView;
import com.trapster.android.manager.PositionManager;
import com.trapster.android.manager.TrapManager;
import com.trapster.android.model.Trap;
import com.trapster.android.model.TrapType;
import com.trapster.android.util.GeographicUtils;
import com.trapster.android.util.PrettyTime;
import roboguice.RoboGuice;

import java.text.DecimalFormat;
import java.util.*;

public class TrapCursorAdapter extends ArrayAdapter<Trap> {

	@Inject TrapManager tm;
	
	@Inject SharedPreferences prefs;
	
	@Inject PositionManager pm;
	
	private List<Trap> entries = new ArrayList<Trap>();
	
	private GeoPosition location;

	public TrapCursorAdapter(Context context) {
		super(context, R.layout.item_nearbytrap);
		
		RoboGuice.injectMembers(getContext(), this);
		
		this.location = pm.getLastLocation(); 

	}
	
	public void updateLocation(GeoPosition location){
		this.location = location; 
	}

	@Override
	public void add(Trap object) {
		entries.add(object);
		super.add(object);
	}

	@Override
	public void addAll(Collection<? extends Trap> collection) {

        for (Trap trap : collection)
        {
            entries.add(trap);
            super.add(trap);
        }
		//entries.addAll(collection);
		//super.addAll(collection);
	}

	@Override
	public void addAll(Trap... items) {
		for(Trap trap:items)
        {
            entries.add(trap);
            super.add(trap);
        }

		//super.addAll(items);
	}
	
	@Override
	public void clear() {
		entries.clear();
		super.clear();
	}

	@Override
	public Trap getItem(int position) {
		return entries.get(position);
	}

	@Override
	public long getItemId(int position) {
		return getItem(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		View v = convertView;

		if (v == null) {
			LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			holder = new ViewHolder();

			v = vi.inflate(R.layout.item_nearbytrap, null);
			holder.trapTypeText = (TextView) v.findViewById(R.id.textTrapType);
			holder.distance = (TextView) v.findViewById(R.id.textDistanceAway);
			holder.mapSnapshot = (TrapMapImageView) v.findViewById(R.id.imageMapSnapshot);
			holder.lastUpdate = (TextView) v
					.findViewById(R.id.textTrapLastUpdate);
			holder.distanceUnits = (TextView) v.findViewById(R.id.textDistanceAwayUnits);

			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}

		final Trap item = getItem(position);
		if (item != null) {
			bindView(holder, item);
		}

		return v;
	}

	public void bindView(ViewHolder holder, Trap trap) {

		TrapType trapType = tm.getTrapType(trap.getTypeid());

		holder.mapSnapshot.setTrap(trap);
		
		// Distance Away
		if (location != null) {
			double km = (GeographicUtils.geographicDistance(
					location.getCoordinate().getLatitude(), location.getCoordinate().getLongitude(),
					trap.getLatitude(), trap.getLongitude())) / 1000;
			DecimalFormat distFormat = new DecimalFormat("#0.0");
			
			if (prefs.getBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_MILES, true))
            {
				holder.distance.setText(distFormat.format(km * 0.62137));
				holder.distanceUnits.setText(getContext().getString(R.string.traplist_unit_mi));
			}
            else
            {
				holder.distance.setText(distFormat.format(km));
				holder.distanceUnits.setText(getContext().getString(R.string.traplist_unit_km));
			}
		}
		
		// location
		holder.trapTypeText.setText(trapType.getName());

		// last update
		Calendar calendar = Calendar.getInstance();

		long trapTime = trap.getLvoteSec() * 1000;
		calendar.setTimeInMillis(trapTime);

		Date trapDate = calendar.getTime(); 
		
		String timeStr = PrettyTime.formatRelativeTime(getContext().getResources(),
                trapDate);
		
		String fullTimeStr = getContext().getString(R.string.traplist_updated_since,timeStr);
		
		holder.lastUpdate.setText(fullTimeStr);

		
	}
	
	
	
	
	private class ViewHolder {
		public TextView distanceUnits;
		public TextView trapTypeText;
		public TextView distance;
		public TextView lastUpdate;
		public TrapMapImageView mapSnapshot;
	}
	

}
