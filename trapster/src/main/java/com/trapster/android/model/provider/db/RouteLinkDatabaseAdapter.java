package com.trapster.android.model.provider.db;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import com.google.inject.Inject;
import com.trapster.android.model.RouteLink;
import com.trapster.android.model.RouteLinkQuadkey;
import com.trapster.android.model.RouteLinks;
import com.trapster.android.util.BackgroundTask;
import com.vividsolutions.jts.geom.Envelope;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;


public class RouteLinkDatabaseAdapter
{
    static final String LOGNAME = "Trapster.RouteLinkDatabaseAdapter";
    //
    static final String DATABASE_NAME = "RouteLink.db";
    static final int DATABASE_VERSION = 5;
    // tables
    static final String ROUTE_LINK_TABLE = "RouteLink";
    static final String ROUTE_LINK_QUADKEY_TABLE = "RouteLinkQuadkey";
    // fields
    static final String FIELD_LINK_ID = "link_id";
    static final String FIELD_LINK_QUADKEY = "quadkey";
    static final String FIELD_LINK_FUNCTIONAL_CLASS = "function_class";
    static final String FIELD_LINK_LENGTH = "length";
    static final String FIELD_LINK_SPEED_LIMIT= "speed_limit";
    static final String FIELD_LINK_SHAPE = "shape";
    static final String FIELD_LINK_LABEL = "label";
    static final String FIELD_LINK_COUNTRY = "country";
    static final String FIELD_LINK_STATE = "state";
    static final String FIELD_LINK_COUNTY = "county";
    static final String FIELD_LINK_CITY = "city";
    static final String FIELD_LINK_STREET = "street";
    static final String FIELD_LINK_TRAFFIC_SPEED = "traffic_speed";
    static final String FIELD_LINK_TRAFFIC_TIME = "traffic_time";
    static final String FIELD_LINK_BASE_SPEED = "base_speed";
    static final String FIELD_LINK_BASE_TIME = "base_time";
    static final String FIELD_LINK_BBOX_MIN_LAT= "bbox_min_lat";
    static final String FIELD_LINK_BBOX_MIN_LNG= "bbox_min_lng";
    static final String FIELD_LINK_BBOX_MAX_LAT= "bbox_max_lat";
    static final String FIELD_LINK_BBOX_MAX_LNG= "bbox_max_lng";
    //
    static final String FIELD_QUAD_QUADKEY= "quadkey";
    static final String FIELD_QUAD_CREATE_DA= "create_da";
    //
    static final String[] ROUTE_LINK_SELECT_COLUMNS= new String[] { "_id",
            FIELD_LINK_ID,
            FIELD_LINK_QUADKEY,
            FIELD_LINK_FUNCTIONAL_CLASS,
            FIELD_LINK_LENGTH,
            FIELD_LINK_SPEED_LIMIT,
            FIELD_LINK_SHAPE,
            FIELD_LINK_LABEL,
            FIELD_LINK_COUNTRY,
            FIELD_LINK_STATE,
            FIELD_LINK_COUNTY,
            FIELD_LINK_CITY,
            FIELD_LINK_STREET,
            FIELD_LINK_TRAFFIC_SPEED,
            FIELD_LINK_TRAFFIC_TIME,
            FIELD_LINK_BASE_SPEED,
            FIELD_LINK_BASE_TIME,
            FIELD_LINK_BBOX_MIN_LAT,
            FIELD_LINK_BBOX_MIN_LNG,
            FIELD_LINK_BBOX_MAX_LAT,
            FIELD_LINK_BBOX_MAX_LNG };

    static final String[] QUADKEY_SELECT_COLUMNS= new String[] { "_id",
            FIELD_QUAD_QUADKEY,
            FIELD_QUAD_CREATE_DA };
    //
    static class InternalDbHelper extends SQLiteOpenHelper
    {
        public InternalDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
        {
            super(context, name, factory, version);
        }
        public void onCreate(SQLiteDatabase db)
        {
            // RouteLink table
            StringBuilder sql= new StringBuilder();
            sql.append("create table ");
            sql.append(ROUTE_LINK_TABLE);
            sql.append(" (");
            sql.append("_id integer primary key autoincrement, ");
            sql.append(FIELD_LINK_ID);
            sql.append(" text, ");
            sql.append(FIELD_LINK_QUADKEY);
            sql.append(" text, ");
            sql.append(FIELD_LINK_FUNCTIONAL_CLASS);
            sql.append(" integer, ");
            sql.append(FIELD_LINK_LENGTH);
            sql.append(" real, ");
            sql.append(FIELD_LINK_SPEED_LIMIT);
            sql.append(" real, ");
            sql.append(FIELD_LINK_SHAPE);
            sql.append(" text, ");
            sql.append(FIELD_LINK_LABEL);
            sql.append(" text, ");
            sql.append(FIELD_LINK_COUNTRY);
            sql.append(" text, ");
            sql.append(FIELD_LINK_STATE);
            sql.append(" text, ");
            sql.append(FIELD_LINK_COUNTY);
            sql.append(" text, ");
            sql.append(FIELD_LINK_CITY);
            sql.append(" text, ");
            sql.append(FIELD_LINK_STREET);
            sql.append(" text, ");
            sql.append(FIELD_LINK_TRAFFIC_SPEED);
            sql.append(" real, ");
            sql.append(FIELD_LINK_TRAFFIC_TIME);
            sql.append(" real, ");
            sql.append(FIELD_LINK_BASE_SPEED);
            sql.append(" real, ");
            sql.append(FIELD_LINK_BASE_TIME);
            sql.append(" real, ");
            sql.append(FIELD_LINK_BBOX_MIN_LAT);
            sql.append(" real, ");
            sql.append(FIELD_LINK_BBOX_MIN_LNG);
            sql.append(" real, ");
            sql.append(FIELD_LINK_BBOX_MAX_LAT);
            sql.append(" real, ");
            sql.append(FIELD_LINK_BBOX_MAX_LNG);
            sql.append(" real);");
            db.execSQL(sql.toString());

            // RouteLinkQuadkey table
            sql= new StringBuilder();
            sql.append("create table ");
            sql.append(ROUTE_LINK_QUADKEY_TABLE);
            sql.append(" (");
            sql.append("_id integer primary key autoincrement, ");
            sql.append(FIELD_QUAD_QUADKEY);
            sql.append(" text, ");
            sql.append(FIELD_QUAD_CREATE_DA);
            sql.append(" integer);");
            sql.append("create index idx1 on ");
            sql.append(ROUTE_LINK_TABLE);
            sql.append("(");
            sql.append(FIELD_LINK_BBOX_MIN_LAT);
            sql.append(",");
            sql.append(FIELD_LINK_BBOX_MIN_LNG);
            sql.append(",");
            sql.append(FIELD_LINK_BBOX_MAX_LAT);
            sql.append(",");
            sql.append(FIELD_LINK_BBOX_MAX_LNG);
            sql.append(");");
            db.execSQL(sql.toString());
        }
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            db.execSQL("drop table if exists " + ROUTE_LINK_TABLE);
            db.execSQL("drop table if exists " + ROUTE_LINK_QUADKEY_TABLE);
            onCreate(db);
        }
    }
    InternalDbHelper dbHelper;
    SQLiteDatabase db;
    //Context context;
    //
    DatabaseUtils.InsertHelper routeLinkInsertHelper;
    int linkIdColIndex;
    int linkQuadkeyColIndex;
    int linkFunctionalClassColIndex;
    int linkLengthColIndex;
    int linkSpeedLimitColIndex;
    int linkShapeColIndex;
    int linkLabelColIndex;
    int linkCountryColIndex;
    int linkStateColIndex;
    int linkCountyColIndex;
    int linkCityColIndex;
    int linkStreetColIndex;
    int linkTrafficSpeedColIndex;
    int linkTrafficTimeColIndex;
    int linkBaseSpeedColIndex;
    int linkBaseTimeColIndex;
    int linkBboxMinLatColIndex;
    int linkBboxMinLngColIndex;
    int linkBboxMaxLatColIndex;
    int linkBboxMaxLngColIndex;

    //
    @Inject
    Application application;


    public RouteLinkDatabaseAdapter()
    {

    }

    @Inject
    public void init() {

        dbHelper = new InternalDbHelper(application, DATABASE_NAME, null, DATABASE_VERSION);
        db = dbHelper.getWritableDatabase();
        db.setLockingEnabled(true);
        db.execSQL("PRAGMA read_uncommitted = true;");
        //
        routeLinkInsertHelper = new DatabaseUtils.InsertHelper(db, ROUTE_LINK_TABLE);
        linkIdColIndex = routeLinkInsertHelper.getColumnIndex(FIELD_LINK_ID);
        linkQuadkeyColIndex = routeLinkInsertHelper.getColumnIndex(FIELD_LINK_QUADKEY);
        linkFunctionalClassColIndex =  routeLinkInsertHelper.getColumnIndex(FIELD_LINK_FUNCTIONAL_CLASS);
        linkLengthColIndex =  routeLinkInsertHelper.getColumnIndex(FIELD_LINK_LENGTH);
        linkSpeedLimitColIndex= routeLinkInsertHelper.getColumnIndex(FIELD_LINK_SPEED_LIMIT);
        linkShapeColIndex =  routeLinkInsertHelper.getColumnIndex(FIELD_LINK_SHAPE);
        linkLabelColIndex =  routeLinkInsertHelper.getColumnIndex(FIELD_LINK_LABEL);
        linkCountryColIndex =  routeLinkInsertHelper.getColumnIndex(FIELD_LINK_COUNTRY);
        linkStateColIndex =  routeLinkInsertHelper.getColumnIndex(FIELD_LINK_STATE);
        linkCountyColIndex =  routeLinkInsertHelper.getColumnIndex(FIELD_LINK_COUNTY);
        linkCityColIndex =  routeLinkInsertHelper.getColumnIndex(FIELD_LINK_CITY);
        linkStreetColIndex= routeLinkInsertHelper.getColumnIndex(FIELD_LINK_STREET);
        linkTrafficSpeedColIndex =  routeLinkInsertHelper.getColumnIndex(FIELD_LINK_TRAFFIC_SPEED);
        linkTrafficTimeColIndex =  routeLinkInsertHelper.getColumnIndex(FIELD_LINK_TRAFFIC_TIME);
        linkBaseSpeedColIndex =  routeLinkInsertHelper.getColumnIndex(FIELD_LINK_BASE_SPEED);
        linkBaseTimeColIndex =  routeLinkInsertHelper.getColumnIndex(FIELD_LINK_BASE_TIME);
        linkBboxMinLatColIndex= routeLinkInsertHelper.getColumnIndex(FIELD_LINK_BBOX_MIN_LAT);
        linkBboxMinLngColIndex= routeLinkInsertHelper.getColumnIndex(FIELD_LINK_BBOX_MIN_LNG);
        linkBboxMaxLatColIndex= routeLinkInsertHelper.getColumnIndex(FIELD_LINK_BBOX_MAX_LAT);
        linkBboxMaxLngColIndex= routeLinkInsertHelper.getColumnIndex(FIELD_LINK_BBOX_MAX_LNG);
    }

    public void addQuadkey(RouteLinkQuadkey routeLinkQuadkey)
    {
        ContentValues values= new ContentValues(2);
        values.put(FIELD_QUAD_QUADKEY, routeLinkQuadkey.getQuadkey());
        values.put(FIELD_QUAD_CREATE_DA, routeLinkQuadkey.getCreateDa().getTime());
        db.insert(ROUTE_LINK_QUADKEY_TABLE, null, values);
    }
    public void bulkAddRouteLinks(String quadkey, RouteLinks incomingRouteLinks)
    {
        db.beginTransaction();
        try
        {
            LinkedList<RouteLink> routeLinks = incomingRouteLinks.getRouteLinks();

            int n= routeLinks.size();

            for (int i= 0; i < n; i++)
            {
                RouteLink routeLink= routeLinks.get(i);
                routeLinkInsertHelper.prepareForInsert();
                routeLinkInsertHelper.bind(linkIdColIndex, routeLink.getLinkId());
                routeLinkInsertHelper.bind(linkQuadkeyColIndex, quadkey);
                routeLinkInsertHelper.bind(linkFunctionalClassColIndex, routeLink.getFunctionalClass());
                routeLinkInsertHelper.bind(linkLengthColIndex, routeLink.getLength());
                routeLinkInsertHelper.bind(linkSpeedLimitColIndex, routeLink.getSpeedLimit());
                routeLinkInsertHelper.bind(linkShapeColIndex, routeLink.getLineString());
                routeLinkInsertHelper.bind(linkLabelColIndex, routeLink.getAddressInfo().getLabel());
                routeLinkInsertHelper.bind(linkCountryColIndex, routeLink.getAddressInfo().getCountry());
                routeLinkInsertHelper.bind(linkStateColIndex, routeLink.getAddressInfo().getState());
                routeLinkInsertHelper.bind(linkCountyColIndex, routeLink.getAddressInfo().getCounty());
                routeLinkInsertHelper.bind(linkCityColIndex, routeLink.getAddressInfo().getCity());
                routeLinkInsertHelper.bind(linkStreetColIndex, routeLink.getAddressInfo().getStreet());
                routeLinkInsertHelper.bind(linkTrafficSpeedColIndex, routeLink.getDynamicSpeedInfo().getTrafficSpeed());
                routeLinkInsertHelper.bind(linkTrafficTimeColIndex, routeLink.getDynamicSpeedInfo().getTrafficTime());
                routeLinkInsertHelper.bind(linkBaseSpeedColIndex, routeLink.getDynamicSpeedInfo().getBaseSpeed());
                routeLinkInsertHelper.bind(linkBaseTimeColIndex, routeLink.getDynamicSpeedInfo().getBaseTime());
                routeLinkInsertHelper.bind(linkBboxMinLatColIndex, routeLink.getBboxMinLat());
                routeLinkInsertHelper.bind(linkBboxMinLngColIndex, routeLink.getBboxMinLng());
                routeLinkInsertHelper.bind(linkBboxMaxLatColIndex, routeLink.getBboxMaxLat());
                routeLinkInsertHelper.bind(linkBboxMaxLngColIndex, routeLink.getBboxMaxLng());
                routeLinkInsertHelper.execute();
            }
            db.setTransactionSuccessful();
        } finally
        {

            db.endTransaction();

        }
    }
    public void destroy()
    {
        routeLinkInsertHelper.close();
        routeLinkInsertHelper= null;
        db.close();
        db= null;
        dbHelper.close();
        dbHelper= null;
    }
    ArrayList<RouteLinkQuadkey> extractQuadkeyRows(Cursor cursor)
    {
        HashMap<String, Integer> columns= new HashMap<String, Integer>(2);
        columns.put(FIELD_QUAD_QUADKEY, cursor.getColumnIndex(FIELD_QUAD_QUADKEY));
        columns.put(FIELD_QUAD_CREATE_DA, cursor.getColumnIndex(FIELD_QUAD_CREATE_DA));
        //
        ArrayList<RouteLinkQuadkey> routeLinkQuadkeys= new ArrayList<RouteLinkQuadkey>(cursor.getCount());
        while (cursor.moveToNext())
        {
            RouteLinkQuadkey routeLinkQuadkey= new RouteLinkQuadkey();
            routeLinkQuadkey.setQuadkey(cursor.getString(columns.get(FIELD_QUAD_QUADKEY)));
            routeLinkQuadkey.setCreateDa(new Timestamp(cursor.getLong(columns.get(FIELD_QUAD_CREATE_DA))));
            routeLinkQuadkeys.add(routeLinkQuadkey);
        }

        return routeLinkQuadkeys;
    }
    ArrayList<RouteLink> extractRouteLinkRows(Cursor cursor)
    {
        HashMap<String, Integer> columns= new HashMap<String, Integer>(20);
        columns.put(FIELD_LINK_ID, cursor.getColumnIndex(FIELD_LINK_ID));
        columns.put(FIELD_LINK_QUADKEY, cursor.getColumnIndex(FIELD_LINK_QUADKEY));
        columns.put(FIELD_LINK_FUNCTIONAL_CLASS, cursor.getColumnIndex(FIELD_LINK_FUNCTIONAL_CLASS));
        columns.put(FIELD_LINK_LENGTH, cursor.getColumnIndex(FIELD_LINK_LENGTH));
        columns.put(FIELD_LINK_SPEED_LIMIT, cursor.getColumnIndex(FIELD_LINK_SPEED_LIMIT));
        columns.put(FIELD_LINK_SHAPE, cursor.getColumnIndex(FIELD_LINK_SHAPE));
        columns.put(FIELD_LINK_LABEL, cursor.getColumnIndex(FIELD_LINK_LABEL));
        columns.put(FIELD_LINK_COUNTRY, cursor.getColumnIndex(FIELD_LINK_COUNTRY));
        columns.put(FIELD_LINK_STATE, cursor.getColumnIndex(FIELD_LINK_STATE));
        columns.put(FIELD_LINK_COUNTY, cursor.getColumnIndex(FIELD_LINK_COUNTY));
        columns.put(FIELD_LINK_CITY, cursor.getColumnIndex(FIELD_LINK_CITY));
        columns.put(FIELD_LINK_STREET, cursor.getColumnIndex(FIELD_LINK_STREET));
        columns.put(FIELD_LINK_TRAFFIC_SPEED, cursor.getColumnIndex(FIELD_LINK_TRAFFIC_SPEED));
        columns.put(FIELD_LINK_TRAFFIC_TIME, cursor.getColumnIndex(FIELD_LINK_TRAFFIC_TIME));
        columns.put(FIELD_LINK_BASE_SPEED, cursor.getColumnIndex(FIELD_LINK_BASE_SPEED));
        columns.put(FIELD_LINK_BASE_TIME, cursor.getColumnIndex(FIELD_LINK_BASE_TIME));
        columns.put(FIELD_LINK_BBOX_MIN_LAT, cursor.getColumnIndex(FIELD_LINK_BBOX_MIN_LAT));
        columns.put(FIELD_LINK_BBOX_MIN_LNG, cursor.getColumnIndex(FIELD_LINK_BBOX_MIN_LNG));
        columns.put(FIELD_LINK_BBOX_MAX_LAT, cursor.getColumnIndex(FIELD_LINK_BBOX_MAX_LAT));
        columns.put(FIELD_LINK_BBOX_MAX_LNG, cursor.getColumnIndex(FIELD_LINK_BBOX_MAX_LNG));
        //
        ArrayList<RouteLink> routeLinks= new ArrayList<RouteLink>(cursor.getCount());
        while (cursor.moveToNext())
        {
            RouteLink routeLink= new RouteLink();
            routeLink.setLinkId(cursor.getString(columns.get(FIELD_LINK_ID)));
            routeLink.setQuadkey(cursor.getString(columns.get(FIELD_LINK_QUADKEY)));
            routeLink.setFunctionalClass(cursor.getInt(columns.get(FIELD_LINK_FUNCTIONAL_CLASS)));
            routeLink.setLength(cursor.getDouble(columns.get(FIELD_LINK_LENGTH)));
            routeLink.setSpeedLimit(cursor.getDouble(columns.get(FIELD_LINK_SPEED_LIMIT)));
            routeLink.setLineString(cursor.getString(columns.get(FIELD_LINK_SHAPE)));
            routeLink.getAddressInfo().setLabel(cursor.getString(columns.get(FIELD_LINK_LABEL)));
            routeLink.getAddressInfo().setCountry(cursor.getString(columns.get(FIELD_LINK_COUNTY)));
            routeLink.getAddressInfo().setState(cursor.getString(columns.get(FIELD_LINK_STATE)));
            routeLink.getAddressInfo().setCounty(cursor.getString(columns.get(FIELD_LINK_COUNTY)));
            routeLink.getAddressInfo().setCity(cursor.getString(columns.get(FIELD_LINK_CITY)));
            routeLink.getAddressInfo().setStreet(cursor.getString(columns.get(FIELD_LINK_STREET)));
            routeLink.getDynamicSpeedInfo().setTrafficSpeed(cursor.getDouble(columns.get(FIELD_LINK_TRAFFIC_SPEED)));
            routeLink.getDynamicSpeedInfo().setTrafficTime(cursor.getDouble(columns.get(FIELD_LINK_TRAFFIC_TIME)));
            routeLink.getDynamicSpeedInfo().setBaseSpeed(cursor.getDouble(columns.get(FIELD_LINK_BASE_SPEED)));
            routeLink.getDynamicSpeedInfo().setBaseTime(cursor.getDouble(columns.get(FIELD_LINK_BASE_TIME)));
            routeLink.setBboxMinLat(cursor.getDouble(columns.get(FIELD_LINK_BBOX_MIN_LAT)));
            routeLink.setBboxMinLng(cursor.getDouble(columns.get(FIELD_LINK_BBOX_MIN_LNG)));
            routeLink.setBboxMaxLat(cursor.getDouble(columns.get(FIELD_LINK_BBOX_MAX_LAT)));
            routeLink.setBboxMaxLng(cursor.getDouble(columns.get(FIELD_LINK_BBOX_MAX_LNG)));
            routeLinks.add(routeLink);
        }
        return routeLinks;
    }

    public RouteLinkQuadkey getQuadkey(String quadkey)
    {
        Cursor cursor= null;
        try
        {
            StringBuilder where= new StringBuilder();
            where.append(FIELD_QUAD_QUADKEY);
            where.append(" = ?");
            //
            cursor= db.query(ROUTE_LINK_QUADKEY_TABLE, QUADKEY_SELECT_COLUMNS, where.toString(),
                    new String[] { String.valueOf(quadkey) }, null, null, null);
            //
            if (cursor.getCount() > 0)
                return extractQuadkeyRows(cursor).get(0);
            return null;
        } finally
        {
            if (cursor != null)
                cursor.close();
        }
    }
    public ArrayList<RouteLinkQuadkey> getQuadkeysBefore(long createDa)
    {
        Cursor cursor= null;
        try
        {
            StringBuilder where= new StringBuilder();
            where.append(FIELD_QUAD_CREATE_DA);
            where.append(" < ?");
            //
            cursor= db.query(ROUTE_LINK_QUADKEY_TABLE, QUADKEY_SELECT_COLUMNS, where.toString(),
                    new String[] { String.valueOf(new Timestamp(createDa)) }, null, null, null);
            //
            return extractQuadkeyRows(cursor);
        } finally
        {
            if (cursor != null)
                cursor.close();
        }
    }
    public RouteLink getRouteLinkByLinkId(String linkId)
    {
        Cursor cursor= null;
        try
        {
            StringBuilder where= new StringBuilder();
            where.append(FIELD_LINK_ID);
            where.append(" = ?");
            //
            cursor= db.query(ROUTE_LINK_TABLE, ROUTE_LINK_SELECT_COLUMNS, where.toString(), new String[] { linkId },
                    null, null, null);
            //
            ArrayList<RouteLink> rows= extractRouteLinkRows(cursor);
            return rows.size() > 0 ? rows.get(0) : null;
        } finally
        {
            if (cursor != null)
                cursor.close();
        }
    }
    public ArrayList<RouteLink> getRouteLinksByBoundingBox(Envelope bbox)
    {
        Cursor cursor= null;
        try
        {
            StringBuilder where= new StringBuilder();
            where.append(FIELD_LINK_BBOX_MAX_LNG);
            where.append(" >= ? and ");
            where.append(FIELD_LINK_BBOX_MIN_LNG);
            where.append(" <= ? and ");
            where.append(FIELD_LINK_BBOX_MAX_LAT);
            where.append(" >= ? and ");
            where.append(FIELD_LINK_BBOX_MIN_LAT);
            where.append(" <= ?");
            //
            cursor= db.query(
                    ROUTE_LINK_TABLE,
                    ROUTE_LINK_SELECT_COLUMNS,
                    where.toString(),
                    new String[] { String.valueOf(bbox.getMinX()), String.valueOf(bbox.getMaxX()),
                            String.valueOf(bbox.getMinY()), String.valueOf(bbox.getMaxY()) }, null, null, null);
            //
            return extractRouteLinkRows(cursor);
        } finally
        {
            if (cursor != null)
                cursor.close();
        }
    }
    public ArrayList<RouteLink> getRouteLinksByQuadkey(String quadkey)
    {
        Cursor cursor= null;
        try
        {
            StringBuilder where= new StringBuilder();
            where.append(FIELD_LINK_QUADKEY);
            where.append(" = ?");
            //
            cursor= db.query(ROUTE_LINK_TABLE, ROUTE_LINK_SELECT_COLUMNS, where.toString(), new String[] { quadkey },
                    null, null, null);
            //
            return extractRouteLinkRows(cursor);
        } finally
        {
            if (cursor != null)
                cursor.close();
        }
    }
    public boolean hasQuadkey(String quadkey)
    {
        Cursor cursor= null;
        try
        {
            StringBuilder sql= new StringBuilder();
            sql.append("select count(1) from ");
            sql.append(ROUTE_LINK_QUADKEY_TABLE);
            sql.append(" where ");
            sql.append(FIELD_QUAD_QUADKEY);
            sql.append(" = ?");
            cursor= db.rawQuery(sql.toString(), new String[] { quadkey });
            if (cursor.moveToNext())
                return cursor.getInt(0) > 0;
            return false;
        } finally
        {
            if (cursor != null)
                cursor.close();
        }
    }
    public void removeQuadkey(RouteLinkQuadkey routeLinkQuadkey)
    {
        String[] whereArgs= new String[] { routeLinkQuadkey.getQuadkey() };
        int linksDeleted = db.delete(ROUTE_LINK_TABLE, FIELD_LINK_QUADKEY + " = ?", whereArgs);
        int quadsDeleted = db.delete(ROUTE_LINK_QUADKEY_TABLE, FIELD_QUAD_QUADKEY + " = ?", whereArgs);
    }
    public void removeRouteLinksByQuadkey(String quadkey)
    {
        String[] whereArgs= new String[] { quadkey };
        db.delete(ROUTE_LINK_TABLE, FIELD_LINK_QUADKEY + " = ?", whereArgs);
    }
}