package com.trapster.android.model.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import com.trapster.android.model.Trap;

public class TrapProvider extends ContentProvider {

	private static final String LOGNAME = "Trapster.TrapProvider";

	public static final String PROVIDER_NAME = "com.trapster.android.trap";

	public static final Uri CONTENT_URI = Uri.parse("content://"+ PROVIDER_NAME + "/traps");

	private static final int TRAPS = 1;
	private static final int TRAP_ID = 2;

	private static final UriMatcher uriMatcher;
	static {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(PROVIDER_NAME, "traps", TRAPS);
		uriMatcher.addURI(PROVIDER_NAME, "traps/#", TRAP_ID);
	}

	private static final String DATABASE_NAME = "Trap.db";
	public static final String DATABASE_TABLE = "Traps";

	private static final int DATABASE_VERSION = 5;

	// The index (key) column name for use in where clauses.
	public static final String KEY_ID = "_id";
	public static final String FIELD_ID = "id";
	public static final String FIELD_TYPE = "type";
	public static final String FIELD_LATITUDE = "latitude";
	public static final String FIELD_LONGITUDE = "longitude";
	public static final String FIELD_NUM = "num";
	public static final String FIELD_OUNAME = "ouname";
	public static final String FIELD_LUNAME = "luname";
	public static final String FIELD_LVOTE = "lvote";
	public static final String FIELD_LVOTE_SEC = "lvote_sec";
	public static final String FIELD_ADDRESS = "address";
	public static final String FIELD_LEVEL = "level";
	public static final String FIELD_BADGE = "badge";


	public static final String FIELD_TIMESTAMP = "timestamp";
	public static final String FIELD_EXPIRY = "expiry";

    /**
     * 4.3 added from rest services and to provide a link for the map tile
     */
    public static final String FIELD_LIFETIME = "lifetime";
    public static final String FIELD_TILEINDEX = "tileindex";



    // SQL Statement to create a new database.
	private static final String DATABASE_CREATE = "create table "
			+ DATABASE_TABLE + " (" + KEY_ID
			+ " INTEGER primary key autoincrement, " + FIELD_ID
			+ " INTEGER(10), " + FIELD_TYPE + " INTEGER, " + FIELD_NUM
			+ " VARCHAR(100), " + FIELD_LATITUDE + " FLOAT, " + FIELD_LONGITUDE
			+ " FLOAT," + FIELD_OUNAME + " VARCHAR(100)," + FIELD_LUNAME
			+ " VARCHAR(100)," + FIELD_LVOTE + " VARCHAR(100),"
			+ FIELD_LVOTE_SEC + " INTEGER," + FIELD_ADDRESS + " VARCHAR(255),"
			+ FIELD_LEVEL + " INTEGER," + FIELD_BADGE + " VARCHAR(255),"
            + FIELD_LIFETIME + " INTEGER," + FIELD_TILEINDEX + " VARCHAR(255),"
			+ FIELD_TIMESTAMP + " INTEGER," + FIELD_EXPIRY + " INTEGER" + ");";
	// Variable to hold the database instance

	private SQLiteDatabase db;
	private DbHelper dbHelper;

	@Override
	public boolean onCreate() {

		this.dbHelper = new DbHelper(getContext(), DATABASE_NAME, null,
				DATABASE_VERSION);

		try {
			db = dbHelper.getWritableDatabase();
		} catch (SQLiteException e) {
			db = null;
{/* 			//Log.d(LOGNAME, "Database Opening exception"); */}
		}

		return (db == null) ? false : true;
	}

	@Override
	public String getType(Uri uri) {
		switch (uriMatcher.match(uri)) {
		case TRAPS:
			return "vnd.com.trapster.android.cursor.dir/traps";
		case TRAP_ID:
			return "vnd.com.trapster.android.cursor.item/traps";
		default:
			throw new IllegalArgumentException("Unsupported URI:"
					+ uri.toString());
		}
	}

	/*
	 * @TODO implement TRAP_ID URI to support deleting of traps by id
	 * 
	 * (non-Javadoc)
	 * 
	 * @see android.content.ContentProvider#delete(android.net.Uri,
	 * java.lang.String, java.lang.String[])
	 */
	@Override
	public int delete(Uri uri, String where, String[] whereArgs) {
		int count = 0;
		switch (uriMatcher.match(uri)) {
		case TRAPS:
			count = db.delete(DATABASE_TABLE, where, whereArgs);
			break;

		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
        if (count > 0)
			getContext().getContentResolver().notifyChange(uri, null);
		
		
		return count;
	}

	@Override
	public Uri insert(Uri _uri, ContentValues v) {
		
		String where = FIELD_ID + "=?";
        String[] whereArgs = new String[]{String.valueOf(v.getAsInteger(FIELD_ID))};

		if (db.update(DATABASE_TABLE, v, where, whereArgs) == 0)
			db.insert(DATABASE_TABLE, "unknown", v);

       getContext().getContentResolver().notifyChange(_uri, null);
		
		return _uri;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String where,
			String[] whereArgs, String sortOrder) {

		long startTime = System.currentTimeMillis();
		
		SQLiteQueryBuilder sqlBuilder = new SQLiteQueryBuilder();
		sqlBuilder.setTables(DATABASE_TABLE);

		if (uriMatcher.match(uri) == TRAP_ID)
			sqlBuilder.appendWhere(FIELD_ID + " = "
					+ uri.getPathSegments().get(1));

		if (sortOrder == null || sortOrder == "")
			sortOrder = FIELD_TYPE;

		/*Log.d(LOGNAME, "URI:" + uri.toString() + ":" + uriMatcher.match(uri));
 		Log.d(LOGNAME,
				"Running:"
						+ sqlBuilder.buildQueryString(false,
								sqlBuilder.getTables(), projection, where,
								null, null, sortOrder, null));*/
		

		Cursor c = sqlBuilder.query(db, projection, where, whereArgs, null,
				null, sortOrder);

		c.setNotificationUri(getContext().getContentResolver(), uri);
		
		{/* Log.i(LOGNAME,
				"Transaction(Where "+where+") finished in  "
						+ (System.currentTimeMillis() - startTime) + " ms"); */}
		
		
		return c;
	}

	@Override
	public int update(Uri uri, ContentValues v, String where, String[] whereArgs) {
		boolean announce = true;

		int count;
		switch (uriMatcher.match(uri)) {
		case TRAPS:
			count = db.update(DATABASE_TABLE, v, where, whereArgs);
			break;
		case TRAP_ID:
			String segment = uri.getPathSegments().get(1);
			count = db.update(DATABASE_TABLE, v,
					KEY_ID
							+ "="
							+ segment
							+ (!TextUtils.isEmpty(where) ? " AND (" + where
									+ ')' : ""), whereArgs);
			break;

		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}

        if (announce)
			getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

	@Override
	public int bulkInsert(Uri uri, ContentValues[] values) {

		long startTime = System.currentTimeMillis();
		/*Log.i(LOGNAME, "Starting Bulk Transaction of " + values.length
				+ " records");*/

		db.beginTransaction();
		int count = 0;

		try {
			for (int i = 0; i < values.length; i++) {

				String where = FIELD_ID + "=" + values[i].getAsInteger(FIELD_ID);
                int trapId = values[i].getAsInteger(FIELD_ID);

				int updateCount = db.update(DATABASE_TABLE, values[i], where, null);

                if(updateCount == 0){
                //    Log.v(LOGNAME,"Inserting new trap:"+trapId);
                    db.insert(DATABASE_TABLE, "unknown", values[i]);
                }
				
				count++;
			}
			db.setTransactionSuccessful();

			getContext().getContentResolver().notifyChange(uri, null);

			return count;
		} finally {
			db.endTransaction();
 			/*Log.i(LOGNAME,
					"Transaction finished in  "
							+ (System.currentTimeMillis() - startTime) + " ms");*/
		}
	}

	
	
	public static ContentValues toContentValues(Trap trap){
		
		ContentValues v = new ContentValues();
		v.put(FIELD_ID, trap.getId());
		v.put(FIELD_TYPE, trap.getTypeid());
		v.put(FIELD_LATITUDE, trap.getLatitude());
		v.put(FIELD_LONGITUDE, trap.getLongitude());
		v.put(FIELD_NUM, trap.getNum());
		v.put(FIELD_OUNAME, trap.getOuname());
		v.put(FIELD_LUNAME, trap.getLuname());
		v.put(FIELD_LVOTE, trap.getLvote());
		v.put(FIELD_LVOTE_SEC, trap.getLvoteSec());

		v.put(FIELD_ADDRESS, trap.getAddress());
		v.put(FIELD_LEVEL, trap.getLevel());
		v.put(FIELD_BADGE, trap.getBadgeKey());
		v.put(FIELD_TIMESTAMP, System.currentTimeMillis());
		v.put(FIELD_EXPIRY, trap.getExpiry());

        v.put(FIELD_LIFETIME,trap.getLifetime());
        v.put(FIELD_TILEINDEX,trap.getTileIndex());
		
		return v; 
		
	}
	
	
	public static Trap fromCursor(Cursor cursor) {

		Trap trap = null;

		trap = new Trap();

		trap.setAddress(cursor.getString(cursor.getColumnIndex(FIELD_ADDRESS)));
		trap.setBadgeKey(cursor.getString(cursor.getColumnIndex(FIELD_BADGE)));
		trap.setId(cursor.getInt(cursor.getColumnIndex(FIELD_ID)));
		
		trap.setLevel(cursor.getInt(cursor.getColumnIndex(FIELD_LEVEL)));
		trap.setExpiry(cursor.getLong(cursor.getColumnIndex(FIELD_EXPIRY)));

        trap.setTileIndex(cursor.getString(cursor.getColumnIndex(FIELD_TILEINDEX)));
        trap.setLifetime(cursor.getInt(cursor.getColumnIndex(FIELD_LIFETIME)));


        trap.setLuname(cursor.getString(cursor.getColumnIndex(FIELD_LUNAME)));
		trap.setLvote(cursor.getString(cursor.getColumnIndex(FIELD_LVOTE)));
		trap.setLvoteSec(cursor.getInt(cursor.getColumnIndex(FIELD_LVOTE_SEC)));
		trap.setNum(cursor.getInt(cursor.getColumnIndex(FIELD_NUM)));
		trap.setOuname(cursor.getString(cursor.getColumnIndex(FIELD_OUNAME)));
		trap.setTypeid(cursor.getInt(cursor.getColumnIndex(FIELD_TYPE)));

		double lat = cursor.getDouble(cursor.getColumnIndex(FIELD_LATITUDE));
		
		double lng = cursor.getDouble(cursor.getColumnIndex(FIELD_LONGITUDE));
		
		trap.setGeometry(lat, lng);
		
		return trap;
	}
	
	class DbHelper extends SQLiteOpenHelper {

		public DbHelper(Context context, String name, CursorFactory factory,
				int version) {
			super(context, name, factory, version);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(DATABASE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
				db.execSQL("Drop TABLE IF EXISTS " + DATABASE_TABLE);
			onCreate(db);
		}

	}

}
