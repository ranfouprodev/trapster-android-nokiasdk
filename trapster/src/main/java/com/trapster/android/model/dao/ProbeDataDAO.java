package com.trapster.android.model.dao;

import java.util.ArrayList;
import java.util.List;

import roboguice.inject.ContextSingleton;
import android.content.Context;
import android.database.Cursor;

import com.google.inject.Inject;
import com.trapster.android.model.ProbeData;
import com.trapster.android.model.provider.ProbeDataProvider;

@ContextSingleton
public class ProbeDataDAO {

	@Inject
	Context context;
	
	public ProbeDataDAO() {
	}
	
	
	
	public void save(ProbeData r) {

		context.getContentResolver().insert(ProbeDataProvider.CONTENT_URI, ProbeDataProvider.toContentValues(r));

	}



	public void delete(int id) {
		// TODO Auto-generated method stub
		String where = ProbeDataProvider.KEY_ID +"="+id;
		
		context.getContentResolver().delete(ProbeDataProvider.CONTENT_URI,where,null);
	}



	public List<ProbeData> findAll() {
		Cursor cursor = null;

		ArrayList<ProbeData> points = new ArrayList<ProbeData>();

		try {

			String where = null;

			String[] whereArgs = null;

	
			cursor = context.getContentResolver().query(
					ProbeDataProvider.CONTENT_URI, null, where, whereArgs, null);
			if (cursor != null && cursor.moveToFirst()) {

				do {
					ProbeData probe = ProbeDataProvider.fromCursor(cursor);

					points.add(probe);
				} while (cursor.moveToNext());
			}

		} finally {
			if (cursor != null)
				cursor.close();
		}

		return points;
	}

}
