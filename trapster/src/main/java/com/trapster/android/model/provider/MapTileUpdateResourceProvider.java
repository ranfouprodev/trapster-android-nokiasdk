package com.trapster.android.model.provider;

import android.content.*;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import com.trapster.android.model.MapTileUpdateResource;

public class MapTileUpdateResourceProvider extends ContentProvider {

	private static final String LOGNAME = "MapTileUpdateResourceProvider";

	private static final String DATABASE_NAME = "Maptile.db";
	private static final String DATABASE_TABLE = "Maptile";

	private static final int DATABASE_VERSION = 5;

	// The index (key) column name for use in where clauses.
	public static final String KEY_ID = "_id";
	public static final String FIELD_LAYER = "layer";
	public static final String FIELD_LAST_UPDATE = "lastupdate";
	public static final String FIELD_ZOOM = "zoom";
	public static final String FIELD_TILEX = "tilex";
	public static final String FIELD_TILEY = "tiley";
	public static final String FIELD_REFERENCE = "reference";
	private static final String FIELD_TIMESTAMP = "timestamp";

	// SQL Statement to create a new database.
	private static final String DATABASE_CREATE = "create table if not exists "
			+ DATABASE_TABLE + " (" + KEY_ID
			+ " INTEGER primary key autoincrement, " + FIELD_LAST_UPDATE
			+ " INTEGER," + FIELD_ZOOM + " INTEGER," + FIELD_TILEX
			+ " INTEGER," + FIELD_TILEY + " INTEGER," + FIELD_LAYER + " TEXT, "
			+ FIELD_REFERENCE + " TEXT, " + FIELD_TIMESTAMP + " LONG); ";

	public static final String PROVIDER_NAME = "com.trapster.android.maptile";

	public static final Uri CONTENT_URI = Uri.parse("content://"
			+ PROVIDER_NAME + "/maptile");

	private static final int MAPTILE = 1;

	private static final UriMatcher uriMatcher;
	static {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(PROVIDER_NAME, "maptile", MAPTILE);
	}

	// Variable to hold the database instance
	private SQLiteDatabase db;
	private DbHelper dbHelper;

	@Override
	public boolean onCreate() {

		this.dbHelper = new DbHelper(getContext(), DATABASE_NAME, null,
				DATABASE_VERSION);
		try {
			db = dbHelper.getWritableDatabase();
		} catch (SQLiteException e) {
			db = null;
{/* 			//Log.d(LOGNAME, "Database Opening exception"); */}
		}

		return (db == null) ? false : true;
	}

	@Override
	public String getType(Uri uri) {
		switch (uriMatcher.match(uri)) {
		case MAPTILE:
			return "vnd.android.cursor.dir/vnd.trapster.maptile";
		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
	}

	@Override
	public int delete(Uri uri, String where, String[] whereArgs) {
		int count = 0;
		switch (uriMatcher.match(uri)) {
		case MAPTILE:
			count = db.delete(DATABASE_TABLE, where, whereArgs);
			break;

		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}

		if (count > 0)
			getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		switch (uriMatcher.match(uri)) {
		case MAPTILE:
			long rowId = -1;

			String where = FIELD_REFERENCE + "='" + values.getAsString(FIELD_REFERENCE) + "' and "+ FIELD_LAYER +"='" + values.getAsString(FIELD_LAYER)+"';";

			if(values.containsKey(KEY_ID))
				rowId = values.getAsInteger(KEY_ID);

			if (db.update(DATABASE_TABLE, values, where, null) == 0)
				rowId = db.insert(DATABASE_TABLE, "unknown", values);
				
			Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowId);
			getContext().getContentResolver().notifyChange(uri, null);
			return _uri;
			

		default:
			throw new SQLException("Failed to insert row, invalid URI " + uri);
		}
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String where,
			String[] whereArgs, String sort) {

		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		qb.setTables(DATABASE_TABLE);

		String orderBy = null;

		switch (uriMatcher.match(uri)) {
		case MAPTILE:
			// add sort?
			break;
		default:
			throw new SQLException("Failed to insert row into " + uri);
		}
{/* 
		//Log.d(LOGNAME, "URI:" + uri.toString() + ":" + uriMatcher.match(uri)); */}
{/* 		//Log.d(LOGNAME,
				"Running:"
						+ qb.buildQueryString(false, qb.getTables(),
								projection, where, null, null, orderBy, null)); */}

		// Apply the query to the underlying database.
		Cursor c = qb.query(db, projection, where, whereArgs, null, null,
				orderBy);
{/* 
		//Log.d(LOGNAME, "Query results:" + c.getCount()); */}

		// Register the contexts ContentResolver to be notified if
		// the cursor result set changes.
		c.setNotificationUri(getContext().getContentResolver(), uri);

		// Return a cursor to the query result.
		return c;
	}

	@Override
	public int update(Uri uri, ContentValues values, String where,
			String[] whereArgs) {

		int count;
		switch (uriMatcher.match(uri)) {
		case MAPTILE:
			count = db.update(DATABASE_TABLE, values, where, whereArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}

		if (count > 0)
			getContext().getContentResolver().notifyChange(uri, null);

		return count;
	}

	public static ContentValues toContentValues(MapTileUpdateResource r,
			long updateTime) {
		ContentValues v = new ContentValues();

		// v.put(KEY_ID, r.getId());
		v.put(FIELD_LAYER, r.getLayer());
		v.put(FIELD_LAST_UPDATE, r.getLastUpdate());
		v.put(FIELD_ZOOM, r.getZoom());
		v.put(FIELD_TILEX, r.getTileX());
		v.put(FIELD_TILEY, r.getTileY());
		v.put(FIELD_REFERENCE, r.getReference());
		v.put(FIELD_TIMESTAMP, updateTime);

		return v;
	}

	public static MapTileUpdateResource fromCursor(Cursor cursor) {

		String layer = cursor.getString(cursor.getColumnIndex(FIELD_LAYER));
		int tileX = cursor.getInt(cursor.getColumnIndex(FIELD_TILEX));
		int tileY = cursor.getInt(cursor.getColumnIndex(FIELD_TILEY));
		int zoom = cursor.getInt(cursor.getColumnIndex(FIELD_ZOOM));

		MapTileUpdateResource a = new MapTileUpdateResource(layer, tileX,
				tileY, zoom);

		a.setLastUpdate(cursor.getLong(cursor.getColumnIndex(FIELD_LAST_UPDATE)));

		return a;
	}

	class DbHelper extends SQLiteOpenHelper {

		public DbHelper(Context context, String name, CursorFactory factory,
				int version) {
			super(context, name, factory, version);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(DATABASE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("Drop TABLE IF EXISTS " + DATABASE_TABLE);
			onCreate(db);
		}

	}
}