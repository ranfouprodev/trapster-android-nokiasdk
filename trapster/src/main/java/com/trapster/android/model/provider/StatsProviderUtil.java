package com.trapster.android.model.provider;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.text.format.DateUtils;

import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.util.Log;

import java.text.DecimalFormat;
import java.util.HashMap;

/**
 *
 * Provides the interfaces into the stats logging classes 
 *
 * @author John
 *
 */
public class StatsProviderUtil {

    public static final String STAT_VOTE = "votes";
    public static final String STAT_REPORT = "reports";
    public static final String STAT_DISTANCE = "distance";
    public static final String STAT_AVOIDED_TRAP = "avoidedtraps";

    public static final String STAT_TRIP_DISTANCE = "trip_distance";
    public static final String STAT_TRIP_AVGSPEED = "trip_avg";
    public static final String STAT_TRIP_TIME = "trip_time";

    public static final int CATEGORY_MAIN = 0;
    public static final int CATEGORY_TRIP = 1;

    private static final String LOGNAME = "StatsProviderUtil";

    public static boolean resetStats(Context context){

        addOrUpdateStat(context,STAT_VOTE,"0");
        addOrUpdateStat(context,STAT_REPORT,"0");
        addOrUpdateStat(context,STAT_DISTANCE,"0.0");
        addOrUpdateStat(context,STAT_AVOIDED_TRAP,"0");
        resetTripStats(context);
        return true;
    }

    public static boolean resetTripStats(Context context){

        addOrUpdateStat(context,STAT_TRIP_DISTANCE,"0.0");
        addOrUpdateStat(context,STAT_TRIP_AVGSPEED,"0.0");
        addOrUpdateStat(context,STAT_TRIP_TIME,String.valueOf(System.currentTimeMillis()));

        return true;
    }


    public static int getCategory(String statString){
        if(statString.equalsIgnoreCase(STAT_VOTE))
            return CATEGORY_MAIN;
        else if(statString.equalsIgnoreCase(STAT_REPORT))
            return CATEGORY_MAIN;
        else if(statString.equalsIgnoreCase(STAT_DISTANCE))
            return CATEGORY_MAIN;
        else if(statString.equalsIgnoreCase(STAT_TRIP_DISTANCE))
            return CATEGORY_TRIP;
        else if(statString.equalsIgnoreCase(STAT_TRIP_AVGSPEED))
            return CATEGORY_TRIP;
        else if(statString.equalsIgnoreCase(STAT_TRIP_TIME))
            return CATEGORY_TRIP;
        else if(statString.equalsIgnoreCase(STAT_AVOIDED_TRAP))
            return CATEGORY_MAIN;
        return CATEGORY_MAIN;

    }


    /**
     * Returns the display string for the stat where applicable
     *
     * @param context
     * @param statString
     * @return
     */
    public static String getNameDisplayString(Context context, String statString){

        SharedPreferences pref = context.getSharedPreferences(Defaults.PREF_SETTINGS, Context.MODE_PRIVATE);

        String units = (pref.getBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_MILES, true))?"miles":"km";
        String speedUnits = (pref.getBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_MILES, true))?"mph":"kph";

        if(statString.equalsIgnoreCase(STAT_VOTE))
            return context.getString(R.string.stats_vote);
        else if(statString.equalsIgnoreCase(STAT_REPORT))
            return context.getString(R.string.stats_report);
        else if(statString.equalsIgnoreCase(STAT_DISTANCE))
            return context.getString(R.string.stats_distance,units);
        else if(statString.equalsIgnoreCase(STAT_TRIP_DISTANCE))
            return context.getString(R.string.stats_trip_distance,units);
        else if(statString.equalsIgnoreCase(STAT_TRIP_AVGSPEED))
            return context.getString(R.string.stats_trip_avg_speed,speedUnits);
        else if(statString.equalsIgnoreCase(STAT_TRIP_TIME))
            return context.getString(R.string.stats_trip_time);
        else if(statString.equalsIgnoreCase(STAT_AVOIDED_TRAP))
            return context.getString(R.string.stats_avoided_traps);
        return null;
    }



    /**
     * Returns the value string. This is required for some unit conversions and unit display (mi/km)
     *
     * @param context
     * @param statString
     * @return
     */
    public static String getValueDisplayString(Context context, String statString, String valueString){
        SharedPreferences pref = context.getSharedPreferences(Defaults.PREF_SETTINGS, Context.MODE_PRIVATE);


        if(statString.equalsIgnoreCase(STAT_DISTANCE) || statString.equalsIgnoreCase(STAT_TRIP_DISTANCE)){

            double metersTraveled = 0;
            try{
                metersTraveled = Double.parseDouble(valueString);
            }catch(NumberFormatException ne){
{/*                 //Log.e(LOGNAME, "Error in the stats for meters traveled:" + valueString); */}
                return "--";
            }

            double km = metersTraveled / 1000;

            DecimalFormat distFormat = new DecimalFormat("#0.0");

            String dist = distFormat.format(km);
            if (pref.getBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_MILES, true))
                dist = distFormat.format(km * 0.62137);


            return dist;

        }else if (statString.equalsIgnoreCase(STAT_TRIP_TIME)){

            long now = System.currentTimeMillis();
            long tripStart = now;
            try{
                tripStart = Long.parseLong(valueString);
            }catch(NumberFormatException ne){
{/*                 //Log.e(LOGNAME, "Error in the stats for trip time:"+valueString); */}
                return "--";
            }

            long elapsed = (now - tripStart)/1000;
            return DateUtils.formatElapsedTime(elapsed);

        }else if (statString.equalsIgnoreCase(STAT_TRIP_AVGSPEED)){

            String tripString = getStat(context,STAT_TRIP_TIME);

            long now = System.currentTimeMillis();
            long tripStart = now;
            try{
                tripStart = Long.parseLong(tripString);
            }catch(NumberFormatException ne){
{/*                 //Log.e(LOGNAME, "Error in the stats for trip time:"+valueString); */}
                return "--";
            }

            long elapsed = (now - tripStart)/1000;

            double hours = (double)elapsed/3600;

            if(hours == 0)
                return "--";

            String distString = getStat(context,STAT_TRIP_DISTANCE);

            double metersTraveled = 0;
            try{
                metersTraveled = Double.parseDouble(distString);
            }catch(NumberFormatException ne){
{/*                 //Log.e(LOGNAME, "Error in the stats for meters traveled:"+valueString); */}
                return "--";
            }

            double km = metersTraveled / 1000;  // km
            DecimalFormat distFormat = new DecimalFormat("#0");

            String dist = distFormat.format(km/hours);
            if (pref.getBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_MILES, true))
                dist = distFormat.format((km * 0.62137)/hours);

            return dist;
        }else
            return valueString;

    }

    public static HashMap<String,String> getStats(Context context){

        HashMap<String,String> statsValues = new HashMap<String,String>();

        Cursor cursor = null;

        try {

            cursor = context.getContentResolver().query(StatsProvider.CONTENT_URI, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {

                do {
                    String name = cursor.getString(cursor.getColumnIndex(StatsProvider.KEY_NAME));
                    String value = cursor.getString(cursor.getColumnIndex(StatsProvider.KEY_VALUE));
                    statsValues.put(name, value);

                } while (cursor.moveToNext());
            }

        }finally{
            if (cursor != null)
                cursor.close();
        }


        return statsValues;

    }

    public static String getStat(Context context, String name){

        String value = null;

        Cursor cursor = null;

        String where = StatsProvider.KEY_NAME + "=?";

        String[] whereArgs = new String[] { name };

        try {

            cursor = context.getContentResolver().query(StatsProvider.CONTENT_URI, null, where, whereArgs, null);
            if (cursor != null && cursor.moveToFirst()) {

                do {
                    value = cursor.getString(cursor.getColumnIndex(StatsProvider.KEY_VALUE));
                } while (cursor.moveToNext());
            }

        }finally{
            if (cursor != null)
                cursor.close();
        }


        return value;

    }


    public static boolean incrementStatByDouble(Context context, String name, double amount){

        if(name == null || amount == 0)
            return false;

        String valueStr = getStat(context,name);

        if(valueStr == null)
            return addOrUpdateStat(context, name, String.valueOf(amount));


        double value = 0;
        try{
            value = Double.parseDouble(valueStr);
            value += amount;
        }catch(NumberFormatException ne){
{/*             //Log.e(LOGNAME, "Invalid value in db. Replacing with new increment:"+valueStr); */}
            return addOrUpdateStat(context, name, String.valueOf(amount));
        }


        ContentValues values = new ContentValues();
        values.put(StatsProvider.KEY_NAME,name);
        values.put(StatsProvider.KEY_VALUE,String.valueOf(value));
        values.put(StatsProvider.KEY_CAT,getCategory(name));
        values.put(StatsProvider.KEY_UPDATE_TIME, System.currentTimeMillis());

        String where = StatsProvider.KEY_NAME + "=?";

        String[] whereArgs = new String[] { name };

        try {

            if (context.getContentResolver().update(
                    StatsProvider.CONTENT_URI, values, where,
                    whereArgs) == 0) {
                if (context.getContentResolver().insert(
                        StatsProvider.CONTENT_URI, values) != null)
                    return true;
                return true;
            }
        } catch (Exception ex) {
{/*             //Log.e(LOGNAME, "Unable to add stat:" + name
                    + ":" + value+" to database."); */}
        }

        return false;

    }

    public static boolean incrementStatByInt(Context context, String name, int amount){

        if(name == null || amount == 0)
            return false;

        // @TODO one day fix this.
        if(name.equalsIgnoreCase(STAT_DISTANCE))
            throw new IllegalArgumentException("Use incrementStat(Context context, String name, double amount) to avoid rounding errors");

        String valueStr = getStat(context,name);

        if(valueStr == null)
            return addOrUpdateStat(context, name, String.valueOf(amount));


        int value = 0;
        try{
            value = Integer.parseInt(valueStr);
            value += amount;
        }catch(NumberFormatException ne){
{/*             //Log.e(LOGNAME, "Invalid value in db(int). Replacing with new increment:"+valueStr); */}
            return addOrUpdateStat(context, name, String.valueOf(amount));
        }


        ContentValues values = new ContentValues();
        values.put(StatsProvider.KEY_NAME,name);
        values.put(StatsProvider.KEY_VALUE,String.valueOf(value));
        values.put(StatsProvider.KEY_CAT,getCategory(name));
        values.put(StatsProvider.KEY_UPDATE_TIME, System.currentTimeMillis());

        String where = StatsProvider.KEY_NAME + "=?";

        String[] whereArgs = new String[] { name };

        try {

            if (context.getContentResolver().update(
                    StatsProvider.CONTENT_URI, values, where,
                    whereArgs) == 0) {
                if (context.getContentResolver().insert(
                        StatsProvider.CONTENT_URI, values) != null)
                    return true;
                return true;
            }
        } catch (Exception ex) {
{/*             //Log.e(LOGNAME, "Unable to add stat:" + name
                    + ":" + value+" to database."); */}
        }

        return false;

    }



    public static boolean addOrUpdateStat(Context context, String name, String value){

        if(name == null || value == null)
            return false;

        ContentValues values = new ContentValues();
        values.put(StatsProvider.KEY_NAME,name);
        values.put(StatsProvider.KEY_VALUE,value);
        values.put(StatsProvider.KEY_CAT,getCategory(name));
        values.put(StatsProvider.KEY_UPDATE_TIME, System.currentTimeMillis());

        String where = StatsProvider.KEY_NAME + "=?";

        String[] whereArgs = new String[] { name };

        try {

            if (context.getContentResolver().update(
                    StatsProvider.CONTENT_URI, values, where,
                    whereArgs) == 0) {
                if (context.getContentResolver().insert(
                        StatsProvider.CONTENT_URI, values) != null)
                    return true;
                return true;
            }
        } catch (Exception ex) {
{/*             //Log.e(LOGNAME, "Unable to add stat:" + name
                    + ":" + value+" to database."); */}
        }

        return false;

    }

}
