package com.trapster.android.model;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 12/7/12
 * Time: 4:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class UpdatedRouteLinkInfo {

    String linkId;
    double lat;
    double lng;
    double speedLimit;

    public void setRouteLink(String linkId)
    {
        this.linkId = linkId;
    }
    public void setLat(double lat)
    {
        this.lat = lat;
    }
    public void setLng (double lng)
    {
        this.lng = lng;
    }
    public void setSpeedLimit (double speedLimit)
    {
        this.speedLimit = speedLimit;
    }
    public String getRouteLinkId()
    {
        return linkId;
    }
    public double getLat()
    {
        return lat;
    }
    public double getLng ()
    {
        return lng;
    }
    public double getSpeedLimit ()
    {
        return speedLimit;
    }
}
