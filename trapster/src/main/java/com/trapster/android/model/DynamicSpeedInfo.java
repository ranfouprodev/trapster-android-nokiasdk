package com.trapster.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 10/24/13
 * Time: 2:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class DynamicSpeedInfo implements Serializable {


    /**
     * Speed in m/s of the traffic
     */
    @SerializedName("TrafficSpeed")
    private double trafficSpeed;


    /**
     * Time in seconds to cover the link with traffic
     */
    @SerializedName("TrafficTime")
    private double trafficTime;


    /**
     * Road speedlimit in m/s
     */
    @SerializedName("BaseSpeed")
    private double baseSpeed;

    /**
     * Time in seconds to cover the link
     */
    @SerializedName("BaseTime")
    private double baseTime;

    @SerializedName("JamFactor")
    private float jamFactor;

    @SerializedName("JamFactorTrend")
    private float jamFactorTrend;

    @SerializedName("Confidence")
    private float confidence;


    public float getJamFactor() {
        return jamFactor;
    }

    public void setJamFactor(float jamFactor) {
        this.jamFactor = jamFactor;
    }

    public float getJamFactorTrend() {
        return jamFactorTrend;
    }

    public void setJamFactorTrend(float jamFactorTrend) {
        this.jamFactorTrend = jamFactorTrend;
    }

    public float getConfidence() {
        return confidence;
    }

    public void setConfidence(float confidence) {
        this.confidence = confidence;
    }

    public double getTrafficSpeed() {
        return trafficSpeed;
    }

    public void setTrafficSpeed(double trafficSpeed) {
        this.trafficSpeed = trafficSpeed;
    }

    public double getTrafficTime() {
        return trafficTime;
    }

    public void setTrafficTime(double trafficTime) {
        this.trafficTime = trafficTime;
    }

    public double getBaseSpeed() {
        return baseSpeed;
    }

    public void setBaseSpeed(double baseSpeed) {
        this.baseSpeed = baseSpeed;
    }

    public double getBaseTime() {
        return baseTime;
    }

    public void setBaseTime(double baseTime) {
        this.baseTime = baseTime;
    }
}