package com.trapster.android.model.provider;

import android.content.*;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import com.trapster.android.Defaults;
import com.trapster.android.model.Category;
import com.trapster.android.model.TrapIcon;
import com.trapster.android.model.TrapLevel;
import com.trapster.android.model.TrapType;
import com.trapster.android.util.Log;

import roboguice.content.RoboContentProvider;

public class TrapTypeProvider extends RoboContentProvider {

	private static final String LOGNAME = "TrapTypeProvider";

	private static final String DATABASE_NAME = "TrapType.db";

	private static final String TYPES_TABLE = "TrapTypes";
	private static final String LEVEL_TABLE = "TrapTypeLevels";

	/**
	 * Version 5 - Added Category Sequence Field
	 */
	private static final int DATABASE_VERSION = 7;

	// The index (key) column name for use in where clauses.
	public static final String KEY_ID = "_id";

	// Field indexes
	public static final String FIELD_ID = "trap_id";
	private static final String FIELD_NAME = "name";
	private static final String FIELD_DESCRIPTION = "description";
	public static final String FIELD_CATEGORY_ID = "categoryId";
	public static final String FIELD_CATEGORY_NAME = "categoryName";
	public static final String FIELD_CATEGORY_SEQUENCE = "categorySequence";
	private static final String FIELD_ICON_ALT1 = "icon_alt1";
	private static final String FIELD_ICON_ALT2 = "icon_alt2";
	private static final String FIELD_ICON_BASE = "icon_base";
	private static final String FIELD_REPORTABLE = "reportable";
	private static final String FIELD_ALERTABLE = "alertable";
	private static final String FIELD_VOTABLE = "votable";
	private static final String FIELD_AUDIO_ID = "audioid";
	private static final String FIELD_ANNOUNCES = "announces";
	private static final String FIELD_NOTIFIABLE = "notifiable";
	private static final String FIELD_LIFETIME = "lifetime";

	// For Trap Levels
	public static final String FIELD_TRAP_ID = "trap_id";
    public static final String FIELD_BIT = "bit";
	public static final String FIELD_LEVEL_NAME = "levelName";
    public static final String FIELD_ICON = "icon";

	// SQL Statement to create a new database.
	private static final String TYPES_DATABASE_CREATE = "create table  "
			+ TYPES_TABLE + " (" + KEY_ID
			+ " INTEGER primary key autoincrement, " + FIELD_ID + " INTEGER, "
			+ FIELD_NAME + " VARCHAR(100), " + FIELD_DESCRIPTION
			+ " VARCHAR(100), " + FIELD_CATEGORY_ID + " INTEGER, "
			+ FIELD_CATEGORY_NAME + " VARCHAR(100), " + FIELD_CATEGORY_SEQUENCE
			+ " INTEGER, " + FIELD_ICON_ALT1 + " VARCHAR(255), "
			+ FIELD_ICON_ALT2 + " VARCHAR(255), " + FIELD_ICON_BASE
			+ " VARCHAR(255), " + FIELD_REPORTABLE + " INTEGER DEFAULT '1', "
			+ FIELD_ALERTABLE + " INTEGER DEFAULT '1', " + FIELD_VOTABLE
			+ " INTEGER DEFAULT 'TRUE', " + FIELD_AUDIO_ID + " VARCHAR(100), "
			+ FIELD_ANNOUNCES + " INTEGER DEFAULT '1', " + FIELD_NOTIFIABLE
			+ " INTEGER DEFAULT '1', " + FIELD_LIFETIME + " INTEGER);";

	private static final String LEVELS_DATABASE_CREATE = "create table "
			+ LEVEL_TABLE + " (" + KEY_ID
			+ " INTEGER primary key autoincrement, " + FIELD_TRAP_ID
			+ " INTEGER, " + FIELD_BIT + " VARCHAR(100), " + FIELD_LEVEL_NAME
			+ " VARCHAR(100), " + FIELD_ICON + " VARCHAR(255));";

	public static final String PROVIDER_NAME = "com.trapster.android.traptype";

	public static final Uri CONTENT_URI = Uri.parse("content://"
			+ PROVIDER_NAME + "/traptype");

	public static final Uri LEVEL_CONTENT_URI = Uri.parse("content://"
			+ PROVIDER_NAME + "/traplevel");

	private static final int TRAPTYPE = 1;
	private static final int TRAPLEVEL = 2;

	private static final UriMatcher uriMatcher;
	static {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(PROVIDER_NAME, "traptype", TRAPTYPE);
		uriMatcher.addURI(PROVIDER_NAME, "traplevel", TRAPLEVEL);
	}

	// Variable to hold the database instance
	private SQLiteDatabase db;
	private DbHelper dbHelper;

	@Override
	public boolean onCreate() {

		this.dbHelper = new DbHelper(getContext(), DATABASE_NAME, null,
				DATABASE_VERSION);
		try {
			db = dbHelper.getWritableDatabase();
		} catch (SQLiteException e) {
			db = null;
{/* 			//Log.d(LOGNAME, "Database Opening exception"); */}
		}

		return (db == null) ? false : true;
	}

	@Override
	public String getType(Uri uri) {
		switch (uriMatcher.match(uri)) {
		case TRAPTYPE:
			return "vnd.android.cursor.dir/vnd.trapster.traptype";
		case TRAPLEVEL:
			return "vnd.android.cursor.dir/vnd.trapster.traplevel";
		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
	}

	/*
	 * Delete will automatically trigger the removal of the trap levels
	 * 
	 * (non-Javadoc)
	 * 
	 * @see android.content.ContentProvider#delete(android.net.Uri,
	 * java.lang.String, java.lang.String[])
	 */
	@Override
	public int delete(Uri uri, String where, String[] whereArgs) {
		int count = 0;
		switch (uriMatcher.match(uri)) {
		case TRAPTYPE:
			count = db.delete(TYPES_TABLE, where, whereArgs);
			break;

		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}

		if (count > 0)
			getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {

		switch (uriMatcher.match(uri)) {
		case TRAPTYPE:
			// Insert the new row, will return the row number if successful.
			long rowID = db.insert(TYPES_TABLE, "nullhack", values);
			if (rowID > 0) {
				Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
				getContext().getContentResolver().notifyChange(uri, null);
				//Log,.v(LOGNAME, "Inserting TrapType:" + rowID);
				return _uri;

			}
			break;
		case TRAPLEVEL:
			rowID = db.insert(LEVEL_TABLE, "nullhack", values);
			if (rowID > 0) {
				Uri _uri = ContentUris.withAppendedId(LEVEL_CONTENT_URI, rowID);
				getContext().getContentResolver().notifyChange(uri, null);
				return _uri;
			}
			break;
		default:
			throw new SQLException("Failed to insert row into " + uri);
		}

		// Return a URI to the newly inserted row on success.
		throw new SQLException("Failed to insert row into " + uri);
	}

	@Override
	public int bulkInsert(Uri uri, ContentValues[] values) {

		long startTime = System.currentTimeMillis();
{/* 		Log.i(LOGNAME, "Starting Bulk Transaction of " + values.length
				+ " records"); */}

	int count = 0;

		switch (uriMatcher.match(uri)) {
		case TRAPTYPE:
			db.beginTransaction();
			
			try {
				for (int i = 0; i < values.length; i++) {

					String where = FIELD_ID + "="
							+ values[i].getAsInteger(FIELD_ID);

					if (db.update(TYPES_TABLE, values[i], where, null) == 0){
						db.insert(TYPES_TABLE, "unknown", values[i]);
                        Log.v(LOGNAME,"Adding New TrapType");
                    }else{
                        Log.v(LOGNAME,"Updating TrapType");
                    }

					count++;
				}
				db.setTransactionSuccessful();

				getContext().getContentResolver().notifyChange(uri, null);

				return count;
			} finally {
				db.endTransaction();
{/* 				Log.i(LOGNAME,
						"Transaction finished in  "
								+ (System.currentTimeMillis() - startTime)
								+ " ms"); */}
			}

		case TRAPLEVEL:
			db.beginTransaction();
			
			try {
				for (int i = 0; i < values.length; i++) {

					String where = FIELD_TRAP_ID + "="
							+ values[i].getAsInteger(FIELD_TRAP_ID) + " AND "+FIELD_BIT+"="+values[i].getAsInteger(FIELD_BIT);

					if (db.update(LEVEL_TABLE, values[i], where, null) == 0)
						db.insert(LEVEL_TABLE, "unknown", values[i]);

					count++;
				}
				db.setTransactionSuccessful();

				getContext().getContentResolver().notifyChange(uri, null);

				return count;
			} finally {
				db.endTransaction();
{/* 				Log.i(LOGNAME,
						"Transaction finished in  "
								+ (System.currentTimeMillis() - startTime)
								+ " ms"); */}
			}

		default:
			throw new SQLException("Failed to insert row into " + uri);
		}
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String where,
			String[] whereArgs, String sort) {

		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

		String orderBy = null;

		switch (uriMatcher.match(uri)) {
		case TRAPTYPE:
			qb.setTables(TYPES_TABLE);
			if (TextUtils.isEmpty(sort)) {
				orderBy = FIELD_CATEGORY_SEQUENCE + " ASC";
			} else {
				orderBy = sort;
			}

			break;
		case TRAPLEVEL:
			qb.setTables(LEVEL_TABLE);
			if (TextUtils.isEmpty(sort)) {
				orderBy = FIELD_BIT + " ASC";
			} else {
				orderBy = sort;
			}
			break;
		default:
			throw new SQLException("Failed to insert row into " + uri);
		}
{/* 
		//Log.d(LOGNAME, "URI:" + uri.toString() + ":" + uriMatcher.match(uri)); */}
{/* 		//Log.d(LOGNAME,
				"Running:"
						+ qb.buildQueryString(false, qb.getTables(),
								projection, where, null, null, orderBy, null)); */}

		// Apply the query to the underlying database.
		Cursor c = qb.query(db, projection, where, whereArgs, null, null,
				orderBy);
{/* 
		//Log.d(LOGNAME, "Query results:" + c.getCount()); */}

		// Register the contexts ContentResolver to be notified if
		// the cursor result set changes.
		c.setNotificationUri(getContext().getContentResolver(), uri);

		// Return a cursor to the query result.
		return c;

	}

	@Override
	public int update(Uri uri, ContentValues values, String where,
			String[] whereArgs) {

		int count;
		switch (uriMatcher.match(uri)) {
		case TRAPTYPE:
			count = db.update(TYPES_TABLE, values, where, whereArgs);
			//Log,.v(LOGNAME, "Updating TrapType:" + count);
			break;
		case TRAPLEVEL:
			count = db.update(LEVEL_TABLE, values, where, whereArgs);
			break;

		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}

		if (count > 0)
			getContext().getContentResolver().notifyChange(uri, null);

		return count;
	}

	public static TrapType trapTypefromCursor(Cursor cursor) {

		TrapType type = new TrapType();

		type.setId(cursor.getInt(cursor.getColumnIndex(FIELD_ID)));
		type.setName(cursor.getString(cursor.getColumnIndex(FIELD_NAME)));
		type.setDescription(cursor.getString(cursor
				.getColumnIndex(FIELD_DESCRIPTION)));
		// Category
		int categoryId = cursor
				.getInt(cursor.getColumnIndex(FIELD_CATEGORY_ID));
		String categoryName = cursor.getString(cursor
				.getColumnIndex(FIELD_CATEGORY_NAME));
		int categorySequence = cursor.getInt(cursor
				.getColumnIndex(FIELD_CATEGORY_SEQUENCE));

		Category cat = new Category(categoryId, categoryName, categorySequence);
		type.setCategory(cat);
		type.setCategoryId(categoryId);

		// Trap Icon
		TrapIcon icon = new TrapIcon();
		icon.setAlt1(cursor.getString(cursor.getColumnIndex(FIELD_ICON_ALT1)));
		icon.setAlt2(cursor.getString(cursor.getColumnIndex(FIELD_ICON_ALT2)));
		icon.setBase(cursor.getString(cursor.getColumnIndex(FIELD_ICON_BASE)));
		type.setIcon(icon);

		type.setReportable(cursor.getInt(cursor
				.getColumnIndex(FIELD_REPORTABLE)) == 1);
		type.setAlertable(cursor.getInt(cursor.getColumnIndex(FIELD_ALERTABLE)) == 1);
		type.setVotable(cursor.getInt(cursor.getColumnIndex(FIELD_VOTABLE)) == 1);
		type.setNotifiable(cursor.getInt(cursor
				.getColumnIndex(FIELD_NOTIFIABLE)) == 1);

		type.setAudioid(cursor.getString(cursor.getColumnIndex(FIELD_AUDIO_ID)));

		type.setAnnounces(cursor.getInt(cursor.getColumnIndex(FIELD_ANNOUNCES)) == 1);
		type.setLifetime(cursor.getLong(cursor.getColumnIndex(FIELD_LIFETIME)));

		return type;

	}

	public static TrapLevel trapLevelfromCursor(Cursor cursor) {
		TrapLevel level = new TrapLevel();

		level.setBit(cursor.getInt(cursor.getColumnIndex(FIELD_BIT)));
		level.setIcon(cursor.getString(cursor.getColumnIndex(FIELD_ICON)));
		level.setLevelName(cursor.getString(cursor
				.getColumnIndex(FIELD_LEVEL_NAME)));

		return level;
	}

	public static ContentValues toContentValues(int trapTypeId, TrapLevel level) {

		ContentValues v = new ContentValues();

		v.put(FIELD_TRAP_ID, trapTypeId);
		v.put(FIELD_BIT, level.getBit());
		v.put(FIELD_LEVEL_NAME, level.getLevelName());
		v.put(FIELD_ICON, level.getIcon());

		return v;

	}

	public static ContentValues toContentValues(TrapType type) {

		ContentValues v = new ContentValues();

		v.put(FIELD_ID, type.getId());
		v.put(FIELD_NAME, type.getName());
		v.put(FIELD_DESCRIPTION, type.getDescription());
		v.put(FIELD_CATEGORY_ID, type.getCategoryId());
		v.put(FIELD_ICON_ALT1, type.getIcon().getAlt1());
		v.put(FIELD_ICON_ALT2, type.getIcon().getAlt2());
		v.put(FIELD_ICON_BASE, type.getIcon().getBase());
		v.put(FIELD_REPORTABLE, (type.isReportable()) ? 1 : 0);
		v.put(FIELD_ALERTABLE, type.isAlertable() ? 1 : 0);
		v.put(FIELD_VOTABLE, type.isVotable() ? 1 : 0);
		v.put(FIELD_AUDIO_ID, type.getAudioid());
		v.put(FIELD_ANNOUNCES, type.isAnnounces() ? 1 : 0);
		v.put(FIELD_NOTIFIABLE, type.isNotifiable() ? 1 : 0);
		v.put(FIELD_LIFETIME, type.getLifetime());

        Category category = type.getCategory();
        if (category != null)
        {
            v.put(FIELD_CATEGORY_NAME, category.getName());
            v.put(FIELD_CATEGORY_SEQUENCE, category.getSequence());
        }

		return v;
	}

	private class DbHelper extends SQLiteOpenHelper {

		public DbHelper(Context context, String name, CursorFactory factory,
				int version) {
			super(context, name, factory, version);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(TYPES_DATABASE_CREATE);
			db.execSQL(LEVELS_DATABASE_CREATE);
			addTriggers(db);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


            SharedPreferences sharedPreferences = getContext().getSharedPreferences(Defaults.PREF_SETTINGS, Context.MODE_PRIVATE);
            sharedPreferences.edit().putLong(Defaults.PREFERENCE_LAST_UPDATE_TIME_ALLATTRIBUTES, 0).commit(); // Reset our attributes
            sharedPreferences.edit().putLong(Defaults.PREFERENCE_LAST_UPDATE_TIME_TRAP_CONFIG, 0).commit(); // Reset our trap config
			db.execSQL("Drop TABLE IF EXISTS " + TYPES_TABLE);
			db.execSQL("Drop TABLE IF EXISTS " + LEVEL_TABLE);
			onCreate(db);
		}

		private void addTriggers(SQLiteDatabase db) {

			String databaseTrigger = "CREATE TRIGGER fkd_" + TYPES_TABLE + "_"
					+ LEVEL_TABLE + " BEFORE DELETE ON " + TYPES_TABLE
					+ " FOR EACH ROW BEGIN " + " DELETE FROM " + LEVEL_TABLE
					+ " WHERE " + FIELD_TRAP_ID + " = OLD." + FIELD_ID + "; "
					+ " END;";

			db.execSQL(databaseTrigger);
		}

	}

}
