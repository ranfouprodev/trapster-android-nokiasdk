package com.trapster.android.model.provider;

import android.content.*;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import com.trapster.android.model.ProbeData;


public class ProbeDataProvider extends ContentProvider {

	private static final String LOGNAME = "ProbeDataProvider";

	private static final String DATABASE_NAME = "Probedata.db";
	private static final String DATABASE_TABLE = "probe";

	private static final int DATABASE_VERSION = 2;

	// The index (key) column name for use in where clauses.
	public static final String KEY_ID = "_id";
	public static final String FIELD_TIMESTAMP = "timestamp";
	public static final String FIELD_LATITUDE = "latitude";
	public static final String FIELD_LONGITUDE = "longitude";
	public static final String FIELD_ACCURACY = "accuracy";
	public static final String FIELD_ALTITUDE = "altitude";
	public static final String FIELD_SPEED = "speed";
	public static final String FIELD_BEARING = "bearing";
	public static final String FIELD_CELLINFO = "cellinfo";
	public static final String FIELD_SOURCE = "source";
	public static final String FIELD_PROVIDER = "provider";
	public static final String FIELD_VEHICLEID = "vehicleid";
	public static final String FIELD_SYNC = "sync";
	
	
	// SQL Statement to create a new database.
	private static final String DATABASE_CREATE = "create table if not exists "
			+ DATABASE_TABLE + " (" + KEY_ID+ " INTEGER primary key autoincrement, " 
			+ FIELD_TIMESTAMP + " FLOAT,"
			+ FIELD_LATITUDE + " FLOAT, "
			+ FIELD_LONGITUDE + " FLOAT, "
			+ FIELD_ACCURACY + " FLOAT, "
			+ FIELD_ALTITUDE + " FLOAT, "
			+ FIELD_SPEED + " FLOAT, "
			+ FIELD_BEARING + " FLOAT, "
			+ FIELD_CELLINFO + " TEXT, "
			+ FIELD_SOURCE + " TEXT, "
			+ FIELD_PROVIDER + " TEXT, "
			+ FIELD_VEHICLEID + " TEXT, "
			+ FIELD_SYNC + " INTEGER); ";

	public static final String PROVIDER_NAME = "com.trapster.android.probedata";

	public static final Uri CONTENT_URI = Uri.parse("content://"
			+ PROVIDER_NAME + "/probe");

	private static final int PROBE = 1;

	private static final UriMatcher uriMatcher;
	static {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(PROVIDER_NAME, "probe", PROBE);
	}

	// Variable to hold the database instance
	private SQLiteDatabase db;
	private DbHelper dbHelper;

	@Override
	public boolean onCreate() {

		this.dbHelper = new DbHelper(getContext(), DATABASE_NAME, null,
				DATABASE_VERSION);
		try {
			db = dbHelper.getWritableDatabase();
		} catch (SQLiteException e) {
			db = null;
{/* 			//Log.d(LOGNAME, "Database Opening exception"); */}
		}

		return (db == null) ? false : true;
	}

	@Override
	public String getType(Uri uri) {
		switch (uriMatcher.match(uri)) {
		case PROBE:
			return "vnd.android.cursor.dir/vnd.trapster.probedata";
		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
	}

	@Override
	public int delete(Uri uri, String where, String[] whereArgs) {
		int count = 0;
		switch (uriMatcher.match(uri)) {
		case PROBE:
			count = db.delete(DATABASE_TABLE, where, whereArgs);
			break;

		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}

		if (count > 0)
			getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		switch (uriMatcher.match(uri)) {
		case PROBE:
			long rowId = -1;

			if (values.containsKey(KEY_ID)) {
				String where = KEY_ID + "=" + values.getAsInteger(KEY_ID);

				rowId = values.getAsInteger(KEY_ID);

				if (db.update(DATABASE_TABLE, values, where, null) == 0)
					db.insert(DATABASE_TABLE, "unknown", values);
			} else {
				rowId = db.insert(DATABASE_TABLE, "unknown", values);
			}

			if (rowId > 0) {
				Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowId);
				getContext().getContentResolver().notifyChange(uri, null);
				return _uri;
			}
			break;

		default:
			throw new SQLException("Failed to insert row, invalid URI " + uri);
		}

		// Return a URI to the newly inserted row on success.
		throw new SQLException("Failed to insert row into " + uri);
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String where,
			String[] whereArgs, String sort) {

		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		qb.setTables(DATABASE_TABLE);

		String orderBy = null;

		switch (uriMatcher.match(uri)) {
		case PROBE:
			// add sort?
			break;
		default:
			throw new SQLException("Failed to insert row into " + uri);
		}
{/* 
		//Log.d(LOGNAME, "URI:" + uri.toString() + ":" + uriMatcher.match(uri)); */}
{/* 		//Log.d(LOGNAME,
				"Running:"
						+ qb.buildQueryString(false, qb.getTables(),
								projection, where, null, null, orderBy, null)); */}

		// Apply the query to the underlying database.
		Cursor c = qb.query(db, projection, where, whereArgs, null, null,
				orderBy);
{/* 
		//Log.d(LOGNAME, "Query results:" + c.getCount()); */}

		// Register the contexts ContentResolver to be notified if
		// the cursor result set changes.
		c.setNotificationUri(getContext().getContentResolver(), uri);

		// Return a cursor to the query result.
		return c;
	}

	@Override
	public int update(Uri uri, ContentValues values, String where,
			String[] whereArgs) {

		int count;
		switch (uriMatcher.match(uri)) {
		case PROBE:
			count = db.update(DATABASE_TABLE, values, where, whereArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}

		if (count > 0)
			getContext().getContentResolver().notifyChange(uri, null);

		return count;
	}

	public static ContentValues toContentValues(ProbeData probe) {
		ContentValues v = new ContentValues();
		
		//v.put(KEY_ID, probe.getId());
		v.put(FIELD_TIMESTAMP, probe.getTimestamp());
		v.put(FIELD_LATITUDE, probe.getLatitude());
		v.put(FIELD_LONGITUDE, probe.getLongitude());
		v.put(FIELD_ACCURACY, probe.getHorizontalAccuracy());
		v.put(FIELD_ALTITUDE, probe.getAltitude());
		v.put(FIELD_SPEED, probe.getSpeed());
		v.put(FIELD_BEARING, probe.getBearing());
		v.put(FIELD_CELLINFO, probe.getCellInformation());
		v.put(FIELD_SOURCE, probe.getSource());
		v.put(FIELD_PROVIDER, probe.getProvider());
		//v.put(FIELD_VEHICLEID, probe.getVehicleId());
		v.put(FIELD_SYNC, probe.isSyncedWithServer()?1:0);
		
		return v; 
	}

	public static ProbeData fromCursor(Cursor cursor) {

		ProbeData probe = new ProbeData(); 
		
		probe.setAltitude(cursor.getDouble(cursor.getColumnIndex(FIELD_ALTITUDE)));
		probe.setBearing(cursor.getFloat(cursor.getColumnIndex(FIELD_BEARING)));
		probe.setCellInformation(cursor.getString(cursor.getColumnIndex(FIELD_CELLINFO)));
		probe.setHorizontalAccuracy(cursor.getDouble(cursor.getColumnIndex(FIELD_ACCURACY)));
		probe.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
		probe.setLatitude(cursor.getDouble(cursor.getColumnIndex(FIELD_LATITUDE)));
		probe.setLongitude(cursor.getDouble(cursor.getColumnIndex(FIELD_LONGITUDE)));
		probe.setProvider(cursor.getString(cursor.getColumnIndex(FIELD_PROVIDER)));
		probe.setSource(cursor.getString(cursor.getColumnIndex(FIELD_SOURCE)));
		probe.setSpeed(cursor.getFloat(cursor.getColumnIndex(FIELD_SPEED)));
		
		probe.setSyncedWithServer((cursor.getInt(cursor.getColumnIndex(FIELD_SYNC))) > 0);
		
		probe.setTimestamp(cursor.getLong(cursor.getColumnIndex(FIELD_TIMESTAMP)));
		//probe.setVehicleId(cursor.getString(cursor.getColumnIndex(FIELD_VEHICLEID)));
				
		return probe;
	}

	class DbHelper extends SQLiteOpenHelper {

		public DbHelper(Context context, String name, CursorFactory factory,
				int version) {
			super(context, name, factory, version);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(DATABASE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// Logger.w(LOGNAME, "Upgrading from version " + oldVersion + " to "
			// + newVersion + ", which will destroy all old data");
			db.execSQL("Drop TABLE IF EXISTS " + DATABASE_TABLE);
			onCreate(db);
		}

	}
}
