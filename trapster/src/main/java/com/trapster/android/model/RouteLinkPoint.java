package com.trapster.android.model;

import java.io.Serializable;
import com.here.android.common.GeoCoordinate;
import com.here.android.mapping.MapFactory;

public class RouteLinkPoint implements Serializable
{
    String linkId;
    int pointSeqNb;
    double pointLat;
    double pointLng;
    //
    public GeoCoordinate toGeoPoint()
    {
        return MapFactory.createGeoCoordinate(pointLat, pointLng);
    }
    //
    public String getLinkId()
    {
        return linkId;
    }
    public void setLinkId(String linkId)
    {
        this.linkId= linkId;
    }
    public int getPointSeqNb()
    {
        return pointSeqNb;
    }
    public void setPointSeqNb(int pointSeqNb)
    {
        this.pointSeqNb= pointSeqNb;
    }
    public double getPointLat()
    {
        return pointLat;
    }
    public void setPointLat(double pointLat)
    {
        this.pointLat= pointLat;
    }
    public double getPointLng()
    {
        return pointLng;
    }
    public void setPointLng(double pointLng)
    {
        this.pointLng= pointLng;
    }
}
