package com.trapster.android.model.provider;


import android.content.*;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import com.trapster.android.model.ImageResource;
import com.trapster.android.model.ImageSpec;

public class ImageResourceProvider extends ContentProvider {

	private static final String LOGNAME = "ImageResourceProvider";

	private static final String DATABASE_NAME = "ImageResources.db";
	private static final String DATABASE_TABLE = "Images";

	private static final int DATABASE_VERSION = 3;

	// The index (key) column name for use in where clauses.
	private static final String KEY_ID = "_id";
	public static final String FIELD_KEY = "name";
	private static final String FIELD_URL = "url";
	private static final String FIELD_SPEC_WIDTH = "width";
	private static final String FIELD_SPEC_HEIGHT = "height";
	private static final String FIELD_SPEC_PINX = "pinx";
	private static final String FIELD_SPEC_PINY = "piny";
	private static final String FIELD_FILENAME = "filename";
	private static final String FIELD_PRELOAD = "preload";
	

	// SQL Statement to create a new database.
	private static final String DATABASE_CREATE = "create table if not exists "
			+ DATABASE_TABLE + " (" + KEY_ID
			+ " INTEGER primary key autoincrement, " + FIELD_KEY
			+ " VARCHAR(100), " + FIELD_URL + " VARCHAR(255), "
			+ FIELD_PRELOAD + " INTEGER,"+ FIELD_SPEC_WIDTH + " INTEGER, " + FIELD_SPEC_HEIGHT
			+ " INTEGER, " + FIELD_SPEC_PINX + " INTEGER, " + FIELD_SPEC_PINY
			+ " INTEGER, " + FIELD_FILENAME + " VARCHAR(255)); ";

	public static final String PROVIDER_NAME = "com.trapster.android.imageresource";

	public static final Uri CONTENT_URI = Uri.parse("content://"
			+ PROVIDER_NAME + "/image");

	private static final int IMAGE = 1;

	private static final UriMatcher uriMatcher;
	static {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(PROVIDER_NAME, "image", IMAGE);
	}

	// Variable to hold the database instance
	private SQLiteDatabase db;
	private DbHelper dbHelper;

	@Override
	public boolean onCreate() {

		this.dbHelper = new DbHelper(getContext(), DATABASE_NAME, null,
				DATABASE_VERSION);
		try {
			db = dbHelper.getWritableDatabase();
		} catch (SQLiteException e) {
			db = null;
{/* 			//Log.d(LOGNAME, "Database Opening exception"); */}
		}

		return (db == null) ? false : true;
	}
	
	@Override
	public String getType(Uri uri) {
		switch (uriMatcher.match(uri)) {
		case IMAGE:
			return "vnd.android.cursor.dir/vnd.trapster.imageresource";
		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
	}

	@Override
	public int delete(Uri uri, String where, String[] whereArgs) {
		int count = 0;
		switch (uriMatcher.match(uri)) {
		case IMAGE:
			count = db.delete(DATABASE_TABLE, where, whereArgs);
			break;

		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}

		if(count > 0)
			getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

	
	@Override
	public Uri insert(Uri uri, ContentValues values) {
		switch (uriMatcher.match(uri)) {
		case IMAGE:
			long rowId = -1; 

			if(values.containsKey(KEY_ID)){
				String where = KEY_ID + "=" + values.getAsInteger(KEY_ID);
		
				rowId = values.getAsInteger(KEY_ID);
				
				if (db.update(DATABASE_TABLE, values, where, null) == 0)
					db.insert(DATABASE_TABLE, "unknown", values);
			}else{
				rowId = db.insert(DATABASE_TABLE, "unknown", values);
			}
			
			if (rowId > 0) {
				Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowId);
				getContext().getContentResolver().notifyChange(uri, null);
				return _uri;
			}
			break;
		
		default:
			throw new SQLException("Failed to insert row, invalid URI " + uri);
		}
		


		// Return a URI to the newly inserted row on success.
		throw new SQLException("Failed to insert row into " + uri);
	}

	@Override
	public int bulkInsert(Uri uri, ContentValues[] values) {

		long startTime = System.currentTimeMillis();
{/* 		Log.i(LOGNAME, "Starting Bulk Transaction of " + values.length
				+ " records"); */}

		db.beginTransaction();
		int count = 0;

		try {
			for (int i = 0; i < values.length; i++) {

				String where = KEY_ID + "=" + values[i].getAsInteger(KEY_ID);

				if (db.update(DATABASE_TABLE, values[i], where, null) == 0)
					db.insert(DATABASE_TABLE, "unknown", values[i]);
				
				count++;
			}
			db.setTransactionSuccessful();

			getContext().getContentResolver().notifyChange(uri, null);
			
			return count;
		} finally {
			db.endTransaction();
{/* 			Log.i(LOGNAME,
					"Transaction finished in  "
							+ (System.currentTimeMillis() - startTime) + " ms"); */}
		}
	}
	
	
	
	@Override
	public Cursor query(Uri uri, String[] projection, String where,
			String[] whereArgs, String sort) {
		
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		qb.setTables(DATABASE_TABLE);
		
		String orderBy = null;

		switch (uriMatcher.match(uri)) {
		case IMAGE:
			// add sort?
			break;
		default:
			throw new SQLException("Failed to insert row into " + uri);
		}
{/* 
		//Log.d(LOGNAME, "URI:" + uri.toString() + ":" + uriMatcher.match(uri)); */}
{/* 		//Log.d(LOGNAME,
				"Running:"
						+ qb.buildQueryString(false, qb.getTables(),
								projection, where, null, null, orderBy, null)); */}

		// Apply the query to the underlying database.
		Cursor c = qb.query(db, projection, where, whereArgs, null, null,
				orderBy);
{/* 
		//Log.d(LOGNAME, "Query results:" + c.getCount()); */}

		// Register the contexts ContentResolver to be notified if
		// the cursor result set changes.
		c.setNotificationUri(getContext().getContentResolver(), uri);

		// Return a cursor to the query result.
		return c;
	}

	@Override
	public int update(Uri uri, ContentValues values, String where,
			String[] whereArgs) {

		int count;
		switch (uriMatcher.match(uri)) {
		case IMAGE:
			count = db.update(DATABASE_TABLE, values, where, whereArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}

		if(count > 0)
			getContext().getContentResolver().notifyChange(uri, null);

		return count;
	}

	public static ContentValues toContentValues(ImageResource r) {
		ContentValues v = new ContentValues();

		v.put(FIELD_KEY, r.getKey());
		v.put(FIELD_URL, r.getPath());
		v.put(FIELD_FILENAME, r.getFilename());
		v.put(FIELD_PRELOAD, (r.isPreload())?1:0);

		v.put(FIELD_SPEC_WIDTH, r.getSpec().getWidth());
		v.put(FIELD_SPEC_HEIGHT, r.getSpec().getHeight());
		v.put(FIELD_SPEC_PINX, r.getSpec().getPinx());
		v.put(FIELD_SPEC_PINY, r.getSpec().getPiny());
		return v;
	}

	public static ContentValues toContentValues(ImageResource r, boolean isPreload) {
		ContentValues v = new ContentValues();

		v.put(FIELD_KEY, r.getKey());
		v.put(FIELD_URL, r.getPath());
		v.put(FIELD_FILENAME, r.getFilename());
		v.put(FIELD_PRELOAD, isPreload?1:0);

		v.put(FIELD_SPEC_WIDTH, r.getSpec().getWidth());
		v.put(FIELD_SPEC_HEIGHT, r.getSpec().getHeight());
		v.put(FIELD_SPEC_PINX, r.getSpec().getPinx());
		v.put(FIELD_SPEC_PINY, r.getSpec().getPiny());
		return v;
	}
	
	public static ImageResource fromCursor(Cursor cursor) {

		int nameIndex = cursor.getColumnIndex(FIELD_KEY);
		int urlIndex = cursor.getColumnIndex(FIELD_URL);
		int specWidthIndex = cursor.getColumnIndex(FIELD_SPEC_WIDTH);
		int specHeightIndex = cursor.getColumnIndex(FIELD_SPEC_HEIGHT);
		int specPinxIndex = cursor.getColumnIndex(FIELD_SPEC_PINX);
		int specPinyIndex = cursor.getColumnIndex(FIELD_SPEC_PINY);

		int filenameIndex = cursor.getColumnIndex(FIELD_FILENAME);
		int idIndex = cursor.getColumnIndex(KEY_ID);

		ImageResource a = new ImageResource();
		a.setId(cursor.getInt(idIndex));
		a.setKey(cursor.getString(nameIndex));
		a.setFilename(cursor.getString(filenameIndex));
		a.setPath(cursor.getString(urlIndex));

		a.setPreload(cursor.getInt(cursor.getColumnIndex(FIELD_PRELOAD))>0);
		
		ImageSpec spec = new ImageSpec();
		spec.setHeight(cursor.getInt(specHeightIndex));
		spec.setWidth(cursor.getInt(specWidthIndex));
		spec.setPinx(cursor.getInt(specPinxIndex));
		spec.setPiny(cursor.getInt(specPinyIndex));

		a.setSpec(spec);
		
		return a; 
	}
	
	class DbHelper extends SQLiteOpenHelper {

		public DbHelper(Context context, String name, CursorFactory factory,
				int version) {
			super(context, name, factory, version);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(DATABASE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("Drop TABLE IF EXISTS " + DATABASE_TABLE);
			onCreate(db);
		}

	}

}
