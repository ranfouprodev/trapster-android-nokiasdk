package com.trapster.android.model.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.google.inject.Inject;
import com.trapster.android.model.MapTileUpdateResource;
import com.trapster.android.model.PatrolPath;
import com.trapster.android.model.PatrolPoint;
import com.trapster.android.model.provider.PatrolPathProvider;

import roboguice.inject.ContextSingleton;

import java.util.ArrayList;

@ContextSingleton
public class PatrolPathDAO {

	@Inject
	Context context;

	public PatrolPathDAO() {
	}

	public void save(MapTileUpdateResource m, PatrolPath r) {

		context.getContentResolver().insert(
				PatrolPathProvider.PATH_CONTENT_URI,
				PatrolPathProvider.toContentValues(m, r));

	}

	public void removePaths(MapTileUpdateResource tile) {

		String mapTileRef = tile.getReference();

		String where = PatrolPathProvider.PATH_FIELD_TILEREF + "=?";

		String[] whereArgs = new String[] { mapTileRef };

		Cursor cursor = null;
		try {

			cursor = context.getContentResolver().query(
					PatrolPathProvider.PATH_CONTENT_URI, null, where,
					whereArgs, null);

			if (cursor != null && cursor.moveToFirst()) {

				do {

					int pathId = cursor.getInt(cursor
							.getColumnIndex(PatrolPathProvider.PATH_FIELD_ID));
					deletePoints(pathId);

				} while (cursor.moveToNext());

			}
		} finally {
			if (cursor != null)
				cursor.close();

		}

		context.getContentResolver().delete(
				PatrolPathProvider.PATH_CONTENT_URI,
				PatrolPathProvider.PATH_FIELD_TILEREF + "=?",
				new String[] { mapTileRef });
	}

	private void deletePoints(int pathId) {

		String where = PatrolPathProvider.POINTS_FIELD_PATHID + "=" + pathId;
		context.getContentResolver().delete(
				PatrolPathProvider.POINT_CONTENT_URI, where, null);

	}

	/*
	 * Query Methods
	 */
	private ArrayList<PatrolPoint> getPointsForPath(int pathId) {

		String where = PatrolPathProvider.POINTS_FIELD_PATHID + "=" + pathId;

		String orderBy = PatrolPathProvider.POINTS_FIELD_SEQUENCE + " ASC";

		ArrayList<PatrolPoint> points = new ArrayList<PatrolPoint>();

		Cursor cursor = null;
		try {

			cursor = context.getContentResolver().query(
					PatrolPathProvider.POINT_CONTENT_URI, null, where, null,
					orderBy);

			points = PatrolPathProvider.getPointsFromCursor(cursor);

		} finally {
			if (cursor != null)
				cursor.close();

		}

		return points;

	}

	private PatrolPath setPathFromPoints(ArrayList<PatrolPoint> points) {
		PatrolPath path = new PatrolPath();
		path.setPoints(points);
		return path;
	}

	public ArrayList<PatrolPath> getPaths(MapTileUpdateResource maptile) {

		String mapTileRef = maptile.getReference();

		ArrayList<PatrolPath> paths = new ArrayList<PatrolPath>();

		String where = PatrolPathProvider.PATH_FIELD_TILEREF + "=?";

		String[] whereArgs = new String[] { mapTileRef };

		Cursor cursor = null;
		int pathId = 0;
		try {

			cursor = context.getContentResolver().query(
					PatrolPathProvider.PATH_CONTENT_URI, null, where,
					whereArgs, null);

			if (cursor != null && cursor.moveToFirst()) {

				do {

					pathId = PatrolPathProvider
							.getPatrolPathIdFromCursor(cursor);
					paths.add(setPathFromPoints(getPointsForPath(pathId)));

				} while (cursor.moveToNext());

			}

		} finally {
			if (cursor != null)
				cursor.close();

		}

		return paths;

	}

	public ArrayList<PatrolPath> getAllPaths() {

		ArrayList<PatrolPath> paths = new ArrayList<PatrolPath>();

		String where = null;

		String[] whereArgs = null;

		Cursor cursor = null;
		int pathId = 0;
		try {

			cursor = context.getContentResolver().query(
					PatrolPathProvider.PATH_CONTENT_URI, null, where,
					whereArgs, null);

			if (cursor != null && cursor.moveToFirst()) {

				do {

					pathId = PatrolPathProvider
							.getPatrolPathIdFromCursor(cursor);
					paths.add(setPathFromPoints(getPointsForPath(pathId)));

				} while (cursor.moveToNext());

			}

		} finally {
			if (cursor != null)
				cursor.close();

		}

		return paths;
	}

}
