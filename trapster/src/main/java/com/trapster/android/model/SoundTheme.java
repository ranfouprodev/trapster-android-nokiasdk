package com.trapster.android.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;


public class SoundTheme {

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getUpdated() {
        return updated;
    }

    public void setUpdated(long updated) {
        this.updated = updated;
    }

    public static enum State {empty, loading, loaded};


    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;
    private transient State state;

    @SerializedName("author")
    private String author;

    @SerializedName("created")
    private long created;

    @SerializedName("updated")
    private long updated;

    @SerializedName("version")
    private String version;

    @SerializedName("links")
    private SoundLinks links = new SoundLinks();

    private class SoundLinks{

        @SerializedName("link")
        protected  ArrayList<SoundLink> links = new ArrayList<SoundLink>();
    }




    private transient HashMap<String,String> resource;


    public State getState() {
        return state;
    }

    public HashMap<String, String> getResource() {
        return resource;
    }

    public void setResource(HashMap<String, String> resource) {
        this.resource = resource;
    }

    public void setState(State state) {
        this.state = state;
    }


    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
    }


    public String getAuthor() {
        return author;
    }


    public void setAuthor(String author) {
        this.author = author;
    }





    public String getVersion() {
        return version;
    }


    public void setVersion(String version) {
        this.version = version;
    }

    public SoundTheme(){
        resource = new HashMap<String,String>();
        state = State.empty;
    }


    public void addSoundReference(String trapType, String resourceUri){
        resource.put(trapType,resourceUri);
    }

    public void clearLocalReferences(){
        resource = new HashMap<String,String>();
        state = State.empty;
    }


    public String getSoundResource(String trapType) throws ResourceNotLoadedException{

        if(!resource.containsKey(trapType))
            throw new ResourceNotLoadedException();

        return resource.get(trapType);

    }

    public void setLinks(ArrayList<SoundLink> links) {
        this.links.links = links;
    }

    public ArrayList<SoundLink> getLinks() {
        return links.links;
    }



}
