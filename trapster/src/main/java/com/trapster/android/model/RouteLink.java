package com.trapster.android.model;

import com.google.gson.annotations.SerializedName;
import com.here.android.common.GeoBoundingBox;
import com.here.android.common.GeoCoordinate;
import com.here.android.mapping.MapFactory;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.StringTokenizer;


public class RouteLink implements Serializable
{
    @SerializedName("LinkId")
    String linkId;

    @SerializedName("FunctionalClass")
    private int functionalClass;

    /**
     * Segment length in meters
     */
    @SerializedName("Length")
    private double length;

    @SerializedName("SpeedLimit")
    double speedLimit;

    /*************************
     Used in Routing
     */

    @SerializedName("RemainDistance")
    private float remainDistance;

    @SerializedName("RemainTime")
    private float remainTime;

    @SerializedName("NextLink")
    private float nextLink;

    //*************************

    @SerializedName("Shape")
    private String [] routeLinkShape;

    @SerializedName("DynamicSpeedInfo")
    private DynamicSpeedInfo dynamicSpeedInfo;

    @SerializedName("Address")
    private AddressInfo addressInfo;

    String quadkey;
    double bboxMinLat;
    double bboxMinLng;
    double bboxMaxLat;
    double bboxMaxLng;

    String routelinkLineString;
    GeoBoundingBox bbox;
    double distanceFromCurrentLocation;
    //


    public float getRemainDistance() {
        return remainDistance;
    }

    public void setRemainDistance(float remainDistance) {
        this.remainDistance = remainDistance;
    }

    public float getRemainTime() {
        return remainTime;
    }

    public void setRemainTime(float remainTime) {
        this.remainTime = remainTime;
    }

    public float getNextLink() {
        return nextLink;
    }

    public void setNextLink(float nextLink) {
        this.nextLink = nextLink;
    }

    public boolean equals(Object o)
    {
        if (o != null && o instanceof RouteLink)
        {
            RouteLink routeLink= (RouteLink) o;
            if (routeLink != null)  // Don't have to check linkId, it'll be checked (or exception thrown) outside of this class
                return linkId.equals(routeLink.getLinkId());
        }
        return false;
    }
    public GeoBoundingBox getBbox()
    {
        if (bbox == null)
            bbox = MapFactory.createGeoBoundingBox(MapFactory.createGeoCoordinate(bboxMaxLat, bboxMinLng),
                    MapFactory.createGeoCoordinate(bboxMinLat, bboxMaxLng));
        return bbox;
    }

    public int hashCode()
    {
        return linkId.hashCode();
    }

    //
    public String[] getShape() {
        return routeLinkShape;
    }

    public void setShape(String[] routeLinkShape) {
        this.routeLinkShape = routeLinkShape;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public void setFunctionalClass(int functionalClass){
        this.functionalClass = functionalClass;
    }

    public int getFunctionalClass(){
        return this.functionalClass;
    }
    public String getLinkId()
    {
        return linkId;
    }
    public void setLinkId(String linkId)
    {
        this.linkId= linkId;
    }
    public String getQuadkey()
    {
        return quadkey;
    }
    public void setQuadkey(String quadkey)
    {
        this.quadkey= quadkey;
    }
    public double getSpeedLimit()
    {
        return speedLimit;
    }
    public void setSpeedLimit(double speedLimit)
    {
        this.speedLimit= speedLimit;
    }
    public double getBboxMinLat()
    {
        return bboxMinLat;
    }
    public DynamicSpeedInfo getDynamicSpeedInfo() {
        if(dynamicSpeedInfo == null)
            dynamicSpeedInfo = new DynamicSpeedInfo();
        return dynamicSpeedInfo;
    }

    public void setDynamicSpeedInfo(DynamicSpeedInfo dynamicSpeedInfo) {
        this.dynamicSpeedInfo = dynamicSpeedInfo;
    }

    public void setAddressInfo(AddressInfo addressInfo){
        this.addressInfo = addressInfo;
    }

    public AddressInfo getAddressInfo(){
        if(addressInfo == null)
            addressInfo = new AddressInfo();
        return addressInfo;
    }

    public void setBboxMinLat(double bboxMinLat)
    {
        this.bboxMinLat= bboxMinLat;
    }
    public double getBboxMinLng()
    {
        return bboxMinLng;
    }
    public void setBboxMinLng(double bboxMinLng)
    {
        this.bboxMinLng= bboxMinLng;
    }
    public double getBboxMaxLat()
    {
        return bboxMaxLat;
    }
    public void setBboxMaxLat(double bboxMaxLat)
    {
        this.bboxMaxLat= bboxMaxLat;
    }
    public double getBboxMaxLng()
    {
        return bboxMaxLng;
    }
    public void setBboxMaxLng(double bboxMaxLng)
    {
        this.bboxMaxLng= bboxMaxLng;
    }

    public void setLineString(String[] routeLinkShape)
    {
        Coordinate[] coordinates = new Coordinate[routeLinkShape.length];

        for (int i = 0; i < routeLinkShape.length; i++)
        {
            String[] latLng = routeLinkShape[i].split(",");
            coordinates[i] = new Coordinate(Double.valueOf(latLng[1]), Double.valueOf(latLng[0]));
        }

        this.routelinkLineString = new GeometryFactory().createLineString(coordinates).toText();
    }

    public void setLineString(String routelinkLineString) {
        this.routelinkLineString = routelinkLineString;
    }

    public String getLineString() {

        if(this.routelinkLineString == null)
            setLineString(routeLinkShape);

        return this.routelinkLineString;
    }


    public LineString getShapeGeometry() {
        try {
            return (LineString) new WKTReader().read(getLineString());
        } catch (ParseException e) {
            return null;
        }
    }

    public GeoCoordinate[] toGeoCoords()
    {
        Coordinate[] coordinates = getShapeGeometry().getCoordinates();
        GeoCoordinate [] geoCoordinates = new GeoCoordinate[coordinates.length];
        Point routeLinkPoint;

        for (int i = 0; i < coordinates.length; i++)
        {
            routeLinkPoint = new GeometryFactory().createPoint(coordinates[i]);
            geoCoordinates[i] = MapFactory.createGeoCoordinate(routeLinkPoint.getY(), routeLinkPoint.getX());
        }

        return geoCoordinates;
    }

    public void setLocationDistance(double distance) {
        distanceFromCurrentLocation = distance;
    }

    public double getLocationDistance(){
        return distanceFromCurrentLocation;
    }

}

