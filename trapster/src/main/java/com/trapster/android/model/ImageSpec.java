package com.trapster.android.model;


import com.google.gson.annotations.SerializedName;

public class ImageSpec {

    @SerializedName("id")
	private int id;

    @SerializedName("width")
	private int width;

    @SerializedName("height")
	private int height;

    @SerializedName("pinx")
	private int pinx;

    @SerializedName("piny")
	private int piny;
	
	
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getPinx() {
		return pinx;
	}
	public void setPinx(int pinx) {
		this.pinx = pinx;
	}
	public int getPiny() {
		return piny;
	}
	public void setPiny(int piny) {
		this.piny = piny;
	} 
	
	
	
}
