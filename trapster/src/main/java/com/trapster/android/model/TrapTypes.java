package com.trapster.android.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TrapTypes {

    @SerializedName("traptype")
	private ArrayList<TrapType> types = new ArrayList<TrapType>();

	public void setTypes(ArrayList<TrapType> types) {
		this.types = types;
	}

	public ArrayList<TrapType> getTypes() {
		return types;
	}

	
	
}
