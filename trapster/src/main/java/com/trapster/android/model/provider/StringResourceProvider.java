package com.trapster.android.model.provider;

import android.content.*;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import com.trapster.android.model.StringResource;

public class StringResourceProvider extends ContentProvider {

	private static final String LOGNAME = "StringResourceProvider";

	private static final String DATABASE_NAME = "StringResources.db";
	private static final String DATABASE_TABLE = "Strings";

	private static final int DATABASE_VERSION = 2;

	// The index (key) column name for use in where clauses.
	private static final String KEY_ID = "_id";
	public static final String FIELD_KEY = "name";
	private static final String FIELD_TEXT = "text";

	// SQL Statement to create a new database.
	private static final String DATABASE_CREATE = "create table if not exists "
			+ DATABASE_TABLE + " (" + KEY_ID
			+ " INTEGER primary key autoincrement, " + FIELD_KEY
			+ " VARCHAR(100), " + FIELD_TEXT + " TEXT); ";

	public static final String PROVIDER_NAME = "com.trapster.android.stringresource";

	public static final Uri CONTENT_URI = Uri.parse("content://"
			+ PROVIDER_NAME + "/string");

	private static final int STRING = 1;

	private static final UriMatcher uriMatcher;
	static {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(PROVIDER_NAME, "string", STRING);
	}

	// Variable to hold the database instance
	private SQLiteDatabase db;
	private DbHelper dbHelper;

	@Override
	public boolean onCreate() {

		this.dbHelper = new DbHelper(getContext(), DATABASE_NAME, null,
				DATABASE_VERSION);
		try {
			db = dbHelper.getWritableDatabase();
		} catch (SQLiteException e) {
			db = null;
{/* 			//Log.d(LOGNAME, "Database Opening exception"); */}
		}

		return (db == null) ? false : true;
	}

	@Override
	public String getType(Uri uri) {
		switch (uriMatcher.match(uri)) {
		case STRING:
			return "vnd.android.cursor.dir/vnd.trapster.stringresource";
		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
	}

	@Override
	public int delete(Uri uri, String where, String[] whereArgs) {
		int count = 0;
		switch (uriMatcher.match(uri)) {
		case STRING:
			count = db.delete(DATABASE_TABLE, where, whereArgs);
			break;

		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}

		if (count > 0)
			getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		switch (uriMatcher.match(uri)) {
		case STRING:
			long rowId = -1;

			if (values.containsKey(KEY_ID)) {
				String where = KEY_ID + "=" + values.getAsInteger(KEY_ID);

				rowId = values.getAsInteger(KEY_ID);

				if (db.update(DATABASE_TABLE, values, where, null) == 0)
					db.insert(DATABASE_TABLE, "unknown", values);
			} else {
				rowId = db.insert(DATABASE_TABLE, "unknown", values);
			}

			if (rowId > 0) {
				Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowId);
				getContext().getContentResolver().notifyChange(uri, null);
				return _uri;
			}
			break;

		default:
			throw new SQLException("Failed to insert row, invalid URI " + uri);
		}

		// Return a URI to the newly inserted row on success.
		throw new SQLException("Failed to insert row into " + uri);
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String where,
			String[] whereArgs, String sort) {

		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		qb.setTables(DATABASE_TABLE);

		String orderBy = null;

		switch (uriMatcher.match(uri)) {
		case STRING:
			// add sort?
			break;
		default:
			throw new SQLException("Failed to insert row into " + uri);
		}
{/* 
		//Log.d(LOGNAME, "URI:" + uri.toString() + ":" + uriMatcher.match(uri)); */}
{/* 		//Log.d(LOGNAME,
				"Running:"
						+ qb.buildQueryString(false, qb.getTables(),
								projection, where, null, null, orderBy, null)); */}

		// Apply the query to the underlying database.
		Cursor c = qb.query(db, projection, where, whereArgs, null, null,
				orderBy);
{/* 
		//Log.d(LOGNAME, "Query results:" + c.getCount()); */}

		// Register the contexts ContentResolver to be notified if
		// the cursor result set changes.
		c.setNotificationUri(getContext().getContentResolver(), uri);

		// Return a cursor to the query result.
		return c;
	}

	@Override
	public int update(Uri uri, ContentValues values, String where,
			String[] whereArgs) {

		int count;
		switch (uriMatcher.match(uri)) {
		case STRING:
			count = db.update(DATABASE_TABLE, values, where, whereArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}

		if (count > 0)
			getContext().getContentResolver().notifyChange(uri, null);

		return count;
	}

	public static ContentValues toContentValues(StringResource r) {
		ContentValues v = new ContentValues();

		v.put(FIELD_KEY, r.getKey());
		v.put(FIELD_TEXT, r.getText());
		return v;
	}

	public static StringResource fromCursor(Cursor cursor) {

		int nameIndex = cursor.getColumnIndex(FIELD_KEY);
		int textIndex = cursor.getColumnIndex(FIELD_TEXT);

		int idIndex = cursor.getColumnIndex(KEY_ID);

		StringResource a = new StringResource();
		a.setId(cursor.getInt(idIndex));
		a.setKey(cursor.getString(nameIndex));
		a.setText(cursor.getString(textIndex));

		return a;
	}

	class DbHelper extends SQLiteOpenHelper {

		public DbHelper(Context context, String name, CursorFactory factory,
				int version) {
			super(context, name, factory, version);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(DATABASE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// Logger.w(LOGNAME, "Upgrading from version " + oldVersion + " to "
			// + newVersion + ", which will destroy all old data");
			db.execSQL("Drop TABLE IF EXISTS " + DATABASE_TABLE);
			onCreate(db);
		}

	}

}
