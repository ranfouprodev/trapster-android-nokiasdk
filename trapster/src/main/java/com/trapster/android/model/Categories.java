package com.trapster.android.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Categories {

    @SerializedName("trapcategory")
    private ArrayList<Category> categories = new ArrayList<Category>();

	public void setCategories(ArrayList<Category> categories) {
		this.categories = categories;
	}

	public ArrayList<Category> getCategories() {
		return categories;
	} 
	
	
}
