package com.trapster.android.model;

import com.google.gson.annotations.SerializedName;

public class Category{

    @SerializedName("id")
	private int id;

    @SerializedName("name")
	private String name;

    @SerializedName("sequence")
	int sequence;
	
	public Category(){ 
	}
	
	public Category(int categoryId, String categoryName, int sequence) {
		this.id = categoryId; 
		this.name = categoryName;
		this.sequence= sequence;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	} 
	public int getSequence() {
		return sequence;
	}
	public void setSequence(int sequence) {
		this.sequence= sequence;
	}

}
