package com.trapster.android.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SoundThemes {

    @SerializedName("removedtheme")
    private ArrayList<Integer> removed = new ArrayList<Integer>();;

    @SerializedName("addedtheme")
    private ArrayList<SoundTheme> added = new ArrayList<SoundTheme>();

    public ArrayList<Integer> getRemoved() {
        return removed;
    }

    public void setRemoved(ArrayList<Integer> removed) {
        this.removed = removed;
    }

    public ArrayList<SoundTheme> getAdded() {
        return added;
    }

    public void setAdded(ArrayList<SoundTheme> added) {
        this.added = added;
    }



}
