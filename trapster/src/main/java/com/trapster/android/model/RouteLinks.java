package com.trapster.android.model;

import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 10/24/13
 * Time: 2:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class RouteLinks {

    @SerializedName("Link")
    LinkedList<RouteLink> routeLinks = new LinkedList<RouteLink>();

    private transient double trafficTotalTime;

    private transient double baseTotalTime;


    public LinkedList<RouteLink> getRouteLinks() {
        return routeLinks;
    }

    public void setRouteLinks(LinkedList<RouteLink> routeLinks) {
        this.routeLinks = routeLinks;
    }


    public double getTrafficTotalTime() {
        double totaltime = 0;
        for(RouteLink routeLink:routeLinks){
            totaltime += routeLink.getDynamicSpeedInfo().getTrafficTime();
        }
        return totaltime;
    }

    public double getBaseTotalTime() {
        double totaltime = 0;
        for(RouteLink routeLink:routeLinks){
            totaltime += routeLink.getDynamicSpeedInfo().getBaseTime();
        }
        return totaltime;
    }
}

