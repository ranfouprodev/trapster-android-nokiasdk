package com.trapster.android.model;

import com.google.gson.annotations.SerializedName;

public class TrapLevel {
	
	private int id; 
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getTrapId() {
		return trapId;
	}
	public void setTrapId(int trapId) {
		this.trapId = trapId;
	}

	private int trapId;

    @SerializedName("bit")
    private int bit;

    @SerializedName("levelname")
	private String levelName; 

    @SerializedName("icon")
	private String icon;
	
	public int getBit() {
		return bit;
	}
	public void setBit(int bit) {
		this.bit = bit;
	}

	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}
	public String getLevelName() {
		return levelName;
	} 
	
	
}
