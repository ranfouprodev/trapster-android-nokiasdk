package com.trapster.android.model.dao;

import java.util.ArrayList;

import roboguice.inject.ContextSingleton;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.google.inject.Inject;
import com.trapster.android.model.ImageResource;
import com.trapster.android.model.TrapType;
import com.trapster.android.model.provider.ImageResourceProvider;
import com.trapster.android.model.provider.TrapTypeProvider;

@ContextSingleton
public class ImageResourceDAO  {

	@Inject
	Context context;
	
	public ImageResourceDAO() {
	}
	
	public ImageResource findByKey(String key){
		
		ImageResource ir = null;

		String where = ImageResourceProvider.FIELD_KEY+"=?";
		String[] whereArgs = new String[]{key};

		Cursor cursor = null;

		try {
			cursor = context.getContentResolver().query(ImageResourceProvider.CONTENT_URI,
					null, where, whereArgs, null);

			if (cursor != null && cursor.moveToFirst()) {

				ir = ImageResourceProvider.fromCursor(cursor);

			}
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return ir;
	}
	
	
	public void save(ImageResource r) {

		context.getContentResolver().insert(ImageResourceProvider.CONTENT_URI, ImageResourceProvider.toContentValues(r));

	}
	
	
	public boolean save(ArrayList<ImageResource> types, boolean isPreload) {
		// Bulk inserts
		ContentValues[] insertValues = new ContentValues[types.size()];
		int i = 0;

		for (ImageResource trap : types) {
			insertValues[i] = ImageResourceProvider.toContentValues(trap,isPreload);
			i++;
			
		}

		if (context.getContentResolver().bulkInsert(ImageResourceProvider.CONTENT_URI,
				insertValues) > 0){
		
			return true;
		}
			

		return false;

	}


}
