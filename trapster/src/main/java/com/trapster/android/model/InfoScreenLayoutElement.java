package com.trapster.android.model;

import java.io.Serializable;

public class InfoScreenLayoutElement implements Serializable
{
    private int layoutResource;
    private String mainText;
    private String subText;
    private int imageID;
    private String className;

    public int getLayoutType()
    {
        return layoutResource;
    }

    public void setLayoutType(int layoutType)
    {
        this.layoutResource = layoutType;
    }

    public String getMainText()
    {
        return mainText;
    }

    public void setMainText(String mainText)
    {
        this.mainText = mainText;
    }

    public String getSubText()
    {
        return subText;
    }

    public void setSubText(String subText)
    {
        this.subText = subText;
    }

    public int getImageID()
    {
        return imageID;
    }

    public void setImageID(int imageID)
    {
        this.imageID = imageID;
    }

    public String getClassName()
    {
        return className;
    }

    public void setClassName(String className)
    {
        this.className = className;
    }
}
