package com.trapster.android.model;

import com.trapster.android.util.GeographicUtils;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;

public class MapTileUpdateResource {

	private int id;

	private String layer;

	private long lastUpdate;

	private int zoom;

	private int tileX;

	private int tileY;

	public void setLastUpdate(long lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	private String reference;

	public static MapTileUpdateResource createFromPoint(String layer,
			double lat, double lon, int zoom) {
		MapTileUpdateResource mapTile = new MapTileUpdateResource();

		mapTile.layer = layer;
		mapTile.lastUpdate = System.currentTimeMillis();
		mapTile.zoom = zoom;
		mapTile.tileX = getXTile(lat, lon, zoom);
		mapTile.tileY = getYTile(lat, lon, zoom);
		return mapTile;
	}

	public MapTileUpdateResource() {
	}

	public MapTileUpdateResource(String layer, int x, int y, int zoom) {

		this.lastUpdate = System.currentTimeMillis();
		this.layer = layer;
		tileX = x;
		tileY = y;
		this.zoom = zoom;
	};

	public Envelope getBoundingBox() {
		return new Envelope(tile2lon(tileX, zoom), tile2lon(tileX + 1, zoom),
				tile2lat(tileY, zoom), tile2lat(tileY + 1, zoom));
	}

	public double getCenterLatitude() {
		return getBoundingBox().centre().y;
	}

	public double getCenterLongitude() {
		return getBoundingBox().centre().x;
	}

	public double getDiagonalRadiusForBoundingBox() {

		Envelope bbox = getBoundingBox();
		Coordinate center = bbox.centre();
		Coordinate corner = new Coordinate(bbox.getMaxX(), bbox.getMaxY());
		return GeographicUtils.geographicDistance(center.y, center.x, corner.y,
				corner.x);

	}

	public int getId() {
		return id;
	}

	public String getLayer() {
		return layer;
	}

	public long getLastUpdate() {
		return lastUpdate;
	}

	public int getZoom() {
		return zoom;
	}

	public int getTileX() {
		return tileX;
	}

	public int getTileY() {
		return tileY;
	}

	public String getReference() {
		return zoom + "-" + tileX + "-" + tileY;
	}


	private double tile2lon(int x, int z) {
		return x / Math.pow(2.0, z) * 360.0 - 180;
	}

	private double tile2lat(int y, int z) {
		double n = Math.PI - (2.0 * Math.PI * y) / Math.pow(2.0, z);
		return Math.toDegrees(Math.atan(Math.sinh(n)));
	}

	private static int getXTile(double lat, double lon, int zoom) {
		return (int) Math.floor((lon + 180) / 360 * (1 << zoom));
	}

	private static int getYTile(double lat, double lon, int zoom) {
		return (int) Math.floor((1 - Math.log(Math.tan(Math.toRadians(lat)) + 1
				/ Math.cos(Math.toRadians(lat)))
				/ Math.PI)
				/ 2 * (1 << zoom));
	}
}
