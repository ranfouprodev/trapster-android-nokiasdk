package com.trapster.android.model;

public class ResourceNotLoadedException extends Exception {

    public ResourceNotLoadedException() {
        // TODO Auto-generated constructor stub
    }

    public ResourceNotLoadedException(String detailMessage) {
        super(detailMessage);
        // TODO Auto-generated constructor stub
    }

    public ResourceNotLoadedException(Throwable throwable) {
        super(throwable);
        // TODO Auto-generated constructor stub
    }

    public ResourceNotLoadedException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
        // TODO Auto-generated constructor stub
    }

}
