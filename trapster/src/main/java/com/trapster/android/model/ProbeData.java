package com.trapster.android.model;

import android.location.Location;
import com.here.android.common.GeoCoordinate;
import com.here.android.common.GeoPosition;
import com.google.gson.annotations.SerializedName;
import com.trapster.android.RESTDefaults;



/*
    Sam - all variables not pushed up to the server must be declared transient!
 */
public class ProbeData {

    private final static String PROVIDER_NAME = "Trapster";

	private transient int id;

    @SerializedName(RESTDefaults.REST_PARAMATER_PROBE_TIME)
	private long timestamp;

    @SerializedName(RESTDefaults.REST_PARAMATER_PROBE_LATITUDE)
	private double latitude;

    @SerializedName(RESTDefaults.REST_PARAMATER_PROBE_LONGITUDE)
	private double longitude; 

    @SerializedName(RESTDefaults.REST_PARAMATER_PROBE_HORIZONTAL_ACCURACY)
	private double horizontalAccuracy;

    @SerializedName(RESTDefaults.REST_PARAMATER_PROBE_VERTICAL_ACCURACY)
    private double verticalAccuracy;

    @SerializedName(RESTDefaults.REST_PARAMATER_PROBE_ALTITUDE)
	private double altitude;

    @SerializedName(RESTDefaults.REST_PARAMATER_PROBE_SPEED)
	private float speed;

    @SerializedName(RESTDefaults.REST_PARAMATER_PROBE_HEADING)
	private float bearing;
	
	private transient String cellInformation;
	
	private transient String source;
	
	private transient String provider;
	
	//private transient String vehicleId;     //Not used, see Dave Carrol

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

/*	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}*/

	private transient boolean syncedWithServer;

	
	public ProbeData(){
		this.provider = PROVIDER_NAME;
		//this.vehicleId = String.valueOf(new Random().nextInt(10000000));
		this.syncedWithServer = false; 
	}

    public ProbeData(GeoPosition geoPosition)
    {
        GeoCoordinate coordinate = geoPosition.getCoordinate();
        this.provider = PROVIDER_NAME;
        this.syncedWithServer = false;

        this.altitude = coordinate.getAltitude();
        this.bearing = (float) geoPosition.getHeading();
        this.cellInformation = "";
        this.latitude = coordinate.getLatitude();
        this.longitude = coordinate.getLongitude();
        this.horizontalAccuracy = Math.sqrt(Math.pow(geoPosition.getLatitudeAccuracy(), 2) + Math.pow(geoPosition.getLongitudeAccuracy(), 2));
        this.source = "";
        this.speed = (float) geoPosition.getSpeed();
        this.timestamp = System.currentTimeMillis() / 1000L;
        this.verticalAccuracy = geoPosition.getAltitudeAccuracy();


    }
	
	public ProbeData(Location location){
		this.provider = PROVIDER_NAME;
		//this.vehicleId = String.valueOf(new Random().nextInt(10000000));
		this.syncedWithServer = false; 
		
		this.altitude = (location.hasAltitude())?location.getAltitude():-1;
		this.bearing = (location.hasBearing())?location.getBearing():-1;
		
		// @TODO not generated here
		this.cellInformation = "";
		
		this.horizontalAccuracy = location.getAccuracy();
		
		this.latitude = location.getLatitude(); 
		
		this.longitude = location.getLongitude(); 
		
		this.source = location.getProvider();
		
		this.speed = (location.hasSpeed())?location.getSpeed():-1;

        this.timestamp = System.currentTimeMillis() / 1000L;
        verticalAccuracy = -1;

	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getHorizontalAccuracy() {
		return horizontalAccuracy;
	}

	public void setHorizontalAccuracy(double horizontalAccuracy) {
		this.horizontalAccuracy = horizontalAccuracy;
	}

	public double getAltitude() {
		return altitude;
	}

	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	public float getBearing() {
		return bearing;
	}

	public void setBearing(float bearing) {
		this.bearing = bearing;
	}

	public String getCellInformation() {
		return cellInformation;
	}

	public void setCellInformation(String cellInformation) {
		this.cellInformation = cellInformation;
	}

	public String getProvider() {
		return provider;
	}

/*	public String getVehicleId() {
		return vehicleId;
	}*/

	public boolean isSyncedWithServer() {
		return syncedWithServer;
	}

	public void setSyncedWithServer(boolean syncedWithServer) {
		this.syncedWithServer = syncedWithServer;
	}


    public double getVerticalAccuracy()
    {
        return verticalAccuracy;
    }
}
