package com.trapster.android.model.provider.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import com.trapster.android.model.Formats;
import com.trapster.android.model.SoundLink;
import com.trapster.android.model.SoundTheme;
import com.trapster.android.model.SoundTheme.State;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class SoundThemeDatabaseAdapter {

    private static final String LOGNAME = "Trapster.SoundThemeDatabaseAdapter";

    private static final String DATABASE_NAME = "SoundTheme.db";

    private static final String DATABASE_TABLE = "SoundThemes";
    private static final String LINKS_DATABASE_TABLE = "SoundThemeLinks";
    private static final String RES_DATABASE_TABLE = "SoundThemeResource";

    private static final int DATABASE_VERSION = 4;

    // The index (key) column name for use in where clauses.
    private static final String KEY_ID = "_id";

    // Field indexes
    private static final String FIELD_ID = "theme_id";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_DESCRIPTION = "description";
    private static final String FIELD_STATE = "state";
    private static final String FIELD_AUTHOR = "author";
    private static final String FIELD_CREATED = "created";
    private static final String FIELD_UPDATED = "updated";
    private static final String FIELD_VERSION = "version";

    private static final String FIELD_LID = "lid";
    private static final String FIELD_PATH = "path";
    private static final String FIELD_AIFF = "aiff";
    private static final String FIELD_MP3 = "mp3";

    private static final String FIELD_RESOURCE_KEY = "name";
    private static final String FIELD_RESOURCE_VALUE = "res";

    // SQL Statement to create a new database.
    private static final String DATABASE_CREATE = "create table  "
            + DATABASE_TABLE + " (" + KEY_ID
            + " INTEGER primary key autoincrement, " + FIELD_ID + " INTEGER, "
            + FIELD_NAME + " VARCHAR(100), " + FIELD_DESCRIPTION
            + " VARCHAR(100), " + FIELD_STATE + " VARCHAR(40), " + FIELD_AUTHOR
            + " VARCHAR(100), " + FIELD_CREATED + " INTEGER, " + FIELD_UPDATED
            + " INTEGER, " + FIELD_VERSION + " VARCHAR(255));";

    private static final String LINKS_DATABASE_CREATE = "create table "
            + LINKS_DATABASE_TABLE + " (" + KEY_ID
            + " INTEGER primary key autoincrement, " + FIELD_ID + " INTEGER, "
            + FIELD_LID + " VARCHAR(100), " + FIELD_PATH + " VARCHAR(255), "
            + FIELD_AIFF + " VARCHAR(250), " + FIELD_MP3 + " VARCHAR(255));";

    private static final String RES_DATABASE_CREATE = "create table "
            + RES_DATABASE_TABLE + " (" + KEY_ID
            + " INTEGER primary key autoincrement, " + FIELD_ID + " INTEGER, "
            + FIELD_RESOURCE_KEY + " VARCHAR(100), " + FIELD_RESOURCE_VALUE + " VARCHAR(255));";


    // Variable to hold the database instance
    private SQLiteDatabase db;
    private DbHelper dbHelper;

    // Context of the application using the database.
    private Context context;

    public SoundThemeDatabaseAdapter(Context context) {
        this.context = context;
        this.dbHelper = new DbHelper(context, DATABASE_NAME, null,
                DATABASE_VERSION);
        open();
    }

    private void open() {
        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        db.close();
    }

    private HashMap<String, String> getResources(int themeId) {

        HashMap<String, String> links = new HashMap<String, String>();

        Cursor cursor = null;

        try {
            cursor = db.query(RES_DATABASE_TABLE, null, FIELD_ID + "=?",
                    new String[]{String.valueOf(themeId)}, null, null, null,
                    null);

            if (cursor != null && cursor.moveToFirst()) {

                do {

                    String key = cursor.getString(cursor.getColumnIndex(FIELD_RESOURCE_KEY));
                    String value = cursor.getString(cursor.getColumnIndex(FIELD_RESOURCE_VALUE));

                    links.put(key, value);
                } while (cursor.moveToNext());

            }

        } finally {
            if (cursor != null)
                cursor.close();
            // close();
        }

        return links;
    }

    private List<SoundLink> getSoundLinks(int themeId) {
        ArrayList<SoundLink> links = new ArrayList<SoundLink>();

        Cursor cursor = null;

        try {
            cursor = db.query(LINKS_DATABASE_TABLE, null, FIELD_ID + "=?",
                    new String[]{String.valueOf(themeId)}, null, null, null,
                    null);

            if (cursor != null && cursor.moveToFirst()) {

                do {

                    SoundLink link = new SoundLink();

                    link.setLid(cursor.getString(cursor
                            .getColumnIndex(FIELD_LID)));
                    link.setPath(cursor.getString(cursor
                            .getColumnIndex(FIELD_PATH)));

                    Formats format = new Formats();
                    format.setAiff(cursor.getString(cursor
                            .getColumnIndex(FIELD_AIFF)));
                    format.setMp3(cursor.getString(cursor
                            .getColumnIndex(FIELD_MP3)));

                    link.setFormats(format);

                    links.add(link);
                } while (cursor.moveToNext());

            }

        } finally {
            if (cursor != null)
                cursor.close();
            // close();
        }

        return links;
    }

    public ArrayList<SoundTheme> getSoundThemes() {

        long start = System.currentTimeMillis();

        ArrayList<SoundTheme> themes = new ArrayList<SoundTheme>();

        Cursor cursor = null;

        try {
            cursor = db.query(DATABASE_TABLE, null, null, null, null, null,
                    null, null);

            if (cursor != null && cursor.moveToFirst()) {

                do {
                    SoundTheme theme = new SoundTheme();

                    theme.setAuthor(cursor.getString(cursor
                            .getColumnIndex(FIELD_AUTHOR)));
                    theme.setDescription(cursor.getString(cursor
                            .getColumnIndex(FIELD_DESCRIPTION)));

                    int themeId = cursor
                            .getInt(cursor.getColumnIndex(FIELD_ID));
                    theme.setId(themeId);
                    theme.setName(cursor.getString(cursor
                            .getColumnIndex(FIELD_NAME)));
                    theme.setState(State.valueOf(cursor.getString(cursor
                            .getColumnIndex(FIELD_STATE))));
                    theme.setVersion(cursor.getString(cursor
                            .getColumnIndex(FIELD_VERSION)));

                    theme.setLinks((ArrayList<SoundLink>) getSoundLinks(themeId));

                    theme.setResource(getResources(themeId));

                    themes.add(theme);
                } while (cursor.moveToNext());

            }

        } finally {
            if (cursor != null)
                cursor.close();
            // close();
        }

//		Logger.d(LOGNAME, "Time to get themes:"+(System.currentTimeMillis()-start)+"ms");

        return themes;
    }

    public SoundTheme getSoundTheme(int themeId) {

        SoundTheme theme = null;

        Cursor cursor = null;

        try {
            cursor = db.query(DATABASE_TABLE, null, FIELD_ID + "=?",
                    new String[]{String.valueOf(themeId)}, null, null, null,
                    null);

            if (cursor != null && cursor.moveToFirst()) {

                theme = new SoundTheme();

                theme.setAuthor(cursor.getString(cursor
                        .getColumnIndex(FIELD_AUTHOR)));
                theme.setDescription(cursor.getString(cursor
                        .getColumnIndex(FIELD_DESCRIPTION)));

                theme.setId(themeId);
                theme.setName(cursor.getString(cursor
                        .getColumnIndex(FIELD_NAME)));
                theme.setState(State.valueOf(cursor.getString(cursor
                        .getColumnIndex(FIELD_STATE))));
                theme.setVersion(cursor.getString(cursor
                        .getColumnIndex(FIELD_VERSION)));

                theme.setLinks((ArrayList<SoundLink>) getSoundLinks(themeId));
                theme.setResource(getResources(themeId));

            }

        } finally {
            if (cursor != null)
                cursor.close();
            // close();
        }

        return theme;
    }

    public void addResource(int themeId, String key, String value) {
        ContentValues v = new ContentValues();
        v.put(FIELD_ID, themeId);
        v.put(FIELD_RESOURCE_KEY, key);
        v.put(FIELD_RESOURCE_VALUE, value);
        db.insert(RES_DATABASE_TABLE, null, v);
    }

    private void addResources(int themeId, HashMap<String, String> resource) {

        for (String key : resource.keySet()) {
            String value = resource.get(key);
            ContentValues v = new ContentValues();

            v.put(FIELD_ID, themeId);
            v.put(FIELD_RESOURCE_KEY, key);
            v.put(FIELD_RESOURCE_VALUE, value);

            db.insert(RES_DATABASE_TABLE, null, v);

        }


    }


    private void addSoundLink(int themeId, SoundLink link) {

        //Log.d(LOGNAME, "Adding SoundLink:"+link.getLid()+" to id:"+themeId);

        ContentValues v = new ContentValues();

        v.put(FIELD_ID, themeId);
        v.put(FIELD_LID, link.getLid());
        v.put(FIELD_PATH, link.getPath());
        v.put(FIELD_AIFF, link.getFormats().getAiff());
        v.put(FIELD_MP3, link.getFormats().getMp3());

        long key = db.insert(LINKS_DATABASE_TABLE, null, v);

    }

    private void add(SoundTheme theme) {

        //Log.d(LOGNAME, "Adding New Sound Theme:" + theme.getName());
        ContentValues v = new ContentValues();

        v.put(FIELD_ID, theme.getId());
        v.put(FIELD_AUTHOR, theme.getAuthor());
        v.put(FIELD_DESCRIPTION, theme.getDescription());
        v.put(FIELD_NAME, theme.getName());
        v.put(FIELD_VERSION, theme.getVersion());
        v.put(FIELD_STATE, theme.getState().toString());

        //db.insertWithOnConflict()
        long key = db.insert(DATABASE_TABLE, null, v);

        for (SoundLink link : theme.getLinks())
            addSoundLink(theme.getId(), link);

        addResources(theme.getId(), theme.getResource());

    }

    public void update(SoundTheme theme) {

//		Logger.i(LOGNAME, "Updating Sound Theme:" + theme.getName());
        ContentValues v = new ContentValues();

        // v.put(FIELD_ID, theme.getId());
        v.put(FIELD_AUTHOR, theme.getAuthor());
        v.put(FIELD_DESCRIPTION, theme.getDescription());
        v.put(FIELD_NAME, theme.getName());
        v.put(FIELD_VERSION, theme.getVersion());
        v.put(FIELD_STATE, theme.getState().toString());

        long key = db.update(DATABASE_TABLE, v, FIELD_ID + "=?",
                new String[]{String.valueOf(theme.getId())});

        for (SoundLink link : theme.getLinks())
            updateSoundLink(theme.getId(), link);

        updateResources(theme.getId(), theme.getResource());

    }

    private void updateResources(int themeId, HashMap<String, String> resource) {

        Iterator<String> iter = resource.keySet().iterator();

        while (iter.hasNext()) {
            String key = iter.next();
            String value = resource.get(key);
            ContentValues v = new ContentValues();

            //v.put(FIELD_RESOURCE_KEY, key);
            v.put(FIELD_RESOURCE_VALUE, value);

            db.update(RES_DATABASE_TABLE, v, FIELD_ID + "=? and " + FIELD_RESOURCE_KEY + "=?",
                    new String[]{String.valueOf(themeId), key});

        }

    }


    private void updateSoundLink(int themeId, SoundLink link) {

        ContentValues v = new ContentValues();

        //v.put(FIELD_LID, link.getLid());
        v.put(FIELD_PATH, link.getPath());
        v.put(FIELD_AIFF, link.getFormats().getAiff());
        v.put(FIELD_MP3, link.getFormats().getMp3());

        long key = db.update(LINKS_DATABASE_TABLE, v, FIELD_ID + "=? and " + FIELD_LID + " =?",
                new String[]{String.valueOf(themeId), link.getLid()});

    }

    public void setSoundThemes(ArrayList<SoundTheme> themes) {


        try {
            db.beginTransaction();

            deleteAllThemes();

            for (SoundTheme theme : themes)
                add(theme);


            db.setTransactionSuccessful();


        } finally {
            db.endTransaction();
        }

    }

    private void deleteAllThemes() {

        db.delete(DATABASE_TABLE, null, null);
        db.delete(LINKS_DATABASE_TABLE, null, null);

    }

    class DbHelper extends SQLiteOpenHelper {

        public DbHelper(Context context, String name, CursorFactory factory,
                        int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DATABASE_CREATE);
            db.execSQL(LINKS_DATABASE_CREATE);
            db.execSQL(RES_DATABASE_CREATE);

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//			Logger.w(LOGNAME, "Upgrading from version " + oldVersion + " to "
//					+ newVersion + ", which will destroy all old data");
            db.execSQL("Drop TABLE IF EXISTS " + DATABASE_TABLE);
            db.execSQL("Drop TABLE IF EXISTS " + LINKS_DATABASE_TABLE);
            db.execSQL("Drop TABLE IF EXISTS " + RES_DATABASE_TABLE);
            onCreate(db);
        }

    }
}
