package com.trapster.android.model.dao;

import roboguice.inject.ContextSingleton;
import android.content.Context;
import android.database.Cursor;

import com.google.inject.Inject;
import com.trapster.android.model.StringResource;
import com.trapster.android.model.provider.StringResourceProvider;
import com.trapster.android.model.provider.TrapProvider;

@ContextSingleton
public class StringResourceDAO {

	@Inject
	Context context;
	
	public StringResourceDAO() {
	}
	
	public StringResource findByKey(String key){
		
		StringResource ir = null;

		String where = StringResourceProvider.FIELD_KEY+"=?";
		String[] whereArgs = new String[]{key};

		Cursor cursor = null;

		try {
			cursor = context.getContentResolver().query(StringResourceProvider.CONTENT_URI,
					null, where, whereArgs, null);

			if (cursor != null && cursor.moveToFirst()) {

				ir = StringResourceProvider.fromCursor(cursor);

			}
		} finally {
			if (cursor != null)
				cursor.close();
		}

		return ir;
	}
	
	
	public void save(StringResource r) {

		context.getContentResolver().insert(StringResourceProvider.CONTENT_URI, StringResourceProvider.toContentValues(r));

	}
}
