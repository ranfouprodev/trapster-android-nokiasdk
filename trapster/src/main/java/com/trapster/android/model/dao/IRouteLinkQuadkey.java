package com.trapster.android.model.dao;

import java.util.ArrayList;

import com.trapster.android.model.RouteLinkQuadkey;


public interface IRouteLinkQuadkey {


	public abstract void addQuadkey(RouteLinkQuadkey routeLinkQuadkey);
	
	public abstract boolean hasQuadkey(String quadkey);

	public abstract void removeQuadkey(RouteLinkQuadkey routeLinkQuadkey);

	
	public abstract RouteLinkQuadkey getQuadkey(String quadkey);

	public abstract ArrayList<RouteLinkQuadkey> getQuadkeysBefore(long createDa);

}