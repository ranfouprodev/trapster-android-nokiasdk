package com.trapster.android.model.provider;

import android.content.*;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import com.trapster.android.model.UpdatedRouteLinkInfo;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 12/8/12
 * Time: 12:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class UpdatedRouteLinkProvider extends ContentProvider {
    private static final String LOGNAME = "UpdatedRouteLinkProvider";
    //
    static final String DATABASE_NAME= "RouteLinkExtraInfo.db";
    static final int DATABASE_VERSION= 4;
    // tables
    static final String DATABASE_TABLE= "RouteLinkExtraInformation";
    // fields
    public static final String ROUTE_LINK_ID= "link_id";
    static final String SELECTED_LAT= "selected_lat";
    static final String SELECTED_LNG= "selected_lng";
    static final String UPDATED_SPEED_LIMIT= "speed_limit";

    private static final String KEY_ID = "_id";

    private static final String DATABASE_CREATE = "create table  "
            + DATABASE_TABLE + " (" + KEY_ID
            + " INTEGER primary key autoincrement, " + ROUTE_LINK_ID + " TEXT, "
            + SELECTED_LAT + " REAL, " + SELECTED_LNG
            + " REAL, " + UPDATED_SPEED_LIMIT + " REAL);";

    public static final String PROVIDER_NAME = "com.trapster.android.updatedroutelinkinfo";

    public static final Uri CONTENT_URI = Uri.parse("content://"
            + PROVIDER_NAME + "/updatedroutelink");

    private static final int UPDATED_ROUTE_LINK = 1;

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "updatedroutelink", UPDATED_ROUTE_LINK);
    }

    // Variable to hold the database instance
    private SQLiteDatabase db;
    private DbHelper dbHelper;

    //
    static final String[] ROUTE_LINK_SELECT_COLUMNS= new String[] { "_id", ROUTE_LINK_ID, SELECTED_LAT,
            SELECTED_LNG, UPDATED_SPEED_LIMIT };
    //



    @Override
    public boolean onCreate() {
        this.dbHelper = new DbHelper(getContext(), DATABASE_NAME, null,
                DATABASE_VERSION);
        try {
            db = dbHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            db = null;
{/*             //Log.d(LOGNAME, "Database Opening exception"); */}
        }

        return (db == null) ? false : true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String where,
                        String[] whereArgs, String sort) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(DATABASE_TABLE);

        String orderBy = null;

        switch (uriMatcher.match(uri)) {
            case UPDATED_ROUTE_LINK:
                // add sort?
                break;
            default:
                throw new SQLException("Failed to insert row into " + uri);
        }
{/* 
        //Log.d(LOGNAME, "URI:" + uri.toString() + ":" + uriMatcher.match(uri)); */}
{/*         //Log.d(LOGNAME,
                "Running:"
                        + qb.buildQueryString(false, qb.getTables(),
                        projection, where, null, null, orderBy, null)); */}

        // Apply the query to the underlying database.
        Cursor c = qb.query(db, projection, where, whereArgs, null, null,
                orderBy);
{/* 
        //Log.d(LOGNAME, "Query results:" + c.getCount()); */}

        // Register the contexts ContentResolver to be notified if
        // the cursor result set changes.
        c.setNotificationUri(getContext().getContentResolver(), uri);

        // Return a cursor to the query result.
        return c;
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case UPDATED_ROUTE_LINK:
                return "vnd.android.cursor.dir/vnd.trapster.updatedroutelinkinfo";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        switch (uriMatcher.match(uri)) {
            case UPDATED_ROUTE_LINK:
                long rowId = -1;

                if (values.containsKey(KEY_ID)) {
                    String where = KEY_ID + "=" + values.getAsInteger(KEY_ID);

                    rowId = values.getAsInteger(KEY_ID);

                    if (db.update(DATABASE_TABLE, values, where, null) == 0)
                        db.insert(DATABASE_TABLE, "unknown", values);
                } else {
                    rowId = db.insert(DATABASE_TABLE, "unknown", values);
                }

                if (rowId > 0) {
                    Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowId);
                    getContext().getContentResolver().notifyChange(uri, null);
                    return _uri;
                }
                break;

            default:
                throw new SQLException("Failed to insert row, invalid URI " + uri);
        }

        // Return a URI to the newly inserted row on success.
        throw new SQLException("Failed to insert row into " + uri);
    }

    @Override
    public int delete(Uri uri, String where, String[] whereArgs) {
        int count = 0;
        switch (uriMatcher.match(uri)) {
            case UPDATED_ROUTE_LINK:
                count = db.delete(DATABASE_TABLE, where, whereArgs);
                break;

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        if (count > 0)
            getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String where, String[] whereArgs) {
        int count;
        switch (uriMatcher.match(uri)) {
            case UPDATED_ROUTE_LINK:
                count = db.update(DATABASE_TABLE, values, where, whereArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        if (count > 0)
            getContext().getContentResolver().notifyChange(uri, null);

        return count;

    }

    public static ContentValues toContentValues(UpdatedRouteLinkInfo updatedRouteLinkInfo)
    {
        ContentValues v = new ContentValues();

        v.put(ROUTE_LINK_ID, updatedRouteLinkInfo.getRouteLinkId());
        v.put(SELECTED_LAT, updatedRouteLinkInfo.getLat());
        v.put(SELECTED_LNG, updatedRouteLinkInfo.getLng());
        v.put(UPDATED_SPEED_LIMIT, updatedRouteLinkInfo.getSpeedLimit());

        return v;
    }

    public static UpdatedRouteLinkInfo fromCursor(Cursor cursor)
    {
        int routeLinkIndex =  cursor.getColumnIndex(ROUTE_LINK_ID);
        int routeLinkLatIndex = cursor.getColumnIndex(SELECTED_LAT);
        int routeLinkLngIndex = cursor.getColumnIndex(SELECTED_LNG);
        int routeLinkSpeedIndex = cursor.getColumnIndex(UPDATED_SPEED_LIMIT);

        UpdatedRouteLinkInfo updatedRouteLinkInfo = new UpdatedRouteLinkInfo();

        updatedRouteLinkInfo.setRouteLink(cursor.getString(routeLinkIndex));
        updatedRouteLinkInfo.setLat(cursor.getDouble(routeLinkLatIndex));
        updatedRouteLinkInfo.setLng(cursor.getDouble(routeLinkLngIndex));
        updatedRouteLinkInfo.setSpeedLimit(cursor.getDouble(routeLinkSpeedIndex));

        return updatedRouteLinkInfo;
    }

    static class DbHelper extends SQLiteOpenHelper
    {
        public DbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
        {
            super(context, name, factory, version);
        }
        public void onCreate(SQLiteDatabase db)
        {
            // RouteLinkExtraInfo table
            db.execSQL(DATABASE_CREATE);
        }
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            db.execSQL("drop table if exists " + DATABASE_TABLE);
            onCreate(db);
        }
    }
}
