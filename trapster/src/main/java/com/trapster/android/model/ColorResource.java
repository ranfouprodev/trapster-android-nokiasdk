package com.trapster.android.model;


public class ColorResource extends TrapsterResource {

	private int id; 
	
	private String key;
	
	private long timestamp;

	public void setKey(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	private String rgba;

	public void setRgba(String rgba) {
		this.rgba = rgba;
	}

	public String getRgba() {
		return rgba;
	} 
	
}
