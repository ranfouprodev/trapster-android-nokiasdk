package com.trapster.android.model.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.google.inject.Inject;
import com.trapster.android.model.TrapLevel;
import com.trapster.android.model.TrapType;
import com.trapster.android.model.provider.TrapTypeProvider;

import java.util.ArrayList;
import java.util.List;

import roboguice.inject.ContextSingleton;

@ContextSingleton
public class TrapTypeDAO {

	private static final String LOGNAME = "TrapTypeDAO";
	@Inject
	protected Context context;

	public TrapTypeDAO() {
	}

	public TrapType findById(int id) {

		TrapType type = null;

		Cursor cursor = null;

		try {
			cursor = context.getContentResolver().query(
					TrapTypeProvider.CONTENT_URI, null,
					TrapTypeProvider.FIELD_TRAP_ID + "=" + id, null, null);

			if (cursor != null && cursor.moveToFirst()) {
				type = TrapTypeProvider.trapTypefromCursor(cursor);

			}

		} finally {
			if (cursor != null)
				cursor.close();
		}

		if (type != null)
			type.setLevels(getTrapLevels(type.getId()));

		return type;

	}

	public ArrayList<TrapType> getTrapTypes() {

		ArrayList<TrapType> trapTypes = new ArrayList<TrapType>();
		Cursor cursor = null;

		try {
			cursor = context.getContentResolver().query(
					TrapTypeProvider.CONTENT_URI, null, null, null, null);

			if (cursor != null && cursor.moveToFirst()) {

				do {
					TrapType type = TrapTypeProvider.trapTypefromCursor(cursor);

                    type.setLevels(getTrapLevels(type.getId()));

					trapTypes.add(type);
				} while (cursor.moveToNext());

			}

		} finally {
			if (cursor != null)
				cursor.close();
		}

		for (TrapType type : trapTypes) {

			type.setLevels(getTrapLevels(type.getId()));
		}

       // if (trapTypes.size() > 0)
		    return trapTypes;
       // else
       //     return null;
	}

	private ArrayList<TrapLevel> getTrapLevels(int typeId) {

		ArrayList<TrapLevel> levels = new ArrayList<TrapLevel>();

		Cursor cursor = null;

		try {

			cursor = context.getContentResolver().query(
					TrapTypeProvider.LEVEL_CONTENT_URI, null,
					TrapTypeProvider.FIELD_TRAP_ID + "=?",
					new String[] { String.valueOf(typeId) }, null);

			if (cursor != null && cursor.moveToFirst()) {

				do {

					TrapLevel level = TrapTypeProvider
							.trapLevelfromCursor(cursor);
					levels.add(level);

				} while (cursor.moveToNext());

			}

		} finally {
			if (cursor != null)
				cursor.close();
		}

		return levels;

	}

	/**
	 * This will bulkInsert (or update) traptypes in the database. The content
	 * change broadcast will only happen after the last trap is added so it
	 * should be used on all bulk updates
	 * 
	 * @param types
	 * @return
	 */
	public synchronized boolean save(List<TrapType> types)
    {
       // Log.i("BBQ", "We have: "  + types.size() + " trap types to be inserted into the database.");
        int numUpdated = 0;
        int numInserts = 0;
        if (types != null && types.size() > 0)
        {
            //clear(); // Clears the table
            String where = TrapTypeProvider.FIELD_TRAP_ID + "=?";

            for (TrapType type : types) {
                ContentValues values = TrapTypeProvider.toContentValues(type);
                Cursor cursor = context.getContentResolver().query(  TrapTypeProvider.CONTENT_URI, null, where,  new String[] { String.valueOf(type.getId()) }, null);

                try {

                    if (cursor != null && cursor.moveToFirst())
                    {
                        TrapType oldType = TrapTypeProvider.trapTypefromCursor(cursor);
                        if (oldType.getId() == type.getId())
                        {
                            // Update the pre-existing trap type with the new data
                            context.getContentResolver().update(  TrapTypeProvider.CONTENT_URI, values, where,  new String[] { String.valueOf(type.getId()) });
                            numUpdated++;
                        }

                    }
                    else
                    {
                        context.getContentResolver().insert(TrapTypeProvider.CONTENT_URI, values);
                        numInserts++;
                    }

                } finally {
                    if (cursor != null)
                        cursor.close();
                }

            }

            saveLevels(types);
        }

        //Log.i("BBQ", "We inserted: "  +  numInserts + " trap types into the database.");
        //Log.i("BBQ", "We updated: "  + numUpdated + " trap types into the database.");


		return true;
	}

	public boolean save(TrapType type) {

		boolean added = false;

		try {

			if (context.getContentResolver().update(
					TrapTypeProvider.CONTENT_URI,
					TrapTypeProvider.toContentValues(type),
					TrapTypeProvider.FIELD_TRAP_ID + "=" + type.getId(), null) == 0) {

				if (context.getContentResolver().insert(
						TrapTypeProvider.CONTENT_URI,
						TrapTypeProvider.toContentValues(type)) != null)
					added = true;

			}
		} catch (Exception ex) {
{/* 			//Log.e(LOGNAME, "Unable to add record " + type.getId()
					+ " to database.Cause:" + Log.getStackTrace(ex)); */}
		}

		/*
		 * for (TrapLevel level : type.getLevels()) save(type.getId(), level);
		 */

		return added;
	}

	private void save(int trapId, TrapLevel level) {

		Uri insertUri = context.getContentResolver().insert(
				TrapTypeProvider.LEVEL_CONTENT_URI,
				TrapTypeProvider.toContentValues(trapId, level));

	}

	private boolean saveLevels(List<TrapType> types) {
		// Bulk inserts LEVEL_CONTENT_URI

		ArrayList<ContentValues> values = new ArrayList<ContentValues>();

		int i = 0;

		String where = TrapTypeProvider.FIELD_LEVEL_NAME + "=? AND "
				+ TrapTypeProvider.FIELD_TRAP_ID + "=?";

		for (TrapType type : types) {
			for (TrapLevel level : type.getLevels()) {

				 values.add(TrapTypeProvider.toContentValues(type.getId(),
				 level));
                /*ContentValues value = TrapTypeProvider.toContentValues(
                        type.getId(), level);
                Cursor cursor = context.getContentResolver().query(
                        TrapTypeProvider.LEVEL_CONTENT_URI,
                        null,
                        where,
                        new String[] { level.getLevelName(),
                                String.valueOf(type.getId()) }, null);

                try
                {

                    if (cursor != null && !cursor.moveToFirst()) {
                        context.getContentResolver().insert(
                                TrapTypeProvider.LEVEL_CONTENT_URI, value);
                    } else {
                        context.getContentResolver().update(
                                TrapTypeProvider.LEVEL_CONTENT_URI, value, where,
                                new String[] { level.getLevelName() });
				}
                }
                finally {
                    if (cursor != null)
                        cursor.close();
                }*/

			}
		}

        ContentValues[] insertValues = values.toArray(new ContentValues[i]);

        int count = context.getContentResolver().bulkInsert(TrapTypeProvider.LEVEL_CONTENT_URI, insertValues);

        if(count > 0){
            android.util.Log.e(LOGNAME,"Added "+count+" Levels");
            return true;
        }




		return false;

	}

	public void clear() {
		context.getContentResolver().delete(TrapTypeProvider.CONTENT_URI, null,
				null);
	}

}
