package com.trapster.android.model.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;


public class StatsProvider extends ContentProvider {

    private SQLiteDatabase db;

    public static final String PROVIDER_NAME = "com.trapster.android.stats";

    public static final Uri CONTENT_URI = Uri.parse("content://"
            + PROVIDER_NAME + "/stats");

    private static final String LOGNAME = "StatsProvider";
    private static final String DATABASE_NAME = "stats.db";
    private static final int DATABASE_VERSION = 7;
    private static final String STATS_TABLE = "stats";

    // Column Names
    public static final String KEY_ID = "_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_VALUE = "value";
    public static final String KEY_CAT = "category";
    public static final String KEY_UPDATE_TIME = "updatetime";

    // Create the constants used to differentiate between the different URI
    // requests.
    private static final int STATS = 1;
    private static final int STATS_ID = 2;

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "stats",
                STATS);
        uriMatcher.addURI(PROVIDER_NAME, "stats/*",
                STATS_ID);
    }

    @Override
    public boolean onCreate() {

        StatsDatabaseHelper dbHelper = new StatsDatabaseHelper(getContext(),
                DATABASE_NAME, null, DATABASE_VERSION);
        try {
            db = dbHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            db = null;
            //Logger.d(LOGNAME, "Database Opening exception:"+e.getMessage());
        }

        return (db == null) ? false : true;
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case STATS:
                return "vnd.android.cursor.dir/vnd.trapster.stats";
            case STATS_ID:
                return "vnd.android.cursor.item/vnd.trapster.stats";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sort) {

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(STATS_TABLE);

        String group = null;

        // If this is a row query, limit the result set to the passed in row.
        switch (uriMatcher.match(uri)) {
            case STATS_ID:
                qb.appendWhere(KEY_ID + "=" + uri.getPathSegments().get(1));
                break;
            default:
                break;
        }

        String orderBy = null;

        //Logger.d(LOGNAME,"URI:"+uri.toString()+":"+uriMatcher.match(uri));
        //Logger.d(LOGNAME, "Running:"+SQLiteQueryBuilder.buildQueryString(false, qb.getTables(), projection, selection, group, null, orderBy, null));

        // Apply the query to the underlying database.
        Cursor c = qb.query(db, projection, selection, selectionArgs,
                group, null, sort);

        //Logger.d(LOGNAME,"Query results:"+c.getCount());

        // Register the contexts ContentResolver to be notified if
        // the cursor result set changes.
        c.setNotificationUri(getContext().getContentResolver(), uri);

        // Return a cursor to the query result.
        return c;
    }

    @Override
    public Uri insert(Uri _uri, ContentValues _initialValues) {
        // Insert the new row, will return the row number if successful.
        long rowID = db.insert(STATS_TABLE, "nullhack", _initialValues);

        // Return a URI to the newly inserted row on success.
        if (rowID > 0) {
            Uri uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            getContext().getContentResolver().notifyChange(uri, null);
            return uri;
        }
        throw new SQLException("Failed to insert row into " + _uri);
    }

    @Override
    public int delete(Uri uri, String where, String[] whereArgs) {
        int count;

        switch (uriMatcher.match(uri)) {
            case STATS:
                count = db.delete(STATS_TABLE, where, whereArgs);
                break;

            case STATS_ID:
                String segment = uri.getPathSegments().get(1);
                count = db.delete(STATS_TABLE,
                        KEY_ID
                                + "="
                                + segment
                                + (!TextUtils.isEmpty(where) ? " AND (" + where
                                + ')' : ""), whereArgs);
                break;

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String where,
                      String[] whereArgs) {
        int count;
        switch (uriMatcher.match(uri)) {
            case STATS:
                count = db.update(STATS_TABLE, values, where, whereArgs);
                break;

            case STATS_ID:
                String segment = uri.getPathSegments().get(1);
                count = db.update(STATS_TABLE, values,
                        KEY_ID
                                + "="
                                + segment
                                + (!TextUtils.isEmpty(where) ? " AND (" + where
                                + ')' : ""), whereArgs);
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    // Helper class for opening, creating, and managing database version control
    private static class StatsDatabaseHelper extends SQLiteOpenHelper {
        private static final String DATABASE_CREATE = "create table "
                + STATS_TABLE + " (" + KEY_ID + " INTEGER primary key autoincrement, "
                + KEY_NAME + " TEXT, "
                + KEY_VALUE + " TEXT, "
                + KEY_CAT + " INT, "
                + KEY_UPDATE_TIME + " LONG); ";


        public StatsDatabaseHelper(Context context, String name,
                                   CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DATABASE_CREATE);
            //	Logger.d(LOGNAME, "Create:"+DATABASE_CREATE);

            insertDefaultStats(db);

        }

        /*
           * @TODO upgrading should not eliminate existing stats
           *
           * (non-Javadoc)
           * @see android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite.SQLiteDatabase, int, int)
           */
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            //	Logger.w(LOGNAME, "Upgrading database from version " + oldVersion + " to "
            //			+ newVersion + ", which will destroy all old data");

            db.execSQL("DROP TABLE IF EXISTS " + STATS_TABLE);
            onCreate(db);


        }

        private void insertDefaultStats(SQLiteDatabase db){
            db.beginTransaction();
            insertDefaultStat(db,StatsProviderUtil.STAT_VOTE,"0");
            insertDefaultStat(db,StatsProviderUtil.STAT_AVOIDED_TRAP,"0");
            insertDefaultStat(db,StatsProviderUtil.STAT_DISTANCE,"0.0");
            insertDefaultStat(db,StatsProviderUtil.STAT_REPORT,"0");
            insertDefaultStat(db,StatsProviderUtil.STAT_TRIP_AVGSPEED,"0.0");
            insertDefaultStat(db,StatsProviderUtil.STAT_TRIP_DISTANCE,"0.0");
            insertDefaultStat(db,StatsProviderUtil.STAT_TRIP_TIME,"0");

            db.setTransactionSuccessful();
            db.endTransaction();

        }

        private void insertDefaultStat(SQLiteDatabase db,String name, String value){


            ContentValues values = new ContentValues();
            values.put(StatsProvider.KEY_NAME,name);
            values.put(StatsProvider.KEY_VALUE,value);
            values.put(StatsProvider.KEY_CAT,StatsProviderUtil.getCategory(name));
            values.put(StatsProvider.KEY_UPDATE_TIME, System.currentTimeMillis());

            db.insert(STATS_TABLE, "null", values);

        }



    }


}
