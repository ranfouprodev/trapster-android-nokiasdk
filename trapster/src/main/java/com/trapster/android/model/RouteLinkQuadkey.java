
package com.trapster.android.model;

import java.sql.Timestamp;

public class RouteLinkQuadkey
{
    String quadkey;
    Timestamp createDa;
    //
    public String getQuadkey()
    {
        return quadkey;
    }
    public void setQuadkey(String quadkey)
    {
        this.quadkey= quadkey;
    }
    public Timestamp getCreateDa()
    {
        return createDa;
    }
    public void setCreateDa(Timestamp createDa)
    {
        this.createDa= createDa;
    }
}