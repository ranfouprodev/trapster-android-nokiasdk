package com.trapster.android.model;

import com.google.gson.annotations.SerializedName;

public class TrapIcon{
	
	private int id;

    @SerializedName("alt2")
	private String alt2;

    @SerializedName("alt1")
	private String alt1;

    @SerializedName("base")
	private String base;
	
	
	public String getAlt2() {
		return alt2;
	}
	public void setAlt2(String alt2) {
		this.alt2 = alt2;
	}
	public String getAlt1() {
		return alt1;
	}
	public void setAlt1(String alt1) {
		this.alt1 = alt1;
	}
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	} 
	
}
