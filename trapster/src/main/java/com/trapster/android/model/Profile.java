package com.trapster.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.trapster.android.RESTDefaults;

import java.util.Date;

public class Profile implements Parcelable {

    public Profile(){}

    @SerializedName(RESTDefaults.REST_PARAMETER_USER_NAME)
	private String name;

    @SerializedName(RESTDefaults.REST_PARAMETER_USER_EMAIL)
	private String email;

    @SerializedName(RESTDefaults.REST_PARAMETER_USER_SIGN_UP_DATE)
	private long signUpDate;

    @SerializedName(RESTDefaults.REST_PARAMETER_USER_KARMA)
	private int karma;

    @SerializedName(RESTDefaults.REST_PARAMETER_USER_TOTAL_USER_VOTES)
	private int numVotes;

    @SerializedName(RESTDefaults.REST_PARAMETER_USER_MOD_LEVEL)
    private String globalModerator;

    private Date signUp;

    private UserStatistics userStatistics;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getSignUp() {
		if(signUp != null)
            return signUp;
        else
        {
            setSignUp(new Date(signUpDate * 1000));
            return signUp;
        }
	}
	public void setSignUp(Date signUp) {
		this.signUp = signUp;
	}
	public int getKarma() {
		return karma;
	}
	public void setKarma(int karma) {
		this.karma = karma;
	} 
	public int getNumVotes() {
		return numVotes;
	}
	public void setNumVotes(int numVotes) {
		this.numVotes = numVotes;
	}

    public void setGlobalModerator(int globalModeratorInput)
    {
        this.globalModerator = String.valueOf(globalModeratorInput);
    }
    public int getGlobalModerator()
    {
        return Integer.valueOf(globalModerator);
    }

    public UserStatistics getUserStatistics()
    {
        if (userStatistics == null)
            userStatistics = new UserStatistics();
        return userStatistics;
    }

    public void setUserStatistics(UserStatistics userStatistics)
    {
        this.userStatistics = userStatistics;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeString(email);
        out.writeString(globalModerator);
        out.writeInt(karma);
        out.writeString(name);
        out.writeInt(numVotes);
        out.writeLong(signUpDate);
    }

    public transient static final Parcelable.Creator<Profile> CREATOR
            = new Parcelable.Creator<Profile>() {
        public Profile createFromParcel(Parcel in) {
            return new Profile(in);
        }

        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };
    private Profile(Parcel in){
        this.email = in.readString();
        this.globalModerator = in.readString();
        this.karma = in.readInt();
        this.name = in.readString();
        this.numVotes = in.readInt();
        this.signUpDate = in.readLong();
    }

    public String getFirstName() {

        if(name != null){
            String firstName = name.split(" ")[0];
            return firstName;
        }

        return null;

    }
}
