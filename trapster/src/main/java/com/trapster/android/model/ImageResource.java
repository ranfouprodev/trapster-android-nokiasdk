package com.trapster.android.model;


import com.google.gson.annotations.SerializedName;

public class ImageResource extends TrapsterResource{

	private transient int id;

    @SerializedName("id")
	private String key;

	private transient  long timestamp;

    @SerializedName("path")
	private String path;

    @SerializedName("spec")
	private ImageSpec spec;

    private transient String filename;
	
	private transient boolean preload;
	
	
	public boolean isPreload() {
		return preload;
	}

	public void setPreload(boolean preload) {
		this.preload = preload;
	}
	
	public void setKey(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}


	public ImageSpec getSpec() {
		return spec;
	}
	public void setSpec(ImageSpec spec) {
		this.spec = spec;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getPath() {
		return path;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(); 
		sb.append("key:"+key);
		sb.append("path:"+path);
		sb.append("timestamp:"+timestamp);
		sb.append("filename"+filename);
		
		return sb.toString();
	}

	
}
