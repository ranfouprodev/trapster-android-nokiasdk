package com.trapster.android.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class TrapLevels {

    @SerializedName("level")
	private List<TrapLevel> levels = new ArrayList<TrapLevel>();

    public List<TrapLevel> getLevels() {
        return levels;
    }

    public void setLevels(List<TrapLevel> levels) {
        this.levels = levels;
    }
}
