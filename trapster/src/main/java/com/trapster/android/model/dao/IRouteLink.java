package com.trapster.android.model.dao;

import java.util.ArrayList;

import com.trapster.android.model.RouteLink;
import com.vividsolutions.jts.geom.Envelope;

public interface IRouteLink {


	public abstract void bulkAddRouteLinks(String quadkey,
			RouteLink[] routeLinks);

	public abstract ArrayList<RouteLink> getOutboundRouteLinks(double lat,
			double lng);


	public abstract RouteLink getRouteLinkByLinkId(String linkId);

	public abstract ArrayList<RouteLink> getRouteLinksByBoundingBox(Envelope envelope);

	public abstract ArrayList<RouteLink> getRouteLinksByQuadkey(String quadkey);

	public abstract void removeRouteLinksByQuadkey(String quadkey);


}