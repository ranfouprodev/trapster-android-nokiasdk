package com.trapster.android.service.filter;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import com.google.inject.Inject;
import com.here.android.common.GeoPosition;
import com.trapster.android.Defaults;
import com.trapster.android.model.Trap;
import com.trapster.android.model.dao.TrapDAO;
import com.trapster.android.service.ConeFilterBroadcastReceiver;
import com.trapster.android.service.UserLocation;
import com.trapster.android.util.GeographicUtils;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;

import java.util.ArrayList;
import java.util.List;

public class ConeTrapFilter extends TrapFilter {

    private static final String LOGNAME = "Trapster.ConeTrapFilter";
    @Inject SharedPreferences settings;
    @Inject Application application;
    @Inject TrapDAO trapDAO;

    private double radiusConeSize;
    private double radiusLength;
    private GeoPosition userLocation;
    private ArrayList<Trap> activeTraps;
    private double userBearing;
    private final static double KM_PER_ENVELOPE_UNIT = 111.0; // ExpandBy(1.0) == 111 km

    private double CONE_ANGLE = 60.0;

    public ConeTrapFilter() {

    }

    @Inject
    public void Init(){


    }



    @Override
    public List<Trap> getTrapList(UserLocation location, List<Trap> currentTraps) {

        userLocation = location.getLocation();

        activeTraps = new ArrayList<Trap>();

        if(userLocation != null ){

            // Determine the cone based on speed
            long start = System.currentTimeMillis();
            calculateCone();
            long end = System.currentTimeMillis();
            //Log.i("BBQ", "calculate took : " + (end - start));

            // Determine the list of traps in the bbox
            start = System.currentTimeMillis();
            ArrayList<Trap> traps = getAllTrapsInRadius();
            end = System.currentTimeMillis();
            //Log.i("BBQ", "get traps took : " + (end - start));

            // check if they are in the cone

            start = end;
            for(Trap trap: traps){
                if(isTrapInCone(trap))
                    activeTraps.add(trap);
            }
            end = System.currentTimeMillis();
            //Log.i("BBQ", "cone check took : " + (end - start));
        }

        //broadcastConeFilter();

        return activeTraps;
    }

    private void broadcastConeFilter(){

        // Broadcast search values to TrapBroadcast Receiver
        Intent i = new Intent(ConeFilterBroadcastReceiver.INTENT_BROADCAST);
        i.putExtra(ConeFilterBroadcastReceiver.INTENT_ARC,(int)radiusConeSize);
        i.putExtra(ConeFilterBroadcastReceiver.INTENT_RADIUS,radiusLength);
        i.putExtra(ConeFilterBroadcastReceiver.INTENT_BEARING,(int)userBearing);
        application.getApplicationContext().sendBroadcast(i);
    }

    private ArrayList<Trap> getAllTrapsInRadius(){

        Coordinate coordinate = new Coordinate (userLocation.getCoordinate().getLongitude(),
                userLocation.getCoordinate().getLatitude());
        Envelope envelope = new Envelope(coordinate);

        double distance = settings.getFloat(Defaults.PREFERENCE_TRAP_ALERT_DISTANCE, (float)Defaults.DEFAULT_ALERT_DISTANCE);
        distance = distance / 10.0f; // The progress bar in AlertSettings works off whole numbers, despite representing a decimal.  We divide by 10 to get the correct value
        if (settings.getBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_MILES, true)) // True for miles, else we're already in KM
            distance = GeographicUtils.milesToKilometers(distance);

        distance = distance / KM_PER_ENVELOPE_UNIT;
        envelope.expandBy(distance);

        Geometry geometry = new GeometryFactory().toGeometry(envelope);

        return trapDAO.getTraps(geometry);
    }


    private void calculateCone(){
        userBearing = userLocation.getHeading()/360;

        // Distance notification
        double radius = settings.getFloat(Defaults.PREFERENCE_TRAP_ALERT_DISTANCE, (float) Defaults.DEFAULT_ALERT_DISTANCE) / 10.0;

        if(!settings.getBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_MILES, true))
            radius = GeographicUtils.milesToKilometers(radius);

        radius = radius * 1000; // to meters

        double maxSpeed = GeographicUtils.mphToMetersPerSecond(Defaults.DYNAMIC_MAX_SPEED);

        double speed;
        if (userLocation.getSpeed() > maxSpeed)
            speed = maxSpeed;
        else
            speed = userLocation.getSpeed();

        radiusConeSize = CONE_ANGLE;
        if (speed < 1)
            radiusLength = radius;
        else
        {
            double speedRatio = (maxSpeed - speed) / maxSpeed;
            // Scale out the radius size
            radiusLength = radius + speedRatio * (radius * Defaults.DYNAMIC_RADIUS_SCALE);
        }
    }

    private boolean isTrapInCone(Trap trap)
    {
        double deltaY = userLocation.getCoordinate().getLatitude() - trap.getLatitude();
        double deltaX = userLocation.getCoordinate().getLongitude() - trap.getLongitude();
        double angleBetweenUserAndTrap = Math.toDegrees(Math.atan2(deltaX, deltaY)) + 180;

        double userHeading = userLocation.getHeading();
        double minAngle = userHeading - (CONE_ANGLE / 2.0);
        double maxAngle = userHeading + (CONE_ANGLE / 2.0);

        return (minAngle <= angleBetweenUserAndTrap && angleBetweenUserAndTrap <= maxAngle);
    }
}
