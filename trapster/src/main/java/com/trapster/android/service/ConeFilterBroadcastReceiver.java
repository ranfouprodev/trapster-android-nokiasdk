package com.trapster.android.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


public class ConeFilterBroadcastReceiver extends BroadcastReceiver {

    public static final String INTENT_BROADCAST = "com.trapster.android.CONE_FILTER";

    private static final String LOGNAME = "Trapster.ConeFilterBroadcastReceiver";

    public static final String INTENT_RADIUS = "radius";

    public static final String INTENT_ARC = "arc";

    public static final String INTENT_BEARING = "bearing";

    private ConeFilterNotificationListener listener;

    public ConeFilterBroadcastReceiver(ConeFilterNotificationListener _listener){
        listener = _listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        //Logger.d(LOGNAME, "Received broadcast from cone filter");

        double radius = intent.getDoubleExtra(INTENT_RADIUS, 0.0f);
        int arc = intent.getIntExtra(INTENT_ARC, 0);
        int bearing = intent.getIntExtra(INTENT_BEARING, 0);

        if(listener!= null)
            listener.onConeFilterUpdate(arc, radius, bearing);


    }

}
