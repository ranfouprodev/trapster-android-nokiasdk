package com.trapster.android.service;

import android.content.Intent;

import com.google.inject.Inject;
import com.here.android.common.GeoCoordinate;
import com.trapster.android.comms.ITrapsterAPI;
import com.trapster.android.comms.PatrolListener;
import com.trapster.android.manager.PositionManager;
import com.trapster.android.model.MapTileUpdateResource;
import com.trapster.android.model.PatrolPath;
import com.trapster.android.model.PatrolPaths;
import com.trapster.android.model.dao.MapTileUpdateResourceDAO;
import com.trapster.android.model.dao.PatrolPathDAO;
import com.trapster.android.util.GeographicUtils;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;

import roboguice.service.RoboIntentService;

public class PatrolSyncService extends RoboIntentService {

	private static final String LOGNAME = "PatrolSyncService";

	public static final String EXTRA_LATITUDE = "patrol_extra_latitude";

	public static final String EXTRA_LONGITUDE = "patrol_extra_longitude";

	public static final String EXTRA_RADIUS = "patrol_extra_radius";

	public static final String EXTRA_FORCE = "patrol_extra_force";

	public static final double MAX_QUERY_RADIUS = 10; // This is the maximum query size available. 
	
	private static final int MAPTILE_CACHE_ZOOM_LEVEL = 13; // Works to roughly
															// 2 miles across
															// (http://msdn.microsoft.com/en-us/library/bb259689.aspx)
	private static final String MAPTILE_CACHE_LAYER = "patrol"; // Layer name for
																// db caching
	public static final int MAPTILE_EXPIRY_IN_SECONDS = 300; // Force the update every 5 min as per iOS

    private static final int PATROL_RADIUS = 5; //in miles

	@Inject PatrolPathDAO patrolPathDAO;

	@Inject
	MapTileUpdateResourceDAO mapTileUpdateResourceDao;

    @Inject PositionManager positionManager;

	@Inject
	ITrapsterAPI api;

	private PatrolDownloadListener patrolListener;

	public PatrolSyncService() {
		super(LOGNAME);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		if (intent != null && intent.hasExtra(EXTRA_LATITUDE)
				&& intent.hasExtra(EXTRA_LONGITUDE)
				&& intent.hasExtra(EXTRA_RADIUS)) {
			double latitude = intent.getDoubleExtra(EXTRA_LATITUDE, 0);
			double longitude = intent.getDoubleExtra(EXTRA_LONGITUDE, 0);
			double radius = intent.getDoubleExtra(EXTRA_RADIUS, 1);

			boolean force = intent.getBooleanExtra(EXTRA_FORCE, false);

			patrolListener = new PatrolDownloadListener();

			requestPatrolUpdate(latitude, longitude, radius, force);
			
			
		}
	}

	/**
	 * 
	 * Traps are updated based on a tile system. Given the coordinates and
	 * radius, we construct a list of tiles that should be updated. Those tiles
	 * are them compared against the maptile database to ensure we aren't
	 * requesting updates too quickly
	 * 
	 * 
	 * @param lat
	 * @param lon
	 * @param radiusInMiles
	 */
	public void requestPatrolUpdate(double lat, double lon, double radiusInMiles,
			boolean force) {

        boolean requestPatrol = false;
        MapTileUpdateResource mapTileResource = null;
	
		radiusInMiles = (radiusInMiles > MAX_QUERY_RADIUS) ? MAX_QUERY_RADIUS
				: radiusInMiles;
		
		float radiusInMeters = (float) (GeographicUtils
				.milesToKilometers(radiusInMiles) * 1000);

		double latSpan = GeographicUtils
				.calculateLatitudeSpanOnSameLongitudialLine(radiusInMeters);
		double lonSpan = GeographicUtils
				.calculateLongitudeSpanOnSameLatitudeLevel(radiusInMeters, lat,
						lon);

		double minLat = lat - latSpan;
		double maxLat = lat + latSpan;

		double minLon = lon - lonSpan;
		double maxLon = lon + lonSpan;

		MapTileUpdateResource topLeft = MapTileUpdateResource.createFromPoint(
				MAPTILE_CACHE_LAYER, maxLat, minLon, MAPTILE_CACHE_ZOOM_LEVEL);
		MapTileUpdateResource bottomRight = MapTileUpdateResource
				.createFromPoint(MAPTILE_CACHE_LAYER, minLat, maxLon,
						MAPTILE_CACHE_ZOOM_LEVEL);

		for (int x = topLeft.getTileX(); x <= bottomRight.getTileX(); x++)
        {
			for (int y = topLeft.getTileY(); y <= bottomRight.getTileY(); y++)
            {
				mapTileResource = mapTileUpdateResourceDao.getMapTileUpdate(MAPTILE_CACHE_LAYER, x, y,MAPTILE_CACHE_ZOOM_LEVEL);

				if (force)
                {
					mapTileResource = new MapTileUpdateResource(MAPTILE_CACHE_LAYER, x, y, MAPTILE_CACHE_ZOOM_LEVEL);
                    requestPatrol = true;
				}
                else
                {
					long lastUpdateTime = 0;
					if (mapTileResource != null)
                    {
						lastUpdateTime = mapTileResource.getLastUpdate();

						long delta = System.currentTimeMillis()	- lastUpdateTime;

						if (delta > (MAPTILE_EXPIRY_IN_SECONDS*1000))
                            requestPatrol = true;
					}
                    else
                    {
						mapTileResource = new MapTileUpdateResource(
								MAPTILE_CACHE_LAYER, x, y,
								MAPTILE_CACHE_ZOOM_LEVEL);
                        requestPatrol = true;
					}
				}

			}
		}
        if (requestPatrol && mapTileResource != null)
            queuePatrolRequest(mapTileResource);
	}

	private void queuePatrolRequest(MapTileUpdateResource resource) {

		// NOTE: current web service (alrtserv2) limits search to 10
		// Mile radius
		// tileRadiusInMiles must be slightly less than 10 Mile
		// limit
		// MAPTILE_CACHE_ZOOM_LEVEL must be set accordingly
		// radius should be slightly larger than half tile diagonal
		// to encompass entire tile
	    /*double tileRadiusInMiles = GeographicUtils.kilometersToMiles(resource
				.getDiagonalRadiusForBoundingBox() / 1000);
		
		api.getPath(resource.getCenterLatitude(),
				resource.getCenterLongitude(), tileRadiusInMiles, patrolListener, patrolListener);*/

        GeoCoordinate geoCoordinate = positionManager.getLastLocation().getCoordinate();
        api.getPath(geoCoordinate.getLatitude(), geoCoordinate.getLongitude(), PATROL_RADIUS, patrolListener, patrolListener);
		
	}

	
	
	
	
	private class PatrolDownloadListener implements CommunicationStatusListener,
			PatrolListener {

		@Override
		public void onOpenConnection() {
			//Log,.v(LOGNAME, "Downloading Patrol");
		}

		@Override
		public void onCloseConnection() {
			//Log,.v(LOGNAME, "Importing Patrol");
		}

		@Override
		public void onConnectionError(String errorMessage) {
{/* 			//Log.e(LOGNAME, "Connection Error downloading Traps:" + errorMessage); */}
		}

		@Override
		public void onError(TrapsterError error) {
{/* 			//Log.e(LOGNAME, "Error downloading Traps:" + error.getDetails()); */}

		}

		@Override
		public void onPatrolPath(PatrolPaths patrolPaths, double lat,
				double lon, double radius) {
				MapTileUpdateResource mt = MapTileUpdateResource.createFromPoint(MAPTILE_CACHE_LAYER,lat, lon, MAPTILE_CACHE_ZOOM_LEVEL);

	            mt.setLastUpdate(System.currentTimeMillis());
	            mapTileUpdateResourceDao.save(mt, System.currentTimeMillis());

	            patrolPathDAO.removePaths(mt);



                int ptcount = 0;
	            for(PatrolPath path:patrolPaths.getPaths()){

                    PatrolPath newPath = path.simplify();

                    ptcount += newPath.getPoints().size();
	                patrolPathDAO.save(mt, newPath);
	            }

            //Log.v(LOGNAME,"Loaded "+patrolPaths.getPaths().size()+" patrol lines totalling "+ptcount+" points");
        }


	}

}
