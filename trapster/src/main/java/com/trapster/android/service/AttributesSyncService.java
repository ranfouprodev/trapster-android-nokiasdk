package com.trapster.android.service;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.AssetManager;
import android.util.Log;

import com.google.inject.Inject;
import com.trapster.android.Defaults;
import com.trapster.android.comms.AttributesListener;
import com.trapster.android.comms.CategoryListener;
import com.trapster.android.comms.ITrapsterAPI;
import com.trapster.android.comms.TrapTypeListener;
import com.trapster.android.comms.php.AttributesParser;
import com.trapster.android.comms.php.CategoryParser;
import com.trapster.android.comms.php.TrapTypeParser;
import com.trapster.android.manager.BitmapCacheManager;
import com.trapster.android.manager.SoundThemeManager;
import com.trapster.android.manager.TrapManager;
import com.trapster.android.model.Categories;
import com.trapster.android.model.Category;
import com.trapster.android.model.ImageResource;
import com.trapster.android.model.StringResource;
import com.trapster.android.model.TrapType;
import com.trapster.android.model.TrapTypes;
import com.trapster.android.model.TrapsterResources;
import com.trapster.android.model.dao.CategoryDAO;
import com.trapster.android.model.dao.ImageResourceDAO;
import com.trapster.android.model.dao.StringResourceDAO;
import com.trapster.android.model.dao.TrapTypeDAO;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import roboguice.service.RoboIntentService;

/**
 * Used on startup (or whenever) to synchronize all the attributes for trap
 * types/sound themes/etc...
 * 
 * @author John
 * 
 */
public class AttributesSyncService extends RoboIntentService {

	private static final String LOGNAME = "AttributesSyncService";

	public static final String ATTRIBUTES_LOADED_LISTENER = "com.trapster.android.ATTRIBUTES_LOADED_LISTENER";

	// Sept 14 2012
	private static final long ATTRIBUTES_BUNDLE_TIMESTAMP = 1384386619L;

    private final String IMAGES_JSON = "{\"request\":null,\"response\":{\"message\":null,\"data\":{\"id\":null,\"string\":[],\"info\":null,\"displayname\":null,\"color\":[],\"image\":[{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/startmilestone/8460EB51-6697-408C-8C86-AAC7C20853C7.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":10,\"piny\":35},\"transcodedimage\":null,\"id\":\"startmilestone\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/milestone/A7C256B6-F487-44BF-82BB-0FDA635678C8.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":10,\"piny\":35},\"transcodedimage\":null,\"id\":\"milestone\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/endmilestone/FE0611DB-E9F0-4616-9923-C08203FAA036.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":10,\"piny\":35},\"transcodedimage\":null,\"id\":\"endmilestone\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/mediamilestone/B3AAC449-B086-41D7-8679-C65785BC44E6.png\",\"spec\":{\"width\":32,\"height\":32,\"pinx\":16,\"piny\":16},\"transcodedimage\":null,\"id\":\"mediamilestone\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/picturemilestone/145F2E2D-1FBC-4E10-A958-545185A2851E.png\",\"spec\":{\"width\":32,\"height\":32,\"pinx\":16,\"piny\":16},\"transcodedimage\":null,\"id\":\"picturemilestone\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/soundmilestone/2C4C4D7A-363C-4E7E-A0FA-A230595BE2E7.png\",\"spec\":{\"width\":32,\"height\":32,\"pinx\":16,\"piny\":16},\"transcodedimage\":null,\"id\":\"soundmilestone\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/textmilestone/51B4ECAA-94B7-4CD8-9530-31DE744E948D.png\",\"spec\":{\"width\":32,\"height\":32,\"pinx\":16,\"piny\":16},\"transcodedimage\":null,\"id\":\"textmilestone\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/searchresult/A28B44F7-9AAB-4A81-B63D-4239EA354C05.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":10,\"piny\":35},\"transcodedimage\":null,\"id\":\"searchresult\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/parenticon/D55A9C83-9C5C-4F5B-94F8-7F1517DF3C46.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":10,\"piny\":35},\"transcodedimage\":null,\"id\":\"parenticon\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/buddy/55ED1D80-7A16-49AF-8B8B-9E9328966A08.png\",\"spec\":{\"width\":32,\"height\":32,\"pinx\":16,\"piny\":16},\"transcodedimage\":null,\"id\":\"buddy\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/buddygroup/89573D9F-C0C2-4608-9DAC-CDBE3789E29C.png\",\"spec\":{\"width\":32,\"height\":32,\"pinx\":16,\"piny\":16},\"transcodedimage\":null,\"id\":\"buddygroup\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/unknownuser/A53B4FE6-5A4C-464F-81C9-DD66537A4A54.png\",\"spec\":{\"width\":32,\"height\":32,\"pinx\":16,\"piny\":16},\"transcodedimage\":null,\"id\":\"unknownuser\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/buddywhite/E7B961A4-011B-4E98-9278-E67B672799A5.png\",\"spec\":{\"width\":32,\"height\":32,\"pinx\":16,\"piny\":16},\"transcodedimage\":null,\"id\":\"buddywhite\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/compass/863614F8-4F7A-436A-8E62-9C7FC5BEE56C.png\",\"spec\":{\"width\":19,\"height\":27,\"pinx\":10,\"piny\":14},\"transcodedimage\":null,\"id\":\"compass\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/musicalnote/E1005D25-FCA4-4EAE-88C0-2F3CE3A5B14B.png\",\"spec\":{\"width\":29,\"height\":36,\"pinx\":15,\"piny\":18},\"transcodedimage\":null,\"id\":\"musicalnote\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/north/836BBE85-F752-445E-9B09-6EA90F66FF8B.png\",\"spec\":{\"width\":23,\"height\":32,\"pinx\":12,\"piny\":16},\"transcodedimage\":null,\"id\":\"north\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/paper/94B39E1B-0332-494C-A39C-FD178FA34EFD.png\",\"spec\":{\"width\":32,\"height\":32,\"pinx\":16,\"piny\":16},\"transcodedimage\":null,\"id\":\"paper\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/paperclip/4DC8DFC6-CB52-49D8-BA2F-0089D9FBA562.png\",\"spec\":{\"width\":32,\"height\":32,\"pinx\":16,\"piny\":16},\"transcodedimage\":null,\"id\":\"paperclip\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/paperclipwide/CE5D8DA1-C281-44D1-A016-1F5B3AD15D42.png\",\"spec\":{\"width\":49,\"height\":32,\"pinx\":25,\"piny\":16},\"transcodedimage\":null,\"id\":\"paperclipwide\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/paperwide/05847C58-ECFD-4E06-8861-64E463480AEC.png\",\"spec\":{\"width\":49,\"height\":32,\"pinx\":25,\"piny\":16},\"transcodedimage\":null,\"id\":\"paperwide\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/road/89B1386F-6E68-435A-8DA1-40C8A179CD04.png\",\"spec\":{\"width\":29,\"height\":32,\"pinx\":15,\"piny\":16},\"transcodedimage\":null,\"id\":\"road\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/trafficwhite/9389BEA7-27E5-43A2-8C4C-BFBB79A45C95.png\",\"spec\":{\"width\":29,\"height\":36,\"pinx\":15,\"piny\":18},\"transcodedimage\":null,\"id\":\"trafficwhite\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/headvoice/0145D12F-825E-4139-A264-F35E103F6293.png\",\"spec\":{\"width\":29,\"height\":28,\"pinx\":14,\"piny\":14},\"transcodedimage\":null,\"id\":\"headvoice\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/lp_green/953EFE01-C1A8-4FFF-A4FC-A07FF0D901F6.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"lp_green\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/lp_yellow/AC5D7BA9-A666-47B0-9575-2A8CBA89BCAB.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"lp_yellow\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/lp_red/7FF4F83A-02B0-4324-BCFE-0906159238FE.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"lp_red\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/sc_green/6E0F08C9-608A-4460-959A-F204451573ED.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"sc_green\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/sc_yellow/245C24A4-A8C4-4F4F-BAD0-2A522C4473D9.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"sc_yellow\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/sc_red/C5C4FB8F-4CFD-43B6-8A48-974C7DA1B9E4.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"sc_red\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/rlc_green/46476028-CE61-4B85-8BC6-B35A130CEA52.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"rlc_green\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/rlc_yellow/311D10BC-77D5-48CE-828C-F394B97078B2.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"rlc_yellow\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/rlc_red/B763624B-EABD-43DC-AF97-FF8F5BC2677E.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"rlc_red\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/pohh_green/22DE018F-6762-4E78-8198-FA6E6F350443.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"pohh_green\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/pohh_yellow/38AE5C04-4540-4C3A-93EB-B466231CBA60.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"pohh_yellow\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/pohh_red/C249088C-A922-4506-A01D-87CE68BBC084.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"pohh_red\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/msc_green/E3D539F6-B67C-46CE-BDCF-F5CE135ED644.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"msc_green\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/msc_yellow/CCEB7E52-B4F4-41A5-88B7-6AAFEEE3533D.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"msc_yellow\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/msc_red/9D22AF26-87D8-4D24-B04E-3661EE13393D.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"msc_red\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/cb_green/D1909FB2-7C16-4851-9FEA-21D3D04E9B38.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"cb_green\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/cb_yellow/0DABF870-AADB-42ED-B869-12FB0EFA3463.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"cb_yellow\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/cb_red/E49CF7D8-9603-4871-8F29-42BF41337C5E.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"cb_red\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ckp_green/9251AB5C-A210-4F33-BB6C-D965F68676A5.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"ckp_green\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ckp_yellow/46FEA6D0-8FBE-418B-8EB0-5E5D6CFAF71D.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"ckp_yellow\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ckp_red/667C6F43-6EE0-458F-8948-2B5610FD73A5.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"ckp_red\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/poi_bank/25B83958-617B-4A0D-B9E0-6C56394748CE.png\",\"spec\":{\"width\":30,\"height\":30,\"pinx\":15,\"piny\":15},\"transcodedimage\":null,\"id\":\"poi_bank\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/poi_firestation/03417D04-D0F0-4072-B197-ECEA92D4C720.png\",\"spec\":{\"width\":30,\"height\":30,\"pinx\":15,\"piny\":15},\"transcodedimage\":null,\"id\":\"poi_firestation\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/poi_police/06E5D655-3638-46AD-A58A-A1C557378361.png\",\"spec\":{\"width\":30,\"height\":30,\"pinx\":15,\"piny\":15},\"transcodedimage\":null,\"id\":\"poi_police\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/poi_restaurant/EECB3E19-6DCB-4B63-9161-31FADA964C9E.png\",\"spec\":{\"width\":30,\"height\":30,\"pinx\":15,\"piny\":15},\"transcodedimage\":null,\"id\":\"poi_restaurant\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/poi_park/128A13CC-6BEA-46F7-A9F7-EA735A692A57.png\",\"spec\":{\"width\":30,\"height\":30,\"pinx\":15,\"piny\":15},\"transcodedimage\":null,\"id\":\"poi_park\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/poi_gas/849FFDBA-3CBE-4F12-99FB-91205546ACBB.png\",\"spec\":{\"width\":30,\"height\":30,\"pinx\":15,\"piny\":15},\"transcodedimage\":null,\"id\":\"poi_gas\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/roadkill/50F6D9D7-6947-481E-8D4B-753AAECAD9D3.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"roadkill\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/childrenatplay/307D4997-0042-4C35-9546-A19141B1CB01.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"childrenatplay\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/accident/735DA4C8-E365-4CDE-ACF0-F4D9107F4B6E.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"accident\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/dangerouscurve/8583D4B7-592F-4F23-B679-D44F42B0C128.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"dangerouscurve\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/dangerintersect/C46F3655-F574-48FB-AC7B-91FF5E9F650E.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"dangerintersect\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/brushfire/E9F61456-6D54-4D5A-819D-36D6D89D2898.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"brushfire\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ice/0CC7CD69-BFEC-4401-BFF2-AD90CF19E810.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"ice\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/roadclosed/F4743C66-8FCB-4F76-AB4A-8232EE2C1B5D.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"roadclosed\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/flood/DE054486-126D-4D56-8482-A7EB5EF608CB.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"flood\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/narrowbridge/12B64D5B-29B0-4122-8DE0-8DF5DCA559ED.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"narrowbridge\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/tollbooth/F02C35C0-3FCD-4E68-B692-E73303544594.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"tollbooth\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/lp_burried/5D9C55BA-1175-494B-855F-DE35F6CBDB8A.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"lp_burried\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/lp_newbie/F3717206-D5E0-4E67-977F-D57206F1A502.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"lp_newbie\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/lp_quarantined/0C547E28-816B-40FC-B46D-AD911357DB7D.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"lp_quarantined\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/lp_probated/FA6E967E-ECB0-4131-AB04-0CFBA7563076.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"lp_probated\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/lp_scheduled/7303F1EF-F11D-4F90-AF7C-C6B572A93C96.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"lp_scheduled\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/lp_aged/299EBCF2-7DDA-45B1-8CEB-D1AC14F1ED93.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"lp_aged\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/rlc_burried/E2643FCB-FF2B-4861-B790-5CB93E1A4346.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"rlc_burried\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/rlc_newbie/5C6C8E58-0BA7-4E27-A3AB-3B3C910DBF56.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"rlc_newbie\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/rlc_quarantined/662D7501-97B2-4898-AB82-A59409D1AC1E.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"rlc_quarantined\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/rlc_probated/1A832F81-A83F-4B6E-9397-A8CC34EBA7D3.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"rlc_probated\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/rlc_pending/0C5ABF5D-74FA-4668-9798-977C717E966A.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"rlc_pending\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/rlc_scheduled/B9237E8B-4174-45AF-A287-005B97C8E644.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"rlc_scheduled\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/rlc_aged/4E2D1404-F607-4FB7-BDCE-CC0990C62388.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"rlc_aged\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/sc_burried/DE386AAA-2756-48B1-8F11-E4C45BD13072.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"sc_burried\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/sc_newbie/B6A16A68-0093-4018-A208-210D2B001A23.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"sc_newbie\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/sc_quarantined/E06BD55E-BFF4-4F6D-B4E9-C0A66EC2D863.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"sc_quarantined\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/sc_probated/52DDDD22-220B-4C8C-A5D8-84368E6F99BA.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"sc_probated\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/sc_pending/2E2F37AF-32E0-4BB0-86E4-695A2A42B86B.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"sc_pending\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/sc_scheduled/E996B796-2B28-4D20-96AC-E2E0D45C510A.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"sc_scheduled\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/sc_aged/8437FB99-5267-4C27-AD0D-A7807DE4C4F6.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"sc_aged\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/pohh_burried/D8818A7B-BD31-4B7B-B621-FB3022EB7A2B.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"pohh_burried\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/pohh_newbie/BA749FB5-958E-4BD9-878C-8E0D3473D84C.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"pohh_newbie\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/pohh_quarantined/CD5A86F1-DF7F-4881-8E23-306AD628EA10.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"pohh_quarantined\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/pohh_probated/10D40D86-3B06-43D3-AF83-6EA267EFF916.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"pohh_probated\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/pohh_pending/40899CB0-4472-47D9-A435-4F029F4321A4.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"pohh_pending\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/pohh_scheduled/09750826-1AB7-4817-8DAD-E40CD467C6E5.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"pohh_scheduled\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/pohh_aged/06CA2F1A-B75C-404F-942C-BCCC45429CC5.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"pohh_aged\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/cb_burried/B3654733-4CD3-40E4-94A7-4E227178F1D3.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"cb_burried\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/cb_newbie/3D17F71B-5CFC-4939-B057-657BF2E5D64C.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"cb_newbie\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/cb_quarantined/552102E0-A421-4CB4-A96C-6CD7149F93B3.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"cb_quarantined\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/cb_probated/B875A2AD-E520-4DEF-8FDA-580F80336DAB.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"cb_probated\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/cb_pending/5FBEBA28-18AB-4787-BC0A-A3A1D93A4C2E.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"cb_pending\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/cb_scheduled/945E529E-6EBC-4E3D-8694-6BCDB7309136.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"cb_scheduled\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/cb_aged/4E8AB26E-FAA8-41B5-B1A7-B01390A185F0.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"cb_aged\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/msc_burried/A7D1601B-BF9E-4F1B-B310-587DDA540D24.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"msc_burried\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/msc_newbie/33B207C6-BAEE-4C32-8363-6D13995B2A98.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"msc_newbie\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/msc_quarantined/332CE202-3852-4367-8BF4-EE39CF99F17F.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"msc_quarantined\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/msc_probated/1B496E5E-3143-4633-8FFC-0261231CF646.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"msc_probated\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/msc_scheduled/49D73F93-6153-40CF-B188-E696F1E74EC1.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"msc_scheduled\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/msc_aged/2A0A95F7-8491-4FDF-916B-F48F7E8FB02B.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"msc_aged\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ckp_burried/A1660EB3-E45D-4C28-8FAD-B77D427B9DC1.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"ckp_burried\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ckp_newbie/C6908D7E-8F6E-4781-8C62-6DDD3666D4A6.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"ckp_newbie\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ckp_quarantined/F2DF4E67-3824-47B4-BEAF-5D0C818BD738.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"ckp_quarantined\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ckp_probated/61B2972E-2EB4-482F-B155-6EA182122881.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"ckp_probated\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ckp_scheduled/EDD0094A-1046-4593-8E9D-C7E71E039D9E.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"ckp_scheduled\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ckp_aged/03BE1307-26DA-4814-9F96-1E5A396FDFD2.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"ckp_aged\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/construction/DB60CFCD-D69A-4557-8612-BB5881D876A1.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"construction\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/patrolicon/096F067B-7779-4DAF-B0D0-CAFB6C640D59.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":17},\"transcodedimage\":null,\"id\":\"patrolicon\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/soundattachment/12102715-EFF2-4FFB-9B39-DA706547C901.png\",\"spec\":{\"width\":50,\"height\":50,\"pinx\":25,\"piny\":25},\"transcodedimage\":null,\"id\":\"soundattachment\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/poi_starbucks/E671DF8B-F6C5-4068-BE21-94B4FF597AA4.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":17},\"transcodedimage\":null,\"id\":\"poi_starbucks\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/poi_restarea/E36C4503-35C1-41D3-B21E-20E5223A7F05.png\",\"spec\":{\"width\":30,\"height\":30,\"pinx\":15,\"piny\":15},\"transcodedimage\":null,\"id\":\"poi_restarea\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/mscosh_aged/D98E5338-1249-4A65-8741-D380224E0EC7.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"mscosh_aged\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/mscosh_burried/9FD3C611-DE92-45AE-A86B-B405F258E3DE.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"mscosh_burried\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/mscosh_green/7EF63C57-11AB-47B3-BE4D-40EFA2A9B2F1.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"mscosh_green\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/mscosh_yellow/42D962AD-1E30-454B-ABB9-0783C4192D21.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"mscosh_yellow\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/mscosh_red/A2CAFCAF-90B8-4570-8257-64E9A091F9C2.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"mscosh_red\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/mscosh_newbie/B6463999-CD3C-480D-827D-6F34976B7354.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"mscosh_newbie\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/mscosh_pending/D436B316-F8EB-4F33-9A81-04AA719B9D1E.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"mscosh_pending\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/mscosh_probated/C9F496DD-3273-46A3-9574-7527A493F895.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"mscosh_probated\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/mscosh_quarantined/777487DB-0C77-49D0-92B8-DAAE40C0C3A9.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"mscosh_quarantined\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/mscosh_scheduled/FC899C88-BEC7-424B-8514-F9933D2D5652.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"mscosh_scheduled\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/badge_travis_county/54A48D6F-AE5C-47AD-B1C9-6E55FCBEF861.png\",\"spec\":{\"width\":200,\"height\":200,\"pinx\":100,\"piny\":200},\"transcodedimage\":null,\"id\":\"badge_travis_county\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/badge_moderator/D4E2BE63-F9E7-4709-B307-6D855A336BE2.png\",\"spec\":{\"width\":200,\"height\":200,\"pinx\":100,\"piny\":200},\"transcodedimage\":null,\"id\":\"badge_moderator\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/vote_tab_icon/ACE1EDA4-C991-4558-BADD-AA5578C3AAD5.png\",\"spec\":{\"width\":36,\"height\":36,\"pinx\":18,\"piny\":18},\"transcodedimage\":null,\"id\":\"vote_tab_icon\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/gold_badge_icon/7F91796F-3C05-4371-9A75-41FB84A15F6E.png\",\"spec\":{\"width\":30,\"height\":30,\"pinx\":15,\"piny\":15},\"transcodedimage\":null,\"id\":\"gold_badge_icon\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/trip_icon/91D2A67D-58D9-4301-BED2-CF57A12B243A.png\",\"spec\":{\"width\":32,\"height\":32,\"pinx\":16,\"piny\":16},\"transcodedimage\":null,\"id\":\"trip_icon\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/filtericon/4614A0E8-4CB3-4739-987D-1609FD203B49.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":17},\"transcodedimage\":null,\"id\":\"filtericon\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/badge_tiden_PA/B2C276FC-C330-4A25-B52E-630D0BA2B6E7.png\",\"spec\":{\"width\":200,\"height\":200,\"pinx\":100,\"piny\":200},\"transcodedimage\":null,\"id\":\"badge_tiden_PA\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/badge_berks_PA/C0F38901-8A21-4710-9F38-F1A813FF0CA8.png\",\"spec\":{\"width\":200,\"height\":200,\"pinx\":100,\"piny\":200},\"transcodedimage\":null,\"id\":\"badge_berks_PA\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/badge_travis_tom_TX/6E18B247-310E-42FF-A6C7-E8C6826DAB82.png\",\"spec\":{\"width\":200,\"height\":200,\"pinx\":100,\"piny\":200},\"transcodedimage\":null,\"id\":\"badge_travis_tom_TX\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/mag_glass/5AC1A706-8285-492F-BEDF-8A07326981DA.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":17},\"transcodedimage\":null,\"id\":\"mag_glass\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/schoolzone/4EFDD2CB-523D-44A0-ABCD-09AC693EE8A6.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"schoolzone\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/gears_icon/CCE8D93E-2FE0-401C-BC23-CEC7506B3C14.png\",\"spec\":{\"width\":29,\"height\":29,\"pinx\":14,\"piny\":14},\"transcodedimage\":null,\"id\":\"gears_icon\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/badge_cincinnati_OH/B07998A1-5259-4C28-8AFB-A48AFE843903.png\",\"spec\":{\"width\":200,\"height\":200,\"pinx\":100,\"piny\":200},\"transcodedimage\":null,\"id\":\"badge_cincinnati_OH\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/MCRRSecretCheckpoint/59C953B8-0ECE-4ADF-B1AE-0DD6282DD413.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":17},\"transcodedimage\":null,\"id\":\"MCRRSecretCheckpoint\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/badge_mcrr_s_ckpt/C43580CE-D535-4DA8-A18B-4A41FC0EEB50.png\",\"spec\":{\"width\":200,\"height\":200,\"pinx\":100,\"piny\":200},\"transcodedimage\":null,\"id\":\"badge_mcrr_s_ckpt\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/badge_mcrr_ckpt/77126156-F904-4E43-909D-3496FF92BB3B.png\",\"spec\":{\"width\":200,\"height\":200,\"pinx\":100,\"piny\":200},\"transcodedimage\":null,\"id\":\"badge_mcrr_ckpt\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/MCRRCheckpoint/0BA49CBE-E463-48B7-9F87-9DBCFB847621.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":17},\"transcodedimage\":null,\"id\":\"MCRRCheckpoint\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/HauntedHouse/5DDDD973-EA31-4161-8B44-B66C90DB02E9.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"HauntedHouse\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/TrickOrTreatZone/59BB91EE-DCB2-4ECE-9203-2137B8FAA62F.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"TrickOrTreatZone\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/roadhazard/39782713-487D-4070-9D76-11273B3A609C.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"roadhazard\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ev_charging_station/1BBFE8C9-E826-4F8E-81F2-65562DEC822E.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"ev_charging_station\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/speed_limit_mph/EB1634E2-3D7D-4420-81BF-9C12F25CDF78.png\",\"spec\":{\"width\":35,\"height\":41,\"pinx\":17,\"piny\":20},\"transcodedimage\":null,\"id\":\"speed_limit_mph\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/speed_limit_kmh/31FEAC61-4A93-436A-AA72-C285BCB22F91.png\",\"spec\":{\"width\":35,\"height\":41,\"pinx\":17,\"piny\":20},\"transcodedimage\":null,\"id\":\"speed_limit_kmh\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/my_speed_mph/0694F80D-6DDF-48A0-9853-7CBAA1D8BD51.png\",\"spec\":{\"width\":35,\"height\":41,\"pinx\":17,\"piny\":20},\"transcodedimage\":null,\"id\":\"my_speed_mph\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/my_speed_kmh/51B63E97-853B-4455-8BF8-1EE7CD7E523D.png\",\"spec\":{\"width\":35,\"height\":41,\"pinx\":17,\"piny\":20},\"transcodedimage\":null,\"id\":\"my_speed_kmh\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/poi_coffee/6EFC2118-7D1F-41A7-B172-480D50840041.png\",\"spec\":{\"width\":33,\"height\":33,\"pinx\":16,\"piny\":16},\"transcodedimage\":null,\"id\":\"poi_coffee\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/traffic_jam/629495C3-A005-43E1-B03A-9065D96E4D82.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"traffic_jam\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ascse_aged/DF18B7BC-280F-4F08-B758-BF16B66412FC.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"ascse_aged\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ascse_buried/0E7086D9-C6C8-47EA-9AE1-FDB15AD6E3DF.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"ascse_buried\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ascse_green/F2C53297-F678-4E65-9CE2-AD8E7F678164.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"ascse_green\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ascse_newbie/D64DF926-BF57-4DAB-9893-FEC79BC4E340.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"ascse_newbie\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ascse_pending/D0F55E8E-41AE-4ED2-B20A-25E01F2C3794.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"ascse_pending\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ascse_probated/3A3A8058-F004-4A04-A010-51DC25A22E2F.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":11,\"piny\":35},\"transcodedimage\":null,\"id\":\"ascse_probated\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ascse_quarantined/BA23991D-775B-4623-8BF8-35663A20BA58.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"ascse_quarantined\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ascse_red/C6CED0BD-5CE8-4338-942E-9958D41BC6D4.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"ascse_red\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ascse_scheduled/DB468E57-3E18-438A-8CDD-70C792DF97D4.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"ascse_scheduled\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ascse_yellow/C009010F-6C0D-4330-83D9-35F878F652DE.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"ascse_yellow\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ascss_aged/3CF00728-3743-4BDB-9062-3FF6AD78D18A.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"ascss_aged\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ascss_buried/44EF0673-3D0B-4EF1-9627-9CE44C9832FA.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"ascss_buried\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ascss_green/C5E6E07B-4698-420D-A245-DB042ADAF564.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"ascss_green\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ascss_newbie/B551EE7C-D4C1-450B-86CF-F42175C23F6D.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"ascss_newbie\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ascss_pending/44AB54CA-22DC-4448-9937-8276FC7D29D6.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"ascss_pending\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ascss_probated/6262EF19-C322-4063-B4CD-2C1575D8479C.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"ascss_probated\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ascss_quarantined/E2ABA247-4FB0-47DF-9C52-7F050305567B.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"ascss_quarantined\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ascss_red/B073ABF2-088C-4C99-B448-77868F6212E6.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"ascss_red\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ascss_scheduled/A82093B9-FEF6-4D6D-B9D4-CA37BE25A927.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"ascss_scheduled\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/ascss_yellow/49B43D45-8070-422E-9CBB-5DE218AA5169.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"ascss_yellow\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/dz_2km/619C3ECB-506F-4F30-92DE-013F799B7273.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"dz_2km\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/dz_300m/BEAB67B9-8F1B-4852-A508-088BF82270DB.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"dz_300m\"},{\"path\":\"http://www.trapster.com/storage/APP_ATTRIBUTE/dz_4km/24F2515E-0C33-43F4-ACDB-B82703155108.png\",\"spec\":{\"width\":35,\"height\":35,\"pinx\":17,\"piny\":35},\"transcodedimage\":null,\"id\":\"dz_4km\"}],\"addedtheme\":[],\"addedpoi\":[],\"removedpoi\":[],\"poiTopics\":[],\"removed\":[],\"removedtheme\":[],\"trapcategory\":[],\"traptype\":[],\"alert\":null,\"marker\":[],\"added\":[],\"emailaddr\":null,\"pwdusernamehash\":null,\"mobilepwdhash\":null,\"usercredlevel\":null,\"alertradius\":null,\"signupepochtime\":null,\"lastvoteepochtime\":null,\"totaluservotes\":null,\"karma\":null,\"tosagreed\":null,\"trustedgrouppref\":null,\"privmsgpref\":null,\"smsaddr\":null,\"moderatorlevel\":null,\"loggedontositeflag\":null,\"newsletterflag\":null,\"emailconfflag\":null,\"userstatus\":null,\"lastkarma\":null,\"sipallowedflag\":null,\"loginfailures\":null,\"modapplyepochtime\":null,\"lastapiepochtime\":null,\"lastwebepochtime\":null,\"confirmepochtime\":null,\"confirmmethod\":null,\"facebook\":null,\"twitter\":null,\"twitterenabled\":null,\"facebookenabled\":null,\"tripSessionId\":null,\"patrollines\":[]},\"status\":\"OK\",\"asofepochtime\":1384394376,\"statuscode\":200}}";

    private final String TRAPCATEGORY_JSON = "{ \"request\": null, \"response\": { \"message\": null, \"data\": { \"id\": null, \"string\": [], \"info\": null, \"displayname\": null, \"color\": [], \"image\": [], \"addedtheme\": [], \"addedpoi\": [], \"removedpoi\": [], \"poiTopics\": [], \"removed\": [], \"removedtheme\": [], \"trapcategory\": [ { \"name\": \"Enforcement Points\", \"sequence\": 2, \"id\": \"1\" }, { \"name\": \"Road Hazards\", \"sequence\": 3, \"id\": \"2\" }, { \"name\": \"Most Popular\", \"sequence\": 1, \"id\": \"3\" }, { \"name\": \"EV Charging Stations\", \"sequence\": 6, \"id\": \"5\" } ], \"traptype\": [], \"alert\": null, \"marker\": [], \"added\": [], \"emailaddr\": null, \"pwdusernamehash\": null, \"mobilepwdhash\": null, \"usercredlevel\": null, \"alertradius\": null, \"signupepochtime\": null, \"lastvoteepochtime\": null, \"totaluservotes\": null, \"karma\": null, \"tosagreed\": null, \"trustedgrouppref\": null, \"privmsgpref\": null, \"smsaddr\": null, \"moderatorlevel\": null, \"loggedontositeflag\": null, \"newsletterflag\": null, \"emailconfflag\": null, \"userstatus\": null, \"lastkarma\": null, \"sipallowedflag\": null, \"loginfailures\": null, \"modapplyepochtime\": null, \"lastapiepochtime\": null, \"lastwebepochtime\": null, \"confirmepochtime\": null, \"confirmmethod\": null, \"facebook\": null, \"twitter\": null, \"twitterenabled\": null, \"facebookenabled\": null, \"tripSessionId\": null, \"patrollines\": [] }, \"status\": \"OK\", \"asofepochtime\": 1375741610, \"statuscode\": 200 } }";

    private final String TRAPTYPE_JSON = "{\"request\":null,\"response\":{\"message\":null,\"data\":{\"id\":null,\"string\":[],\"info\":null,\"displayname\":null,\"color\":[],\"image\":[],\"addedtheme\":[],\"addedpoi\":[],\"removedpoi\":[],\"poiTopics\":[],\"removed\":[],\"removedtheme\":[],\"trapcategory\":[],\"traptype\":[{\"name\":\"Live Police\",\"description\":\"Police speed trap currently observing traffic.\",\"category\":3,\"audioid\":\"lp\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"Y\",\"icons\":{\"alt2\":\"lp_red\",\"alt1\":\"lp_yellow\",\"base\":\"lp_green\"},\"lifetime\":3600,\"levels\":{\"level\":[{\"icon\":\"lp_red\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"lp_red\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"lp_green\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"lp_yellow\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"lp_red\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"0\"},{\"name\":\"Red Light Camera\",\"description\":\"Red light camera. Be careful of surveillance cameras that look similar!\",\"category\":1,\"audioid\":\"rlc\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"N\",\"icons\":{\"alt2\":\"rlc_red\",\"alt1\":\"rlc_yellow\",\"base\":\"rlc_green\"},\"lifetime\":0,\"levels\":{\"level\":[{\"icon\":\"rlc_red\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"rlc_red\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"rlc_green\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"rlc_yellow\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"rlc_red\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"1\"},{\"name\":\"Fixed Speed Camera\",\"description\":\"Speed camera. Typically roadside on highways/speedways.\",\"category\":1,\"audioid\":\"sc\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"N\",\"icons\":{\"alt2\":\"sc_red\",\"alt1\":\"sc_yellow\",\"base\":\"sc_green\"},\"lifetime\":0,\"levels\":{\"level\":[{\"icon\":\"sc_red\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"sc_red\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"sc_green\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"sc_yellow\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"sc_red\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"2\"},{\"name\":\"Known Enforcement Point\",\"description\":\"Where police are often seen enforcing traffic with laser or radar.\",\"category\":1,\"audioid\":\"hp\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"N\",\"notifiable\":\"N\",\"icons\":{\"alt2\":\"pohh_red\",\"alt1\":\"pohh_yellow\",\"base\":\"pohh_green\"},\"lifetime\":0,\"levels\":{\"level\":[{\"icon\":\"pohh_red\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"pohh_red\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"pohh_green\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"pohh_yellow\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"pohh_red\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"3\"},{\"name\":\"Combo Camera\",\"description\":\"A combined red light and speed photo enforcement camera.\",\"category\":1,\"audioid\":\"cb\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"N\",\"icons\":{\"alt2\":\"cb_red\",\"alt1\":\"cb_yellow\",\"base\":\"cb_green\"},\"lifetime\":0,\"levels\":{\"level\":[{\"icon\":\"cb_red\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"cb_red\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"cb_green\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"cb_yellow\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"cb_red\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"4\"},{\"name\":\"Mobile Speed Camera\",\"description\":\"A speed camera inside of a vehicle enforcing passing traffic.\",\"category\":3,\"audioid\":\"msc\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"Y\",\"icons\":{\"alt2\":\"msc_red\",\"alt1\":\"msc_yellow\",\"base\":\"msc_green\"},\"lifetime\":7200,\"levels\":{\"level\":[{\"icon\":\"msc_red\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"msc_red\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"msc_green\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"msc_yellow\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"msc_red\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"5\"},{\"name\":\"Children at Play\",\"description\":\"Children at Play!\",\"category\":2,\"audioid\":\"childrenatplay\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"N\",\"icons\":{\"alt2\":\"childrenatplay\",\"alt1\":\"childrenatplay\",\"base\":\"childrenatplay\"},\"lifetime\":7200,\"levels\":{\"level\":[{\"icon\":\"childrenatplay\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"childrenatplay\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"childrenatplay\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"childrenatplay\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"childrenatplay\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"20\"},{\"name\":\"Accident\",\"description\":\"Accident or other roadway incident.\",\"category\":3,\"audioid\":\"accident\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"Y\",\"icons\":{\"alt2\":\"accident\",\"alt1\":\"accident\",\"base\":\"accident\"},\"lifetime\":7200,\"levels\":{\"level\":[{\"icon\":\"accident\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"accident\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"accident\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"accident\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"accident\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"21\"},{\"name\":\"Brush Fire\",\"description\":\"Brush Fire.\",\"category\":2,\"audioid\":\"brushfire\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"Y\",\"icons\":{\"alt2\":\"brushfire\",\"alt1\":\"brushfire\",\"base\":\"brushfire\"},\"lifetime\":21600,\"levels\":{\"level\":[{\"icon\":\"brushfire\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"brushfire\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"brushfire\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"brushfire\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"brushfire\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"22\"},{\"name\":\"Dangerous Intersection\",\"description\":\"Dangerous intersection ahead.\",\"category\":2,\"audioid\":\"dangerintersect\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"N\",\"icons\":{\"alt2\":\"dangerintersect\",\"alt1\":\"dangerintersect\",\"base\":\"dangerintersect\"},\"lifetime\":0,\"levels\":{\"level\":[{\"icon\":\"dangerintersect\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"dangerintersect\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"dangerintersect\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"dangerintersect\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"dangerintersect\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"23\"},{\"name\":\"Dangerous Curve\",\"description\":\"Dangerous curve ahead.\",\"category\":2,\"audioid\":\"dangerouscurve\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"N\",\"icons\":{\"alt2\":\"dangerouscurve\",\"alt1\":\"dangerouscurve\",\"base\":\"dangerouscurve\"},\"lifetime\":0,\"levels\":{\"level\":[{\"icon\":\"dangerouscurve\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"dangerouscurve\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"dangerouscurve\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"dangerouscurve\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"dangerouscurve\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"24\"},{\"name\":\"Flooded Road\",\"description\":\"Flooded road ahead.\",\"category\":2,\"audioid\":\"flood\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"Y\",\"icons\":{\"alt2\":\"flood\",\"alt1\":\"flood\",\"base\":\"flood\"},\"lifetime\":21600,\"levels\":{\"level\":[{\"icon\":\"flood\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"flood\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"flood\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"flood\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"flood\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"25\"},{\"name\":\"Ice On Road\",\"description\":\"Ice on road ahead.\",\"category\":2,\"audioid\":\"ice\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"Y\",\"icons\":{\"alt2\":\"ice\",\"alt1\":\"ice\",\"base\":\"ice\"},\"lifetime\":14400,\"levels\":{\"level\":[{\"icon\":\"ice\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"ice\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"ice\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"ice\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"ice\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"26\"},{\"name\":\"Narrow Bridge Ahead\",\"description\":\"Narrow Bridge Ahead.\",\"category\":2,\"audioid\":\"narrowbridge\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"N\",\"icons\":{\"alt2\":\"narrowbridge\",\"alt1\":\"narrowbridge\",\"base\":\"narrowbridge\"},\"lifetime\":0,\"levels\":{\"level\":[{\"icon\":\"narrowbridge\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"narrowbridge\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"narrowbridge\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"narrowbridge\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"narrowbridge\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"27\"},{\"name\":\"Road Closed Ahead\",\"description\":\"Road closed ahead!\",\"category\":2,\"audioid\":\"roadclosed\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"Y\",\"icons\":{\"alt2\":\"roadclosed\",\"alt1\":\"roadclosed\",\"base\":\"roadclosed\"},\"lifetime\":21600,\"levels\":{\"level\":[{\"icon\":\"roadclosed\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"roadclosed\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"roadclosed\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"roadclosed\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"roadclosed\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"28\"},{\"name\":\"Road Kill\",\"description\":\"Dead animal on road ahead.\",\"category\":2,\"audioid\":\"roadkill\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"Y\",\"icons\":{\"alt2\":\"roadkill\",\"alt1\":\"roadkill\",\"base\":\"roadkill\"},\"lifetime\":21600,\"levels\":{\"level\":[{\"icon\":\"roadkill\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"roadkill\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"roadkill\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"roadkill\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"roadkill\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"29\"},{\"name\":\"Toll Booth\",\"description\":\"Toll booth or toll station requiring payment.\",\"category\":2,\"audioid\":\"tollbooth\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"N\",\"icons\":{\"alt2\":\"tollbooth\",\"alt1\":\"tollbooth\",\"base\":\"tollbooth\"},\"lifetime\":0,\"levels\":{\"level\":[{\"icon\":\"tollbooth\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"tollbooth\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"tollbooth\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"tollbooth\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"tollbooth\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"30\"},{\"name\":\"Construction Zone\",\"description\":\"Construction Zone.\",\"category\":3,\"audioid\":\"construction\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"Y\",\"icons\":{\"alt2\":\"construction\",\"alt1\":\"construction\",\"base\":\"construction\"},\"lifetime\":86400,\"levels\":{\"level\":[{\"icon\":\"construction\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"construction\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"construction\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"construction\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"construction\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"31\"},{\"name\":\"MSC Often Seen Here\",\"description\":\"Known location where mobile speed cameras are often seen.\",\"category\":1,\"audioid\":\"mscosh\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"N\",\"icons\":{\"alt2\":\"mscosh_red\",\"alt1\":\"mscosh_yellow\",\"base\":\"mscosh_green\"},\"lifetime\":1814400,\"levels\":{\"level\":[{\"icon\":\"mscosh_red\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"mscosh_red\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"mscosh_green\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"mscosh_yellow\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"mscosh_red\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"7\"},{\"name\":\"School Zone\",\"description\":\"School and/or campus where children are present.\",\"category\":2,\"audioid\":\"schoolzone\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"N\",\"icons\":{\"alt2\":\"schoolzone\",\"alt1\":\"schoolzone\",\"base\":\"schoolzone\"},\"lifetime\":0,\"levels\":{\"level\":[{\"icon\":\"schoolzone\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"schoolzone\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"schoolzone\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"schoolzone\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"schoolzone\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"32\"},{\"name\":\"Road Hazard\",\"description\":\"Objects or other obstructions on the road.\",\"category\":3,\"audioid\":\"roadhazard\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"Y\",\"icons\":{\"alt2\":\"roadhazard\",\"alt1\":\"roadhazard\",\"base\":\"roadhazard\"},\"lifetime\":3600,\"levels\":{\"level\":[{\"icon\":\"roadhazard\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"roadhazard\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"roadhazard\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"roadhazard\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"roadhazard\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"33\"},{\"name\":\"EV Charging Station\",\"description\":\"Electric Vehicle Charging Station.\",\"category\":5,\"audioid\":\"\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"N\",\"notifiable\":\"N\",\"icons\":{\"alt2\":\"ev_charging_station\",\"alt1\":\"ev_charging_station\",\"base\":\"ev_charging_station\"},\"lifetime\":0,\"levels\":{\"level\":[{\"icon\":\"ev_charging_station\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"ev_charging_station\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"ev_charging_station\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"ev_charging_station\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"ev_charging_station\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"40\"},{\"name\":\"Traffic Jam\",\"description\":\"Vehicles are moving slowly or stopped.\",\"category\":3,\"audioid\":\"trafficjam\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"Y\",\"icons\":{\"alt2\":\"traffic_jam\",\"alt1\":\"traffic_jam\",\"base\":\"traffic_jam\"},\"lifetime\":3600,\"levels\":{\"level\":[{\"icon\":\"traffic_jam\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"traffic_jam\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"traffic_jam\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"traffic_jam\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"traffic_jam\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"34\"}],\"alert\":null,\"marker\":[],\"added\":[],\"emailaddr\":null,\"pwdusernamehash\":null,\"mobilepwdhash\":null,\"usercredlevel\":null,\"alertradius\":null,\"signupepochtime\":null,\"lastvoteepochtime\":null,\"totaluservotes\":null,\"karma\":null,\"tosagreed\":null,\"trustedgrouppref\":null,\"privmsgpref\":null,\"smsaddr\":null,\"moderatorlevel\":null,\"loggedontositeflag\":null,\"newsletterflag\":null,\"emailconfflag\":null,\"userstatus\":null,\"lastkarma\":null,\"sipallowedflag\":null,\"loginfailures\":null,\"modapplyepochtime\":null,\"lastapiepochtime\":null,\"lastwebepochtime\":null,\"confirmepochtime\":null,\"confirmmethod\":null,\"facebook\":null,\"twitter\":null,\"twitterenabled\":null,\"facebookenabled\":null,\"tripSessionId\":null,\"patrollines\":[]},\"status\":\"OK\",\"asofepochtime\":1384386619,\"statuscode\":200}}";


    @Inject
	SharedPreferences sharedPreferences;

	
	@Inject
	ITrapsterAPI api;

	@Inject
	BitmapCacheManager bitmapCacheManager;

	@Inject
	TrapManager trapManager;

	@Inject
	SoundThemeManager soundThemeManager;

	@Inject
	ImageResourceDAO imageResourceDao;

	@Inject
	StringResourceDAO stringResourceDao;

	@Inject
	CategoryDAO categoryDao;

	@Inject
	TrapTypeDAO trapTypeDao;

	private AttributesDownloadListener attributesDownloadListener;

	private TrapTypeDownloadListener trapTypesDownloadListener;

	private CategoriesDownloadListener categoriesDownloadListener;

	private int activeConnections;

	private long startTime;

	
	public AttributesSyncService() {
		super(LOGNAME);
	}

	/*
	 * @TODO need a intent trigger to force the download.
	 * 
	 * (non-Javadoc)
	 * 
	 * @see android.app.IntentService#onHandleIntent(android.content.Intent)
	 */
	@Override
	protected void onHandleIntent(Intent intent) {


        startTime = System.currentTimeMillis();
	

		/*
		 * Load the local resources first
		 */
		try {
			//preloadResources();
            preloadResourcesFromJson();


            Log.v(LOGNAME,
                    "Finished PreLoading Assets:"
                            + (System.currentTimeMillis() - startTime) + " ms");

            sendCompleteBroadcast();
		} catch (Exception e) {
            //Log.e(LOGNAME,"Unable to preload resources:"+e.getMessage());
        }


        Editor e = sharedPreferences.edit();
        e.putLong(
                Defaults.PREFERENCE_LAST_UPDATE_TIME_ALLATTRIBUTES,
                ATTRIBUTES_BUNDLE_TIMESTAMP );
        e.commit();



        checkForNewTermsAndConditions();

        // Sound Themes
        syncSoundThemesWithServer();


		/*
		 * This will sync the appattributes with the local version
		 *
		 * This will be performed asynchronously
		 */
		syncAttributesWithServer();

		/*
		 * This will sync the trap categories with the local version. Once the
		 * categories are syncd the trap types are updated (category is linked
		 * in trap type)
		 * 
		 * This will be performed asynchronously
		 */
		 syncCategoriesWithServer();

        // syncTrapTypesWithServer();

    }

    private void syncSoundThemesWithServer()
    {
        soundThemeManager.retrieveThemeList();
       // openConnection();
    }

	private void checkForNewTermsAndConditions() {

		long lastUpdateTimeAttributes = sharedPreferences.getLong(
				Defaults.ASSET_TERMSANDCONDITIONS_NEWEST_DATE, 0) / 1000;
		//Log.v(LOGNAME, "Checking Terms:" + lastUpdateTimeAttributes);

		// ReloadAttributesListener listener = new
		// ReloadAttributesListener(sharedPreferences, null, stringResourceDao);

		TermsAttributesDownloadListener termsPreloadListener = new TermsAttributesDownloadListener();

		api.getLegalAttributes(lastUpdateTimeAttributes, termsPreloadListener,
				termsPreloadListener);
		openConnection();
	}

	private void sendCompleteBroadcast() {
		sendBroadcast(new Intent(ATTRIBUTES_LOADED_LISTENER));

	}

    private void preloadResources() throws IOException {


        long lastUpdateTimeAttributes = sharedPreferences.getLong(
                Defaults.PREFERENCE_LAST_UPDATE_TIME_ALLATTRIBUTES, 0);

        if (lastUpdateTimeAttributes == 0) {

            //Log.v(LOGNAME, "Loading Bundled Assets");

            InputStream attributesInputStream = this.getAssets().open(
                    "attributes_bundle.xml", AssetManager.ACCESS_STREAMING);

            AttributesDownloadListener attributesPreloadListener = new AttributesDownloadListener(
                    true);
            new AttributesParser(attributesPreloadListener)
                    .process(attributesInputStream);

            InputStream categoriesInputStream = this.getAssets().open(
                    "categories_bundle.xml", AssetManager.ACCESS_STREAMING);

            CategoriesDownloadListener categoryPreloadListener = new CategoriesDownloadListener(
                    true);
            new CategoryParser(categoryPreloadListener)
                    .parse(categoriesInputStream);

            InputStream trapTypesInputStream = this.getAssets().open(
                    "trap_types_bundle.xml", AssetManager.ACCESS_STREAMING);

            TrapTypeDownloadListener trapTypesPreloadListener = new TrapTypeDownloadListener(
                    true);
            new TrapTypeParser(trapTypesPreloadListener)
                    .parse(trapTypesInputStream);

        }


        bitmapCacheManager.preloadSVGImages();

    }

    private void preloadResourcesFromJson() throws Exception {

        long lastUpdateTimeAttributes = sharedPreferences.getLong(
                Defaults.PREFERENCE_LAST_UPDATE_TIME_ALLATTRIBUTES, 0);

        if (lastUpdateTimeAttributes == 0) {


           //InputStream attributesInputStream = this.getAssets().open(
            //        "images.json", AssetManager.ACCESS_STREAMING);

           /* new BackgroundTask()
            {
                @Override
                public void onExecute()
                {*/

                    try
                    {
                        AttributesDownloadListener attributesPreloadListener = new AttributesDownloadListener(true);
                        new com.trapster.android.comms.rest.parser.AttributesParser(AttributesSyncService.this,attributesPreloadListener).process(200, IMAGES_JSON);


                        CategoriesDownloadListener categoryPreloadListener = new CategoriesDownloadListener(true);
                        new com.trapster.android.comms.rest.parser.TrapCategoryParser(categoryPreloadListener).process(200,TRAPCATEGORY_JSON);

                        TrapTypeDownloadListener trapTypesPreloadListener = new TrapTypeDownloadListener(true);
                        new com.trapster.android.comms.rest.parser.TrapTypeParser(trapTypesPreloadListener).process(200,TRAPTYPE_JSON);

                        bitmapCacheManager.preloadSVGImages();

                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
              /*  }

                @Override
                public void onTimeout(long runTime)
                {
                    Log.v(LOGNAME,"Timeout on preload");
                }
            }.execute();*/


     /*      // InputStream categoriesInputStream = this.getAssets().open(
           //         "trapcategory.json", AssetManager.ACCESS_STREAMING);

            new BackgroundTask()
            {
                @Override
                public void onExecute()
                {

                    try
                    {
                        CategoriesDownloadListener categoryPreloadListener = new CategoriesDownloadListener(true);
                        new com.trapster.android.comms.rest.parser.TrapCategoryParser(categoryPreloadListener).process(200,TRAPCATEGORY_JSON);
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onTimeout(long runTime)
                {
                }
            }.execute();


           // InputStream trapTypesInputStream = this.getAssets().open(
           //         "traptypes.json", AssetManager.ACCESS_STREAMING);



            new BackgroundTask()
            {
                @Override
                public void onExecute()
                {

                    try
                    {
                        TrapTypeDownloadListener trapTypesPreloadListener = new TrapTypeDownloadListener(true);
                        new com.trapster.android.comms.rest.parser.TrapTypeParser(trapTypesPreloadListener).process(200,TRAPTYPE_JSON);
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onTimeout(long runTime)
                {
                }
            }.execute();





            new BackgroundTask()
            {
                @Override
                public void onExecute()
                {
                    bitmapCacheManager.preloadSVGImages();
                }

                @Override
                public void onTimeout(long runTime)
                {
                }
            }.execute();
*/
        }

    }


	private void openConnection() {
		activeConnections++;
	}

	private void closeConnection() {
		activeConnections--;
		if (activeConnections <= 0) {
			Editor e = sharedPreferences.edit();
			e.putLong(Defaults.PREFERENCE_LAST_UPDATE_TIME_ALLATTRIBUTES,
					System.currentTimeMillis() / 1000);
			e.commit();

			Log.v(LOGNAME,
                   "Finished Loading Assets:"
                           + (System.currentTimeMillis() - startTime) + " ms");

		}
	}

	private void syncAttributesWithServer() {
		////Log,.v(LOGNAME, "Syncing Attributes with Server");

		// Attributes
		long lastUpdateTimeAttributes = sharedPreferences.getLong(
				Defaults.PREFERENCE_LAST_UPDATE_TIME_ATTRIBUTES, 0);
		attributesDownloadListener = new AttributesDownloadListener(false);
		api.getAttributes(lastUpdateTimeAttributes, attributesDownloadListener,
				attributesDownloadListener);
		openConnection();

	}

	private void syncCategoriesWithServer() {

		////Log,.v(LOGNAME, "Syncing Categories with Server");

		// Categories
		long lastUpdateTimeCategories = sharedPreferences.getLong(
				Defaults.PREFERENCE_LAST_UPDATE_TIME_CATEGORIES, 0);
		categoriesDownloadListener = new CategoriesDownloadListener(false);
		api.getCategories(lastUpdateTimeCategories, categoriesDownloadListener,
				categoriesDownloadListener);
		openConnection();
	}

	private void syncTrapTypesWithServer() {
		Log.v(LOGNAME, "Syncing TrapTypes with Server");

		// Trap Types
		long lastUpdateTimeTrapTypes = sharedPreferences.getLong(
				Defaults.PREFERENCE_LAST_UPDATE_TIME_TRAP_CONFIG, 0);
		trapTypesDownloadListener = new TrapTypeDownloadListener(false);
		api.getTrapTypes(lastUpdateTimeTrapTypes, trapTypesDownloadListener,
				trapTypesDownloadListener);
		openConnection();
	}

	private class CategoriesDownloadListener implements
			CommunicationStatusListener, CategoryListener {

		private boolean preload;

		public CategoriesDownloadListener(boolean preload) {
			this.preload = preload;
		}

		@Override
		public void onOpenConnection() {
			//Log,.v(LOGNAME, "Downloading Categories");
		}

		@Override
		public void onCloseConnection() {
			//Log,.v(LOGNAME, "Importing Categories");
		}

		@Override
		public void onConnectionError(String errorMessage) {
{/* 			//Log.e(LOGNAME, "Comms Error downloading categories:" + errorMessage); */}
		}

		@Override
		public void onError(TrapsterError error) {
{/* 			//Log.e(LOGNAME, "Error downloading categories:" + error.getDetails()); */}

		}

		@Override
		public void onCategories(Categories categories) {

			for (Category category : categories.getCategories())
            {
				categoryDao.save(category);
			}

            //setTrapTypeCategories();

			// update the preferences
			Editor e = sharedPreferences.edit();
			e.putLong(
					Defaults.PREFERENCE_LAST_UPDATE_TIME_CATEGORIES,
					(preload) ? ATTRIBUTES_BUNDLE_TIMESTAMP : System
							.currentTimeMillis() / 1000);
			e.commit();

			if (!preload) {
				syncTrapTypesWithServer();
				closeConnection();
			}

		}

	}

    private List<TrapType> setTrapTypeCategories(List<TrapType> trapTypes)
    {
        ArrayList<TrapType> trapTypesAdded = new ArrayList<TrapType>();
        if (trapTypes != null)
        {
            for (TrapType trapType : trapTypes)
            {
                Category category = categoryDao.findById(trapType.getCategoryId());
                if (category != null)
                    trapType.setCategory(category);

                trapTypesAdded.add(trapType);
            }

           // trapTypeDao.save(trapTypes);
           // trapManager.sortTrapTypes();
        }

        return trapTypesAdded;

    }

	private class TrapTypeDownloadListener implements
			CommunicationStatusListener, TrapTypeListener {

		private boolean preload;

		public TrapTypeDownloadListener(boolean preload) {
			this.preload = preload;
		}

		@Override
		public void onOpenConnection() {
			//Log,.v(LOGNAME, "Downloading TrapTypes");
		}

		@Override
		public void onCloseConnection() {
			//Log,.v(LOGNAME, "Importing TrapTypes");
		}

		@Override
		public void onConnectionError(String errorMessage) {
            //Log.e(LOGNAME, "Error downloading traptypes:" + errorMessage);
		}

		@Override
		public void onError(TrapsterError error) {
            //Log.e(LOGNAME, "Error downloading traptypes:" + error.getDetails());
		}

		@Override
		public void onTrapTypes(TrapTypes types) {

            android.util.Log.e(LOGNAME,"Updating "+types.getTypes().size()+" TrapTypes("+preload+")");

            //if (!preload)

				/*Log,.v(LOGNAME,
						"Downloading "
								+ types.getTypes().size()
								+ " new trap types:"
								+ sharedPreferences
										.getLong(
												Defaults.PREFERENCE_LAST_UPDATE_TIME_TRAP_CONFIG,
												0));*/



            trapTypeDao.save(setTrapTypeCategories(types.getTypes()));

           // bitmapCacheManager.preloadSVGImages();

            trapManager.sortTrapTypes();

			Editor e = sharedPreferences.edit();
			e.putLong(
					Defaults.PREFERENCE_LAST_UPDATE_TIME_TRAP_CONFIG,
					(preload) ? ATTRIBUTES_BUNDLE_TIMESTAMP : System
							.currentTimeMillis() / 1000);
			e.commit();


			if (!preload)
				closeConnection();

		}

	}

	private class AttributesDownloadListener implements
			CommunicationStatusListener, AttributesListener {

		private boolean preload;

		public AttributesDownloadListener(boolean preload) {
			this.preload = preload;
		}

		@Override
		public void onError(TrapsterError error) {
            //Log.e(LOGNAME, "Error downloading attributes:" + error.getDetails());
		}

		@Override
		public void onAttributes(TrapsterResources resources) {
			// Download the images
			checkImageDownload(resources.getImages());

			// @TODO Strings are not used except T&C; they are loaded at the
			// start of the service
			// populate the text and color db's
			checkStringResource(resources.getStrings());

			// @TODO we never use color resources. No point storing them
			// checkColorResource(resources.getColors());

			// update the preferences
			Editor e = sharedPreferences.edit();

			e.putLong(
					Defaults.PREFERENCE_LAST_UPDATE_TIME_ATTRIBUTES,
					(preload) ? ATTRIBUTES_BUNDLE_TIMESTAMP : System
							.currentTimeMillis() / 1000);
			e.commit();

			if (!preload)
				closeConnection();
		}

		@Override
		public void onOpenConnection() {
			////Log,.v(LOGNAME, "Downloading Attributes");
		}

		@Override
		public void onCloseConnection() {
			////Log,.v(LOGNAME, "Importing Attributes");
		}

		@Override
		public void onConnectionError(String errorMessage) {
{/* 			//Log.e(LOGNAME, "Connection Error downloading attributes:"
					+ errorMessage); */}
		}

		private void checkStringResource(
				ArrayList<StringResource> stringResources) {

			for (StringResource resource : stringResources) {

				stringResourceDao.save(resource);
{/* 
				//Log.d(LOGNAME, "String Resource:" + resource.getKey() + ":"
						+ resource.getText()); */}

				if (resource.getKey().equalsIgnoreCase(
						Defaults.ASSET_TERMSANDCONDITIONS)) {

					////Log,.v(LOGNAME, "New Terms:" + resource.getText());

					Editor edit = sharedPreferences.edit();
					edit.putLong(Defaults.ASSET_TERMSANDCONDITIONS_NEWEST_DATE,
							System.currentTimeMillis());
					edit.commit();



				}

			}

		}

		/**
		 * Loads and processes all the images synchronously
		 * 
		 * 
		 * 
		 * @param imageResources
		 */
		private void checkImageDownload(ArrayList<ImageResource> imageResources) {

			imageResourceDao.save(imageResources, preload);
				
			/*for (ImageResource resource : imageResources) {

				resource.setPreload(preload);

				Log.d(LOGNAME, "New ImageResource:" + resource.getKey());

				try {
					//imageResourceDao.save(resource);

					bitmapCacheManager.getBitmapFromImageResource(
							resource.getKey(), null, false);

				} catch (Exception e) {
{*//* 					//Log.e(LOGNAME,
							"Unable to download and store image resource:"
									+ e.getMessage()); *//*}
				}

			}*/

		}

	}

	private class TermsAttributesDownloadListener implements
			CommunicationStatusListener, AttributesListener {

		
		public TermsAttributesDownloadListener() {
		}

		@Override
		public void onError(TrapsterError error) {
{/* 			//Log.e(LOGNAME, "Error downloading terms:" + error.getDetails()); */}
		}

		@Override
		public void onAttributes(TrapsterResources resources) {
			
			for (StringResource resource : resources.getStrings()) {

				stringResourceDao.save(resource);

				if (resource.getKey().equalsIgnoreCase(
						Defaults.ASSET_TERMSANDCONDITIONS)) {

				//	Log.v(LOGNAME, "New Terms:" + System.currentTimeMillis());

					Editor edit = sharedPreferences.edit();
					edit.putLong(Defaults.ASSET_TERMSANDCONDITIONS_NEWEST_DATE,
							System.currentTimeMillis());
					//edit.commit();
					edit.apply();


				}

			}
			
			closeConnection();
		}

		@Override
		public void onOpenConnection() {
			//Log,.v(LOGNAME, "Downloading Terms");
		}

		@Override
		public void onCloseConnection() {
			//Log,.v(LOGNAME, "Importing Terms");
		}

		@Override
		public void onConnectionError(String errorMessage) {
{/* 			//Log.e(LOGNAME, "Connection Error downloading Terms:"
					+ errorMessage); */}
		}

	}

}
