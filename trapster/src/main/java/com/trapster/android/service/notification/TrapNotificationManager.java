package com.trapster.android.service.notification;

import java.util.HashMap;
import java.util.List;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import com.google.inject.Inject;
import com.trapster.android.model.Trap;
import com.trapster.android.service.UserLocation;

public class TrapNotificationManager {

    private static final String LOGNAME = "TrapNotificationManager";

    @Inject Application application;
    @Inject BroadcastNotifier broadcastNotifier;
    @Inject PlaySoundNotifier playSoundNotifier;
 

    private Context context;

    private HashMap<NOTIFIERS, Notifier> notifiers = new HashMap<NOTIFIERS, Notifier>();

    public static enum NOTIFIERS { broadcast, playsound, statusbar, stats  }

    public TrapNotificationManager() {
    }

    @Inject
    public void init() {
        //Log,.v(LOGNAME, "TrapNotificationManager initialized");
        this.context = application.getApplicationContext();
        addNotifiers();
    }

    public void sendTrapNotification(UserLocation userLocation, List<Trap> traps, boolean isBackground, List<NOTIFIERS> notifierList){

        for(NOTIFIERS n: notifierList){

            Notifier notifier = notifiers.get(n);
            if(notifier != null){
                notifier.setUserLocation(userLocation);

                notifier.send(traps, isBackground);
            }
        }
    }


    private void addNotifiers(){

        notifiers.put(NOTIFIERS.broadcast,broadcastNotifier);
        notifiers.put(NOTIFIERS.playsound,playSoundNotifier);
        //notifiers.put(NOTIFIERS.stats,new StatsNotifier(context));

    }

}
