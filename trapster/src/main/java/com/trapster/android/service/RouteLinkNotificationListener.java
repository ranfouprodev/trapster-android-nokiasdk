package com.trapster.android.service;

import java.util.Set;

import com.trapster.android.model.RouteLink;

public interface RouteLinkNotificationListener
{
    void onRouteLinkUpdate(RouteLink routeLink, Set<RouteLink> approximateRouteLinks, int resultCode);
}
