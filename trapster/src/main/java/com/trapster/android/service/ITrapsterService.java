/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: E:\\Workspaces\\Trapster\\Trapster-SmartAPI-Android_2_2\\src\\com\\trapster\\android\\service\\ITrapsterService.aidl
 */
package com.trapster.android.service;
public interface ITrapsterService extends android.os.IInterface
{
    /** Local-side IPC implementation stub class. */
    public static abstract class Stub extends android.os.Binder implements com.trapster.android.service.ITrapsterService
    {
        private static final java.lang.String DESCRIPTOR = "com.trapster.android.service.ITrapsterService";
        /** Construct the stub at attach it to the interface. */
        public Stub()
        {
            this.attachInterface(this, DESCRIPTOR);
        }
        /**
         * Cast an IBinder object into an com.trapster.android.service.ITrapsterService interface,
         * generating a proxy if needed.
         */
        public static com.trapster.android.service.ITrapsterService asInterface(android.os.IBinder obj)
        {
            if ((obj==null)) {
                return null;
            }
            android.os.IInterface iin = (android.os.IInterface)obj.queryLocalInterface(DESCRIPTOR);
            if (((iin!=null)&&(iin instanceof com.trapster.android.service.ITrapsterService))) {
                return ((com.trapster.android.service.ITrapsterService)iin);
            }
            return new com.trapster.android.service.ITrapsterService.Stub.Proxy(obj);
        }
        public android.os.IBinder asBinder()
        {
            return this;
        }
        @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
        {
            switch (code)
            {
                case INTERFACE_TRANSACTION:
                {
                    reply.writeString(DESCRIPTOR);
                    return true;
                }
                case TRANSACTION_getState:
                {
                    data.enforceInterface(DESCRIPTOR);
                    int _result = this.getState();
                    reply.writeNoException();
                    reply.writeInt(_result);
                    return true;
                }
                case TRANSACTION_requestTrapsByRadius:
                {
                    data.enforceInterface(DESCRIPTOR);
                    double _arg0;
                    _arg0 = data.readDouble();
                    double _arg1;
                    _arg1 = data.readDouble();
                    double _arg2;
                    _arg2 = data.readDouble();
                    this.requestTrapsByRadius(_arg0, _arg1, _arg2);
                    reply.writeNoException();
                    return true;
                }
                case TRANSACTION_requestTrapsByBoundingBox:
                {
                    data.enforceInterface(DESCRIPTOR);
                    double _arg0;
                    _arg0 = data.readDouble();
                    double _arg1;
                    _arg1 = data.readDouble();
                    double _arg2;
                    _arg2 = data.readDouble();
                    double _arg3;
                    _arg3 = data.readDouble();
                    this.requestTrapsByBoundingBox(_arg0, _arg1, _arg2, _arg3);
                    reply.writeNoException();
                    return true;
                }
                case TRANSACTION_acknowledgeTrap:
                {
                    data.enforceInterface(DESCRIPTOR);
                    int _arg0;
                    _arg0 = data.readInt();
                    boolean _result = this.acknowledgeTrap(_arg0);
                    reply.writeNoException();
                    reply.writeInt(((_result)?(1):(0)));
                    return true;
                }
                case TRANSACTION_stopService:
                {
                    data.enforceInterface(DESCRIPTOR);
                    boolean _arg0;
                    _arg0 = (0!=data.readInt());
                    this.stopService(_arg0);
                    reply.writeNoException();
                    return true;
                }
            }
            return super.onTransact(code, data, reply, flags);
        }
        private static class Proxy implements com.trapster.android.service.ITrapsterService
        {
            private android.os.IBinder mRemote;
            Proxy(android.os.IBinder remote)
            {
                mRemote = remote;
            }
            public android.os.IBinder asBinder()
            {
                return mRemote;
            }
            public java.lang.String getInterfaceDescriptor()
            {
                return DESCRIPTOR;
            }
            public int getState() throws android.os.RemoteException
            {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                int _result;
                try {
                    _data.writeInterfaceToken(DESCRIPTOR);
                    mRemote.transact(Stub.TRANSACTION_getState, _data, _reply, 0);
                    _reply.readException();
                    _result = _reply.readInt();
                }
                finally {
                    _reply.recycle();
                    _data.recycle();
                }
                return _result;
            }
            public void requestTrapsByRadius(double latitude, double longitude, double radiusInMeters) throws android.os.RemoteException
            {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(DESCRIPTOR);
                    _data.writeDouble(latitude);
                    _data.writeDouble(longitude);
                    _data.writeDouble(radiusInMeters);
                    mRemote.transact(Stub.TRANSACTION_requestTrapsByRadius, _data, _reply, 0);
                    _reply.readException();
                }
                finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
            public void requestTrapsByBoundingBox(double minLatitude, double minLongitude, double maxLatitude, double maxLongitude) throws android.os.RemoteException
            {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(DESCRIPTOR);
                    _data.writeDouble(minLatitude);
                    _data.writeDouble(minLongitude);
                    _data.writeDouble(maxLatitude);
                    _data.writeDouble(maxLongitude);
                    mRemote.transact(Stub.TRANSACTION_requestTrapsByBoundingBox, _data, _reply, 0);
                    _reply.readException();
                }
                finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
            public boolean acknowledgeTrap(int trapId) throws android.os.RemoteException
            {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                boolean _result;
                try {
                    _data.writeInterfaceToken(DESCRIPTOR);
                    _data.writeInt(trapId);
                    mRemote.transact(Stub.TRANSACTION_acknowledgeTrap, _data, _reply, 0);
                    _reply.readException();
                    _result = (0!=_reply.readInt());
                }
                finally {
                    _reply.recycle();
                    _data.recycle();
                }
                return _result;
            }
            public void stopService(boolean background) throws android.os.RemoteException
            {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(DESCRIPTOR);
                    _data.writeInt(((background)?(1):(0)));
                    mRemote.transact(Stub.TRANSACTION_stopService, _data, _reply, 0);
                    _reply.readException();
                }
                finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }
        static final int TRANSACTION_getState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
        static final int TRANSACTION_requestTrapsByRadius = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
        static final int TRANSACTION_requestTrapsByBoundingBox = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
        static final int TRANSACTION_acknowledgeTrap = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
        static final int TRANSACTION_stopService = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
    }
    public int getState() throws android.os.RemoteException;
    public void requestTrapsByRadius(double latitude, double longitude, double radiusInMeters) throws android.os.RemoteException;
    public void requestTrapsByBoundingBox(double minLatitude, double minLongitude, double maxLatitude, double maxLongitude) throws android.os.RemoteException;
    public boolean acknowledgeTrap(int trapId) throws android.os.RemoteException;
    public void stopService(boolean background) throws android.os.RemoteException;
}

