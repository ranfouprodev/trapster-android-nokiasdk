package com.trapster.android.service;

import java.util.Timer;
import java.util.TimerTask;

import com.here.android.common.GeoPosition;


import com.trapster.android.comms.TrapUpdateMonitorListener;
import com.trapster.android.util.GeographicUtils;
import com.trapster.android.util.Log;

public class TrapUpdateMonitor {

    private static final String LOGNAME = "Trapster.TrapUpdateMonitor";

    private GeoPosition currentLocation;
    private GeoPosition lastUpdateLocation;
    private long lastUpdateTime = 0;
    private double searchAheadRadius;
    private TrapUpdateMonitorListener listener;
    private int maxUpdateTime;
    private boolean skipUpdate = true;

    private Timer updateCheck;
    private TimerTask update = new TimerTask() {


        public void run() {
            if(skipUpdate){
                skipUpdate = false;
            }else{
                check();
            }
        }
    };

    public TrapUpdateMonitor( double searchAheadRadius, int maxUpdateTime, TrapUpdateMonitorListener listener ) {
        super();
{/*         Log.i(LOGNAME, "Trap TrapUpdateMonitor"); */}
        this.listener = listener;
        this.maxUpdateTime = maxUpdateTime;
        this.searchAheadRadius = searchAheadRadius;

        if (updateCheck == null) {
            updateCheck = new Timer("TrapTimerUpdateMonitor");
            updateCheck.scheduleAtFixedRate(update,0, maxUpdateTime);
        }

    }


    public void stop(){
{/*         Log.i(LOGNAME, "Trap stop"); */}
        if(updateCheck != null){
            updateCheck.cancel();
        }
    }

    public boolean update(GeoPosition geoPosition){
{/*         Log.i(LOGNAME, "Trap update"); */}
        this.currentLocation = geoPosition;
        skipUpdate = true; // skip the next timer check
        return check();
    }


    private boolean check(){
{/*         Log.i(LOGNAME, "Trap check"); */}

        if(currentLocation == null)
            return false;

        // check for timeout
        if( (System.currentTimeMillis() - lastUpdateTime) > maxUpdateTime){
            if(listener != null)
                  listener.onTrapUpdateRequest(currentLocation.getCoordinate().getLatitude(),
                          currentLocation.getCoordinate().getLongitude(), searchAheadRadius);

            lastUpdateTime = System.currentTimeMillis();
            lastUpdateLocation = currentLocation;

            return true;
        }

        // check for distance
        double travel = GeographicUtils.geographicDistance(currentLocation.getCoordinate().getLatitude(),
                currentLocation.getCoordinate().getLongitude(),
                lastUpdateLocation.getCoordinate().getLatitude(),
                lastUpdateLocation.getCoordinate().getLongitude());

        if(travel > (searchAheadRadius / 2)){
            if(listener != null)
                listener.onTrapUpdateRequest(currentLocation.getCoordinate().getLatitude(),
                        currentLocation.getCoordinate().getLongitude(), searchAheadRadius);

            lastUpdateTime = System.currentTimeMillis();
            lastUpdateLocation = currentLocation;

            return true;
        }

        return false;
    }



}
