package com.trapster.android.service;

import com.trapster.android.model.Trap;

import java.io.Serializable;
import java.util.ArrayList;

public interface TrapsterServiceNotificationListener extends Serializable
{

    public void onActiveTrapUpdate(ArrayList<Trap> traps);

    public void onUpdateSpeedAndDirection(double speed, int direction);

    public void onError(int errorCode, String message);

    public String getOwner();

}