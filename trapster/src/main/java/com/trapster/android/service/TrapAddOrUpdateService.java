package com.trapster.android.service;

import android.content.Intent;

import com.google.inject.Inject;
import com.trapster.android.activity.fragment.TrapMapLayer;
import com.trapster.android.comms.BasicResponseListener;
import com.trapster.android.comms.ITrapsterAPI;
import com.trapster.android.comms.NewTrapListener;
import com.trapster.android.model.Trap;
import com.trapster.android.model.dao.TrapDAO;
import com.trapster.android.util.Log;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;

import roboguice.service.RoboIntentService;

public class TrapAddOrUpdateService extends RoboIntentService {

	private static final String LOGNAME = "TrapAddorUpdateService";
	
	public static final String INTENT_EXTRA_TRAP = "INTENTTRAPEXTRA";
	
	public static final String INTENT_EXTRA_DELETE_TRAP = "INTENTTRAPEXTRADELETE";

	@Inject
	TrapMapLayer trapMapLayer;
	
	@Inject
	TrapDAO trapDao;

	@Inject
	ITrapsterAPI api;

	private PostingTrapListener postTrapListener;

	private DeleteTrapListener deleteTrapListener;

	private Trap deletedTrap;

	public TrapAddOrUpdateService() {
		super(LOGNAME);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		if (intent != null && intent.hasExtra(INTENT_EXTRA_TRAP)){
			Trap trap = intent.getParcelableExtra(INTENT_EXTRA_TRAP);
			// New Trap
			if(trap.getId()== -1){
                double heading  = 0; // get from position manager
				addTrap(trap, heading);
			}else{// @todo Updating trap (voting or deleting?)  
				
			}
		}else if (intent != null && intent.hasExtra(INTENT_EXTRA_DELETE_TRAP)){
			Trap trap = intent.getParcelableExtra(INTENT_EXTRA_DELETE_TRAP);
			deleteTrap(trap);
		}
	}

	
	private void deleteTrap(Trap trap){
		trapDao.delete(trap.getId());

        trapMapLayer.removeTrap(trap);
		
		deleteTrapListener = new DeleteTrapListener(); 
		
		deletedTrap = trap; 
		api.deleteTrap(trap,deleteTrapListener, deleteTrapListener);
		
	}
	private void addTrap(Trap trap, double heading) {
		//Log.v(LOGNAME, "Adding new trap:"+trap.getLatitude()+","+trap.getLongitude());
		postTrapListener = new PostingTrapListener(); 
		api.addNewTrap(trap,postTrapListener , postTrapListener, heading);


	}
	
	

	private class PostingTrapListener implements  NewTrapListener ,CommunicationStatusListener{

		@Override
		public void onOpenConnection() {
			//Log,.v(LOGNAME, "Updating Trap");
		}

		@Override
		public void onCloseConnection() {
			//Log,.v(LOGNAME, "Closing Connection");
		}

		@Override
		public void onConnectionError(String errorMessage) {
 			android.util.Log.e(LOGNAME, "Connection Error Updating Traps:" + errorMessage);
		}

		@Override
		public void onError(TrapsterError error) {
 			Log.e(LOGNAME, "Error Updating Traps:" + error.getDetails());

		}

		@Override
		public void onNewTrapAdded(Trap trap, String optionalMessage) {

            Log.v(LOGNAME,"New Trap Added");


            // Remove temporary traps
            //removeDummyTraps();

            trapDao.save(trap);
            trapMapLayer.addTrap(trap);

			// This pulls the trap from the database and populates all the details
			Intent intent = new Intent(getApplicationContext(), TrapSyncService.class);
	    	intent.putExtra(TrapSyncService.EXTRA_LATITUDE, trap.getLatitude());
	    	intent.putExtra(TrapSyncService.EXTRA_LONGITUDE, trap.getLongitude());
	    	intent.putExtra(TrapSyncService.EXTRA_RADIUS, 0.5);
	    	intent.putExtra(TrapSyncService.EXTRA_FORCE, true);
			startService(intent);


	    	
		}
		
	}

    private void removeDummyTraps() {
        trapMapLayer.removeTrap(-1);
        trapDao.delete(-1);
    }

    private class DeleteTrapListener implements  BasicResponseListener, CommunicationStatusListener{

		@Override
		public void onOpenConnection() {
			//Log,.v(LOGNAME, "Updating Trap");
		}

		@Override
		public void onCloseConnection() {
			//Log,.v(LOGNAME, "Closing Connection");
		}

		@Override
		public void onConnectionError(String errorMessage) {
{/* 			//Log.e(LOGNAME, "Connection Error Updating Traps:" + errorMessage); */}
		}

		@Override
		public void onError(TrapsterError error) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onComplete() {
			Intent intent = new Intent(getApplicationContext(), TrapSyncService.class);
	    	intent.putExtra(TrapSyncService.EXTRA_LATITUDE, deletedTrap.getLatitude());
	    	intent.putExtra(TrapSyncService.EXTRA_LONGITUDE, deletedTrap.getLongitude());
	    	intent.putExtra(TrapSyncService.EXTRA_RADIUS, 1);
	    	intent.putExtra(TrapSyncService.EXTRA_FORCE, true);
			startService(intent);
		}

	}

}
