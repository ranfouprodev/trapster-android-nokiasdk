package com.trapster.android.service;

public interface ConeFilterNotificationListener {
    void onConeFilterUpdate(int arc, double radius, int bearing );
}
