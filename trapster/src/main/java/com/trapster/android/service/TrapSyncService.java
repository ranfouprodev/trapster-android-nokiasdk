package com.trapster.android.service;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import com.google.inject.Inject;
import com.trapster.android.activity.fragment.TrapMapLayer;
import com.trapster.android.comms.ITrapsterAPI;
import com.trapster.android.comms.TrapListener;
import com.trapster.android.model.MapTileUpdateResource;
import com.trapster.android.model.Trap;
import com.trapster.android.model.Traps;
import com.trapster.android.model.dao.MapTileUpdateResourceDAO;
import com.trapster.android.model.dao.TrapDAO;
import com.trapster.android.util.GeographicUtils;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;
import roboguice.service.RoboIntentService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TrapSyncService extends RoboIntentService {

    public static final String LOGNAME = "TrapSyncService";

    public static final String EXTRA_LATITUDE = "trap_extra_latitude";

    public static final String EXTRA_LONGITUDE = "trap_extra_longitude";

    public static final String EXTRA_RADIUS = "trap_extra_radius";

    public static final String EXTRA_FORCE = "trap_extra_force";

    public static final double MAX_QUERY_RADIUS = 10;

    public static final int MAPTILE_CACHE_ZOOM_LEVEL = 11; // Works to roughly
    // 6 miles across
    // (http://msdn.microsoft.com/en-us/library/bb259689.aspx)
    public static final String MAPTILE_CACHE_LAYER = "traps"; // Layer name for
    // db caching
    public static final int MAPTILE_EXPIRY_IN_SECONDS = 120;

    @Inject
    TrapDAO trapDao;
    @Inject
    MapTileUpdateResourceDAO mapTileUpdateResourceDao;
    @Inject
    ITrapsterAPI api;
    @Inject
    TrapMapLayer trapMapLayer;

    private TrapDownloadListener trapListener;

    private int connections;

    private ArrayList<Trap> trapList = new ArrayList<Trap>();

    /**
     * This class forces the update after a TIMEOUT_IN_MS period. This ensures that any strange connections
     * won't stop the rest of the tiles from updating
     *
     * @john connections are not sent as they come in. The trap layer handles it
     */
    private Handler timeoutHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            connections = 0;
            // updateTraps();
        }
    };
    private static int TIMEOUT_IN_MS = 15000;

    public TrapSyncService() {
        super(LOGNAME);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        connections = 0;

        if (intent != null && intent.hasExtra(EXTRA_LATITUDE)
                && intent.hasExtra(EXTRA_LONGITUDE)
                && intent.hasExtra(EXTRA_RADIUS)) {
            double latitude = intent.getDoubleExtra(EXTRA_LATITUDE, 0);
            double longitude = intent.getDoubleExtra(EXTRA_LONGITUDE, 0);
            double radius = intent.getDoubleExtra(EXTRA_RADIUS, 1);

            boolean force = intent.getBooleanExtra(EXTRA_FORCE, false);

            trapListener = new TrapDownloadListener();

            requestTrapUpdate(latitude, longitude, radius, force);


        }
    }

    /**
     * Traps are updated based on a tile system. Given the coordinates and
     * radius, we construct a list of tiles that should be updated. Those tiles
     * are them compared against the maptile database to ensure we aren't
     * requesting updates too quickly
     *
     * @param lat
     * @param lon
     * @param radiusInMiles
     */
    public void requestTrapUpdate(double lat, double lon, double radiusInMiles,
                                  boolean force) {

        radiusInMiles = (radiusInMiles > MAX_QUERY_RADIUS) ? MAX_QUERY_RADIUS : radiusInMiles;

        float radiusInMeters = (float) (GeographicUtils
                .milesToKilometers(radiusInMiles) * 1000);

        double latSpan = GeographicUtils
                .calculateLatitudeSpanOnSameLongitudialLine(radiusInMeters);
        double lonSpan = GeographicUtils
                .calculateLongitudeSpanOnSameLatitudeLevel(radiusInMeters, lat, lon);

        double minLat = lat - latSpan;
        double maxLat = lat + latSpan;

        double minLon = lon - lonSpan;
        double maxLon = lon + lonSpan;

        MapTileUpdateResource topLeft = MapTileUpdateResource.createFromPoint(
                MAPTILE_CACHE_LAYER, maxLat, minLon, MAPTILE_CACHE_ZOOM_LEVEL);
        MapTileUpdateResource bottomRight = MapTileUpdateResource
                .createFromPoint(MAPTILE_CACHE_LAYER, minLat, maxLon, MAPTILE_CACHE_ZOOM_LEVEL);

        for (int x = topLeft.getTileX(); x <= bottomRight.getTileX(); x++) {
            for (int y = topLeft.getTileY(); y <= bottomRight.getTileY(); y++) {
                MapTileUpdateResource mapTileResource = mapTileUpdateResourceDao.getMapTileUpdate(MAPTILE_CACHE_LAYER, x, y,
                        MAPTILE_CACHE_ZOOM_LEVEL);

                if (force) {
                    mapTileResource = new MapTileUpdateResource(MAPTILE_CACHE_LAYER, x, y, MAPTILE_CACHE_ZOOM_LEVEL);
                    queueTrapRequest(mapTileResource);
                } else {
                    long lastUpdateTime = 0;
                    if (mapTileResource != null) {
                        lastUpdateTime = mapTileResource.getLastUpdate();

                        long delta = System.currentTimeMillis() - lastUpdateTime;

                        if (delta > (MAPTILE_EXPIRY_IN_SECONDS * 1000))
                            queueTrapRequest(mapTileResource);
                    } else {
                        mapTileResource = new MapTileUpdateResource(MAPTILE_CACHE_LAYER, x, y, MAPTILE_CACHE_ZOOM_LEVEL);
                        queueTrapRequest(mapTileResource);
                    }
                }

            }
        }
    }

    private void queueTrapRequest(MapTileUpdateResource resource) {

        //Log.v(LOGNAME,"Trap Sync. Requesting tile update:"+resource.getReference());

        // NOTE: current web service (alrtserv2) limits search to 10
        // Mile radius
        // tileRadiusInMiles must be slightly less than 10 Mile
        // limit
        // MAPTILE_CACHE_ZOOM_LEVEL must be set accordingly
        // radius should be slightly larger than half tile diagonal
        // to encompass entire tile
        double tileRadiusInMiles = GeographicUtils.kilometersToMiles(resource
                .getDiagonalRadiusForBoundingBox() / 1000);
        api.updateAllTraps(resource.getCenterLatitude(), resource.getCenterLongitude(),
                tileRadiusInMiles, trapListener, trapListener);

        //if(timeoutHandler != null)
        //    timeoutHandler.removeMessages(0);

        //timeoutHandler.sendEmptyMessageDelayed(0,TIMEOUT_IN_MS);


    }


    public void addOpenConnection() {
        connections = connections + 1;
    }

    public void closeConnection() {
        connections = connections - 1;
    }

    private void updateTraps() {
        synchronized (trapList) {
            trapDao.save(trapList);
            trapList = new ArrayList<Trap>();
        }

        if (timeoutHandler != null)
            timeoutHandler.removeMessages(0);
    }


    private class TrapDownloadListener implements CommunicationStatusListener,
            TrapListener {

        @Override
        public void onOpenConnection() {

            addOpenConnection();
        }

        @Override
        public void onCloseConnection() {
            closeConnection();
        }

        @Override
        public void onConnectionError(String errorMessage) {
        }

        @Override
        public void onError(TrapsterError error) {

        }

        @Override
        public void onTraps(Traps traps) {

            //android.util.Log.v(LOGNAME,"Syncing "+traps.getTraps().size()+" traps with db");

            MapTileUpdateResource newMapTileResource = MapTileUpdateResource
                    .createFromPoint(MAPTILE_CACHE_LAYER, traps.getLat(), traps.getLon(), MAPTILE_CACHE_ZOOM_LEVEL);

            mapTileUpdateResourceDao.save(newMapTileResource, System.currentTimeMillis());


            // Get list of existing traps for the tile
            List<Trap> existingTraps = trapDao.getTrapsByTile(newMapTileResource.getReference());
            //Log.v(LOGNAME,"Trap sync. Traps in tile:"+existingTraps.size()+" index:"+newMapTileResource.getReference());

            // Convert to sparsearray for searching
            HashMap<Integer, Trap> existingTrapIndex = new HashMap<Integer, Trap>();
            for (Trap trap : existingTraps)
                existingTrapIndex.put(trap.getId(), trap);


            int added = 0;
            int changed = 0;
            int deleted = 0;
            if (traps.getTraps() != null) {
                ArrayList<Trap> trapAddedList = new ArrayList<Trap>();

                for (Trap trap : traps.getTraps()) {

                    trap.setTileIndex(newMapTileResource.getReference());

                    /**
                     * Check if the trap has been changed
                     */
                    if (existingTrapIndex.get(trap.getId()) != null) {
                        if (!trap.equals(existingTrapIndex.get(trap.getId()))) {
                            changed++;
                            trapAddedList.add(trap);
                        }

                        existingTrapIndex.remove(trap.getId());
                    } else {

                        added++;
                        /**
                         * New trap to be added
                         */
                        trapAddedList.add(trap);
                    }

                }

                trapDao.save(trapAddedList);
            }

            /**
             * Remove the list of traps in the list
             */
            for (int key : existingTrapIndex.keySet()) {

                Trap removedTrap = existingTrapIndex.get(key);

                trapMapLayer.removeTrap(removedTrap);
                trapDao.delete(removedTrap);

                deleted++;

            }

            //Log.v(LOGNAME,"Trap sync. "+added+" added, "+changed+" changed,"+deleted+" deleted traps index:"+newMapTileResource.getReference());



			/*
             * @TODO Don't update traps that are already in the database. Remove
			 * traps not in the query
			 * 
			 * NOT TESTED YET
			 *//*
			ArrayList<Integer> includedTrapIds = new ArrayList<Integer>();
			for (Trap trap : traps.getTraps())
				includedTrapIds.add(trap.getId());
			
			
			List<Trap> deletedTraps = trapDao.getListOfDeletedTraps(newMapTileResource, includedTrapIds);
			
			if(deletedTraps.size() > 0)
            {
				for(Trap trap:deletedTraps)
                {
					trapMapLayer.removeTrap(trap);
					trapDao.delete(trap);
				}
			}

            trapDao.save(traps.getTraps());*/

        }

    }


}
