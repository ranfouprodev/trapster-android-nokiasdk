package com.trapster.android.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.trapster.android.model.RouteLink;
import roboguice.service.RoboIntentService;

import java.util.Set;

public class RouteLinkBroadcastReceiver extends BroadcastReceiver
{
    static final String LOGNAME= "Trapster.NavteqRouteLinkBroadcastReceiver";
    //
    public static final String INTENT_BROADCAST= "com.trapster.android.ROUTE_LINK_UPDATE";
    public static final String INTENT_ROUTE_LINK= "intentroutelink";
    public static final String INTENT_APPROXIMATE_ROUTE_LINKS= "intentapproximateroutelinks";
    public static final String INTENT_RESULT_CODE= "intentresultcode";
    //
    private static RouteLinkNotificationListener listener;
    //
    public RouteLinkBroadcastReceiver(RouteLinkNotificationListener listener)
    {
        this.listener= listener;
    }
    public void destroy()
    {
        this.listener= null;
    }
    public void onReceive(Context context, Intent intent)
    {
        final RouteLinkNotificationListener listener= this.listener;
        //
        if (listener != null)
        {
            intent.setClass(context, RouteLinkFilterIntentService.class);
            context.startService(intent);
        }
    }

    public static class RouteLinkFilterIntentService extends RoboIntentService
    {
        public RouteLinkFilterIntentService()
        {
            super("RouteLinkFilterIntentService");
        }

        @Override
        protected void onHandleIntent(Intent intent)
        {
            RouteLink routeLink= (RouteLink) intent.getSerializableExtra(INTENT_ROUTE_LINK);
            Set<RouteLink> approximateRouteLinks= (Set<RouteLink>) intent.getSerializableExtra(INTENT_APPROXIMATE_ROUTE_LINKS);
            Integer resultCode= (Integer) intent.getIntExtra(RouteLinkBroadcastReceiver.INTENT_RESULT_CODE, -1);
            // Logger.e(LOGNAME, "broadcastRouteLink!!!" + resultCode);
            if (listener != null)
                listener.onRouteLinkUpdate(routeLink, approximateRouteLinks, resultCode);
        }
    }
}