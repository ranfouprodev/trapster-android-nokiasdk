package com.trapster.android.service.filter;

import java.util.List;

import android.content.Context;

import com.trapster.android.model.Trap;
import com.trapster.android.service.UserLocation;
import com.trapster.android.util.GeographicUtils;

/**
 *
 * Assumes the traps are already sorted by distance
 *
 *
 *
 *
 */
public class PrioritizeNearbyFilter extends TrapFilter {

    private static final String LOGNAME = "Trapster.ReorderDuplicatesFilter";

    private static final double MIN_DUPLICATE_NOTIFICATION_DISTANCE = 50;

    public PrioritizeNearbyFilter() {

    }

    @Override
    public List<Trap> getTrapList(UserLocation location, List<Trap> currentList) {

        for(int i=0;i < currentList.size()-1;i++){
            Trap current = currentList.get(i);
            Trap next = currentList.get(i+1);

            double distance = GeographicUtils.geographicDistance(current.getLatitude(), current.getLongitude(),
                    next.getLatitude(), next.getLongitude());

            if(distance < MIN_DUPLICATE_NOTIFICATION_DISTANCE){
                int currentPriority = current.getTypeid();
                int nextPriority = next.getTypeid();

                if(currentPriority <= nextPriority)
                    currentList.remove(i);
                else
                    currentList.remove(i+1);
            }

        }

        return currentList;

    }


}
