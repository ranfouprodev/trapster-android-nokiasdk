package com.trapster.android.service.notification;

import java.util.ArrayList;
import java.util.List;

import android.app.Application;
import android.content.Context;
import android.content.Intent;

import com.google.inject.Inject;
import com.trapster.android.model.Trap;
import com.trapster.android.model.Traps;
import com.trapster.android.receiver.TrapsterServiceBroadcastReceiver;

public class BroadcastNotifier extends Notifier {

    @Inject Application application;

    public BroadcastNotifier() {


    }

    @Inject
    public void Init(){

    }

    @Override
    void send(List<Trap> trap, boolean isBackground) {

        Traps traps = new Traps();
        traps.setTraps((ArrayList<Trap>) trap);

        Intent i = new Intent(TrapsterServiceBroadcastReceiver.INTENT_BROADCAST);
        i.putExtra(TrapsterServiceBroadcastReceiver.INTENT_NOTIFICATION_TYPE, TrapsterServiceBroadcastReceiver.INTENT_UPDATE_TRAP_NOTIFICATION);
        i.putExtra(TrapsterServiceBroadcastReceiver.INTENT_UPDATE_TRAPS, traps);

        application.getApplicationContext().sendBroadcast(i);
    }



}
