package com.trapster.android.service.filter;

import android.content.Context;

import com.trapster.android.model.Trap;
import com.trapster.android.service.UserLocation;

import java.util.ArrayList;
import java.util.List;

/**
 * Returns only the first trap in the list
 *
 *
 */
public class RemoveAllButTopFilter extends TrapFilter {


    public RemoveAllButTopFilter() {

    }

    @Override
    public List<Trap> getTrapList(UserLocation location, List<Trap> currentList) {


        ArrayList<Trap> revisedTrapList = new ArrayList<Trap>();

        if(currentList.size() > 0)
            revisedTrapList.add(currentList.get(0));

        return revisedTrapList;

    }

}
