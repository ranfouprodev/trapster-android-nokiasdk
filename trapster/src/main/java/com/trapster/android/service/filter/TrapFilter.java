package com.trapster.android.service.filter;

import java.util.List;
import android.content.Context;

import com.trapster.android.model.Trap;
import com.trapster.android.service.UserLocation;

public abstract class TrapFilter {



    public abstract List<Trap> getTrapList(UserLocation location, List<Trap> currentList);


}
