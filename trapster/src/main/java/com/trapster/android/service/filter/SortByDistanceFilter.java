package com.trapster.android.service.filter;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import android.content.Context;
import android.location.Location;
import com.here.android.common.GeoPosition;
import com.trapster.android.model.Trap;
import com.trapster.android.service.UserLocation;
import com.trapster.android.util.GeographicUtils;

public class SortByDistanceFilter extends TrapFilter {

    private static final String LOGNAME = "Trapster.SortByDistanceFilter";


    public SortByDistanceFilter() {

    }

    @Override
    public List<Trap> getTrapList(UserLocation location, List<Trap> currentList) {

        //Location position= location.getLocation();
        GeoPosition position = location.getLocation();
        if (position != null) {
            // Using Priority Queue and Trap Notifier
            Collections.sort(currentList, new PriorityTrapComparator(position.getCoordinate().getLatitude(),
                    position.getCoordinate().getLongitude()));
        }

        return currentList;

    }


    private class PriorityTrapComparator implements Comparator<Trap> {

        private double lat;
        private double lon;

        public PriorityTrapComparator(double _lat, double _lon) {
            lat = _lat;
            lon = _lon;
        }

        public int compare(Trap t1, Trap t2) {

            double d1 = GeographicUtils.geographicDistance(lat, lon, t1
                    .getLatitude(), t1.getLongitude());
            double d2 = GeographicUtils.geographicDistance(lat, lon, t2
                    .getLatitude(), t2.getLongitude());

            return new Double(d1).compareTo(new Double(d2));
        }
    }
}

