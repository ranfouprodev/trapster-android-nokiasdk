package com.trapster.android.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;

import com.google.inject.Inject;
import com.here.android.common.GeoPosition;
import com.here.android.common.LocationMethod;
import com.here.android.common.LocationStatus;
import com.here.android.common.PositionListener;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.activity.MapScreen;
import com.trapster.android.comms.ITrapsterAPI;
import com.trapster.android.comms.TrapListener;
import com.trapster.android.comms.TrapUpdateMonitorListener;
import com.trapster.android.manager.FlurryManager;
import com.trapster.android.manager.MapController;
import com.trapster.android.manager.PatrolManager;
import com.trapster.android.manager.PositionManager;
import com.trapster.android.manager.SessionManager;
import com.trapster.android.manager.TrapManager;
import com.trapster.android.manager.TrapUpdateManager;
import com.trapster.android.manager.flurry.FLURRY_EVENT_NAME;
import com.trapster.android.manager.flurry.FLURRY_PARAMETER_NAME;
import com.trapster.android.manager.flurry.FlurryEvent;
import com.trapster.android.model.Trap;
import com.trapster.android.model.Traps;
import com.trapster.android.receiver.TrapsterServiceBroadcastReceiver;
import com.trapster.android.service.filter.TrapFilterManager;
import com.trapster.android.service.filter.TrapFilterManager.FILTERS;
import com.trapster.android.service.notification.PlaySoundNotifier;
import com.trapster.android.service.notification.TrapNotificationManager;
import com.trapster.android.service.notification.TrapNotificationManager.NOTIFIERS;
import com.trapster.android.util.GeographicUtils;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import roboguice.service.RoboService;

public class TrapsterService extends RoboService implements
		TrapUpdateMonitorListener, CommunicationStatusListener, TrapListener {

	public static final String SERVICE_INTENT = "com.trapster.TRAPSTER_SERVICE";

	private static final String LOGNAME = "Trapster.TrapsterService";

	private static final int BACKGROUND_SERVICE_ID = 123;
/*    private static final long BACKGROUND_TIMEOUT = 1000 * 60 * 3; // 3 minutes
    private static final long BACKGROUND_TIMEOUT_CHECK_INTERVAL = 1000 * 30; // 30 seconds*/
    private static final double BACKGROUND_TIMEOUT_GPS_DISTANCE = 50.0; // meters

    public static final String BACKGROUND_TIMER_EVENT = "com.trapster.android.HOME_KEY_PRESSED";

    private static final Handler handler = new Handler(Looper.getMainLooper());
/*    private final BackgroundTimeoutRunnable backgroundTimeoutRunnable = new BackgroundTimeoutRunnable();*/

    private final LinkedList<FILTERS> trapFilters = new LinkedList<FILTERS>();
    private final ArrayList<NOTIFIERS> notifyFilters = new ArrayList<NOTIFIERS>();

	@Inject
	SessionManager sessionManager;

	@Inject
	TrapManager trapManager;
	@Inject
	PatrolManager patrolManager;

	@Inject
	PositionManager positionManager;
	@Inject
	RouteLinkUpdateMonitor routeLinkUpdateMonitor;
	@Inject
	TrapNotificationManager trapNotifier;
	@Inject
	TrapFilterManager filter;
	@Inject
	ITrapsterAPI api;
	@Inject
	MapController mapController;
    @Inject PlaySoundNotifier playSoundNotifier;

	@Inject SharedPreferences sharedPreferences;
    @Inject TrapUpdateManager trapUpdateManager;

	private GeoPosition current;
    private long lastPositionUpdate;

    private double backgroundDistanceTravelled = 0.0;
    private TrapUpdateManager.TrapUpdater trapUpdater = new TrapUpdateSoundPlayer();

	public static enum STATE {
		running, stopped, idle
	};

	public static STATE state = STATE.stopped;

	protected boolean background = false;

	public static boolean detectionActive = false;

	private ConstantPositionListener constantPositionListener;

	private TrapUpdateMonitor trapUpdateMonitor;

	 static final String NAME="com.trapster.android.TRAPSTER_SERVICE";

	  private static volatile PowerManager.WakeLock lockStatic=null;

    private static final String WAKELOCK_NAME_DIM="com.trapster.android.TRAPSTER_MAP_DIM";
    private static final String WAKELOCK_NAME_BRIGHT="com.trapster.android.TRAPSTER_MAP_BRIGHT";
    private static final String WAKELOCK_NAME_PARTIAL = "com.trapster.android.TRAPSTER_WAKELOCK_PARTIAL";

    public synchronized static PowerManager.WakeLock getLock(Context context, int wakeLockType) {
        PowerManager mgr =(PowerManager)context.getSystemService(Context.POWER_SERVICE);

        switch (wakeLockType)
        {
            case PowerManager.SCREEN_DIM_WAKE_LOCK:
            {
                return mgr.newWakeLock(wakeLockType, WAKELOCK_NAME_DIM);
            }
            case PowerManager.SCREEN_BRIGHT_WAKE_LOCK:
            {
                return mgr.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, WAKELOCK_NAME_BRIGHT);
            }
            case PowerManager.PARTIAL_WAKE_LOCK:
            {
                return mgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, WAKELOCK_NAME_PARTIAL);
            }
        }

        return null;
    }


	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}


	@Override
	public void onCreate() {
		super.onCreate();

        lastPositionUpdate = System.currentTimeMillis();

        /*
           * Main filter for trap. Continuously updates the nearest traps. No time
           * base filters
           */
        trapFilters.add(FILTERS.conetrap);
        trapFilters.add(FILTERS.settings);
        trapFilters.add(FILTERS.expirytrap);
        trapFilters.add(FILTERS.routelink);
        trapFilters.add(FILTERS.sortbydistance);
        trapFilters.add(FILTERS.prioritizenearby);
        trapFilters.add(FILTERS.removeallbuttop);

        notifyFilters.add(NOTIFIERS.playsound);
        notifyFilters.add(NOTIFIERS.broadcast);
        notifyFilters.add(NOTIFIERS.statusbar);

		positionManager.start(PositionManager.METHOD_GPS);
		/*try {
        		positionManager.start(PositionManager.METHOD_GPX_PLAYBACK, getAssets().open("gpx/Test.gpx") );
        	} catch (IOException e) {
        		android.util.Log.e(LOGNAME, "Unable to open GPX file" + e.getMessage());
        	}*/


		startForeground(BACKGROUND_SERVICE_ID, getRunningNotification());
		startBackgroundServices();

		// else started from service
		startUp();

		broadcastStatusChange(STATE.running);
        TrapUpdateManager.addListener(trapUpdater);
	}


	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
        PowerManager.WakeLock lock=getLock(this.getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);

	    if (!lock.isHeld()) {
	      lock.acquire();
	    }


		FlurryManager.startDataTrafficMonitoring();

/*        if (intent != null && intent.hasExtra(BACKGROUND_TIMER_EVENT))
        {
            if (intent.getBooleanExtra(BACKGROUND_TIMER_EVENT, false))
            {
                lastPositionUpdate = System.currentTimeMillis();
                handler.postDelayed(backgroundTimeoutRunnable, BACKGROUND_TIMEOUT_CHECK_INTERVAL);
            }
            else
            {
                handler.removeCallbacks(backgroundTimeoutRunnable);
            }

        }*/

        autoLogin();
	    return START_STICKY;
	}

    private void autoLogin()
    {
        if (sessionManager.hasCredentials() && !sessionManager.isLoggedIn())
            sessionManager.autoLogin(null);
    }


	@Override
	public void onDestroy() {
		super.onDestroy();
{/* 		Log.i(LOGNAME, "Shutting Down Trapster Service"); */}

		trapUpdateMonitor.stop();

		positionManager.removePositionListener(constantPositionListener);
		positionManager.stop();


        PowerManager.WakeLock lock=getLock(this.getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);

	    if (lock.isHeld() ) {
	     lock.release();
	    }

        broadcastStatusChange(STATE.stopped);


        FlurryManager.endDataTrafficMonitoring();
        TrapUpdateManager.removeListener(trapUpdater);

        sessionManager.stop();

	}

	private void startUp() {
		// Log.i(LOGNAME, "Starting Trapster Service");
		trapUpdateMonitor = new TrapUpdateMonitor(
				Defaults.BACKGROUND_TRAPS_UPDATE_DISTANCE,
				(int) Defaults.BACKGROUND_TRAPS_UPDATE_TIME, this);
	}

	private void startBackgroundServices() {
		// Log.i(LOGNAME, "Trap startBackgroundServices");
		background = true;
		detectionActive = true;
		startLocationServices();
	}

	private void startLocationServices() {
		// Log.i(LOGNAME, "Trap startLocationServices");

		constantPositionListener = new ConstantPositionListener();
		positionManager.addPositionListener(constantPositionListener);
	}

	private Notification getRunningNotification() {
		// Log.i(LOGNAME, "Trap getRunningNotification");

		String text = getString(R.string.notification_status_text);

		Intent notificationIntent = new Intent(this, MapScreen.class);
		notificationIntent.putExtra("RESTART", true);
		notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		notificationIntent.addFlags(Intent.FLAG_FROM_BACKGROUND);
		notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		PendingIntent launchIntent = PendingIntent.getActivity(this,
				Defaults.PENDING_INTENT_START_SCREEN_FROM_BACKGROUND,
				notificationIntent, 0);

		NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

		builder.setContentIntent(launchIntent)
				.setSmallIcon(R.drawable.notification_bar_logo)
				.setLargeIcon(
						BitmapFactory.decodeResource(this.getResources(),
								R.drawable.notification_bar_logo))
				.setTicker(text).setWhen(System.currentTimeMillis())
				.setAutoCancel(false).setContentTitle(text)
				.setContentText(text);

		Notification notification = builder.getNotification();

		return notification;

	}

	/*
	 * From the current location check for traps
	 */
	private void checkTraps()
    {
		UserLocation userLocation = new UserLocation();

		userLocation.setLocation(current);

		List<Trap> notificationTraps = filter.filter(new ArrayList<Trap>(), userLocation,trapFilters);

        if (notificationTraps.size() > 0)
        {
            //Log.i("BBQ", "Sending notification at " + System.currentTimeMillis());
            trapUpdateManager.notifyTrapUpdate(notificationTraps);
            //trapNotifier.sendTrapNotification(userLocation, notificationTraps, background,notifyFilters);
        }

	}

	private void requestTrapUpdate(double lat, double lng, double radius) {
		// Log.i(LOGNAME, "Trap requestTrapUpdate");

		trapManager.requestTrapUpdate(lat, lng, radius);
	}

	private void requestPatrolUpdate(double lat, double lng, double radius) {
		// Log.i(LOGNAME, "Trap requestTrapUpdate");

		patrolManager.requestPatrolUpdate(lat, lng, radius);
	}
	
	
	
	private void updateCurrentLocation(GeoPosition geoPosition) {

        if (current != null)
        {
            double startLat = current.getCoordinate().getLatitude();
            double startLng = current.getCoordinate().getLongitude();
            double endLat = geoPosition.getCoordinate().getLatitude();
            double endLng = geoPosition.getCoordinate().getLongitude();
/*            Coordinate oldCoordinate = new Coordinate(current.getCoordinate().getLongitude(), current.getCoordinate().getLatitude());
            Coordinate currentCoordinate = new Coordinate(geoPosition.getCoordinate().getLongitude(), geoPosition.getCoordinate().getLatitude());*/

            double distance = GeographicUtils.geographicDistance(startLat, startLng, endLat, endLng);

            if (distance >= BACKGROUND_TIMEOUT_GPS_DISTANCE)
            {
                lastPositionUpdate = System.currentTimeMillis();
                current = geoPosition;
            }

        }
        else
        {
            current = geoPosition;
        }

	}

	private void broadcastStatusChange(STATE _state) {

		state = _state;

		Intent i = new Intent(TrapsterServiceBroadcastReceiver.INTENT_BROADCAST);
		i.putExtra(TrapsterServiceBroadcastReceiver.INTENT_NOTIFICATION_TYPE,
				TrapsterServiceBroadcastReceiver.INTENT_UPDATE_STATUS);
		i.putExtra(TrapsterServiceBroadcastReceiver.INTENT_STATUS,
				state.toString());

		sendBroadcast(i);

	}

	private void broadcastError(int code, String message) {

		Intent i = new Intent(TrapsterServiceBroadcastReceiver.INTENT_BROADCAST);
		i.putExtra(TrapsterServiceBroadcastReceiver.INTENT_NOTIFICATION_TYPE,
				TrapsterServiceBroadcastReceiver.INTENT_UPDATE_ERROR);
		i.putExtra(TrapsterServiceBroadcastReceiver.INTENT_ERROR_ID, code);
		i.putExtra(TrapsterServiceBroadcastReceiver.INTENT_ERROR_MESSAGE,
				message);

		sendBroadcast(i);

	}



	@Override
	public void onTrapUpdateRequest(double lat, double lng, double radius) {
		
		double radiusInMiles = GeographicUtils.kilometersToMiles(radius/1000);
		
		requestTrapUpdate(lat, lng, radiusInMiles);
		
		requestPatrolUpdate(lat, lng, radiusInMiles);
	}

	@Override
	public void onCloseConnection() {
		// nothing
	}

	@Override
	public void onConnectionError(String errorMessage) {
		// Error updating traps
		// Log.i(LOGNAME, "Trap onConnectionError");
		broadcastError(0, errorMessage);

	}

	@Override
	public void onOpenConnection() {
		// nothing
	}

	@Override
	public void onTraps(Traps traps) {
		// nothing here
	}

	@Override
	public void onError(TrapsterError error) {
		// Error updating the traps.
		// Log.i(LOGNAME, "Trap onError");
		broadcastError(0, error.getDetails());
	}

/*    private void checkForBackgroundTimeout()
    {
        long timeDifference = (System.currentTimeMillis() - lastPositionUpdate);

        if (timeDifference >= BACKGROUND_TIMEOUT)
        {
            sendFlurryIdleOffEvent();
            // turn off
            stopSelf();
            stopForeground(true);
        }
        else
        {
            handler.postDelayed(backgroundTimeoutRunnable, BACKGROUND_TIMEOUT_CHECK_INTERVAL);
        }


    }*/

	
	private class ConstantPositionListener implements PositionListener {

		@Override
		public void onPositionUpdated(LocationMethod locationMethod,
				GeoPosition geoPosition) {

			// Check for traps if necessary
			if (trapUpdateMonitor != null)
				trapUpdateMonitor.update(geoPosition);

			updateCurrentLocation(geoPosition);
			
						
			// Check for traps
			if (detectionActive && sessionManager.isAttributesLoaded())
				checkTraps();


			// Update Route Link Bounding Box
			routeLinkUpdateMonitor.onLocationChanged(geoPosition);

		}

		@Override
		public void onPositionFixChanged(LocationMethod locationMethod,
				LocationStatus locationStatus) {

		}
	}

/*    private class BackgroundTimeoutRunnable implements Runnable
    {

        @Override
        public void run()
        {
            checkForBackgroundTimeout();
        }
    }*/

    private void sendFlurryIdleOffEvent()
    {
        FlurryEvent event = FlurryManager.createEvent(FLURRY_EVENT_NAME.IDLE_OFF);

        event.putParameter(FLURRY_PARAMETER_NAME.LOCATION_STATE, FlurryManager.getStateCode());
        event.putParameter(FLURRY_PARAMETER_NAME.LOCATION_COUNTRY, FlurryManager.getCountryCode());

        event.send();
    }

    private class TrapUpdateSoundPlayer implements TrapUpdateManager.TrapUpdater
    {
        @Override
        public void onTrapUpdate(Trap trap)
        {
            playSoundNotifier.send(trap);
        }
    }



}