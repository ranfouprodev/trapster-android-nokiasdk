package com.trapster.android.service.filter;

import android.content.SharedPreferences;
import com.google.inject.Inject;
import com.trapster.android.Defaults;
import com.trapster.android.manager.TrapManager;
import com.trapster.android.model.Trap;
import com.trapster.android.model.TrapType;
import com.trapster.android.service.UserLocation;

import java.util.ArrayList;
import java.util.List;

/**
 * Removes any traps that the user has ignored in the settings.
 *
 *
 */
public class SettingsFilter extends TrapFilter {

    @Inject TrapManager tm;
    @Inject SharedPreferences sharedPreferences;
    @Inject TrapManager trapManager;



    public SettingsFilter() {

    }

    @Inject
    public void Init (){

    }

    @Override
    public List<Trap> getTrapList(UserLocation location, List<Trap> currentList) {


        ArrayList<Trap> revisedTrapList = new ArrayList<Trap>();

        for(Trap trap: currentList){

            TrapType type = tm.getTrapType(trap.getTypeid());

            TrapType trapType = trapManager.getTrapType(type.getId());
            boolean  isAlertable = sharedPreferences.getBoolean(trapType.getAudioid() + Defaults.SETTING_ALERTING, trapType.isAlertable());
            //check if the trap is alertable and notifiable
            if(isAlertable)
                revisedTrapList.add(trap);
        }


        return revisedTrapList;

    }

}
