package com.trapster.android.service.filter;

import android.app.Application;
import android.content.Context;
import com.google.inject.Inject;
import com.trapster.android.model.Trap;
import com.trapster.android.service.UserLocation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class TrapFilterManager {

    @Inject Application application;
    @Inject RouteLinkFilter routeLinkFilter;
    @Inject SettingsFilter settingsFilter;
    @Inject ConeTrapFilter coneTrapFilter;
    @Inject ExpiryTrapFilter expiryTrapFilter;

    private Context context;

    private HashMap<FILTERS,TrapFilter> filters = new HashMap<FILTERS,TrapFilter>();


    public static enum FILTERS { conetrap, settings, routelink, sortbydistance, prioritizenearby, removeallbuttop, expirytrap}


//    public TrapFilterManager(Context context) {
//        super();
//        this.context = context;
//        addFilters();
//    }
    @Inject
    public void Init(){
        this.context = application.getApplicationContext();
        addFilters();
    }
    public void TrapFilterManager() {

    }

    public List<Trap> filter(List<Trap> traps, UserLocation userLocation, LinkedList<FILTERS> filterList){

        if(traps == null)
            traps = new ArrayList<Trap>();

        for(FILTERS f: filterList){

            TrapFilter filter = filters.get(f);

            if(filter != null)
                traps = filter.getTrapList(userLocation, traps);
        }

        return traps;
    }



    private void addFilters(){
        filters.put(FILTERS.conetrap,coneTrapFilter);
        filters.put(FILTERS.settings,settingsFilter);
        filters.put(FILTERS.routelink,routeLinkFilter);
        filters.put(FILTERS.sortbydistance,new SortByDistanceFilter());
        filters.put(FILTERS.prioritizenearby,new PrioritizeNearbyFilter());
        filters.put(FILTERS.removeallbuttop,new RemoveAllButTopFilter());
        filters.put(FILTERS.expirytrap,expiryTrapFilter);


    }




}