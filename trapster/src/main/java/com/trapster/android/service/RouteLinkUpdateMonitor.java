package com.trapster.android.service;

import java.util.HashSet;
import java.util.Set;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
//import android.location.Location;
import com.google.inject.Inject;
import com.here.android.common.GeoPosition;
import com.trapster.android.Defaults;
import com.trapster.android.TrapsterApplication;
import com.trapster.android.manager.RouteLinkManager;
import com.trapster.android.manager.UpdatedRouteLinkManager;
import com.trapster.android.manager.RouteLinkManager.OnNearestRouteLinkUpdateListener;
import com.trapster.android.model.RouteLink;
import com.trapster.android.model.UpdatedRouteLinkInfo;
import com.trapster.android.util.Log;

public class RouteLinkUpdateMonitor implements RouteLinkManager.OnNearestRouteLinkUpdateListener
{
    static final String LOGNAME= "Trapster.RouteLinkUpdateMonitor";
    // Result Codes
    public static final int RESULT_CODE_SUCCESS= 0;
    public static final int RESULT_CODE_NO_LINK= 1;
    public static final int RESULT_CODE_FETCH_TILE= 2;
    //

    Context mContext;
    //
    @Inject SharedPreferences mSettings;

    @Inject RouteLinkManager mRouteLinkManager;
    @Inject Application application;
    //
    GeoPosition mPosition;
    static private RouteLink mBroadcastRouteLink;
    static HashSet<RouteLink> mBroadcastApproximateRouteLinks;
    //

    public RouteLinkUpdateMonitor() {


    }

    @Inject
    public void init() {
        this.mContext = application.getApplicationContext();

    }




    void broadcastRouteLink(RouteLink nearestRouteLink)
    {
        if (nearestRouteLink != null)
        {
            if (mBroadcastRouteLink == null || !mBroadcastRouteLink.equals(nearestRouteLink))
            {

                HashSet<RouteLink> approximateRouteLinks = mRouteLinkManager.getProjectedRouteLinks();
                // broadcast updated route info
                Intent i= new Intent(RouteLinkBroadcastReceiver.INTENT_BROADCAST);
                i.putExtra(RouteLinkBroadcastReceiver.INTENT_ROUTE_LINK, nearestRouteLink);
                i.putExtra(RouteLinkBroadcastReceiver.INTENT_APPROXIMATE_ROUTE_LINKS, approximateRouteLinks);
                i.putExtra(RouteLinkBroadcastReceiver.INTENT_RESULT_CODE, RESULT_CODE_SUCCESS);
                mContext.sendBroadcast(i);
                //

                mBroadcastRouteLink= nearestRouteLink;
                mBroadcastApproximateRouteLinks= approximateRouteLinks;
            }
        }
        else
        {
            wipeoutBroadcast();
            // broadcast to listeners no link found
            Intent i= new Intent(RouteLinkBroadcastReceiver.INTENT_BROADCAST);
            i.putExtra(RouteLinkBroadcastReceiver.INTENT_RESULT_CODE, RESULT_CODE_NO_LINK);
            mContext.sendBroadcast(i);
        }
    }    static public Set<RouteLink> getBroadcastApproximateRouteLinks()
{
    return mBroadcastApproximateRouteLinks;
}
    static public RouteLink getBroadcastRouteLink()
    {
        return mBroadcastRouteLink;
    }
    public void invalidate()
    {
        wipeoutBroadcast();
        onLocationChanged(mPosition);
    }

    public void onLocationChanged(GeoPosition location)
    {

        if (location != null)
        {
            mPosition= location;
            //
            /*
                * Sam Nazi
                *
                * location.getSpeed() used to display speed limit only when
                * traveling over 10mph
                */
            if ((mPosition.getSpeed() * 2.23693629) > 10)
            {
                if (mRouteLinkManager.requestNearestRouteLinkUpdate(location,mBroadcastRouteLink, this) == 1)
                {
                    if(mBroadcastRouteLink == null)
                    {
                        wipeoutBroadcast();
                        // broadcast to listeners tile being fetched
                        Intent i= new Intent(RouteLinkBroadcastReceiver.INTENT_BROADCAST);
                        i.putExtra(RouteLinkBroadcastReceiver.INTENT_RESULT_CODE, RESULT_CODE_FETCH_TILE);
                        mContext.sendBroadcast(i);
                    }
                }

            }
        }

    }
    @Override
    public void onNearestRouteLinkUpdated(RouteLink nearestRouteLinks)
    {
        broadcastRouteLink(nearestRouteLinks != null ? nearestRouteLinks : null);
    }
    void wipeoutBroadcast()
    {
        mBroadcastRouteLink= null;
        mBroadcastApproximateRouteLinks= null;
    }
}
