package com.trapster.android.service;

import android.content.Intent;
import android.location.Location;
import com.google.inject.Inject;
import com.trapster.android.comms.ITrapsterAPI;
import com.trapster.android.model.ProbeData;
import com.trapster.android.model.dao.ProbeDataDAO;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;
import roboguice.service.RoboIntentService;

import java.util.List;

public class ProbeDataUpdateService extends RoboIntentService {

	public static final String EXTRA_KEY_LOCATION = "EXTRA_KEY_LOCATION";
	
	private static final String LOGNAME = "ProbeDataUpdateService";

	
	/*
	 * Minimum number of points (or time) to sync up to the server. Avoids clogging the comms channels
	 */
    private static final int PROBE_DATA_TIME_TO_SYNC = 1000 * 60; // 60 Seconds between syncs
	private static final int PROBE_DATA_SIZE_TO_SYNC = 5;
    private static long lastSyncTime = System.currentTimeMillis(); // No need to send immediately, send after first minute so we can collect data
	
	@Inject
	ProbeDataDAO probeDao;
	
	@Inject
	ITrapsterAPI api;
	
	public ProbeDataUpdateService() {
		super(LOGNAME);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		if(intent != null && intent.hasExtra(EXTRA_KEY_LOCATION)){
			Location location = (Location) intent.getExtras().get(EXTRA_KEY_LOCATION);
			
			createNewProbeDataLocation(location); 
			
			syncProbeDataWithServer(); 

		}
	}

	/**
	 * Removes probe data points thats are synchronized with the server from the local db
	 * @param probeDataPoints 
	 */
	private void cleanOldLocations(List<ProbeData> probeDataPoints) {
		for(ProbeData probeData: probeDataPoints)
			probeDao.delete(probeData.getId());
		
	}

	/**
	 * Pushes the probe data up to the service. This should be configured to only send batches up to the server (i.e. 5 points min) 
	 * 
	 * @TODO we are NOT verifying the locations have been sent up. Not sure if that is a requirement
	 */
	private void syncProbeDataWithServer() {
		List<ProbeData> probeDataPoints = probeDao.findAll();
		
		if(lastSyncTime + PROBE_DATA_TIME_TO_SYNC < System.currentTimeMillis() && probeDataPoints != null && probeDataPoints.size() > PROBE_DATA_SIZE_TO_SYNC)
        {
			api.sendProbeData(probeDataPoints, new UpdateProbeDataListener());
			cleanOldLocations(probeDataPoints);
            lastSyncTime = System.currentTimeMillis();
		}
		
	}

	/*
	 * Creates a new probe data location that is stored on the server
	 */
	private boolean createNewProbeDataLocation(Location location) {
		//Log,.v(LOGNAME, "Creating new probe data location");
		ProbeData probe = new ProbeData(location);
		
		probeDao.save(probe);
		
		return true; 
	}

	private class UpdateProbeDataListener implements CommunicationStatusListener{

		@Override
		public void onOpenConnection() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onCloseConnection() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onConnectionError(String errorMessage) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
}
