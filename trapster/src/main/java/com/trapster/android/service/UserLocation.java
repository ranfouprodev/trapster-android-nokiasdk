package com.trapster.android.service;

import com.here.android.common.GeoPosition;

public class UserLocation {

    private GeoPosition location;
    private String roadSegment;

    public GeoPosition getLocation() {
        return location;
    }
    public void setLocation(GeoPosition location) {
        this.location = location;
    }
    public String getRoadSegment() {
        return roadSegment;
    }
    public void setRoadSegment(String roadSegment) {
        this.roadSegment = roadSegment;
    }


}
