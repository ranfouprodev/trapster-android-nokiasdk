package com.trapster.android.service.filter;

import com.google.inject.Inject;
import com.trapster.android.manager.RouteLinkManager;
import com.trapster.android.manager.TrapManager;
import com.trapster.android.model.RouteLink;
import com.trapster.android.model.Trap;
import com.trapster.android.model.TrapType;
import com.trapster.android.service.RouteLinkUpdateMonitor;
import com.trapster.android.service.UserLocation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


public class RouteLinkFilter extends TrapFilter
{
    static final String LOGNAME= "Trapster.RouteLinkFilter";
    //
    @Inject RouteLinkManager mRouteLinkManager;
    @Inject RouteLinkUpdateMonitor mRouteLinkUpdateMonitor;

    @Inject
    TrapManager trapManger;

    //
    public RouteLinkFilter()
    {

    }

    @Inject
    public void Init(){

    }

    public List<Trap> getTrapList(UserLocation location, List<Trap> currentList)
    {
        RouteLink broadcastRouteLink= mRouteLinkUpdateMonitor.getBroadcastRouteLink();
        Set<RouteLink> broadcastApproximateRouteLinks= mRouteLinkUpdateMonitor.getBroadcastApproximateRouteLinks();

        if (broadcastRouteLink != null && broadcastApproximateRouteLinks != null)
        {
            int numNotificationTraps= currentList.size();
            if (numNotificationTraps > 0)
            {
                ArrayList<RouteLink> nearbyRouteLinks= new ArrayList<RouteLink>(
                        broadcastApproximateRouteLinks.size() + 1);
                nearbyRouteLinks.add(broadcastRouteLink);
                nearbyRouteLinks.addAll(broadcastApproximateRouteLinks);

                boolean snapped;
                for (int i= numNotificationTraps - 1; i > -1; i--)
                {
                    snapped= false;
                    Trap trap= currentList.get(i);

                    TrapType trapType = trapManger.getTrapType(trap.getTypeid());

                    // Ignore any traptype that isn't in the Most Popular category
/*                    if(trapType.getCategoryId() != 3){

                        for (Iterator<RouteLink> q= nearbyRouteLinks.iterator(); q.hasNext() && !snapped;)
                        {
                            RouteLink nearbyRouteLink= q.next();
                            if (mRouteLinkManager.snapToRouteLink(nearbyRouteLink, trap.getLatitude(), trap.getLongitude()))
                                snapped= true;
                        }
                        if (!snapped)
                        {
                            currentList.remove(i);
                        }
                    }*/
                }
            }
        }

        return currentList;
    }
}