package com.trapster.android.service.notification;

import java.util.List;

import android.content.Context;

import com.trapster.android.model.Trap;
import com.trapster.android.service.UserLocation;

public abstract class Notifier {

    protected Context context;
    private UserLocation userLocation;

    abstract void send(List<Trap> traps, boolean isBackground);


    public Notifier() {
        super();

    }


    //public Context getContext() {
    //    return context;
    //}


    public void setUserLocation(UserLocation userLocation) {
        this.userLocation = userLocation;
    }


    public UserLocation getUserLocation() {
        return userLocation;
    }
}