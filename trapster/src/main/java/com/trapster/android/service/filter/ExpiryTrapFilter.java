package com.trapster.android.service.filter;


import com.google.inject.Inject;
import com.trapster.android.manager.TrapManager;
import com.trapster.android.model.Trap;
import com.trapster.android.service.UserLocation;

import java.util.ArrayList;
import java.util.List;

public class ExpiryTrapFilter extends TrapFilter
{
    private static final String LOGNAME = "ExpiryTrapFilter";

    @Inject TrapManager tm;

    public ExpiryTrapFilter()
    {

    }

    @Inject
    public void Init(){

    }

    public List<Trap> getTrapList(UserLocation location, List<Trap> currentList)
    {

        if (currentList != null)
        {


            int numTraps= currentList.size();
            if (numTraps > 0)
            {
                ArrayList<Trap> newList = new ArrayList<Trap>();
                for (int i = 0; i < numTraps; i++)
                {
                    Trap trap = currentList.get(i);

                    if(!tm.isDismissedTrap(trap.getId())){

                        newList.add(trap);
                    }

                }

                return newList;
            }
            else
                return currentList;


        }
        return currentList;
    }
}
