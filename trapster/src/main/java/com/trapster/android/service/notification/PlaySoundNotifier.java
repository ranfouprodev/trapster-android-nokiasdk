package com.trapster.android.service.notification;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import com.google.inject.Inject;
import com.trapster.android.Defaults;
import com.trapster.android.manager.SoundManager;
import com.trapster.android.manager.SoundThemeManager;
import com.trapster.android.manager.TrapManager;
import com.trapster.android.model.Trap;
import com.trapster.android.model.TrapType;

import java.util.HashMap;
import java.util.List;


public class PlaySoundNotifier extends Notifier {
    static final String LOGNAME = "Trapster.PlaySoundNotifier";
    //

    @Inject SharedPreferences pref;
    @Inject TrapManager trapManager;
    @Inject SoundManager soundManager;
    @Inject SoundThemeManager soundThemeManager;
    @Inject Application application;
    HashMap<Integer, Long> notified = new HashMap<Integer, Long>();

    @Override
    void send(List<Trap> traps, boolean isBackground)
    {
    }

    //
    public PlaySoundNotifier() {

    }

    @Inject
    public void Init(){

    }

    void expireOldNotified(Trap trap) {


            long trapId = trap.getId();
            if (notified.containsKey(trapId)) {
                long timestamp = notified.get(trapId);
                long elapsed = System.currentTimeMillis() - timestamp;
                if (elapsed > 5 * 60 * 1000)
                    notified.remove(trapId);
        }
    }

    public void playSound(Trap trap) {
        TrapType type = trapManager.getTrapType(trap.getTypeid());
        soundManager.playSound(type.getAudioid(), soundThemeManager.getCurrentTheme());
    }

    public void send(Trap trap) {
       // SharedPreferences pref = getContext().getSharedPreferences(
       //         Defaults.PREF_SETTINGS, Context.MODE_PRIVATE);

        // is the trap layer active && its not in the background?
        boolean layerActive = pref.getBoolean(Defaults.SETTING_MAP_DISPLAY_TRAPS, true);

        boolean backgroundSound = layerActive;
        
        // Does the user want repeated warnings?
        //boolean repeat = pref.getBoolean(Defaults.SETTING_AUDIBLE, Defaults.SETTING_DEFAULT_REPEAT_AUDIBLE);
        
        int repeatIntervalPerMillis = pref.getInt(Defaults.PREFERENCE_TRAP_ALERT_REPEAT_AUDIBLE, Defaults.DEFAULT_ALERT_REPEAT_MINUTES) *1000;
        boolean repeat = (repeatIntervalPerMillis > 0);

        //boolean muteAllTraps = pref.getBoolean(Defaults.SETTING_MUTEALL, false);

        // Play the sound for the notification
        // Traps are already sorted by distance?
        expireOldNotified(trap);




            TrapType type = trapManager.getTrapType(trap.getTypeid());
            if (type == null) // still loading the traptypes from the server
                return;
            // Is the trap on (not disabled in settings)
            boolean visible = trapManager.isTrapVisible(trap);

            // Does the trap allow audible?
            boolean audible = pref.getBoolean(type.getAudioid() + "_audible",
                    true);
            // Does the trap allow vibrate?
            boolean vibrate = pref.getBoolean(type.getAudioid() + "_vibrate",
                    true);
{/* 
            //Log.d(LOGNAME, "Sound verification for trap type:" + type.getName() + " alertable:" + type.isAlertable() + " visible:" + visible + " audible:" + audible + " vibrate:" + vibrate + " maplayer:" + backgroundSound + " repeat:" + repeat); */}

            if (visible && backgroundSound && type.isAlertable()) {
                if (!notified.containsKey(trap.getId())) {
                    if (audible)
                        playSound(trap);
                    if (vibrate)
                        vibrate();
                    notified.put(trap.getId(), System.currentTimeMillis());
                } else {
                    long timestamp = System.currentTimeMillis();
                    long lastTimestamp = notified.get(trap.getId());
                    long elapsed = timestamp - lastTimestamp;
{/*                     //Log.d(LOGNAME, "Repeat:"+elapsed+" interval:"+repeatIntervalPerMillis); */}

                    if (repeat && elapsed > repeatIntervalPerMillis) {
                        if (audible)
                            playSound(trap);
                        if (vibrate)
                            vibrate();
                        notified.put(trap.getId(), timestamp);
                    }
                }
            }
    }

    void vibrate() {
        NotificationManager nm = (NotificationManager) application.getApplicationContext()
                .getSystemService(Context.NOTIFICATION_SERVICE);
        long[] vibrate = new long[] { 300, 300 };
        Notification notification = new Notification();
        notification.vibrate = vibrate;
        nm.notify(Defaults.NOTIFICATION_VIBRATION, notification);
    }
}
