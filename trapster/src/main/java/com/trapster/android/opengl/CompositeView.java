package com.trapster.android.opengl;

import com.trapster.android.opengl.drawable.GLShape;

public interface CompositeView
{
    public void addShape(GLShape shape);
    public void hideAll();
    public void unHideAll();
    public void setVisibility(int visibility);
}
