package com.trapster.android.opengl.drawable;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.opengl.GLES20;
import android.opengl.Matrix;
import com.trapster.android.BuildFeatures;
import com.trapster.android.Defaults;
import com.trapster.android.TrapsterApplication;
import com.trapster.android.opengl.CompositeTextureView;
import com.trapster.android.opengl.util.Shaders;
import com.trapster.android.opengl.util.Vertex;

import java.util.Random;

public class GLCone extends GLShape
{
    private static int NUMBER_INVISIBLE_FRAMES = 18;
    private static int NUMBER_VISIBLE_FRAMES = (int) (CompositeTextureView.NUMBER_OF_FRAMES * 1.2);;
    private static int NUMBER_OF_FRAMES = NUMBER_VISIBLE_FRAMES + NUMBER_INVISIBLE_FRAMES;
    private static double BASE_RADIUS = 10;
    private static double MAX_RADIUS = 275;
    private static double MAX_RADIUS_PRE_ICS = 125;
    private static int NUMBER_OF_POINTS = 45;
    private static final double ANGLE_INCREMENT = 360 / NUMBER_OF_POINTS;
    private static final double DEGREES_IN_ARC = 60.0;


    private double radius = BASE_RADIUS;
    private double radiusIncrement = (MAX_RADIUS - BASE_RADIUS) / NUMBER_OF_FRAMES;
    private float ALPHA_DECREMENT = (1.0f /NUMBER_OF_FRAMES);
    private int currentFrame = 0;
    private float alpha = 1.0f;
    private double startAngle = 0.0;
    private double heading = 0.0;


    private static float LINE_WIDTH = 5f;
    private int coneStartElement = 0;
    private int coneEndElement;
    private boolean isCircle = true;

    private boolean isFabulous = false;
    private final Random rng = new Random();

    private float[] hsv = {0.0f, 1.0f, 1.0f};


    private Vertex.VertexArray[] precalculatedVertices;

    public GLCone()
    {
        super();
        setVertexValues(calculateCircle(NUMBER_OF_POINTS, 0));
        shouldDraw = true;

        color = new float[]{1, 0, 0, 1};

        if (BuildFeatures.SUPPORTS_ICS)
            radiusIncrement = (MAX_RADIUS - BASE_RADIUS) / NUMBER_OF_FRAMES;
        else
            radiusIncrement = (MAX_RADIUS_PRE_ICS - BASE_RADIUS) / NUMBER_OF_FRAMES;
    }

    @Override
    public void setup(int width, int height)
    {
        super.setup(width, height);
        setupCircleValues();
        currentFrame = 0;
        vertices = precalculatedVertices[currentFrame];
    }

    @Override
    public void loadShaders()
    {
        vertexShader = Shaders.loadLineVertexShader();
        fragmentShader = Shaders.loadLineFragmentShader();
    }
    @Override
    public void doDraw(float[] pvMatrix)
    {
        updateValues();
        if (shouldDraw && currentFrame < NUMBER_VISIBLE_FRAMES)
        {
            int positionHandle = GLES20.glGetAttribLocation(program, "a_position");
            int colorHandle = GLES20.glGetUniformLocation(program, "uColor");
            int mvpMatrixHandle = GLES20.glGetUniformLocation(program, "uMVPMatrix");

            float[] modelMatrix = new float[16];
            float[] MVPMatrix = new float[16];

            Matrix.setIdentityM(modelMatrix, 0);
            Matrix.translateM(modelMatrix, 0, 0, 0,  0);
            Matrix.translateM(modelMatrix, 0, xOffset, yOffset, zOffset);
            Matrix.rotateM(modelMatrix, 0, (float) ((float)-heading - (DEGREES_IN_ARC / 2)), 0, 0, 1);

            Matrix.multiplyMM(MVPMatrix, 0, pvMatrix, 0, modelMatrix, 0);

            GLES20.glUniformMatrix4fv(mvpMatrixHandle, 1, false, MVPMatrix, 0);

            GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, vertices.getVertexBufferPointer());
            GLES20.glVertexAttribPointer(positionHandle, 3, GLES20.GL_FLOAT, false, 0, 0);
            GLES20.glEnableVertexAttribArray(positionHandle);

            GLES20.glUniform4fv(colorHandle, 1, color, 0);

            GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, coneEndElement - coneStartElement);
        }

    }

    private void updateValues()
    {
        // Moving this to the Labs
         SharedPreferences sharedPreferences = TrapsterApplication.getAppInstance().getSharedPreferences(Defaults.PREF_SETTINGS, Context.MODE_PRIVATE);
        isFabulous = sharedPreferences.getBoolean(Defaults.PREFERENCE_SOUND_THEME_FABULOUS_EASTER_EGG, false);
        //boolean isModerator = SessionManager.profile != null && SessionManager.profile.getGlobalModerator() > 0;

        currentFrame++;
        if (currentFrame >= NUMBER_OF_FRAMES)
        {
            hsv[0] = rng.nextFloat() * 360.0f;
            currentFrame = 0;
        }

        if (currentFrame < NUMBER_VISIBLE_FRAMES)
        {
            radius = BASE_RADIUS + radiusIncrement * currentFrame;
            alpha = 1.0f - (ALPHA_DECREMENT * currentFrame);
            startAngle++;

            vertices = precalculatedVertices[currentFrame];
            //color[3] = alpha;
        }

        if (isFabulous)
        {
            hsv[0]+= 360 / NUMBER_VISIBLE_FRAMES;
            if (hsv[0] > 360)
                hsv[0] = 0.0f;

            int c = Color.HSVToColor(hsv);
            int r = (c >> 16) & 0xFF;
            int g = (c >> 8) & 0xFF;
            int b = (c & 0xFF);

            color[0] = (float) r / 255;
            color[1] = (float) g / 255;
            color[2] = (float) b / 255;
            color[3] = 1.0f;
        }
        else
        {
            color[0] = 1.0f;
            color[1] = 0.0f;
            color[2] = 0.0f;
            color[3] = alpha;
        }




        if (!BuildFeatures.SUPPORTS_ICS)
            isCircle = false;

        if (isCircle) // Draw the whole circle
        {
            coneStartElement = 0;
            coneEndElement = vertices.getCount();
        }
        else
        {
            int verticesInArc = (int)(DEGREES_IN_ARC / ANGLE_INCREMENT) * 2;
            coneStartElement = 0;
            coneEndElement = coneStartElement + verticesInArc;
        }
    }

    private float[] calculateCircle(int numPoints, double radius)
    {

        double theta1 = ANGLE_INCREMENT;
        double theta2 = ANGLE_INCREMENT / 2;

        double midDistance = Math.sin(Math.toRadians(theta2)) * radius;
        double hypotenuse = Math.pow(midDistance * 2, 2);
        double base = Math.pow(midDistance, 2);
        double height = Math.sqrt(hypotenuse / base) * LINE_WIDTH;



        double radius1 = radius;
        double radius2 = radius + height;

        numPoints++;
        float[] values = new float[(numPoints * 2) * 3];

        int whichPoint = 0;
        for (int i = 0; i < values.length; i = i + 6)
        {

            values[i] = (float)(radius1 * Math.cos(Math.toRadians(theta1 + (ANGLE_INCREMENT * whichPoint))));
            values[i + 1] = (float)(radius1 * Math.sin(Math.toRadians(theta1 + (ANGLE_INCREMENT * whichPoint))));
            values[i + 2] = 0.0f;

            values[i + 3] = (float)(radius2 * Math.cos(Math.toRadians(theta1 + (ANGLE_INCREMENT * whichPoint))));
            values[i + 4] = (float)(radius2 * Math.sin(Math.toRadians(theta1 + (ANGLE_INCREMENT * whichPoint))));
            values[i + 5] = 0.0f;

            whichPoint++;
        }



        return values;
    }

    private void setupCircleValues()
    {
        precalculatedVertices = new Vertex.VertexArray[NUMBER_VISIBLE_FRAMES];

        for (int i = 0; i < NUMBER_VISIBLE_FRAMES; i++)
        {
            float[] values = calculateCircle(NUMBER_OF_POINTS, BASE_RADIUS + radiusIncrement * i);
            precalculatedVertices[i] = Vertex.createVertexArray(values);
        }
    }

    public void useCircle(boolean isCircle)
    {
        this.isCircle = isCircle;
    }

    public void setHeading(double heading)
    {
        this.heading = heading;
    }
}
