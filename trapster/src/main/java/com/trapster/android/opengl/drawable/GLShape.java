package com.trapster.android.opengl.drawable;

import android.opengl.GLES20;
import com.trapster.android.manager.CompositePositionIndicatorManager;
import com.trapster.android.opengl.util.Shaders;
import com.trapster.android.opengl.util.Vertex;

import java.nio.FloatBuffer;

public abstract class GLShape implements GLDrawable
{
    private int screenWidth;
    private int screenHeight;
    //private int colorPointer;
    protected float[] color = DEFAULT_COLOR;
    protected float rotationAngle;
    protected float xScale;
    protected float yScale;
    protected float zScale;

    protected float xOffset;
    protected float yOffset;
    protected float zOffset;

    protected int vertexShader;
    protected int fragmentShader;
    protected static final int COORDS_PER_VERTEX = 3;

    private static final float[] DEFAULT_COLOR = {0, 0, 1, 1}; // Full Blue, fully opaque

    protected int program;



    protected Vertex.VertexArray vertices;
    private FloatBuffer colorBuffer;
    private float[] values;

    private int resourceID;
    protected volatile boolean shouldDraw = true;

    private CompositePositionIndicatorManager.CoordinateCallback callback;

    private static final float[] DEFAULT_VALUES = new float[]
            {

                    0, 1, 0.0f,   // top left         0
                    1, 1, 0.0f,      // top right         3
                    0, 0, 0.0f,   // bottom left      1
                    1, 0, 0.0f    // bottom right      2



            };

    protected GLShape()
    {
        rotationAngle = 0;
        xScale = 1.0f;
        yScale = 1.0f;
        zScale = 1.0f;

        xOffset = 0.0f;
        yOffset = 0.0f;
        zOffset = 0.0f;
    }

    public GLShape(float[] values)
    {
        setVertexValues(values);
    }

    protected void setVertexValues(float[] values)
    {
        this.values = values;
    }

    @Override
    public void setup(int width, int height)
    {
        screenHeight = height;
        screenWidth = width;
        loadShaders();

        vertices = Vertex.createVertexArray(values);
        program = GLES20.glCreateProgram();

        GLES20.glAttachShader(program, vertexShader);
        GLES20.glAttachShader(program, fragmentShader);
        GLES20.glLinkProgram(program);

        //createColorBuffer();

    }

    @Override
    public final void draw(float[] pvMatrix)
    {
        if (callback != null)
            callback.requestCoordinateUpdate(this);
        GLES20.glUseProgram(program);

        doDraw(pvMatrix);

    }

    protected void doDraw(float[] pvMatrix)
    {
        int positionHandle = GLES20.glGetAttribLocation(program, "vPosition");
        GLES20.glEnableVertexAttribArray(positionHandle);

        GLES20.glVertexAttribPointer(positionHandle, COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, COORDS_PER_VERTEX * 4, vertices.toVertexBuffer());

        int colorHandle = GLES20.glGetUniformLocation(program, "vColor");
        GLES20.glUniform4fv(colorHandle, 1, color, 0);

        int mvpMatrixHandle = GLES20.glGetUniformLocation(program, "uMVPMatrix");
        GLES20.glUniformMatrix4fv(mvpMatrixHandle, 1, false, pvMatrix, 0);

        GLES20.glDrawElements(GLES20.GL_TRIANGLES, vertices.getDrawOrderLength(), GLES20.GL_UNSIGNED_SHORT, vertices.getDrawOrder());
        GLES20.glDisableVertexAttribArray(positionHandle);
    }

    protected void loadShaders()
    {
        vertexShader = Shaders.loadVertexShader();
        fragmentShader = Shaders.loadFragmentShader();
    }

    public void setRotation(float angle)
    {
        rotationAngle = angle;
        shouldDraw = true;
    }

    public void setXScale(float xScale)
    {
        this.xScale = xScale;
    }

    public void setYScale(float yScale)
    {
        this.yScale = yScale;
    }

    public void setZScale(float zScale)
    {
        this.zScale = zScale;
    }

    public void setXOffset(float xOffset, boolean fromSDK)
    {
        if (fromSDK)
        {
            xOffset = xOffset - (screenWidth / 2);
        }
        this.xOffset = xOffset;
    }

    public void setYOffset(float yOffset, boolean fromSDK)
    {
        if (fromSDK)
        {
            yOffset = yOffset - (screenHeight / 2);
        }
        this.yOffset = yOffset;
    }

    public void setZOffset(float zOffset)
    {
        this.zOffset = zOffset;
    }

    public void setCoordinateCallback(CompositePositionIndicatorManager.CoordinateCallback callback)
    {
        this.callback = callback;
    }

    public void setVisible(boolean isVisible)
    {
        shouldDraw = isVisible;
    }

    public boolean willDraw()
    {
        return shouldDraw;
    }
}
