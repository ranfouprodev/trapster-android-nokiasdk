package com.trapster.android.opengl.drawable;

public class InvalidShapeException extends RuntimeException
{
    public InvalidShapeException(GLDrawable drawable)
    {
        super("Invalid number of points specified to draw a " + drawable.getClass().getCanonicalName());
    }
}
