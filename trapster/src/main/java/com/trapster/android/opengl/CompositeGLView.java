package com.trapster.android.opengl;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.AttributeSet;
import com.trapster.android.opengl.drawable.GLDrawable;
import com.trapster.android.opengl.drawable.GLShape;
import com.trapster.android.opengl.util.TextureManager;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CompositeGLView extends GLSurfaceView implements CompositeView
{
    private CompositeGLRenderer renderer;
    private BackgroundRenderScheduler renderScheduler;
    private volatile boolean isHidden = false;
    private volatile boolean needsToClear = false;
    private volatile boolean isPaused = false;

    public CompositeGLView(Context context)
    {
        super(context);
        setup();
    }

    public CompositeGLView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        setup();
    }

    private void setup()
    {
       // Log.i("BBQ", "New composite view");
        setEGLContextClientVersion(2);

        setEGLConfigChooser(8, 8, 8, 8, 16, 0);

        TextureManager.init(getContext());
        renderer = new CompositeGLRenderer();
        setRenderer(renderer);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        setDrawingCacheBackgroundColor(Color.TRANSPARENT);
        getHolder().setFormat(PixelFormat.TRANSPARENT);

        setZOrderOnTop(true);
    }

    public void onPause()
    {

        if (renderScheduler != null)
            renderScheduler.stopRender();
        // super.onPause();
        super.onPause();
    }

    public void onResume()
    {
       // Log.i("BBQ", "onResume");
        // super.onResume();
        super.onResume();
    }

    public void addShape(GLShape shape)
    {
        renderer.addShape(shape);
    }

    @Override
    public void hideAll()
    {
        isHidden = true;
        needsToClear = true;
    }

    @Override
    public void unHideAll()
    {
        isHidden = false;
        isPaused = false;
        if (renderScheduler != null)
        {
            synchronized (renderScheduler)
            {
                renderScheduler.notify();
            }
        }
    }

    public class CompositeGLRenderer implements Renderer
    {
        public static final int NUMBER_OF_FRAMES = 30;
        private static final long TIME_BETWEEN_DRAWS = 1000 / NUMBER_OF_FRAMES;

        private ArrayList<GLDrawable> drawableList = new ArrayList<GLDrawable>();
        private List<GLDrawable> newDrawables = new LinkedList<GLDrawable>();

        private final float[] pvMatrix = new float[16];
        private final float[] projectionMatrix = new float[16];
        private final float[] viewMatrix = new float[16];

        private int width;
        private int height;


        public CompositeGLRenderer()
        {

        }

        public void addShape(GLShape shape)
        {
            synchronized (newDrawables)
            {
                newDrawables.add(shape);
            }

        }

        public ArrayList<GLDrawable> getDrawList()
        {
            return drawableList;
        }

        @Override
        public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig)
        {
           // Log.i("BBQ", "Surface created");
            GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
            //GLES20.glEnable(GLES20.GL_BLEND);


        }

        @Override
        public void onSurfaceChanged(GL10 gl10, int width, int height)
        {
          //  Log.i("BBQ", "Surface changed");
            this.width = width;
            this.height = height;

            GLES20.glViewport(0, 0, width, height);
            float ratio = (float) width / height;
            //Matrix.frustumM(projectionMatrix, 0, -ratio, ratio, 0, 1, 2, 4);
            Matrix.frustumM(projectionMatrix, 0, (width / 2), (width / -2), (height / 2), (height / -2), 1, 3);
            Matrix.setLookAtM(viewMatrix, 0, 0, 0, -1f, 0f, 0f, 0f, 0f, 1.0f, 0.0f);
            Matrix.multiplyMM(pvMatrix, 0, projectionMatrix, 0, viewMatrix, 0);

            if (renderScheduler != null)
                renderScheduler.stopRender(); // Make sure the old one stopped
            renderScheduler = new BackgroundRenderScheduler(CompositeGLView.this);
            renderScheduler.start();

            //public static void setLookAtM(float[] rm, int rmOffset, float eyeX, float eyeY, float eyeZ, float centerX,
            // float centerY, float centerZ, float upX, float upY, float upZ)

        }

        @Override
        public void onDrawFrame(GL10 gl10)
        {

            synchronized (newDrawables)
            {
                for (GLDrawable drawable : newDrawables)
                {
                    drawable.setup(width, height);
                    drawableList.add(drawable);
                }
                newDrawables.clear();
            }

            if (needsToClear && isHidden)
            {
                GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);

                needsToClear = false;
            }

            if (!isHidden)
            {
                GLES20.glEnable(GLES20.GL_BLEND);
                GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
                GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);

                for (GLDrawable drawable : drawableList)
                {
                    drawable.draw(pvMatrix);
                }
            }

        }

    }

    private class BackgroundRenderScheduler extends Thread
    {
        private GLSurfaceView surfaceView;
        private volatile boolean isRendering;

        public BackgroundRenderScheduler(GLSurfaceView surfaceView)
        {
            super("BackgroundRenderSchedule-2.3");
            this.surfaceView = surfaceView;
            isRendering = true;
            isPaused = false;
        }

        public void stopRender()
        {
            isRendering = false;
            isPaused = true;
        }

        @Override
        public void run()
        {
            while (isRendering)
            {
                while (!isPaused)
                {
                    surfaceView.requestRender();

                    try
                    {
                        Thread.sleep(CompositeGLRenderer.TIME_BETWEEN_DRAWS);
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                        isRendering = false;
                    }

                    if (isHidden && !needsToClear)
                        isPaused = true;
                }

                synchronized (this)
                {
                    try
                    {
                        this.wait();
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                        isPaused = false;
                    }
                }
            }
        }
    }
}
