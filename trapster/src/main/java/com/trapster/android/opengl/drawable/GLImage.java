package com.trapster.android.opengl.drawable;

import android.opengl.GLES20;
import android.opengl.Matrix;
import com.trapster.android.opengl.Texture;
import com.trapster.android.opengl.util.Shaders;
import com.trapster.android.opengl.util.TextureManager;

public class GLImage extends GLShape
{
    protected Texture texture;
    private int resourceID;

    public GLImage(int resourceID)
    {
        super();
        this.resourceID = resourceID;
        shouldDraw = true;
    }

    @Override
    protected void doDraw(float[] pvMatrix)
    {
        if (shouldDraw)
        {
            int mvpMatrixHandle = GLES20.glGetUniformLocation(program, "uMVPMatrix");
            int positionHandle = GLES20.glGetAttribLocation(program, "aVerPos");
            int textureCoordinateHandle = GLES20.glGetAttribLocation(program, "aTexPos");
            int uSamp =GLES20.glGetUniformLocation(program, "uSampler");

            float[] modelMatrix = new float[16];
            float[] MVPMatrix = new float[16];

            Matrix.setIdentityM(modelMatrix, 0);
            Matrix.translateM(modelMatrix, 0, 0, 0,  0);
            Matrix.translateM(modelMatrix, 0, xOffset, yOffset, zOffset);
            Matrix.rotateM(modelMatrix, 0, -rotationAngle, 0, 0, 1);

            Matrix.multiplyMM(MVPMatrix, 0, pvMatrix, 0, modelMatrix, 0);

            GLES20.glUniformMatrix4fv(mvpMatrixHandle, 1, false, MVPMatrix, 0);

            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texture.getID());
            GLES20.glUniform1i(uSamp, 0);

            GLES20.glEnableVertexAttribArray(positionHandle);
            GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, vertices.getVertexBufferPointer());
            GLES20.glVertexAttribPointer(positionHandle, 3, GLES20.GL_FLOAT, false, 0, 0);

            GLES20.glEnableVertexAttribArray(textureCoordinateHandle);
            GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, texture.getCoordinateHandle());
            GLES20.glVertexAttribPointer(textureCoordinateHandle, 2, GLES20.GL_FLOAT, false, 0, 0);

            GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
        }
    }

    @Override
    public void setup(int width, int height)
    {
        if (resourceID != 0)
        {
            texture = TextureManager.getTexture(resourceID);
            texture.setxScale(xScale);
            texture.setyScale(yScale);
            texture.setzScale(zScale);
            setVertexValues(texture.calculateCoordinates());
        }

        super.setup(width, height);
    }

    @Override
    public void setXScale(float xScale)
    {
        super.setXScale(xScale);
        texture.setxScale(xScale);
        setVertexValues(texture.calculateCoordinates());
    }

    @Override
    public void setYScale(float yScale)
    {
        super.setYScale(yScale);
        texture.setyScale(yScale);
        setVertexValues(texture.calculateCoordinates());
    }

    protected void loadShaders()
    {
        vertexShader = Shaders.loadTextureVertexShader();
        fragmentShader = Shaders.loadTextureFragmentShader();
    }
}