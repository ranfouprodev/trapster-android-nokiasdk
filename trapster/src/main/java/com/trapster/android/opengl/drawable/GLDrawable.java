package com.trapster.android.opengl.drawable;

public interface GLDrawable
{
    public void draw(float[] transformationMatrix);
    public void setup(int width, int height);
    public boolean willDraw();
}
