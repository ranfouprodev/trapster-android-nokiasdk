package com.trapster.android.opengl;

import android.graphics.SurfaceTexture;
import android.opengl.GLES20;
import android.opengl.GLException;
import android.opengl.GLUtils;

import javax.microedition.khronos.egl.*;
import javax.microedition.khronos.opengles.GL11;

public class TextureViewRenderer
{
    private EGL10 gl;
    private GL11 gl11;
    private EGLDisplay eglDisplay;
    private EGLContext eglContext;
    private EGLSurface eglSurface;
    private EGLConfig eglConfig;
    private static final int EGL_OPENGL_ES2_BIT = 4;
    private static final int EGL_CONTEXT_CLIENT_VERSION = 0x3098;

    private long creatorID;
    private boolean initialized = false;

    public synchronized void initGL(SurfaceTexture surfaceTexture)
    {
        if (!initialized)
        {
            creatorID = Thread.currentThread().getId();
            gl = (EGL10) EGLContext.getEGL();

            eglDisplay = gl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
            if (eglDisplay == EGL10.EGL_NO_DISPLAY)
            {
                throw new RuntimeException("eglGetDisplay failed " + GLUtils.getEGLErrorString(gl.eglGetError()));
            }

            int[] version = new int[2];
            if (!gl.eglInitialize(eglDisplay, version))
            {
                throw new RuntimeException("eglInitialize failed " + GLUtils.getEGLErrorString(gl.eglGetError()));
            }

            eglConfig = chooseEglConfig();
            if (eglConfig == null)
            {
                throw new RuntimeException("eglConfig not initialized");
            }

            int[] attrib_list = { 0x3098, 2, EGL10.EGL_NONE };
            eglContext = createContext(gl, eglDisplay, eglConfig);
            //checkEglError();
            eglSurface = gl.eglCreateWindowSurface(eglDisplay, eglConfig, surfaceTexture, null);
            //checkEglError();
            if (eglSurface == null || eglSurface == EGL10.EGL_NO_SURFACE)
            {
                int error = gl.eglGetError();
                throw new RuntimeException("eglCreateWindowSurface failed " + GLUtils.getEGLErrorString(error));
            }

            if (!gl.eglMakeCurrent(eglDisplay, eglSurface, eglSurface, eglContext))
            {
                throw new RuntimeException("eglMakeCurrent failed " + GLUtils.getEGLErrorString(gl.eglGetError()));
            }

            gl11 = (GL11) eglContext.getGL();

            initialized = true;
        }

    }

    public synchronized void draw()
    {
       gl.eglSwapBuffers(eglDisplay, eglSurface);
       checkGlError();
    }

    EGLContext createContext(EGL10 egl, EGLDisplay eglDisplay, EGLConfig eglConfig) {
        int[] attrib_list = { EGL_CONTEXT_CLIENT_VERSION, 2, EGL10.EGL_NONE };
        return egl.eglCreateContext(eglDisplay, eglConfig, EGL10.EGL_NO_CONTEXT, attrib_list);
    }

    private EGLConfig chooseEglConfig() {
        int[] configsCount = new int[1];
        EGLConfig[] configs = new EGLConfig[1];
        int[] configSpec = getConfig();
        if (!gl.eglChooseConfig(eglDisplay, configSpec, configs, 1, configsCount))
        {
            throw new IllegalArgumentException("eglChooseConfig failed " + GLUtils.getEGLErrorString(gl.eglGetError()));
        }
        else if (configsCount[0] > 0)
        {
            return configs[0];
        }
        return null;
    }

    private int[] getConfig() {
        return new int[] {
                EGL10.EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
                EGL10.EGL_RED_SIZE, 8,
                EGL10.EGL_GREEN_SIZE, 8,
                EGL10.EGL_BLUE_SIZE, 8,
                EGL10.EGL_ALPHA_SIZE, 8,
                EGL10.EGL_DEPTH_SIZE, 0,
                EGL10.EGL_STENCIL_SIZE, 0,
                EGL10.EGL_NONE
        };
    }


    public synchronized void finishGL()
    {
        if (eglDisplay != null && eglContext != null)
            gl.eglDestroyContext(eglDisplay, eglContext);
        if (eglDisplay != null && eglSurface != null)
            gl.eglDestroySurface(eglDisplay, eglSurface);

        eglDisplay = null;
        eglContext = null;
        eglSurface = null;
        eglConfig = null;
        gl = null;

        //Log.e("BBQ", "FinishGL");
    }

    public void checkCurrent(SurfaceTexture surfaceTexture)
    {
        if (!eglContext.equals(gl.eglGetCurrentContext()) || !eglSurface.equals(gl.eglGetCurrentSurface(EGL10.EGL_DRAW)))
        {
            eglSurface = gl.eglCreateWindowSurface(eglDisplay, eglConfig, surfaceTexture, null);
            if (!gl.eglMakeCurrent(eglDisplay, eglSurface, eglSurface, eglContext))
            {
                throw new RuntimeException("eglMakeCurrent failed " + GLUtils.getEGLErrorString(gl.eglGetError()));
            }
        }
    }

    private void checkGlError()
    {
        int error = GLES20.glGetError();
        if (error != GLES20.GL_NO_ERROR)
        {
            //Log.e("BBQ", "draw failed");
            throw new GLException(error);
        }
    }

    public long getCreatorID()
    {
        return creatorID;
    }
}
