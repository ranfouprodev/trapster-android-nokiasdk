package com.trapster.android.opengl.util;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class Vertex
{
    public float x, y, z;
    Vertex(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public static class VertexArray
    {
        private int vertexBufferPointer;
        protected Vertex[] vertices;
        private float[] values;
        private int drawOrderLength;

        private FloatBuffer vertexBuffer;
        private ShortBuffer drawOrderBuffer;

        public VertexArray(Vertex[] vertices, float[] values)
        {
            this.vertices = vertices;
            this.values = values;
            setup();
        }

        private VertexArray(Vertex[] vertices)
        {
            this.vertices = vertices;
            values = new float[vertices.length * 3]; // 3 values for 3 points
            int slot = 0;
            for (int i = 0; i < vertices.length; i++)
            {
                Vertex v = vertices[i];
                values[slot] = v.x;
                values[slot + 1 ] = v.y;
                values[slot + 2] = v.z;
                slot += 3;
            }
            setup();
        }

        private void setup()
        {
            createVertexBuffer();
            createDrawOrderBuffer();
        }

        public Vertex getFirst()
        {
            return vertices[0];
        }

        public Vertex getSecond()
        {
            return vertices[1];
        }

        public Vertex getThird()
        {
            return vertices[2];
        }

        public Vertex getVertex(int whichVertex)
        {
            return vertices[whichVertex];
        }

        public FloatBuffer toVertexBuffer()
        {
            vertexBuffer.position(0);
            return vertexBuffer;
        }

        public ShortBuffer getDrawOrder()
        {
            drawOrderBuffer.position(0);
            return drawOrderBuffer;
        }

        public int getVertexBufferPointer()
        {
            return vertexBufferPointer;
        }

        private void createVertexBuffer()
        {
            ByteBuffer buffer = ByteBuffer.allocateDirect(values.length * (Float.SIZE / 8));
            buffer.order(ByteOrder.nativeOrder());
            vertexBuffer = buffer.asFloatBuffer();
            vertexBuffer.put(values);
            vertexBuffer.position(0);

            int[] b = new int[1];
            GLES20.glGenBuffers(1, b, 0);
            vertexBufferPointer = b[0];
            GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, vertexBufferPointer);
            GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, values.length * 4, vertexBuffer, GLES20.GL_STATIC_DRAW);
        }

        private void createDrawOrderBuffer()
        {
            int numVertices = values.length / 3;

            short[] drawOrder;
            switch (numVertices)
            {
                case 3:
                {
                    // Triangle
                    drawOrder = new short[]{0, 1, 2};
                    break;
                }
                case 4:
                {
                    // Square
                    drawOrder = new short[]{0, 1, 2, 0, 2, 3};
                    break;
                }
                default:
                {
                    drawOrder = new short[numVertices];
                    for (short i = 0; i < numVertices; i++)
                    {
                        drawOrder[i] = i;
                    }
                    break;
                }
            }

            ByteBuffer buffer = ByteBuffer.allocateDirect(drawOrder.length * 2); // 2 bytes per vertex
            buffer.order(ByteOrder.nativeOrder());
            drawOrderBuffer = buffer.asShortBuffer();



            drawOrderLength = drawOrder.length;
            drawOrderBuffer.put(drawOrder);
            drawOrderBuffer.position(0);
        }

        public int getDrawOrderLength()
        {
            return drawOrderLength;
        }

        public int getCount()
        {
            return values.length / 3;
        }
    }

    public static class SquareVertexArray extends VertexArray
    {
        public SquareVertexArray(Vertex[] vertices, float[] values)
        {
            super(vertices, values);
        }

        public Vertex getFourth()
        {
            return vertices[3];
        }
    }

    public static VertexArray createTriangleVertices(float[] values)
    {
        try
        {
            Vertex[] vertices = createVertices(values, 3); // Triangles only have 3 points
            return new VertexArray(vertices, values);
        }
        catch (IndexOutOfBoundsException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public static VertexArray createTriangleVertices(Vertex firstPoint, Vertex secondPoint, Vertex thirdPoint)
    {
        Vertex[] vertices = new Vertex[3];
        vertices[0] = firstPoint;
        vertices[1] = secondPoint;
        vertices[2] = thirdPoint;

        return new VertexArray(vertices);
    }

    public static SquareVertexArray createSquareVertices(float[] values)
    {
        try
        {
            Vertex[] vertices = createVertices(values, 4); // Squares have 4 points
            return new SquareVertexArray(vertices, values);
        }
        catch (IndexOutOfBoundsException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    private static Vertex[] createVertices(float[] values, int numberOfVertices) throws IndexOutOfBoundsException
    {
        Vertex[] vertices = new Vertex[numberOfVertices];
        int whichVertex = 0;
        for (int i = 0; i < values.length; i+=3)
        {
            vertices[whichVertex] = new Vertex(values[i], values[i + 1], values[i + 2]);
            whichVertex++;
        }

        return vertices;
    }

    public static VertexArray createVertexArray(float[] values)
    {
        VertexArray vertexArray = new VertexArray(createVertices(values, values.length / 3));
        return vertexArray;
    }
}
