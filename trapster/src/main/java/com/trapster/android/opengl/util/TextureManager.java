package com.trapster.android.opengl.util;

import android.content.Context;
import android.util.SparseArray;
import com.trapster.android.opengl.Texture;

public class TextureManager
{
    private static Context context;
    private static SparseArray<Texture> textureMap;

    public static void init(Context context)
    {
        TextureManager.context = context;
        textureMap = new SparseArray<Texture>();
    }

    public static void clear()
    {
        textureMap.clear();
    }

    private static Texture createTexture(int resourceID)
    {
        Texture texture = new Texture(context, resourceID);
        textureMap.put(resourceID, texture);

        return texture;
    }

    public static Texture getTexture(int resourceID)
    {
        Texture texture = textureMap.get(resourceID);
        if (texture == null)
            texture = createTexture(resourceID);

        return texture;
    }


}
