package com.trapster.android.opengl;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.SurfaceTexture;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.AttributeSet;
import android.view.TextureView;
import android.view.View;
import com.trapster.android.opengl.drawable.GLDrawable;
import com.trapster.android.opengl.drawable.GLShape;
import com.trapster.android.opengl.util.TextureManager;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CompositeTextureView extends TextureView implements TextureView.SurfaceTextureListener, CompositeView
{
    private BackgroundRenderScheduler renderScheduler;

    public static final int NUMBER_OF_FRAMES = 40;
    private static final long TIME_BETWEEN_DRAWS = 1000 / NUMBER_OF_FRAMES;

    private ArrayList<GLDrawable> drawableList = new ArrayList<GLDrawable>();
    private List<GLDrawable> newDrawables = new LinkedList<GLDrawable>();

    private final float[] pvMatrix = new float[16];
    private final float[] projectionMatrix = new float[16];
    private final float[] viewMatrix = new float[16];

    private int width;
    private int height;

    private volatile boolean isHidden = false;
    private volatile boolean isPaused = false;
    private boolean hasValidSurface = false;
    private boolean needsToClear = false;

    public CompositeTextureView(Context context)
    {
        super(context);
        setup();
    }

    public CompositeTextureView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        setup();

    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height)
    {
        this.width = width;
        this.height = height;
        hasValidSurface = true;
        startRender(width, height);
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int width, int height)
    {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture)
    {
        stopRender();
        hasValidSurface = false;
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture)
    {
    }

    private void setup()
    {
        setSurfaceTextureListener(this);
        TextureManager.init(getContext());

        // Update - setAlpha(0.99f) doesn't work on Android 4.3, changing it to use a Paint object fixes it, however it suffers the same pitfall
        // of ignoring the alpha channel entirely if it's set to 255.  Ridiculous, but fortunately backwards compatible to all 4.x devices
        // To be very safe, including the old setAlpha(), just in case the paint doesn't do what it should do on some devices
        setAlpha(0.99f); // Setting it to 1.0 is a full black background (alpha in glClearColor is ignored), all other values are correct
        Paint p = new Paint();
        p.setAlpha(254);
        setLayerType(View.LAYER_TYPE_HARDWARE, p);
    }

    private void startRender(int width, int height)
    {
        stopRender();

        renderScheduler = new BackgroundRenderScheduler(width, height);
        renderScheduler.start();
    }

    public void pauseRender()
    {
        isPaused = true;
    }

    public void resumeRender()
    {
        isPaused = false;
        if (renderScheduler != null)
        {
            synchronized (renderScheduler)
            {
                renderScheduler.notifyAll();
            }
        }

    }

    public void stopRender()
    {
        if (renderScheduler != null)
        {
            renderScheduler.stopRender();
            renderScheduler.interrupt();
            synchronized (renderScheduler)
            {
                renderScheduler.notifyAll();
            }
        }
    }

    public void addShape(GLShape shape)
    {
        synchronized (newDrawables)
        {
            newDrawables.add(shape);
        }

    }

    @Override
    public void hideAll()
    {
        //Log.i("BBQ", "Elements are hidden!");
        isHidden = true;
        needsToClear = true;
    }

    @Override
    public void unHideAll()
    {
        //Log.i("BBQ", "Elements are visible!");
        isHidden = false;
        isPaused = false;
        if (renderScheduler != null)
        {
            synchronized (renderScheduler)
            {
                renderScheduler.notify();
            }
        }
    }

    private class BackgroundRenderScheduler extends Thread
    {
        private TextureViewRenderer renderer = new TextureViewRenderer();
        private volatile boolean isRendering;

        private int height;
        private int width;

        public BackgroundRenderScheduler(int width, int height)
        {
            super("BackgroundRenderScheduler-4.x");
            isRendering = true;
            this.height = height;
            this.width = width;
        }

        public void stopRender()
        {
            isRendering = false;
            isPaused = true;
        }

        @Override
        public void run()
        {
            TextureManager.clear();
            renderer.initGL(getSurfaceTexture());
            setupGL(width, height);

            while (isRendering)
            {
                while (!isPaused)
                {
                    draw();
                    try
                    {
                        Thread.sleep(TIME_BETWEEN_DRAWS);
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }

                    if (isHidden && !needsToClear)
                        isPaused = true;
                }
                synchronized (this)
                {
                    try
                    {
                        this.wait();
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                        isPaused = false;
                    }
                }


            }

            renderer.finishGL();
        }

        private void draw()
        {
            synchronized (newDrawables)
            {
                for (GLDrawable drawable : newDrawables)
                {
                    drawable.setup(width, height);
                    drawableList.add(drawable);
                }
                newDrawables.clear();
            }

            if (needsToClear && isHidden)
            {
                GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
                renderer.draw();

                needsToClear = false;
            }

            if (!isHidden)
            {
                GLES20.glEnable(GLES20.GL_BLEND);
                GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
                GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);

                for (GLDrawable drawable : drawableList)
                {
                    drawable.draw(pvMatrix);
                }

                renderer.draw();
            }


        }
    }

    private void setupGL(int width, int height)
    {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        this.width = width;
        this.height = height;

        GLES20.glViewport(0, 0, width, height);
        Matrix.frustumM(projectionMatrix, 0, (width / 2), (width / -2), (height / 2), (height / -2), 1, 3);
        Matrix.setLookAtM(viewMatrix, 0, 0, 0, -1f, 0f, 0f, 0f, 0f, 1.0f, 0.0f);
        Matrix.multiplyMM(pvMatrix, 0, projectionMatrix, 0, viewMatrix, 0);
    }

    /*@Override
    public void setVisibility(int visibility)
    {

    }*/


}
