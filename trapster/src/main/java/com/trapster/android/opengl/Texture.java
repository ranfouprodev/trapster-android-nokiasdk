package com.trapster.android.opengl;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class Texture
{
    public static final int COORDINATES_PER_VERTEX = 2;
    private static final float textureCoords[] =
            {
                    0.0f, 0.0f,  // bottom left       2
                    1.0f, 0.0f,    // bottom right             3
                    0.0f, 1.0f,  // top left     1
                    1.0f, 1.0f,  // top right       0

            };

    private float[] coordinates = new float[12];

    private static final short drawOrder[] = {0, 1, 2, 3};

    private int id;
    private int width;
    private int height;
    private int coordinateHandle;

    protected float xScale = 1.0f;
    protected float yScale = 1.0f;
    protected float zScale = 1.0f;

    private final float yOffset = 0.0f;

    private FloatBuffer coordinateBuffer;
    private ShortBuffer drawOrderBuffer;

    public Texture(Context context, int resourceID)
    {
        int[] textureArray = new int[1];

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;   // No pre-scaling

        // TODO - Check out scaling
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resourceID, options);
        width = bitmap.getWidth();
        height = bitmap.getHeight();
/*        Matrix flip = new Matrix();
        flip.postScale(1.0f, -1.0f);
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), flip, true);*/


        GLES20.glGenTextures(1, textureArray, 0);

        id = textureArray[0];
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, id);


        //GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGB, bitmap, GLES20.GL_UNSIGNED_BYTE, 0);

        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);

        bitmap.recycle();

        ByteBuffer tcbb = ByteBuffer.allocateDirect(textureCoords.length * 4);
        tcbb.order(ByteOrder.nativeOrder());
        coordinateBuffer = tcbb.asFloatBuffer();
        coordinateBuffer.put(textureCoords);
        coordinateBuffer.position(0);

        ByteBuffer buffer = ByteBuffer.allocateDirect(drawOrder.length * 2);
        tcbb.order(ByteOrder.nativeOrder());
        drawOrderBuffer = buffer.asShortBuffer();
        drawOrderBuffer.put(drawOrder);
        drawOrderBuffer.position(0);

        createDataHandle();
    }

    private void createDataHandle()
    {
        int[] buffer = new int[1];
        GLES20.glGenBuffers(1, buffer, 0);

        coordinateHandle = buffer[0];
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, coordinateHandle);

        GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, textureCoords.length * 4, coordinateBuffer, GLES20.GL_STATIC_DRAW);
    }

    public float[] calculateCoordinates()
    {
        // top left
        coordinates[0] = getTop();
        coordinates[1] = getLeft();
        coordinates[2] = 0;
        coordinates[3] = getTop();
        coordinates[4] = getRight();
        coordinates[5] = 0;
        coordinates[6] = getBottom();
        coordinates[7] = getLeft();
        coordinates[8] = 0;
        coordinates[9] = getBottom();
        coordinates[10] = getRight();
        coordinates[11] = 0;

        /*private static final float[] DEFAULT_VALUES = new float[]
                {

                        -0, 1, 0.0f,   // top left         0
                        1, 1, 0.0f,      // top right         3
                        -0, -0, 0.0f,   // bottom left      1
                        1, -0, 0.0f    // bottom right      2



                };*/

        return coordinates;
    }

    public int getCoordinateHandle()
    {
        return coordinateHandle;
    }

    public ShortBuffer getDrawOrderBuffer()
    {
        drawOrderBuffer.position(0);
        return drawOrderBuffer;
    }

    public FloatBuffer getCoordinateBuffer()
    {
        coordinateBuffer.position(0);
        return coordinateBuffer;
    }

    public int getDrawOrderLength()
    {
        return drawOrder.length;
    }

    public float[] getTextureCoords()
    {
        return textureCoords;
    }

    public int getID()
    {
        return id;
    }

    public int getHeight()
    {
        return height;
    }

    public int getWidth()
    {
        return width;
    }

    // A note on this madness - The Y axis goes left and right, X goes up and down.  I probably am mapping the texture wrong, but this is a much easier fix!
    public int getTop()
    {
        return (int)(getWidth() / 2 * yScale);
    }

    public int getBottom()
    {
        return (int)(getWidth() / -2 * yScale);
    }

    public int getLeft()
    {
        return (int)(getHeight() / -2 * xScale);
    }

    public int getRight()
    {
        return (int)(getHeight() / 2 * xScale);
    }

    public void setxScale(float xScale)
    {
        this.xScale = xScale;
    }

    public void setyScale(float yScale)
    {
        this.yScale = yScale;
    }

    public void setzScale(float zScale)
    {
        this.zScale = zScale;
    }
}
