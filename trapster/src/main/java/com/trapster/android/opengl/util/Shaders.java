package com.trapster.android.opengl.util;

import android.opengl.GLES20;

public abstract class Shaders
{
    private Shaders(){} // To prevent instantiation


    // Stolen, with little shame, from Google:
    // http://developer.android.com/training/graphics/opengl/draw.html
   private final static String vertexShaderCode = "uniform mat4 uMVPMatrix; attribute vec4 aPosition; void main() { gl_PointSize = 45.0; gl_Position = aPosition;}";
   private final static String fragmentShaderCode = "precision mediump float; uniform vec4 uColor; void main() {gl_FragColor = vec4(1, 1, 1, 1);}";

    private final static String textureVertexShaderCode =
            "attribute vec2 aTexPos;" +
            "uniform mat4 uMVPMatrix;" +
            "attribute vec3 aVerPos;\n" +
            "attribute vec4 aVerCol;\n" +
            "varying vec4 vVerCol;\n" +
            "varying vec2 vTexPos;\n" +
            "\n" +
            "void main(void) {\n" +
            "    vTexPos = aTexPos;\n" +
            "    vVerCol = aVerCol;\n" +
            "    gl_Position = uMVPMatrix * vec4(aVerPos, 1.0);\n" +
            "}";

    private final static String textureFragmentShaderCode =
    "precision mediump float;\n" +
            "\n" +
            "varying vec4 vVerCol;\n" +
            "varying vec2 vTexPos;\n" +
            "\n" +
            "uniform sampler2D uSampler;\n" +
            "\n" +
            "void main(void) {\n" +
            "    gl_FragColor = texture2D(uSampler, vTexPos);\n" +
            "}";

    private final static String lineVertexShaderCode = "attribute vec4 a_position; uniform mat4 uMVPMatrix;\n"+
            "void main()\n" +
            "{\n" +
            "gl_PointSize = 7.0;\n" +
            "gl_Position = uMVPMatrix * a_position;\n"+
            "}";
    private final static String lineFragmentShaderCode = "precision mediump float; uniform vec4 uColor; " +
            "void main() " +
            "{" +
            "gl_FragColor = uColor;" +
            "}";

    public static int loadShader(int type, String shaderCode)
    {
        // create a vertex shader type (GLES20.GL_VERTEX_SHADER)
        // or a fragment shader type (GLES20.GL_FRAGMENT_SHADER)
        int shader = GLES20.glCreateShader(type);

        // add the source code to the shader and compile it
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);

        return shader;
    }

    public static int loadFragmentShader()
    {
        return loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode);
    }

    public static int loadVertexShader()
    {
        return loadShader(GLES20.GL_VERTEX_SHADER, vertexShaderCode);
    }

    public static int loadTextureFragmentShader()
    {
        return loadShader(GLES20.GL_FRAGMENT_SHADER, textureFragmentShaderCode);
    }

    public static int loadTextureVertexShader()
    {
        return loadShader(GLES20.GL_VERTEX_SHADER, textureVertexShaderCode);
    }

    public static int loadLineVertexShader()
    {
        return loadShader(GLES20.GL_VERTEX_SHADER, lineVertexShaderCode);
    }

    public static int loadLineFragmentShader()
    {
        return loadShader(GLES20.GL_FRAGMENT_SHADER, lineFragmentShaderCode);
    }

}
