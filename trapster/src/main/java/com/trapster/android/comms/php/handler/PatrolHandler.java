package com.trapster.android.comms.php.handler;

import com.trapster.android.PHPDefaults;
import com.trapster.android.comms.ErrorListener;
import com.trapster.android.model.PatrolPath;
import com.trapster.android.model.PatrolPaths;
import com.trapster.android.model.PatrolPoint;
import org.xml.sax.Attributes;


public class PatrolHandler extends TrapsterHandler
{
    PatrolPaths patrolPaths= new PatrolPaths();
    PatrolPath path;
    PatrolPoint point;
    //
    public PatrolHandler(ErrorListener listener)
    {
        super(listener);
    }
    public PatrolPaths getPatrolPaths()
    {
        return patrolPaths;
    }
    protected void onEndElement(String uri, String localName, String qName)
    {
        if (PHPDefaults.XML_RSP_PATH.equals(localName))
        {
            patrolPaths.getPaths().add(path);
            path= null;
        } else if (PHPDefaults.XML_RSP_POINT.equals(localName))
        {
            path.getPoints().add(point);
            point= null;
        } else if (PHPDefaults.XML_RSP_POINT_LAT.equals(localName))
        {
            point.setLat(Double.valueOf(buffer.toString()));
            buffer= null;
        } else if (PHPDefaults.XML_RSP_POINT_LON.equals(localName))
        {
            point.setLon(Double.valueOf(buffer.toString()));
            buffer= null;
        } else if (PHPDefaults.XML_RSP_POINT_COLOR.equals(localName))
        {
            point.setColor(buffer.toString());
            buffer= null;
        }
    }
    protected void onStartElement(String uri, String localName, String qName, Attributes attributes)
    {
        if (PHPDefaults.XML_RSP_PATH.equals(localName))
            path= new PatrolPath();
        else if (PHPDefaults.XML_RSP_POINT.equals(localName))
            point= new PatrolPoint();
        else if (point != null)
            buffer= new StringBuilder();
    }
}

