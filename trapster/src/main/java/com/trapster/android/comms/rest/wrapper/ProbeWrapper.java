package com.trapster.android.comms.rest.wrapper;

import com.google.gson.annotations.SerializedName;
import com.trapster.android.RESTDefaults;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 5/29/13
 * Time: 10:33 AM
 * To change this template use File | Settings | File Templates.
 */
public class ProbeWrapper {

    @SerializedName(RESTDefaults.REST_PARAMATER_PROBE_POINTS)
    Object[] points;

    public ProbeWrapper (Object [] probePoints)
    {
        points = probePoints;
    }

}
