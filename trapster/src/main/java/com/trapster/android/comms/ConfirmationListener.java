package com.trapster.android.comms;

import com.trapster.android.model.User;

public interface ConfirmationListener {
    public void onResend(User user);
    public void onLogin(User user, String code);
}
