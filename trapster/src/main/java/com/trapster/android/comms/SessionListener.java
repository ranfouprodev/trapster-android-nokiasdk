package com.trapster.android.comms;


import com.trapster.android.comms.rest.StatusCode;
import com.trapster.android.model.User;

public interface SessionListener extends ErrorListener{
    public void onLoginSuccess(User user,String optionalMessage);
    public void onLoginFail(User user,StatusCode code);
}
