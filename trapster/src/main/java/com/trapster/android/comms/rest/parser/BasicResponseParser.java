package com.trapster.android.comms.rest.parser;


import com.trapster.android.comms.BasicResponseListener;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.service.comms.JsonParser;

/**
 * Created by John on 5/28/13.
 *
 */
public class BasicResponseParser extends JsonParser {

    private static final String LOGNAME = "BasicResponseParser";
    private final BasicResponseListener listener;

    public BasicResponseParser(BasicResponseListener listener)
    {
        super();
        this.listener= listener;
    }

    @Override
    public void parseJson(Status status, String jsonStream) {

        if(listener != null)
            if(status.status.equalsIgnoreCase("OK"))
                listener.onComplete();
            else
                listener.onError(new TrapsterError(TrapsterError.TYPE_API_ERROR,"Status:"+status.statusCode));


    }



}
