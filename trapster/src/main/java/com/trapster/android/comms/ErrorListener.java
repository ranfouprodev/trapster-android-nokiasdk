package com.trapster.android.comms;

import com.trapster.android.util.TrapsterError;

public interface ErrorListener {
	public void onError(TrapsterError error);
	
}
