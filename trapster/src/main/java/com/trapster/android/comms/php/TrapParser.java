package com.trapster.android.comms.php;

import java.io.InputStream;

import javax.xml.parsers.SAXParserFactory;

import com.trapster.android.comms.TrapListener;
import com.trapster.android.comms.php.handler.TrapsHandler;
import com.trapster.android.model.Traps;
import com.trapster.android.util.TrapsterError;

public class TrapParser extends TrapsterStreamParser
{
	static final String LOGNAME= "Trapster.TrapParser";
	//
	TrapListener listener;
	private double lat;
	//
	private double lon;
	private double radius;
	
	public TrapParser(TrapListener listener, double lat, double lon,
			double radius) {
		this.listener = listener;
		this.lat = lat; 
		this.lon = lon; 
		this.radius = radius;
		
	}
	public void parse(InputStream in)
	{
		try
		{
			if (listener != null)
			{
				TrapsHandler handler= new TrapsHandler(listener);
				SAXParserFactory.newInstance().newSAXParser().parse(in, handler);
				Traps traps = handler.getTraps();
				traps.setLat(lat);
				traps.setLon(lon);
				traps.setRadius(radius);
				
				listener.onTraps(traps);
			}
		} catch (Exception e)
		{
			if (listener != null)
				listener.onError(new TrapsterError(TrapsterError.TYPE_INVALID_XML_FORMAT, " Unable to import traps:"
						+ e.getMessage()));
		}
	}
}
