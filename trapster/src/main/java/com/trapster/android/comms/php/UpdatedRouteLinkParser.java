package com.trapster.android.comms.php;

import com.trapster.android.comms.UpdatedRouteLinkListener;
import com.trapster.android.comms.php.handler.UpdatedRouteLinkHandler;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.service.comms.Parser;

import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 12/8/12
 * Time: 2:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class UpdatedRouteLinkParser extends Parser {

    UpdatedRouteLinkListener listener;

    public UpdatedRouteLinkParser(UpdatedRouteLinkListener updatedRouteLinkListener)
    {
       listener = updatedRouteLinkListener;
    }

    @Override
    public void process(InputStream in) throws IOException {
        try
        {
            if (listener != null)
            {
                UpdatedRouteLinkHandler handler= new UpdatedRouteLinkHandler(listener);
                SAXParserFactory.newInstance().newSAXParser().parse(in, handler);
                listener.onUpdatedRouteLinkReceived(handler.getRouteLinkInfo());
            }
        } catch (Exception e)
        {
            if (listener != null)
                listener.onError(new TrapsterError(TrapsterError.TYPE_INVALID_XML_FORMAT,
                        " Unable to import Speed Limit data:" + e.getMessage()));
        }
    }
}
