package com.trapster.android.comms;

public interface TrapUpdateMonitorListener {
    void onTrapUpdateRequest(double lat, double lng, double radius);
}
