package com.trapster.android.comms.rest.parser;

import com.trapster.android.RESTDefaults;
import com.trapster.android.comms.StatisticsListener;
import com.trapster.android.model.UserStatistics;
import com.twolinessoftware.android.framework.service.comms.JsonParser;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class StatisticsParser extends JsonParser
{
    private StatisticsListener listener;
    private UserStatistics userStatistics;

    public StatisticsParser(StatisticsListener listener)
    {
        this.listener = listener;
        userStatistics = new UserStatistics();
    }

    @Override
    public void parseJson(Status status, String jsonString)
    {
        // This is easier to do explicitly instead of @SerializedName since it doesn't match the Services structure
        //UserStatistics userStatistics = gson.fromJson(jsonString, UserStatistics.class););
    }

    private void processReports(JSONObject reportObject) throws JSONException
    {
        UserStatistics.TrapStatistics trapStatistics = userStatistics.getTrapStatistics();

        JSONObject trapData = reportObject.getJSONObject("data");
        Iterator<String> trapIterator = trapData.keys();
        while (trapIterator.hasNext())
        {
            String trapID = trapIterator.next();
            JSONObject trapObject = trapData.getJSONObject(trapID);
            //String name = trapObject.getString("name");
            int count = trapObject.getInt("count");
            trapStatistics.addTrapStat(trapID, count);
        }

        if (listener != null)
            listener.onTrapStatisticsLoaded(userStatistics);
    }

    private void processMyTrapVotes(JSONObject reportObject) throws JSONException
    {
        UserStatistics.MyVoteStatistics voteStatistics = userStatistics.getMyVoteStatistics();

        JSONObject trapData = reportObject.getJSONObject("breakdown");
        Iterator<String> trapIterator = trapData.keys();
        while (trapIterator.hasNext())
        {
            String trapID = trapIterator.next();
            JSONObject trapObject = trapData.getJSONObject(trapID);
            //String name = trapObject.getString("name");
            int count = trapObject.getInt("Y");
            voteStatistics.addTrapStat(trapID, count);
        }
    }

    private void processOtherTrapVotes(JSONObject reportObject) throws JSONException
    {
        UserStatistics.VotesOnMyTrapsStatistics voteStatistics = userStatistics.getVotesOnMyTrapsStatistics();

        JSONObject trapData = reportObject.getJSONObject("breakdown");
        Iterator<String> trapIterator = trapData.keys();
        while (trapIterator.hasNext())
        {
            String trapID = trapIterator.next();
            JSONObject trapObject = trapData.getJSONObject(trapID);
            //String name = trapObject.getString("name");
            int count = trapObject.getInt("Y");
            voteStatistics.addTrapStat(trapID, count);
        }
    }

    @Override
    public void process(int responseCode, String jsonString) throws Exception
    {

        setResponseCode(responseCode);

        JSONObject jsonResponseObject = new JSONObject(jsonString);
        //String responseString = jsonResponseObject.get(RESTDefaults.REST_STATISTICS_RESPONSE_USER_STATS).toString();
        // checkErrors(responseString);

        //JSONObject jsonDataObject = new JSONObject(responseString);
        String dataString = null;
        if (jsonResponseObject.has(RESTDefaults.REST_RESPONSE_DATA))
        {
            JSONObject dataObject = jsonResponseObject.getJSONObject(RESTDefaults.REST_RESPONSE_DATA).getJSONObject("userstats");
            ;
            processReports(dataObject.getJSONObject("reported"));
            processMyTrapVotes(dataObject.getJSONObject("my_trap_votes"));
            processOtherTrapVotes(dataObject.getJSONObject("votes_on_my_traps"));
        }

        parseJson(getStatus(), dataString);

    }
}
