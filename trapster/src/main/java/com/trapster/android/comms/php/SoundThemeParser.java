package com.trapster.android.comms.php;

import com.trapster.android.comms.SoundThemeListener;
import com.trapster.android.comms.php.handler.SoundThemesHandler;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.service.comms.Parser;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.SAXParserFactory;

public class SoundThemeParser extends Parser
{
    static final String LOGNAME= "Trapster.SoundThemeParser";
    //
    SoundThemeListener listener;
    //
    public SoundThemeParser(SoundThemeListener listener)
    {
        this.listener= listener;
    }
    /*public void parse(InputStream in)
    {
        try
        {
            if (listener != null)
            {
                SoundThemesHandler handler= new SoundThemesHandler(listener);
                SAXParserFactory.newInstance().newSAXParser().parse(in, handler);
                SoundThemes themes;
                themes =  handler.getSoundThemes();
                listener.onSoundThemeChanges(handler.getSoundThemes());
            }
        } catch (Exception e)
        {
            if (listener != null)
                listener.onError(new TrapsterError(TrapsterError.TYPE_INVALID_XML_FORMAT,
                        " Unable to import sound themes:" + e.getMessage()));
        }
    }*/

    @Override
    public void process(InputStream stream) throws IOException {
        //To change body of implemented methods use File | Settings | File Templates.
        try
        {
            if (listener != null)
            {
                SoundThemesHandler handler= new SoundThemesHandler(listener);

                SAXParserFactory.newInstance().newSAXParser().parse(stream, handler);

                listener.onSoundThemeChanges(handler.getSoundThemes());

            }
        } catch (Exception e)
        {
            if (listener != null)
                listener.onError(new TrapsterError(TrapsterError.TYPE_INVALID_XML_FORMAT,
                        " Unable to import sound themes:" + e.getMessage()));
        }
    }
}
