package com.trapster.android.comms.rest.wrapper;

import com.google.gson.annotations.SerializedName;
import com.trapster.android.RESTDefaults;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 5/29/13
 * Time: 10:36 AM
 * To change this template use File | Settings | File Templates.
 */
public class RequestWrapper {

    @SerializedName(RESTDefaults.REST_PARAMETER_AUTH)
    AuthWrapper auth;
    @SerializedName(RESTDefaults.REST_PARAMETER_META)
    MetaWrapper meta;
    @SerializedName(RESTDefaults.REST_PARAMETER_PARAMS)
    Object params;

    public RequestWrapper(AuthWrapper authWrapper, MetaWrapper metaDataParameters, Object paramsString)
    {
        auth = authWrapper;
        meta = metaDataParameters;
        params = paramsString;
    }
}
