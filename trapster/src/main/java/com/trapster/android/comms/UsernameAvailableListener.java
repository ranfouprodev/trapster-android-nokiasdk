package com.trapster.android.comms;

/**
 * Created by John on 8/20/13.
 */
public interface UsernameAvailableListener {
    void onUsernameAvailable(String name, boolean available);
}
