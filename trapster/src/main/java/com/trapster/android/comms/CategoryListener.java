package com.trapster.android.comms;

import com.trapster.android.model.Categories;

public interface CategoryListener extends ErrorListener {
	void onCategories(Categories categories);
}
