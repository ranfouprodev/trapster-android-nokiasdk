package com.trapster.android.comms.rest.parser;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 5/22/13
 * Time: 4:03 PM
 * To change this template use File | Settings | File Templates.
 */

import com.trapster.android.comms.PatrolListener;
import com.trapster.android.model.PatrolPaths;
import com.trapster.android.util.Log;
import com.twolinessoftware.android.framework.service.comms.JsonParser;

public class PatrolParser extends JsonParser{

    private static final String LOGNAME = "PatrolParser";
    private final PatrolListener listener;
    private final double lat;
    private final double lng;
    private final double radius;

    public PatrolParser(PatrolListener listener, double lat, double lng, double radius) {
        super();
        this.lat = lat;
        this.lng = lng;
        this.radius = radius;
        this.listener = listener;
    }

    @Override
    public void parseJson(Status status, String jsonStream) {


        try{

            PatrolPaths patrolPaths = gson.fromJson(jsonStream, PatrolPaths.class);

            if(listener != null)
                listener.onPatrolPath(patrolPaths,lat,lng,radius);

        }catch(Exception e){
            Log.e(LOGNAME, "Patrol parsing:" + e.getMessage());
        }


    }

  }
