package com.trapster.android.comms.php;

import java.io.InputStream;

import com.trapster.android.Defaults;
import com.trapster.android.comms.ErrorListener;
import com.trapster.android.util.Log;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.service.comms.Parser;

public abstract class TrapsterStreamParser extends Parser
{
	private static final String LOGNAME = "TrapsterParser";
		
	public void process(InputStream stream)
	{
		parse(stream);
	}
	
	public abstract void parse(InputStream stream);
	
	
	
	
}
