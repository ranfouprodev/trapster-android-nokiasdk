package com.trapster.android.comms.php.handler;

import com.trapster.android.comms.ErrorListener;
import com.trapster.android.model.Profile;
import org.xml.sax.Attributes;

import java.text.SimpleDateFormat;


public class ProfileHandler extends TrapsterHandler
{
	private static final String LOGNAME = "ProfileHandler";


	private static final String XML_RSP_PROFILE = "profile";


	private static final String XML_RSP_NAME = "name";


	private static final String XML_RSP_EMAIL = "uname";


	private static final String XML_RSP_KARMA = "karma";


	private static final String XML_RSP_SIGNUP = "signupdate";
	
	private static final String XML_RSP_NUMVOTES = "numvotes";
    private static final String XML_RSP_GLOBAL_MODERATOR = "global_moderator";
	
	
	private Profile profile = new Profile();


	/**
	 * The profile xml has a profile tag embedded in the profile (I know, amirite?)  <profile>..<profile/> ...</profile> so we ignore it if
	 * the parsing is already started
	 */
	private boolean started; 
	
	public ProfileHandler(ErrorListener listener)
	{
		super(listener);
		buffer= new StringBuilder();
		started = false; 
	}

	public Profile getProfile()
	{
		return profile;
	}
	
	protected void onStartElement(String uri, String localName, String qName, Attributes attributes)
	{
		buffer= new StringBuilder();
		
		if (XML_RSP_PROFILE.equals(localName) && !started){
			profile = new Profile(); 
			started = true; 
		}
				
		
	}
	
	protected void onEndElement(String uri, String localName, String qName)
	{
		if (XML_RSP_NAME.equals(localName))
		{
			profile.setName(buffer.toString());
	
		} else if (XML_RSP_NUMVOTES.equals(localName))
		{
			try{
				profile.setNumVotes(Integer.parseInt(buffer.toString()));
			}catch(NumberFormatException ne){
{/* 				//Log.e(LOGNAME,"Error Parsing Num Votes Value:"+ne.getMessage()); */}
			}
		
		}else if (XML_RSP_EMAIL.equals(localName))
		{
			profile.setEmail(buffer.toString());
		
		} else if (XML_RSP_KARMA.equals(localName))
		{
			try{
				profile.setKarma(Integer.parseInt(buffer.toString()));
			}catch(NumberFormatException ne){
{/* 				//Log.e(LOGNAME,"Error Parsing Karma Value:"+ne.getMessage()); */}
			}
			
		} else if (XML_RSP_SIGNUP.equals(localName))
		{
			try{
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				profile.setSignUp(df.parse(buffer.toString()));
			}catch(Exception e){
{/* 				//Log.e(LOGNAME,"Error Parsing Date Value:"+e.getMessage()); */}
			}
			
			
		}
        else if (XML_RSP_GLOBAL_MODERATOR.equals(localName))
        {
            profile.setGlobalModerator(Integer.parseInt(buffer.toString()));
        }
	}
	
}
