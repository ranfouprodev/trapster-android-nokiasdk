package com.trapster.android.comms.php;

import java.io.InputStream;
import javax.xml.parsers.SAXParserFactory;

import com.trapster.android.comms.PatrolListener;
import com.trapster.android.comms.php.handler.PatrolHandler;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.service.comms.Parser;

public class PatrolParser extends Parser
{
    static final String LOGNAME= "Trapster.PatrolParser";
    //
    PatrolListener listener;
    double radius;
    double lat;
    double lon;
    //
    public PatrolParser(PatrolListener listener, double lat, double lon, double radius)
    {
        this.listener= listener;
        this.radius= radius;
        this.lat= lat;
        this.lon= lon;
    }
    public void process(InputStream in)
    {
        try
        {
            if (listener != null)
            {
                PatrolHandler handler= new PatrolHandler(listener);
                SAXParserFactory.newInstance().newSAXParser().parse(in, handler);
                listener.onPatrolPath(handler.getPatrolPaths(), lat, lon, radius);
            }
        } catch (Exception e)
        {
            if (listener != null)
                listener.onError(new TrapsterError(TrapsterError.TYPE_INVALID_XML_FORMAT, " Unable to import patrol:"
                        + e.getMessage()));
        }
    }
}
