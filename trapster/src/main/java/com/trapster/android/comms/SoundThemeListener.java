package com.trapster.android.comms;

import com.trapster.android.model.SoundThemes;

public interface SoundThemeListener extends ErrorListener {
    void onSoundThemeChanges(SoundThemes themes);
}
