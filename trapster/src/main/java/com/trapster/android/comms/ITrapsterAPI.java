package com.trapster.android.comms;

import com.here.android.common.GeoPosition;
import com.trapster.android.model.ProbeData;
import com.trapster.android.model.Trap;
import com.trapster.android.model.UpdatedRouteLinkInfo;
import com.trapster.android.model.User;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;

import java.util.ArrayList;
import java.util.List;


public interface ITrapsterAPI {

	public abstract void login(User user, LoginListener listener,
			CommunicationStatusListener statusListener);

	public abstract void login(User user, String confcode,
			LoginListener listener, CommunicationStatusListener statusListener);

	public abstract void resendCode(User user, LoginListener listener,
			CommunicationStatusListener statusListener);

	public abstract void register(String email, String userName,
			String password, boolean agree, boolean newsletter, String smsaddr, String carrier,
			LoginListener listener, CommunicationStatusListener statusListener);

	public abstract void getAttributes(long lastUpdateTime,
			AttributesListener listener,
			CommunicationStatusListener statusListener);

	public abstract void getLegalAttributes(long lastUpdateTime,
			AttributesListener listener,
			CommunicationStatusListener statusListener);

	public abstract void getCategories(long lastUpdateTime,
			CategoryListener listener,
			CommunicationStatusListener statusListener);

	public abstract void getTrapTypes(long lastUpdateTime,
			TrapTypeListener listener,
			CommunicationStatusListener statusListener);

	public abstract void sendProbeData(List<ProbeData> locations,
			CommunicationStatusListener statusListener);

	public abstract void sendNPSResults(String vote, String feedback,
			String emailAddress, CommunicationStatusListener statusListener);
	
	// Trap Methods
	
	public abstract void updateAllTraps(double lat, double lon, double radius,
			TrapListener listener, CommunicationStatusListener statusListener);

	public abstract void rateTrap(boolean agree, Trap trap,
			BasicResponseListener confirmListener, CommunicationStatusListener statusListener);

	public abstract void addNewTrap(Trap trap, NewTrapListener listener,
			CommunicationStatusListener statusListener, double heading);

	public abstract void deleteTrap(Trap trap,BasicResponseListener listener,
			CommunicationStatusListener statusListener);

    public abstract void ignoreTrap(Trap trap,
                                    CommunicationStatusListener statusListener);

    public abstract void listSounds(long lastUpdateTime, SoundThemeListener listener,
                                     CommunicationStatusListener statusListener);

    public void sendIAmHere(List<GeoPosition> locations,
                            CommunicationStatusListener statusListener);

    public void getPath(double lat, double lon, double radius,
                        PatrolListener listener, CommunicationStatusListener statusListener);

    //updated Speed Limit Methods

    public void updateNewSpeedLimit(ArrayList<UpdatedRouteLinkInfo> updatedRouteLinkInfo,
                                    CommunicationStatusListener statusListener);

    public void getNewSpeedLimit(UpdatedRouteLinkListener updatedRouteLinkListener,
                                 CommunicationStatusListener communicationStatusListener);

    public void getUserStatistics(String username, StatisticsListener statisticsListener, CommunicationStatusListener statusListener);

    //Get Launch Message

    public abstract void getLaunchMessage(LaunchMessageListener listener, CommunicationStatusListener statusListener);

    // Submit become a moderator request.  We don't care about the response, so no listeners are passed
    public void submitBecomeModeratorRequest();
	
}
