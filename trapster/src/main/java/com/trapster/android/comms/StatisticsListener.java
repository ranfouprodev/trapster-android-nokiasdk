package com.trapster.android.comms;

import com.trapster.android.model.UserStatistics;

public interface StatisticsListener extends ErrorListener
{
    public void onTrapStatisticsLoaded(UserStatistics userStatistics);
}
