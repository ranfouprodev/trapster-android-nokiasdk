package com.trapster.android.comms.php.handler;

import com.trapster.android.PHPDefaults;
import com.trapster.android.comms.ErrorListener;
import com.trapster.android.model.LaunchMessage;
import org.xml.sax.Attributes;


/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 3/8/13
 * Time: 11:42 AM
 * To change this template use File | Settings | File Templates.
 */
public class LaunchMessageHandler extends TrapsterHandler {

    private LaunchMessage launchMessage;

    public LaunchMessageHandler(ErrorListener listener) {
        super(listener);
    }

    public LaunchMessage getLaunchMessage()
    {
        return launchMessage;
    }

    protected void onStartElement(String uri, String localName, String qName, Attributes attributes)
    {
        if (PHPDefaults.XML_RSP_DATA.equals(localName))
            launchMessage = new LaunchMessage();
        else if (launchMessage != null)
            buffer= new StringBuilder();
    }

    protected void onEndElement(String uri, String localName, String qName)
    {
        if (PHPDefaults.XML_RSP_LAUNCH_MESSAGE_DISPLAY.equals(localName))
        {
            launchMessage.setDisplayText(buffer.toString());
            buffer = null;
        }
        else if (PHPDefaults.XML_RSP_LAUNCH_MESSAGE_TITLE.equals(localName))
        {
            launchMessage.setTitle(buffer.toString());
            buffer = null;
        }
        else if (PHPDefaults.XML_RSP_LAUNCH_MESSAGE_BUTTON.equals(localName))
        {
            launchMessage.setButtonText(buffer.toString());
            buffer = null;
        }
        else if (PHPDefaults.XML_RSP_LAUNCH_MESSAGE_URL.equals(localName))
        {
            launchMessage.setUrl(buffer.toString());
            buffer = null;
        }
        else if (PHPDefaults.XML_RSP_LAUNCH_MESSAGE_MESSAGE.equals(localName))
        {
            launchMessage.setMessage(buffer.toString());
            buffer = null;
        }
    }
}
