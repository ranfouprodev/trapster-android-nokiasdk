package com.trapster.android.comms.nlp;

import com.google.gson.Gson;
import com.here.android.common.GeoCoordinate;
import com.trapster.android.NLPDefaults;

import javax.inject.Singleton;
import java.net.URLEncoder;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 10/3/13
 * Time: 9:11 AM
 * To change this template use File | Settings | File Templates.
 */

/*
Not sure if we need this as a singleton. What state is maintained here?
 */
@Singleton
public class NLPJsonWriter {

    private final Gson gson;

    public NLPJsonWriter()
    {
        gson = new Gson();
    }

    public String createNLPQuadkeyRequest(String quadkey)
    {
        StringBuffer sb = new StringBuffer(NLPDefaults.NLP_GET_LINK_INFO);
        sb.append(encodeKeyValue(NLPDefaults.NLP_QUERY_PARAM_APP_ID, NLPDefaults.NLP_APP_ID) + "&");
        sb.append(encodeKeyValue(NLPDefaults.NLP_QUERY_PARAM_APP_CODE, NLPDefaults.NLP_APP_CODE) + "&");
        sb.append(encodeKeyValue(NLPDefaults.NLP_QUERY_PARAM_QUADKEY, quadkey) + "&");
        sb.append(NLPDefaults.NLP_QUERY_PARAM_LINK_ATTRIBUTES + NLPDefaults.NLP_DEFAULT_LINK_ATTRIBUTES);

        return sb.toString();
    }

    public String createNLPWaypointRequest(String lat, String lng)
    {
        StringBuffer sb = new StringBuffer(NLPDefaults.NLP_GET_LINK_INFO);
        sb.append(encodeKeyValue(NLPDefaults.NLP_QUERY_PARAM_APP_ID, NLPDefaults.NLP_APP_ID) + "&");
        sb.append(encodeKeyValue(NLPDefaults.NLP_QUERY_PARAM_APP_CODE, NLPDefaults.NLP_APP_CODE) + "&");
        sb.append(NLPDefaults.NLP_QUERY_PARAM_WAYPOINT + lat + "," + lng + "&");
        sb.append(NLPDefaults.NLP_QUERY_PARAM_LINK_ATTRIBUTES + NLPDefaults.NLP_DEFAULT_LINK_ATTRIBUTES);

        return sb.toString();
    }

    public String createNLPRoutingRequest(GeoCoordinate startPosition, GeoCoordinate endPosition, String routingType)
    {
        StringBuffer sb = new StringBuffer(NLPDefaults.NLP_CALCULATE_ROUTE);
        sb.append(encodeKeyValue(NLPDefaults.NLP_QUERY_PARAM_APP_ID, NLPDefaults.NLP_APP_ID) + "&");
        sb.append(encodeKeyValue(NLPDefaults.NLP_QUERY_PARAM_APP_CODE, NLPDefaults.NLP_APP_CODE) + "&");
        sb.append(NLPDefaults.NLP_QUERY_PARAM_WAYPOINT_0 + startPosition.getLatitude() + "," + startPosition.getLongitude() + "&");
        sb.append(NLPDefaults.NLP_QUERY_PARAM_WAYPOINT_1 + endPosition.getLatitude() + "," + endPosition.getLongitude() + "&");
        sb.append(NLPDefaults.NLP_QUERY_PARAM_MODE + routingType + ";" + NLPDefaults.NLP_TRANSPORT_TYPE_CAR + ";" + NLPDefaults.NLP_TRAFFIC_TYPE_ENABLED + "&");
        sb.append(NLPDefaults.NLP_QUERY_PARAM_REPRESENTATION + NLPDefaults.NLP_DEFAULT_REPRESENTATION_ATTRIBUTES_LINK_PAGING);

        return sb.toString();
    }

    public String createNLPRoutingRequestSummary(GeoCoordinate startPosition, GeoCoordinate endPosition, String routingType)
    {
        StringBuffer sb = new StringBuffer(NLPDefaults.NLP_CALCULATE_ROUTE);
        sb.append(encodeKeyValue(NLPDefaults.NLP_QUERY_PARAM_APP_ID, NLPDefaults.NLP_APP_ID) + "&");
        sb.append(encodeKeyValue(NLPDefaults.NLP_QUERY_PARAM_APP_CODE, NLPDefaults.NLP_APP_CODE) + "&");
        sb.append(NLPDefaults.NLP_QUERY_PARAM_WAYPOINT_0 + startPosition.getLatitude() + "," + startPosition.getLongitude() + "&");
        sb.append(NLPDefaults.NLP_QUERY_PARAM_WAYPOINT_1 + endPosition.getLatitude() + "," + endPosition.getLongitude() + "&");
        sb.append(NLPDefaults.NLP_QUERY_PARAM_MODE + routingType + ";" + NLPDefaults.NLP_TRANSPORT_TYPE_CAR + ";" + NLPDefaults.NLP_TRAFFIC_TYPE_ENABLED + "&");
        sb.append(NLPDefaults.NLP_QUERY_PARAM_REPRESENTATION + NLPDefaults.NLP_DEFAULT_REPRESENTATION_ATTRIBUTES_OVERVIEW);

        return sb.toString();
    }

    private String encodeKeyValue (String key, String value )
    {
        String encoded = "";
        try
        {
            encoded = key + "=" + URLEncoder.encode(value, "utf-8");
        }
        catch (Exception e){}

        return encoded;
    }
}
