package com.trapster.android.comms;


import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 10/24/13
 * Time: 3:08 PM
 * To change this template use File | Settings | File Templates.
 */
public interface INLPAPI {

    public abstract void getQuadkeyLinkInfo(String quadkey, TrafficRouteLinkListener linkListener, CommunicationStatusListener statusListener);

    public abstract void getWaypointLinkInfo(double lat, double lng, TrafficRouteLinkListener linkListener, CommunicationStatusListener statusListener);

}
