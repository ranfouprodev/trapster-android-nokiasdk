package com.trapster.android.comms.rest.parser;

import com.trapster.android.comms.CategoryListener;
import com.trapster.android.model.Categories;
import com.trapster.android.util.Log;
import com.twolinessoftware.android.framework.service.comms.JsonParser;

/**
 * Created by John on 5/28/13.
 */
public class TrapCategoryParser extends JsonParser {

    private static final String LOGNAME = "TrapCategoryParser";
    private final CategoryListener listener;

    public TrapCategoryParser(CategoryListener listener)
    {
        super();
        this.listener= listener;
    }

    @Override
    public void parseJson(Status status, String jsonStream) {


        try{

            Categories categories =  gson.fromJson(jsonStream, Categories.class);

        if(listener != null)
            listener.onCategories(categories);

        }catch(Exception e){
            Log.e(LOGNAME,"TrapCategory parsing:"+e.getMessage());
        }

    }



}
