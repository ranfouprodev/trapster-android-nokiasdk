package com.trapster.android.comms.rest;

import com.trapster.android.comms.BasicResponseListener;
import com.trapster.android.comms.EmailAvailableListener;
import com.trapster.android.comms.LoginListener;
import com.trapster.android.comms.UsernameAvailableListener;
import com.trapster.android.model.User;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;

/**
 * Created by John on 8/20/13.
 */
public interface IUserAPI {

    public void getUserByFacebookToken(User user,LoginListener listener,
                                       CommunicationStatusListener statusListener);

    public void isUsernameTaken(String username, UsernameAvailableListener listener, CommunicationStatusListener statusListener);

    public void signUpWithFacebookToken(User user, LoginListener listener,
                                        CommunicationStatusListener statusListener);

    public void mergeWithFacebook(User user, String token, BasicResponseListener listener, CommunicationStatusListener statusListener);

    public void forgotPassword(String name, BasicResponseListener listener,
                                        CommunicationStatusListener statusListener);

    public void isEmailTaken(String email, EmailAvailableListener listener, CommunicationStatusListener statusListener);



}
