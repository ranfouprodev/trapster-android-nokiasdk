package com.trapster.android.comms;

import com.trapster.android.model.UpdatedRouteLinkInfo;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 12/8/12
 * Time: 2:27 PM
 * To change this template use File | Settings | File Templates.
 */
public interface UpdatedRouteLinkListener extends ErrorListener {

    void onUpdatedRouteLinkReceived(ArrayList<UpdatedRouteLinkInfo> updatedRouteLinkInfo);
}
