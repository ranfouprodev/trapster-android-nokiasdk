package com.trapster.android.comms.rest;

import android.app.Application;

import com.here.android.common.GeoPosition;
import com.trapster.android.RESTDefaults;
import com.trapster.android.comms.AttributesListener;
import com.trapster.android.comms.BasicResponseListener;
import com.trapster.android.comms.CategoryListener;
import com.trapster.android.comms.CommunicationManager;
import com.trapster.android.comms.ITrapsterAPI;
import com.trapster.android.comms.LaunchMessageListener;
import com.trapster.android.comms.LoginListener;
import com.trapster.android.comms.NewTrapListener;
import com.trapster.android.comms.PatrolListener;
import com.trapster.android.comms.SoundThemeListener;
import com.trapster.android.comms.StatisticsListener;
import com.trapster.android.comms.TrapListener;
import com.trapster.android.comms.TrapTypeListener;
import com.trapster.android.comms.UpdatedRouteLinkListener;
import com.trapster.android.comms.rest.parser.AttributesParser;
import com.trapster.android.comms.rest.parser.BasicResponseParser;
import com.trapster.android.comms.rest.parser.NewTrapParser;
import com.trapster.android.comms.rest.parser.PatrolParser;
import com.trapster.android.comms.rest.parser.SoundThemesParser;
import com.trapster.android.comms.rest.parser.StatisticsParser;
import com.trapster.android.comms.rest.parser.TrapCategoryParser;
import com.trapster.android.comms.rest.parser.TrapParser;
import com.trapster.android.comms.rest.parser.TrapTypeParser;
import com.trapster.android.comms.rest.parser.UserAuthenicationParser;
import com.trapster.android.manager.SessionManager;
import com.trapster.android.model.ProbeData;
import com.trapster.android.model.Trap;
import com.trapster.android.model.UpdatedRouteLinkInfo;
import com.trapster.android.model.User;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;
import com.twolinessoftware.android.framework.service.comms.HttpDeleteJsonWorker;
import com.twolinessoftware.android.framework.service.comms.HttpGetJsonWorker;
import com.twolinessoftware.android.framework.service.comms.HttpGetWorker;
import com.twolinessoftware.android.framework.service.comms.HttpPostJsonWorker;
import com.twolinessoftware.android.framework.service.comms.HttpPutJsonWorker;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

public class RESTTrapsterAPI implements ITrapsterAPI {
    private static final String LOGNAME = "RESTTrapsterAPI";
    @Inject
    JSONWriter jsonWriter;

    @Inject
    CommunicationManager communicationManager;

    @Inject
    SessionManager sessionManager;

    @Inject
    Application application;

    @Override
    public void login(User user, LoginListener listener, CommunicationStatusListener statusListener)
    {
        String URL = null;
        try {
            URL = RESTDefaults.REST_API_RESOURCE_USER + URLEncoder.encode(user.getUserName(), "UTF-8") + RESTDefaults.REST_API_RESOURCE_USER_AUTHENTICATION;
        } catch (UnsupportedEncodingException e) {
            URL = RESTDefaults.REST_API_RESOURCE_USER + user.getUserName() + RESTDefaults.REST_API_RESOURCE_USER_AUTHENTICATION;
        }
        String requestString = jsonWriter.createLoginRequest(user);

        HttpPostJsonWorker worker = new HttpPostJsonWorker(URL, requestString);
        communicationManager.handleAsyncMessage(worker, new UserAuthenicationParser(user,listener, application, false), statusListener);
    }

    @Override
    public void login(User user, String confcode, LoginListener listener, CommunicationStatusListener statusListener) {

        String URL = null;
        try {
            URL = RESTDefaults.REST_API_RESOURCE_USER + URLEncoder.encode(user.getUserName(), "UTF-8") + RESTDefaults.REST_API_RESOURCE_USER_CONFIRM + confcode;
        } catch (UnsupportedEncodingException e) {
            URL = RESTDefaults.REST_API_RESOURCE_USER + user.getUserName()+ RESTDefaults.REST_API_RESOURCE_USER_CONFIRM + confcode;
        }


        String requestString = jsonWriter.createLoginRequest(user);


        HttpPostJsonWorker worker = new HttpPostJsonWorker(URL, requestString);
        communicationManager.handleAsyncMessage(worker, new UserAuthenicationParser(user,listener, application, false), statusListener);
    }

    @Override
    public void resendCode(User user, LoginListener listener, CommunicationStatusListener statusListener) {

        String URL = RESTDefaults.REST_API_RESOURCE_USER + user.getUserName() + RESTDefaults.REST_API_RESOURCE_USER_RESEND;
        String requestString = jsonWriter.createLoginRequest(user);


        HttpPostJsonWorker worker = new HttpPostJsonWorker(URL, requestString);
        communicationManager.handleAsyncMessage(worker, new UserAuthenicationParser(user,listener, application, false), statusListener);

    }

    @Override
    public void register(String email, String userName, String password, boolean agree, boolean newsletter, String smsaddr, String carrier, LoginListener listener, CommunicationStatusListener statusListener)
    {
        User user = new User();
        user.setCredentials(userName,password);
        user.setEmail(email);
        user.setSignUpDate(new Date());


        String URL = jsonWriter.postRegisterUser(userName);
        String requestString = jsonWriter.createRegisterRequest(email, userName, password, agree, newsletter, smsaddr);

        android.util.Log.v(LOGNAME,"Register :"+URL+" body:"+requestString);

        HttpPutJsonWorker worker = new HttpPutJsonWorker(URL, requestString);
        communicationManager.handleAsyncMessage(worker, new UserAuthenicationParser(user,listener, application, true), statusListener);
    }
    @Override
    public void getAttributes(long lastUpdateTime, AttributesListener listener, CommunicationStatusListener statusListener) {
        // This includes color, image and string resources. Color is not used at this time and not included in the parsing

        // We can send out multiple requests for image and string resources. This should make it easier
        HttpGetJsonWorker imageWorker = new HttpGetJsonWorker( jsonWriter.getImageAttributes(lastUpdateTime));
        AttributesParser imageParser = new AttributesParser(application,listener);
        communicationManager.handleAsyncMessage(imageWorker, imageParser, statusListener);


        HttpGetJsonWorker worker = new HttpGetJsonWorker( jsonWriter.getStringAttributes(lastUpdateTime));
        AttributesParser parser = new AttributesParser(application,listener);
        communicationManager.handleAsyncMessage(worker, parser, statusListener);

    }

    @Override
    public void getLegalAttributes(long lastUpdateTime, AttributesListener listener, CommunicationStatusListener statusListener) {
        HttpGetJsonWorker worker = new HttpGetJsonWorker( jsonWriter.getStringAttributes(lastUpdateTime));
        AttributesParser parser = new AttributesParser(application,listener);
        communicationManager.handleAsyncMessage(worker, parser, statusListener);
    }

    @Override
    public void getCategories(long lastUpdateTime, CategoryListener listener, CommunicationStatusListener statusListener) {
        HttpGetJsonWorker worker = new HttpGetJsonWorker( jsonWriter.getCategories(lastUpdateTime));
        TrapCategoryParser parser = new TrapCategoryParser(listener);
        communicationManager.handleAsyncMessage(worker, parser, statusListener);
    }

    @Override
    public void getTrapTypes(long lastUpdateTime, TrapTypeListener listener, CommunicationStatusListener statusListener) {

        HttpGetJsonWorker worker = new HttpGetJsonWorker( jsonWriter.getTrapTypes(lastUpdateTime));
        TrapTypeParser parser = new TrapTypeParser(listener);
        communicationManager.handleAsyncMessage(worker, parser, statusListener);

    }

    @Override
    public void sendProbeData(List<ProbeData> locations, CommunicationStatusListener statusListener)
    {
        String requestString = jsonWriter.createProbeDataRequest(locations);
        HttpPostJsonWorker worker = new HttpPostJsonWorker(RESTDefaults.REST_API_RESOURCE_PROBE, requestString);
        communicationManager.handleAsyncMessage(worker, null, statusListener);
    }

    @Override
    public void sendNPSResults(String vote, String feedback, String emailAddress, CommunicationStatusListener statusListener) {
    }

    @Override
    public void updateAllTraps(double lat, double lon, double radius, TrapListener listener, CommunicationStatusListener statusListener) {

        String url = jsonWriter.getTraps(lat, lon, radius);

        HttpGetJsonWorker worker = new HttpGetJsonWorker( url );

        android.util.Log.v(LOGNAME, "GET:" + url);

        TrapParser parser = new TrapParser(lat,lon,radius,listener);
        communicationManager.handleAsyncMessage(worker, parser, statusListener);
    }

    @Override
    public void rateTrap(boolean agree, Trap trap, BasicResponseListener confirmListener, CommunicationStatusListener statusListener) {
        String Url = jsonWriter.postVoteOnTrap(trap);
        String requestString = jsonWriter.createVoteTrapRequest(agree);

        HttpPostJsonWorker worker = new HttpPostJsonWorker(Url, requestString);
        communicationManager.handleAsyncMessage(worker, new BasicResponseParser(confirmListener), statusListener);
    }

    @Override
    public void addNewTrap(Trap trap, NewTrapListener listener, CommunicationStatusListener statusListener, double heading) {

        String URL = jsonWriter.postAddNewTrap();
        String requestString = jsonWriter.createAddNewTrapRequest(trap, (int) heading);

        HttpPostJsonWorker worker = new HttpPostJsonWorker(URL, requestString);
        communicationManager.handleAsyncMessage(worker, new NewTrapParser(trap,listener), statusListener);
    }

    @Override
    public void deleteTrap(Trap trap, BasicResponseListener listener, CommunicationStatusListener statusListener) {

        String URL = jsonWriter.deleteTrapRequest(trap);

        HttpDeleteJsonWorker worker = new HttpDeleteJsonWorker(URL);
        communicationManager.handleAsyncMessage(worker, new BasicResponseParser(listener), statusListener);

    }

    @Override
    public void ignoreTrap(Trap trap, CommunicationStatusListener statusListener) {
    }

    @Override
    public void listSounds(long lastUpdateTime, SoundThemeListener listener, CommunicationStatusListener statusListener) {

        HttpGetJsonWorker worker = new HttpGetJsonWorker( jsonWriter.getSoundThemes(lastUpdateTime));
        SoundThemesParser parser = new SoundThemesParser(listener);
        communicationManager.handleAsyncMessage(worker, parser, statusListener);
    }

    @Override
    public void sendIAmHere(List<GeoPosition> locations, CommunicationStatusListener statusListener) {
    }

    @Override
    public void getPath(double lat, double lon, double radius, PatrolListener listener, CommunicationStatusListener statusListener) {
        String requestString = jsonWriter.createPatrolURLString(lat, lon, radius);

        //Log.v(LOGNAME,"GET:"+requestString);

        HttpGetWorker worker = new HttpGetWorker(requestString);
        communicationManager.handleAsyncMessage(worker, new PatrolParser(listener,lat,lon,radius), statusListener);
    }

    @Override
    public void updateNewSpeedLimit(ArrayList<UpdatedRouteLinkInfo> updatedRouteLinkInfo, CommunicationStatusListener statusListener) {
    }

    @Override
    public void getNewSpeedLimit(UpdatedRouteLinkListener updatedRouteLinkListener, CommunicationStatusListener communicationStatusListener) {
    }

    @Override
    public void getUserStatistics(String username, StatisticsListener statisticsListener, CommunicationStatusListener statusListener)
    {
        try
        {
            String url = RESTDefaults.WEB_ENDPOINT_USERSTATS + "?name=" + URLEncoder.encode(username, "utf-8");
            HttpGetJsonWorker worker = new HttpGetJsonWorker(url);
            StatisticsParser parser = new StatisticsParser(statisticsListener);
            communicationManager.handleAsyncMessage(worker, parser, statusListener);
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public void getLaunchMessage(LaunchMessageListener listener, CommunicationStatusListener statusListener) {
    }

    @Override
    public void submitBecomeModeratorRequest()
    {
    }
}
