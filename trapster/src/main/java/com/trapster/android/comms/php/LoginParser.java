package com.trapster.android.comms.php;

import com.trapster.android.PHPDefaults;
import com.trapster.android.comms.LoginListener;
import com.trapster.android.comms.php.handler.ProfileHandler;
import com.trapster.android.comms.rest.StatusCode;
import com.trapster.android.manager.SessionManager;
import com.trapster.android.model.User;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.xml.parsers.SAXParserFactory;

public class LoginParser extends TrapsterParser {

	private static final String LOGNAME = "Trapster.LoginParser";
    private final User user;

    private LoginListener listener;

	SessionManager sessionManager;
	
	public LoginParser(User user, LoginListener listener, SessionManager sessionManager) {
		super(listener);
        this.user = user;
		this.listener = listener;
        this.sessionManager = sessionManager;
	}

	@Override
	public void parseXml(String xml) {

		// Get the status Tag only
		String message = getTextFromNode(xml, PHPDefaults.XML_RSP_STATUS);

		if (message.equalsIgnoreCase("OK-PARTIAL")) {
			if (listener != null)
				listener.onLoginFail(user, StatusCode.REQUIRES_CONFIRMATION_CODE);
		} else {

			String showMessage = getTextFromNode(xml,
					PHPDefaults.XML_RSP_SHOWMESSAGE);
			String msg = null;
			if (showMessage != null && showMessage.equalsIgnoreCase("Y")) {
				msg = getTextFromNode(xml, PHPDefaults.XML_RSP_MESSAGE);
			}

			if (listener != null)
				listener.onLoginSuccess(user,msg);
		}

		/*
		 * For Handling profile updates
		 * 
		 * This is a little messy going from inputstream->string->inputstream
		 * .We should look at moving the login parser to the Sax format for
		 * the new services version.
		 */
		String profile = getNodesFromXml(xml, "profile");
		if (profile != null) {
			try {

				InputStream in = new ByteArrayInputStream(xml.getBytes());
				ProfileHandler handler = new ProfileHandler(listener);
				SAXParserFactory.newInstance().newSAXParser()
						.parse(in, handler);
				
				if(handler.getProfile() != null)
					sessionManager.getUser().setProfile(handler.getProfile());
				
			} catch (Exception e) {
{/* 				//Log.e(LOGNAME, "Error parsing profile:"+e.getMessage()); */}
			}

		}

	}

}
