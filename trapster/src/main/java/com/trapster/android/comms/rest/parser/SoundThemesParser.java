package com.trapster.android.comms.rest.parser;

import com.trapster.android.comms.SoundThemeListener;
import com.trapster.android.model.SoundThemes;
import com.trapster.android.util.Log;
import com.twolinessoftware.android.framework.service.comms.JsonParser;

/**
 * Created by John on 5/28/13.
 */
public class SoundThemesParser extends JsonParser {

    private static final String LOGNAME = "SoundThemesParser";

    private final SoundThemeListener listener;

    public SoundThemesParser(SoundThemeListener listener)
    {
        super();
        this.listener= listener;
    }

    @Override
    public void parseJson(Status status, String jsonStream) {

        try{
            SoundThemes resources =  gson.fromJson(jsonStream, SoundThemes.class);

            if(listener != null)
                listener.onSoundThemeChanges(resources);

        }catch(Exception e){
            Log.e(LOGNAME,"SoundTheme parsing:"+e.getMessage());
        }

    }
}
