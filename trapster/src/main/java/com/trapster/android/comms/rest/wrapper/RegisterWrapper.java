package com.trapster.android.comms.rest.wrapper;

import com.google.gson.annotations.SerializedName;
import com.trapster.android.RESTDefaults;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 5/29/13
 * Time: 10:25 AM
 * To change this template use File | Settings | File Templates.
 */
public class RegisterWrapper {

    @SerializedName(RESTDefaults.REST_PARAMETER_USER_EMAIL)
    String email;

    @SerializedName (RESTDefaults.REST_PARAMETER_USER_NAME)
    String userName;

    @SerializedName (RESTDefaults.REST_PARAMETER_USER_PASSWORD)
    String password;

    @SerializedName (RESTDefaults.REST_PARAMETER_USER_PASSWORD_CONFIRMATION)
    String confPassword;

    @SerializedName (RESTDefaults.REST_PARAMETER_USER_SMS_ADDRESS)
    String smsaddr;

    @SerializedName (RESTDefaults.REST_PARAMETER_USER_TOS_AGREEMENT)
    String agree;

    @SerializedName (RESTDefaults.REST_PARAMETER_USER_NEWSLETTER_FLAG)
    String newsletter;

    public RegisterWrapper(String email, String userName,
                    String password, boolean agree, boolean newsletter, String smsaddr)
    {
        this.email = email;
        this.userName = userName;
        this.password = password;
        this.confPassword = password;
        this.smsaddr = smsaddr;
        if(agree)
            this.agree = "Y";
        else
            this.agree = "N";
        if(newsletter)
            this.newsletter = "Y";
        else
            this.newsletter = "N";
    }
}
