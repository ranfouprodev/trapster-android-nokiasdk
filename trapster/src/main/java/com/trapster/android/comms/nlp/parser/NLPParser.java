package com.trapster.android.comms.nlp.parser;


import com.twolinessoftware.android.framework.service.comms.Parser;

import java.io.InputStream;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 10/11/13
 * Time: 11:10 AM
 * To change this template use File | Settings | File Templates.
 */
public abstract class NLPParser extends Parser {


    public abstract void parseJson(String result);

    @Override
    public void process(InputStream stream) throws Exception {
        parseJson(convertStreamToString(stream));
    }


}
