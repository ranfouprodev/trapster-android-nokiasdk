package com.trapster.android.comms;

public interface DataLoadedListener
{
    void onDataLoad();
    void onDataLoadComplete();
    void onDataLoadError();
}
