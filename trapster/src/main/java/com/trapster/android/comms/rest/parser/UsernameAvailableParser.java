package com.trapster.android.comms.rest.parser;

import com.trapster.android.comms.UsernameAvailableListener;
import com.trapster.android.comms.rest.StatusCode;
import com.twolinessoftware.android.framework.service.comms.JsonParser;

/**
 * Created by John on 8/20/13.
 */
public class UsernameAvailableParser extends JsonParser {

    private final UsernameAvailableListener listener;
    private final String username;


    public UsernameAvailableParser(String username, UsernameAvailableListener listener) {
        super();
        this.listener = listener;
        this.username = username;
    }


    @Override
    public void parseJson(Status status, String jsonString) {

        switch (StatusCode.getByCode(status.statusCode)) {
            case OK:
                if(listener != null)
                    listener.onUsernameAvailable(username,false);
                break;

            // Any error assumes that the name isn't available. Should we be checking for only 404 response codes?
            default:
                if(listener != null)
                    listener.onUsernameAvailable(username,true);
        }


    }

    @Override
    public boolean handleHttpStatusErrors(Status status) {
        if(status.statusCode == 404){
            if(listener != null)
                listener.onUsernameAvailable(username,true);
            return true;
        }
        return false;
    }
}
