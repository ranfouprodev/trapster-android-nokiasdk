package com.trapster.android.comms.rest.wrapper;

import android.os.Build;
import com.google.gson.annotations.SerializedName;
import com.trapster.android.RESTDefaults;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 5/29/13
 * Time: 10:30 AM
 * To change this template use File | Settings | File Templates.
 */
public class MetaWrapper {

    @SerializedName(RESTDefaults.REST_PARAMETER_META_OPERATING_SYSTEM)
    String operatingSystemName;

    public MetaWrapper()
    {
        operatingSystemName = Build.VERSION.RELEASE;
    }

}
