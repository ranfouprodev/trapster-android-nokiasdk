package com.trapster.android.comms.rest.parser;

import com.trapster.android.comms.TrapTypeListener;
import com.trapster.android.model.TrapTypes;
import com.trapster.android.util.Log;
import com.twolinessoftware.android.framework.service.comms.JsonParser;

/**
 * Created by John on 5/28/13.
 */
public class TrapTypeParser extends JsonParser {

    private static final String LOGNAME = "TrapTypeParser";
    private final TrapTypeListener listener;

    public TrapTypeParser(TrapTypeListener listener)
    {
        super();
        this.listener= listener;
    }

    @Override
    public void parseJson(Status status, String jsonStream) {


        try{

            TrapTypes types =  gson.fromJson(jsonStream, TrapTypes.class);

            //Log.i("BBQ", "Parsed: " + types.getTypes().size() + " trap types");
            /*for(TrapType type:types.getTypes()){
                Log.e(LOGNAME,"Parsed "+gson.toJson(type.getLevels()));
            }
*/
        if(listener != null)
            listener.onTrapTypes(types);

        }catch(Exception e){
            Log.e(LOGNAME,"TrapType parsing:"+e.getMessage());
        }

    }



}
