package com.trapster.android.comms.php;

import com.trapster.android.PHPDefaults;
import com.trapster.android.comms.BasicResponseListener;

public class BasicResponseParser extends TrapsterParser {

	private static final String LOGNAME = "BasicResponseParser";

	private BasicResponseListener listener;

	public BasicResponseParser(BasicResponseListener listener) {
		super(listener);
		this.listener = listener;
	}

	@Override
	public void parseXml(String xml) {

		
		String data = getTextFromNode(xml, PHPDefaults.XML_RSP_STATUS);

		if (data.equalsIgnoreCase("OK") && listener != null)
			listener.onComplete();

	}

}
