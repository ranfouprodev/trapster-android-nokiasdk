package com.trapster.android.comms;


import com.trapster.android.model.LaunchMessage;

public interface LaunchMessageListener extends ErrorListener{
    void onLaunchMessage(LaunchMessage launchMessage);
}
