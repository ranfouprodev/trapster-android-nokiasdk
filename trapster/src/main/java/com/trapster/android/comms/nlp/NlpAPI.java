package com.trapster.android.comms.nlp;

import android.util.Log;
import com.google.inject.Inject;
import com.trapster.android.comms.CommunicationManager;
import com.trapster.android.comms.INLPAPI;
import com.trapster.android.comms.TrafficRouteLinkListener;
import com.trapster.android.comms.nlp.parser.TrafficRouteLinkParser;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;
import com.twolinessoftware.android.framework.service.comms.HttpGetWorker;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 10/24/13
 * Time: 3:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class NlpAPI implements INLPAPI {

    @Inject
    NLPJsonWriter nlpJsonWriter;
    @Inject
    CommunicationManager communicationManager;

    @Override
    public void getQuadkeyLinkInfo(String quadkey, TrafficRouteLinkListener linkListener, CommunicationStatusListener statusListener)
    {
        HttpGetWorker worker = new HttpGetWorker(nlpJsonWriter.createNLPQuadkeyRequest(quadkey));
        TrafficRouteLinkParser parser = new TrafficRouteLinkParser(linkListener, quadkey);
        communicationManager.handleAsyncMessage(worker, parser,statusListener);
    }

    @Override
    public void getWaypointLinkInfo(double lat, double lng, TrafficRouteLinkListener linkListener, CommunicationStatusListener statusListener)
    {
        HttpGetWorker worker = new HttpGetWorker(nlpJsonWriter.createNLPWaypointRequest(String.valueOf(lat), String.valueOf(lng)));
        TrafficRouteLinkParser parser = new TrafficRouteLinkParser(linkListener, null);
        communicationManager.handleAsyncMessage(worker, parser,statusListener);
    }

}