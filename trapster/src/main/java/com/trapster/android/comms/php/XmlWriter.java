package com.trapster.android.comms.php;

import android.os.Build;
import com.google.inject.Inject;
import com.here.android.common.GeoPosition;
import com.trapster.android.BuildFeatures;
import com.trapster.android.Defaults;
import com.trapster.android.PHPDefaults;
import com.trapster.android.manager.SessionManager;
import com.trapster.android.model.*;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.net.URLEncoder;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.List;

//import com.trapster.android.manager.SessionManager;
//import com.trapster.android.model.UpdatedRouteLinkInfo;


public class XmlWriter {

	private static final String LOGNAME = "XmlWriter";
	
	@Inject SessionManager sessionManager;
	
	public String probeData(List<ProbeData> locations) {

		StringBuffer sbPoint = new StringBuffer();
		for (ProbeData loc : locations) {
			StringBuffer sbTemp = new StringBuffer();
			sbTemp.append(enclose(PHPDefaults.XML_PARAMS_LAT,
					String.valueOf(loc.getLatitude())));
			sbTemp.append(enclose(PHPDefaults.XML_PARAMS_LON,
					String.valueOf(loc.getLongitude())));
			sbTemp.append(enclose(PHPDefaults.XML_PARAMS_TIME,
					String.valueOf(loc.getTimestamp() / 1000)));
			sbTemp.append(enclose(PHPDefaults.XML_PARAMS_HEADING,
					String.valueOf((int) loc.getBearing())));
			sbTemp.append(enclose(PHPDefaults.XML_PARAMS_SPEED,
                    String.valueOf(Math.round(loc.getSpeed()))));
            sbTemp.append(enclose(PHPDefaults.XML_PARAMS_GPS_ACCURACY_HORIZONTAL,
                    String.valueOf(Math.round(loc.getHorizontalAccuracy()))));
            sbTemp.append(enclose(PHPDefaults.XML_PARAMS_GPS_ACCURACY_VERTICAL,
                    String.valueOf(Math.round(loc.getVerticalAccuracy()))));
            sbTemp.append(enclose(PHPDefaults.XML_PARAMS_ALTITUDE,
                    String.valueOf(Math.round(loc.getAltitude()))));
            //sbTemp.append(enclose(PHPDefaults.XML_PARAMS_SATELLITE_COUNT,
                    //String.valueOf(Math.round(-1))));  Not currently available in GeoPosition or Location
			sbPoint.append(encloseWithoutEncode(PHPDefaults.XML_PARAMS_POINT,
					sbTemp.toString()));
		}

		StringBuffer sb = new StringBuffer();
		sb.append(encloseWithoutEncode(PHPDefaults.XML_PARAMS_POINTS,
				sbPoint.toString()));
		
		
		User user = sessionManager.getUser();
		if (user == null)
			user = sessionManager.getDemoUser();

		String authMsg = authMessage(null, user);
		String paramsMsg = paramsMessage(sb.toString());

		return trapsterMessage(authMsg + paramsMsg);
	}

    public String iAmHereUpdate(List<GeoPosition> locations) {

        StringBuffer sbPoint = new StringBuffer();
        for (GeoPosition loc : locations) {
            StringBuffer sbTemp = new StringBuffer();
            sbTemp.append(enclose(PHPDefaults.XML_PARAMS_LAT,
                    String.valueOf(loc.getCoordinate().getLatitude())));
            sbTemp.append(enclose(PHPDefaults.XML_PARAMS_LON,
                    String.valueOf(loc.getCoordinate().getLongitude())));
            sbTemp.append(enclose(PHPDefaults.XML_PARAMS_TIME,
                    String.valueOf(System.currentTimeMillis() / 1000)));
            sbTemp.append(enclose(PHPDefaults.XML_PARAMS_HEADING,
                    String.valueOf((int) loc.getHeading())));
            sbTemp.append(enclose(PHPDefaults.XML_PARAMS_SPEED,
                    String.valueOf(Math.round(loc.getSpeed()))));
            sbPoint.append(encloseWithoutEncode(PHPDefaults.XML_PARAMS_POINT,
                    sbTemp.toString()));
        }

        StringBuffer sb = new StringBuffer();
        sb.append(encloseWithoutEncode(PHPDefaults.XML_PARAMS_POINTS,
                sbPoint.toString()));

        User user = sessionManager.getUser();
        if (user == null)
            user = sessionManager.getDemoUser();

        String authMsg = authMessage(null, user);
        String paramsMsg = paramsMessage(sb.toString());

        return trapsterMessage(authMsg + paramsMsg);
    }

	
	public String listSoundThemes(long lastUpdateTime) {

		StringBuffer sb = new StringBuffer();

		sb.append(enclose(PHPDefaults.XML_PARAMS_IF_CHANGED_AFTER,
				String.valueOf(lastUpdateTime)));

		User user = sessionManager.getDemoUser();

		String authMsg = authMessage(null, user);
		String paramsMsg = paramsMessage(sb.toString());
		
		return trapsterMessage(authMsg+paramsMsg);
	}

	public String reportTrapMessage(Trap t, double heading) {

		
		
		StringBuffer sb = new StringBuffer();
		sb.append(enclose(PHPDefaults.XML_PARAMS_LAT, String.valueOf(t.getGeometry().getCoordinate().y)));
		sb.append(enclose(PHPDefaults.XML_PARAMS_LON, String.valueOf(t.getGeometry().getCoordinate().x)));
		sb.append(enclose(PHPDefaults.XML_PARAMS_OBJ,
				String.valueOf(t.getTypeid())));
        sb.append(enclose(PHPDefaults.XML_PARAMS_HEADING,
                String.valueOf(heading)));
		if (t.getAddress() != null && !t.getAddress().equals(""))
			sb.append(enclose(PHPDefaults.XML_PARAMS_ADDR,
					String.valueOf(t.getAddress())));

		User user = sessionManager.getUser();

		String authMsg = authMessage(null, user);
		String paramsMsg = paramsMessage(sb.toString());

		return trapsterMessage(authMsg + paramsMsg);
	}

    private String encodeKeyValue (String key, String value )
    {
        String encoded = "";
        try
        {
            encoded = key + "=" + URLEncoder.encode(value, "utf-8");
        }
        catch (Exception e){}

        return encoded;
    }

    public String reportNPSResults(String vote, String feedback, String emailAddress)
    {
        StringBuffer sb = new StringBuffer();

        sb.append(encodeKeyValue(PHPDefaults.XML_PARAMS_NPS_SOURCEID, Defaults.NPS_SOURCE_ID) + "&");
        sb.append(encodeKeyValue(PHPDefaults.XML_PARAMS_NPS_PROJID, Defaults.NPS_PROJECT_ID) + "&");
        sb.append(encodeKeyValue(PHPDefaults.XML_PARAMS_NPS_SCORE, vote) + "&");
        sb.append(encodeKeyValue(PHPDefaults.XML_PARAMS_NPS_FEEDBACK, feedback) + "&");
        sb.append(encodeKeyValue(PHPDefaults.XML_PARAMS_NPS_EMAIL, emailAddress) + "&");
        sb.append(encodeKeyValue(PHPDefaults.XML_PARAMS_NPS_RELEASE_VERSION, BuildFeatures.VERSIONNAME) + "&");
        sb.append(encodeKeyValue(PHPDefaults.XML_PARAMS_NPS_DEVICE,  Build.MODEL) + "&");
        sb.append(encodeKeyValue(PHPDefaults.XML_PARAMS_NPS_VERSION,  Defaults.NPS_VERSION));

        return sb.toString();
    }

    public String getSpeedLimitMessage() {

        User user = sessionManager.getUser();
        String authMsg = authMessage(null, user);
        return trapsterMessage(authMsg);
    }

    public String launchMessage()
    {
        User user = sessionManager.getUser();
        String authMsg = authMessage(null, user);
        return trapsterMessage(authMsg);
    }
	
	public String reportSpeedLimitMessage(ArrayList<UpdatedRouteLinkInfo> updatedRouteLinkInfo) {

		StringBuffer sb = new StringBuffer();
		StringBuffer routeLinkIds = new StringBuffer();
		StringBuffer lats = new StringBuffer();
		StringBuffer lngs = new StringBuffer();
		for (int i = 0; i < updatedRouteLinkInfo.size(); i++)
		{
			routeLinkIds.append(updatedRouteLinkInfo.get(i).getRouteLinkId());
			lats.append(String.valueOf(updatedRouteLinkInfo.get(i).getLat()));
			lngs.append(String.valueOf(updatedRouteLinkInfo.get(i).getLng()));
			
			if (i != updatedRouteLinkInfo.size()-1)
			{
				routeLinkIds.append(",");		
				lats.append(",");
				lngs.append(",");
			}
		}
		sb.append(enclose(PHPDefaults.XML_PARAMS_ROUTELINKID, String.valueOf(routeLinkIds)));
		sb.append(enclose(PHPDefaults.XML_PARAMS_SPEEDLIMIT, String.valueOf(updatedRouteLinkInfo.get(0).getSpeedLimit())));
		sb.append(enclose(PHPDefaults.XML_PARAMS_LAT, String.valueOf(lats)));
		sb.append(enclose(PHPDefaults.XML_PARAMS_LON, String.valueOf(lngs)));

        User user = sessionManager.getUser();

		String authMsg = authMessage(null, user);
		String paramsMsg = paramsMessage(sb.toString());

		return trapsterMessage(authMsg + paramsMsg);
	}

	public String rateTrapMessage(Trap t, String rating) {

		StringBuffer sb = new StringBuffer();
		sb.append(enclose(PHPDefaults.XML_PARAMS_TRAP_ID,
				String.valueOf(t.getId())));
		sb.append(enclose(PHPDefaults.XML_PARAMS_VOTE, rating));

		User user = sessionManager.getUser();

		String authMsg = authMessage(null, user);
		String paramsMsg = paramsMessage(sb.toString());

		return trapsterMessage(authMsg + paramsMsg);
	}

	public String deleteTrapMessage(Trap t) {

		StringBuffer sb = new StringBuffer();
		sb.append(enclose(PHPDefaults.XML_PARAMS_TRAP_ID,
				String.valueOf(t.getId())));

		User user = sessionManager.getUser();

		String authMsg = authMessage(null, user);
		String paramsMsg = paramsMessage(sb.toString());

		return trapsterMessage(authMsg + paramsMsg);
	}
    public String ignoreTrapMessage(Trap t) {

        StringBuffer sb = new StringBuffer();
        sb.append(enclose(PHPDefaults.XML_PARAMS_TRAP_ID,
                String.valueOf(t.getId())));

        User user = sessionManager.getUser();

        String authMsg = authMessage(null, user);
        String paramsMsg = paramsMessage(sb.toString());

        return trapsterMessage(authMsg + paramsMsg);
    }

	public String requestAllTrapsInArea(double lat, double lon,
			double radius) {

		StringBuffer sb = new StringBuffer();
		sb.append(enclose(PHPDefaults.XML_PARAMS_LAT, String.valueOf(lat)));
		sb.append(enclose(PHPDefaults.XML_PARAMS_LON, String.valueOf(lon)));

		sb.append(enclose(PHPDefaults.XML_PARAMS_RADIUS, String.valueOf(radius)));
		

		User user = sessionManager.getUser();
		if (user == null)
			user = sessionManager.getDemoUser();

		String authMsg = authMessage(null, user);
		String paramsMsg = paramsMessage(sb.toString());

		return trapsterMessage(authMsg + paramsMsg);
	}

	public String requestChangedTrapsInArea(long lastUpdateTime,
			double lat, double lon, double radius) {

		StringBuffer sb = new StringBuffer();
		sb.append(enclose(PHPDefaults.XML_PARAMS_LAT, String.valueOf(lat)));
		sb.append(enclose(PHPDefaults.XML_PARAMS_LON, String.valueOf(lon)));
		sb.append(enclose(PHPDefaults.XML_PARAMS_IF_CHANGED_AFTER,
				String.valueOf(lastUpdateTime)));
		sb.append(enclose(PHPDefaults.XML_PARAMS_RADIUS, String.valueOf(radius)));

		User user = sessionManager.getUser();
		if (user == null)
			user = sessionManager.getDemoUser();

		String authMsg = authMessage(null, user);
		String paramsMsg = paramsMessage(sb.toString());

		return trapsterMessage(authMsg + paramsMsg);
	}

	public String getTrapTypes(long lastUpdateTime) {
		StringBuffer sb = new StringBuffer();
		sb.append(enclose(PHPDefaults.XML_PARAMS_IF_CHANGED_AFTER,
				String.valueOf(lastUpdateTime)));
		sb.append(enclose(PHPDefaults.XML_PARAMS_CMD, "traps"));

		User user = sessionManager.getDemoUser();

		String authMsg = authMessage(null, user);
		String paramsMsg = paramsMessage(sb.toString());

		return trapsterMessage(authMsg + paramsMsg);
	}

	public String getCategories(long lastUpdateTime) {
		StringBuffer sb = new StringBuffer();
		sb.append(enclose(PHPDefaults.XML_PARAMS_IF_CHANGED_AFTER,
				String.valueOf(lastUpdateTime)));
		sb.append(enclose(PHPDefaults.XML_PARAMS_CMD, "categories-sequence"));

		User user = sessionManager.getDemoUser();

		String authMsg = authMessage(null, user);
		String paramsMsg = paramsMessage(sb.toString());

		return trapsterMessage(authMsg + paramsMsg);
	}

	public String getLegalAttributes(long lastUpdateTime) {
		StringBuffer sb = new StringBuffer();

		sb.append(enclose(PHPDefaults.XML_PARAMS_IF_CHANGED_AFTER,
				String.valueOf(lastUpdateTime)));
		
		sb.append(enclose(PHPDefaults.XML_PARAMS_HAS_KEY, Defaults.ASSET_TERMSANDCONDITIONS));

		User user = sessionManager.getDemoUser();

		String authMsg = authMessage(null, user);
		String paramsMsg = paramsMessage(sb.toString());

		return trapsterMessage(authMsg + paramsMsg);
	}

	public String getAttributes(long lastUpdateTime) {
		StringBuffer sb = new StringBuffer();

		sb.append(enclose(PHPDefaults.XML_PARAMS_IF_CHANGED_AFTER,
				String.valueOf(lastUpdateTime)));

		User user = sessionManager.getDemoUser();

		String authMsg = authMessage(null, user);
		String paramsMsg = paramsMessage(sb.toString());

		return trapsterMessage(authMsg + paramsMsg);
	}

	public String getPatrolPoints(double lat, double lon, double radius) {
		StringBuffer sb = new StringBuffer();

		sb.append(enclose(PHPDefaults.XML_PARAMS_LAT, String.valueOf(lat)));
		sb.append(enclose(PHPDefaults.XML_PARAMS_LON, String.valueOf(lon)));
		sb.append(enclose(PHPDefaults.XML_PARAMS_RADIUS, String.valueOf(radius)));

		User user = sessionManager.getDemoUser();

		String authMsg = authMessage(null, user);
		String paramsMsg = paramsMessage(sb.toString());

		return trapsterMessage(authMsg + paramsMsg);
	}

	public String registerMessage(String email, String userName,
			String password, boolean agree, boolean newsletter, String smsaddr, String carrier) {

		StringBuffer sb = new StringBuffer();
		sb.append(enclose(PHPDefaults.XML_PARAMS_EMAIL, email));
		sb.append(enclose(PHPDefaults.XML_PARAMS_UNAME, userName));
		sb.append(enclose(PHPDefaults.XML_PARAMS_PWD, password));
		sb.append(enclose(PHPDefaults.XML_PARAMS_CPWD, password));
		if (agree)
			sb.append(enclose(PHPDefaults.XML_PARAMS_AGREE, "Y"));
		else
			sb.append(enclose(PHPDefaults.XML_PARAMS_AGREE, "N"));
        if (newsletter)
            sb.append(enclose(PHPDefaults.XML_PARAMS_STAY_IN_TOUCH, "Y"));
        else
            sb.append(enclose(PHPDefaults.XML_PARAMS_STAY_IN_TOUCH, "N"));

		sb.append(enclose(PHPDefaults.XML_PARAMS_SMS, smsaddr));
		sb.append(enclose(PHPDefaults.XML_PARAMS_CARRIER, carrier));

		User u = new User();
		u.setCredentials(userName,password);

		String authMsg = authMessage(null, u);
		String paramsMsg = paramsMessage(sb.toString());

		return trapsterMessage(authMsg + paramsMsg);

	}

	public String loginMessage(User user, String code) {
		code = (code == null) ? "" : code;
		String authMsg = authMessage(code, user);
		StringBuffer sb = new StringBuffer();
		sb.append(enclose("profile", "Y"));
		String paramsMsg = paramsMessage(sb.toString());
		
		return trapsterMessage(authMsg + paramsMsg);
	}

	public String authOnly(User user) {
		String authMsg = authMessage(null, user);
		return trapsterMessage(authMsg);
	}

	public String loginMessage(User user) {
		String authMsg = authMessage(null, user);
		
		StringBuffer sb = new StringBuffer();
		sb.append(enclose("profile", "Y"));
		String paramsMsg = paramsMessage(sb.toString());
		
		return trapsterMessage(authMsg + paramsMsg);
	}

	private String trapsterMessage(String message) {
		// Adding os meta
		String os = enclose(PHPDefaults.XML_META_OS, Build.VERSION.SDK);
		String meta = encloseWithoutEncode(PHPDefaults.XML_META, os);

		return encloseWithoutEncode(PHPDefaults.XML_TRAPSTER, meta + message);
	}

	private String authMessage(String code, User user) {

		if (user == null || user.getUserName() == null
				|| user.getPassword() == null) {
			user = new User();
            user.setCredentials(Defaults.DEMO_USERNAME,Defaults.DEMO_PASSWORD);
		}

		StringBuffer sb = new StringBuffer();

		sb.append(enclose(PHPDefaults.XML_AUTH_KEY,BuildFeatures.TRAPSTERKEY));
		sb.append(enclose(PHPDefaults.XML_AUTH_UNAME, user.getUserName()));

		sb.append(enclose(PHPDefaults.XML_AUTH_HASH, toHash(user)));

		sb.append(enclose(PHPDefaults.XML_AUTH_DEVICETYPE, BuildFeatures.getDeviceType()));
		sb.append(enclose(PHPDefaults.XML_AUTH_DEVICEID, BuildFeatures.getDeviceId()));

		if (code != null)
			sb.append(enclose(PHPDefaults.XML_AUTH_CODE, code));

		sb.append(enclose(PHPDefaults.XML_AUTH_CAP, PHPDefaults.PHP_APP_CAPABILITIES));
		sb.append(enclose(PHPDefaults.XML_AUTH_APPID, PHPDefaults.PHP_APP_APPID + BuildFeatures.VERSIONNAME));

		return encloseWithoutEncode(PHPDefaults.XML_AUTH, sb.toString());
	}

	private String paramsMessage(String message) {
		return encloseWithoutEncode(PHPDefaults.XML_PARAMS, message);
	}

	private String encloseWithoutEncode(String tag, String value) {
		StringBuffer sb = new StringBuffer();

		sb.append("<");
		sb.append(tag);
		sb.append(">");
		sb.append(value);
		sb.append("</");
		sb.append(tag);
		sb.append(">");

		return sb.toString();
	}

	private String enclose(String tag, String value) {
		StringBuffer sb = new StringBuffer();

		sb.append("<");
		sb.append(tag);
		sb.append(">");
		sb.append(forXML(value));
		sb.append("</");
		sb.append(tag);
		sb.append(">");

		return sb.toString();
	}

	public String toHash(User user) {
		/*String hashalg = user.getUserName().toLowerCase() + "TRAPSTER"
				+ user.getPassword();
		try {
			return SecureHash.secureHash("MD5", hashalg);
		} catch (Exception e) {
		}
		return null;*/
        return user.getPassword();
	}

	public static String forXML(String aText) {

		if (aText == null)
			return aText;

		final StringBuilder result = new StringBuilder();
		final StringCharacterIterator iterator = new StringCharacterIterator(
				aText);
		char character = iterator.current();
		while (character != CharacterIterator.DONE) {
			if (character == '<') {
				result.append("&lt;");
			} else if (character == '>') {
				result.append("&gt;");
			} else if (character == '\"') {
				result.append("&quot;");
			} else if (character == '\'') {
				result.append("&#039;");
			} else if (character == '&') {
				result.append("&amp;");
			} else {
				// the char is not a special one
				// add it to the result as is
				result.append(character);
			}
			character = iterator.next();
		}
		return result.toString();

	}

    public ArrayList<NameValuePair> createModeratorPOSTBodyl()
    {
        ArrayList<NameValuePair> values = new ArrayList<NameValuePair>();


        User user = sessionManager.getUser();
        Profile profile = sessionManager.getUser().getProfile();
        if (profile != null && user != null)
        {
            values.add(new BasicNameValuePair(PHPDefaults.XML_PARAMS_EMAIL, profile.getEmail()));
            values.add(new BasicNameValuePair(PHPDefaults.XML_PARAMS_UNAME, user.getUserName()));
        }

        return values;
    }



}
