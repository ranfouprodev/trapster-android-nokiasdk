package com.trapster.android.comms.rest.parser;


import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.trapster.android.comms.BasicResponseListener;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.service.comms.Parser;

import java.io.InputStream;
import java.lang.reflect.Modifier;
import java.net.HttpURLConnection;

/**
 * Created by John on 5/28/13.
 *
 */
public class ForgotPasswordParser extends Parser {

    private static final String LOGNAME = "ForgotPasswordParser";
    private final BasicResponseListener listener;

    private final GsonBuilder builder;

    protected Gson gson = null;

    public ForgotPasswordParser(BasicResponseListener listener)
    {
        super();
        this.listener= listener;

        builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.TRANSIENT);
        //See notes below. Once 2.3 is released this needs to be enabled
        // builder.registerTypeAdapter(boolean.class, new BooleanSerializer());
        gson = builder.create();

    }

    @Override
    public void process(InputStream stream) throws Exception {

        if(getResponseCode() == HttpURLConnection.HTTP_OK )
        {
            try
            {
                String jsonString = convertStreamToString(stream);



                WebStatus status = gson.fromJson(jsonString, WebStatus.class);

                Log.v(LOGNAME, "Parsing:" + jsonString +" Status:"+status.status);

                if(status.status.equals("OK") && listener != null)
                    listener.onComplete();

            } catch (Exception e) {
                if(listener != null)
                    listener.onError(new TrapsterError(TrapsterError.TYPE_API_ERROR,""));

            }
        }
        else
        {

           if(listener != null)
               listener.onError(new TrapsterError(TrapsterError.TYPE_API_ERROR,"Response Code:"+getResponseCode()));
        }
    }

    private static class WebStatus{
        String status;
        long timestamp;
    }

}
