package com.trapster.android.comms;

import com.trapster.android.model.Trap;


public interface NewTrapListener extends ErrorListener{
	void onNewTrapAdded(Trap trap, String optionalMessage );
}
