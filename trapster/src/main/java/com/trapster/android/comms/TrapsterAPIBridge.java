package com.trapster.android.comms;

import com.google.inject.Inject;
import com.here.android.common.GeoPosition;
import com.trapster.android.comms.php.PhpTrapsterAPI;
import com.trapster.android.comms.rest.RESTTrapsterAPI;
import com.trapster.android.model.ProbeData;
import com.trapster.android.model.Trap;
import com.trapster.android.model.UpdatedRouteLinkInfo;
import com.trapster.android.model.User;
import com.trapster.android.util.Log;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;

import java.util.ArrayList;
import java.util.List;

// This class functions as a bridge as we move between REST and php APIs.  Once both are fully implemented (and used exclusively of each other), this class won't be needed
public class TrapsterAPIBridge implements ITrapsterAPI
{
    @Inject PhpTrapsterAPI phpAPI;
    @Inject RESTTrapsterAPI restAPI;

    // Because I love enums
    private enum API_CALL
    {
        LOGIN(true),
        LOGIN_CONFIRMATION(true),
        RESEND_CODE(true),
        REGISTER(true),
        GET_ATTRIBUTES(true),
        GET_LEGAL_ATTRIBUTES(true),
        GET_CATEGORIES(true),
        GET_TRAP_TYPES(true),
        SEND_PROBE_DATA(true),
        SEND_NPS_RESULTS(false),
        UPDATE_ALL_TRAPS(true),
        RATE_TRAP(true),
        ADD_NEW_TRAP(true),
        DELETE_TRAP(true),
        IGNORE_TRAP(false),
        LIST_SOUNDS(true),
        SEND_I_AM_HERE(false),
        GET_PATH(true),
        UPDATE_NEW_SPEED_LIMIT(false),
        GET_NEW_SPEED_LIMIT(false),
        GET_USER_STATISTICS(true),
        GET_LAUNCH_MESSAGE(false),
        BECOME_A_MOD(false);

        public boolean isRest;
        API_CALL(boolean isRest)
        {
            this.isRest = isRest;
        }
    }

    @Override
    public void login(User user, LoginListener listener, CommunicationStatusListener statusListener)
    {
        
        ITrapsterAPI api;
        if (API_CALL.LOGIN.isRest)
            api = restAPI;
        else
            api = phpAPI;

        api.login(user, listener, statusListener);
    }

    @Override
    public void login(User user, String confcode, LoginListener listener, CommunicationStatusListener statusListener)
    {
        
        ITrapsterAPI api;
        if (API_CALL.LOGIN_CONFIRMATION.isRest)
            api = restAPI;
        else
            api = phpAPI;

        api.login(user, confcode, listener, statusListener);
    }

    @Override
    public void resendCode(User user, LoginListener listener, CommunicationStatusListener statusListener)
    {
        
        ITrapsterAPI api;
        if (API_CALL.RESEND_CODE.isRest)
            api = restAPI;
        else
            api = phpAPI;

        api.resendCode(user, listener, statusListener);
    }

    @Override
    public void register(String email, String userName, String password, boolean agree, boolean newsletter, String smsaddr, String carrier, LoginListener listener, CommunicationStatusListener statusListener)
    {
        
        ITrapsterAPI api;
        if (API_CALL.REGISTER.isRest)
            api = restAPI;
        else
            api = phpAPI;

        api.register(email, userName, password, agree, newsletter, smsaddr, carrier, listener, statusListener);
    }

    @Override
    public void getAttributes(long lastUpdateTime, AttributesListener listener, CommunicationStatusListener statusListener)
    {
        
        ITrapsterAPI api;
        if (API_CALL.GET_ATTRIBUTES.isRest)
            api = restAPI;
        else
            api = phpAPI;

        api.getAttributes(lastUpdateTime, listener, statusListener);           
    }

    @Override
    public void getLegalAttributes(long lastUpdateTime, AttributesListener listener, CommunicationStatusListener statusListener)
    {
        
        ITrapsterAPI api;
        if (API_CALL.GET_LEGAL_ATTRIBUTES.isRest)
            api = restAPI;
        else
            api = phpAPI;

        api.getLegalAttributes(lastUpdateTime, listener, statusListener);
    }

    @Override
    public void getCategories(long lastUpdateTime, CategoryListener listener, CommunicationStatusListener statusListener)
    {
        
        ITrapsterAPI api;
        if (API_CALL.GET_CATEGORIES.isRest)
            api = restAPI;
        else
            api = phpAPI;

        api.getCategories(lastUpdateTime, listener, statusListener);
    }

    @Override
    public void getTrapTypes(long lastUpdateTime, TrapTypeListener listener, CommunicationStatusListener statusListener)
    {
        
        ITrapsterAPI api;
        if (API_CALL.GET_TRAP_TYPES.isRest)
            api = restAPI;
        else
            api = phpAPI;

        api.getTrapTypes(lastUpdateTime, listener, statusListener);
    }

    @Override
    public void sendProbeData(List<ProbeData> locations, CommunicationStatusListener statusListener)
    {
        
        ITrapsterAPI api;
        if (API_CALL.SEND_PROBE_DATA.isRest)
            api = restAPI;
        else
            api = phpAPI;

        api.sendProbeData(locations, statusListener);
    }

    @Override
    public void sendNPSResults(String vote, String feedback, String emailAddress, CommunicationStatusListener statusListener)
    {
        
        ITrapsterAPI api;
        if (API_CALL.SEND_NPS_RESULTS.isRest)
            api = restAPI;
        else
            api = phpAPI;

        api.sendNPSResults(vote, feedback,  emailAddress,  statusListener);
    }
    @Override
    public void updateAllTraps(double lat, double lon, double radius, TrapListener listener, CommunicationStatusListener statusListener)
    {
        
        ITrapsterAPI api;
        if (API_CALL.UPDATE_ALL_TRAPS.isRest)
            api = restAPI;
        else
            api = phpAPI;

        api.updateAllTraps(lat,  lon,  radius, listener, statusListener);
    }

    @Override
    public void rateTrap(boolean agree, Trap trap, BasicResponseListener successListener, CommunicationStatusListener statusListener)
    {
        
        ITrapsterAPI api;
        if (API_CALL.RATE_TRAP.isRest)
            api = restAPI;
        else
            api = phpAPI;

        api.rateTrap(agree, trap,successListener, statusListener);
    }

    @Override
    public void addNewTrap(Trap trap, NewTrapListener listener, CommunicationStatusListener statusListener, double heading)
    {
        
        ITrapsterAPI api;
        if (API_CALL.ADD_NEW_TRAP.isRest)
            api = restAPI;
        else
            api = phpAPI;

        api.addNewTrap(trap, listener, statusListener, heading);
    }

    @Override
    public void deleteTrap(Trap trap, BasicResponseListener listener, CommunicationStatusListener statusListener)
    {
        
        ITrapsterAPI api;
        if (API_CALL.DELETE_TRAP.isRest)
            api = restAPI;
        else
            api = phpAPI;

        api.deleteTrap(trap, listener, statusListener);
    }

    @Override
    public void ignoreTrap(Trap trap, CommunicationStatusListener statusListener)
    {
        
        ITrapsterAPI api;
        if (API_CALL.IGNORE_TRAP.isRest)
            api = restAPI;
        else
            api = phpAPI;

        api.ignoreTrap(trap,statusListener);
    }

    @Override
    public void listSounds(long lastUpdateTime, SoundThemeListener listener, CommunicationStatusListener statusListener)
    {
        
        ITrapsterAPI api;
        if (API_CALL.LIST_SOUNDS.isRest)
            api = restAPI;
        else
            api = phpAPI;

        api.listSounds(lastUpdateTime,listener,statusListener);
    }

    @Override
    public void sendIAmHere(List<GeoPosition> locations, CommunicationStatusListener statusListener)
    {
        
        ITrapsterAPI api;
        if (API_CALL.SEND_I_AM_HERE.isRest)
            api = restAPI;
        else
            api = phpAPI;

        api.sendIAmHere(locations, statusListener);
    }

    @Override
    public void getPath(double lat, double lon, double radius, PatrolListener listener, CommunicationStatusListener statusListener)
    {
        
        ITrapsterAPI api;
        if (API_CALL.GET_PATH.isRest)
            api = restAPI;
        else
            api = phpAPI;

        api.getPath(lat, lon, radius, listener, statusListener);
    }

    @Override
    public void updateNewSpeedLimit(ArrayList<UpdatedRouteLinkInfo> updatedRouteLinkInfo, CommunicationStatusListener statusListener)
    {
        
        ITrapsterAPI api;
        if (API_CALL.UPDATE_NEW_SPEED_LIMIT.isRest)
            api = restAPI;
        else
            api = phpAPI;

        api.updateNewSpeedLimit(updatedRouteLinkInfo, statusListener);
    }

    @Override
    public void getNewSpeedLimit(UpdatedRouteLinkListener updatedRouteLinkListener, CommunicationStatusListener communicationStatusListener)
    {
        
        ITrapsterAPI api;
        if (API_CALL.GET_NEW_SPEED_LIMIT.isRest)
            api = restAPI;
        else
            api = phpAPI;

        api.getNewSpeedLimit(updatedRouteLinkListener, communicationStatusListener);
    }

    @Override
    public void getUserStatistics(String username, StatisticsListener statisticsListener, CommunicationStatusListener statusListener)
    {
        
        ITrapsterAPI api;
        if (API_CALL.GET_USER_STATISTICS.isRest)
            api = restAPI;
        else
            api = phpAPI;

        api.getUserStatistics(username, statisticsListener, statusListener);
    }

    @Override
    public void getLaunchMessage(LaunchMessageListener listener, CommunicationStatusListener statusListener)
    {
        
        ITrapsterAPI api;
        if (API_CALL.GET_LAUNCH_MESSAGE.isRest)
            api = restAPI;
        else
            api = phpAPI;

        api.getLaunchMessage(listener, statusListener);
    }

    @Override
    public void submitBecomeModeratorRequest()
    {
        
        ITrapsterAPI api;
        if (API_CALL.BECOME_A_MOD.isRest)
            api = restAPI;
        else
            api = phpAPI;

        api.submitBecomeModeratorRequest();
    }

    private void outputAPITracking()
    {
        //3 ==
        // getStackTrace()
        // currentThread()
        // apiFunction() that called this
        Log.i("BBQ", "API call to " + Thread.currentThread().getStackTrace()[3].getMethodName());
    }
}
