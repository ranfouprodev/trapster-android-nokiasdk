package com.trapster.android.comms.php.handler;

import com.trapster.android.PHPDefaults;
import com.trapster.android.comms.SoundThemeListener;
import com.trapster.android.model.Formats;
import com.trapster.android.model.SoundLink;
import com.trapster.android.model.SoundTheme;
import com.trapster.android.model.SoundThemes;
import org.xml.sax.Attributes;

public class SoundThemesHandler extends TrapsterHandler
{
    SoundThemes soundThemes= new SoundThemes();
    SoundTheme soundTheme;
    SoundLink soundLink;
    Formats formats;
    //
    public SoundThemesHandler(SoundThemeListener listener)
    {
        super(listener);
    }
    public SoundThemes getSoundThemes()
    {
        return soundThemes;
    }
    protected void onEndElement(String uri, String localName, String qName)
    {
        if (PHPDefaults.XML_RSP_THEME_ADDED.equals(localName))
        {
            soundThemes.getAdded().add(soundTheme);
            soundTheme= null;
        } else if (PHPDefaults.XML_RSP_THEME_REMOVED.equals(localName))
        {
            soundThemes.getRemoved().add(Integer.valueOf(buffer.toString()));
            buffer= null;
        } else if (PHPDefaults.XML_RSP_THEME_ID.equals(localName))
        {
            soundTheme.setId(Integer.valueOf(buffer.toString()));
            buffer= null;
        } else if (PHPDefaults.XML_RSP_THEME_NAME.equals(localName))
        {
            soundTheme.setName(buffer.toString());
            buffer= null;
        } else if (PHPDefaults.XML_RSP_THEME_DESC.equals(localName))
        {
            soundTheme.setDescription(buffer.toString());
            buffer= null;
        } else if (PHPDefaults.XML_RSP_THEME_AUTHOR.equals(localName))
        {
            soundTheme.setAuthor(buffer.toString());
            buffer= null;
        } else if (PHPDefaults.XML_RSP_THEME_VERSION.equals(localName))
        {
            soundTheme.setVersion(buffer.toString());
            buffer= null;
        } else if (PHPDefaults.XML_RSP_THEME_LINK.equals(localName))
        {
            soundTheme.getLinks().add(soundLink);
            soundLink= null;
        } else if (PHPDefaults.XML_RSP_THEME_LID.equals(localName))
        {
            soundLink.setLid(buffer.toString());
            buffer= null;
        } else if (PHPDefaults.XML_RSP_THEME_PATH.equals(localName))
        {
            soundLink.setPath(buffer.toString());
            buffer= null;
        } else if (PHPDefaults.XML_RSP_THEME_FORMATS.equals(localName))
        {
            soundLink.setFormats(formats);
            formats= null;
        } else if (PHPDefaults.XML_RSP_THEME_AIFF.equals(localName))
        {
            formats.setAiff(buffer.toString());
            buffer= null;
        } else if (PHPDefaults.XML_RSP_THEME_MP3.equals(localName))
        {
            formats.setMp3(buffer.toString());
            buffer= null;
        }
    }
    protected void onStartElement(String uri, String localName, String qName, Attributes attributes)
    {
        if (PHPDefaults.XML_RSP_THEME_ADDED.equals(localName))
            soundTheme= new SoundTheme();
        else if (PHPDefaults.XML_RSP_THEME_LINK.equals(localName))
            soundLink= new SoundLink();
        else if (PHPDefaults.XML_RSP_THEME_FORMATS.equals(localName))
            formats= new Formats();
        else if (PHPDefaults.XML_RSP_THEME_REMOVED.equals(localName) || soundTheme != null || soundLink != null
                || formats != null)
            buffer= new StringBuilder();
    }
}

