package com.trapster.android.comms.php.handler;

import com.trapster.android.PHPDefaults;
import com.trapster.android.comms.ErrorListener;
import com.trapster.android.util.TrapsterError;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DefaultHandler2;

// Function as ErrorParser
public class TrapsterHandler extends DefaultHandler2
{
	ErrorListener listener;
	//
	boolean read;
	boolean error;
	TrapsterError trapsterError;
	StringBuilder errorMessageString;
	//
	protected StringBuilder buffer;
	//
	public TrapsterHandler(ErrorListener listener)
	{
		this.listener= listener;
	}
	public final void characters(char[] ch, int start, int length) throws SAXException
	{
		if (error)
		{
			if (errorMessageString != null)
				errorMessageString.append(ch, start, length);
		} else
			onCharacters(ch, start, length);
	}
	public final void endDocument() throws SAXException
	{
		if (error)
			listener.onError(trapsterError);
	}
	public final void endElement(String uri, String localName, String qName) throws SAXException
	{
		if (error)
		{
			if (errorMessageString != null)
			{
				trapsterError= new TrapsterError(TrapsterError.TYPE_API_ERROR, errorMessageString.toString());
				errorMessageString= null;
			}
		} else
			onEndElement(uri, localName, qName);
	}
	protected void onCharacters(char[] ch, int start, int length) throws SAXException
	{
		if (buffer != null)
			buffer.append(ch, start, length);
	}
	protected void onEndElement(String uri, String localName, String qName) throws SAXException
	{
	}
	protected void onStartElement(String uri, String localName, String qName, Attributes attributes)
			throws SAXException
	{
	}
	public final void startElement(String uri, String localName, String qName, Attributes attributes)
			throws SAXException
	{
		if (!read)
		{
			if (!PHPDefaults.XML_TRAPSTER.equals(localName))
			{
				trapsterError= new TrapsterError(TrapsterError.TYPE_COMMUNICATION_ERROR,
						"Error Communicating With Server");
				error= true;
			}
			read= true;
		} else if (PHPDefaults.XML_RSP_ERROR.equals(localName))
		{
			errorMessageString= new StringBuilder();
			error= true;
		} else if (!error)
			onStartElement(uri, localName, qName, attributes);
	}
}
