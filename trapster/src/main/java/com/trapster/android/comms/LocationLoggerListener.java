package com.trapster.android.comms;

import java.util.List;

import com.here.android.common.GeoPosition;

public interface LocationLoggerListener {

    public void onBulkLocationUpdate(List<GeoPosition> locations);

}
