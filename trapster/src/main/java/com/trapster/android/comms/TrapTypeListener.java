package com.trapster.android.comms;

import com.trapster.android.model.TrapTypes;

public interface TrapTypeListener extends ErrorListener {
	void onTrapTypes(TrapTypes types);
}
