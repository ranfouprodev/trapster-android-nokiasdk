package com.trapster.android.comms.php;

import java.io.IOException;
import java.io.InputStream;

import com.twolinessoftware.android.framework.service.comms.Parser;

public class ResourceStorageParser extends Parser {

	private Object resource;
	private ResourceLoadedListener listener;

	public interface ResourceLoadedListener {

		public void onResourceLoaded(Object resource, String filename);

	}

	public ResourceStorageParser(Object resource,
			ResourceLoadedListener listener) {
		super();
		this.resource = resource;
		this.listener = listener;
	}

	@Override
	public void process(InputStream stream) throws IOException {
		String xml = convertStreamToString(stream);
		if (listener != null)
			listener.onResourceLoaded(resource, xml);
	}

}
