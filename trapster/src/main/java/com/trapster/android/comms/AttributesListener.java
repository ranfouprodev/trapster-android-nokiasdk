package com.trapster.android.comms;

import com.trapster.android.model.TrapsterResources;

public interface AttributesListener extends ErrorListener {
	void onAttributes(TrapsterResources resources);
}
