package com.trapster.android.comms.rest.parser;

import android.content.Context;
import android.util.DisplayMetrics;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.trapster.android.comms.AttributesListener;
import com.trapster.android.model.ImageSpec;
import com.trapster.android.model.TrapsterResources;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.service.comms.JsonParser;

import java.lang.reflect.Type;

/**
 * Created by John on 5/28/13.
 */
public class AttributesParser extends JsonParser {

    private static final String LOGNAME = "AttributesParser";
    private static final boolean ADJUST_IMAGE_SIZES_FOR_RESOLUTION = true;

    private final AttributesListener listener;

   private DisplayMetrics displayMetrics;


    public AttributesParser(Context context,AttributesListener listener)
    {
        super();
        this.listener= listener;
        displayMetrics = context.getResources().getDisplayMetrics();
    }

    @Override
    public void parseJson(Status status, String jsonStream) {

        try{

            Gson gson = getBuilder().registerTypeAdapter(ImageSpec.class, new ImageSpecDeserializer()).create();

            TrapsterResources resources =  gson.fromJson(jsonStream, TrapsterResources.class);

            if(listener != null)
                listener.onAttributes(resources);

        }catch(Exception e){
            //Log.e(LOGNAME, "Attributes parsing:" + e.getMessage());

            if(listener != null)
                listener.onError(new TrapsterError(TrapsterError.TYPE_INVALID_JSON_FORMAT,"Incorrect Attributes:"+e.getMessage()));
        }

    }


    private class ImageSpecDeserializer implements JsonDeserializer<ImageSpec> {

        @Override
        public ImageSpec deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {

            JsonObject jsonOnject = jsonElement.getAsJsonObject();

            Gson gson = new Gson();
            ImageSpec imagespec = gson.fromJson(jsonElement, ImageSpec.class);

            if(ADJUST_IMAGE_SIZES_FOR_RESOLUTION){
                float ratio = displayMetrics.densityDpi / DisplayMetrics.DENSITY_LOW;

                if(ratio > 1){
                    imagespec.setHeight((int) (jsonOnject.get("height").getAsInt() * ratio));
                    imagespec.setWidth((int) (jsonOnject.get("width").getAsInt() * ratio));
                    imagespec.setPinx((int) (jsonOnject.get("pinx").getAsInt() * ratio));
                    imagespec.setPiny((int) (jsonOnject.get("piny").getAsInt() * ratio));
                }

             //   Log.v(LOGNAME,"Scaling ratio:"+ratio+" Display:"+displayMetrics.densityDpi+" Height:"+imagespec.getHeight());

            }

            return imagespec;
        }
    }
}
