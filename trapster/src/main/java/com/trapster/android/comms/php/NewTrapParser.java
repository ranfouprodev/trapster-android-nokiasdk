package com.trapster.android.comms.php;

import com.trapster.android.PHPDefaults;
import com.trapster.android.comms.NewTrapListener;
import com.trapster.android.model.Trap;

public class NewTrapParser extends TrapsterParser {

	private static final String LOGNAME = "NewTrapParser";

	private NewTrapListener listener;

	private Trap trap;

	public NewTrapParser(Trap trap, NewTrapListener listener) {
		super(listener);
		this.listener = listener;
		this.trap = trap;

	}

	@Override
	public void parseXml(String xml) {

		String data = getNodesFromXml(xml, PHPDefaults.XML_RSP_DATA);

		String id = getNodesFromXml(data, PHPDefaults.XML_RSP_ID);

		String message = getNodesFromXml(data, PHPDefaults.XML_RSP_MSG);

		trap.setId(Integer.parseInt(getTextFromNode(id, "id")));

		if (listener != null)
			listener.onNewTrapAdded(trap, message);

	}

}
