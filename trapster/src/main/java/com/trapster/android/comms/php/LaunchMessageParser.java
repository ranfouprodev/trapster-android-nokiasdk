package com.trapster.android.comms.php;

import com.trapster.android.comms.LaunchMessageListener;
import com.trapster.android.comms.php.handler.LaunchMessageHandler;
import com.trapster.android.util.Log;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.service.comms.Parser;

import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;


public class LaunchMessageParser extends Parser {

    static final String LOGNAME= "Trapster.LaunchMessageParser";
    //
    LaunchMessageListener listener;

    public LaunchMessageParser(LaunchMessageListener listener)
    {
       this.listener = listener;
    }

    @Override
    public void process(InputStream in) throws IOException {
        try
        {
            if (listener != null)
            {
                LaunchMessageHandler handler= new LaunchMessageHandler(listener);
                SAXParserFactory.newInstance().newSAXParser().parse(in, handler);
                listener.onLaunchMessage(handler.getLaunchMessage());
            }
        } catch (Exception e)
        {
            if (listener != null)
                listener.onError(new TrapsterError(TrapsterError.TYPE_INVALID_XML_FORMAT, " Unable to import launch message:"
                        + e.getMessage()));
        }
    }
}
