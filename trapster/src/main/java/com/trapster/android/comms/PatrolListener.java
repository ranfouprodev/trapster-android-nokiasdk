package com.trapster.android.comms;

import com.trapster.android.model.PatrolPaths;

public interface PatrolListener extends ErrorListener {
    void onPatrolPath(PatrolPaths patrolPaths, double lat, double lon, double radius);
}
