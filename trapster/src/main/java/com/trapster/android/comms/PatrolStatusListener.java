package com.trapster.android.comms;

public interface PatrolStatusListener extends DataLoadedListener
{
    void onPatrolPathUpdate();
}
