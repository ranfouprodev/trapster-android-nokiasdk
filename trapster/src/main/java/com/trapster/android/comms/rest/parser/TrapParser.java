package com.trapster.android.comms.rest.parser;


import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.trapster.android.comms.TrapListener;
import com.trapster.android.model.Trap;
import com.trapster.android.model.Traps;
import com.trapster.android.util.Log;
import com.twolinessoftware.android.framework.service.comms.JsonParser;

import java.lang.reflect.Type;

/**
 * Created by John on 5/28/13.
 */
public class TrapParser extends JsonParser {

    private static final String LOGNAME = "TrapParser";
    private final TrapListener listener;
    private final double lat;
    private final double lon;
    private final double radius;


    public TrapParser(double lat, double lon, double radius, TrapListener listener) {

        super();
        this.listener= listener;
        this.lat = lat;
        this.lon = lon;
        this.radius = radius;
    }

    @Override
    public void parseJson(Status status, String jsonStream) {

       // Log.v(LOGNAME,"Parsing jsonStream:"+jsonStream);

        try{

            Gson gson = getBuilder().registerTypeAdapter(Trap.class, new TrapDeserializer()).create();

            Traps traps =  gson.fromJson(jsonStream, Traps.class);

            traps.setLat(lat);
            traps.setLon(lon);
            traps.setRadius(radius);

        if(listener != null)
            listener.onTraps(traps);

        }catch(Exception e){
            Log.e(LOGNAME,"Trap parsing:"+e.getMessage()+":"+Log.getStackTrace(e));
        }

    }

    private class TrapDeserializer implements JsonDeserializer<Trap> {

        @Override
        public Trap deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {

            JsonObject jsonOnject = jsonElement.getAsJsonObject();

            Gson gson = new Gson();
            Trap trap = gson.fromJson(jsonElement,Trap.class);

            double lat = jsonOnject.get("latitude").getAsDouble();
            double lng = jsonOnject.get("longitude").getAsDouble();

            trap.setGeometry(lat, lng);

             if(trap.getLifetime() == 0)
                trap.setExpiry(0);
             else
                trap.setExpiry(trap.getLvoteSec()+trap.getLifetime());



            return trap;
        }
    }

}
