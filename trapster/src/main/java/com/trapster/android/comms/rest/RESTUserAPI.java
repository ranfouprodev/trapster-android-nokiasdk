package com.trapster.android.comms.rest;

import android.app.Application;
import android.util.Log;

import com.trapster.android.RESTDefaults;
import com.trapster.android.comms.BasicResponseListener;
import com.trapster.android.comms.CommunicationManager;
import com.trapster.android.comms.EmailAvailableListener;
import com.trapster.android.comms.LoginListener;
import com.trapster.android.comms.UsernameAvailableListener;
import com.trapster.android.comms.rest.parser.BasicResponseParser;
import com.trapster.android.comms.rest.parser.EmailAvailableParser;
import com.trapster.android.comms.rest.parser.ForgotPasswordParser;
import com.trapster.android.comms.rest.parser.UserAuthenicationParser;
import com.trapster.android.comms.rest.parser.UsernameAvailableParser;
import com.trapster.android.comms.rest.wrapper.AuthWrapper;
import com.trapster.android.model.User;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;
import com.twolinessoftware.android.framework.service.comms.HttpGetJsonWorker;
import com.twolinessoftware.android.framework.service.comms.HttpPostJsonWorker;
import com.twolinessoftware.android.framework.service.comms.HttpPutJsonWorker;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.inject.Inject;

/**
 * Created by John on 8/20/13.
 */
public class RESTUserAPI implements IUserAPI{

    private static final String LOGNAME = "RESTUserAPI";

    @Inject
    JSONWriter jsonWriter;

    @Inject
    CommunicationManager communicationManager;

    @Inject
    Application application;

    @Override
    public void getUserByFacebookToken(User user, LoginListener listener, CommunicationStatusListener statusListener) {

        AuthWrapper authWrapper = new AuthWrapper(user);



        String URL = RESTDefaults.REST_API_RESOURCE_USER_FACEBOOK +"?token="+user.getAccessToken()+"&"+authWrapper.createGetAuthString();

        Log.v(LOGNAME,"Sending:"+URL);

        HttpGetJsonWorker worker = new HttpGetJsonWorker( URL );

        communicationManager.handleAsyncMessage(worker,  new UserAuthenicationParser(user,listener, application, false), statusListener);
    }

    @Override
    public void isUsernameTaken(String username, UsernameAvailableListener listener, CommunicationStatusListener statusListener) {

        AuthWrapper authWrapper = new AuthWrapper(null);

        String URL = RESTDefaults.REST_API_RESOURCE_USER_AVAILABLE+"?login="+username+"&"+authWrapper.createGetAuthString();

        Log.v(LOGNAME,"Sending:"+URL);

        HttpGetJsonWorker worker = new HttpGetJsonWorker( URL );

        communicationManager.handleAsyncMessage(worker, new UsernameAvailableParser(username,listener),statusListener);
    }

    @Override
    public void signUpWithFacebookToken(User user, LoginListener listener, CommunicationStatusListener statusListener) {

        String URL = RESTDefaults.REST_API_RESOURCE_USER_FACEBOOK +"?token="+user.getAccessToken()+"&login="+user.getUserName();

        String requestString = jsonWriter.createFacebookRegisterRequest(user, true, false);

        Log.v(LOGNAME,"Posting:"+URL+" Request:"+requestString);


        HttpPostJsonWorker worker = new HttpPostJsonWorker(URL, requestString);
        communicationManager.handleAsyncMessage(worker, new UserAuthenicationParser(user,listener, application, true), statusListener);

    }

    @Override
    public void mergeWithFacebook(User user, String token, BasicResponseListener listener, CommunicationStatusListener statusListener) {

        String URL = RESTDefaults.REST_API_RESOURCE_USER_FACEBOOK +"?token="+user.getAccessToken();

        String requestString = jsonWriter.putMergeFacebook(user,token);

        HttpPutJsonWorker worker = new HttpPutJsonWorker(URL, requestString);

        Log.v(LOGNAME,"Merging Account Put:"+URL + " Request:"+requestString);

        communicationManager.handleAsyncMessage(worker, new BasicResponseParser(listener), statusListener);
    }

    @Override
    public void forgotPassword(String name, BasicResponseListener listener, CommunicationStatusListener statusListener) {

        String tempUrl = null;
        try {
             tempUrl = RESTDefaults.REST_API_FORGOT_PASSWORD + URLEncoder.encode(name,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            tempUrl = RESTDefaults.REST_API_FORGOT_PASSWORD + name;
        }

        HttpGetJsonWorker worker = new HttpGetJsonWorker( tempUrl );

        communicationManager.handleAsyncMessage(worker,  new ForgotPasswordParser(listener), statusListener);

    }

    @Override
    public void isEmailTaken(String email, EmailAvailableListener listener, CommunicationStatusListener statusListener) {

        AuthWrapper authWrapper = new AuthWrapper(null);

        String URL = RESTDefaults.REST_API_RESOURCE_EMAIL_AVAILABLE+"?emailaddr="+email+"&"+authWrapper.createGetAuthString();

        Log.v(LOGNAME,"Sending:"+URL);

        HttpGetJsonWorker worker = new HttpGetJsonWorker( URL );

        communicationManager.handleAsyncMessage(worker, new EmailAvailableParser(email,listener),statusListener);


    }

}
