package com.trapster.android.comms.rest;

/**
 * Result Codes from the server. Not all are included.
 *
 * Created by John on 7/18/13.
 */

import java.util.HashMap;
import java.util.Map;

/**
 * Registration error codes
 *
 608 Bad email address The specified email address is not valid.
 609 User email already in use The email address %s is already registered.
 610 Email confirmation does
 not match
 The confirmation email does not match the entered email.
 611 Missing user name You must enter a username.
 612 Invalid user name Only letters, numbers and _ are permitted in a username.
 613 Invalid user name length Username must not have more than %d characters.
 614 User name already in use The username %s is not available. Please try another.
 615 Missing password You must enter a password.
 616 Invalid password length Password length must be between %d-%d characters.
 617 Password confirmation
 does not match
 Password does not match.
 620 Invalid phone number Please enter only your numeric phone number, including
 area code, i.e., 2225551212. Do not put a country code
 before it.
 621 T&C not accepted You must agree to the Terms and Conditions.
 801 Invalid confirmation code Incorrect confirmation code.

 */
public enum StatusCode {
    OK(200),
    ACCEPTED(202),
    INVALID_IP(401),
    INVALID_APPKEY(402),
    // Tested
    INVALID_USER_CREDENTIALS(403),
    // Tested
    REQUIRES_CONFIRMATION_CODE(404),
    BAD_EMAIL(608),
    //Tested
    EMAIL_TAKEN(609),
    EMAIL_MISMATCH(610),
    MISSING_USERNAME(611),
    INVALID_USERNAME(612),
    USERNAME_TOO_LONG(613),
    // Tested
    USERNAME_IN_USE(614),
    MISSING_PASSWORD(615),
    INVALID_PASSWORD_LENGTH(616),
    PASS_CONFIRM_ERROR(617),
    INVALID_SMS(620),
    NO_TERMS(621),
    // Tested
    INVALID_CONF_CODE(800),
    UNKNOWN(999);

    private final int code;

    private static final Map<Integer, StatusCode> map = new HashMap<Integer, StatusCode>();

    static {
        for (StatusCode sc : values()) {
            map.put(sc.getCode(), sc);
        }
    }

    StatusCode(int i) {
        this.code = i;
    }

    public static StatusCode getByCode(int code) {
        return map.get(code);
    }

    public int getCode() {
        return code;
    }
}
