package com.trapster.android.comms.php;

import com.google.inject.Inject;
import com.here.android.common.GeoPosition;
import com.trapster.android.Defaults;
import com.trapster.android.PHPDefaults;
import com.trapster.android.comms.*;
import com.trapster.android.manager.SessionManager;
import com.trapster.android.model.ProbeData;
import com.trapster.android.model.Trap;
import com.trapster.android.model.UpdatedRouteLinkInfo;
import com.trapster.android.model.User;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;
import com.twolinessoftware.android.framework.service.comms.HttpGetWorker;
import com.twolinessoftware.android.framework.service.comms.HttpPostWorker;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class PhpTrapsterAPI implements ITrapsterAPI {

	@Inject
	CommunicationManager cm;
	
	@Inject XmlWriter xmlWriter;

    @Inject
    SessionManager sessionManager;

	@Override
	public void login(User user, LoginListener listener,
			CommunicationStatusListener statusListener) {

		HttpPostWorker worker = new HttpPostWorker(PHPDefaults.PHP_API_CHECK_LOGIN,
				xmlWriter.loginMessage(user));
		worker.setId(Defaults.WORKER_LOGIN);
		LoginParser parser = new LoginParser(user,listener, sessionManager);

		cm.handleAsyncMessage(worker, parser, statusListener, false);
	}

	public void login(User user, String confcode, LoginListener listener,
			CommunicationStatusListener statusListener) {

		HttpPostWorker worker = new HttpPostWorker(PHPDefaults.PHP_API_CHECK_LOGIN,
				xmlWriter.loginMessage(user, confcode));
		worker.setId(Defaults.WORKER_LOGIN);
		LoginParser parser = new LoginParser(user,listener, sessionManager);

		cm.handleAsyncMessage(worker, parser, statusListener, false);
	}

	public void resendCode(User user, LoginListener listener,
			CommunicationStatusListener statusListener) {

		HttpPostWorker worker = new HttpPostWorker(PHPDefaults.PHP_API_RESEND,
				xmlWriter.loginMessage(user));
		LoginParser parser = new LoginParser(user,listener, sessionManager);

		cm.handleAsyncMessage(worker, parser, statusListener, false);
	}

	public void register(String email, String userName, String password,
			boolean agree, boolean newsletter, String smsaddr, String carrier,
			LoginListener listener, CommunicationStatusListener statusListener) {

        User user = new User();
        user.setCredentials(userName,password);

		HttpPostWorker worker = new HttpPostWorker(PHPDefaults.PHP_API_SIGNUP,
				xmlWriter.registerMessage(email, userName, password,
						agree, newsletter, smsaddr, carrier));
		LoginParser parser = new LoginParser(user,listener, sessionManager);

		cm.handleAsyncMessage(worker, parser, statusListener, false);
	}

	public void getAttributes(long lastUpdateTime, AttributesListener listener,
			CommunicationStatusListener statusListener) {

		HttpPostWorker worker = new HttpPostWorker(PHPDefaults.PHP_API_ATTRIBUTES,
				xmlWriter.getAttributes(lastUpdateTime));
		AttributesParser parser = new AttributesParser(listener);

		cm.handleAsyncMessage(worker, parser, statusListener);
	}

	public void getLegalAttributes(long lastUpdateTime,
			AttributesListener listener,
			CommunicationStatusListener statusListener) {

		HttpPostWorker worker = new HttpPostWorker(PHPDefaults.PHP_API_ATTRIBUTES,
				xmlWriter.getLegalAttributes(lastUpdateTime));
		worker.setId(Defaults.WORKER_LEGAL);
		AttributesParser parser = new AttributesParser(listener);

		cm.handleAsyncMessage(worker, parser, statusListener);
	}

	public void getCategories(long lastUpdateTime, CategoryListener listener,
			CommunicationStatusListener statusListener) {

		HttpPostWorker worker = new HttpPostWorker(PHPDefaults.PHP_API_TYPES,
				xmlWriter.getCategories(lastUpdateTime));
		CategoryParser parser = new CategoryParser(listener);

		cm.handleAsyncMessage(worker, parser, statusListener);
	}

	public void getTrapTypes(long lastUpdateTime, TrapTypeListener listener,
			CommunicationStatusListener statusListener) {

		HttpPostWorker worker = new HttpPostWorker(PHPDefaults.PHP_API_TYPES,
				xmlWriter.getTrapTypes(lastUpdateTime));
		TrapTypeParser parser = new TrapTypeParser(listener);

		cm.handleAsyncMessage(worker, parser, statusListener);
	}
	
	public void updateAllTraps(double lat, double lon, double radius, TrapListener listener, CommunicationStatusListener statusListener){
		
		HttpPostWorker worker = new HttpPostWorker(PHPDefaults.PHP_API_TRAPS_IN_RANGE, xmlWriter.requestAllTrapsInArea(lat, lon, radius));
		TrapParser parser = new TrapParser(listener,lat,lon,radius);
		
		cm.handleAsyncMessage( worker, parser,statusListener);
	}
	
	
	public void rateTrap(boolean agree, Trap trap,BasicResponseListener confirmListener, CommunicationStatusListener statusListener ){
		
		
		HttpPostWorker worker = new HttpPostWorker(PHPDefaults.PHP_API_TRAP_VOTE, xmlWriter.rateTrapMessage(trap, (agree) ? "Y":"N"));
				
		cm.handleAsyncMessage(worker, new BasicResponseParser(confirmListener),statusListener);
		
	}
	
	public void addNewTrap(Trap trap , NewTrapListener listener, CommunicationStatusListener statusListener, double heading){
		
		HttpPostWorker worker = new HttpPostWorker(PHPDefaults.PHP_API_NEW_TRAP_WITH_ID, xmlWriter.reportTrapMessage(trap, heading));
		NewTrapParser parser = new NewTrapParser(trap,listener);
		
		cm.handleAsyncMessage(worker, parser,statusListener);
		
	}
	
	public void deleteTrap(Trap trap , BasicResponseListener listener, CommunicationStatusListener statusListener){
		
		HttpPostWorker worker = new HttpPostWorker(PHPDefaults.PHP_API_REMOVE_TRAP, xmlWriter.deleteTrapMessage(trap));
		BasicResponseParser parser = new BasicResponseParser(listener);
		cm.handleAsyncMessage(worker, parser,statusListener);
		
	}
    public void ignoreTrap(Trap trap ,  CommunicationStatusListener statusListener){

        HttpPostWorker worker = new HttpPostWorker(PHPDefaults.PHP_API_IGNORE_TRAP, xmlWriter.ignoreTrapMessage(trap));

        cm.handleAsyncMessage(worker, null,statusListener);

    }

	/*Formerly known as sendIamHere
	 * 
	 *  (non-Javadoc)
	 * @see com.trapster.android.comms.ITrapsterAPI#sendProbeData(java.util.List, com.twolinessoftware.android.framework.comms.CommunicationStatusListener)
	 */
	@Override
	public void sendProbeData(List<ProbeData> locations,
			CommunicationStatusListener statusListener) {
		// TODO Auto-generated method stub
		//public void sendIAmHere(List<Location> locations,
		//		CommunicationStatusListener statusListener) {

			HttpPostWorker worker = new HttpPostWorker(PHPDefaults.PHP_API_IAMHERE,
					xmlWriter.probeData(locations));

			cm.handleAsyncMessage(worker, null, statusListener);
	//	}
	}

    public void listSounds(long lastUpdateTime, SoundThemeListener listener, CommunicationStatusListener statusListener){


        HttpPostWorker worker = new HttpPostWorker(PHPDefaults.PHP_API_SOUNDTHEME, xmlWriter.listSoundThemes(lastUpdateTime));
        SoundThemeParser parser = new SoundThemeParser(listener);

        cm.handleAsyncMessage(worker, parser,statusListener);
    }

    public void sendIAmHere(List<GeoPosition> locations,CommunicationStatusListener statusListener){


        HttpPostWorker worker = new HttpPostWorker(PHPDefaults.PHP_API_IAMHERE, xmlWriter.iAmHereUpdate(locations));

        cm.handleAsyncMessage(worker, null,statusListener);
    }

    public void getPath(double lat, double lon, double radius, PatrolListener listener, CommunicationStatusListener statusListener){

        HttpPostWorker worker = new HttpPostWorker(PHPDefaults.PHP_API_PATROLPATH, xmlWriter.getPatrolPoints(lat, lon, radius));
        PatrolParser parser = new PatrolParser(listener,lat,lon,radius);

        worker.setId(Defaults.WORKER_PATROL);

        cm.handleAsyncMessage(worker, parser,statusListener);
    }

    public void getLaunchMessage(LaunchMessageListener listener, CommunicationStatusListener statusListener)
    {
        HttpPostWorker worker = new HttpPostWorker(PHPDefaults.PHP_API_LAUNCHMESSAGE, xmlWriter.launchMessage());
        LaunchMessageParser parser = new LaunchMessageParser(listener);

        worker.setId(Defaults.WORKER_LAUNCH);

        cm.handleAsyncMessage(worker, parser,statusListener);
    }

    @Override
    public void submitBecomeModeratorRequest()
    {
        HttpPostWorker worker = new HttpPostWorker(Defaults.PHP_API_BECOME_MODERATOR, xmlWriter.createModeratorPOSTBodyl());

        worker.setId(Defaults.WORKER_LAUNCH);

        cm.handleAsyncMessage(worker, null,null);
    }

    public void updateNewSpeedLimit(ArrayList<UpdatedRouteLinkInfo> updatedRouteLinkInfo, CommunicationStatusListener statusListener){


        HttpPostWorker worker = new HttpPostWorker(PHPDefaults.PHP_API_REPORT_NEW_SPEED_LIMIT,
                xmlWriter.reportSpeedLimitMessage(updatedRouteLinkInfo));

        cm.handleAsyncMessage(worker, null,statusListener);

    }

    public void sendNPSResults(String vote, String feedback,
                               String emailAddress, CommunicationStatusListener statusListener) {

        try
        {
            String encodedurl =  xmlWriter.reportNPSResults(vote, feedback, emailAddress);
            HttpGetWorker worker = new HttpGetWorker(PHPDefaults.REPORT_NPS + "?" + encodedurl);
            cm.handleAsyncMessage(worker, null, statusListener);
        }
        catch (Exception e){}
    }

    public void getNewSpeedLimit(UpdatedRouteLinkListener updatedRouteLinkListener,
                                 CommunicationStatusListener statusListener)
    {
        try
        {
            String encodedurl = URLEncoder.encode(xmlWriter.getSpeedLimitMessage(), "utf-8");
            HttpGetWorker worker = new HttpGetWorker(PHPDefaults.PHP_API_REPORT_NEW_SPEED_LIMIT + "?format=xml&request=" + encodedurl);
            UpdatedRouteLinkParser parser = new UpdatedRouteLinkParser(updatedRouteLinkListener);
            cm.handleAsyncMessage(worker, parser,statusListener);
        }
        catch (Exception e){}
    }

    public void getUserStatistics(String username, StatisticsListener statisticsListener, CommunicationStatusListener statusListener)
    {
    }

}
