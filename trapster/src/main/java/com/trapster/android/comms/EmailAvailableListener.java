package com.trapster.android.comms;

/**
 * Created by John on 8/20/13.
 */
public interface EmailAvailableListener {
    void onEmailAvailable(String email, String displayname, long id,  boolean available);
}
