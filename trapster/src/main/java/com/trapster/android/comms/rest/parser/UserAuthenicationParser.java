package com.trapster.android.comms.rest.parser;

import android.content.Context;
import android.util.Log;

import com.trapster.android.comms.LoginListener;
import com.trapster.android.comms.rest.StatusCode;
import com.trapster.android.model.Profile;
import com.trapster.android.model.User;
import com.twolinessoftware.android.framework.service.comms.JsonParser;

import roboguice.RoboGuice;


/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 5/24/13
 * Time: 10:21 AM
 * To change this template use File | Settings | File Templates.
 */
public class UserAuthenicationParser extends JsonParser {

    private static final String LOGNAME = "UserAuthenicationParser";

    private final User user;

    private LoginListener listener;

    private boolean register;


    public UserAuthenicationParser(User user,LoginListener listener, Context context, boolean register) {
        super();
        this.user = user;
        this.listener = listener;
        this.register = register;
        RoboGuice.getInjector(context).injectMembersWithoutViews(this);
    }

    @Override
    public void parseJson(Status status, String jsonString) {
        switch (StatusCode.getByCode(status.statusCode)) {
            case OK:
            case ACCEPTED:

                if (!register) {
                    Profile profile = gson.fromJson(jsonString, Profile.class);
                    user.setUserName(profile.getName());
                    user.setProfile(profile);
                }

                if (listener != null)
                    listener.onLoginSuccess(user,status.message);

                break;
            case REQUIRES_CONFIRMATION_CODE:
                if (listener != null)
                    listener.onLoginFail(user, StatusCode.INVALID_CONF_CODE);
                break;
            case INVALID_USER_CREDENTIALS:
                if (listener != null)
                    listener.onLoginFail(user, StatusCode.INVALID_USER_CREDENTIALS);
                break;
            case EMAIL_TAKEN:
                if (listener != null)
                    listener.onLoginFail(user, StatusCode.EMAIL_TAKEN);
                break;

            case USERNAME_IN_USE:
                if (listener != null)
                    listener.onLoginFail(user, StatusCode.USERNAME_IN_USE);
                break;

            case INVALID_CONF_CODE:
                if (listener != null)
                    listener.onLoginFail(user, StatusCode.INVALID_CONF_CODE);
                break;
            default:
                if (listener != null)
                    listener.onLoginFail(user, StatusCode.UNKNOWN);
        }
    }

    @Override
    public boolean handleHttpStatusErrors(Status status) {
        Log.v(LOGNAME,"User not found:"+status.statusCode);

        if(status.statusCode == 404){
            if (listener != null)
                listener.onLoginFail(user, StatusCode.INVALID_USERNAME);
            return true;
        }
        return false;
    }
}
