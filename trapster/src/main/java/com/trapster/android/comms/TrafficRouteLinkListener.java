package com.trapster.android.comms;

import com.trapster.android.comms.ErrorListener;
import com.trapster.android.model.RouteLinks;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 10/24/13
 * Time: 2:49 PM
 * To change this template use File | Settings | File Templates.
 */
public interface TrafficRouteLinkListener extends ErrorListener {
    void onTrafficUpdate(RouteLinks segments, String quadkey);
}
