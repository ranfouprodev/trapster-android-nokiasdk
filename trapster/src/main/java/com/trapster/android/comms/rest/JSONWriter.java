package com.trapster.android.comms.rest;


import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.trapster.android.RESTDefaults;
import com.trapster.android.comms.rest.wrapper.AddTrapWrapper;
import com.trapster.android.comms.rest.wrapper.AuthWrapper;
import com.trapster.android.comms.rest.wrapper.FacebookRegisterWrapper;
import com.trapster.android.comms.rest.wrapper.MetaWrapper;
import com.trapster.android.comms.rest.wrapper.ProbeWrapper;
import com.trapster.android.comms.rest.wrapper.RegisterWrapper;
import com.trapster.android.comms.rest.wrapper.RequestWrapper;
import com.trapster.android.comms.rest.wrapper.VoteTrapWrapper;
import com.trapster.android.manager.SessionManager;
import com.trapster.android.model.ProbeData;
import com.trapster.android.model.Trap;
import com.trapster.android.model.User;

import java.net.URLEncoder;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;


/*
Not sure if we need this as a singleton. What state is maintained here?
 */
@Singleton
public class JSONWriter
{
    @Inject SessionManager sessionManager;
    private final Gson gson;

    public JSONWriter()
    {
        gson = new Gson();
    }

    private String encodeKeyValue (String key, String value )
    {
        String encoded = "";
        try
        {
            encoded = key + "=" + URLEncoder.encode(value, "utf-8");
        }
        catch (Exception e){}

        return encoded;
    }

    public String createPatrolURLString(double lat, double lng, double radius)
    {
        AuthWrapper authWrapper = new AuthWrapper(sessionManager.getUser());
        StringBuffer sb = new StringBuffer(RESTDefaults.REST_API_RESOURCE_PATROL + authWrapper.createGetAuthString());

        sb.append(encodeKeyValue(RESTDefaults.REST_PARAMETER_PATROL_LATITUDE, String.valueOf(lat)) + "&");
        sb.append(encodeKeyValue(RESTDefaults.REST_PARAMETER_PATROL_LONGITUDE, String.valueOf(lng)) + "&");
        sb.append(encodeKeyValue(RESTDefaults.REST_PARAMETER_PATROL_RADIUS, String.valueOf(radius)));

        return sb.toString();
    }




    public String createLoginRequest(User user)
    {
        return gson.toJson(new RestRequest(new RequestWrapper(new AuthWrapper(user), new MetaWrapper(), null)));
    }

    public String createFacebookRegisterRequest(User user, boolean agree, boolean newsletter)
    {

        return gson.toJson(new RestRequest(new RequestWrapper(new AuthWrapper(user),
                new MetaWrapper(), new FacebookRegisterWrapper(user.getAccessToken(), user.getUserName()))));

    }

    public String createRegisterRequest(String email, String userName,
            String password, boolean agree, boolean newsletter, String smsaddr)
    {
        return createRequestParameters(new RegisterWrapper(email, userName, password, agree, newsletter, smsaddr));
    }

    public String createProbeDataRequest(List<ProbeData> probeData)
    {
        return createRequestParameters(new ProbeWrapper(probeData.toArray()));
    }

    private String createRequestParameters(Object params)
    {
        return gson.toJson(new RestRequest(new RequestWrapper(new AuthWrapper(sessionManager.getUser()),
                new MetaWrapper(), params)));
    }

    public String createAddNewTrapRequest(Trap trap, int heading) {
        return createRequestParameters(new AddTrapWrapper(trap,heading));
    }

    public String createVoteTrapRequest(boolean agree) {
        return createRequestParameters(new VoteTrapWrapper(agree));
    }



    public String getSoundThemes(long lastUpdateTime) {

        AuthWrapper authWrapper = new AuthWrapper(sessionManager.getUser());
        StringBuffer sb = new StringBuffer(RESTDefaults.REST_API_RESOURCE_ATTRIBUTES_SOUNDTHEMES + authWrapper.createGetAuthString());

        sb.append(encodeKeyValue(RESTDefaults.REST_PARAMETER_ATTRIBUTES_LAST_MODIFIED_DATE, String.valueOf(lastUpdateTime)));

        return sb.toString();
    }

    public String getCategories(long lastUpdateTime) {

        AuthWrapper authWrapper = new AuthWrapper(sessionManager.getUser());
        StringBuffer sb = new StringBuffer(RESTDefaults.REST_API_RESOURCE_ATTRIBUTES_TRAPCATEGORIES + authWrapper.createGetAuthString());

        sb.append(encodeKeyValue(RESTDefaults.REST_PARAMETER_ATTRIBUTES_LAST_MODIFIED_DATE, String.valueOf(lastUpdateTime)));

        return sb.toString();
    }

    public String getTraps(double lat, double lon, double radius) {
        AuthWrapper authWrapper = new AuthWrapper(sessionManager.getUser());
        StringBuffer sb = new StringBuffer(RESTDefaults.REST_API_RESOURCE_TRAP+"?"+ authWrapper.createGetAuthString());

        sb.append(encodeKeyValue(RESTDefaults.REST_PARAMETER_ATTRIBUTES_LATITUDE, String.valueOf(lat))+"&");
        sb.append(encodeKeyValue(RESTDefaults.REST_PARAMETER_ATTRIBUTES_LONGITUDE, String.valueOf(lon))+"&");
        sb.append(encodeKeyValue(RESTDefaults.REST_PARAMETER_ATTRIBUTES_RADIUS, String.valueOf(radius)));

        return sb.toString();
    }

    public String getStringAttributes(long lastUpdateTime) {

        AuthWrapper authWrapper = new AuthWrapper(sessionManager.getUser());
        StringBuffer sb = new StringBuffer(RESTDefaults.REST_API_RESOURCE_ATTRIBUTES_STRINGS_TERMS + authWrapper.createGetAuthString());

        sb.append(encodeKeyValue(RESTDefaults.REST_PARAMETER_ATTRIBUTES_LAST_MODIFIED_DATE, String.valueOf(lastUpdateTime)));

        return sb.toString();
    }

    public String getImageAttributes(long lastUpdateTime) {

        AuthWrapper authWrapper = new AuthWrapper(sessionManager.getUser());
        StringBuffer sb = new StringBuffer(RESTDefaults.REST_API_RESOURCE_ATTRIBUTES_IMAGES + authWrapper.createGetAuthString());

        sb.append(encodeKeyValue(RESTDefaults.REST_PARAMETER_ATTRIBUTES_LAST_MODIFIED_DATE, String.valueOf(lastUpdateTime)));

        return sb.toString();
    }

    public String getTrapTypes(long lastUpdateTime) {

        AuthWrapper authWrapper = new AuthWrapper(sessionManager.getUser());
        StringBuffer sb = new StringBuffer(RESTDefaults.REST_API_RESOURCE_ATTRIBUTES_TRAPTYPES + authWrapper.createGetAuthString());

        sb.append(encodeKeyValue(RESTDefaults.REST_PARAMETER_ATTRIBUTES_LAST_MODIFIED_DATE, String.valueOf(lastUpdateTime)));

        return sb.toString();
    }


    public String postAddNewTrap() {

        AuthWrapper authWrapper = new AuthWrapper(sessionManager.getUser());
        StringBuffer sb = new StringBuffer(RESTDefaults.REST_API_RESOURCE_TRAP+"?" + authWrapper.createGetAuthString());
        return sb.toString();
    }


    public String postVoteOnTrap(Trap trap) {

        AuthWrapper authWrapper = new AuthWrapper(sessionManager.getUser());
        StringBuffer sb = new StringBuffer(RESTDefaults.REST_API_RESOURCE_TRAP);
        sb.append("/"+trap.getId()+"?");
        sb.append(authWrapper.createGetAuthString());
        return sb.toString();
    }


    public String postRegisterUser(String userName) {

        AuthWrapper authWrapper = new AuthWrapper(sessionManager.getUser());
        StringBuffer sb = new StringBuffer(RESTDefaults.REST_API_RESOURCE_USER);
        sb.append(userName+"?");
       // sb.append(authWrapper.createGetAuthString());
        return sb.toString();
    }

    public String deleteTrapRequest(Trap trap) {

        AuthWrapper authWrapper = new AuthWrapper(sessionManager.getUser());
        StringBuffer sb = new StringBuffer(RESTDefaults.REST_API_RESOURCE_TRAP);
        sb.append("/"+trap.getId()+"?");
        sb.append(authWrapper.createGetAuthString());
        return sb.toString();

    }


    public String putMergeFacebook(User user, String token) {

        /*AuthWrapper authWrapper = new AuthWrapper(user);
        StringBuffer sb = new StringBuffer(RESTDefaults.REST_API_RESOURCE_USER_FACEBOOK);
        sb.append("?token="+token+"&");
        sb.append(authWrapper.createGetAuthString());*/


        return gson.toJson(new RestRequest(new RequestWrapper(new AuthWrapper(user),
                new MetaWrapper(),new FacebookRegisterWrapper(token,user.getUserName()))));
    }


    private static class RestRequest
    {
        @SerializedName(RESTDefaults.REST_PARAMETER_REQUEST)
        RequestWrapper request;

        RestRequest(RequestWrapper requestWrapper)
        {
            request = requestWrapper;
        }
    }
}
