package com.trapster.android.comms.php;

import java.io.InputStream;

import javax.xml.parsers.SAXParserFactory;

import com.trapster.android.comms.CategoryListener;
import com.trapster.android.comms.php.handler.CategoriesHandler;
import com.trapster.android.util.TrapsterError;

public class CategoryParser extends TrapsterStreamParser
{
	static final String LOGNAME= "Trapster.CategoryParser";
	//
	CategoryListener listener;
	//
	public CategoryParser(CategoryListener listener)
	{
		this.listener= listener;
	}
	public void parse(InputStream in)
	{
		try
		{
			if (listener != null)
			{
				CategoriesHandler handler= new CategoriesHandler(listener);
				SAXParserFactory.newInstance().newSAXParser().parse(in, handler);
				listener.onCategories(handler.getCategories());
			}
		} catch (Exception e)
		{
			if (listener != null)
				listener.onError(new TrapsterError(TrapsterError.TYPE_INVALID_XML_FORMAT,
						" Unable to import categories:" + e.getMessage()));
		}
	}
}
