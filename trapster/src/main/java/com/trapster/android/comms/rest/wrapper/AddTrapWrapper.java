package com.trapster.android.comms.rest.wrapper;

import com.google.gson.annotations.SerializedName;
import com.trapster.android.model.Trap;

public class AddTrapWrapper {

    @SerializedName("latitude")
    private double latitude;

    @SerializedName("longitude")
    private double longitude;

    @SerializedName("reporteddirection")
    private int heading;

    @SerializedName("traptypeids")
    private int[] trapTypeIds = new int[1];



    public AddTrapWrapper(Trap trap, int heading)
    {
        this.latitude = trap.getLatitude();
        this.longitude = trap.getLongitude();
        this.trapTypeIds[0] = trap.getTypeid();
        this.heading = heading;

    }

}
