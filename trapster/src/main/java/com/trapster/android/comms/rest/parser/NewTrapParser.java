package com.trapster.android.comms.rest.parser;


import com.trapster.android.comms.NewTrapListener;
import com.trapster.android.model.Trap;
import com.trapster.android.util.Log;
import com.twolinessoftware.android.framework.service.comms.JsonParser;

import org.json.JSONObject;

/**
 * Created by John on 5/28/13.
 */
public class NewTrapParser extends JsonParser {

    private static final String LOGNAME = "NewTrapParser";
    private final NewTrapListener listener;
    private Trap trap;

    public NewTrapParser(Trap trap, NewTrapListener listener)
    {
        super();
        this.trap = trap;
        this.listener= listener;
    }

    @Override
    public void parseJson(Status status, String jsonStream) {


       try{
           JSONObject jsonResponseObject = new JSONObject(jsonStream);
           int trapId = Integer.parseInt(jsonResponseObject.get("id").toString());
           trap.setId(trapId);


       }catch(Exception e){
           Log.e(LOGNAME,"Unable to parse json:"+e.getMessage());
       }

        if (listener != null)
            listener.onNewTrapAdded(trap, status.message);

    }



}
