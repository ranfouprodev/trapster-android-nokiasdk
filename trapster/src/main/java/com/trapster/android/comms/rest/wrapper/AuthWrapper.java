package com.trapster.android.comms.rest.wrapper;

import android.os.Build;

import com.google.gson.annotations.SerializedName;
import com.trapster.android.BuildFeatures;
import com.trapster.android.Defaults;
import com.trapster.android.RESTDefaults;
import com.trapster.android.model.User;

import java.net.URLEncoder;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 5/29/13
 * Time: 9:12 AM
 * To change this template use File | Settings | File Templates.
 */
public class AuthWrapper {

    @SerializedName(RESTDefaults.REST_PARAMETER_AUTH_APP_CAPABILITIES)
    String appCapabilities = RESTDefaults.REST_APP_CAPABILITIES;

    @SerializedName(RESTDefaults.REST_PARAMETER_AUTH_APP_ID)
    String appId = RESTDefaults.REST_APP_APPID;

    @SerializedName(RESTDefaults.REST_PARAMETER_AUTH_APP_KEY)
    String appKey = BuildFeatures.TRAPSTERKEY;

    @SerializedName(RESTDefaults.REST_PARAMETER_AUTH_DEVICE_ID)
    String deviceID = BuildFeatures.getDeviceId();

    @SerializedName(RESTDefaults.REST_PARAMETER_AUTH_DEVICE_TYPE)
    String deviceType = BuildFeatures.getDeviceType();

    @SerializedName(RESTDefaults.REST_PARAMETER_AUTH_USER_NAME)
    String userName;

    @SerializedName(RESTDefaults.REST_PARAMETER_AUTH_PASSWORD_HASH)
    String password;

    public AuthWrapper(User user)
    {
        if (user == null || user.getUserName() == null
                || user.getPassword() == null) {
            user = new User();
            user.setCredentials(Defaults.DEMO_USERNAME,Defaults.DEMO_PASSWORD);
        }

        userName = user.getUserName();
        password = user.getPassword();
    }



    public String createGetAuthString()
    {
        StringBuffer sb = new StringBuffer();

        sb.append(encodeKeyValue(RESTDefaults.REST_PARAMETER_AUTH_DEVICE_ID, deviceID) + "&");
        sb.append(encodeKeyValue(RESTDefaults.REST_PARAMETER_META_OPERATING_SYSTEM, Build.VERSION.RELEASE) + "&");
        sb.append(encodeKeyValue(RESTDefaults.REST_PARAMETER_AUTH_DEVICE_TYPE, deviceType) + "&");
        sb.append(encodeKeyValue(RESTDefaults.REST_PARAMETER_AUTH_PASSWORD_HASH, password) + "&");
        sb.append(encodeKeyValue(RESTDefaults.REST_PARAMETER_AUTH_USER_NAME, userName) + "&");
        sb.append(encodeKeyValue(RESTDefaults.REST_PARAMETER_AUTH_APP_CAPABILITIES, appCapabilities) + "&");
        sb.append(encodeKeyValue(RESTDefaults.REST_PARAMETER_AUTH_APP_KEY, appKey) + "&");
        sb.append(encodeKeyValue(RESTDefaults.REST_PARAMETER_AUTH_APP_ID, appId) + "&");

        return sb.toString();
    }

    private String encodeKeyValue (String key, String value )
    {
        String encoded = "";
        try
        {
            encoded = key + "=" + URLEncoder.encode(value, "utf-8");
        }
        catch (Exception e){}

        return encoded;
    }
}
