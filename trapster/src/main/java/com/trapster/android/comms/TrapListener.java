package com.trapster.android.comms;

import com.trapster.android.model.Traps;

public interface TrapListener extends ErrorListener {
	void onTraps(Traps traps);
}
