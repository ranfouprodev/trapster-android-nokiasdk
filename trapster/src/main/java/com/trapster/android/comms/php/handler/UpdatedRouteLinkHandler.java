package com.trapster.android.comms.php.handler;

import com.trapster.android.PHPDefaults;
import com.trapster.android.comms.ErrorListener;
import com.trapster.android.model.UpdatedRouteLinkInfo;
import org.xml.sax.Attributes;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 12/8/12
 * Time: 2:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class UpdatedRouteLinkHandler extends TrapsterHandler {

    private ArrayList<UpdatedRouteLinkInfo> updatedRouteLinkInfos = new ArrayList<UpdatedRouteLinkInfo>();
    private UpdatedRouteLinkInfo updatedRouteLinkInfo;

    public UpdatedRouteLinkHandler(ErrorListener listener) {
        super(listener);
    }

    public ArrayList<UpdatedRouteLinkInfo> getRouteLinkInfo()
    {
        return updatedRouteLinkInfos;
    }

    protected void onStartElement(String uri, String localName, String qName, Attributes attributes)
    {
        if (PHPDefaults.XML_RSP_ATTRIB_LINK.equals(localName))
            updatedRouteLinkInfo = new UpdatedRouteLinkInfo();
        else if (updatedRouteLinkInfo != null)
            buffer= new StringBuilder();
    }

    protected void onEndElement(String uri, String localName, String qName)
    {
        if (PHPDefaults.XML_RSP_ATTRIB_LINKID.equals(localName))
        {
            updatedRouteLinkInfo.setRouteLink(buffer.toString());
            buffer = null;
        }
        else if (PHPDefaults.XML_RSP_ATTRIB_SPEED.equals(localName))
        {
            updatedRouteLinkInfo.setSpeedLimit(Double.valueOf(buffer.toString()));
            buffer = null;
        }
        else if (PHPDefaults.XML_RSP_ATTRIB_LINK.equals(localName))
        {
            updatedRouteLinkInfos.add(updatedRouteLinkInfo);
            updatedRouteLinkInfo = null;
        }
    }
}
