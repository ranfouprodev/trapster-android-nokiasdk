package com.trapster.android.comms.php;

import com.trapster.android.PHPDefaults;
import com.trapster.android.comms.ErrorListener;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.service.comms.Parser;

import java.io.InputStream;

public abstract class TrapsterParser extends Parser
{
	private static final String LOGNAME = "TrapsterParser";
	ErrorListener listener;
	

	public TrapsterParser(ErrorListener listener) {
		this.listener = listener; 
	}
	
	public void process(InputStream stream)
	{
		String xml = convertStreamToString(stream);
			
		// Check for errors
		// If there is no error parse the xml
		if (!checkForErrors(xml))
		{
			parseXml(xml);
		}
	}
	
	public abstract void parseXml(String xml);
	
	
	private boolean checkForErrors(String xml){
		boolean error = false; 
		// Check for other than xml responses
		String response = getNodesFromXml(xml, PHPDefaults.XML_TRAPSTER);
		
		if(response == null){
				error = true;
				TrapsterError e = new TrapsterError(TrapsterError.TYPE_COMMUNICATION_ERROR,"Error Communicating With Server");
				if(listener != null)
					listener.onError(e);
		}else{
		
		
			// Get the Error Tag only
			String errorResponse = getNodesFromXml(xml, PHPDefaults.XML_RSP_ERROR);
			
			if(errorResponse != null){
				error = true;
				String message = getTextFromNode(errorResponse, PHPDefaults.XML_RSP_ERROR);
			
				TrapsterError e = new TrapsterError(TrapsterError.TYPE_API_ERROR,message);
				if(listener != null)
					listener.onError(e);
			}
		
		}
		
		return error; 
	}
	
	protected String getNodesFromXml(String xml, String tag){
		
		String match = "<" + tag; 
		String response = null;
		
				
		if(xml.contains(match+" ") || xml.contains(match+">")){
		
		String startTag = (xml.contains(match+" ")) ? "<"+tag+" " :"<"+tag+">";	
			
		//String startTag = "<"+tag; // space is to prevent subwords i.e. car and cars would both be true
		String endTag = "</"+tag;
		
			try{
				response = xml.substring(xml.indexOf(startTag),xml.indexOf(endTag)+endTag.length()+1);
			}catch(StringIndexOutOfBoundsException se){
{/* 				Log.i(LOGNAME, "Parser:Invalid or missing XML Tag:"+tag); */}
			}
		}
		return response; 
		
	}
	
	protected String getTextFromNode(String xml, String tag){
		String startTag = "<"+tag+">";
		String endTag = "</"+tag+">";
		
		String response = null; 
		
		try{
			response = xml.substring(xml.indexOf(startTag)+startTag.length(),xml.indexOf(endTag));
		}catch(StringIndexOutOfBoundsException se){
{/* 			//Log.d(LOGNAME, "Parser:Invalid or missing XML Tag:"+tag); */}
		}
		
		return response; 
		
	}
	
}
