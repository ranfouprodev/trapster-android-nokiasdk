package com.trapster.android.comms.rest.parser;

import com.trapster.android.comms.EmailAvailableListener;
import com.trapster.android.comms.rest.StatusCode;
import com.twolinessoftware.android.framework.service.comms.JsonParser;

/**
 * Created by John on 8/20/13.
 */
public class EmailAvailableParser extends JsonParser {

    private final EmailAvailableListener listener;
    private final String email;


    public EmailAvailableParser(String email, EmailAvailableListener listener) {
        super();
        this.listener = listener;
        this.email  = email;
    }


    @Override
    public void parseJson(Status status, String jsonString) {

        switch (StatusCode.getByCode(status.statusCode)) {
            case OK:

                UserDetails details = gson.fromJson(jsonString,UserDetails.class);

                if(listener != null)
                    listener.onEmailAvailable(email, details.displayname,details.id,false);
                break;

            // Any error assumes that the name isn't available. Should we be checking for only 404 response codes?
            default:
                if(listener != null)
                    listener.onEmailAvailable(email,null,0,true);
        }


    }

    @Override
    public boolean handleHttpStatusErrors(Status status) {
        if(status.statusCode == 404){
            if(listener != null)
                listener.onEmailAvailable(email,null,0,true);
            return true;
        }
        return false;
    }

    private static class UserDetails{
        String displayname;
        long id;
    }
}
