package com.trapster.android.comms.php;

import java.io.InputStream;
import javax.xml.parsers.SAXParserFactory;

import android.util.Log;

import com.trapster.android.comms.AttributesListener;
import com.trapster.android.comms.php.handler.AttributesHandler;
import com.trapster.android.util.TrapsterError;

public class AttributesParser extends TrapsterStreamParser
{
	static final String LOGNAME= "Trapster.AttributesParser";
	//
	AttributesListener listener;
	//
	public AttributesParser(AttributesListener listener)
	{
		this.listener= listener;
	}
	public void parse(InputStream in)
	{
		
		try
		{
			if (listener != null)
			{
				AttributesHandler handler= new AttributesHandler(listener);
				SAXParserFactory.newInstance().newSAXParser().parse(in, handler);
				listener.onAttributes(handler.getTrapsterResources());
			}
		} catch (Exception e)
		{
{/* 			//Log.e(LOGNAME, "Attributes Parser:"+Log.getStackTraceString(e)); */}
			
			if (listener != null)
				listener.onError(new TrapsterError(TrapsterError.TYPE_INVALID_XML_FORMAT,
						" Unable to import attributes:" + e.getMessage()));
		}
	}
}
