package com.trapster.android.comms.php.handler;

import com.google.inject.Inject;
import com.trapster.android.PHPDefaults;
import com.trapster.android.TrapsterApplication;
import com.trapster.android.comms.TrapListener;
import com.trapster.android.manager.TrapManager;
import com.trapster.android.model.Trap;
import com.trapster.android.model.TrapType;
import com.trapster.android.model.Traps;
import org.xml.sax.Attributes;
import roboguice.RoboGuice;


public class TrapsHandler extends TrapsterHandler
{
	
	@Inject TrapManager trapManager; 
	
	Traps traps= new Traps();
	Trap trap;
	private float lat;
	private float lng;
	//
	public TrapsHandler(TrapListener listener)
	{
		super(listener);
		RoboGuice.injectMembers(TrapsterApplication.getAppInstance(), this);
	}
	public Traps getTraps()
	{
		return traps;
	}
	protected void onEndElement(String uri, String localName, String qName)
	{
		if (PHPDefaults.XML_RSP_MARKER.equals(localName))
		{
			traps.getTraps().add(trap);
			trap= null;
		} else if (PHPDefaults.XML_RSP_TRAP_ID.equals(localName))
		{
			trap.setId(Integer.valueOf(buffer.toString()));
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_LAT.equals(localName))
		{
			//trap.setLat(Double.valueOf(buffer.toString()));
			lat = Float.valueOf(buffer.toString());
			if(lat != 0 && lng != 0){
				trap.setGeometry(lat, lng);
				lat = 0f; 
				lng = 0f;
			}
			
			
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_LON.equals(localName))
		{
			//trap.setLng(Double.valueOf(buffer.toString()));
			lng = Float.valueOf(buffer.toString());
			if(lat != 0 && lng != 0){
				trap.setGeometry(lat, lng);
				lat = 0f; 
				lng = 0f;
			}
			
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_TYPE_ID.equals(localName))
		{
			int typeId = Integer.valueOf(buffer.toString()); 
			trap.setTypeid(typeId);
			
			TrapType type = trapManager.getTrapType(typeId);
			
			if(type != null){
				trap.setExpiry(System.currentTimeMillis() + (type.getLifetime()*1000)); 	
			}else{
				trap.setExpiry(Long.MAX_VALUE);
			}
			
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_NUM.equals(localName))
		{
			trap.setNum(Integer.valueOf(buffer.toString()));
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_OUNAME.equals(localName))
		{
			trap.setOuname(buffer.toString());
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_LUNAME.equals(localName))
		{
			trap.setLuname(buffer.toString());
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_LVOTE.equals(localName))
		{
			trap.setLvote(buffer.toString());
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_LVOTE_SEC.equals(localName))
		{
			trap.setLvoteSec(Long.valueOf(buffer.toString()));
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_ADDRESS.equals(localName))
		{
			trap.setAddress(buffer.toString());
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_LEVEL.equals(localName))
		{
			trap.setLevel(Integer.valueOf(buffer.toString()));
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_BADGEKEY.equals(localName))
		{
			trap.setBadgeKey(buffer.toString());
			buffer= null;
		}
	}
	protected void onStartElement(String uri, String localName, String qName, Attributes attributes)
	{
		if (PHPDefaults.XML_RSP_MARKER.equals(localName))
			trap= new Trap();
		else if (trap != null)
			buffer= new StringBuilder();
	}
}
