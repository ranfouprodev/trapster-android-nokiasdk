package com.trapster.android.comms.rest.wrapper;

import com.google.gson.annotations.SerializedName;

public class VoteTrapWrapper {

    @SerializedName("vote")
    private String vote;

    public VoteTrapWrapper(boolean agree)
    {
        this.vote = (agree)?"Y":"N";
    }

}
