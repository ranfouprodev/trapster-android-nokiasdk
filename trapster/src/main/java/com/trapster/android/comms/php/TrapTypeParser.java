package com.trapster.android.comms.php;

import com.trapster.android.comms.TrapTypeListener;
import com.trapster.android.comms.php.handler.TrapTypesHandler;
import com.trapster.android.model.TrapType;
import com.trapster.android.util.TrapsterError;

import javax.xml.parsers.SAXParserFactory;
import java.io.InputStream;
import java.util.List;

public class TrapTypeParser extends TrapsterStreamParser
{
	static final String LOGNAME= "Trapster.TrapTypeParser";

    private final static int KNOWN_ENFORCEMENT_POINT_ID = 3;
	//
	TrapTypeListener listener;
	//
	public TrapTypeParser(TrapTypeListener listener)
	{
		this.listener= listener;
	}
	public void parse(InputStream in)
	{
		try
		{
			if (listener != null)
			{
				TrapTypesHandler handler= new TrapTypesHandler(listener);
				SAXParserFactory.newInstance().newSAXParser().parse(in, handler);

                List<TrapType> trapTypes = handler.getTrapTypes().getTypes();

                for (TrapType trapType : trapTypes)
                {
                    if (trapType.getId() == KNOWN_ENFORCEMENT_POINT_ID)
                    {
                        trapType.setAlertable(false);
                        break;
                    }

                }
				listener.onTrapTypes(handler.getTrapTypes());
			}
		} catch (Exception e)
		{
			if (listener != null)
				listener.onError(new TrapsterError(TrapsterError.TYPE_INVALID_XML_FORMAT,
						" Unable to import trap types:" + e.getMessage()));
		}
	}
}
