package com.trapster.android.comms.rest.wrapper;

import com.google.gson.annotations.SerializedName;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 5/29/13
 * Time: 10:25 AM
 * To change this template use File | Settings | File Templates.
 */
public class FacebookRegisterWrapper {

    @SerializedName("token")
    String token;

    @SerializedName ("login")
    String login;

    public FacebookRegisterWrapper(String token, String login) {
        this.token = token;
        this.login = login;
    }
}
