package com.trapster.android.comms;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;
import com.twolinessoftware.android.framework.service.comms.Parser;
import com.twolinessoftware.android.framework.service.comms.Worker;

import java.util.LinkedList;
import java.util.concurrent.*;

/**
 * Main Communication Controller
 * 
 * @TODO move asyncworker to ExecutorService for thread pooling
 */
@Singleton
public class CommunicationManager implements CommunicationStatusListener {

	private static final String LOGNAME = "CommunicationManager";

	public static final int ERASE_FIRST = 0;
	public static final int ERASE_LAST = 1;
	public static final String NODATACONNECTION = "nodataconnection";

	private static final int CORE_POOL_SIZE = 10;
	private static final int MAXIMUM_POOL_SIZE = 20;
	private static final int KEEP_ALIVE = 10; // seconds


	@Inject
	Application application;
	
	@Inject ConnectivityManager cm; 
	
	/*
	 * This runs the comms requests as FIFO. Performance optimization may want to prioritize different calls
	 */
	private static final BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<Runnable>();

	private LinkedList<Worker> pauseQueue = new LinkedList<Worker>(); 
	
    private boolean hasDataConnection;

	private ThreadPoolExecutor asyncWorker;

	public CommunicationManager() {

	 
	}
	
	
	@Inject
	public void init() {
	
		asyncWorker = new ThreadPoolExecutor(CORE_POOL_SIZE,MAXIMUM_POOL_SIZE, KEEP_ALIVE, TimeUnit.SECONDS, workQueue, new BlockPolicy());
	
        hasDataConnection = (cm.getActiveNetworkInfo() != null);
	}

	private CommunicationStatusListener statusListener;
	private int connections;

	public boolean hasDataConnection(Context context) {
		return hasDataConnection; 
	}

	public void handleAsyncMessage(Worker worker, Parser parser,
			CommunicationStatusListener listener,
			boolean addToQueueOnNoConnection) {

	    hasDataConnection = (cm.getActiveNetworkInfo() != null);
		
	    worker.setContext(application.getApplicationContext());
		
		if (parser != null)
			worker.setParser(parser);

		if (listener != null)
			worker.addStatusListener(listener);

		worker.addStatusListener(this);
	    
		if (!hasDataConnection) {

			if (listener != null)
				listener.onConnectionError(NODATACONNECTION);

			if (statusListener != null)
				statusListener.onConnectionError(NODATACONNECTION);

			if (!addToQueueOnNoConnection)
				return;
			else{
{/* 				//Log.d(LOGNAME, "Caching Http Request:"+worker.getClass().getCanonicalName()); */}

                synchronized (pauseQueue)
                {
                    pauseQueue.addLast(worker);
                }

			}
		}else{
			
			
			
			asyncWorker.submit(worker);

		}
	}
	
	public void clearPausedQueue(){
		
		Worker worker = null;
        synchronized (pauseQueue)
        {
            while((worker = pauseQueue.poll()) != null){
                asyncWorker.submit(worker);
            }
        }

	}
	

	/*
	 * Added context for connectivity checking
	 */
	public void handleAsyncMessage(Worker worker, Parser parser,
			CommunicationStatusListener listener) {

		handleAsyncMessage(worker, parser, listener, true);
	}


	public void clearAsyncQueue() {
		asyncWorker.shutdown();
	}

	public void stop() {
		clearAsyncQueue();
	}

	@Override
	public void onCloseConnection() {
		connections--;
		if (connections <= 0) {
			connections = 0;
			if (statusListener != null)
				statusListener.onCloseConnection();
		}
	}

	@Override
	public void onConnectionError(String errorMessage) {
		connections--;
		if (connections <= 0) {
			connections = 0;
			if (statusListener != null)
				statusListener.onCloseConnection();
		}
	}

	@Override
	public void onOpenConnection() {
		if (connections == 0) {
			if (statusListener != null)
				statusListener.onOpenConnection();
		}

		connections++;
	}

	public void setGlobalCommunicationStatusListener(
			CommunicationStatusListener listener) {
		this.statusListener = listener;
	}
	
	public void removeGlobalCommunicationStatusListener(){
		this.statusListener = null; 
	}

	public boolean hasOpenConnections() {
		return connections > 0;
	}

	/**
	 * A handler for rejected tasks that will have the caller block until
	 * space is available.
	 */
	public static class BlockPolicy implements RejectedExecutionHandler {

	    /**
	     * Creates a <tt>BlockPolicy</tt>.
	     */
	    public BlockPolicy() { }

	    /**
	     * Puts the Runnable to the blocking queue, effectively blocking
	     * the delegating thread until space is available.
	     * @param r the runnable task requested to be executed
	     * @param e the executor attempting to execute this task
	     */
	    public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
	        try {
	            e.getQueue().put( r );
	        }
	        catch (InterruptedException e1) {
{/* 	            //Log.e(LOGNAME, "Work discarded, thread was interrupted while waiting for space to schedule: {}" ); */}
	        }
	    }
	}
	
	
	
}
