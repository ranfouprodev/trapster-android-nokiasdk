package com.trapster.android.comms.php.handler;

import com.trapster.android.PHPDefaults;
import com.trapster.android.comms.CategoryListener;
import com.trapster.android.model.Categories;
import com.trapster.android.model.Category;
import org.xml.sax.Attributes;

public class CategoriesHandler extends TrapsterHandler
{
	Categories categories= new Categories();
	Category category;
	//
	public CategoriesHandler(CategoryListener listener)
	{
		super(listener);
	}
	public Categories getCategories()
	{
		return categories;
	}
	protected void onEndElement(String uri, String localName, String qName)
	{
		if (PHPDefaults.XML_RSP_CATEGORY.equals(localName))
		{
			categories.getCategories().add(category);
			category= null;
		} else if (PHPDefaults.XML_RSP_CATEGORY_NAME.equals(localName))
		{
			category.setName(buffer.toString());
			buffer= null;
		} else if (PHPDefaults.XML_RSP_CATEGORY_ID.equals(localName))
		{
			category.setId(Integer.valueOf(buffer.toString()));
			buffer= null;
		}
        else if (PHPDefaults.XML_RSP_CATEGORY_SEQUENCE.equals(localName))
        {
            category.setSequence(Integer.valueOf(buffer.toString()));
            buffer= null;
        }
	}
	protected void onStartElement(String uri, String localName, String qName, Attributes attributes)
	{
		if (PHPDefaults.XML_RSP_CATEGORY.equals(localName))
			category= new Category();
		else if (category != null)
			buffer= new StringBuilder();
	}
}
