package com.trapster.android.comms.nlp.parser;

import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.trapster.android.NLPDefaults;
import com.trapster.android.comms.TrafficRouteLinkListener;
import com.trapster.android.model.RouteLinks;
import com.trapster.android.util.TrapsterError;
import org.json.JSONObject;

import java.lang.reflect.Modifier;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 9/24/13
 * Time: 3:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class TrafficRouteLinkParser extends NLPParser {

    private static final String LOGNAME = "TrafficRouteLinkParser";
    private final GsonBuilder builder;
    private final Gson gson;
    private final TrafficRouteLinkListener listener;
    private final String quadkey;


    public TrafficRouteLinkParser(TrafficRouteLinkListener listener, String quadkey){

        this.listener = listener;
        this.quadkey = quadkey;

        builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.TRANSIENT);
        //See notes below. Once 2.3 is released this needs to be enabled
        // builder.registerTypeAdapter(boolean.class, new BooleanSerializer());
        gson = builder.create();
    }

    @Override
    public void parseJson(String jsonString) {
        try
        {
            JSONObject jsonResponseObject = new JSONObject(jsonString);
            String responseString = jsonResponseObject.get(NLPDefaults.NLP_RESPONSE_RESPONSE).toString();

            RouteLinks routelinks = gson.fromJson(responseString,RouteLinks.class);

            if(listener != null)
                listener.onTrafficUpdate(routelinks, quadkey);
        } catch (Exception e) {
            if(listener != null)
                listener.onError(new TrapsterError(TrapsterError.TYPE_INVALID_JSON_FORMAT,e.getMessage()));

        }
    }
}

