package com.trapster.android.comms.php.handler;

import com.trapster.android.PHPDefaults;
import com.trapster.android.comms.TrapTypeListener;
import com.trapster.android.model.TrapIcon;
import com.trapster.android.model.TrapLevel;
import com.trapster.android.model.TrapType;
import com.trapster.android.model.TrapTypes;

import org.xml.sax.Attributes;

public class TrapTypesHandler extends TrapsterHandler
{
	TrapTypes trapTypes= new TrapTypes();
	TrapType trapType;
	TrapIcon trapIcon;
	TrapLevel trapLevel;
	//
	public TrapTypesHandler(TrapTypeListener listener)
	{
		super(listener);
	}
	public TrapTypes getTrapTypes()
	{
		return trapTypes;
	}
	protected void onEndElement(String uri, String localName, String qName)
	{
		if (PHPDefaults.XML_RSP_TRAP_TYPE.equals(localName))
		{
			trapTypes.getTypes().add(trapType);
			trapType= null;
		} else if (PHPDefaults.XML_RSP_TRAP_TYPE_CATEGORY.equals(localName))
		{
			trapType.setCategoryId(Integer.valueOf(buffer.toString()));
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_TYPE_NAME.equals(localName))
		{
			trapType.setName(buffer.toString());
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_TYPE_DESCRIPTION.equals(localName))
		{
			trapType.setDescription(buffer.toString());
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_TYPE_ICONS.equals(localName))
		{
			trapType.setIcon(trapIcon);
			trapIcon= null;
		} else if (PHPDefaults.XML_RSP_TRAP_TYPE_ALT2.equals(localName))
		{
			trapIcon.setAlt2(buffer.toString());
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_TYPE_ALT1.equals(localName))
		{
			trapIcon.setAlt1(buffer.toString());
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_TYPE_BASE.equals(localName))
		{
			trapIcon.setBase(buffer.toString());
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_TYPE_REPORTABLE.equals(localName))
		{
			trapType.setReportable("y".equalsIgnoreCase(buffer.toString()));
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_TYPE_ALERTABLE.equals(localName))
		{
			trapType.setAlertable("y".equalsIgnoreCase(buffer.toString()));
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_TYPE_VOTABLE.equals(localName))
		{
			trapType.setVotable("y".equalsIgnoreCase(buffer.toString()));
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_TYPE_AUDIOID.equals(localName))
		{
			trapType.setAudioid(buffer.toString());
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_TYPE_ANNOUNCES.equals(localName))
		{
			trapType.setAnnounces("y".equalsIgnoreCase(buffer.toString()));
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_TYPE_NOTIFIABLE.equals(localName))
		{
			trapType.setNotifiable("y".equalsIgnoreCase(buffer.toString()));
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_TYPE_TYPE_ID.equals(localName))
		{
			trapType.setId(Integer.valueOf(buffer.toString()));
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_TYPE_LIFETIME.equals(localName))
		{
			trapType.setLifetime(Long.valueOf(buffer.toString()));
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_TYPE_LEVEL.equals(localName))
		{
			trapType.getLevels().add(trapLevel);
			trapLevel= null;
		} else if (PHPDefaults.XML_RSP_TRAP_TYPE_BIT.equals(localName))
		{
			trapLevel.setBit(Integer.valueOf(buffer.toString()));
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_TYPE_LEVEL_NAME.equals(localName))
		{
			trapLevel.setLevelName(buffer.toString());
			buffer= null;
		} else if (PHPDefaults.XML_RSP_TRAP_TYPE_ICON.equals(localName))
		{
			trapLevel.setIcon(buffer.toString());
			buffer= null;
		}
	}
	protected void onStartElement(String uri, String localName, String qName, Attributes attributes)
	{

   		if (PHPDefaults.XML_RSP_TRAP_TYPE.equals(localName))
			trapType= new TrapType();
		else if (PHPDefaults.XML_RSP_TRAP_TYPE_ICONS.equals(localName))
			trapIcon= new TrapIcon();
		else if (PHPDefaults.XML_RSP_TRAP_TYPE_LEVEL.equals(localName))
			trapLevel= new TrapLevel();
		else if (trapType != null || trapIcon != null || trapLevel != null)
			buffer= new StringBuilder();
	}
}
