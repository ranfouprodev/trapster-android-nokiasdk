package com.trapster.android.comms.php.handler;

import com.trapster.android.PHPDefaults;
import com.trapster.android.comms.AttributesListener;
import com.trapster.android.model.*;
import org.xml.sax.Attributes;


public class AttributesHandler extends TrapsterHandler
{
	TrapsterResources trapsterResources= new TrapsterResources();
	ColorResource colorResource;
	ImageResource imageResource;
	ImageSpec imageSpec;
	StringResource stringResource;
	//
	public AttributesHandler(AttributesListener listener)
	{
		super(listener);
	}
	public TrapsterResources getTrapsterResources()
	{
		return trapsterResources;
	}
	protected void onEndElement(String uri, String localName, String qName)
	{
		if (PHPDefaults.XML_RSP_ATTRIB_KEY.equals(localName))
		{
			if (colorResource != null)
				colorResource.setKey(buffer.toString());
			else if (imageResource != null)
				imageResource.setKey(buffer.toString());
			else
				stringResource.setKey(buffer.toString());
			buffer= null;
		} else if (PHPDefaults.XML_RSP_ATTRIB_COLOR.equals(localName))
		{
			trapsterResources.getColors().add(colorResource);
			colorResource= null;
		} else if (PHPDefaults.XML_RSP_ATTRIB_RGBA.equals(localName))
		{
			colorResource.setRgba(buffer.toString());
			buffer= null;
		} else if (PHPDefaults.XML_RSP_ATTRIB_IMAGE.equals(localName))
		{
			trapsterResources.getImages().add(imageResource);
			imageResource= null;
		} else if (PHPDefaults.XML_RSP_ATTRIB_PATH.equals(localName))
		{
			imageResource.setPath(buffer.toString());
			buffer= null;
		} else if (PHPDefaults.XML_RSP_ATTRIB_SPEC.equals(localName))
		{
			imageResource.setSpec(imageSpec);
			imageSpec= null;
		} else if (PHPDefaults.XML_RSP_ATTRIB_WIDTH.equals(localName))
		{
			imageSpec.setWidth(Integer.valueOf(buffer.toString()));
			buffer= null;
		} else if (PHPDefaults.XML_RSP_ATTRIB_HEIGHT.equals(localName))
		{
			imageSpec.setHeight(Integer.valueOf(buffer.toString()));
			buffer= null;
		} else if (PHPDefaults.XML_RSP_ATTRIB_PINX.equals(localName))
		{
			imageSpec.setPinx(Integer.valueOf(buffer.toString()));
			buffer= null;
		} else if (PHPDefaults.XML_RSP_ATTRIB_PINY.equals(localName))
		{
			imageSpec.setPiny(Integer.valueOf(buffer.toString()));
			buffer= null;
		} else if (PHPDefaults.XML_RSP_ATTRIB_STRING.equals(localName))
		{
			trapsterResources.getStrings().add(stringResource);
			stringResource= null;
		} else if (PHPDefaults.XML_RSP_ATTRIB_TEXT.equals(localName))
		{
			stringResource.setText(buffer.toString());
			buffer= null;
		}
	}
	protected void onStartElement(String uri, String localName, String qName, Attributes attributes)
	{
		if (PHPDefaults.XML_RSP_ATTRIB_COLOR.equals(localName))
			colorResource= new ColorResource();
		else if (PHPDefaults.XML_RSP_ATTRIB_IMAGE.equals(localName))
			imageResource= new ImageResource();
		else if (PHPDefaults.XML_RSP_ATTRIB_SPEC.equals(localName))
			imageSpec= new ImageSpec();
		else if (PHPDefaults.XML_RSP_ATTRIB_STRING.equals(localName))
			stringResource= new StringResource();
		else if (colorResource != null || imageResource != null || imageSpec != null || stringResource != null)
			buffer= new StringBuilder();
	}
}
