package com.trapster.android;

public class RESTDefaults
{
    // Application specific values
    public static final String REST_APP_APPID = "TPST-AND-" + Defaults.getLanguage()+"-" + BuildFeatures.VERSIONNAME;
    public static final String REST_APP_CAPABILITIES = "3";


    private static boolean USE_SCOURGE = true; // Set to false for production

    private static final String REST_BASE_URL; // Change this to affect the URL path for all outgoing communication
    private static final String REST_SECURE_APIURL;
    private static final String BASE_URL;

    static
    {
        if (USE_SCOURGE)
        {
            BASE_URL = "trap-scourge.com/";

            REST_SECURE_APIURL = "http://";
        }
        else
        {
            BASE_URL = "trapster.com/";
            REST_SECURE_APIURL = "https://";
        }

        REST_BASE_URL = "api." + BASE_URL;
    }

    private static final String REST_API_VERSION = "2/";
    private static final String REST_SUBDOMAIN_PROBE = "probe.";
    private static final String REST_SUBDOMAIN_USER = "user.";
    private static final String REST_SUBDOMAIN_TRAP = "trap.";
    private static final String REST_SUBDOMAIN_ATTRIBUTES = "attributes.";


    // REST Service Endpoints
    private static final String REST_API_ENDPOINT_PROBE = REST_SECURE_APIURL + REST_SUBDOMAIN_PROBE + REST_BASE_URL + REST_API_VERSION;
    private static final String REST_API_ENDPOINT_ATTRIBUTES = REST_SECURE_APIURL + REST_SUBDOMAIN_ATTRIBUTES + REST_BASE_URL + REST_API_VERSION;
    private static final String REST_API_ENDPOINT_TRAP = REST_SECURE_APIURL + REST_SUBDOMAIN_TRAP + REST_BASE_URL + REST_API_VERSION;
    private static final String REST_API_ENDPOINT_USER = REST_SECURE_APIURL + REST_SUBDOMAIN_USER + REST_BASE_URL + REST_API_VERSION;

    // REST Resources
    public static final String REST_API_RESOURCE_PROBE = REST_API_ENDPOINT_PROBE + "probe";
    public static final String REST_API_RESOURCE_PATROL = REST_API_ENDPOINT_PROBE + "patrol.json?";
    public static final String REST_API_RESOURCE_ATTRIBUTES = REST_API_ENDPOINT_ATTRIBUTES + "attributes";
    public static final String REST_API_RESOURCE_ATTRIBUTES_TRAPTYPES = REST_API_RESOURCE_ATTRIBUTES + "/traptype?";
    public static final String REST_API_RESOURCE_ATTRIBUTES_STRINGS = REST_API_RESOURCE_ATTRIBUTES + "/string?";
    public static final String REST_API_RESOURCE_ATTRIBUTES_STRINGS_TERMS = REST_API_RESOURCE_ATTRIBUTES + "/string/termsofservice?";
    public static final String REST_API_RESOURCE_ATTRIBUTES_IMAGES = REST_API_RESOURCE_ATTRIBUTES + "/image?";
    public static final String REST_API_RESOURCE_ATTRIBUTES_SOUNDTHEMES = REST_API_RESOURCE_ATTRIBUTES + "/theme?";
    public static final String REST_API_RESOURCE_ATTRIBUTES_TRAPCATEGORIES =REST_API_RESOURCE_ATTRIBUTES + "/trapcategory?";

    public static final String REST_API_RESOURCE_TRAP = REST_API_ENDPOINT_TRAP + "trap";
    public static final String REST_API_RESOURCE_TRAP_BLACKLIST = REST_API_ENDPOINT_TRAP + "blacklist";
    public static final String REST_API_RESOURCE_USER = REST_API_ENDPOINT_USER + "user/";
   //

    //
    // Hack for testing

    public static final String REST_API_RESOURCE_USER_FACEBOOK = REST_API_ENDPOINT_USER +"facebook";
    public static final String REST_API_RESOURCE_USER_AVAILABLE = REST_API_ENDPOINT_USER +"username";
    public static final String REST_API_RESOURCE_EMAIL_AVAILABLE = REST_API_ENDPOINT_USER +"emailaddr"; ;

   // public static final String REST_API_RESOURCE_USER_FACEBOOK = "http://user-scourge-240.elasticbeanstalk.com/2/facebook";
    public static final String REST_API_RESOURCE_USER_AUTHENTICATION = "/authentication";
    public static final String REST_API_RESOURCE_USER_CONFIRM = "/confirm/sms/";

    public static final String REST_API_RESOURCE_USER_RESEND = "/resend";

    // Forgot password hack
    public static final String REST_API_FORGOT_PASSWORD = (USE_SCOURGE)?"http://trap-scourge.com/rest/web.php?service=forgotpassword&name=":"http://trapster.com/rest/web.php?service=forgotpassword&name=";

    /*** REST Parameters ***/
    // User
    public static final String REST_PARAMETER_USER_NAME = "displayname";
    public static final String REST_PARAMETER_USER_EMAIL = "emailaddr";
    public static final String REST_PARAMETER_USER_TOS_AGREEMENT = "tosagreed";
    public static final String REST_PARAMETER_USER_PASSWORD = "pwd";
    public static final String REST_PARAMETER_USER_PASSWORD_CONFIRMATION = "pwdcnf";
    public static final String REST_PARAMETER_USER_CARRIER = "carrierName";
    public static final String REST_PARAMETER_USER_NEWSLETTER_FLAG = "newsletterflag";
    public static final String REST_PARAMETER_USER_SMS_ADDRESS = "smsaddr";
    public static final String REST_PARAMETER_USER_KARMA = "karma";
    public static final String REST_PARAMETER_USER_TOTAL_USER_VOTES = "totaluservotes";
    public static final String REST_PARAMETER_USER_SIGN_UP_DATE = "signupepochtime";
    public static final String REST_PARAMETER_USER_MOD_LEVEL = "moderatorlevel";


    // Trap
    public static final String REST_PARAMETER_TRAP_LATITUDE = "latitude";
    public static final String REST_PARAMETER_TRAP_LONGITUDE = "latitude";
    public static final String REST_PARAMETER_TRAP_TRAP_TYPE_IDS = "traptypeids";
    public static final String REST_PARAMETER_TRAP_VOTE = "vote";
    public static final String REST_PARAMETER_TRAP_REQUIRED_CREDIBILITY = "highestusercred";
    public static final String REST_PARAMETER_TRAP_MAX_RESULTS = "maxresults";
    public static final String REST_PARAMETER_TRAP_MINIMUM_VOTES = "numvotes";
    public static final String REST_PARAMETER_TRAP_RADIUS = "radius";
    public static final String REST_PARAMETER_TRAP_SORT_ORDER = "sortorder";
    public static final String REST_PARAMETER_TRAP_LAST_MODIFIED_DATE = "modifiedaftertime";

    // Attributes
    public static final String REST_PARAMETER_ATTRIBUTES_LAST_MODIFIED_DATE = "modifiedaftertime";
    public static final String REST_PARAMETER_ATTRIBUTES_COLOR_ID = "id";
    public static final String REST_PARAMETER_ATTRIBUTES_IMAGE_ID = "id";
    public static final String REST_PARAMETER_ATTRIBUTES_STRING_ID = "id";
    public static final String REST_PARAMETER_ATTRIBUTES_CATEGORY_ID = "id";
    public static final String REST_PARAMETER_ATTRIBUTES_TRAP_TYPE_ID = "id";
    public static final String REST_PARAMETER_ATTRIBUTES_THEME_ID = "id";
    public static final String REST_PARAMETER_ATTRIBUTES_POI_ID = "id";

    // Probe
    public static final String REST_PARAMATER_PROBE_POINTS = "points";
    public static final String REST_PARAMATER_PROBE_LATITUDE = "latitude";
    public static final String REST_PARAMATER_PROBE_LONGITUDE = "longitude";
    public static final String REST_PARAMATER_PROBE_ALTITUDE = "altitude";
    public static final String REST_PARAMATER_PROBE_SPEED = "speed";
    public static final String REST_PARAMATER_PROBE_HEADING = "heading";
    public static final String REST_PARAMATER_PROBE_TIME = "time";
    public static final String REST_PARAMATER_PROBE_HORIZONTAL_ACCURACY = "horizontalaccuracy";
    public static final String REST_PARAMATER_PROBE_VERTICAL_ACCURACY = "verticalaccuracy";
    public static final String REST_PARAMATER_PROBE_SATELLITE_COUNT = "satellitecount";
    public static final String REST_PARAMATER_PROBE_ACCELERATION = "acceleration";

    // Patrol
    public static final String REST_PARAMETER_PATROL_LONGITUDE = "longitude";
    public static final String REST_PARAMETER_PATROL_LATITUDE = "latitude";
    public static final String REST_PARAMETER_PATROL_RADIUS = "radius";
    public static final String REST_PARAMETER_PATROL_PATROLLINES = "patrollines";
    public static final String REST_PARAMETER_PATROL_PATROLPOINTS = "patrolpoints";
    public static final String REST_PARAMETER_PATROL_COLOR = "color";

    /** REST Responses **/
    public static final String REST_RESPONSE_RESPONSE = "response";
    public static final String REST_RESPONSE_EPOCH_TIME = "asofepochtime";
    public static final String REST_RESPONSE_STATUS = "status";
    public static final String REST_RESPONSE_STATUS_CODE = "statuscode";
    public static final String REST_RESPONSE_ERROR_MESSAGE = "message";
    public static final String REST_RESPONSE_DATA = "data";

    // Meta Parameters
    public static final String REST_PARAMETER_META_OPERATING_SYSTEM = "opsys";

    // Authorization Parameters
    public static final String REST_PARAMETER_AUTH = "auth";
    public static final String REST_PARAMETER_META = "meta";
    public static final String REST_PARAMETER_REQUEST = "request";
    public static final String REST_PARAMETER_PARAMS = "params";
    public static final String REST_PARAMETER_AUTH_APP_CAPABILITIES = "appcapabilities";
    public static final String REST_PARAMETER_AUTH_APP_ID = "appid";
    public static final String REST_PARAMETER_AUTH_APP_KEY = "appkey";
    public static final String REST_PARAMETER_AUTH_DEVICE_ID = "deviceid";
    public static final String REST_PARAMETER_AUTH_DEVICE_TYPE = "devicetype";
    public static final String REST_PARAMETER_AUTH_USER_NAME = "login";
    public static final String REST_PARAMETER_AUTH_PASSWORD_HASH = "pwdhash";

    // For Trap Requests
    public static final String REST_PARAMETER_ATTRIBUTES_LATITUDE = "latitude";
    public static final String REST_PARAMETER_ATTRIBUTES_LONGITUDE = "longitude";
    public static final String REST_PARAMETER_ATTRIBUTES_RADIUS = "radius";

    // Statistics
    public static final String REST_STATISTICS_RESPONSE_USER_STATS = "userstats";
    public static final String WEB_ENDPOINT_USERSTATS = REST_SECURE_APIURL + BASE_URL + "api/userstats.php";

}
