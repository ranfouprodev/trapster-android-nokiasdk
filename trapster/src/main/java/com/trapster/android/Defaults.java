package com.trapster.android;

import android.content.pm.ActivityInfo;

import java.util.Arrays;
import java.util.Locale;


public class Defaults {
	
	// Application Specifics
	public static final String APP_APPID = "TPST-AND-"+getLanguage()+"-";
	public static final String APP_CAPABILITIES = "3";


    public static String getLanguage(){

		final String[] supportedLanguages = new String[]{"EN","ES","DE","FR","PT","RU","AR", "TR", "SV"};
		String lang = "EN";
		String localeLanguage = Locale.getDefault().getLanguage().toUpperCase();
		if(Arrays.asList(supportedLanguages).contains(localeLanguage))
			return localeLanguage;
		return lang;
	}


 /*
  * Nokia Developer tokens (api.developer.nokia.com) tied to Developers@trapster.com.
 */
    public static final String NOKIA_STATIC_MAP_APPID = "SfH5LSg2FY1hrVG1BdvU";

    public static final String NOKIA_STATIC_MAP_TOKEN = "TKm0phtr6cLDbKhRZuD7zg";

    public static final String FACEBOOK_APP_ID = "66164807139";
    public static final String NLP_APP_ID = "sfajBLtQoUUjYFeKEMPn";
    public static final String NLP_APP_CODE = "Bcl4_BkJbrW9vS2qHvDpfw";
	
	
	// Demo User
	public static final String DEMO_USERNAME = "demo";
	public static final String DEMO_PASSWORD = "rewqtyui";
	
	public static final String STORAGE_PATH = "/data/data/com.trapster.android/files/";

	public static final String ASSET_TERMSANDCONDITIONS = "termsofservice";

    public static final String ASSET_TERMSANDCONDITIONS_ACCEPTED_DATE = "termsofserviceaccepted";
    public static final String ASSET_TERMSANDCONDITIONS_NEWEST_DATE = "termsofserviceupdatetime";
	
	//NPS Specifics
	public static final String NPS_PROJECT_ID = "2272";
    public static final String NPS_SOURCE_ID = "61f01c55c6cf5b123b67d383e7ed4018";
    public static final String NPS_VERSION = "5aafdb8";
	
	// Worker Filters
	public static final int WORKER_PATROL = 2;
	public static final int WORKER_LOGIN= 3;
	public static final int WORKER_LEGAL= 4;
	public static final int WORKER_LAUNCH= 5;

	// GPS Settings
	public static final int GPS_SECONDS_FOR_UPDATE = 0;
	public static final float GPS_DISTANCE_FOR_UPDATE = 0;

	// This is the speed at which the cone is smallest. Cone size is
	// proportional to min-max radius
	public static final double DYNAMIC_MAX_SPEED = 80; // mph at which the cone
														// is the smallest

	// This is the percentage increase in detection size based on speed. 0.2 is
	// 20% increase at max speed
	public static final double DYNAMIC_RADIUS_SCALE = 0.5; // The radius gets
															// this percentage
															// bigger

	public static final float INITIAL_LAT = 25.799891f;
	public static final float INITIAL_LNG = -40.133057f;
	
	public static final double BACKGROUND_TRAPS_UPDATE_DISTANCE = 5000; // m

	public static final long BACKGROUND_TRAPS_UPDATE_TIME = 60 * 1000;  //ms

	public static final int AGE_FOR_NEW_TRAPS = 24; // in hours
	public static final double NEARBY_TRAP_RADIUS = 5000; // in meters

    public static final int UNLIMITED_SPEED_998 = 998;
    public static final int UNLIMITED_SPEED_999 = 999;

    public static final double DEFAULT_ALERT_DISTANCE = 3.0; // This represents 0.3 miles/km, see ConeTrapFilter
    public static final int DEFAULT_ALERT_REPEAT_MINUTES = 0;

    //Orientation Screen view id's
    public static final int SETTING_DEFAULT_SCREEN_ORIENTATION = ActivityInfo.SCREEN_ORIENTATION_SENSOR;
    public static final int SCREEN_ORIENTATION_SENSOR = ActivityInfo.SCREEN_ORIENTATION_SENSOR;
    public static final int SCREEN_ORIENTATION_LANDSCAPE = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
    public static final int SCREEN_ORIENTATION_PORTRAIT = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

    //Map Mode Preference Screen id's
    public static final int PREFERENCE_MAP_MODE_AUTO = 1;
    public static final int PREFERENCE_MAP_MODE_DAY = 2;
    public static final int PREFERENCE_MAP_MODE_NIGHT = 3;

    //Map Type Preference Screen id's
    public static final int PREFERENCE_MAP_TYPE_MAP = 1;
    public static final int PREFERENCE_MAP_TYPE_SATELLITE = 2;
    public static final int PREFERENCE_MAP_TYPE_TERRAIN = 3;

    public static final String POI_KEY_BANK = "POIKeyBank";
    public static final String POI_KEY_GAS = "POIKeyGas";
    public static final String POI_KEY_PARKING = "POIKeyParking";
    public static final String POI_KEY_RESTAURANTS = "POIKeyRestaurants";
    public static final String POI_KEY_POLICE = "POIKeyPolice";
    public static final String POI_KEY_FIRE = "POIKeyFire";
    public static final String POI_KEY_COFFEE = "POIKeyCoffee";

	// Preferences Lookup
	public static final String PREF_CREDENTIALS = "TrapsterUser";
	public static final String PREF_SETTINGS = "TrapsterSettings";
	
	public static final String PREFERENCE_USERNAME = "username";
	public static final String PREFERENCE_PASSWORD = "password";

    public static final String SESSION_IS_FIRST_REGISTER = "sessionIsFirstRegister";
	/*
	 * the time when appattributes was last called
	 */
	public static final String PREFERENCE_LAST_UPDATE_TIME_ATTRIBUTES = "attributeslastupdatetime";
	/*
	 * The time when traprtypes was last called
	 */
	public static final String PREFERENCE_LAST_UPDATE_TIME_TRAP_CONFIG = "lastupdatetimetrapconfig";
	/*
	 * The time when the categories was last called
	 */
	public static final String PREFERENCE_LAST_UPDATE_TIME_CATEGORIES = "lastupdatetimecategories";
	/*
	 * The time when all the attributes have been updated
	 */
	public static final String PREFERENCE_LAST_UPDATE_TIME_ALLATTRIBUTES = "lastupdatetimeallattributes";
	/*
	 * Flag to enable probe data collection
	 */
	public static final String PREFERENCE_ENABLE_PROBEDATA = "preferenceenableprobedata";
	/*
	 * Last gps location by the device used to fix the startup
	 */
	public static final String PREFERENCE_LAST_UPDATE_LATITUDE = "preferencelastupdatelatitude";
	public static final String PREFERENCE_LAST_UPDATE_LONGITUDE = "preferencelastupdatelongitude";
    /*
     * 	Preference to determine the screen orientation.  1 = auto, 2 = portrait, 3 = landscape
     */
    public static final String PREFERENCE_ORIENTATION = "orientation";
    /*
    * 	Preference to determine the screen map mode.  1 = auto, 2 = day, 3 = night
    */
    public static final String PREFERENCE_MAP_MODE = "preferencemapmode";
    /*
    * 	Preference to determine the screen type mode.  1 = map, 2 = satellite, 3 = terrain
    */
    public static final String PREFERENCE_MAP_TYPE = "preferencemaptype";
    /*
    * 	Preference to determine the screen display mode.  true = 3d, false = 2d,
    */
    public static final String PREFERENCE_MAP_OPTIONS_DISPLAY_3D = "preferencemapoptionsdisplay3d";
    /*
    * 	Preference to determine the screen display units.  true = miles, false = km,
    */
    public static final String PREFERENCE_MAP_OPTIONS_DISPLAY_MILES = "preferencemapoptionsdisplaymiles";
    /*
    * 	Preference to determine the screen display auto zoom.  true, false,
    */
    public static final String PREFERENCE_MAP_OPTIONS_DISPLAY_AUTO_ZOOM = "preferencemapoptionsdisplayautozoom";
    /*
    * 	Preference to determine the screen display follow.  true, false,
    */
    public static final String PREFERENCE_MAP_OPTIONS_DISPLAY_FOLLOW = "preferencemapoptionsdisplayfollow";
    /*
    * 	Preference to determine to alert speed limit.  true = show, false = don't show,
    */
    public static final String PREFERENCE_MAP_DISPLAY_ALERT_SPEED_LIMIT= "preferencemapdisplayalertspeedlimit";
    /*
    * 	Preference to determine to show traffic.  true = show, false = don't show,
    */
    public static final String PREFERENCE_MAP_DISPLAY_SHOW_TRAFFIC= "preferencemapdisplayshowtraffic";
    /*
    * 	Preference to determine to show patrol lines.  true = show, false = don't show,
    */
    public static final String PREFERENCE_MAP_DISPLAY_SHOW_PATROL= "preferencemapdisplayshowpatrol";
    /*
    * 	Preference to determine to show virtual radar.  true = show, false = don't show,
    */
    public static final String PREFERENCE_MAP_DISPLAY_SHOW_VIRTUAL_RADAR= "preferencemapdisplayshowvirtualradar";
    /*
    * 	Preference to determine to hide speed limit tutorial.  true = hide, false = show,
    */
    public static final String PREFERENCE_HIDE_SPEED_LIMIT_TUTORIAL= "preferencehidespeedlimittutorial";
    /*
    * 	Preference to determine the alerting distance of a trap.  Float ranging from 0 to 2.0 km/miles
    */
    public static final String PREFERENCE_TRAP_ALERT_DISTANCE= "preferencetrapalertdistance";
    /*
    * 	Preference to determine the repeat trap audible alert.  Int ranging from 0 to 60 seconds.
    */
    public static final String PREFERENCE_TRAP_ALERT_REPEAT_AUDIBLE= "preferencetrapalertrepeataudible";
    /*
    * 	Preference to determine to hide dashboard tutorial.  true = hide, false = show,
    */
    public static final String PREFERENCE_HIDE_DASHBOARD_TUTORIAL= "preferencehidedashboardtutorial";
    /*
    * 	Preference to determine the tilt level.
    */
    public static final String PREFERENCE_TILT_LEVEL= "preferencetiltlevel";

    public static final String PREFERENCE_ALERTS_GLOBAL_SETTINGS = "preferenceAlertsGlobalSettings";

    /*
Will be set to true when the maps are downloaded the first time. Used to display the map downloading dialog
 */
    public static final String PREFERENCE_HAS_FIRST_MAP_DOWNLOAD = "preferencefirstmapdownload";

    /*
    * 	Preference to determine the index of the chosen sound theme,
    */
    public static final String PREFERENCE_SOUND_THEME_CHOSEN= "preferencesoundthemechosen";
    public static final String PREFERENCE_SOUND_THEME_FABULOUS_EASTER_EGG= "preferencesoundthemefabulouseasteregg";
    public static final int PREFERENCE_SOUND_THEME_DEFAULT= 0;
    public static final String PREFERENCE_SOUND_THEME_TEMP= "preferencesoundthemetemp";
    public static final String PREFERENCE_SOUND_DIALOG_SHOWING= "preferencessounddialogshowing";
    public static final boolean PREFERENCE_DIALOG_SHOWING_DEFAULT= false;
	public static final String PREFERENCE_LAST_UPDATE_TIME_SOUNDTHEME = "preferencelastupdatetimesoundtheme";
    
    
	// Preferences Strings
	public static final String PREFERENCE_SHOW_TUTORIAL = "PREFERENCESHOWTUTORIAL";
	public static final String SETTING_WIDGET_TRAP_INDEX= "WIDGETTRAPINDEX";
	public static final String SETTING_WIDGET_MESSAGE_TIME= "WIDGETMESSAGETIME";

	public static final String SETTING_MAP_DISPLAY_TRAPS = "MAPDISPLAYTRAPS";

	public static final String SETTING_SPEED_LIMITS_SHOW_SPEED_LIMIT = "SPEEDLIMITSSHOWSPEEDLIMIT";

    public static final String RATE_US_DIALOG_IGNORE_COUNT = "rateUsDialogIgnoreCount";
    public static final String RATE_US_DIALOG_FEEDBACK_SENT = "preferenceRateUsFeedbackSent";
    public static final String RATE_US_APP_START_COUNT = "rateUsAppStartCount";

    public static final String NPS_LAST_RATING_TIME = "npsLastRateTimeKey";

	// Notification ID
	public static final int NOTIFICATION_VIBRATION = 1;

	public static final boolean SETTING_DEFAULT_SHOW_SPEED_LIMIT = true;
	
	// Error response messages
	public static final String ERROR_INVALID_CREDENTIALS = "BAD";

	public static final String ERROR_CONFIRMATION_NEEDED = "UNCONFCODE";

    public static final String TRAPSTER_UNAVAILABLE = "We are sorry we are unable to reach trapster.com at this time.";

	// PendingIntent Request Codes
	public static final int PENDING_INTENT_START_SCREEN_FROM_BACKGROUND= 0;
	public static final int PENDING_INTENT_START_SCREEN_FROM_WIDGET_LOGO= 1;
	public static final int PENDING_INTENT_START_SCREEN_FROM_WIDGET_CONTAINER= 2;

	// Intent Extra Parameters
    public static final String INTENT_HELP_VIEW_ID = "helpviewid";
    public static final String INTENT_HELP_VIEW_TITLE = "helpviewtitle";


    public static final double FLT_MAX = 340282346638528859811704183484516925440.000000;
    public static final double FLT_MIN = 0.0000;
    public static final int FIXED_IMAGE_SIZE = 256;
    public static final int MIN_SUPPORTED_WORLD_SIZE = 16 * FIXED_IMAGE_SIZE;/*=(2^4)*128*/
    public static final int MAX_SUPPORTED_WORLD_SIZE = 262144 * FIXED_IMAGE_SIZE; /*=(2^18)*128*/


    public static final int VIRTUAL_RADAR_LAYER_INDEX = 2;
    public static final int POSITION_INDICATOR_LAYER_INDEX = 3;

	// API Locations
    private static final String BASE_URL = "trapster.com"; // Change this to affect the URL path for all outgoing communication
	private static final String APIURL = "http://www." + BASE_URL + "/api/";
	private static final String SECURE_APIURL = "https://www." + BASE_URL + "/api/";
//	public static final String API_NEW_TRAP = APIURL + "trapserv.php";
	public static final String API_NEW_TRAP_WITH_ID = APIURL + "trapservid.php";
	public static final String API_TRAPS_IN_RANGE = APIURL + "alrtserv2.php";
//	public static final String API_CHANGED_TRAPS_IN_RANGE = APIURL + "alrtserv3.php";
	public static final String API_TRAP_VOTE = APIURL + "vote.php";
	public static final String API_REMOVE_TRAP = APIURL + "deltrap.php";
    public static final String API_IGNORE_TRAP = APIURL + "trapblacklist.php";
	public static final String API_CHECK_LOGIN = APIURL + "checkLogin.php";
	public static final String API_FORGOT_PASSWORD = "http://www." + BASE_URL + "/forgot-password.php";
//	// public static final String API_NEW_TRAP =
//	// "http://trapster.com/api/getUID.php - get the numeric user id. currently
//	// only needed for Windows apps
//	public static final String API_GET_PREFS = APIURL + "getmprefs.php";
	public static final String API_SIGNUP = SECURE_APIURL + "signup.php";
	public static final String API_RESEND = APIURL + "resend.php";
//	public static final String API_TRIP = APIURL + "trip.php";
	public static final String API_IAMHERE = APIURL + "locationdata.php";
	public static final String API_LAUNCHMESSAGE = APIURL + "launchmessage.php";
	public static final String API_SOUNDTHEME = APIURL + "themes2.php";
	public static final String API_PATROLPATH = APIURL + "patrol.php";
	public static final String API_ATTRIBUTES = APIURL + "appattributes.php";
	public static final String API_TYPES = APIURL + "types.php";
//	public static final String API_GATEKEEPER = APIURL + "gatekeeper.php";
	public static final String API_REPORT_NEW_SPEED_LIMIT = APIURL + "speedlimit.php";

	public static final String HELP_URL = "http://" + BASE_URL + "/app/android/help.php";
    public static final String PHP_API_BECOME_MODERATOR = "http://www." +  BASE_URL + "/moderator.php";
	
	public static final String REPORT_NPS = "https://cpq.nokia.com/api.php";

    //  This is a TEMPORARY URL for retrieving stats.  This is part of the REST services and should not be used in the PHP api
    public static final String API_TEMPORARY_STATS_URL = "http://" + BASE_URL + "/rest/web.php?service=userstats";

    //NLP Constants
    private static final String NLP_APIURL = "https://route.nlp.nokia.com/routing/6.2/" ;
    public static final String NLP_GET_LINK_INFO =  NLP_APIURL + "getlinkinfo.json?";

	public static final String XML_PARAMS_IF_CHANGED_AFTER = "if_changed_after";
    // Map Defaults
    public static final int MAP_ORIENTATION_DEFAULT = 0;
    public static final float MAP_TILT_DEFAULT = 45.0f;
    public static final float MAP_MAX_TILT = 65.0f;
    public static final int DEFAULT_MAP_ZOOM_LEVEL = 16;
    public static final int MAP_MAX_CENTER_ON_ME_ZOOM_LEVEL = 10;
    public static final int MAP_MAX_ZOOM_LEVEL = 3;
    public static final String MAP_FIRST_LOAD_COMPLETE = "mapFirstLoadComplete";

    // Trap type notification settings
    public static final String SETTING_ALERTING = "_alerting";
    public static final String SETTING_VISIBLE = "_visible";
    public static final String SETTING_AUDIBLE = "_audible";
    public static final String SETTING_VIBRATE = "_vibrate";
    public static final String SETTING_SHOW_ON_MAP = "_showOnMap";

    // Statistics
    public static final String PREFERENCE_STATS_USERS_HELPED = "usersHelpedStatsPreference";


}
