package com.trapster.android;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 10/24/13
 * Time: 3:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class NLPDefaults {

    public static final String NLP_APP_ID = "sfajBLtQoUUjYFeKEMPn";
    public static final String NLP_APP_CODE = "Bcl4_BkJbrW9vS2qHvDpfw";

    /** NLP Responses **/
    public static final String NLP_RESPONSE_RESPONSE = "Response";
    public static final String NLP_RESPONSE_ROUTE = "Route";
    public static final String NLP_RESPONSE_LEG = "Leg";
    public static final String NLP_RESPONSE_SUMMARY = "Summary";

    public static final String NLP_RESPONSE_PARAM_DISTANCE = "Distance";
    public static final String NLP_RESPONSE_PARAM_TRAFFIC_TIME = "TrafficTime";
    public static final String NLP_RESPONSE_PARAM_BASE_TIME = "BaseTime";




    //NLP Constants
    private static final String NLP_APIURL = "https://route.nlp.nokia.com/routing/6.2/" ;
    public static final String NLP_GET_LINK_INFO =  NLP_APIURL + "getlinkinfo.json?";
    public static final String NLP_CALCULATE_ROUTE =  NLP_APIURL + "calculateroute.json?";

    public static final String NLP_DEFAULT_LINK_ATTRIBUTES= "sl,sh,fc,le";
    public static final String NLP_DEFAULT_REPRESENTATION_ATTRIBUTES_LINK_PAGING= "linkPaging";
    public static final String NLP_DEFAULT_REPRESENTATION_ATTRIBUTES_OVERVIEW= "overview";
    //
    public static final String NLP_QUERY_PARAM_APP_ID= "app_id";
    public static final String NLP_QUERY_PARAM_APP_CODE = "app_code";
    public static final String NLP_QUERY_PARAM_WAYPOINT= "waypoint=geo!";
    public static final String NLP_QUERY_PARAM_WAYPOINT_0= "waypoint0=geo!";
    public static final String NLP_QUERY_PARAM_WAYPOINT_1= "waypoint1=geo!";
    public static final String NLP_QUERY_PARAM_QUADKEY= "quadkey";
    public static final String NLP_QUERY_PARAM_LINK_ATTRIBUTES= "linkAttributes=";
    public static final String NLP_QUERY_PARAM_MODE = "mode=";
    public static final String NLP_QUERY_PARAM_REPRESENTATION = "representation=";

    //NLP Routing Mode Types
    public static final String NLP_ROUTING_TYPE_FASTEST = "fastest";
    public static final String NLP_ROUTING_TYPE_SHORTEST = "shortest";
    public static final String NLP_ROUTING_TYPE_ECONOMIC = "economic";

    //NLP Traffic Mode Types
    public static final String NLP_TRAFFIC_TYPE_ENABLED = "traffic:enabled";

    //NLP Transport Mode Types
    public static final String NLP_TRANSPORT_TYPE_CAR = "car";
}

