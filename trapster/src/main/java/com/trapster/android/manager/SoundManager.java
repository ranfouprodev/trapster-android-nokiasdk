package com.trapster.android.manager;

import android.app.Application;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import com.google.inject.Inject;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.model.ResourceNotLoadedException;
import com.trapster.android.model.SoundTheme;


import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;


public class SoundManager
{
    static final String LOGNAME= "Trapster.SoundManager";
    //
//    static SoundManager instance;
//    public static synchronized SoundManager getInstance()
//    {
//        if (instance == null)
//            instance= new SoundManager(TrapsterApplication.getAppInstance());
//        return instance;
//    }
    //
    static class InternalOnLoadCompleteListener implements OnLoadCompleteListener
    {
        public void onLoadComplete(SoundPool soundPool, int sampleId, int status)
        {
            soundPool.play(sampleId, 100, 100, 1, 0, 1.0f);
        }
    }
    @Inject Application application;
    //
    Context context;
    //
    SoundPool soundPool;
    Map<String, Integer> soundPoolMap;
    int streamID;
    //

    public  SoundManager(){

    }

    @Inject
    public void init() {
        ////Log,.v(LOGNAME, "SoundManager initialized");
        this.context= application.getApplicationContext();
        soundPool= new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        soundPool.setOnLoadCompleteListener(new InternalOnLoadCompleteListener());
        soundPoolMap= Collections.synchronizedMap(new HashMap<String, Integer>());
    }

//    private SoundManager(Context context)
//    {
//        this.context= context;
//        soundPool= new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
//        soundPool.setOnLoadCompleteListener(new InternalOnLoadCompleteListener());
//        soundPoolMap= Collections.synchronizedMap(new HashMap<String, Integer>());
//    }
    int getSoundReference(String audioId)
    {
        int id= -1; // Default
        if (audioId.equals("lp"))
            id= R.raw.lp;
        else if (audioId.equals("rlc"))
            id= R.raw.rlc;
        else if (audioId.equals("sc"))
            id= R.raw.sc;
        else if (audioId.equals("hp"))
            id= R.raw.hp;
        else if (audioId.equals("cc"))
            id= R.raw.cc;
        else if (audioId.equals("msc"))
            id= R.raw.msc;
        else if (audioId.equals("mscosh"))
            id= R.raw.mscosh;
        else if (audioId.equals("cp"))
            id= R.raw.cp;
        else if (audioId.equals("accident"))
            id= R.raw.accident;
        else if (audioId.equals("brushfire"))
            id= R.raw.brushfire;
        else if (audioId.equals("cb"))
            id= R.raw.cb;
        else if (audioId.equals("childrenatplay"))
            id= R.raw.childrenatplay;
        else if (audioId.equals("construction"))
            id= R.raw.construction;
        else if (audioId.equals("dangerintersect"))
            id= R.raw.dangerintersect;
        else if (audioId.equals("dangerouscurve"))
            id= R.raw.dangerouscurve;
        else if (audioId.equals("flood"))
            id= R.raw.flood;
        else if (audioId.equals("ice"))
            id= R.raw.ice;
        else if (audioId.equals("narrowbridge"))
            id= R.raw.narrowbridge;
        else if (audioId.equals("roadclosed"))
            id= R.raw.roadclosed;
        else if (audioId.equals("roadkill"))
            id= R.raw.roadkill;
        else if (audioId.equals("schoolzone"))
            id= R.raw.schoolzone;
        else if (audioId.equals("roadhazard"))
            id= R.raw.roadhazard;
        else if (audioId.equals("tollbooth"))
            id= R.raw.tollbooth;
        return id;
    }
    private void setStreamID(int streamID)
    {
        this.streamID = streamID;
    }
    public int getStreamID()
    {
        return this.streamID;
    }

    public void playSound(int resourceId)
    {
        try
        {
            if (resourceId != -1)
            {
                String soundKey= "res_" + resourceId;
                Integer soundId= soundPoolMap.get(soundKey);
                if (soundId == null)
                {
                    soundId= soundPool.load(context, resourceId, 1);
                    soundPoolMap.put(soundKey, soundId);
                } else
                {
                    if (getStreamID() != 0)
                        soundPool.stop(getStreamID());
                    setStreamID(soundPool.play(soundId, 100, 100, 1, 0, 1.0f));
                }
            }
        } catch (Exception e)
        {
            StringWriter out= new StringWriter();
            e.printStackTrace(new PrintWriter(out));
{/*             //Log.e(LOGNAME, "Unable to play sound[playSound]:" + out.toString()); */}
        }
    }
    public int playSound(String audioId, SoundTheme theme)
    {

        int soundId = theme != null ? playSoundByResource(audioId, theme) : 0;
        return soundId != 0 ? soundId : playSoundByReference(audioId);
    }
    int playSoundByReference(String audioId)
    {
        try
        {
            int refId= getSoundReference(audioId);
            if (refId != -1)
            {
                String soundKey= "ref_" + refId;
                Integer soundId= soundPoolMap.get(soundKey);
                if (soundId == null)
                {
                    AssetFileDescriptor afd= context.getResources().openRawResourceFd(refId);
                    if (afd == null)
                        return 0;
                    soundId= soundPool.load(afd, 1);
                    soundPoolMap.put(soundKey, soundId);
                } else
                {
                    if (getStreamID() != 0)
                        soundPool.stop(getStreamID());
                    setStreamID(soundPool.play(soundId, 100, 100, 1, 0, 1.0f));
                }
                return soundId;
            }
        } catch (Exception e)
        {
            StringWriter out= new StringWriter();
            e.printStackTrace(new PrintWriter(out));
{/*             //Log.e(LOGNAME, "Unable to play sound[playSoundByReference]:" + out.toString()); */}
        }
        return 0;
    }
    int playSoundByResource(String audioId, SoundTheme theme)
    {

        try
        {
            String soundResource= theme.getSoundResource(audioId);
            String soundKey= "pathRes_" + soundResource;


            Integer soundId= soundPoolMap.get(soundKey);
            if (soundId == null)
            {
                 soundId= soundPool.load(Defaults.STORAGE_PATH + soundResource, 1);
                soundPoolMap.put(soundKey, soundId);
            } else
            {
                if (getStreamID() != 0)
                    soundPool.stop(getStreamID());
                setStreamID(soundPool.play(soundId, 100, 100, 1, 0, 1.0f));
            }
            return soundId;
        } catch (ResourceNotLoadedException e)
        {
          // Log.e(LOGNAME, "Unable to load sound theme file:" + e.getMessage());
        } catch (Exception e)
        {
            StringWriter out= new StringWriter();
            e.printStackTrace(new PrintWriter(out));
         //   Log.e(LOGNAME, "Unable to play sound[playSoundByResource]:" + out.toString());
        }
        return 0;
    }
    public void removeSounds(Set<Integer> soundIds)
    {
        synchronized (soundPoolMap)
        {
            for (Iterator<Integer> q= soundPoolMap.values().iterator(); q.hasNext();)
            {
                Integer soundId= q.next();
                if (soundIds.contains(soundId))
                {
                    soundPool.unload(soundId);
                    q.remove();
                }
            }
        }
    }
}
