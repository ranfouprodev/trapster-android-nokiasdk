package com.trapster.android.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.WindowManager;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.here.android.common.GeoCoordinate;
import com.here.android.mapping.*;
import com.luckycatlabs.sunrisesunset.SunriseSunsetCalculator;
import com.luckycatlabs.sunrisesunset.dto.Location;
import com.trapster.android.Defaults;
import com.trapster.android.util.BackgroundTask;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.concurrent.CopyOnWriteArrayList;

//import com.here.android.mapping.MapFactory;

/**
 * Map controller facade
 * 
 * @TODO need default starting point
 * 
 */
@Singleton
public class MapController {

	private static final String LOGNAME = "MapController";

	@Inject
	SharedPreferences sharedPreferences;

	@Inject
	PositionManager positionManager;

	@Inject
	WindowManager windowManager;

	@Inject
	TrapManager trapManager;

	@Inject
	PatrolManager patrolManager;

    private boolean wasDayTime = false;

	private static final double RADIUS_TO_FIRST_UPDATE_TRAPS_IN_MILES = 2;

	protected boolean mapLoaded;

	private boolean isCenterOnMe;
    private static boolean hasInitialized = false;

	@Inject
	public MapController(Context context)
    {
        if (!hasInitialized)
        {
            MapFactory.initFactory(context, new FactoryInitListener() {

                @Override
                public void onFactoryInitializationCompleted(InitError initError)
                {
                    if (initError == InitError.NONE) {
                        mapLoaded = true;
                        onMapReadyRunnable.run();
                    } else {
                        // @TODO not sure how to handle this
                        // handle factory initialization failure
                    }
                }
            });
        }
	}

	@Inject
	public void init() {
		updateCenterOnMeFromPreferences();
	}

	public void updateCenterOnMeFromPreferences() {
		isCenterOnMe = sharedPreferences.getBoolean(
				Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_FOLLOW, true);
	}

	strictfp float getVal() {
		return 0.0f;
	}

	public Runnable onMapReadyRunnable = new Runnable() {
		@Override
		public void run() {
			mapLoaded = true;
			for (OnMapLoadCompleteListener listener : listeners)
				listener.onMapReady();
		}
	};

	public interface OnMapLoadCompleteListener {
		void onMapReady();
	}
	
	private ArrayList<OnMapLoadCompleteListener> listeners = new ArrayList<OnMapLoadCompleteListener>();


	public void addOnMapLoadCompleteListener(OnMapLoadCompleteListener listener) {
		if (mapLoaded)
			listener.onMapReady();
		else
			listeners.add(listener);
	}

	public boolean isMapReady() {
		return mapLoaded;
	}
	
	
	
	public interface OnMapUpdateListener{
		void onCenterOnMeChanged(boolean isCenterOnMe);
	}
	private CopyOnWriteArrayList<OnMapUpdateListener> mapUpdateListener = new CopyOnWriteArrayList<OnMapUpdateListener>();

	public void addMapUpdateListener(OnMapUpdateListener listener){
		mapUpdateListener.add(listener);
	}
	public boolean removeMapUpdateListener(OnMapUpdateListener listener){
		return mapUpdateListener.remove(listener);
	}
	
	private void sendUpdateCenterOnMe(){
		for(OnMapUpdateListener listener:mapUpdateListener){
			listener.onCenterOnMeChanged(isCenterOnMe());
		}
	}

	/*
	 * Toggles the center on me functions
	 */
	public void toggleCenterOnMe() {

		isCenterOnMe = !isCenterOnMe; // Toggle the center on me state

		setCenterOnMe(isCenterOnMe);
	}

	public void setCenterOnMe(boolean centerOnMe) {

		isCenterOnMe = centerOnMe;

		saveCenterOnMe();
		
		sendUpdateCenterOnMe();


	}

	private void saveCenterOnMe() {

        new BackgroundTask()
        {
            @Override
            public void onExecute()
            {
                sharedPreferences.edit().putBoolean( Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_FOLLOW,isCenterOnMe).commit();
            }

            @Override
            public void onTimeout(long runTime)
            {
            }
        }.execute();
	}

	public boolean isCenterOnMe() {
		return isCenterOnMe;
	}

    /**
     * Used in the center on me to set the day/night icon
     * @return
     */
    public String getExpectedMapScheme(){

        int mapScheme = sharedPreferences.getInt(Defaults.PREFERENCE_MAP_TYPE,
                Defaults.PREFERENCE_MAP_TYPE_MAP);
        if (mapScheme == Defaults.PREFERENCE_MAP_TYPE_TERRAIN)
            return MapScheme.TERRAIN_DAY;
        else if (mapScheme == Defaults.PREFERENCE_MAP_TYPE_SATELLITE)
            return MapScheme.SATELLITE_DAY;
        else {
            int timeMode = sharedPreferences.getInt(
                    Defaults.PREFERENCE_MAP_MODE,
                    Defaults.PREFERENCE_MAP_MODE_AUTO);
            switch (timeMode) {
                case Defaults.PREFERENCE_MAP_MODE_DAY: {
                    return MapScheme.NORMAL_DAY;

                }
                case Defaults.PREFERENCE_MAP_MODE_NIGHT: {
                    return MapScheme.NORMAL_NIGHT;

                }
                case Defaults.PREFERENCE_MAP_MODE_AUTO: {
                    if (isDayTime())
                    {
                       return MapScheme.NORMAL_DAY;
                    } else {
                        return MapScheme.NORMAL_NIGHT;
                    }

                }
            }
        }
        return MapScheme.NORMAL_DAY;
    }

    public void updateMapScheme(Map map)
    {

        int mapScheme = sharedPreferences.getInt(Defaults.PREFERENCE_MAP_TYPE,
                Defaults.PREFERENCE_MAP_TYPE_MAP);
        if (mapScheme == Defaults.PREFERENCE_MAP_TYPE_TERRAIN)
            map.setMapScheme(MapScheme.TERRAIN_DAY);
        else if (mapScheme == Defaults.PREFERENCE_MAP_TYPE_SATELLITE)
            map.setMapScheme(MapScheme.SATELLITE_DAY);
        else
        {
            int timeMode = sharedPreferences.getInt(
                    Defaults.PREFERENCE_MAP_MODE,
                    Defaults.PREFERENCE_MAP_MODE_AUTO);
            switch (timeMode)
            {
                case Defaults.PREFERENCE_MAP_MODE_DAY:
                {
                    map.setMapScheme(MapScheme.NORMAL_DAY);
                    break;
                }
                case Defaults.PREFERENCE_MAP_MODE_NIGHT:
                {
                    map.setMapScheme(MapScheme.NORMAL_NIGHT);
                    break;
                }
                case Defaults.PREFERENCE_MAP_MODE_AUTO:
                {
                    if (isDayTime())
                    {
                        map.setMapScheme(MapScheme.NORMAL_DAY);
                    } else
                    {
                        map.setMapScheme(MapScheme.NORMAL_NIGHT);
                    }
                    break;
                }
            }
        }

    }

    public void setDefaultTilt(Map map)
    {
        boolean display3d = sharedPreferences.getBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_3D, true);

        if (display3d)
            map.setTilt(Defaults.MAP_TILT_DEFAULT);
        else
            map.setTilt(0);
    }

    public void updateDayMapScheme(Map map)
    {
        boolean isDay = isDayTime();

        if (isDay != wasDayTime)
        {
            if (isDay) {
                map.setMapScheme(MapScheme.NORMAL_DAY);
            } else {
                map.setMapScheme(MapScheme.NORMAL_NIGHT);
            }
        }

    }

	private boolean isDayTime() {
		GeoCoordinate coordinate = positionManager.getLastLocation().getCoordinate();
        if (coordinate.isValid())
        {
            Location location = new Location(coordinate.getLatitude(),coordinate.getLongitude());
            TimeZone timeZone = TimeZone.getDefault();

            SunriseSunsetCalculator calculator = new SunriseSunsetCalculator(location, timeZone);
            Calendar date = Calendar.getInstance(timeZone);
            Calendar sunriseTime = calculator.getCivilSunriseCalendarForDate(date);
            Calendar currentTime = Calendar.getInstance(timeZone);
            currentTime.setTimeInMillis(System.currentTimeMillis());
            Calendar sunsetTime = calculator.getCivilSunsetCalendarForDate(date);

            int sunriseHours = sunriseTime.get(Calendar.HOUR_OF_DAY);
            int sunriseMinutes = sunriseTime.get(Calendar.MINUTE);
            int sunrise = (sunriseHours * 60) + sunriseMinutes;

            int currentHours = currentTime.get(Calendar.HOUR_OF_DAY);
            int currentMinutes = currentTime.get(Calendar.MINUTE);
            int current = (currentHours * 60) + currentMinutes;

            int sunsetHours = sunsetTime.get(Calendar.HOUR_OF_DAY);
            int sunsetMinutes = sunsetTime.get(Calendar.MINUTE);
            int sunset = (sunsetHours * 60) + sunsetMinutes;


            wasDayTime = sunrise < current && current < sunset;
        }
        else
            wasDayTime = true;


        return wasDayTime;
	}

}
