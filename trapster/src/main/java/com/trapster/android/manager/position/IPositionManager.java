package com.trapster.android.manager.position;

import com.here.android.common.GeoPosition;
import com.here.android.common.LocationStatus;
import com.here.android.common.PositionListener;

/**
 * 
 * This will be the eventual interface for positioning managers. This allows us to 
 * switch between location libraries without impacting the rest of the code
 * 
 * @author John
 *
 */
public interface IPositionManager {

	public void start(); 
	
	public void stop(); 
	
	public GeoPosition getLastLocation(); 
	
	public LocationStatus getStatus(); 
	
	public void addPositionListener(PositionListener listener);
	
	public void removePositionListener(PositionListener listener);
	
	
}
