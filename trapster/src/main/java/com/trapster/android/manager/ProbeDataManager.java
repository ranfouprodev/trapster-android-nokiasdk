package com.trapster.android.manager;

import android.app.Application;
import android.content.SharedPreferences;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * ProbeDataManager pushes the probedata to the server and maintains the probedatabase 
 * 
 * 
 * @author John
 *
 */
@Singleton
public class ProbeDataManager {

	private static final String LOGNAME = "ProbeDataManager";
	
	@Inject Application application;
	
	@Inject SharedPreferences sharedPreferences;
	
	public ProbeDataManager() {
	}
	
    @Inject
	public void init() {
        ////Log,.v(LOGNAME, "ProbeDataManager initialized");
    }


    
    
   
}
