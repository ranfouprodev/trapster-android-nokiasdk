package com.trapster.android.manager;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.v4.util.LruCache;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.here.android.common.Image;
import com.here.android.mapping.MapAnimation;
import com.here.android.mapping.MapBitmapEventListener;
import com.here.android.mapping.MapFactory;
import com.here.android.restricted.mapping.Map;
import com.here.android.restricted.mapping.RestrictedMapFactory;
import com.jakewharton.DiskLruCache;
import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParseException;
import com.larvalabs.svgandroid.SVGParser;
import com.trapster.android.Defaults;
import com.trapster.android.activity.fragment.util.TrapMapMarker;
import com.trapster.android.model.ImageResource;
import com.trapster.android.model.TrapType;
import com.trapster.android.model.dao.ImageResourceDAO;
import com.trapster.android.util.BackgroundTask;
import com.trapster.android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.GZIPInputStream;

/**
 * Bitmap Cache Manager.
 * <p/>
 * Enables caching on both disk and in-memory for the fastest access to bitmap
 * images
 */
@Singleton
public class BitmapCacheManager {

    private static final int DISK_CACHE_SIZE = 1024 * 1024 * 10; // 10MB

    private static final String DISK_CACHE_SUBDIR = "trapsterimages";

    private static final int APP_VERSION = 1;

    private static final int VALUE_COUNT = 1;

    @Inject
    ImageResourceDAO imageResourceDao;

    @Inject
    Application application;

    // For adjusting the downloaded and local bitmaps for the screen sizes
    @Inject
    WindowManager windowManager;
    @Inject
    TrapManager trapManager;

    @Inject
    ActivityManager activityManager;

    /*
     * Jakewharton port of the Android 4.x DiskLruCache class
     */
    private DiskLruCache mDiskCache;


    // In-Memory Cache
    private LruCache<String, Bitmap> mMemoryCache;

    private LruCache<String, SVG> svgMap;



    private static final String LOGNAME = "BitmapCacheManager";

    private Context context;

    private ExecutorService executorService;

    private static final String ASSET_IMAGE_DIRECTORY = "images";


    private List<String> assetList;

    private LruCache<String,Image> mapImageCache;

    public BitmapCacheManager() {
    }

    @Inject
    public void init() {
        this.context = application.getApplicationContext();

		/*
         * Initialize the file cache
		 */

        File cacheDir = getCacheDir(context, DISK_CACHE_SUBDIR);
        try {
            mDiskCache = DiskLruCache.open(cacheDir, APP_VERSION, VALUE_COUNT, DISK_CACHE_SIZE);
        } catch (IOException e) {
            e.printStackTrace();
        }

                /*
             * Initialize the memory cache
             */
        // Get memory class of this device, exceeding this amount will throw an
        // OutOfMemory exception.
        int memClass = activityManager.getMemoryClass();

        if (memClass <= 0)
            memClass = 16; // default heap size for java devices

        // Use 1/8th of the available memory for this memory cache.
        //final int cacheSize = 1024 * 1024 * memClass / 8;
        final int cacheSize = 1024 * 1024 * memClass / 8;
        {/*
                Log.i(LOGNAME, "Creatinng mem cache of size " + cacheSize); */
        }

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap value) {
                return value.getRowBytes() * value.getHeight();
            }
        };

        svgMap = new LruCache<String, SVG>(cacheSize);

        mapImageCache = new LruCache<String, Image>(cacheSize);


                /*
             * Set the http downloader pool size
             */
        executorService = Executors.newCachedThreadPool();

    }

    public static interface BitmapLoadedListener {
        void onBitmapLoaded(Bitmap bitmap);
    }

    public static class InvalidResourceKeyException extends Exception {
        public InvalidResourceKeyException(String detailMessage) {
            super(detailMessage);
        }
    }


    public void addMapImageToCache(String key, Image image) {

        if (key == null || image == null) {
            {/* 			//Log.e(LOGNAME, "ImageCache key or bitmap == null.  Key was: " + key); */}
            return;
        }

        // Add to memory cache as before
        synchronized (mapImageCache) {
            if (getImageFromCache(key) == null) {
                mapImageCache.put(key, image);
            }

        }
    }


    public Image getImageFromCache(String key) {
        return mapImageCache.get(key);
    }


    public void getMapImageSnapshot(final double latitude, final double longitude, final int zoomLevel, final int width, final int height, final TrapMapMarker icon, final BitmapLoadedListener listener) {

        // Log.v(LOGNAME, "Loading map:" + latitude + "," + longitude + ": " + width + "x" + height);


        new BackgroundTask() {

            @Override
            public void onExecute() {
                final String imageKey = "imagesnapshot-" + latitude + "-" + longitude + "-" + zoomLevel + "-" + width + "-" + height + "-" + icon.getTrap().getId();
                Bitmap bitmap = getBitmapFromCache(imageKey);

                if (bitmap == null) {


                    Map map = RestrictedMapFactory.createMap();
                    map.setCenter(MapFactory.createGeoCoordinate(latitude, longitude), MapAnimation.NONE);
                    map.setZoomLevel(zoomLevel);
                    map.addMapObject(icon.getMarker());

                    map.getBitmapAsync(width, height, new MapBitmapEventListener() {
                        @Override
                        public void onGetBitmapCompleted(Bitmap loadedBitmap) {
                            addBitmapToCache(imageKey, loadedBitmap, true);

                            if (listener != null)
                                listener.onBitmapLoaded(loadedBitmap);
                        }
                    });

                } else {
                    if (listener != null)
                        listener.onBitmapLoaded(bitmap);
                }
            }

            @Override
            public void onTimeout(long runTime) {

            }


        }.execute();

    }


    public void getMapImageSnapshot(double latitude, double longitude, int zoomLevel, int width, int height, BitmapLoadedListener listener) {

        StringBuilder builder = new StringBuilder();
        builder.append("http://m.nok.it/?app_id=");
        builder.append(Defaults.NOKIA_STATIC_MAP_APPID);
        builder.append("&token=");
        builder.append(Defaults.NOKIA_STATIC_MAP_TOKEN);
        builder.append("&ctr=");
        builder.append(latitude);
        builder.append(",");
        builder.append(longitude);
        builder.append("&h=");
        builder.append(height);
        builder.append("&w=");
        builder.append(width);
        builder.append("&z=");
        builder.append(zoomLevel);
        builder.append("&nord");

        String url = builder.toString();
        Bitmap bitmap = getBitmapFromCache(URLEncoder.encode(url));

        if (bitmap == null) {
            executorService
                    .submit(new MapImageLoader(url, width, height, listener));
        } else {
            if (listener != null)
                listener.onBitmapLoaded(bitmap);
        }

        //Map map = RestrictedMapFactory.getMap();

    }


    public Bitmap getBitmapFromImageResource(String key,
                                             BitmapLoadedListener listener) throws InvalidResourceKeyException {
        return getBitmapFromImageResource(key, listener, true);
    }

    /**
     * This method is used to optimize the access to any of the dynamic images.
     * The key is the Trapster image key. The image loading is designed to check
     * memory, disk and finally http locations for the image. In the
     *
     * @param key
     * @param listener
     * @return
     * @throws InvalidResourceKeyException
     */
    public Bitmap getBitmapFromImageResource(String key,
                                             BitmapLoadedListener listener, boolean downloadIfNecessary) throws InvalidResourceKeyException {

        ImageResource imageResource = imageResourceDao.findByKey(key);

        if (imageResource == null)
            throw new InvalidResourceKeyException("Unknown ImageResouceKey:"
                    + key);

        Bitmap bitmap = getBitmapFromCache(imageResource.getKey());

        if (bitmap == null) {
            if (imageResource.isPreload()) {

                String svgName = imageResource.getKey() + ".svg";

                String svgzName = imageResource.getKey() + ".svgz";

                if (checkIfInAssets(svgName) || checkIfInAssets(svgzName))
                    executorService.submit(new SvgLoader(imageResource, true));
                else
                    // Download from http
                    if (downloadIfNecessary)
                        executorService
                                .submit(new ImageLoader(imageResource, listener));
            } else {
                // Download from http
                if (downloadIfNecessary)
                    executorService
                            .submit(new ImageLoader(imageResource, listener));
            }
        }

        return bitmap;
    }

    public void preloadSVGImages() {
        List<TrapType> trapTypes = trapManager.getTrapTypes();
        if (trapTypes != null)
        {

            for(TrapType type:trapTypes){
                Log.e(LOGNAME, "Parsed " + new GsonBuilder().create().toJson(type.getLevels()));
            }

            for (TrapType trapType : trapTypes) {

                for(String key: trapType.getImageFilenames()){

                ImageResource imageResource = imageResourceDao.findByKey(key);

                    Log.e(LOGNAME, "PreLoading Image:" + key+":"+imageResource.toString());

                    if (imageResource != null) {



                        String svgName = imageResource.getKey() + ".svg";
                        String svgzName = imageResource.getKey() + ".svgz";

                        if (svgMap.get(svgName) == null && svgMap.get(svgzName) == null) // Not already loaded into memory
                        {
                            if (checkIfInAssets(svgName) || checkIfInAssets(svgzName)) // We have the assets available to preload
                                executorService.submit(new SvgLoader(imageResource, true));
                        }


                    }
                }


            }
        }



    }

    public Bitmap getScaledBitmapFromImageResource(String key, int width,
                                                   int height, BitmapLoadedListener listener)
            throws InvalidResourceKeyException {

        ImageResource imageResource = imageResourceDao.findByKey(key);

        if (imageResource == null)
            throw new InvalidResourceKeyException("Unknown ImageResouceKey:" + key);

		/*
         * Scale the image based on the screen resolution. HDPI is the baseline resolution
		 */
        width = (int) ((float) width * getScalingImageDensity());
        height = (int) ((float) height * getScalingImageDensity());

        String imageKey = imageResource.getKey() + String.valueOf(width) + String.valueOf(height);

        Bitmap bitmap = getBitmapFromCache(imageKey);

        if (bitmap == null) {
            Bitmap originalBitmap = getBitmapFromCache(imageResource.getKey());

            if (originalBitmap == null) {
                if (imageResource.isPreload()) {
                    executorService.submit(new SvgLoader(imageResource, width, height, listener));
                } else {
                    // Download from http
                    executorService.submit(new ImageLoader(imageResource, width, height, listener));
                }
            } else {
                {/*                 Log.i(LOGNAME, "Found original bitmap for " + imageResource.getKey() + ".  Rescaling using key " + key); */}
                Bitmap scaledBitmap = resizeBitmap(originalBitmap, width, height);
                addBitmapToCache(imageKey, scaledBitmap, true);
                listener.onBitmapLoaded(scaledBitmap);
            }

        } else
            listener.onBitmapLoaded(bitmap);

        return bitmap;

    }

    private Bitmap getBitmapFromAssetsDirectory(ImageResource resource,
                                                int width, int height) {

        String pngName = resource.getKey() + ".png";

        String svgName = resource.getKey() + ".svg";

        String svgzName = resource.getKey() + ".svgz";

	

		/*if (checkIfInAssets(pngName)) {

			//Log,.v(LOGNAME,
					"Loading PNG  into bitmap cache:" + resource.getKey());

			// @TODO if we use png's they should be scaled by width/height
			InputStream in = null;
			try {
				try {

					in = context.getAssets().open(
							ASSET_IMAGE_DIRECTORY + "/" + pngName,
							AssetManager.ACCESS_STREAMING);
					return BitmapFactory.decodeStream(in);
				} finally {
					if (in != null)
						in.close();
				}

			} catch (IOException e) {
			}

		}*/
        if (checkIfInAssets(svgName)) {

            ////Log,.v(LOGNAME, "Loading SVG into bitmap cache:" + resource.getKey());
			/*
			 * Using svg assets. SVG are loaded once and rendered to the size
			 * required
			 */
            try {
                SVG svg = SVGParser.getSVGFromAsset(context.getAssets(),
                        ASSET_IMAGE_DIRECTORY + "/" + svgName);

                svgMap.put(resource.getKey() + ".svg", svg);

                // Config conf = Bitmap.Config.ARGB_8888; // see other conf types
                Config conf = Config.ARGB_4444; // see other conf types
                Bitmap bmp = Bitmap.createBitmap(width, height, conf); // this
                // creates
                // a
                // MUTABLE
                // bitmap

                Canvas canvas = new Canvas(bmp);
                canvas.drawPicture(svg.getPicture(), new Rect(0, 0, width,
                        height));

                return bmp;

            } catch (SVGParseException e) {
                // //Log.e(LOGNAME, "Invalid SVG:" + Log.getStackTraceString(e));
                {/* 				//Log.e(LOGNAME, "Invalid SVG:" + e.getMessage()); */}
            } catch (IOException e) {
                {/* 				//Log.e(LOGNAME, "Cannot load ImageResource:" + e.getMessage()); */}
            }

        }
        if (checkIfInAssets(svgzName)) {

            ////Log,.v(LOGNAME,"Loading SVGZ into bitmap cache:" + resource.getKey());
			/*
			 * Using svg assets. SVG are loaded once and rendered to the size
			 * required
			 */
            try {
                // SVG svg = SVGParser.getSVGFromAsset(context.getAssets(),
                // ASSET_IMAGE_DIRECTORY+"/"+svgName);

                InputStream is = context.getAssets().open(
                        ASSET_IMAGE_DIRECTORY + "/" + svgzName,
                        AssetManager.ACCESS_STREAMING);
                GZIPInputStream gis = new GZIPInputStream(is);

                SVG svg = SVGParser.getSVGFromInputStream(gis);

                //Config conf = Bitmap.Config.ARGB_8888; // see other conf types
                Config conf = Config.ARGB_4444; // see other conf types
                Bitmap bmp = Bitmap.createBitmap(width, height, conf); // this
                // creates
                // a
                // MUTABLE
                // bitmap

                Canvas canvas = new Canvas(bmp);
                canvas.drawPicture(svg.getPicture(), new Rect(0, 0, width,
                        height));

                return bmp;

            } catch (SVGParseException e) {
                // //Log.e(LOGNAME, "Invalid SVG:" + Log.getStackTraceString(e));
                {/* 				//Log.e(LOGNAME, "Invalid SVG:" + e.getMessage()); */}
            } catch (IOException e) {
                {/* 				//Log.e(LOGNAME, "Cannot load ImageResource:" + e.getMessage()); */}
            }

        }

        return null;
    }

    public Drawable getDrawableFromSvg(ImageResource imageResource) {
        if (imageResource != null && svgMap != null)
        {
            String svgName = imageResource.getKey() + ".svg";

            SVG svg = svgMap.get(svgName);
            if (svg != null)
                return svg.createPictureDrawable();
            else
                preloadSVGImages();
        }
            return null;
    }

    private boolean checkIfInAssets(String assetName) {
        if (assetList == null) {
            try {
                assetList = Arrays.asList(context.getAssets().list(
                        ASSET_IMAGE_DIRECTORY));
            } catch (IOException e) {
            }
        }
        return assetList.contains(assetName) ? true : false;
    }

    public void removeBitmapFromCache(String key) {

        mMemoryCache.remove(key);

        try {
            mDiskCache.remove(key);
        } catch (IOException e) {
            {/* 			Log.w(LOGNAME, "Key:" + key + " no in diskcache"); */}
        }
    }

    /**
     * Pulls an image from the cache. Note: this will not download the image.
     * Don't use this for ImageResources as it doesn't run any checks. Useful
     * for a generic image cache
     *
     * @param imageKey
     * @return
     */
    public Bitmap getBitmapFromCache(String imageKey) {

        Bitmap bitmap = getBitmapFromMemCache(imageKey);


        if (bitmap == null) {
            bitmap = getBitmapFromDiskCache(imageKey);
        }

        return bitmap;
    }

    /**
     * Adds the image into the cache. Cache is both in-memory and on-disk.
     *
     * @param key
     * @param bitmap
     * @param useDiskCache
     */
    public void addBitmapToCache(String key, Bitmap bitmap, boolean useDiskCache) {

        if (key == null || bitmap == null) {
            {/* 			//Log.e(LOGNAME, "ImageCache key or bitmap == null.  Key was: " + key); */}
            return;
        }
        {/*
        Log.i(LOGNAME, "Successfully loaded bitmap for key " + key); */
        }

        // Add to memory cache as before
        synchronized (mMemoryCache) {
            if (getBitmapFromMemCache(key) == null) {
                mMemoryCache.put(key, bitmap);
                {/*                 Log.i(LOGNAME, "Added bitmap to mem cache for key " + key); */}
            } else {/*                 Log.i(LOGNAME, key + " already loaded into mem cache"); */}

        }

        if (useDiskCache) {
            // Also add to disk cache
            try {
                if (mDiskCache.get(key) == null) {
                    synchronized (mDiskCache) {
                        DiskLruCache.Editor edit = mDiskCache.edit(key);
                        writeBitmapToFile(bitmap, edit);
                        mDiskCache.flush();
                        edit.commit();
                    }
                }
            } catch (Exception e) {
                {/* 				//Log.e(LOGNAME,
						"Unable to write to disk cache:" + e.getMessage()); */
                }
            }
        }
    }

    private boolean writeBitmapToFile(Bitmap bitmap, DiskLruCache.Editor editor)
            throws IOException, FileNotFoundException {
        OutputStream out = null;
        try {
            out = new BufferedOutputStream(editor.newOutputStream(0), 8192);
            return bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    private static File getCacheDir(Context context, String uniqueName) {

        final String cachePath = context.getCacheDir().getPath();

        return new File(cachePath + File.separator + uniqueName);
    }

    private Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

    private Bitmap getBitmapFromDiskCache(String key) {
        Bitmap bitmap = null;
        DiskLruCache.Snapshot snapshot = null;
        try {

            snapshot = mDiskCache.get(key);
            if (snapshot == null) {
                return null;
            }
            final InputStream in = snapshot.getInputStream(0);
            if (in != null) {
                final BufferedInputStream buffIn = new BufferedInputStream(in,
                        8192);
                bitmap = BitmapFactory.decodeStream(buffIn);
            }
        } catch (IOException e) {
            {/* 			//Log.e(LOGNAME, "Unable to read disk file:" + e.getMessage()); */}
        } finally {
            if (snapshot != null) {
                snapshot.close();
            }
        }
        return bitmap;
    }

    private float getScalingImageDensity() {
        // Scale for the relative screen size
        DisplayMetrics metrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(metrics);

        switch (metrics.densityDpi) {
            case DisplayMetrics.DENSITY_LOW:
                return 0.50f;
            case DisplayMetrics.DENSITY_MEDIUM:
                return 0.67f;
            case DisplayMetrics.DENSITY_HIGH:
                return 1.0f;
            case DisplayMetrics.DENSITY_XHIGH:
                return 1.33f;
        }

        return 1.0f;
    }


    private class MapImageLoader implements Runnable {

        private BitmapLoadedListener listener;
        private int width;
        private int height;
        private String url;

        public MapImageLoader(String url, int width, int height,
                              BitmapLoadedListener listener) {
            super();
            this.url = url;
            this.width = width;
            this.height = height;
            this.listener = listener;
        }

        @Override
        public void run() {
            {/* 			//Log.d(LOGNAME, "Loading Image from:" + url); */}
            Bitmap bitmap = getBitmapFromUrl(url);
            if (bitmap != null) {
                //Bitmap newBitmap = resizeBitmap(bitmap, width, height);

                Bitmap rounded = addRoundedCornerBitmap(bitmap, 10);

                addBitmapToCache(URLEncoder.encode(url), rounded, true);

                if (listener != null)
                    listener.onBitmapLoaded(rounded);
            } else {
                // Handle image can't be loaded
                //listener.onBitmapError()
            }


        }

        private Bitmap getBitmapFromUrl(String url) {

            try {
                URL imageUrl = new URL(url);
                HttpURLConnection conn = (HttpURLConnection) imageUrl
                        .openConnection();
                conn.setConnectTimeout(30000);
                conn.setReadTimeout(30000);
                conn.setInstanceFollowRedirects(true);
                InputStream is = conn.getInputStream();
                return BitmapFactory.decodeStream(is);
            } catch (Exception ex) {
                {/* 				//Log.e(LOGNAME, "Error downloading image:" + ex.getMessage()); */}
                return null;
            }
        }

    }


    private class SvgLoader implements Runnable {

        private ImageResource imageResource;
        private int width;
        private int height;
        private String imageKey;
        private BitmapLoadedListener listener;
        private boolean saveSVG;

        public SvgLoader(ImageResource imageResource, boolean saveSVG) {
            this.imageResource = imageResource;
            this.width = imageResource.getSpec().getWidth();
            this.height = imageResource.getSpec().getHeight();
            this.imageKey = imageResource.getKey();
            this.listener = null;
            this.saveSVG = saveSVG;

        }

        public SvgLoader(ImageResource imageResource, int width, int height, BitmapLoadedListener listener) {
            this.imageResource = imageResource;
            this.width = width;
            this.height = height;
            this.imageKey = imageResource.getKey() + String.valueOf(width)
                    + String.valueOf(height);
            this.listener = listener;

        }

        @Override
        public void run() {
            // Load from assets directory
            Bitmap bitmap = getBitmapFromAssetsDirectory(imageResource, width,
                    height);
            addBitmapToCache(imageKey, bitmap, false);

            if (listener != null)
                listener.onBitmapLoaded(bitmap);
        }

    }

    private class ImageLoader implements Runnable {

        private ImageResource imageResource;
        private BitmapLoadedListener listener;
        private int width;
        private int height;
        private String imageKey;

        public ImageLoader(ImageResource imageResource,
                           BitmapLoadedListener listener) {
            super();
            this.imageResource = imageResource;
            this.width = imageResource.getSpec().getWidth();
            this.height = imageResource.getSpec().getHeight();
            this.imageKey = imageResource.getKey();
            this.listener = listener;
        }

        public ImageLoader(ImageResource imageResource, int width, int height,
                           BitmapLoadedListener listener) {
            super();
            this.imageResource = imageResource;
            this.width = width;
            this.height = height;
            this.imageKey = imageResource.getKey() + String.valueOf(width)
                    + String.valueOf(height);
            this.listener = listener;
        }

        @Override
        public void run() {


            Bitmap bitmap = getBitmapFromUrl(imageResource.getPath());
            if (bitmap != null) {
                Bitmap newBitmap = resizeBitmap(bitmap, width, height);
                addBitmapToCache(imageKey, newBitmap, true);
            }
            if (listener != null)
                listener.onBitmapLoaded(bitmap);
        }

        private Bitmap getBitmapFromUrl(String url) {

            try {
                URL imageUrl = new URL(url);
                HttpURLConnection conn = (HttpURLConnection) imageUrl
                        .openConnection();
                conn.setConnectTimeout(30000);
                conn.setReadTimeout(30000);
                conn.setInstanceFollowRedirects(true);
                InputStream is = conn.getInputStream();
                return BitmapFactory.decodeStream(is);
            } catch (Exception ex) {
                {/* 				//Log.e(LOGNAME, "Error downloading image:" + ex.getMessage()); */}
                return null;
            }
        }

    }

    private Bitmap resizeBitmap(Bitmap bm, int newHeight, int newWidth) {
        {/* 		//Log.d(LOGNAME, "Resizing Bitmap:"+newHeight+":"+newWidth); */}


        int width = bm.getWidth();
        int height = bm.getHeight();

        // Add this to avoid unnecessary scaling
        if (width == newWidth && height == newHeight)
            return bm;

        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // RECREATE THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
                matrix, false);
        return resizedBitmap;
    }

    private Bitmap addRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Config.ARGB_4444);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

}