package com.trapster.android.manager;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.trapster.android.Defaults;
import com.trapster.android.comms.CommunicationManager;
import com.trapster.android.comms.DataLoadedListener;
import com.trapster.android.comms.ITrapsterAPI;
import com.trapster.android.comms.SoundThemeListener;
import com.trapster.android.comms.php.ResourceStorageParser;
import com.trapster.android.model.SoundLink;
import com.trapster.android.model.SoundTheme;
import com.trapster.android.model.SoundTheme.State;
import com.trapster.android.model.SoundThemes;
import com.trapster.android.model.provider.db.SoundThemeDatabaseAdapter;
//import com.trapster.android.util.Log;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;
import com.twolinessoftware.android.framework.service.comms.HttpGetBinaryWorker;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Singleton
public class SoundThemeManager implements SoundThemeListener,
        CommunicationStatusListener,
        ResourceStorageParser.ResourceLoadedListener {

    private String LOGNAME = "Trapster.SoundThemeManager";

    private boolean themesLoaded;
    private Context context;

    private DataLoadedListener listener;
    private int activeThreads;


    // This really should be injected and accessed using contentproviders instead of through this class
    private SoundThemeDatabaseAdapter themeProvider;
    //
    @Inject
    ITrapsterAPI api;
    @Inject
    private CommunicationManager cm;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    Application application;
    @Inject
    TrapManager trapManager;
    // -1= notLoaded; 0= notSet
    int soundThemeId = -1;
    SoundTheme soundTheme;
    private SoundThemeListener soundThemeListener;

    // For testing, remove
    private long startTime;


    public void SoundThemeManager() {

    }

    @Inject
    public void Init() {
        // //Log,.v(LOGNAME, "SoundThemeManager initialized");
        this.context = application.getApplicationContext();
        themeProvider = new SoundThemeDatabaseAdapter(context);

         /*
        This will download all the themes and mp3s. We don't really want that do we?
         */
        /*new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... voids) {
		        themeProvider = new SoundThemeDatabaseAdapter(context);

                ArrayList<SoundTheme> themes = themeProvider.getSoundThemes();
                for (SoundTheme theme : themes)
                {
                    downloadTheme(theme, false);
                }
                if (themes.size() > 0)
                    setSoundResource(themes);

				return null;
			}
		}.execute(null, null);
        */
    }

    private void downloadThemePreviews() {
        for (SoundTheme theme : themeProvider.getSoundThemes()) {
            downloadThemePreview(theme, "rlc");
        }
    }

    public DataLoadedListener getListener() {
        return listener;
    }

    public void setListener(DataLoadedListener listener) {
        this.listener = listener;
    }

    public void retrieveThemeList() {

        long lastUpdateTime = sharedPreferences.getLong(
                Defaults.PREFERENCE_LAST_UPDATE_TIME_SOUNDTHEME, 0);
        // Log.d(LOGNAME,"Getting SoundThemes as of:"+lastUpdateTime);

        startTime = System.currentTimeMillis();
        api.listSounds(lastUpdateTime, this, this);
    }

    public void retrieveThemeList(SoundThemeListener soundThemeListener) {
        long lastUpdateTime = sharedPreferences.getLong(
                Defaults.PREFERENCE_LAST_UPDATE_TIME_SOUNDTHEME, 0);

        api.listSounds(lastUpdateTime, this, this);
        this.soundThemeListener = soundThemeListener;
    }

    private void updateTheme(SoundTheme theme) {
        themeProvider.update(theme);
    }

    public void deleteLocalFiles(SoundTheme t) {
        // synchronized(themes){
        SoundTheme theme = getTheme(t.getId());
        theme.clearLocalReferences();
        for (SoundLink link : theme.getLinks())
            deleteFile(theme, link);
        updateTheme(theme);
        // }
    }

    private void deleteFile(SoundTheme theme, SoundLink link) {
        File file = new File(Defaults.STORAGE_PATH
                + getDefaultReference(theme, link));
        if (file.exists())
            file.delete();
    }

    private void downloadThemePreview(SoundTheme theme, String lid) {

        if (!hasFileCache(theme)) {
            theme.setState(State.loading);
            updateTheme(theme);
            ArrayList<SoundLink> tempLinks = theme.getLinks();

            for (SoundLink link : tempLinks) {
                if (link.getLid().equalsIgnoreCase(lid)) {
                    if (!hasFileCache(theme, link)) {
                        addActiveConnection();
                        downloadTheme(theme, link);
                    } else {
                        if (listener != null)
                            listener.onDataLoadComplete();
                    }
                    break;
                }
            }

        } else {
            if (listener != null)
                listener.onDataLoadComplete();
        }
    }

    public void downloadTheme(SoundTheme theme, boolean force) {
		
		
		/*Log.v(LOGNAME, "Downloading "+theme.getName());
		for(SoundLink link:theme.getLinks())
			Log.v(LOGNAME, "      Link " + link.getLid());
		*/
        if (!hasFileCache(theme) || force) {
            theme.setState(State.loading);
            updateTheme(theme);
            for (SoundLink link : theme.getLinks()) {
                addActiveConnection();
                downloadTheme(theme, link);
            }
        } else {
            if (listener != null)
                listener.onDataLoadComplete();
        }
    }

    public SoundTheme getTheme(int id) {
        return themeProvider.getSoundTheme(id);
    }

    public SoundTheme getCurrentTheme() {
        if (this.soundThemeId == -1) {
            int soundThemeId = sharedPreferences.getInt("SoundTheme.ID", 0);
            //if (soundThemeId != 0)
                this.soundTheme = getTheme(soundThemeId);
            this.soundThemeId = soundThemeId;
        }

        return this.soundTheme;
    }

    public void setCurrentTheme(SoundTheme theme) {
        Editor edit = sharedPreferences.edit();
        if (theme != null) {
            int soundThemeId = theme.getId();
            edit.putInt("SoundTheme.ID", soundThemeId);
            //
            this.soundThemeId = soundThemeId;
            this.soundTheme = theme;
        } else {
            edit.remove("SoundTheme.ID");
            //
            this.soundThemeId = 0;
            this.soundTheme = null;
        }
        edit.commit();
    }

    public ArrayList<SoundTheme> getSoundThemes() {
        return themeProvider.getSoundThemes();
    }

    public boolean isThemesLoaded() {
        return this.themesLoaded;
    }

    public boolean hasFileCache(SoundTheme theme) {
        boolean loaded = true;
        if (theme != null && theme.getLinks() != null)
        {
            for (SoundLink link : theme.getLinks()) {
                if (!hasFileCache(theme, link)) {
                    loaded = false;
                    break;
                }
            }
        }
        return loaded;
    }

    private boolean hasFileCache(SoundTheme theme, SoundLink link) {


        File file = new File(Defaults.STORAGE_PATH
                + getDefaultReference(theme, link));

        // Log.d(LOGNAME,"Checking:"+Defaults.STORAGE_PATH
        //         + getDefaultReference(theme, link)+" :"+file.exists());
        return file.exists();
    }

    private String getDefaultReference(SoundTheme theme, SoundLink link) {
        String fullFile = theme.getId() + "_" + theme.getVersion() + "_"
                + link.getLid();
        return fullFile;
    }

    @Override
    public void onError(TrapsterError error) {
        //Log.e(LOGNAME, "Theme load error:" + error.getDetails());

        closeActiveConnection();
        if (listener != null)
            listener.onDataLoadError();
        if (soundThemeListener != null)
            soundThemeListener.onSoundThemeChanges(null);
    }

    @Override
    public void onConnectionError(String errorMessage) {
        //Log.e(LOGNAME, "Theme load error(connection):" + errorMessage);

        closeActiveConnection();
        if (listener != null)
            listener.onDataLoadError();
    }

    private void downloadTheme(SoundTheme theme, SoundLink link) {
        ResourceHolder r = new ResourceHolder();
        r.link = link;
        r.theme = theme;
        String fullFile = theme.getId() + "_" + theme.getVersion() + "_"
                + link.getLid();

        // android.util.Log.d(LOGNAME, "Downloading:"+link.getFormats().getMp3()+" to "+fullFile);

        HttpGetBinaryWorker worker = new HttpGetBinaryWorker(link.getFormats()
                .getMp3(), context, fullFile);
        ResourceStorageParser parser = new ResourceStorageParser(r, this);
        cm.handleAsyncMessage(worker, parser, this);
    }

    public static String extractFileNameFromUrl(String fullName) {
        Pattern p = Pattern.compile(".*?([^\\\\/]+)$");
        Matcher m = p.matcher(fullName);
        return (m.find()) ? m.group(1) : "";
    }

    public static boolean hasFileCache(Context ctx, String filename) {
        String fullpath = ctx.getFilesDir() + "/" + filename;
        File file = new File(fullpath);
        return file.exists();
    }

    @Override
    public void onCloseConnection() {
        // //Log.d(LOGNAME, "Connection to server closed");
    }

    @Override
    public void onOpenConnection() {
        // //Log.d(LOGNAME, "Opening Connection To Server:");
    }

    @Override
    public void onResourceLoaded(Object resource, String filename) {
        ResourceHolder r = (ResourceHolder) resource;

        //	Log.d(LOGNAME, "Downloaded theme:"+filename);

        themeProvider.addResource(r.theme.getId(), r.link.getLid(), filename);

        if (soundTheme != null && r.theme.getId() == soundTheme.getId())
            soundTheme.addSoundReference(r.link.getLid(),
                    getDefaultReference(r.theme, r.link));


        finalizeActiveConnection();
    }

    private ArrayList<SoundTheme> setSoundResource(ArrayList<SoundTheme> themes) {
        int cached = 0;
        // Set the filename for downloaded themes
        Iterator<SoundTheme> i = themes.iterator();
        int j = 0;
        while (i.hasNext()) {
            SoundTheme theme = i.next();
            if (hasFileCache(theme)) {
                cached++;
                // Set the filenames in the resources
                for (SoundLink link : theme.getLinks()) {
                    theme.addSoundReference(link.getLid(),
                            getDefaultReference(theme, link));
                }
                themes.set(j, theme);
            }
            //else
            //    downloadTheme(theme, true);
            j++;
        }
        // Log.i(LOGNAME, "Loaded " + themes.size() +
        // " Themes. Themes cached =" + cached);
        return themes;
    }

    @Override
    public void onSoundThemeChanges(SoundThemes _themes) {

        themesLoaded = true;
        //Log.d(LOGNAME,"SoundTheme List +"+_themes.getAdded().size()+" New Themes. Download Time:"+(System.currentTimeMillis() - startTime)+" ms");

        if (_themes != null && _themes.getAdded().size() > 0) {
            ArrayList<SoundTheme> themes = _themes.getAdded();
            themes = setSoundResource(themes);

            themeProvider.setSoundThemes(themes);

            if (soundThemeListener != null)
                soundThemeListener.onSoundThemeChanges(_themes);

            downloadThemePreviews();

            if (themes.size() > 0)
                sharedPreferences.edit().putLong(Defaults.PREFERENCE_LAST_UPDATE_TIME_SOUNDTHEME, System.currentTimeMillis() / 1000).commit();
        }
    }

    class ResourceHolder {
        public SoundTheme theme;
        public SoundLink link;
    }

    private void addActiveConnection() {
        activeThreads++;
    }

    private void closeActiveConnection() {
        activeThreads--;
    }

    private void finalizeActiveConnection() {
        activeThreads--;
        // //Log.d(LOGNAME, "Active Threads:" + activeThreads);
        activeThreads = (activeThreads < 0) ? 0 : activeThreads;
        if (activeThreads == 0) {
            if (listener != null)
                listener.onDataLoadComplete();
        }
    }
}