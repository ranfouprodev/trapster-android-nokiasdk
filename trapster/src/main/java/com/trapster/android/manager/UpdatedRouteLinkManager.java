package com.trapster.android.manager;

import android.app.Application;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.trapster.android.comms.ITrapsterAPI;
import com.trapster.android.comms.UpdatedRouteLinkListener;
import com.trapster.android.model.UpdatedRouteLinkInfo;
import com.trapster.android.model.dao.UpdatedRouteLinkDAO;
import com.trapster.android.model.provider.StatsProviderUtil;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 12/8/12
 * Time: 11:43 AM
 * Manager for accessing the database and pushing data to the server
 */

@Singleton
public class UpdatedRouteLinkManager implements UpdatedRouteLinkListener, CommunicationStatusListener
{

    @Inject Application application;
    @Inject ITrapsterAPI api;
    @Inject UpdatedRouteLinkDAO dao;

    public UpdatedRouteLinkManager ()
    {

    }

    @Inject
    public void init() {

    }

    public void pushNewSpeedLimitToServer(ArrayList<UpdatedRouteLinkInfo> updatedRouteLinkInfo)
    {
        api.updateNewSpeedLimit(updatedRouteLinkInfo, null);

        StatsProviderUtil.incrementStatByInt(application.getApplicationContext(), StatsProviderUtil.STAT_REPORT, 1);

    }
    public void saveRouteLinkExtraInfo(ArrayList<UpdatedRouteLinkInfo> updatedRouteLinkInfo)
    {
        dao.save(updatedRouteLinkInfo);
    }
    public UpdatedRouteLinkInfo findRouteLinkExtraInfo(String link_Id)
    {
        return dao.findByRouteLink(link_Id);
    }
    public void getSpeedLimitFromServer()
    {
          api.getNewSpeedLimit(this, this);
    }

    @Override
    public void onUpdatedRouteLinkReceived(ArrayList<UpdatedRouteLinkInfo> updatedRouteLinkInfo) {
        int numRowsDeleted = dao.clearCache();
        dao.save(updatedRouteLinkInfo);
    }

    @Override
    public void onError(TrapsterError error) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onOpenConnection() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onCloseConnection() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onConnectionError(String errorMessage) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
