package com.trapster.android.manager.position;

import com.here.android.common.*;
import com.here.android.mapping.MapFactory;

import java.util.concurrent.CopyOnWriteArrayList;

public class NokiaPositionManager implements IPositionManager {

	private static final String LOGNAME = "NokiaPositionManager";
	
	// Nokia Implementation of the PositionManager
	private PositioningManager pm;
	
	private CopyOnWriteArrayList<PositionListener> listeners = new CopyOnWriteArrayList<PositionListener>();
	
	public NokiaPositionManager() {
		pm = MapFactory.getPositioningManager();
	}

	public void start() {
{/* 		Log.i(LOGNAME, "Starting Nokia Position Manager"); */}

		pm.start(LocationMethod.GPS_NETWORK);
	}

	public void stop() {
{/* 		Log.i(LOGNAME, "Stopping Position Manager"); */}

		// Removing all the listeners will cause the location to stop working.
		for (PositionListener listener : listeners)
			pm.removePositionListener(listener);

		pm.stop();

	}

	public GeoPosition getLastLocation() {
		return pm.getPosition();
	}

	public void addPositionListener(PositionListener listener) {
		listeners.add(listener);
		pm.addPositionListener(listener);
	}

	public void removePositionListener(PositionListener listener) {
		listeners.remove(listener);
		pm.removePositionListener(listener);
	}

	@Override
	public LocationStatus getStatus() {
		return pm.getLocationStatus(LocationMethod.GPS_NETWORK);
	}


}
