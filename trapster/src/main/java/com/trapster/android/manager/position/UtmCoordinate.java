package com.trapster.android.manager.position;

public class UtmCoordinate {

	private int zone;
	private double easting;
	private double northing;

	public UtmCoordinate(int zone, double easting, double northing) {
		super();
		this.zone = zone;
		this.easting = easting;
		this.northing = northing;
	}

	public UtmCoordinate() {
		zone = 0;
		easting = 0;
		northing = 0;
	}

	public int getZone() {
		return zone;
	}

	public void setZone(int zone) {
		this.zone = zone;
	}

	public double getEasting() {
		return easting;
	}

	public void setEasting(double easting) {
		this.easting = easting;
	}

	public double getNorthing() {
		return northing;
	}

	public void setNorthing(double northing) {
		this.northing = northing;
	}

}
