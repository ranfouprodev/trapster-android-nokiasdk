package com.trapster.android.manager;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.OnAccountsUpdateListener;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;

import com.facebook.Session;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.here.android.common.GeoPosition;
import com.trapster.android.Defaults;
import com.trapster.android.comms.BasicResponseListener;
import com.trapster.android.comms.ITrapsterAPI;
import com.trapster.android.comms.LoginListener;
import com.trapster.android.comms.SessionListener;
import com.trapster.android.comms.StatisticsListener;
import com.trapster.android.comms.rest.IUserAPI;
import com.trapster.android.comms.rest.StatusCode;
import com.trapster.android.manager.flurry.FLURRY_EVENT_NAME;
import com.trapster.android.manager.flurry.FLURRY_PARAMETER_NAME;
import com.trapster.android.manager.flurry.FlurryEvent;
import com.trapster.android.model.Profile;
import com.trapster.android.model.User;
import com.trapster.android.model.UserStatistics;
import com.trapster.android.util.GeographicUtils;
import com.trapster.android.util.Log;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;

import java.lang.reflect.Modifier;
import java.util.ArrayList;

@Singleton
public class SessionManager implements OnAccountsUpdateListener,LoginListener, CommunicationStatusListener
{
    private static final long USER_STATS_CHECK_INTERVAL = 1000 * 60; // 1 Minute
    public static final String ACCOUNT_TYPE = "com.trapster";
    public static final String TOKEN_TYPE = "MAIN APP";

    @Inject AccountManager am;

	@Inject Application application;
	
	@Inject ITrapsterAPI api;


    /**
     * New User API, oAuth
     */
    @Inject
    IUserAPI userApi;
	
	@Inject SharedPreferences sharedPreferences;
	
	@Inject TrapManager trapManager;

    @Inject PositionManager positionManager;

    private static final int MAX_PHONE_NUMBER_LENGTH = 10;

    @Inject
    TelephonyManager tm;

    private final Handler handler = new Handler();

    public static final String INTENT_BROADCAST_ACTION_LOGON= "com.trapster.android.manager.action.LOGON";
    private final UserStatsCheckRunnable userStatsCheckRunnable = new UserStatsCheckRunnable();
	//
	static final String LOGNAME= "Trapster.SessionManager";

    private static final String PREF_CRED = "";

	SessionListener sessionListener;
	//
	User user;

	final User demoUser = new User(Defaults.DEMO_USERNAME,Defaults.DEMO_PASSWORD);

    /*
	 * used to contextualize the application on first login or register
	 */
	boolean firstLogin;

    private ArrayList<UserFeedbackListener> userFeedbackListeners;

	private Context context;
    private Gson gson;

    public SessionManager()
	{
		
	}
	
	@Inject
	public void init() {
		this.context= application.getApplicationContext();

        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.TRANSIENT);
        gson = builder.create();

        // Check and automatically migrate users off the old system
        checkAndMigrateAccounts();

        userFeedbackListeners = new ArrayList<UserFeedbackListener>();

        handler.postDelayed(userStatsCheckRunnable, USER_STATS_CHECK_INTERVAL);


        am.addOnAccountsUpdatedListener(this,null,true);

    }

    private void checkAndMigrateAccounts() {

        SharedPreferences credentials = context.getSharedPreferences(Defaults.PREF_CREDENTIALS, Context.MODE_PRIVATE);
        boolean hasCredentials =  credentials.contains("jsonprefcred");
        if(hasCredentials){
            String jsonString = credentials.getString(PREF_CRED,null);
            user = gson.fromJson(jsonString, User.class);

            addAccountToManager();

            credentials.edit().remove("jsonprefcred");
        }

    }

    public boolean isAttributesLoaded(){
		long lastUpdateTimeAttributes = sharedPreferences.getLong(
				Defaults.PREFERENCE_LAST_UPDATE_TIME_ALLATTRIBUTES, 0);
		return lastUpdateTimeAttributes != 0; 
	}
	
	public boolean isLoggedIn() {
        return user != null;
    }

    private Account getAccount(){
        Account[] accounts = am.getAccountsByType(ACCOUNT_TYPE);
        return accounts.length > 0 ? accounts[0] : null;
    }

    public boolean isModerator(){
        return getUser() != null && getUser().getProfile() != null && getUser().getProfile().getGlobalModerator() > 0;
    }
	
	public void autoLogin(SessionListener listener)
	{
		user = null;

        if(hasCredentials()){

            String jsonString = am.getUserData(getAccount(),"user");
            User user = gson.fromJson(jsonString, User.class);
            api.login(user, this, this);
        }else{
            api.login(demoUser, this, this);
        }

        sessionListener= listener;

        sendFlurryAutoLoginEvent(true);
	}

	public User getDemoUser()
	{
		return demoUser;
	}

	public User getUser()
	{
		return user;
	}

	public boolean hasCredentials()
	{
        return getAccount() != null;
	}
	public void login(User u, String code, SessionListener listener)
	{
		user = null;

		sessionListener= listener;
		
		//
		if (code != null)
			api.login(u, code, this, this);
		else
			api.login(u, this, this);
	}

	public void logout()
	{
		user = null;

        if(getAccount() != null)
		    am.removeAccount(getAccount(),null,new Handler());

        sendFlurryLogoutEvent();

		// FB Logout
        if (Session.getActiveSession() != null) {
            Session.getActiveSession().closeAndClearTokenInformation();
        }

        Session.setActiveSession(null);

		// Clear the trap cache
        trapManager.deleteAllTraps();

        GeoPosition position = positionManager.getLastLocation();
        if(position != null)
            trapManager.requestTrapUpdate(position.getCoordinate().getLatitude(), position.getCoordinate().getLongitude(), GeographicUtils.kilometersToMiles(Defaults.BACKGROUND_TRAPS_UPDATE_DISTANCE/1000),true);

		
	}

	public void onCloseConnection()
	{
	}

	public void onConnectionError(String errorMessage)
	{
		if (sessionListener != null)
			sessionListener.onError(new TrapsterError(TrapsterError.TYPE_COMMUNICATION_ERROR,
					"We are sorry we are unable to reach trapster.com at this time."));
	}
	public void onError(TrapsterError error)
	{
		if (sessionListener != null)
		   sessionListener.onError(error);
	}

    private void checkUserFeedbackDeltas(UserStatistics userStatistics)
    {
        if (userFeedbackListeners.size() > 0)  // If nobody is listening, we really don't care
        {
            int currentUsersHelped = userStatistics.getVotesOnMyTrapsStatistics().getNumVotes() + userStatistics.getTrapStatistics().getTrapReportCount() + userStatistics.getMyVoteStatistics().getNumVotes();
            int oldUsersHelped = sharedPreferences.getInt(Defaults.PREFERENCE_STATS_USERS_HELPED, 0);
            if (currentUsersHelped != oldUsersHelped)
            {
                for (UserFeedbackListener listener : userFeedbackListeners)
                {
                    listener.onUsersHelpedChanged(oldUsersHelped, currentUsersHelped);
                }
                sharedPreferences.edit().putInt(Defaults.PREFERENCE_STATS_USERS_HELPED, currentUsersHelped).commit();

            }
        }
    }

    public void addUserFeedbackListener(UserFeedbackListener userFeedbackListener)
    {
        userFeedbackListeners.add(userFeedbackListener);
    }

    public void removeUserFeedbackListener(UserFeedbackListener userFeedbackListener)
    {
        userFeedbackListeners.remove(userFeedbackListener);
    }

    @Override
    public void onLoginSuccess(User user, String optionalMessage) {

        try{

            user.setTempPassword(null);

            this.user = user;

            requestUserStatistics();

            addAccountToManager();

            if (sessionListener != null)
            {
                sessionListener.onLoginSuccess(user, optionalMessage);
                sessionListener = null;

                handler.removeCallbacks(null); // To prevent double checks
                handler.postDelayed(userStatsCheckRunnable, USER_STATS_CHECK_INTERVAL);
            }

            GeoPosition position = positionManager.getLastLocation();
            if (position != null)
                trapManager.requestTrapUpdate(position.getCoordinate().getLatitude(), position.getCoordinate().getLongitude(), GeographicUtils.kilometersToMiles(Defaults.BACKGROUND_TRAPS_UPDATE_DISTANCE / 1000), true);

            // broadcast logon
            context.sendBroadcast(new Intent(INTENT_BROADCAST_ACTION_LOGON));
        }
        catch(Exception e)
        {
            Log.e(LOGNAME,"Logging user in:"+ Log.getStackTrace(e));
        }



    }

    private void addAccountToManager(){

        if(getAccount() == null){
            final Account account = new Account(user.getUserName(),ACCOUNT_TYPE);

            Bundle userData = new Bundle();
            userData.putString("user",gson.toJson(user));

            am.addAccountExplicitly(account,user.getPassword(), userData);
            String token = String.valueOf(System.currentTimeMillis());
            am.setAuthToken(account,TOKEN_TYPE,token);
        }
    }

    public void onLoginFail(User user, StatusCode statusCode)
	{
		if (sessionListener != null)
			sessionListener.onLoginFail(user, statusCode);
	}



    public void requestUserStatistics(){
        requestUserStatistics(new StatisticsListener()
        {
            @Override
            public void onTrapStatisticsLoaded(UserStatistics userStatistics)
            {
                checkUserFeedbackDeltas(userStatistics);
            }
            @Override public void onError(TrapsterError error){}
        });
    }

    public void requestUserStatistics(final StatisticsListener listener)
    {
        String username =  user.getUserName();
        api.getUserStatistics(username, new StatisticsListener()
                {
                    @Override
                    public void onTrapStatisticsLoaded(UserStatistics userStatistics)
                    {
                        user.getProfile().setUserStatistics(userStatistics);
                        if(listener != null)
                            listener.onTrapStatisticsLoaded(userStatistics);
                        checkUserFeedbackDeltas(userStatistics);
                    }

                    @Override
                    public void onError(TrapsterError error)
                    {
                        if(listener != null)
                            listener.onError(error);
                    }
                }, new CommunicationStatusListener()
                {
                    @Override public void onOpenConnection(){}
                    @Override public void onCloseConnection(){}

                    @Override
                    public void onConnectionError(String errorMessage)
                    {
                    }
                });
    }

	public void onOpenConnection()
	{
	}


    public void loginFacebook(User user, SessionListener listener) {
        this.sessionListener = listener;
        userApi.getUserByFacebookToken(user,this,this);
    }

    public void registerFacebook(User user, SessionListener listener) {
        this.sessionListener = listener;
        userApi.signUpWithFacebookToken(user, this, this);
    }

    public void completeLogin(User user, SessionListener listener){
        this.sessionListener = listener;
        onLoginSuccess(user,null);
    }



    public void register(String email, String username, String password, SessionListener listener, boolean terms)
    {
        user = null;

        String carrier = tm.getNetworkOperatorName();
        String phoneNumber = tm.getLine1Number();

        if(phoneNumber != null && phoneNumber.length() > MAX_PHONE_NUMBER_LENGTH - 1)
            phoneNumber = phoneNumber.substring(phoneNumber.length() - (MAX_PHONE_NUMBER_LENGTH));

        sessionListener= listener;

        api.register(email,username, password, true, terms, phoneNumber, carrier,this, this);
    }



	public void removeSessionListener(SessionListener listener)
	{
		if (sessionListener != null && listener == sessionListener)
			sessionListener= null;
	}

	public void forgotPassword(String name, BasicResponseListener listener, CommunicationStatusListener commListener)
	{
		userApi.forgotPassword(name,listener,commListener);
	}

    public void resendLogin(User user){
        api.resendCode(user,this,this);
    }
	/*
	 * For context of the login state
	 */
	public boolean isFirstLogin()
	{
		return firstLogin;
	}
	public void setFirstLogin(boolean firstLogin)
	{
		this.firstLogin= firstLogin;
	}
	public boolean isFirstRegister()
	{
		return sharedPreferences.getBoolean(Defaults.SESSION_IS_FIRST_REGISTER, true);
	}
	public void setFirstRegister(boolean firstRegister)
	{
		sharedPreferences.edit().putBoolean(Defaults.SESSION_IS_FIRST_REGISTER, firstRegister).commit();
	}

	private void sendFlurryLogoutEvent()
    {
        FlurryEvent event = FlurryManager.createEvent(FLURRY_EVENT_NAME.LOGGED_OUT);
        event.send();
    }
    public void sendFlurryAutoLoginEvent(boolean isLoggedIn)
    {
        FlurryEvent event = FlurryManager.createEvent(FLURRY_EVENT_NAME.START_APP);

        event.putParameter(FLURRY_PARAMETER_NAME.IS_AUTO_LOGGED, String.valueOf(isLoggedIn));
    }

    @Override
    public void onAccountsUpdated(Account[] accounts) {

        if(getAccount() == null && isLoggedIn()){
            logout();
        }

    }

    public void stop() {
        am.removeOnAccountsUpdatedListener(this);
    }

    public interface UserFeedbackListener
    {
        public void onUsersHelpedChanged(int oldCount, int newCount);
    }

    private  class UserStatsCheckRunnable implements Runnable
    {
        @Override
        public void run()
        {
            if (isLoggedIn())
            {
                requestUserStatistics();
                handler.postDelayed(this, USER_STATS_CHECK_INTERVAL);
            }
        }
    }

    public Profile getProfile() {
        return (getUser() != null)?getUser().getProfile():null;
    }

}
