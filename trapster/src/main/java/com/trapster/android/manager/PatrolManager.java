package com.trapster.android.manager;

import android.app.Application;
import android.content.Intent;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.here.android.common.GeoCoordinate;
import com.here.android.common.GeoPosition;
import com.trapster.android.comms.ITrapsterAPI;
import com.trapster.android.comms.PatrolListener;
import com.trapster.android.comms.PatrolStatusListener;
import com.trapster.android.model.MapTileUpdateResource;
import com.trapster.android.model.PatrolPath;
import com.trapster.android.model.PatrolPaths;
import com.trapster.android.model.dao.MapTileUpdateResourceDAO;
import com.trapster.android.model.dao.PatrolPathDAO;
import com.trapster.android.service.PatrolSyncService;
import com.trapster.android.util.BackgroundTask;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;

import java.util.ArrayList;

@Singleton
public class PatrolManager implements PatrolListener,
        CommunicationStatusListener {

    private static final String LOGNAME = "Trapster.PatrolManager";

    // TODO: store mapTiles in database
    private static final int MAPTILE_CACHE_ZOOM_LEVEL = 13; // Works to roughly 3 miles across (http://msdn.microsoft.com/en-us/library/bb259689.aspx)
    private static final int MAPTILE_EXPIRY_IN_SECONDS = 20*60;
    private static final String MAPTILE_CACHE_LAYER = "paths"; // Layer name for db caching

    private static double MAX_RADIUS_PATROL = 0.01; // roughly 2 miles
    private static final int PATROL_RADIUS = 5; //in miles

    private PatrolStatusListener patrolStatusListener;

    private ArrayList<PatrolPath> paths;

    @Inject MapTileUpdateResourceDAO mapTileUpdateResourceDao;
    @Inject PatrolPathDAO patrolPathDAO;
    @Inject ITrapsterAPI api;
   // @Inject PatrolMapLayer patrolMapLayer;
	@Inject Application application;
    @Inject PositionManager positionManager;


    private ArrayList<String> pendingQueue;

    public PatrolStatusListener getPatrolStatusListener() {
        return patrolStatusListener;
    }

    public void setPatrolStatusListener(
            PatrolStatusListener patrolStatusListener) {
        this.patrolStatusListener = patrolStatusListener;
    }

    public ArrayList<PatrolPath> getPaths() {
        return paths;
    }

    public void PatrolManager()
    {

    }

    @Inject
    public void Init(){
        pendingQueue = new ArrayList<String>();
    }

    
    /*
     * Calls to the server to update the traps in this area
     * 
     * @TODO we should have a throttle check on this
     */
    public void requestPatrolUpdate(double lat, double lon, double radiusInMiles) {
{/*     	Log.i(LOGNAME, "New Patrol Request ("+lat+","+lon+") r:"+radiusInMiles); */}
	      
    	Intent intent = new Intent(application.getApplicationContext(), PatrolSyncService.class);
    	intent.putExtra(PatrolSyncService.EXTRA_LATITUDE, lat);
    	intent.putExtra(PatrolSyncService.EXTRA_LONGITUDE, lon);
    	intent.putExtra(PatrolSyncService.EXTRA_RADIUS, radiusInMiles);
    	application.getApplicationContext().startService(intent);
    	
    	purgeOldTiles(); 
    }
    
    public ArrayList<PatrolPath> getAllPaths()
    {
    	 return patrolPathDAO.getAllPaths();
    }

    public ArrayList<PatrolPath> getPaths(GeoPosition currentLocation)
    {
        Coordinate coordinate = new Coordinate(currentLocation.getCoordinate().getLatitude(),
                currentLocation.getCoordinate().getLongitude());
        Envelope envelope = new Envelope(coordinate);
        envelope.expandBy(MAX_RADIUS_PATROL);
        return getPaths(envelope);
    }

    public ArrayList<PatrolPath> getPaths(Envelope bbox) {

        ArrayList<PatrolPath> allPaths = new ArrayList<PatrolPath>();

        MapTileUpdateResource topLeft = MapTileUpdateResource.createFromPoint(MAPTILE_CACHE_LAYER,bbox.getMaxX(),
                bbox.getMinY(), MAPTILE_CACHE_ZOOM_LEVEL);
        MapTileUpdateResource bottomRight = MapTileUpdateResource.createFromPoint(MAPTILE_CACHE_LAYER,bbox.getMinX(),
                bbox.getMaxY(), MAPTILE_CACHE_ZOOM_LEVEL);

        for(int x = topLeft.getTileX();x <= bottomRight.getTileX();x++){
            for(int y = topLeft.getTileY();y <= bottomRight.getTileY();y++){


                MapTileUpdateResource tile = new MapTileUpdateResource(MAPTILE_CACHE_LAYER,x,y,MAPTILE_CACHE_ZOOM_LEVEL);

                // Check if the tile is in db or requires updating
                MapTileUpdateResource mapTileResource = mapTileUpdateResourceDao.getMapTileUpdate(MAPTILE_CACHE_LAYER,x,
                        y, MAPTILE_CACHE_ZOOM_LEVEL);

                if(mapTileResource != null){
                	
                    long tileAge = System.currentTimeMillis() - mapTileResource.getLastUpdate();

                    if(tileAge > (MAPTILE_EXPIRY_IN_SECONDS*1000)){
                        update(tile);
                    }
                    
                }else{
                    update(tile);
                }


                ArrayList<PatrolPath> tempPaths = patrolPathDAO.getPaths(tile);
                if(tempPaths != null && tempPaths.size() > 0)
                    allPaths.addAll(tempPaths);

            }

        }
    
        return allPaths;

    }

    private void purgeOldTiles(){

        new BackgroundTask()
        {

            @Override
            public void onExecute()
            {
                ArrayList<MapTileUpdateResource> expiredTiles = mapTileUpdateResourceDao.getExpiredTiles(MAPTILE_CACHE_LAYER,MAPTILE_EXPIRY_IN_SECONDS*1000);

                for(MapTileUpdateResource r:expiredTiles)
                {
                    patrolPathDAO.removePaths(r);
                    /*if (isRunning())
                        patrolPathDAO.removePaths(r);
                    else
                        return;*/
                }

            }

            @Override
            public void onTimeout(long runTime)
            {
            }
        }.execute();
    	

    }
    
    
    private void update(MapTileUpdateResource tile){

        // Check if it is in the queue
        String tileRef = tile.getReference();

        if(!pendingQueue.contains(tileRef)){

            pendingQueue.add(tileRef);

/*            double radius = tile.getDiagonalRadiusForBoundingBox();

            radius = GeographicUtils.kilometersToMiles(radius / 1000);

            double lat = tile.getCenterLatitude();
            double lon = tile.getCenterLongitude();

            api.getPath(lat, lon, radius, this, this);*/

            GeoCoordinate geoCoordinate = positionManager.getLastLocation().getCoordinate();
            api.getPath(geoCoordinate.getLatitude(), geoCoordinate.getLongitude(), PATROL_RADIUS, this, this);

        }

    }

    @Override
    public void onPatrolPath(PatrolPaths patrolPaths, double lat, double lon, double radius) {

        try
        {

            MapTileUpdateResource mt = MapTileUpdateResource.createFromPoint(MAPTILE_CACHE_LAYER,lat, lon, MAPTILE_CACHE_ZOOM_LEVEL);
            mt.setLastUpdate(System.currentTimeMillis());
            mapTileUpdateResourceDao.save(mt, System.currentTimeMillis());

            patrolPathDAO.removePaths(mt);

            for(PatrolPath path:patrolPaths.getPaths()){
                patrolPathDAO.save(mt, path);
            }

            if(pendingQueue.contains(mt.getReference()))
                pendingQueue.remove(mt.getReference());

            if (patrolStatusListener != null)
            {
                patrolStatusListener.onDataLoad();
                patrolStatusListener.onPatrolPathUpdate();
                patrolStatusListener.onDataLoadComplete();
            }

        }
        catch (Exception e)
        {
            if (patrolStatusListener != null)
                patrolStatusListener.onDataLoadError();
        }


    }

    @Override
    public void onError(TrapsterError error) {
{/*         //Log.e(LOGNAME, "Patrol Error:" + error.getDetails()); */}

//		if (patrolStatusListener != null)
//			patrolStatusListener.onError(error);

    }

    @Override
    public void onCloseConnection() {
        //
    }

    @Override
    public void onConnectionError(String errorMessage) {
{/*         //Log.e(LOGNAME, "Connection Error:" + errorMessage); */}

//		if (patrolStatusListener != null)
//			patrolStatusListener.onError(new TrapsterError(
//					TrapsterError.TYPE_COMMUNICATION_ERROR, errorMessage));

    }

    @Override
    public void onOpenConnection() {
        //
    }

}
