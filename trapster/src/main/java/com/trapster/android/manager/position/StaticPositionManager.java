package com.trapster.android.manager.position;

import android.os.Handler;
import com.here.android.common.GeoPosition;
import com.here.android.common.LocationMethod;
import com.here.android.common.LocationStatus;
import com.here.android.common.PositionListener;

import java.util.ArrayList;
import java.util.List;

public class StaticPositionManager implements IPositionManager {

	private static final String LOGNAME = "StaticPositionManager";

	public static final long DEFAULT_TIME_BETWEEN_LOCATIONS = 1000; // 1000 is standard, lower will speed playback

	private PositionListener listener;
	protected List<GeoPosition> positions;
	private int pointIndex;

	public static GeoPosition lastPosition;
	private static double lastSpeed;


	private Handler handler = new Handler();
	private Runnable sendPointRunnable = new Runnable(){
		@Override
		public void run() {
			// Send the current point
			sendPoint();
            pointIndex++;
    		handler.postDelayed(sendPointRunnable, DEFAULT_TIME_BETWEEN_LOCATIONS);


		}
	};
	private long playbackStartTime;
    private boolean running;
    private GeoPosition lastKnownLocation;


    public StaticPositionManager(List<GeoPosition> positions) {
		super();
		this.positions = positions;
        start();
    }

    public StaticPositionManager(GeoPosition position) {
        super();
        this.positions = new ArrayList<GeoPosition>();
        positions.add(position);
        start();
    }


	@Override
	public void start() {
		
    		running = true;
		
			pointIndex = 0;

            lastKnownLocation = positions.get(pointIndex);

			handler.postDelayed(sendPointRunnable, DEFAULT_TIME_BETWEEN_LOCATIONS);

	}
	
	private void sendPoint() {

        if(pointIndex >= positions.size())
            pointIndex = 0;

        lastKnownLocation = positions.get(pointIndex);

		if(listener != null)
			listener.onPositionUpdated(LocationMethod.NONE, positions.get(pointIndex));
				

		
	}

	@Override
	public void stop() {
		running = false; 
		
		handler.removeCallbacks(sendPointRunnable);
	}

	@Override
	public GeoPosition getLastLocation() {
		return lastKnownLocation;
	}

	@Override
	public void addPositionListener(PositionListener listener) {
		this.listener = listener; 
	}

	@Override
	public void removePositionListener(PositionListener listener) {
		stop(); 
		this.listener = null; 
	}



	@Override
	public LocationStatus getStatus() {
		return LocationStatus.AVAILABLE; 
	}

}
