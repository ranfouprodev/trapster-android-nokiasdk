package com.trapster.android.manager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.TrafficStats;
import com.flurry.android.FlurryAgent;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.here.android.common.GeoPosition;
import com.here.android.common.LocationMethod;
import com.here.android.common.LocationStatus;
import com.here.android.common.PositionListener;
import com.trapster.android.BuildFeatures;
import com.trapster.android.manager.flurry.FLURRY_EVENT_NAME;
import com.trapster.android.manager.flurry.FLURRY_PARAMETER_NAME;
import com.trapster.android.manager.flurry.FlurryEvent;
import com.trapster.android.util.GeographicUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

//import com.here.android.search.Address;
//import com.here.android.search.ErrorCode;
//import com.here.android.search.geocoder.ReverseGeocodeRequest;

@Singleton

public class FlurryManager implements PositionListener
{
    @Inject PositionManager positionManager;
    private static ArrayList<FlurryEvent> eventList = new ArrayList<FlurryEvent>();
    private static LinkedList<FlurryEvent> availableEventList = new LinkedList<FlurryEvent>();


    private static final int STARTING_EVENT_COUNT = 20;
    private static final int REFILL_EVENT_COUNT = 10;

    private static final String LOGNAME = "FlurryManagerLog";
    //private Geocoder geoCoder;


    /*
    * For data traffic monitoring
    */
    private static int uid;
    private static long totalRcv;
    private static long totalSent;

    private static String countryCode = "Unavailable";
    private static String stateCode = "Unavailable";
    private static String currentSpeed = "<= 0";

    private static final int SPEED_CHUNK_SIZE = 10;
    private static final double TRAP_DISTANCE_CHUNK_SIZE = 0.25; // miles
    private static final double TRAP_DISTANCE_MAX = 2.0; // miles
    private static final double TRAP_DISTANCE_MAX_CHUNKS = TRAP_DISTANCE_MAX / TRAP_DISTANCE_CHUNK_SIZE;


    @Inject
    public void init()
    {
        positionManager.addPositionListener(this);


        // GeoCoder is currently broken in this build (beton12-405-prod, 1333) of the SDK, DO NOT USE!
      /*geoCoder = MapFactory.getGeocoder();

      ReverseGeocodeRequest request = geoCoder.createReverseGeocodeRequest(mapController.getMap().getCenter());

        request.execute(new ResultListener<Address>()
        {
            @Override
            public void onCompleted(Address address, ErrorCode error)
            {
                if (address != null)
                {
                    if (address.getCountryCode() != null)
                        countryCode = address.getCountryCode();
                    if (address.getState() != null)
                        stateCode = address.getState();
                }

            }

		
        });*/

        // Create available events, so we're front-loading our event count
        for (int i = 0; i < STARTING_EVENT_COUNT; i++)
        {
            availableEventList.push(new FlurryEvent());
        }
     
    }

    public static void startSession(Context context)
    {
        FlurryAgent.setUseHttps(true);
        FlurryAgent.onStartSession(context, BuildFeatures.FLURRYKEY);
        
    }

    public static FlurryEvent createEvent(FLURRY_EVENT_NAME eventName)
    {
        FlurryEvent event = getAvailableEvent();
        event.setName(eventName);

        return event;
    }

    private static FlurryEvent getAvailableEvent()
    {
        FlurryEvent event;
        synchronized (availableEventList)
        {
            if (availableEventList.size() == 0)
            {
                synchronized (eventList)
                {
                    Iterator<FlurryEvent> i = eventList.iterator();
                    while (i.hasNext())
                    {
                        FlurryEvent e = i.next();
                        if (e.isFinished())
                        {
                            e.recycle();
                            i.remove();
                            availableEventList.push(e);
                        }
                    }
                }

                for (int i = 0; i < REFILL_EVENT_COUNT; i++)
                {
                    availableEventList.push(new FlurryEvent());
                }
            }

            event = availableEventList.pop();
        }

        synchronized (eventList)
        {
            eventList.add(event);
        }

        event.reset();

        return event;
    }

    public static String getCountryCode()
    {
        return countryCode;
    }

    public static String getStateCode()
    {
        return stateCode;
    }

    public static String getDrivingSpeed()
    {
        return currentSpeed;
    }

    public static void endSession(Context context)
    {
        synchronized (eventList)
        {
            Iterator<FlurryEvent> i = eventList.iterator();
            while (i.hasNext())
            {
                FlurryEvent event = i.next();
                event.send();
                event.recycle();
            }
        }

        FlurryAgent.onEndSession(context);
 
    }

    @Override
    public void onPositionUpdated(LocationMethod locationMethod, GeoPosition geoPosition)
    {
        final double speed = geoPosition.getSpeed();
            double numChunks = speed / SPEED_CHUNK_SIZE;

            currentSpeed = "<= " + String.valueOf((int)numChunks * SPEED_CHUNK_SIZE);  // Truncate

    }

    @Override
    public void onPositionFixChanged(LocationMethod locationMethod, LocationStatus locationStatus)
    {
    }

    public static String convertToMphChunk(double mphSpeed)
    {
        double numChunks = mphSpeed / SPEED_CHUNK_SIZE + 1;

        String speedLimitChunk = "<= " + String.valueOf((int)numChunks * SPEED_CHUNK_SIZE);

        return speedLimitChunk;
    }
    
    @SuppressLint("NewApi")
    public static void startDataTrafficMonitoring()
    {
        if (BuildFeatures.SUPPORTS_ICS)
        {
            try
            {
                uid = android.os.Process.myUid();

                totalSent = TrafficStats.getUidTcpTxBytes(uid);
                totalRcv = TrafficStats.getUidTcpRxBytes(uid);
            }
            catch (NoSuchMethodError e)
            {
                // This block shouldn't be hit, since we're checking for ICS support.  However, it's possible that some phones (rooted, most likely) could pass this check and still not have
                // the required function.  In that case, we'll catch the exception and ignore the data tracking.
                totalSent = 0;
                totalRcv = 0;
            }

        }

		
	}
	
	@SuppressLint("NewApi")
	public static void endDataTrafficMonitoring()
    {
        if (totalSent > 0 && totalRcv > 0)
        {
            float mbSent = ((float)TrafficStats.getUidTcpTxBytes(uid) - (float)totalSent) / 1024; // into Kb
            float mbRcv = ((float)TrafficStats.getUidTcpRxBytes(uid) - (float)totalRcv) / 1024; // into kb

            float totalTraffic = mbSent + mbRcv;

            FlurryEvent event = FlurryManager.createEvent(FLURRY_EVENT_NAME.SESSION_DATA_USAGE);
            event.putParameter(FLURRY_PARAMETER_NAME.DATA_TRAFFIC_SENT, String.valueOf(mbSent) +" kb");
            event.putParameter(FLURRY_PARAMETER_NAME.DATA_TRAFFIC_RECEIVED, String.valueOf(mbRcv) +" kb");
            event.putParameter(FLURRY_PARAMETER_NAME.DATA_TRAFFIC_TOTAL, String.valueOf(totalTraffic) +" kb");
            event.send();
        }
				

	}

    public static String getDistanceBetweenTrapAndUser(double distance)
    {
        double numChunks = GeographicUtils.kilometersToMiles(distance) / TRAP_DISTANCE_CHUNK_SIZE + 1;

        if (numChunks > TRAP_DISTANCE_MAX_CHUNKS)
        {
            return ">= " + TRAP_DISTANCE_MAX;
        }

        return "<= " + String.valueOf((int)numChunks * TRAP_DISTANCE_CHUNK_SIZE);  // Truncate

    }

}


