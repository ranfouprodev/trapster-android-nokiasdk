package com.trapster.android.manager;

import com.trapster.android.util.BackgroundTask;
import roboguice.inject.ContextSingleton;

import java.util.concurrent.*;

@ContextSingleton
public final class BackgroundTaskManager
{
    private static final long MAX_RUN_TIME = 30; // Anything that takes more than 30 seconds should be killed

    private static final int CORE_POOL_SIZE = 3;
    private static final int MAX_POOL_SIZE = 6;
    private static final long KEEP_ALIVE_TIME = 30; // Seconds
    private static final int MAX_QUEUE_SIZE = 128;
    private static final ScheduledExecutorService threadPool = Executors.newScheduledThreadPool(CORE_POOL_SIZE);

    public static void execute(final BackgroundTask task)
    {
        final Future handler = threadPool.submit(new Callable<Object>()
        {
            @Override
            public Object call() throws Exception
            {
                task.runTask();
                return null;
            }
        });
        threadPool.schedule(new Runnable()
        {
            @Override
            public void run()
            {
                handler.cancel(true);
                task.postTimeout();
            }
        }, MAX_RUN_TIME, TimeUnit.SECONDS);
    }
}
