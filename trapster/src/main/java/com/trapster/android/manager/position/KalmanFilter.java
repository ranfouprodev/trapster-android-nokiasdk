package com.trapster.android.manager.position;

import com.trapster.android.util.GeographicUtils;
import com.trapster.android.util.Log;

import android.location.Location;

public class KalmanFilter {

	private long lastUpdateTime;

	private Matrix cx;
	private Matrix state;
	private Matrix I = Matrix.identity(6, 6);

	private int zone;

	private static final double ACCELERATION_UNC = 5;
	private static final double VELOCITY_UNC = 25;
	private static final double POSITION_UNC = 50;

	private static final String LOGNAME = "KalmanFilter";

	private boolean hasInitial = false;

	private Location lastFix;

	private static KalmanFilter kf;

	/*
	 * Singleton Implementation to maintain state
	 */
	private KalmanFilter() {
		initializeFilter();
	}

	public static KalmanFilter getInstance() {
		if (kf == null)
			kf = new KalmanFilter();

		return kf;
	}

	private void initializeFilter() {

		lastFix = new Location("Kalman");
		lastUpdateTime = System.currentTimeMillis();

		cx = new Matrix(6, 6);
		cx.set(0, 0, POSITION_UNC );
		cx.set(1, 1, POSITION_UNC );
		cx.set(2, 2, VELOCITY_UNC );
		cx.set(3, 3, VELOCITY_UNC );
		cx.set(4, 4, ACCELERATION_UNC );
		cx.set(5, 5, ACCELERATION_UNC );
		
		state = new Matrix(6, 1);
		hasInitial = false;
		zone = 0;
	}

	private void resetFilter(){
		UtmCoordinate current = null;

		if (zone == 0) {
			current = GeographicUtils.convertToUTM(lastFix.getLatitude(),
					lastFix.getLongitude());
			zone = current.getZone();
		} else
			current = GeographicUtils.convertToUTM(lastFix.getLatitude(),
					lastFix.getLongitude(), zone);
		
		resetFilter(current);
	}
	
	
	private void resetFilter(UtmCoordinate current){

		cx = new Matrix(6, 6);
		cx.set(0, 0, POSITION_UNC );
		cx.set(1, 1, POSITION_UNC );
		cx.set(2, 2, VELOCITY_UNC );
		cx.set(3, 3, VELOCITY_UNC );
		cx.set(4, 4, ACCELERATION_UNC );
		cx.set(5, 5, ACCELERATION_UNC );

		state.set(0, 0, current.getEasting());
		state.set(1, 0, current.getNorthing());
		state.set(2, 0, 0);
		state.set(3, 0, 0);
		state.set(4, 0, 0);
		state.set(5, 0, 0);
		lastUpdateTime = System.currentTimeMillis();
		
	}
	
	public Location getLastFix() {
		return lastFix;
	}

	public void setLastFix(Location lastFix) {
		this.lastFix = lastFix;
	}

	public Location predict() {
		if (!hasInitial)
			return null; 
		
		double dt = ((double)(System.currentTimeMillis() - lastUpdateTime)) /1000;
{/* 
		//Log.d(LOGNAME, "Predicting ahead:"+dt+" seconds"); */}
		
		// For estimations that are too far out
		if(dt > 10){
			
			resetFilter();
			
			lastFix.setSpeed(0);
			return lastFix;
		}
			 
		
		
		Matrix A = new Matrix(6, 6);

		A.set(0, 0, 1);
		A.set(0, 2, dt);
		A.set(0, 4, (dt*dt)/2);
		
		A.set(1, 1, 1);
		A.set(1, 3, dt);
		A.set(1, 5, (dt*dt)/2);
		
		A.set(2, 2, 1);
		A.set(2, 4, dt);
		
		A.set(3, 3, 1);
		A.set(3, 5, dt);
		
		A.set(4, 4, 1);
		A.set(5, 5, 1);
		

		// Compute the predicted state
		Matrix stateP = new Matrix(6, 1);

		stateP = A.times(state);

		UtmCoordinate c = new UtmCoordinate();
		c.setEasting(stateP.get(0, 0));
		c.setNorthing(stateP.get(1, 0));
		c.setZone(zone);

		double[] newcoords = GeographicUtils.convertToLL(c);

		Location update = new Location("Kalman");

		update.setLatitude(newcoords[0]);
		update.setLongitude(newcoords[1]);

		
		//double speed = Math.sqrt(Math.pow(stateP.get(2, 0),2) +Math.pow(stateP.get(3, 0),2)); 
		
		double distance = update.distanceTo(lastFix);
		
		double speed = distance / dt;  
		
		update.setSpeed((float) speed);

		float bearing = lastFix.bearingTo(update);
		
		update.setBearing(bearing);
{/* 
		
		//Log.d(LOGNAME, "Predicted Location:"+update.getLatitude()+","+update.getLongitude()+" speed:"+GeographicUtils.metersPerSecondToKph(update.getSpeed())+" bearing:"+update.getBearing()); */}
		
		return update; 
	}

	public Location update(Location original) {

		Location update = new Location(original);

		double dt = ((double)(System.currentTimeMillis() - lastUpdateTime)) /1000;

		if (dt == 0)
			return update;
{/* 
		//Log.d(LOGNAME, "Updating:"+dt+" seconds"); */}
		
		lastUpdateTime = System.currentTimeMillis(); 

		/*
		 * Convert the GPSLocation to utm for calculations
		 */
		UtmCoordinate current = null;

		if (zone == 0) {
			current = GeographicUtils.convertToUTM(update.getLatitude(),
					update.getLongitude());
			zone = current.getZone();
		} else
			current = GeographicUtils.convertToUTM(update.getLatitude(),
					update.getLongitude(), zone);

		if (!hasInitial) {
			
			resetFilter(current);
			hasInitial = true;
			return update;
		}

		double ha = update.getAccuracy();

		//if (ha == 0)
			ha = POSITION_UNC;

		Matrix cl = new Matrix(6, 6);
		cl.set(0, 0, ha );
		cl.set(1, 1, ha );
		cl.set(2, 2, VELOCITY_UNC );
		cl.set(3, 3, VELOCITY_UNC );
		cl.set(4, 4, ACCELERATION_UNC );
		cl.set(5, 5, ACCELERATION_UNC );
		

		Matrix A = new Matrix(6, 6);

		A.set(0, 0, 1);
		A.set(0, 2, dt);
		A.set(0, 4, (dt*dt)/2);
		
		A.set(1, 1, 1);
		A.set(1, 3, dt);
		A.set(1, 5, (dt*dt)/2);
		
		A.set(2, 2, 1);
		A.set(2, 4, dt);
		
		A.set(3, 3, 1);
		A.set(3, 5, dt);
		
		A.set(4, 4, 1);
		A.set(5, 5, 1);

		// Compute the predicted state
		Matrix stateP = new Matrix(6, 1);

		stateP = A.times(state);

		// Use position/time difference
		//double mvx = ((current.getEasting() - state.get(0, 0)) / dt);
		//double mvy = ((current.getNorthing() - state.get(1, 0)) / dt);
		
		// Use last estimate
		double mvx = state.get(2,0);
		double mvy = state.get(3,0);

		
		/*if(update.hasSpeed() && update.hasBearing()){
			mvx = update.getSpeed() * Math.sin(Math.toRadians(update.getBearing()));
			mvy = update.getSpeed() * Math.cos(Math.toRadians(update.getBearing()));
		}*/
		
		
		Matrix m = new Matrix(6, 1);
		m.set(0, 0, current.getEasting());
		m.set(1, 0, current.getNorthing());
		m.set(2, 0, mvx);
		m.set(3, 0, mvy);
		m.set(4, 0, state.get(4, 0));
		m.set(5, 0, state.get(5, 0));
		

		Matrix r = new Matrix(6, 1);

		r = m.minus(stateP);

		Matrix res = r.transpose().times(r);

		double residual = Math.sqrt(res.get(0, 0));

		if (residual > 3 * POSITION_UNC) {
{/* 
			//Log.d(LOGNAME, "Resetting filter: res:" + res.printDebug()); */}

			resetFilter(current);
			
			// Carry forward the velocities
			//state.set(2, 0, mvx);
			//state.set(3, 0, mvy);
			
			lastFix = new Location(update);
			
			return update;
		}

		Matrix cxp = new Matrix(6, 6);

		cxp = A.times(cx).times(A.transpose()).plus(cl);

		Matrix K = new Matrix(6, 6);
		Matrix K1 = new Matrix(6, 6);

		K1 = A.times(cxp).times(A.transpose()).plus(cl);

		/*
		 * watch for singular
		 */
		try {
			K = cxp.times(A.transpose()).times(K1.inverse());
		} catch (RuntimeException re) {
			K1.printDebug();
		}

		state = stateP.plus((K.times(r)));
		cx = (I.minus((K.times(A)))).times(cxp);
		
		UtmCoordinate c = new UtmCoordinate();
		c.setEasting(state.get(0, 0));
		c.setNorthing(state.get(1, 0));
		c.setZone(zone);

		double[] newcoords = GeographicUtils.convertToLL(c);

		update.setLatitude(newcoords[0]);
		update.setLongitude(newcoords[1]);

	
		double distance = update.distanceTo(original);
			
		double speed = distance / dt;
		
		//double speed = Math.sqrt(Math.pow(stateP.get(2, 0),2) +Math.pow(stateP.get(3, 0),2)); 
			
		update.setSpeed((float) speed);

		float bearing = update.bearingTo(original);
			
		update.setBearing(bearing);


		float newha = (float) Math.sqrt(cx.get(0, 0) + cx.get(1, 1));

		float speedha = (float) Math.sqrt(cx.get(2, 2) + cx.get(3, 3));
{/* 
		//Log.d(LOGNAME, "HA:" + newha + " VA:" + speedha + " Residual:"
				+ residual); */}

		update.setAccuracy(newha);

		lastFix = new Location(update);
{/* 
		
		//Log.d(LOGNAME, "Updated Location:"+update.getLatitude()+","+update.getLongitude()+" speed:"+GeographicUtils.metersPerSecondToKph(update.getSpeed())+" bearing:"+update.getBearing()); */}
		
		
		return update;

	}

}
