package com.trapster.android.manager.position;

import com.here.android.common.GeoCoordinate;
import com.here.android.common.GeoPosition;

import java.util.Date;

public class FilteredGeoPosition implements GeoPosition {

	
	private GeoCoordinate coordinate; 
	private double heading;
	private double speed;
	private Date timestamp; 
	private float latitudeAccuracy;
	private float longitudeAccuracy;
	private float altitudeAccuracy;
	
	private String source; 

    public FilteredGeoPosition(){}

    public FilteredGeoPosition(GeoCoordinate coordinate){
        this.coordinate = coordinate;
        this.source = "Manual";
    }


	@Override
	public float getAltitudeAccuracy() {
		return altitudeAccuracy; 
	}

	@Override
	public GeoCoordinate getCoordinate() {
		return coordinate; 
	}

	@Override
	public double getHeading() {
		return heading; 
	}

	@Override
	public float getLatitudeAccuracy() {
		return latitudeAccuracy;
	}

	@Override
	public float getLongitudeAccuracy() {
		return longitudeAccuracy;
	}

	@Override
	public double getSpeed() {
		return speed; 
	}

	@Override
	public Date getTimestamp() {
		return timestamp; 
	}

	@Override
	public boolean isValid() {
		return true; 
	}

	public void setCoordinate(GeoCoordinate coordinate) {
		this.coordinate = coordinate;
	}

	public void setHeading(double heading) {
		this.heading = heading;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public void setLatitudeAccuracy(float latitudeAccuracy) {
		this.latitudeAccuracy = latitudeAccuracy;
	}

	public void setLongitudeAccuracy(float longitudeAccuracy) {
		this.longitudeAccuracy = longitudeAccuracy;
	}

	public void setAltitudeAccuracy(float altitudeAccuracy) {
		this.altitudeAccuracy = altitudeAccuracy;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	
	
}
