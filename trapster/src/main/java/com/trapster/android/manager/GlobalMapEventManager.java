package com.trapster.android.manager;

import android.graphics.PointF;
import com.here.android.common.GeoCoordinate;
import com.here.android.common.ViewObject;
import com.here.android.mapping.MapEventListener;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Singleton
public class GlobalMapEventManager implements MapEventListener
{
    public enum FUNCTION_CALL
    {
        MAP_OBJECTS_SELECTED,
        MAP_MOVE_START,
        MAP_MOVE_END,
        MAP_ANIMATING_START,
        MAP_ANIMATING_END,
        TAP,
        DOUBLE_TAP,
        PINCH_ZOOM,
        ROTATE,
        TILT,
        LONG_PRESS_START,
        LONG_PRESS_END,
        TWO_FINGER_TAP,
        MAP_SCHEME_SET;
    }

    private final long[] lastCallTimes = new long[FUNCTION_CALL.values().length];
    private final static LinkedList<MapEventListener>[] registeredListeners = new LinkedList[FUNCTION_CALL.values().length];
    private static final int THREAD_POOL_SIZE = 1;
    private static final int THREAD_POOL_MAX_SIZE = 1;
    private static final int KEEP_ALIVE_TIME = 10;
    private final static long DEFAULT_TIME_BETWEEN_FUNCTION_CALLS = 30L; // Don't call the same function more than once per 30ms
    private final static long ON_ROTATE_TIMEOUT = 100L;
    private final static int MAX_RUNNABLE_COUNT = 25;
    private final ThreadPoolExecutor executor = new ThreadPoolExecutor(THREAD_POOL_SIZE, THREAD_POOL_MAX_SIZE, KEEP_ALIVE_TIME, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(MAX_RUNNABLE_COUNT));


    @Inject
    public void init()
    {
        FUNCTION_CALL[] functionCalls = FUNCTION_CALL.values();
        for (FUNCTION_CALL functionCall: functionCalls)
        {
            lastCallTimes[functionCall.ordinal()] = 0;
            registeredListeners[functionCall.ordinal()] = new LinkedList<MapEventListener>();
        }
        executor.setRejectedExecutionHandler(new RejectedExecutionHandler()
        {
            @Override
            public void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor)
            {
                threadPoolExecutor.getQueue().clear();
                threadPoolExecutor.purge();
            }
        });
    }

    // Registers a listener on all functions
    public static void addListener(MapEventListener listener)
    {
        addListener(listener, FUNCTION_CALL.values());
    }

    public static void addListener(MapEventListener listener, FUNCTION_CALL[] whichFunctions)
    {
        for (FUNCTION_CALL functionCall : whichFunctions)
        {
            addListener(listener, functionCall);
        }
    }

    public static void addListener(MapEventListener listener, FUNCTION_CALL whichFunction)
    {
        LinkedList<MapEventListener> listeners;
        synchronized (registeredListeners)
        {
            listeners = registeredListeners[whichFunction.ordinal()];
        }

        synchronized (listeners)
        {
            if (!listeners.contains(listener))
            {
                listeners.add(listener);
            }
        }
    }

    public static void removeListener(MapEventListener listener)
    {
        LinkedList<MapEventListener> listeners;
        for (FUNCTION_CALL whichFunction : FUNCTION_CALL.values())
        {
            synchronized (registeredListeners)
            {
                listeners = registeredListeners[whichFunction.ordinal()];
            }

            synchronized (listeners)
            {
                if (listeners.contains(listener))
                {
                    listeners.remove(listener);
                }
            }
        }

    }


    private boolean isReadyToCall(FUNCTION_CALL whichFunction)
    {
        boolean isReady = isReadyToCall(whichFunction, DEFAULT_TIME_BETWEEN_FUNCTION_CALLS);
        //Log.i("BBQ", whichFunction.name() + " isReady: " + isReady);
        return isReady;
    }

    private boolean isReadyToCall(FUNCTION_CALL whichFunction, long timeOut)
    {
        long lastCallTime = lastCallTimes[whichFunction.ordinal()]; // Because we're accessing a primitive type, thread safety should be fine
/*        synchronized (lastCallTimes)
        {
            lastCallTime
        }*/
        long currentTime = System.currentTimeMillis();
        boolean isReady = (currentTime - timeOut >= lastCallTime);

        //if (!isReady)
           // Log.i("BBQ", "Not ready to perform another " + whichFunction.name() + ", last one was " + (currentTime - lastCallTime) + "ms ago.");

        return isReady;
    }

    private void updateLastCallTime(FUNCTION_CALL whichFunction)
    {
        lastCallTimes[whichFunction.ordinal()] = System.currentTimeMillis();
        /*synchronized (lastCallTimes)
        {
            lastCallTimes[whichFunction.ordinal()] = System.currentTimeMillis();
        }*/
    }

    @Override
    public boolean onMapObjectsSelected(final List<ViewObject> viewObjects)
    {
        final FUNCTION_CALL thisFunction = FUNCTION_CALL.MAP_OBJECTS_SELECTED;
        if (viewObjects.size() > 0 && isReadyToCall(thisFunction))
        {
            executor.execute(new EventRunnable(thisFunction.name())
            {
                @Override
                public void performTask()
                {
                    LinkedList<MapEventListener> listeners;
                    synchronized (registeredListeners)
                    {
                        listeners = registeredListeners[thisFunction.ordinal()];
                    }

                    synchronized (listeners)
                    {
                        Iterator<MapEventListener> iterator = listeners.iterator();
                        while (iterator.hasNext())
                        {
                            iterator.next().onMapObjectsSelected(viewObjects);
                        }
                    }

                    updateLastCallTime(thisFunction);
                }
            });
        }
        return true; // This could potentially cause issues, but returning false causes the map to pan whenever we click on a map object.  Look into this further
    }

    @Override
    public void onMapMoveStart()
    {
        final FUNCTION_CALL thisFunction = FUNCTION_CALL.MAP_MOVE_START;
        if (isReadyToCall(thisFunction))
        {
            executor.execute(new EventRunnable(thisFunction.name())
            {
                @Override
                public void performTask()
                {
                    LinkedList<MapEventListener> listeners;
                    synchronized (registeredListeners)
                    {
                        listeners = registeredListeners[thisFunction.ordinal()];
                    }

                    synchronized (listeners)
                    {
                        Iterator<MapEventListener> iterator = listeners.iterator();
                        while (iterator.hasNext())
                        {
                            iterator.next().onMapMoveStart();
                        }
                    }

                    updateLastCallTime(thisFunction);
                }
            });
        }
    }

    @Override
    public void onMapMoveEnd(final GeoCoordinate geoCoordinate)
    {
        final FUNCTION_CALL thisFunction = FUNCTION_CALL.MAP_MOVE_END;
        if (isReadyToCall(thisFunction))
        {
            executor.execute(new EventRunnable(thisFunction.name())
            {
                @Override
                public void performTask()
                {
                    LinkedList<MapEventListener> listeners;
                    synchronized (registeredListeners)
                    {
                        listeners = registeredListeners[thisFunction.ordinal()];
                    }

                    synchronized (listeners)
                    {
                        Iterator<MapEventListener> iterator = listeners.iterator();
                        while (iterator.hasNext())
                        {
                            iterator.next().onMapMoveEnd(geoCoordinate);
                        }
                    }

                    updateLastCallTime(thisFunction);
                }
            });
        }
    }

    @Override
    public void onMapAnimatingStart()
    {
        final FUNCTION_CALL thisFunction = FUNCTION_CALL.MAP_ANIMATING_START;
        if (isReadyToCall(thisFunction))
        {
            executor.execute(new EventRunnable(thisFunction.name())
            {
                @Override
                public void performTask()
                {
                    LinkedList<MapEventListener> listeners;
                    synchronized (registeredListeners)
                    {
                        listeners = registeredListeners[thisFunction.ordinal()];
                    }

                    synchronized (listeners)
                    {
                        Iterator<MapEventListener> iterator = listeners.iterator();
                        while (iterator.hasNext())
                        {
                            iterator.next().onMapAnimatingStart();
                        }
                    }

                    updateLastCallTime(thisFunction);
                }
            });
        }
    }

    @Override
    public void onMapAnimatingEnd()
    {
        final FUNCTION_CALL thisFunction = FUNCTION_CALL.MAP_ANIMATING_END;
        if (isReadyToCall(thisFunction))
        {
            executor.execute(new EventRunnable(thisFunction.name())
            {
                @Override
                public void performTask()
                {
                    LinkedList<MapEventListener> listeners;
                    synchronized (registeredListeners)
                    {
                        listeners = registeredListeners[thisFunction.ordinal()];
                    }

                    synchronized (listeners)
                    {
                        Iterator<MapEventListener> iterator = listeners.iterator();
                        while (iterator.hasNext())
                        {
                            iterator.next().onMapAnimatingEnd();
                        }
                    }

                    updateLastCallTime(thisFunction);
                }
            });
        }
    }

    @Override
    public boolean onTap(final PointF point)
    {
        final FUNCTION_CALL thisFunction = FUNCTION_CALL.TAP;
        if (isReadyToCall(thisFunction))
        {
            executor.execute(new EventRunnable(thisFunction.name())
            {
                @Override
                public void performTask()
                {
                    LinkedList<MapEventListener> listeners;
                    synchronized (registeredListeners)
                    {
                        listeners = registeredListeners[thisFunction.ordinal()];
                    }

                    synchronized (listeners)
                    {
                        Iterator<MapEventListener> iterator = listeners.iterator();
                        while (iterator.hasNext())
                        {
                            iterator.next().onTap(point);
                        }
                    }

                    updateLastCallTime(thisFunction);
                }
            });
        }
        return true; // Needed for speed limit reporting to work, but is going to screw up the map panning on tap, we can debate whether or not we want that.
    }

    @Override
    public boolean onDoubleTap(final PointF point)
    {
        final FUNCTION_CALL thisFunction = FUNCTION_CALL.DOUBLE_TAP;
        if (isReadyToCall(thisFunction))
        {
            executor.execute(new EventRunnable(thisFunction.name())
            {
                @Override
                public void performTask()
                {
                    LinkedList<MapEventListener> listeners;
                    synchronized (registeredListeners)
                    {
                        listeners = registeredListeners[thisFunction.ordinal()];
                    }

                    synchronized (listeners)
                    {
                        Iterator<MapEventListener> iterator = listeners.iterator();
                        while (iterator.hasNext())
                        {
                            iterator.next().onDoubleTap(point);
                        }
                    }

                    updateLastCallTime(thisFunction);
                }
            });
        }
        return false;
    }

    @Override
    public boolean onPinchZoom(final float v, final PointF point)
    {
        final FUNCTION_CALL thisFunction = FUNCTION_CALL.PINCH_ZOOM;
        if (isReadyToCall(thisFunction))
        {
            executor.execute(new EventRunnable(thisFunction.name())
            {
                @Override
                public void performTask()
                {
                    LinkedList<MapEventListener> listeners;
                    synchronized (registeredListeners)
                    {
                        listeners = registeredListeners[thisFunction.ordinal()];
                    }

                    synchronized (listeners)
                    {
                        Iterator<MapEventListener> iterator = listeners.iterator();
                        while (iterator.hasNext())
                        {
                            iterator.next().onPinchZoom(v, point);
                        }
                    }

                    updateLastCallTime(thisFunction);
                }
            });
        }
        return false;
    }

    @Override
    public boolean onRotate(final float v)
    {
        final FUNCTION_CALL thisFunction = FUNCTION_CALL.ROTATE;
        if (isReadyToCall(thisFunction, ON_ROTATE_TIMEOUT))
        {
            executor.execute(new EventRunnable(thisFunction.name())
            {
                @Override
                public void performTask()
                {
                    LinkedList<MapEventListener> listeners;
                    synchronized (registeredListeners)
                    {
                        listeners = registeredListeners[thisFunction.ordinal()];
                    }

                    synchronized (listeners)
                    {
                        Iterator<MapEventListener> iterator = listeners.iterator();
                        while (iterator.hasNext())
                        {
                            iterator.next().onRotate(v);
                        }
                    }

                    updateLastCallTime(thisFunction);
                }
            });
        }
        return false;
    }

    @Override
    public boolean onTilt(final float v)
    {
        final FUNCTION_CALL thisFunction = FUNCTION_CALL.TILT;
        if (isReadyToCall(thisFunction, ON_ROTATE_TIMEOUT))
        {
            executor.execute(new EventRunnable(thisFunction.name())
            {
                @Override
                public void performTask()
                {
                    LinkedList<MapEventListener> listeners;
                    synchronized (registeredListeners)
                    {
                        listeners = registeredListeners[thisFunction.ordinal()];
                    }

                    synchronized (listeners)
                    {
                        Iterator<MapEventListener> iterator = listeners.iterator();
                        while (iterator.hasNext())
                        {
                            iterator.next().onTilt(v);
                        }
                    }

                    updateLastCallTime(thisFunction);
                }
            });
        }
        return false;
    }

    @Override
    public boolean onLongPressed(final PointF point)
    {
        final FUNCTION_CALL thisFunction = FUNCTION_CALL.LONG_PRESS_START;
        if (isReadyToCall(thisFunction))
        {
            executor.execute(new EventRunnable(thisFunction.name())
            {
                @Override
                public void performTask()
                {
                    LinkedList<MapEventListener> listeners;
                    synchronized (registeredListeners)
                    {
                        listeners = registeredListeners[thisFunction.ordinal()];
                    }

                    synchronized (listeners)
                    {
                        Iterator<MapEventListener> iterator = listeners.iterator();
                        while (iterator.hasNext())
                        {
                            iterator.next().onLongPressed(point);
                        }
                    }

                    updateLastCallTime(thisFunction);
                }
            });
        }
        return false;
    }

    @Override
    public void onLongPressReleased()
    {
        final FUNCTION_CALL thisFunction = FUNCTION_CALL.LONG_PRESS_END;
        if (isReadyToCall(thisFunction))
        {
            executor.execute(new EventRunnable(thisFunction.name())
            {
                @Override
                public void performTask()
                {
                    LinkedList<MapEventListener> listeners;
                    synchronized (registeredListeners)
                    {
                        listeners = registeredListeners[thisFunction.ordinal()];
                    }

                    synchronized (listeners)
                    {
                        Iterator<MapEventListener> iterator = listeners.iterator();
                        while (iterator.hasNext())
                        {
                            iterator.next().onLongPressReleased();
                        }
                    }

                    updateLastCallTime(thisFunction);
                }
            });
        }
    }

    @Override
    public boolean onTwoFingerTap(final PointF point)
    {
        final FUNCTION_CALL thisFunction = FUNCTION_CALL.TWO_FINGER_TAP;
        if (isReadyToCall(thisFunction))
        {
            executor.execute(new EventRunnable(thisFunction.name())
            {
                @Override
                public void performTask()
                {
                    LinkedList<MapEventListener> listeners;
                    synchronized (registeredListeners)
                    {
                        listeners = registeredListeners[thisFunction.ordinal()];
                    }

                    synchronized (listeners)
                    {
                        Iterator<MapEventListener> iterator = listeners.iterator();
                        while (iterator.hasNext())
                        {
                            iterator.next().onTwoFingerTap(point);
                        }
                    }

                    updateLastCallTime(thisFunction);
                }
            });
        }
        return false;
    }

    @Override
    public void onMapSchemeSet()
    {
        final FUNCTION_CALL thisFunction = FUNCTION_CALL.MAP_SCHEME_SET;
        if (isReadyToCall(thisFunction))
        {
            executor.execute(new EventRunnable(thisFunction.name())
            {
                @Override
                public void performTask()
                {
                    LinkedList<MapEventListener> listeners;
                    synchronized (registeredListeners)
                    {
                        listeners = registeredListeners[thisFunction.ordinal()];
                    }

                    synchronized (listeners)
                    {
                        Iterator<MapEventListener> iterator = listeners.iterator();
                        while (iterator.hasNext())
                        {
                            iterator.next().onMapSchemeSet();
                        }
                    }

                    updateLastCallTime(thisFunction);
                }
            });
        }
    }

    private abstract static class EventRunnable implements Runnable
    {
        private final String runnableName;
        public EventRunnable(String runnableName)
        {
            this.runnableName = runnableName;
        }
        public final void run()
        {
            //long startTime = System.currentTimeMillis();
            performTask();
            //long endTime = System.currentTimeMillis();
            //outputEventInfo(endTime - startTime);
        }

        private void outputEventInfo(long taskDuration)
        {
            //Log.i("BBQ", runnableName + " operation took " + taskDuration);
        }

        protected abstract void performTask();
    }
}

