package com.trapster.android.manager;

import android.app.Application;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.here.android.common.GeoPosition;
import com.here.android.common.LocationMethod;
import com.here.android.common.LocationStatus;
import com.here.android.common.PositionListener;
import com.here.android.mapping.MapFactory;
import com.trapster.android.Defaults;
import com.trapster.android.manager.position.*;
import com.trapster.android.receiver.ProbeDataLocationChangedReceiver;
import com.trapster.android.receiver.TrapsterServiceBroadcastReceiver;
import com.trapster.android.util.BackgroundTask;

import java.io.InputStream;
import java.util.List;
import java.util.concurrent.*;

/**
 * This class centralizes the GPS position requests and distributes them to the
 * components
 * <p/>
 * If we can avoid using the LocationManager / Nokia PositionManager we have
 * better control on the positions.
 * <p/>
 * The position manager centralizes all the locations so that we can calculate
 *
 * @author John
 */
@Singleton
public class PositionManager {

    /*
     * The start() methods provide access to changing the source of the
     */
    public static int METHOD_GPS = 0;
    public static int METHOD_GPX_PLAYBACK = 1;

    private static final String LOGNAME = "PositionManager";

    private static final long PROBE_TIME_BETWEEN_LOCATIONS = 1000; // ms

    private static final float PROBE_DISTANCE_BETWEEN_LOCATIONS = 5; // meters

    private static final int THREAD_POOL_SIZE = 1;
    private static final int KEEP_ALIVE_TIME = 15;
    private static final int MAX_QUEUE_SIZE = 3;

    @Inject
    Application application;

    @Inject
    SharedPreferences sharedPreferences;

    @Inject
    LocationManager locationManager;

    @Inject
    RouteLinkManager routeLinkManager;

    IPositionManager positionSource;

    private PendingIntent probeDataPendingIntent;

    private CopyOnWriteArrayList<PositionListener> listeners = new CopyOnWriteArrayList<PositionListener>();

    private ConstantPositionListener constantPositionListener;

    private GeoPosition lastLocation;

    private ExecutorService broadcastExecutor;
    private boolean running;
    private KalmanFilter kalmanFilter;

    private boolean hasFirstLocation = false;

	/*private Handler handler = new Handler();
    private Runnable broadcastLocation = new Runnable() {

		@Override
		public void run() {

			if(hasFirstLocation){

				Location filtered = kalmanFilter.predict();

                if (filtered != null)
                {
                    FilteredGeoPosition filteredGeoPosition = new FilteredGeoPosition();
                    filteredGeoPosition.setCoordinate(MapFactory.createGeoCoordinate(
                            filtered.getLatitude(), filtered.getLongitude()));
                    filteredGeoPosition.setSource("kalman");
                    filteredGeoPosition.setSpeed(filtered.getSpeed());
                    filteredGeoPosition.setHeading(filtered.getBearing());

                    broadcastLocation(filteredGeoPosition);

                    broadcastSpeedAndDirection((int) filteredGeoPosition.getHeading(),
                            filteredGeoPosition.getSpeed());
                }
			}

			handler.postDelayed(broadcastLocation, LOCATION_UPDATE_SAMPLE_RATE);
		}

	};*/

    public PositionManager() {
    }

    @Inject
    public void init() {
		////Log,.v(LOGNAME, "PositionManager initialized");

        running = false;

		/*
		 * Each IPositionManager broadcasts here
		 */
        constantPositionListener = new ConstantPositionListener();

        //kalmanFilter = KalmanFilter.getInstance();

    }

    public boolean hasFirstLocation() {
        return hasFirstLocation;
    }


    public void start(IPositionManager _positionSource){

        prepareStart();

        positionSource = _positionSource;
        positionSource.addPositionListener(constantPositionListener);
        positionSource.start();
    }

	public void start(int mode, InputStream gpxStream) {

        if (mode != METHOD_GPX_PLAYBACK)
            throw new IllegalArgumentException(
                    "Use start(int) to specify the GPS mode");
        prepareStart();

        positionSource = new GpxPlaybackPositionManager(gpxStream);
        positionSource.addPositionListener(constantPositionListener);
        positionSource.start();

        //handler.postDelayed(broadcastLocation,5000);

    }

	public void start(int mode) {

        if (mode == METHOD_GPX_PLAYBACK)
            throw new IllegalArgumentException(
                    "Use start(int, InputStream) to specify the GPX file");

        prepareStart();

        enableProbeData();

		/*
		 * Assigns the Nokia Engine as the position source
		 */
            positionSource = new NokiaPositionManager(); // Default

        positionSource.addPositionListener(constantPositionListener);
        positionSource.start();

        //handler.postDelayed(broadcastLocation,1000);

    }

    public void stop() {
        {/* 		Log.i(LOGNAME, "Stopping Position Manager"); */}

        running = false;

        disableProbeData();

        positionSource.removePositionListener(constantPositionListener);
        positionSource.stop();

        broadcastExecutor.shutdown();

        //handler.removeCallbacks(broadcastLocation);

    }

    private void prepareStart() {

        if (running) {
            if (positionSource != null)
                positionSource.stop();

            disableProbeData();

            broadcastExecutor.shutdown();
        }

		/*
		 * Broadcasts are send async to prevent performance problems with long
		 * running code on onPositionUpdated/Changed code
		 */
        //broadcastExecutor = Executors.newSingleThreadExecutor();
        broadcastExecutor = new ThreadPoolExecutor(THREAD_POOL_SIZE, THREAD_POOL_SIZE, KEEP_ALIVE_TIME, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(MAX_QUEUE_SIZE), new RejectedExecutionHandler() {
            @Override
            public void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
                threadPoolExecutor.getQueue().clear();
                threadPoolExecutor.purge();
            }
        });

        running = true;
    }

    public LocationStatus getStatus() {
        if (positionSource == null)
            return LocationStatus.TEMPORARILY_UNAVAILABLE;
        else
            return positionSource.getStatus();
    }

    public GeoPosition getLastLocation() {

        if (lastLocation == null)
            setDefaultLocation();

        return lastLocation;
    }

    public void addPositionListener(PositionListener listener) {
        // listener.onPositionUpdated(null, getLastLocation());
            listeners.add(listener);
        }

    public void removePositionListener(PositionListener listener) {
        listeners.remove(listener);
    }

    /**
     * Sets the last location based on the values in the preferences or initial
     */
    private void setDefaultLocation() {
        float lat = sharedPreferences.getFloat(Defaults.PREFERENCE_LAST_UPDATE_LATITUDE, Defaults.INITIAL_LAT);
        float lng = sharedPreferences.getFloat(Defaults.PREFERENCE_LAST_UPDATE_LONGITUDE, Defaults.INITIAL_LNG);


        if (lat != Defaults.INITIAL_LAT)
            hasFirstLocation = true;

		lastLocation = MapFactory.createGeoPosition(MapFactory.createGeoCoordinate(lat, lng));

    }

    /**
     * Probe Data is pushed out to a receiver that logs the positions and
     * synchronizes that with the server automatically.
     */
    private void enableProbeData() {

        if (sharedPreferences.getBoolean(Defaults.PREFERENCE_ENABLE_PROBEDATA,
                true)) {
            ////Log,.v(LOGNAME, "Enabling ProbeData");
            Intent activeIntent = new Intent(application.getApplicationContext(), ProbeDataLocationChangedReceiver.class);
            probeDataPendingIntent = PendingIntent.getBroadcast(application.getApplicationContext(), 0, activeIntent, 0);

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, PROBE_TIME_BETWEEN_LOCATIONS, PROBE_DISTANCE_BETWEEN_LOCATIONS, probeDataPendingIntent);

        } else {
            ////Log,.v(LOGNAME, "Probe collection disabled");
        }

    }

    private void disableProbeData() {

        if (probeDataPendingIntent != null)
            locationManager.removeUpdates(probeDataPendingIntent);

    }

    /**
     * Stores the last known location for reference and startup speed
     *
     * @param geoPosition
     */
    private void storeLastLocation(GeoPosition geoPosition) {
        lastLocation = geoPosition;
        storeLastLocation();
    }

    private void storeLastLocation() {
        new BackgroundTask() {

            @Override
            public void onExecute() {
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putFloat(Defaults.PREFERENCE_LAST_UPDATE_LATITUDE,
                        (float) lastLocation.getCoordinate().getLatitude());
                edit.putFloat(Defaults.PREFERENCE_LAST_UPDATE_LONGITUDE,
                        (float) lastLocation.getCoordinate().getLongitude());
                edit.commit();
            }

            @Override
            public void onTimeout(long runTime) {
            }
        }.execute();

    }

    public boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private class ConstantPositionListener implements PositionListener {

        @Override
        public void onPositionUpdated(LocationMethod locationMethod,
                                      GeoPosition geoPosition) {

            //Log.v(LOGNAME, "Position Update ("
            //       + geoPosition.getCoordinate().getLatitude() + ","
            //       + geoPosition.getCoordinate().getLongitude() + ")");

            if (!hasFirstLocation)
                hasFirstLocation = true;
			
		
			/*if (lastLocation != null) {

				Location newLocation = new Location("Filter");
				newLocation.setTime(geoPosition.getTimestamp().getTime());
				newLocation.setLatitude(geoPosition.getCoordinate()
						.getLatitude());
				newLocation.setLongitude(geoPosition.getCoordinate()
						.getLongitude());
				newLocation.setSpeed((float) geoPosition.getSpeed());
				newLocation.setBearing((float) geoPosition.getHeading());

				kalmanFilter.update(newLocation);

			}*/


            broadcastLocation(geoPosition);

            if (geoPosition.getSpeed() != Long.MAX_VALUE && geoPosition.getHeading() != Long.MAX_VALUE) {
                broadcastSpeedAndDirection((int) geoPosition.getHeading(),
                        geoPosition.getSpeed());
            }


            // Cache the last location in preferences for speed of startup and
            // to access last known location quickly
            storeLastLocation(geoPosition);

        }

        @Override
        public void onPositionFixChanged(LocationMethod locationMethod,
                                         LocationStatus locationStatus) {
            // Broadcast to each of the listeners on a background thread
            for (PositionListener listener : listeners) {
                broadcastExecutor.execute(new SendPositionFixChangedTask(locationMethod, locationStatus, listener));
            }
        }
    }

    private class SendPositionUpdatedTask implements Runnable {

        private LocationMethod method;
        private GeoPosition position;
        private List<PositionListener> listeners;

        public SendPositionUpdatedTask(LocationMethod method,
                                       GeoPosition position, List<PositionListener> listeners) {
            this.method = method;
            this.position = position;
            this.listeners = listeners;
        }

        @Override
        public void run() {
            for (PositionListener listener : listeners) {
                listener.onPositionUpdated(method, position);
            }


        }

    }

    private class SendPositionFixChangedTask implements Runnable {

        private LocationMethod method;
        private LocationStatus status;
        private PositionListener listener;

        public SendPositionFixChangedTask(LocationMethod method,
                                          LocationStatus status, PositionListener listener) {
            this.method = method;
            this.status = status;
            this.listener = listener;
        }

        @Override
        public void run() {
            listener.onPositionFixChanged(method, status);

        }

    }

    private void broadcastSpeedAndDirection(int bearing, double speed) {

        Intent i = new Intent(TrapsterServiceBroadcastReceiver.INTENT_BROADCAST);
        i.putExtra(TrapsterServiceBroadcastReceiver.INTENT_NOTIFICATION_TYPE,
                TrapsterServiceBroadcastReceiver.INTENT_UPDATE_VALUES);
        i.putExtra(TrapsterServiceBroadcastReceiver.INTENT_DIRECTION, bearing);
        i.putExtra(TrapsterServiceBroadcastReceiver.INTENT_SPEED, speed);

        application.sendBroadcast(i);

    }

    public void broadcastLocation(GeoPosition geoPosition) {
        // Broadcast to each of the listeners on a background thread
        broadcastExecutor.execute(new SendPositionUpdatedTask(LocationMethod.NONE, geoPosition, listeners));
    }

}
