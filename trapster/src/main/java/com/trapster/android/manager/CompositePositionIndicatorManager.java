package com.trapster.android.manager;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.view.WindowManager;
import com.google.inject.Inject;
import com.here.android.common.*;
import com.here.android.mapping.Map;
import com.here.android.mapping.MapFactory;
import com.here.android.mapping.MapMarker;
import com.trapster.android.BuildFeatures;
import com.trapster.android.Defaults;
import com.trapster.android.R;
import com.trapster.android.TrapsterApplication;
import com.trapster.android.activity.fragment.AbstractMapFragment;
import com.trapster.android.activity.fragment.MapFragment;
import com.trapster.android.activity.fragment.util.MapMotionDetector;
import com.trapster.android.opengl.CompositeView;
import com.trapster.android.opengl.drawable.GLCone;
import com.trapster.android.opengl.drawable.GLImage;
import com.trapster.android.opengl.drawable.GLShape;
import roboguice.inject.ContextSingleton;

@ContextSingleton
public class CompositePositionIndicatorManager implements PositionListener
{
    public static final int UNKNOWN_HEADING = 32768; // Represents a 2 byte integer max, the documentation differs on the actual value received

    private double mapRotation = 0.0;
    private double userHeading = 90; // Start pointing towards north

    private int lastRotation = 0;
    private static final int MINIMUM_DEGREES_PER_IMAGE_UPDATE = 1;

    private double MINIMUM_SPEED_FOR_UPDATE = 2.2352; // Converts to 5 miles per hour or 8.45km/hour
    private double MINIMUM_CONE_SPEED = 4.4704; // Converts to 10 miles per hour or 16.09km/hour

    private GLShape arrowImage;
    private GLCone circleImage;
    private GeoCoordinate location;

    // These two references are used ONLY to verify that the compsiteView and mapFragment are up-to-date, it is unsafe to use any functions they provide
    private AbstractMapFragment mapFragment;
    private CompositeView compositeView;

    //private Map map;
    private PointF lastOffsetPoint;
    private MapMarker pinMarker;
    private Image pinImage;

    @Inject SharedPreferences sharedPreferences;
    @Inject PositionManager positionManager;
    @Inject MapController mapController;
    @Inject WindowManager windowManager;

    private final AnimationListener animationListener = new AnimationListener();

    private boolean isCenterOnMe = true;
    private boolean isAttached = false;
    private boolean isPinDown = true;


    @Override
    public void onPositionUpdated(LocationMethod locationMethod, GeoPosition geoPosition)
    {
        location = geoPosition.getCoordinate();

        double speed = geoPosition.getSpeed();
        if (speed < 200 && speed > MINIMUM_SPEED_FOR_UPDATE) // 200 or higher is probably a bogus value
        {
            /*int heading = (int) -geoPosition.getHeading();
            if (heading != UNKNOWN_HEADING)
            {
                userHeading = heading;
                //updateHeading();
            }*/

            circleImage.useCircle(speed < MINIMUM_CONE_SPEED);
            //circleImage.useCircle(false);

            if (mapFragment != null)
            {
                update();
            }
        }

    }

    // This is another section of terrible code in the spirit of "get it done fast"
    private void setCoordinateOffsets(GLShape shape)
    {
        updateHeading();
        if (lastOffsetPoint != null)
        {
            shape.setVisible(true);
            shape.setXOffset(lastOffsetPoint.x, true);
            shape.setYOffset(lastOffsetPoint.y, true);
        }
        else
        {
            shape.setVisible(false);
        }

        if (shape instanceof GLCone && !sharedPreferences.getBoolean(Defaults.PREFERENCE_MAP_DISPLAY_SHOW_VIRTUAL_RADAR, true))
        {
            shape.setVisible(false);
        }
    }

    @Override
    public void onPositionFixChanged(LocationMethod locationMethod, LocationStatus locationStatus)
    {
    }

    @Inject
    public void init()
    {
        GeoPosition position = positionManager.getLastLocation();
        if (position != null)
        {
            setup();
        }
        else
        {
            mapController.addOnMapLoadCompleteListener(new MapController.OnMapLoadCompleteListener()
            {
                @Override
                public void onMapReady()
                {
                    setup();
                }
            });
        }

    }

    private void setup()
    {
        location = positionManager.getLastLocation().getCoordinate();
        CoordinateCallback callback = new CoordinateCallback()
        {
            @Override
            public void requestCoordinateUpdate(GLShape shape)
            {
                setCoordinateOffsets(shape);
            }
        };
        circleImage = new GLCone();
        arrowImage = new GLImage(R.drawable.arrow_tilt);
        arrowImage.setCoordinateCallback(callback);
        circleImage.setCoordinateCallback(callback);

        pinMarker = MapFactory.createMapMarker();
        pinImage = MapFactory.createImage();

        Resources resources = TrapsterApplication.getAppInstance().getResources();
        pinImage.setBitmap( BitmapFactory.decodeResource(resources, R.drawable.arrow_tilt));
        pinMarker.setIcon(pinImage);
        pinMarker.setVisible(false);
    }

    private void  updateHeading()
    {
        double degrees = mapRotation + userHeading + 90;
        double normalizedDegrees = normalizeHeading((int) degrees);

        if (Math.abs(lastRotation - normalizedDegrees) >= MINIMUM_DEGREES_PER_IMAGE_UPDATE)
        {
            arrowImage.setRotation(90f);
            circleImage.setHeading(90f);
        }
    }

    private double normalizeHeading(double heading)
    {
        // First, let's fix crazy values by clamping the value we receive between 0 and 360
        double newHeading = heading;
        int numCircles = (int) (newHeading / 360);
        newHeading = newHeading - (numCircles * 360); // Gives the same angle, minus the number of complete circles

        // After the above, fix clamping again.
        if (newHeading < 0)
            newHeading = newHeading + 360;
        if (newHeading > 360)
            newHeading = newHeading - 360;

        return newHeading;
    }

    public void forceUpdate(float mapRotation, PointF point)
    {
        this.mapRotation = mapRotation;
        //lastOffsetPoint = point;
        //updateHeading();
        int midWidth = windowManager.getDefaultDisplay().getWidth() / 2;
        int midHeight = windowManager.getDefaultDisplay().getHeight() - (windowManager.getDefaultDisplay().getHeight() / 3);
        lastOffsetPoint = new PointF(midWidth, midHeight);
    }

    public void update()
    {
        int midWidth = windowManager.getDefaultDisplay().getWidth() / 2;
        int midHeight = windowManager.getDefaultDisplay().getHeight() - (windowManager.getDefaultDisplay().getHeight() / 3);
        lastOffsetPoint = new PointF(midWidth, midHeight);
    }

    public GLShape getIndicator()
    {
        return arrowImage;
    }

    public GLShape getCircle()
    {
        return circleImage;
    }

    public void setupForView(CompositeView compositeView, final AbstractMapFragment mapFragment)
    {
        location = positionManager.getLastLocation().getCoordinate();
        if (this.compositeView != compositeView)
        {
            this.compositeView = compositeView;
            compositeView.addShape(getCircle());
            compositeView.addShape(getIndicator());
        }

        if (this.mapFragment != mapFragment)
        {
            this.mapFragment = mapFragment;
            getCircle().setVisible(false);
            getIndicator().setVisible(false);
        }
    }

    public void attach(final MapFragment mapFragment)
    {
        this.mapFragment = mapFragment;
        isAttached = true;
        mapFragment.getMap().addMapObject(pinMarker);
        update();

    }

    public void detach()
    {
        if (mapFragment != null)
        {
            Map map = mapFragment.getMap();
            if (map != null)
            {
                map.removeMapObject(pinMarker);
                mapFragment = null;
            }
        }
        //lastOffsetPoint = null;
        isAttached = false;
    }

    public AnimationListener getAnimationListener()
    {
        return animationListener;
    }

    public class AnimationListener extends MapMotionDetector implements MapController.OnMapUpdateListener
    {
        @Override
        public void onCenterOnMeChanged(boolean centerOnMe)
        {
            isCenterOnMe = centerOnMe;
            if (!isCenterOnMe)
            {
                dropPin();
            }
            else
            {
                pickupPin();
            }

        }

        @Override
        public boolean onPinchZoom(float scale, PointF center)
        {

            if (BuildFeatures.SUPPORTS_ICS)
            {
                if (isCenterOnMe)
                    pickupPin();
            }
            else
                mapController.setCenterOnMe(false);



            return false;
        }

        @Override
        public void onMapAnimatingEnd()
        {
            Map map = mapFragment.getMap();
            if (map != null && !isPinDown)
            {
                update();
                if (isCenterOnMe)
                    pickupPin();
            }
        }
    }

    private void dropPin()
    {
        if (location.isValid())
        {
            compositeView.hideAll();
            pinMarker.setCoordinate(location);
            pinMarker.setVisible(true);
            isPinDown = false;
        }
    }

    private void pickupPin()
    {
        boolean isLocationNull = location == null;
        boolean isMapFragmentNull = mapFragment == null;

        // Spoke to Matt, we are not going to delve too deep into why we get a NullPointer on this line
        // since this occurrence is incredibly rare.  Instead we're just going to ensure that we don't crash here.
        if (!isLocationNull && !isMapFragmentNull && location.isValid() && mapFragment.isVisible() && isAttached)
        {
            pinMarker.setVisible(false);
            update();
            compositeView.unHideAll();
            isPinDown = true;
        }

    }

    public interface CoordinateCallback
    {
        public void requestCoordinateUpdate(GLShape shape);
    }


}
