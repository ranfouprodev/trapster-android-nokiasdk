package com.trapster.android.manager.flurry;

import com.flurry.android.FlurryAgent;
import com.trapster.android.util.Log;

import java.security.InvalidParameterException;
import java.util.HashMap;

public class FlurryEvent
{
    private boolean isTimed;
    private boolean isRecycled;
    private boolean isSent;
    private HashMap<String, String> parameters;

    private String eventName;

    private final static int MAX_PARAMETER_COUNT = 10;
    private final static int MAX_PARAMETER_LENGTH = 255;

    private final static String LOGNAME = "FlurryEventLog";

    public FlurryEvent()
    {
        isRecycled = false;
        parameters = new HashMap<String, String>();
        isTimed = false;
        isSent = false;
    }

    public void setName(FLURRY_EVENT_NAME eventName)
    {
        if (eventName.name.length() > MAX_PARAMETER_LENGTH)
            throw new InvalidParameterException("Flurry event parameters must be 255 characters or less.");
        else
            this.eventName = eventName.name;
    }

    public void enableTimer()
    {
        isTimed = true;
    }

    public void endTimer()
    {
        if (isTimed)
        {
{/*             Log.i(LOGNAME, "Ending timer on event " + eventName); */}
            FlurryAgent.endTimedEvent(eventName);
            isTimed = false;
        }

    }

    public void send()
    {
{/*         Log.i(LOGNAME, "Sending Flurryevent with name " + eventName + " and " + parameters.size() + " parameters.  isTimed = " + isTimed); */}

        if (isTimed)
        {
            if (parameters.size() > 0)
                FlurryAgent.logEvent(eventName, parameters, isTimed);
            else
                FlurryAgent.logEvent(eventName, isTimed);
        }
        else
        {
            if (parameters.size() > 0)
                FlurryAgent.logEvent(eventName, parameters);
            else
                FlurryAgent.logEvent(eventName);
        }
        isSent = true;

    }

    public void recycle()
    {
{/*         Log.i(LOGNAME, "Recycling event " + eventName); */}
        if (isTimed)
            enableTimer();
        parameters.clear();
        isTimed = false;
        isRecycled = true;
        isSent = false;
        eventName = "";

    }

    public void reset()
    {
        isRecycled = false;
    }

    public boolean isTimed()
    {
        return isTimed;
    }

    public boolean isRecycled()
    {
        return isRecycled;
    }

    public boolean isFinished()
    {
        return isSent && !isTimed;
    }

    public void putParameter(FLURRY_PARAMETER_NAME parameterName, String value)
    {
        if (parameters.size() > MAX_PARAMETER_COUNT)
            throw new StackOverflowError("Flurry events can only have 10 parameters!");
        else if (parameterName.name.length() > MAX_PARAMETER_LENGTH || value.length() > MAX_PARAMETER_LENGTH)
            throw new InvalidParameterException("Flurry event parameters must be 255 characters or less.");
        else
            parameters.put(parameterName.name, value);
    }
}
