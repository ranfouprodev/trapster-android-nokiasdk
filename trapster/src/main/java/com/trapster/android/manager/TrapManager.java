package com.trapster.android.manager;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.SparseArray;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.trapster.android.Defaults;
import com.trapster.android.manager.BitmapCacheManager.InvalidResourceKeyException;
import com.trapster.android.model.*;
import com.trapster.android.model.dao.ImageResourceDAO;
import com.trapster.android.model.dao.TrapDAO;
import com.trapster.android.model.dao.TrapTypeDAO;
import com.trapster.android.service.TrapSyncService;
import com.trapster.android.util.Log;

import java.util.*;


/**
 * TrapController handles all the Trap queries and requests
 * 
 */
/**
 * @author John
 *
 */
@Singleton
public class TrapManager {

	private static final String LOGNAME = "TrapManager";
    private int numRequests;
	
	@Inject Application application;

	@Inject	ImageResourceDAO imageResourceDao;
	
	@Inject TrapDAO trapDao;
	
	@Inject TrapTypeDAO trapTypeDao;

	@Inject BitmapCacheManager bitmapCacheManager;
	
	@Inject SharedPreferences sharedPreferences;

    private List<TrapType> sortedTypes;
	
	
	
	private SparseArray<TrapType> trapTypes = new SparseArray<TrapType>();
    //private List<Trap> activeTraps = new ArrayList<Trap>();
    private HashMap<Long, Long> dismissedTraps = new HashMap<Long,Long>();
	
	public TrapManager() {
	}
	
    @Inject
	public void init() {
        ////Log,.v(LOGNAME, "TrapController initialized");
    }

    
    /*
     * Calls to the server to update the traps in this area
     * 
     * @TODO we should have a throttle check on this
     */
    public void requestTrapUpdate(double lat, double lon, double radiusInMiles) {
    	//Log.i(LOGNAME, "New Trap Request (" + lat + "," + lon + ") r:" + radiusInMiles);
	      
    	Intent intent = new Intent(application.getApplicationContext(), TrapSyncService.class);
    	intent.putExtra(TrapSyncService.EXTRA_LATITUDE, lat);
    	intent.putExtra(TrapSyncService.EXTRA_LONGITUDE, lon);
    	intent.putExtra(TrapSyncService.EXTRA_RADIUS, radiusInMiles);
    	application.getApplicationContext().startService(intent);
    }

    public void requestTrapUpdate(double lat, double lon, double radiusInMiles, boolean force) {
        {/*     	Log.i(LOGNAME, "New Trap Request ("+lat+","+lon+") r:"+radiusInMiles); */}

        Intent intent = new Intent(application.getApplicationContext(), TrapSyncService.class);
        intent.putExtra(TrapSyncService.EXTRA_LATITUDE, lat);
        intent.putExtra(TrapSyncService.EXTRA_LONGITUDE, lon);
        intent.putExtra(TrapSyncService.EXTRA_RADIUS, radiusInMiles);
        intent.putExtra(TrapSyncService.EXTRA_FORCE, force);
        application.getApplicationContext().startService(intent);
    }
   
    /**
     * Gets the appropriate bitmap icon for the trap. 
     * 
     * @TODO scale the bitmap icon for the device
     * 
     * @param trap
     * @return
     */
    public Bitmap getMapIconForTrap(Trap trap){
    	
    	String iconName = getIconNameForTrap(trap);
    	
    	if(iconName != null){

	 		try {
				return bitmapCacheManager.getBitmapFromImageResource(iconName,null);
			} catch (InvalidResourceKeyException e) {
 				android.util.Log.e(LOGNAME, "Invalid key:"+iconName+" or resource not loaded");
				return null;
			}
    	}else{
    		android.util.Log.e(LOGNAME, "Invalid traptype:"+trap.getTypeid()+" or resource not loaded");
    		return null; // default loading image? 
    	}
    }
    
    
    /**
     * Gets the traptype from the cache. @TODO should be be reloading the traptypes if this fails? 
     * 
     * 
     * @param trapTypeId
     * @return
     */
    public TrapType getTrapType(int trapTypeId)
    {
        TrapType type = null;
        if (sortedTypes == null)
            sortTrapTypes();
        for (TrapType trapType : sortedTypes)
        {
            if (trapType.getId() == trapTypeId)
            {
                type = trapType;
                break;
            }

        }
        if (type == null)
    	    type = trapTypeDao.findById(trapTypeId);
	 	return type;
    }
    
    public List<TrapType> getTrapTypes(){
    	return trapTypeDao.getTrapTypes();
    }
    
    /**
     * Returns the ImageSpec for the icon. This may be adjusted for screen size
     * 
     * @TODO adjust the images for screen size
     * 
     * @param trap
     * @return
     */
    public ImageSpec getImageSpecForTrap(Trap trap){
    	
    	String iconName = getIconNameForTrap(trap);
    	
    	ImageResource resource = imageResourceDao.findByKey(iconName);
    	
    	return (resource != null)?resource.getSpec():null;
    	
    }

    public int deleteAllTraps()
    {
        return trapDao.clearAllTraps();
    }
    
    /**
     * 
     * Returns true if the traptypes, categories and attributes are loaded
     * 
     * @return
     */
    public boolean isConfigLoaded(){
    	return (sharedPreferences.getLong(Defaults.PREFERENCE_LAST_UPDATE_TIME_ALLATTRIBUTES, 0) != 0);	
    }
    
    
    public String getIconNameForTrap(Trap trap){
    	String name = null;
    	TrapType type = getTrapType(trap.getTypeid());
    	
    	if(type != null){
    	
	    	int trapLevel = trap.getLevel();
	
	    	for (TrapLevel level : type.getLevels()) {
				if (trapLevel == (1 << level.getBit())){
                    name = level.getIcon();
                    Log.e(LOGNAME,"TrapType Icon Name:"+name);
                }
			}
			
    	}else{
     		android.util.Log.e(LOGNAME, "Invalid traptype:"+trap.getTypeid()+" or resource not loaded."+trapTypes.size());
    		return null; // default loading image? 
    	}
    	return name; 
    }

    public boolean isTrapVisible(Trap trap){

        boolean display = false;

        TrapType type = getTrapType(trap.getTypeid());

        if(type == null)// traps not loaded yet
            return false;


        String showOnMapPreference = type.getAudioid() + "_visible";
        boolean showOnMap = sharedPreferences.getBoolean(showOnMapPreference, true);


        if(showOnMap){
            StringBuffer sb = new StringBuffer();

            for(TrapLevel level:type.getLevels())
                sb.append(level.getBit()+",");

            String filter = sharedPreferences.getString(type.getAudioid()+"_filter", sb.toString());

            StringTokenizer token = new StringTokenizer(filter,",");

            while(token.hasMoreTokens()){
                int bitField = Integer.parseInt(token.nextToken());
                if (trap.getLevel() == (1 << bitField)){

                    display = true;
                    break;
                }
            }
        }

        return display;
    }

    public void setActiveTraps(List<Trap> activeTraps) {
        //this.activeTraps = activeTraps;
    }

    public void addToDismissTraps(long trapId){
{/*         //Log.d(LOGNAME,"Adding trap:"+trapId+" to dismissed list"); */}
        dismissedTraps.put(trapId, System.currentTimeMillis());
    }

    public boolean isDismissedTrap(long trapId){

        boolean dismiss = true;

        if(!dismissedTraps.containsKey(trapId))
            dismiss = false;
        else{

            long timestamp = dismissedTraps.get(trapId);

            long elapsed= System.currentTimeMillis() - timestamp;
            long repeatInterval = sharedPreferences.getInt(Defaults.PREFERENCE_TRAP_ALERT_REPEAT_AUDIBLE, Defaults.DEFAULT_ALERT_REPEAT_MINUTES) * 1000;
            if (repeatInterval > 0)
            {
                if(elapsed > repeatInterval)
                {
                    dismissedTraps.remove(trapId);
                    dismiss = false;
                }
            }


        }
{/* 
        //Log.d(LOGNAME,"Trap  :"+trapId+" is in dismiss list:"+dismiss); */}

        return dismiss;

    }

    public void sortTrapTypes()
    {
        sortedTypes = getTrapTypes();
        if (sortedTypes != null)
            Collections.sort(sortedTypes, new PriorityTrapTypeComparator());

    }

    public void preloadTrapImages()
    {
        for (TrapType trapType : getTrapTypes())
        {
            try
            {
                String trapTypeIcon = trapType.getImageFilename();
                ImageResource imageResource = imageResourceDao.findByKey(trapTypeIcon);
                bitmapCacheManager.getBitmapFromImageResource(imageResource.getKey(), null);
                bitmapCacheManager.preloadSVGImages();
            }
            catch (InvalidResourceKeyException e)
            {

            }
            catch (NullPointerException npe)
            {

            }

        }
    }

    public List<TrapType> getSortedTrapTypes()
    {
        //if (sortedTypes == null)
            sortTrapTypes();
        return sortedTypes;
    }

    public List<TrapType> getSortedReportableOnlyTrapTypes()
    {
        ArrayList<TrapType> reportable = new ArrayList<TrapType>();
        //if (sortedTypes != null)
        //{
            for(TrapType type:getSortedTrapTypes())
            {
                boolean shownOnMap = sharedPreferences.getBoolean(type.getAudioid() + Defaults.SETTING_SHOW_ON_MAP, true);
                if(shownOnMap && type.isReportable())
                    reportable.add(type);

            }
      //  }
        return reportable;

    }



    private class PriorityTrapTypeComparator implements Comparator<TrapType> {

        public PriorityTrapTypeComparator() {
        }

        public int compare(TrapType t1, TrapType t2)
        {
            int sequence1 = t1.getCategory().getSequence();
            int sequence2 = t2.getCategory().getSequence();

            if (sequence1 > sequence2)
                return 1;
            else if (sequence1 == sequence2)
                return 0;
            else
                return -1;
        }
    }
   
}
