package com.trapster.android.manager;

import android.graphics.Color;
import android.util.Log;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.here.android.common.GeoPosition;
import com.trapster.android.activity.fragment.LinkMatchingMapLayer;
import com.trapster.android.comms.INLPAPI;
import com.trapster.android.comms.TrafficRouteLinkListener;
import com.trapster.android.model.RouteLink;
import com.trapster.android.model.RouteLinkQuadkey;
import com.trapster.android.model.RouteLinks;
import com.trapster.android.model.provider.db.RouteLinkDatabaseAdapter;
import com.trapster.android.util.GeographicUtils;
import com.trapster.android.util.MapUtils;
import com.trapster.android.util.TrapsterError;
import com.vividsolutions.jts.geom.*;

import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Singleton
public class RouteLinkManager implements TrafficRouteLinkListener
{
    static final String LOGNAME= "Trapster.RouteLinkManager";
    //
    private static final int MAX_STORED_GEOPOSITIONS = 5;
    private static final int MAX_STORED_ROUTELINKS = 3;
    private static final int MAPTILE_CACHE_ZOOM_LEVEL= 15;
    private static final int PROJECT_ROUTE_LINK_NUM_LEVELS= 5;
    private static final double DISTANCE_TOLERANCE = 0.0005;
    private static final int MIN_BOUNDING_BOX_SIZE_IN_METERS = 40;
    private static final long QUADKEY_SHELF_LIFE =  7 * 24 * 60 * 60 * 1000; //7 days

    @Inject
    RouteLinkDatabaseAdapter mProvider;
    @Inject
    INLPAPI nlpApi;
    @Inject
    LinkMatchingMapLayer linkMatchingMapLayer;


    @Override
    public void onError(TrapsterError error) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    //
    public static interface OnNearestRouteLinkUpdateListener
    {
        void onNearestRouteLinkUpdated(RouteLink nearestRouteLink);
    }
    //
    boolean hackyCheckToDiscardTiles = true;
    String mCurrentQuadkey = "";
    String mFetchedQuadkey = "";
    GeoPosition mLocation;
    RouteLink mCurrentRouteLink;
    LinkedList<GeoPosition> locations = new LinkedList<GeoPosition>();
    ArrayList<RouteLink> routeLinks;
    LinkedList<RouteLink> historicalRouteLinks = new LinkedList<RouteLink>();
    ArrayList<RouteLink> projectedRouteLinks= new ArrayList<RouteLink>();
    //
    ExecutorService mReadableThreadPool= Executors.newFixedThreadPool(1);
    List<String> mHttpRequestIds= Collections.synchronizedList(new ArrayList<String>());
    //

    public RouteLinkManager() {

    }

    @Inject
    public void init() {

    }


    public String calculateQuadkey(double lat, double lng)
    {
        int[] tiles= MapUtils.calculateTileRange((float) lat, (float) lng, (float) lat, (float) lng,
                MAPTILE_CACHE_ZOOM_LEVEL, new int[5]);
        return MapUtils.xyzToQuadkey(tiles[0], tiles[1], MAPTILE_CACHE_ZOOM_LEVEL);
    }

    private void discardExpiredQuadkeys()
    {
        hackyCheckToDiscardTiles = false;
        // all quadkeys created past 7 days ago
        ArrayList<RouteLinkQuadkey> routeLinkQuadkeys= mProvider.getQuadkeysBefore(System.currentTimeMillis() - QUADKEY_SHELF_LIFE);

        int n= routeLinkQuadkeys.size();
        for (int i= 0; i < n; i++)
        {
            RouteLinkQuadkey routeLinkQuadkey= routeLinkQuadkeys.get(i);
            String quadkey= routeLinkQuadkey.getQuadkey();

            //do not remove the quadkey I am currently in!
            if (!mCurrentQuadkey.equals(quadkey))
            {
                mProvider.removeQuadkey(routeLinkQuadkey);
                mProvider.removeRouteLinksByQuadkey(quadkey);
            }
        }
    }

    private ArrayList<RouteLink> createProjectedRoute()
    {
        RouteLink routeLink = null;
        projectedRouteLinks.clear();

        if(mCurrentRouteLink == null)
            return null;

        projectedRouteLinks.add(mCurrentRouteLink);

        for (int i = 0; i < PROJECT_ROUTE_LINK_NUM_LEVELS; i++)
        {
            routeLink = getProjectedRouteLink(getAdjacentRouteLinks());
            if(routeLink == null)
                return projectedRouteLinks;
            else
                projectedRouteLinks.add(routeLink);
        }
        return projectedRouteLinks;
    }

    /*
    This is the meat of intelligent alerting.  Determines which route link we THINK you will be travelling on.
     */

    private RouteLink getProjectedRouteLink(ArrayList<RouteLink> adjacentRouteLinks)
    {
        RouteLink adjacentRouteLink;
        String adjacentRouteLinkLabel, routeLinkLabel;
        Point startPoint;
        RouteLink routeLink = projectedRouteLinks.get(projectedRouteLinks.size()-1);
        Point routeLinkEndPoint = routeLink.getShapeGeometry().getEndPoint();
        for (int i = 0; i < adjacentRouteLinks.size(); i++)
        {
            //need to compair the endpoint and start points first
            adjacentRouteLink = adjacentRouteLinks.get(i);
            startPoint = adjacentRouteLink.getShapeGeometry().getStartPoint();

            adjacentRouteLinkLabel = adjacentRouteLink.getAddressInfo().getLabel();
            routeLinkLabel = routeLink.getAddressInfo().getLabel();

            //if the start and end points match AND the labels are the same then we have a WINNER!
            if(routeLinkEndPoint.equals(startPoint) && adjacentRouteLinkLabel.compareTo(routeLinkLabel) == 0)
                return adjacentRouteLink;
        }
        return getUnmatchedRouteLink(routeLink, adjacentRouteLinks);
    }

    private ArrayList<RouteLink> eliminateFoundRouteLinks(ArrayList<RouteLink> routeLinkArrayList)
    {
        String projectRouteLinkID = null;
        RouteLink projectedRouteLink = projectedRouteLinks.get(projectedRouteLinks.size()-1);
        ArrayList<RouteLink> cleanRouteLinks = new ArrayList<RouteLink>();

        //get rid of the +/- in front of the route link id.  This will remove route links in both directions
        projectRouteLinkID = projectedRouteLink.getLinkId().substring(1);
        for (int j = 0; j < routeLinkArrayList.size(); j++)
        {
            String routeLinkId = routeLinkArrayList.get(j).getLinkId().substring(1);
            if(routeLinkId.compareTo(projectRouteLinkID) != 0)
                cleanRouteLinks.add(routeLinkArrayList.get(j));
        }
        return cleanRouteLinks;
    }

           /*
        author: snazi

        Need to calcuate the angle between two lines.  The one closest to 180 degrees or a straight lines wins.
        If we can't match an adjacent route link to the previous route link using the street name
        then we are assuming the user will be travelling along a straight line.

     */

    RouteLink getUnmatchedRouteLink(RouteLink projectedRouteLink, ArrayList<RouteLink> adjacentRouteLinks)
    {
        int numofPoints =  projectedRouteLink.getShapeGeometry().getNumPoints();
        Point startPoint = projectedRouteLink.getShapeGeometry().getPointN(numofPoints-2);
        Point endPoint =  projectedRouteLink.getShapeGeometry().getEndPoint();
        double projctedRouteLinkAngle = GeographicUtils.angleBetweenTwoPointsInDegrees(startPoint, endPoint);
        double shortestAngle = Double.MAX_VALUE;
        double angleOffset = 0.0;
        RouteLink routeLink = null;
        RouteLink chosenRouteLink = null;
        double routeLinkAngle = 0.0;

        for (int i = 0; i < adjacentRouteLinks.size(); i++)
        {
            routeLink = adjacentRouteLinks.get(i);
            startPoint = routeLink.getShapeGeometry().getStartPoint();
            endPoint =  routeLink.getShapeGeometry().getPointN(1);
            routeLinkAngle = GeographicUtils.angleBetweenTwoPointsInDegrees(startPoint, endPoint);

            angleOffset = Math.abs(projctedRouteLinkAngle - routeLinkAngle);

            if(angleOffset < shortestAngle)
            {
                chosenRouteLink = routeLink;
                shortestAngle = angleOffset;
            }
        }

        return chosenRouteLink;

    }

    ArrayList<RouteLink> getAdjacentRouteLinks()
    {
        Point endPoint = projectedRouteLinks.get(projectedRouteLinks.size()-1).getShapeGeometry().getEndPoint();
        Geometry bbox = GeographicUtils.createBoundingBox(endPoint.getY(), endPoint.getX(), MIN_BOUNDING_BOX_SIZE_IN_METERS*2);

        //linkMatchingMapLayer.drawBoundingBox(bbox.getEnvelopeInternal(), Color.RED);

        ArrayList<RouteLink> bboxRouteLinks = new ArrayList<RouteLink>();

        for (int i = 0; i < routeLinks.size(); i++)
        {
            if(bbox.crosses(routeLinks.get(i).getShapeGeometry()))
                bboxRouteLinks.add(routeLinks.get(i));
        }

        return eliminateFoundRouteLinks(bboxRouteLinks);
    }

    public HashSet<RouteLink> getProjectedRouteLinks()
    {
        return new HashSet<RouteLink>(projectedRouteLinks);
    }

    RouteLink routeLinkEntryHeadingFilter(ArrayList<RouteLink> routeLinkArrayList)
    {
        Point routeLinkStartPoint, routeLinkEndPoint;
        int adjacentRouteLinkAngle = 0;
        RouteLink routeLink = null;
        RouteLink chosenRouteLink = null;
        double shortestAngle = Double.MAX_VALUE;
        double angleOffset = 0.0;

        for (int i = 0; i < routeLinkArrayList.size(); i++)
        {
            routeLink = routeLinkArrayList.get(i);

            routeLinkStartPoint = routeLink.getShapeGeometry().getPointN(0);
            routeLinkEndPoint = routeLink.getShapeGeometry().getPointN(1);

            adjacentRouteLinkAngle = (int)GeographicUtils.angleBetweenTwoPointsInDegrees(routeLinkStartPoint, routeLinkEndPoint);
            angleOffset = Math.abs(mLocation.getHeading() - adjacentRouteLinkAngle);

            if(angleOffset < shortestAngle)
            {
                chosenRouteLink = routeLink;
                shortestAngle = angleOffset;
            }
        }

        return chosenRouteLink;
    }

    /*
        return the nearest route links based on the distance tolerance


     */

    ArrayList<RouteLink> sortNearestRouteLinks(ArrayList<RouteLink> routeLinkArrayList)
    {
        double averageDistance = -1.0;
        double cumulativeDistance = 0.0;
        RouteLink routeLink = null;

        ArrayList<RouteLink> shortestRouteLinks = new ArrayList<RouteLink>();

        for (int i = 0; i < routeLinkArrayList.size(); i++)
        {
            cumulativeDistance = 0.0;
            routeLink = routeLinkArrayList.get(i);
            LineString routelinkLineString = routeLink.getShapeGeometry();
            for(int j = 0; j < locations.size(); j++)
            {
                Coordinate coordinate = new Coordinate(locations.get(j).getCoordinate().getLongitude(), mLocation.getCoordinate().getLatitude());
                Point myPoint = new GeometryFactory().createPoint(coordinate);

                cumulativeDistance += routelinkLineString.distance(myPoint);
            }

            averageDistance = cumulativeDistance/locations.size();

            if(averageDistance < DISTANCE_TOLERANCE)
            {
                routeLink.setLocationDistance(averageDistance);
                if(shortestRouteLinks.size() == 0)
                    shortestRouteLinks.add(routeLink);
                else
                {
                    for(int k = 0; k < shortestRouteLinks.size(); k++)
                    {
                        if (shortestRouteLinks.get(k).getLocationDistance() >  routeLink.getLocationDistance())
                        {
                            shortestRouteLinks.add(k, routeLink);
                            break;
                        }
                        //if this route link is the longest distance away add it to the end of the list
                        if(k == shortestRouteLinks.size()-1)
                        {
                            shortestRouteLinks.add(routeLink);
                            break;
                        }
                    }
                }
            }
        }

        return shortestRouteLinks;
    }

    RouteLink calcNearestRouteLink(ArrayList<RouteLink> routeLinkArrayList)
    {
        ArrayList<RouteLink> shortestRouteLinks = sortNearestRouteLinks(routeLinkArrayList);

        if(shortestRouteLinks.size() == 0)
            return null;

        return routeLinkEntryHeadingFilter(shortestRouteLinks);
    }



    RouteLink getNearestRouteLink()
    {
        double accuracy = 0.0;
        //only fetch the quad if i don't have it already
        if(mCurrentQuadkey.compareTo(mFetchedQuadkey) != 0)
        {
            routeLinks = mProvider.getRouteLinksByQuadkey(mCurrentQuadkey);
            mFetchedQuadkey = mCurrentQuadkey;
        }

        if (mLocation.getLatitudeAccuracy() >= MIN_BOUNDING_BOX_SIZE_IN_METERS)
            accuracy = mLocation.getLatitudeAccuracy();
        else
            accuracy = MIN_BOUNDING_BOX_SIZE_IN_METERS;

        Geometry bbox = GeographicUtils.createBoundingBox(mLocation.getCoordinate().getLatitude(),
                mLocation.getCoordinate().getLongitude(), accuracy);

        ArrayList<RouteLink> bboxRouteLinks = new ArrayList<RouteLink>();

        for (int i = 0; i < routeLinks.size(); i++)
        {
            if(bbox.crosses(routeLinks.get(i).getShapeGeometry()))
                bboxRouteLinks.add(routeLinks.get(i));
        }

        addToHistoricalRouteLinks(calcNearestRouteLink(bboxRouteLinks));

        return getMatchedRouteLink();
    }

    int loadQuadkey(String quadkey)
    {
        // 1 = loaded
        // 0 = fetching
        // -1 = already fetching
        int result= 1;

        if (!mProvider.hasQuadkey(quadkey))
        {
            if (!mHttpRequestIds.contains(quadkey))
            {
                mHttpRequestIds.add(quadkey);
                // fetch quadkey
                nlpApi.getQuadkeyLinkInfo(quadkey, this, null);

                result= 0;
            } else
                result= -1;
        }

        return result;
    }


    //load in quads with the three in front and two os either side
    void lookahead ()
    {
        double radius = 0.01; //roughly 1 KM
        double angleOffset = 45.0;
        double startAngle = -90.0;
        double lng = mLocation.getCoordinate().getLongitude();
        double lat = mLocation.getCoordinate().getLatitude();
        Coordinate myLocation = new Coordinate(lng, lat);

        for (int i = 0; i < 5; i++)
        {
            Point point = GeographicUtils.findPointUsingHeadingAndRadius(myLocation, mLocation.getHeading(), (angleOffset*i) + startAngle, radius);
            loadQuadkey(calculateQuadkey(point.getY(), point.getX()));
        }
    }

    private void addToHistoricalLocations()
    {
        if(locations.size() >= MAX_STORED_GEOPOSITIONS)
            locations.remove(0);

        locations.add(mLocation);
    }

    private void addToHistoricalRouteLinks(RouteLink routeLink)
    {
        if(historicalRouteLinks.size() >= MAX_STORED_ROUTELINKS)
            historicalRouteLinks.remove(0);

        historicalRouteLinks.add(routeLink);
    }

    private RouteLink getMatchedRouteLink()
    {
        RouteLink routeLink = null;
        int matchedRouteLinks = 0;

        if(historicalRouteLinks.size() == 0)
            return null;

        for (int i = 0; i < historicalRouteLinks.size(); i++)
        {
            matchedRouteLinks = 0;
            routeLink = historicalRouteLinks.get(i);

            if(routeLink == null)
                return null;

            for(int j = 0; j < historicalRouteLinks.size(); j++)
            {
                if(historicalRouteLinks.get(j) != null)
                {
                    if(historicalRouteLinks.get(j).getLinkId().compareTo(routeLink.getLinkId()) == 0)
                        matchedRouteLinks++;

                    if(matchedRouteLinks >= (MAX_STORED_ROUTELINKS/2)+1)
                        return routeLink;
                }
            }
        }
        return historicalRouteLinks.get(historicalRouteLinks.size()-1);
    }

    public int requestNearestRouteLinkUpdate(final GeoPosition location, final RouteLink previousRouteLink, final OnNearestRouteLinkUpdateListener listener)
    {
        mLocation = location;
        int fetchFlag; // 0 if no httpRequest is needed; otherwise 1
        //

        mCurrentQuadkey = calculateQuadkey(mLocation.getCoordinate().getLatitude(), mLocation.getCoordinate().getLongitude());

        if(hackyCheckToDiscardTiles)
            discardExpiredQuadkeys();

        //adds your location to your historical locations
        addToHistoricalLocations();

        int result= loadQuadkey(mCurrentQuadkey);

        //linkMatchingMapLayer.drawBoundingBox(mLocation, Color.RED);


        // 1 = loaded
        // 0 = fetching
        // -1 = already fetching
        if (result == 1)
        {
            Runnable fetchWorker= new Runnable()
            {
                public void run()
                {
                    linkMatchingMapLayer.drawMyLocation(location.getCoordinate(), Color.BLACK);

                    mCurrentRouteLink = getNearestRouteLink();

                    if (mCurrentRouteLink != null)
                    {
                        linkMatchingMapLayer.drawRouteLink(mCurrentRouteLink, Color.GREEN);
                        lookahead();
                        if(previousRouteLink == null || previousRouteLink.getLinkId().compareTo(mCurrentRouteLink.getLinkId()) != 0)
                            createProjectedRoute();
                        //linkMatchingMapLayer.drawProjectedRoute(projectedRouteLinks, Color.MAGENTA);
                    }

                    listener.onNearestRouteLinkUpdated(mCurrentRouteLink);
                }
            };
            mReadableThreadPool.execute(fetchWorker);
            //
            fetchFlag= 0;
        } else
        {
            if (result == 0)
            {
                // fetch waypoint for faster response
                nlpApi.getWaypointLinkInfo(mLocation.getCoordinate().getLatitude(), mLocation.getCoordinate().getLongitude(), this, null );
            }
            //
            fetchFlag= 1;
        }
        return fetchFlag;
    }



    public int requestNearestRouteLinkUpdate(final double lat, final double lng,
                                             final OnNearestRouteLinkUpdateListener listener)
    {
        int fetchFlag; // 0 if no httpRequest is needed; otherwise 1
        //
        mCurrentQuadkey= calculateQuadkey(lat, lng);
        int result= loadQuadkey(mCurrentQuadkey);
        // 1 = loaded
        // 0 = fetching
        // -1 = already fetching
        if (result == 1)
            fetchFlag= 0;
        else
        {
            if (result == 0)
            {
                // fetch waypoint for faster response
                nlpApi.getWaypointLinkInfo(lat, lng, this, null );
            }
            //
            fetchFlag= 1;
        }
        return fetchFlag;
    }


    public RouteLinkQuadkey getQuadFromDB(String Quadkey)
    {
        return mProvider.getQuadkey(Quadkey);
    }

    public ArrayList<RouteLink> getRouteLinkbyQuadkey(String quadkey)
    {
        return mProvider.getRouteLinksByQuadkey(quadkey);
    }

    public ArrayList<RouteLink> getRouteLinksByBBox(Envelope envelope)
    {
        return mProvider.getRouteLinksByBoundingBox(envelope);
    }

    @Override
    public void onTrafficUpdate(RouteLinks routelinks, String quadkey) {

        if(routelinks != null)
        {
            if(quadkey != null)
            {
                //quadkey callback
                mProvider.bulkAddRouteLinks(quadkey, routelinks);
                //
                RouteLinkQuadkey routeLinkQuadkey= new RouteLinkQuadkey();
                routeLinkQuadkey.setQuadkey(quadkey);
                routeLinkQuadkey.setCreateDa(new Timestamp(System.currentTimeMillis()));
                mProvider.addQuadkey(routeLinkQuadkey);
                mHttpRequestIds.remove(quadkey);
            }
            else
            {
                //waypoint callback
                RouteLink routeLink = routelinks.getRouteLinks().get(0);
                routeLink.setLineString(routeLink.getShape());
            }
        }
    }
}
