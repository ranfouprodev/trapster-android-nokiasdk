package com.trapster.android.manager;

import com.trapster.android.model.Trap;
import roboguice.inject.ContextSingleton;

import java.util.ArrayList;
import java.util.List;

@ContextSingleton
public class TrapUpdateManager
{
    private static ArrayList<TrapUpdater> listeners;
    private static ArrayList<Trap> queuedTraps;

    private static Thread trapProcessor = new TrapProcessor();

    public TrapUpdateManager()
    {
        listeners = new ArrayList<TrapUpdater>();
        queuedTraps = new ArrayList<Trap>();
    }

    public static void addListener(TrapUpdater listener)
    {
        listeners.add(listener);
    }

    public static void removeListener(TrapUpdater listener)
    {
        listeners.remove(listener);
    }

    public static void notifyTrapUpdate(List<Trap> traps)
    {
        synchronized (queuedTraps)
        {
            queuedTraps.addAll(traps);
        }

        if (trapProcessor == null)
            trapProcessor = new TrapProcessor();

        switch (trapProcessor.getState())
        {
            case NEW:
            {
                trapProcessor.start();
                break;
            }
            case TERMINATED:
            {
                trapProcessor = new TrapProcessor();
                trapProcessor.start();
                break;
            }
        }
    }

    private static class TrapProcessor extends Thread
    {
        public void run()
        {
            boolean isFinished;

            synchronized (queuedTraps)
            {
                isFinished = (queuedTraps.size() == 0);
            }

            while (!isFinished)
            {
                Trap trap = null;
                synchronized (queuedTraps)
                {
                    isFinished = (queuedTraps.size() == 0);
                    if (queuedTraps.size() > 0)
                    {
                        trap = queuedTraps.get(0);
                        queuedTraps.remove(0);
                    }

                }
                if (trap != null)
                {
                    for (TrapUpdater listener : listeners)
                    {
                        listener.onTrapUpdate(trap);
                    }
                }

                try {Thread.sleep(1000);} catch (InterruptedException e) { e.printStackTrace();}

            }
        }
    }

    public interface TrapUpdater
    {
        public void onTrapUpdate(Trap traps);
    }
}
