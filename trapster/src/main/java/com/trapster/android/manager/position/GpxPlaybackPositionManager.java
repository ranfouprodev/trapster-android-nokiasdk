package com.trapster.android.manager.position;

import android.os.Handler;

import com.here.android.common.GeoCoordinate;
import com.here.android.common.GeoPosition;
import com.here.android.common.LocationMethod;
import com.here.android.common.LocationStatus;
import com.here.android.common.PositionListener;
import com.here.android.mapping.MapFactory;
import com.trapster.android.util.GeographicUtils;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;


public class GpxPlaybackPositionManager implements IPositionManager {

    private static final String LOGNAME = "GpxPlaybackPositionManager";
    public static final long DEFAULT_TIME_BETWEEN_LOCATIONS = 2000; // 1000 is standard, lower will speed playback
    private InputStream is;
    private GpxReader reader;
    private boolean imported;
    private boolean running;
    private boolean startWhenLoaded;

    private PositionListener listener;
    protected TrackPoint[] points;
    private int pointIndex;

    public static GeoPosition lastPosition;
    private static double lastSpeed;


    private Handler handler = new Handler();
    private Runnable sendPointRunnable = new Runnable(){
        @Override
        public void run() {
            // Send the current point
            sendPoint();

            pointIndex++;
            if( pointIndex <= points.length-1){

                long currentPointTime = points[pointIndex-1].getTime().getTime();
                long pointTime = points[pointIndex].getTime().getTime();

                long delay = pointTime - currentPointTime;

                handler.postDelayed(sendPointRunnable, delay);

            }else{
                {/* 				//Log.d(LOGNAME,"All the points have been sent"); */}
                stop();
            }

        }
    };
    private long playbackStartTime;
    private GeoPosition lastKnownLocation;


    public GpxPlaybackPositionManager(final InputStream is) {
        super();
        setStream(is);
    }

    public void setStream(InputStream is) {
        this.is = is;

        importGpx();
    }

    private void importGpx() {
        {/*
		//Log.d(LOGNAME,"Importing GPX File"); */}

        reader = new GpxReader();

        startWhenLoaded = false;

        imported = false;

        new Thread(new Runnable() {
            public void run() {
                try {
                    points = reader.readTrack(is);
                    {/*
					//Log.d(LOGNAME,"Finished Importing GPX File:"+points.length+" points loaded"); */}

                    imported = true;
                    if(startWhenLoaded)
                        start();
                } catch (IOException e) {
                    // TODO Don't know how to handle errors here
                    {/* 					//Log.e(LOGNAME, "Unable to read gpx file:"+e.getMessage()); */}
                }
            }
        }).start();
    }

    @Override
    public void start() {

        running = true;

        if(imported){
            pointIndex = 0;

            sendPoint();

            playbackStartTime = points[0].getTime().getTime();

            pointIndex++;
            if( pointIndex <= points.length){
                long pointTime = points[pointIndex].getTime().getTime();

                long delay = pointTime - playbackStartTime;

                handler.postDelayed(sendPointRunnable, delay);

            }else{
                {/* 				//Log.d(LOGNAME,"All the points have been sent"); */}
                stop();
            }


        }else{
            // File isn't finished loading yet
            {/* 			//Log.d(LOGNAME,"Delay start pending file being loaded"); */}
            startWhenLoaded = true;
        }


    }

    private void sendPoint() {
        if( pointIndex < points.length){

            TrackPoint point = points[pointIndex];
            {/* 				//Log.d(LOGNAME, "Sending Point ("+point.getLat()+","+point.getLng()+")"); */}

            if(listener != null)
                listener.onPositionUpdated(LocationMethod.NONE, point.toGeoPosition());

            lastKnownLocation = point.toGeoPosition();


        }else{
            {/* 				//Log.d(LOGNAME,"All the points have been sent"); */}
            stop();
        }


    }

    @Override
    public void stop() {
        running = false;

        handler.removeCallbacks(sendPointRunnable);
    }

    @Override
    public GeoPosition getLastLocation() {
        return (lastKnownLocation != null)?lastKnownLocation:null;
    }

    @Override
    public void addPositionListener(PositionListener listener) {
        this.listener = listener;
    }

    @Override
    public void removePositionListener(PositionListener listener) {
        stop();
        this.listener = null;
    }

    private class GpxReader extends DefaultHandler {
        private final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

        private List<TrackPoint> track = new ArrayList<TrackPoint>();
        private StringBuffer buf = new StringBuffer();
        private double lat;
        private double lon;
        private double ele;
        private Date time;

        private long startTime;

        public TrackPoint[] readTrack(InputStream in) throws IOException {
            try {
                SAXParserFactory factory = SAXParserFactory.newInstance();
                //factory.setValidating(true);
                SAXParser parser = factory.newSAXParser();
                startTime = System.currentTimeMillis();

                parser.parse(in, this);
                return getTrack();
            } catch (ParserConfigurationException e) {
                throw new IOException(e.getMessage());
            } catch (SAXException e) {
                throw new IOException(e.getMessage());
            }
        }

        @Override
        public void startElement(String uri, String localName, String qName,
                                 Attributes attributes) throws SAXException {
            buf.setLength(0);
            if (qName.equals("trkpt")) {
                lat = Double.parseDouble(attributes.getValue("lat"));
                lon = Double.parseDouble(attributes.getValue("lon"));
                time = null;
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName)
                throws SAXException {
            if (qName.equals("trkpt")) {

                if(time == null){
                    startTime += DEFAULT_TIME_BETWEEN_LOCATIONS;
                    time = new Date(startTime);
                }

                track.add(new TrackPoint(lat, lon, ele, time));
            } else if (qName.equals("ele")) {
                ele = Double.parseDouble(buf.toString());
            } else if (qName.equals("time")) {
                try {
                    time = TIME_FORMAT.parse(buf.toString());
                } catch (ParseException e) {
                    throw new SAXException("Invalid time " + buf.toString());
                }
            }
        }

        @Override
        public void characters(char[] chars, int start, int length)
                throws SAXException {
            buf.append(chars, start, length);
        }

        private TrackPoint[] getTrack() {
            return track.toArray(new TrackPoint[track.size()]);
        }
    }

    private class TrackPoint {
        private double lat;
        private double lng;
        private double h;
        private Date time;

        public TrackPoint(double lat, double lng, double h, Date time) {
            this.lat = lat;
            this.lng = lng;
            this.h = h;
            this.time = time;
        }

        public GeoPosition toGeoPosition() {

            GeoCoordinate coord = MapFactory.createGeoCoordinate(lat, lng);
            coord.setAltitude(h);

            //GeoPosition position = MapFactory.createGeoPosition(coord);
            FilteredGeoPosition position = new FilteredGeoPosition();
            position.setCoordinate(coord);
            position.setTimestamp(time);
            position.setSource("gpx");

            if(lastPosition != null){

                double heading = GeographicUtils.bearingTo(coord.getLatitude(), coord.getLongitude(),lastPosition.getCoordinate().getLatitude(), lastPosition.getCoordinate().getLongitude());

                heading = Math.abs((360 - heading) % 360);

                position.setHeading(heading);

                double distance = lastPosition.getCoordinate().distanceTo(coord);

                double deltaT = Math.abs((((double)lastPosition.getTimestamp().getTime()) - (double)(time.getTime()))/1000);

                double speed = distance/deltaT;

                lastSpeed = lastSpeed + ((speed - lastSpeed)*0.25);

                position.setSpeed(lastSpeed);

            }

            lastPosition = position;


            return position;
        }

        public double getLat() {
            return lat;
        }

        public double getLng() {
            return lng;
        }

        public double getH() {
            return h;
        }

        public Date getTime() {
            return time;
        }
    }

    @Override
    public LocationStatus getStatus() {
        return LocationStatus.AVAILABLE;
    }

}