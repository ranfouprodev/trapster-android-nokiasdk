package com.trapster.android.util;

import android.os.Handler;
import com.here.android.mapping.MapDataDownloadListener;
import com.here.android.mapping.MapRenderListener;
import com.here.android.restricted.odml.MapEngine;
import com.here.android.restricted.odml.ODMLMapFactory;
import com.trapster.android.activity.fragment.MapFragment;

import java.util.concurrent.atomic.AtomicInteger;

public class MapLoadCompleteListener
{
    private final static long DOWNLOAD_TIMEOUT = 2 * 1000;
    private MapEngine mapEngine;
    private MapFragment mapFragment;
    private OnMapLoadCompleted callback;

    private final Handler handler = new Handler();
    private volatile AtomicInteger numDownloads = new AtomicInteger(0);

    public MapLoadCompleteListener(MapFragment mapFragment, final OnMapLoadCompleted callback)
    {
        mapEngine = ODMLMapFactory.getMapEngine();
        this.mapFragment = mapFragment;
        this.callback = callback;

        mapEngine.addMapDataDownloadListener(new DownloadListener());
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                if (numDownloads.get() == 0)
                {
                    // Presumably, nothing needs to download
                    callback.onMapLoadCompleted();
                }
            }
        },DOWNLOAD_TIMEOUT);
    }

    private class DownloadListener implements MapDataDownloadListener
    {


        @Override
        public void onMapDataDownloadStart()
        {
            numDownloads.incrementAndGet();
            handler.removeCallbacks(null);
        }

        @Override
        public void onMapDataDownloadInProgress()
        {
        }

        @Override
        public void onMapDataDownloadEnd()
        {
            int currentValue = numDownloads.decrementAndGet();
            if (currentValue == 0)
            {
                handler.postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        /*if (numDownloads.get() == 0)
                        {

                        }
                        else
                            handler.postDelayed(this, DOWNLOAD_TIMEOUT);*/

                        mapEngine.removeMapDataDownloadListener(DownloadListener.this);
                        mapFragment.addRenderListener(new RenderListener());
                    }
                }, DOWNLOAD_TIMEOUT);
            }
        }
    }

    private class RenderListener implements MapRenderListener
    {
        @Override
        public void onDrawEnd(boolean b, long l)
        {
            if (!b)
            {
                mapFragment.removeRenderListener(this);
                callback.onMapLoadCompleted();
            }

        }

        @Override
        public void onSizeChanged(int i, int i2)
        {
        }
    }

    public interface OnMapLoadCompleted
    {
        public void onMapLoadCompleted();
    }
}
