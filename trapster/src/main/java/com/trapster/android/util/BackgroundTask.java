package com.trapster.android.util;

import android.app.Activity;
import com.trapster.android.manager.BackgroundTaskManager;

public abstract class BackgroundTask
{
    private long startTime = -1;
    private volatile long endTime = -1;
    private Activity activity;
    private boolean isRunning;

    public BackgroundTask()
    {

    }

    public BackgroundTask(Activity activity)
    {
        this.activity = activity;
    }

    public final void execute()
    {
        BackgroundTaskManager.execute(this);
    }

    public final void runTask()
    {
        isRunning = true;
        startTime = System.currentTimeMillis();
        try
        {
            if (isRunning)
                onPreExecuteInternal();
            if (isRunning)
                onExecuteInternal();
            if (isRunning)
                onPostExecuteInternal();
            endTime = System.currentTimeMillis();


            if (activity != null && isRunning)
            {
                activity.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        runOnUIThread();
                    }
                });
            }

        }
        catch (Exception e)
        {
            onException(e);
        }
        finally
        {
            isRunning = false;
        }
    }

    private final void onPreExecuteInternal() throws InterruptedException
    {
        onPreExecute();
    }

    public void onPreExecute()
    {

    }

    private final void onExecuteInternal() throws InterruptedException
    {
        onExecute();
    }

    public abstract void onExecute();


    private final void onPostExecuteInternal() throws InterruptedException
    {
        onPostExecute();
    }

    public void onPostExecute()
    {

    }

    public void runOnUIThread()
    {

    }

    public final void postTimeout()
    {
        if (isRunning)
            onTimeout(getRunningTime());
    }

    public abstract void onTimeout(long runTime);

    public void onException(Exception e)
    {

    }

    public synchronized boolean isRunning()
    {
        return isRunning;
    }

    public synchronized long getRunningTime()
    {
        long runningTime;
        if (startTime > -1)
            runningTime = System.currentTimeMillis() - startTime;
        else
            runningTime = 0;
        return runningTime;
    }

    public void cancel()
    {
        isRunning = false;
    }
}
