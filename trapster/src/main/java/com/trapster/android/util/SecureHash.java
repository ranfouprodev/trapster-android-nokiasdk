package com.trapster.android.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SecureHash {

	public static String secureHash(String alg, String input)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {

		MessageDigest md;
		md = MessageDigest.getInstance("MD5");
		byte[] md5hash = new byte[32];
		md.update(input.getBytes("iso-8859-1"), 0, input.length());
		md5hash = md.digest();

		 StringBuffer buf = new StringBuffer();
	        for (int i = 0; i < md5hash.length; i++) {
	        	int halfbyte = (md5hash[i] >>> 4) & 0x0F;
	        	int two_halfs = 0;
	        	do {
		        	if ((0 <= halfbyte) && (halfbyte <= 9))
		                buf.append((char) ('0' + halfbyte));
		            else
		            	buf.append((char) ('a' + (halfbyte - 10)));
		        	halfbyte = md5hash[i] & 0x0F;
	        	} while(two_halfs++ < 1);
	        }
	        return buf.toString();

	}

}
