package com.trapster.android.util;

public class TrapsterError {

	public static int TYPE_API_ERROR = 0;
	public static int TYPE_NO_RESPONSE = 1;
	public static int TYPE_INVALID_XML_FORMAT = 2;
	public static int TYPE_COMMUNICATION_ERROR = 3;
	public static int TYPE_INVALID_JSON_FORMAT = 4;
	public static int TYPE_UNKNOWN = 5;
	
	private int type; 
	private String details;
	
	public TrapsterError(int _type, String _details){
		type = _type; 
		details = _details; 
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
	
	
}
