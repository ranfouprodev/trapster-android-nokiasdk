package com.trapster.android.util;

import java.io.PrintWriter;
import java.io.StringWriter;

import com.trapster.android.BuildFeatures;

public class Log {

	public static enum LEVEL {
		all, none, debug, warn, info, error
	};
	
	private static LEVEL level = BuildFeatures.getLoggingLevel(); 

	public static void setLevel(LEVEL _level) {
		level = _level;
	}

	public static void d(String tag, String msg) {
		if (level == LEVEL.all  || level == LEVEL.warn || level == LEVEL.info || level == LEVEL.debug || level == LEVEL.error) {
			android.util.Log.d(tag, msg);
		}

	}

	public static void i(String tag, String msg) {
		if (level == LEVEL.all || level == LEVEL.warn || level == LEVEL.info || level == LEVEL.error ) {
			android.util.Log.i(tag, msg);
		}
	}

	public static void w(String tag, String msg) {
		if (level == LEVEL.all || level == LEVEL.warn || level == LEVEL.error) {
			android.util.Log.w(tag, msg);
		}
	}

	public static void e(String tag, String msg) {
		if (level == LEVEL.all || level == LEVEL.error) {
			android.util.Log.e(tag, msg);
		}
	}

	public static void v(String tag, String msg) {
		if (level != LEVEL.none) {
			android.util.Log.v(tag, msg);
		}
	}

	public static String getStackTrace(Throwable t) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw, true);
		t.printStackTrace(pw);
		pw.flush();
		sw.flush();
		return sw.toString();
	}

	public static void e(String tag, String string, NullPointerException e) {
		e(tag,string+":"+getStackTrace(e));
	}

	
	
}
