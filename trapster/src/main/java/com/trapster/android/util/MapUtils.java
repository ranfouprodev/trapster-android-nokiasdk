package com.trapster.android.util;

import com.here.android.common.GeoCoordinate;
import com.here.android.mapping.MapFactory;
import com.here.android.mapping.MapPolyline;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;

import java.util.Arrays;
import java.util.List;


public class MapUtils
{

   public static final double M_PI = Math.PI;
   public static final double EARTH_RADIUS_IN_METERS = 6372797.560856;

   private static final double DEG_TO_RAD = 0.017453292519943295769236907684886;
   private static final double RAD_TO_DEG = 57.295779513082322;

   
   /**
    * Retruns quadkey from x, y and z(zoom)
    * 
    * @param x the x co-ordinate of tile in Mercator system
    * @param y the x co-ordinate of tile in Mercator system
    * @param z the zoom level
    * @return the quadkey of the tile
    */
   public static String xyzToQuadkey(int x, int y, int z)
   {
      int xBit, yBit;
      int bit;
      int qBits;
      StringBuffer qString = new StringBuffer();
      for(int i = z - 1; i >= 0; i--)
      {
         bit = 0x01 << i;
         xBit = x & bit;
         yBit = y & bit;
         qBits = ((yBit == 0) ? 0 : 2) + ((xBit == 0) ? 0 : 1);
         qString.append(qBits);
      }
      return qString.toString();
   }

    /*
    * private static MapUtils mapUtils;
    *
    * private MapUtils() {
    *
    * }
    *
    * public static MapUtils getInstance() {
    *
    * if (mapUtils == null) { mapUtils = new MapUtils(); } return mapUtils; }
    */
    public static int[] calculateTileRange(float lat1, float lon1, float lat2, float lon2, int zoom, int[] resultRange)
    {

        double left, right, top, bottom;

        if(lat1 < lat2)
        {

            top = calculateNormalizedCoordinatesForLat(lat2);
            bottom = calculateNormalizedCoordinatesForLat(lat1);

        }
        else
        {

            top = calculateNormalizedCoordinatesForLat(lat1);
            bottom = calculateNormalizedCoordinatesForLat(lat2);

        }

        if(lon1 < lon2)
        {

            left = calculateNormalizedCoordinatesForLon(lon1);
            right = calculateNormalizedCoordinatesForLon(lon2);

        }
        else
        {

            left = calculateNormalizedCoordinatesForLon(lon2);
            right = calculateNormalizedCoordinatesForLon(lon1);
        }

        return calculateTileRangeWithNormalizedEdgeCoords(resultRange, top, left, bottom, right, zoom);

    }

    public static double calculateNormalizedCoordinatesForLat(double lat)
    {

        double invGD = inverseGudermannian(lat);
        double coord = (double)(M_PI - invGD) / (2.0 * M_PI);
        return coord;
    }

    public static double calculateNormalizedCoordinatesForLon(double lon)
    {

        if(lon > 180.0)
            return 1.0;
        else if(lon < -180.0)
            return 0.0;

        return (180.0 + lon) / 360.0;
    }

    public static double calculateLatForNormalizedCoord(double y)
    {
        // atan(sinh *)
        if(y > 1.0)
        {
            return -85.05112878;
        }
        else if(y < 0.0)
        {
            return 85.05112878;
        }
        // gudermannian function
        return(180.0d * (Math.atan(Math.sinh(2d * M_PI * (0.5d - y)))) / M_PI);
    }

    public static double calculateLonForNormalizedCoord(double x)
    {

        if(x > 1.0)
            return 180.0;
        else if(x < 0.0)
            return -180.0;
        return (double)(-180.0 + x * 360.0);
    }

    public static double inverseGudermannian(double lat)
    {
        if(lat >= 85.05112878)
            return M_PI;
        else if(lat <= -85.05112878)
            return -M_PI;

        // inv_gd = ln((1 + sin) / 1 - sin) / 2;
        double sinVal = Math.sin(M_PI * lat / 180.0);
        double invGD = Math.log((1 + sinVal) / (1 - sinVal)) / 2;
        return invGD;
    }

    public static int[] calculateTileRangeWithNormalizedEdgeCoords(int[] resultRange, double topNormalized, double leftNormalized, double bottomNormalized,
                                                                   double rightNormalized, int zoom)
    {

        int zoomTileRange = (int)Math.pow(2, zoom);//0x01 << zoom;Mat
        double tileXStart = leftNormalized * zoomTileRange;
        double tileXEnd = rightNormalized * zoomTileRange;
        double tileYStart = topNormalized * zoomTileRange;
        double tileYEnd = bottomNormalized * zoomTileRange;
        // make sure that the tileIDs are not out of index range
        tileXEnd = Math.min(tileXEnd, zoomTileRange - 1);
        tileXStart = Math.max(tileXStart, 0);
        tileYEnd = Math.min(tileYEnd, zoomTileRange - 1);
        tileYStart = Math.max(tileYStart, 0);

        resultRange[0] = (int)tileXStart;
        resultRange[1] = (int)tileYStart;
        resultRange[2] = (int)tileXEnd;
        resultRange[3] = (int)tileYEnd;
        resultRange[4] = zoom;
        return resultRange;
    }


    /**
     * To create the Bounding box from center and radius
     * @param center
     * @param radius
     * @return
     */
    public static Envelope getBoundingBoxByCenterAndRadius(GeoCoordinate center, double radius) {
        double longitudeSpan = GeographicUtils.calculateLongitudeSpanOnSameLatitudeLevel(
                (float) radius, center.getLatitude(), center.getLongitude());
        double latitudeSpan = GeographicUtils
                .calculateLatitudeSpanOnSameLongitudialLine((float) radius);
        double topLeftLat = center.getLatitude() - latitudeSpan;
        double topLeftLon = center.getLongitude() - longitudeSpan;
        double bottomRightLat = center.getLatitude() + latitudeSpan;
        double bottomRightLon = center.getLongitude() + longitudeSpan;

        Coordinate topLeft = new Coordinate(topLeftLat, topLeftLon);
        Coordinate bottomRight = new Coordinate(bottomRightLat, bottomRightLon);
        return new Envelope(topLeft, bottomRight );
    }

    public static MapPolyline generatePolyline(GeoCoordinate[] points)
    {
        return generatePolyline(Arrays.asList(points));
    }
    public static MapPolyline generatePolyline(List<GeoCoordinate> list)
    {
        return MapFactory.createMapPolyline(MapFactory.createGeoPolyline(list));
    }


   
}
