package com.trapster.android.util;

import android.content.res.Resources;
import com.trapster.android.R;

import java.util.Date;

public class PrettyTime
{
	
	public static String formatRelativeTime(Resources res, Date date){
		
		Date now = new Date(); 
			
		long deltaTime = (now.getTime() - date.getTime())/1000;
	
        return formatRelativeTime(res, deltaTime);
	}

    public static String formatRelativeTime(Resources res, long deltaTime)
    {
        long minutes = deltaTime / 60;

        long hours = deltaTime / 3600;

        long days = hours / 24;
        long months = days / 30;

        long years = days / 365;

        if(deltaTime < 2*60) // 2minutes
            return (res.getString(R.string.pretty_time_moment)==null)?"a moment ":res.getString(R.string.pretty_time_moment);

        if(deltaTime < 2*60*60){ // 2hours
            String ending =(res.getString(R.string.pretty_time_minutes)==null)?"minutes ":res.getString(R.string.pretty_time_minutes);
            return String.valueOf(minutes)+" "+ending;
        }

        if(deltaTime < 2*24*60*60){ // 2days
            String ending =(res.getString(R.string.pretty_time_hours)==null)?"hours ":res.getString(R.string.pretty_time_hours);
            return String.valueOf(hours)+" "+ending;
        }

        if(deltaTime < 60*24*60*60){ // ~2months
            String ending = (res.getString(R.string.pretty_time_days)==null)?"days ":res.getString(R.string.pretty_time_days);
            return String.valueOf(days)+" "+ending;
        }


        if(deltaTime < 2*365*24*60*60){ // ~2years
            String ending = (res.getString(R.string.pretty_time_months)==null)?"months ":res.getString(R.string.pretty_time_months);
            return String.valueOf(months)+" "+ending;
        }


        String ending = (res.getString(R.string.pretty_time_years)==null)?"years ":res.getString(R.string.pretty_time_years);
        return String.valueOf(years)+" "+ending;
    }

	
		
}
