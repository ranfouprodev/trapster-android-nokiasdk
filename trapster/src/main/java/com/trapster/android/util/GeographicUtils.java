package com.trapster.android.util;

import android.content.SharedPreferences;
import com.here.android.common.GeoBoundingBox;
import com.here.android.common.GeoCoordinate;
import com.here.android.mapping.MapFactory;
import com.trapster.android.Defaults;
import com.trapster.android.manager.position.UtmCoordinate;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;

import java.util.List;


public class GeographicUtils {
	private final static double pi = 3.1415926535;
	private final static double deg2rad = pi / 180;
	private final static double rad2deg = 180 / pi;
	// Using WGS84 Ellipsoid Parameters
	private final static double a = 6378137;
	private final static double ecc = 0.00669438;
	private final static double eccSquared = 0.000044815;
	private final static double sqrtEccSquared = 0.999977592;
	private final static double k0 = 0.9996;
	private static final String LOGNAME = "Framework.GeographicUtils";

	
	/**
	 * Calculate the latitude span on same longitude
	 * 
	 * @param distanceInMeters
	 * @return
	 */
	public static double calculateLatitudeSpanOnSameLongitudialLine(
			float distanceInMeters) {
		return rad2deg * (distanceInMeters / a);
	}
	
	public static Geometry createBoundingBox(double lat, double lng, double radiusInMeters){
		
		double latSpan = calculateLatitudeSpanOnSameLongitudialLine((float)radiusInMeters);
		
		double lonSpan = calculateLongitudeSpanOnSameLatitudeLevel((float)radiusInMeters, lat,lng);
		
		double minLatitude= lat - latSpan/2; 
		double maxLatitude= lat + latSpan/2;
		double minLongitude= lng - lonSpan/2;
		double maxLongitude= lng + lonSpan/2;
				
		Coordinate[] box = new Coordinate[5]; 
		box[0] = new Coordinate(minLongitude,maxLatitude);
		box[1] = new Coordinate(maxLongitude,maxLatitude);
		box[2] = new Coordinate(maxLongitude,minLatitude);
		box[3] = new Coordinate(minLongitude,minLatitude);
		box[4] = new Coordinate(minLongitude,maxLatitude);
		
		GeometryFactory gf = new GeometryFactory(); 
		Geometry envelope = gf.createPolygon(gf.createLinearRing(box), null); 
				
		return envelope; 
		  
	}
	

	/**
	 * Calculates the longitude span on same latitude
	 * 
	 * @param distanceInMeters
	 *            distance in meters
	 * @param latitude
	 *            the latitude
	 * @param longitude
	 *            the longitude
	 * @return
	 */
	public static double calculateLongitudeSpanOnSameLatitudeLevel(
			float distanceInMeters, double latitude, double longitude) {
		if (latitude == 90.0 || latitude == -90.0)
			return 0.0;
		double cosine = Math.cos(latitude * deg2rad);
		double radius = a * cosine;
		return rad2deg * distanceInMeters / radius;
	}
	
	public static double geographicDistance(double latFrom, double lonFrom, double latTo, double lonTo) {
		double R = 6378137; // radius in meters
		double DEG2RAD = Math.PI / 180;
		double deltaLat = latFrom - latTo;
		double deltaLon = lonFrom - lonTo;

		deltaLat = deltaLat * DEG2RAD;
		deltaLon = deltaLon * DEG2RAD;

		double a = (Math.sin(deltaLat / 2) * Math.sin(deltaLat / 2))
				+ Math.cos(latTo * DEG2RAD) * Math.cos(latFrom * DEG2RAD)
				* (Math.sin(deltaLon / 2) * Math.sin(deltaLon / 2));
		double c = 2 * java.lang.Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = R * c;

		return distance;
	}
	
	/**
	 * Calculates a bearing from north (0) increasing east in radians
	 * Uses the spherical law of cosines
	 * 
	 * @param latFrom
	 * @param lonFrom
	 * @param latTo
	 * @param lonTo
	 * @return
	 */
	public static double bearingTo(double latFrom, double lonFrom, double latTo, double lonTo){
		
	 	double deltaLon = (lonTo - lonFrom)*deg2rad; 

		double y = Math.sin(deltaLon) * Math.cos(latFrom*deg2rad);
		double x = Math.cos(latTo*deg2rad)*Math.sin(latFrom*deg2rad) -
		        Math.sin(latTo*deg2rad)*Math.cos(latFrom*deg2rad)*Math.cos(deltaLon);
		double bearing = Math.atan2(y, x);
		
		bearing = bearing * rad2deg;
		bearing = (bearing + 360) % 360;
		
		
		return bearing; 
	}
	
	
	public static double mphToMetersPerSecond(double distance){
		return distance * 0.44704;
	}
	
	public static double kphToMetersPerSecond(double distance){
		return distance * 0.27778;
	}
	
	public static double metersPerSecondToKph(double distance){
		return distance * 3.6;
	}
	
	public static double milesToKilometers(double distance){
		return distance * 1.609344;
	}
	
	public static double kilometersToMiles(double distance){
		return distance / 1.609344;
	}

	public static double metersPerSecondToMph(double speed){
		return speed * 2.23694;
	}
	
	
	
    /**
     * Calculates and returns an GeoBoundingBox object that geometrically contains
     * all points in the given array.
     * @param points Array containing GeoCoordinate objects that must be contained
     * in the resulting bounding box.
     * @return An GeoBoundingBox that is sized and positioned so that every GeoCoordinates
     * in the given array is contained in the returned bounding box.
     * \note If the array of points is null or contains less than 2 GeoCoordinates,
     * it returns null. All invalid points are not included.
     */
    public static GeoBoundingBox coverAll(List<GeoCoordinate> points)
    {
        if(points.size() < 2)
        {
            return null;
        }
        float minLat = Float.MAX_VALUE;
        float minLon = Float.MAX_VALUE;
        float maxLat = -Float.MAX_VALUE;
        float maxLon = -Float.MAX_VALUE;

        for (GeoCoordinate p : points) {
            if (p != null && p.isValid()) {
                minLat = (float)Math.min(minLat, p.getLatitude());
                minLon = (float)Math.min(minLon, p.getLongitude());
                maxLat = (float)Math.max(maxLat, p.getLatitude());
                maxLon = (float)Math.max(maxLon, p.getLongitude());
            }
        }
        return MapFactory.createGeoBoundingBox(MapFactory.createGeoCoordinate(maxLat, minLon),
                MapFactory.createGeoCoordinate(minLat, maxLon));
    }

    public static long convertSpeedToUnits(double speed, SharedPreferences settings)
    {
        if (settings.getBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_MILES, true))
            return Math.round(speed * 2.23693629);
        else
            return Math.round(speed * 3.6);
    }

    public static double convertSpeedToMps(double speed, SharedPreferences settings)
    {
        if (settings.getBoolean(Defaults.PREFERENCE_MAP_OPTIONS_DISPLAY_MILES, true))
            return speed / 2.23693629;
        else
            return speed / 3.6;
    }

    
    public static UtmCoordinate convertToUTM(double lat, double lon) {
		int zone = computeZone(lat,lon);
		return convertToUTM(lat,lon, zone);
	}

	private static int computeZone(double lat, double lon) {


		double LongTemp = lon;
		// compute the UTM Zone from the latitude and longitude
		int zone  = (int)Math.floor((LongTemp + 180) / 6) + 1;
				
		if ((lat >= 56.0) && (lat < 64.0) && (LongTemp >= 3.0)
				&& (LongTemp < 12.0)) {
			zone = 32;
		}

		// Special zones for Svalbard
		if ((lat >= 72.0) && (lat < 84.0)) {
			if ((LongTemp >= 0.0) && (LongTemp < 9.0)) {
				zone = 31;
			} else if ((LongTemp >= 9.0) && (LongTemp < 21.0)) {
				zone = 33;
			} else if ((LongTemp >= 21.0) && (LongTemp < 33.0)) {
				zone = 35;
			} else if ((LongTemp >= 33.0) && (LongTemp < 42.0)) {
				zone = 37;
			}
		}

		return zone;
	}
	
	
	
	public static UtmCoordinate convertToUTM(double lat, double lon, int zone) {

		double LongOrigin;
		double eccPrimeSquared;
		double N;
		double T;
		double C;
		double A;
		double M;

		double LongTemp = lon;
		double LatRad = lat * deg2rad;
		double LongRad = LongTemp * deg2rad;
		double LongOriginRad;

		LongOrigin = ((zone - 1) * 6) - 180 + 3; // +3 puts origin in middle
													// of zone
		LongOriginRad = LongOrigin * deg2rad;

		eccPrimeSquared = (eccSquared) / (1 - eccSquared);
		
		N = a
				/ Math.sqrt(1 - (eccSquared * Math.sin(LatRad) * Math
						.sin(LatRad)));
		T = Math.tan(LatRad) * Math.tan(LatRad);
		C = eccPrimeSquared * Math.cos(LatRad) * Math.cos(LatRad);
		A = Math.cos(LatRad) * (LongRad - LongOriginRad);

		M = a
				* ((((1 - (eccSquared / 4)
						- ((3 * eccSquared * eccSquared) / 64) - ((5
						* eccSquared * eccSquared * eccSquared) / 256)) * LatRad)
						- ((((3 * eccSquared) / 8)
								+ ((3 * eccSquared * eccSquared) / 32) + ((45
								* eccSquared * eccSquared * eccSquared) / 1024)) * Math
								.sin(2 * LatRad)) + ((((15 * eccSquared * eccSquared) / 256) + ((45
						* eccSquared * eccSquared * eccSquared) / 1024)) * Math
						.sin(4 * LatRad))) - (((35 * eccSquared * eccSquared * eccSquared) / 3072) * Math
						.sin(6 * LatRad)));

		double easting = (double) ((k0 * N * (A
				+ (((1 - T + C) * A * A * A) / 6) + ((((5 - (18 * T) + (T * T) + (72 * C)) - (58 * eccPrimeSquared))
				* A * A * A * A * A) / 120))) + 500000.0);
		double northing = (double) (k0 * (M + (N * Math.tan(LatRad) * (((A * A) / 2)
				+ (((5 - T + (9 * C) + (4 * C * C)) * A * A * A * A) / 24) + ((((61
				- (58 * T) + (T * T) + (600 * C)) - (330 * eccPrimeSquared))
				* A * A * A * A * A * A) / 720)))));

		if (lat < 0) {
			northing += 10000000.0; // 10000000 meter offset for southern
									// hemisphere
		}

		UtmCoordinate utm = new UtmCoordinate();
		utm.setEasting(easting);
		utm.setNorthing(northing);
		utm.setZone(zone);

		return utm;
	}
	
	
	public static double[] convertToLL(UtmCoordinate coord) {
		double easting = coord.getEasting();
		double northing = coord.getNorthing();
		double zone = coord.getZone();
		
		double eccPrimeSquared;
		double e1 = (1 - sqrtEccSquared)
				/ (1 + sqrtEccSquared);
		double N1, T1, C1, R1, D, M;
		double LongOrigin;
		double mu,  phi1Rad;
		double x, y;

		x = easting - 500000.0; // remove 500,000 meter offset for longitude
		y = northing;

		if (y > 10000000)
			y = y - 10000000.0;// remove 10,000,000 meter offset used for
								// southern hemisphere

		LongOrigin = (zone - 1) * 6 - 180 + 3; // +3 puts origin in middle of
												// zone

		eccPrimeSquared = (eccSquared) / (1 - eccSquared);

		M = y / k0;
		mu = M
				/ (a * (1 - eccSquared / 4 - 3 * eccSquared * eccSquared / 64 - 5
						* eccSquared * eccSquared * eccSquared / 256));

		phi1Rad = mu + (3 * e1 / 2 - 27 * e1 * e1 * e1 / 32) * Math.sin(2 * mu)
				+ (21 * e1 * e1 / 16 - 55 * e1 * e1 * e1 * e1 / 32)
				* Math.sin(4 * mu) + (151 * e1 * e1 * e1 / 96)
				* Math.sin(6 * mu);
	

		N1 = a
				/ Math.sqrt(1 - eccSquared * Math.sin(phi1Rad)
						* Math.sin(phi1Rad));
		T1 = Math.tan(phi1Rad) * Math.tan(phi1Rad);
		C1 = eccPrimeSquared * Math.cos(phi1Rad) * Math.cos(phi1Rad);
	
		double r1t = 1 - eccSquared * Math.sin(phi1Rad)* Math.sin(phi1Rad);
		double r1t2 = (r1t*r1t)/Math.sqrt(r1t);
		R1 = a* (1 - eccSquared)/ r1t2;
		D = x / (N1 * k0);
		
		double lat = phi1Rad
				- (N1 * Math.tan(phi1Rad) / R1)
				* (D
						* D
						/ 2
						- (5 + 3 * T1 + 10 * C1 - 4 * C1 * C1 - 9 * eccPrimeSquared)
						* D * D * D * D / 24 + (61 + 90 * T1 + 298 * C1 + 45
						* T1 * T1 - 252 * eccPrimeSquared - 3 * C1 * C1)
						* D * D * D * D * D * D / 720);
		lat = lat * rad2deg;

		double lon = (D - (1 + 2 * T1 + C1) * D * D * D / 6 + (5 - 2 * C1 + 28
				* T1 - 3 * C1 * C1 + 8 * eccPrimeSquared + 24 * T1 * T1)
				* D * D * D * D * D / 120)
				/ Math.cos(phi1Rad);
		lon = LongOrigin + lon * rad2deg;

		
		double[] coords = new double[]{lat,lon};
	
		return coords;

	}


    public static double angleBetweenTwoPointsInDegrees(Point startPoint, Point endPoint)
    {
        double angle =  Math.toDegrees(Math.atan2(endPoint.getX()-startPoint.getX(), endPoint.getY()-startPoint.getY()));

        if(angle < 0)
            return angle+=360;
        else
            return angle;
    }


    public static Point findPointUsingHeadingAndRadius(Coordinate currentLocation, double heading, double angleOffset, double radius)
    {
        double x = radius * Math.cos(Math.toRadians(heading + angleOffset));
        double y = radius * Math.sin(Math.toRadians(heading + angleOffset));

        // new Coordinate(currentLocation.x + y, currentLocation.y + x) is correct.  X and Y are mixed together =/.
        Coordinate coordinate = new Coordinate(currentLocation.x + y, currentLocation.y + x);

        return new GeometryFactory().createPoint(coordinate);
    }
}
