package com.trapster.android.util;

import android.util.Log;
/**
 * Utility class to measure startup timing of events
 *  
 * @author John
 *
 */
public class Timing {

	private static final String LOGNAME = "Timing";
	private static long start = 0; 
	
	public static void tick(String id){
		if(start == 0)
			start = System.currentTimeMillis(); 
{/* 		
		//Log.d(LOGNAME, "Timing:"+id+" Elapsed "+(System.currentTimeMillis()-start)); */}
	}
	
}
