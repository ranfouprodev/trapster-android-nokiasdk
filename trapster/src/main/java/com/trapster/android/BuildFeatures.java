package com.trapster.android;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import com.trapster.android.util.Log.LEVEL;
import com.trapster.android.util.SecureHash;

import java.util.EnumSet;

public class BuildFeatures {

	public enum Feature {
		USE_LOGGING, USE_HOCKEY, USE_FLURRY, USE_STRICTMODE
	}

	private static final EnumSet<Feature> DEBUG = EnumSet
			.of(Feature.USE_LOGGING);
	private static final EnumSet<Feature> DOGFOOD = EnumSet.of(
			Feature.USE_LOGGING, Feature.USE_HOCKEY, Feature.USE_FLURRY);
	private static final EnumSet<Feature> MODERATOR = EnumSet.of(
			Feature.USE_LOGGING, Feature.USE_HOCKEY, Feature.USE_FLURRY);
	private static final EnumSet<Feature> PRODUCTION = EnumSet.of(
			Feature.USE_LOGGING, Feature.USE_FLURRY);

	private static final String LOGNAME = "BuildFeatures";

	// Device Info
    public static final boolean SUPPORTS_KITKAT =           android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT;
    public static final boolean SUPPORTS_JELLY_BEAN_MR2 =   android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2;
    public static final boolean SUPPORTS_JELLY_BEAN_MR1 =   android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1;
    public static final boolean SUPPORTS_JELLY_BEAN =       android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN;
    public static final boolean SUPPORTS_ICS_MR2 =          android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1;
	public static final boolean SUPPORTS_ICS =              android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH;
    public static final boolean SUPPORTS_HONEYCOMB_MR2 =    android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB_MR2;
    public static final boolean SUPPORTS_HONEYCOMB_MR1 =    android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB_MR1;
    public static final boolean SUPPORTS_HONEYCOMB =        android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB;
    public static final boolean SUPPORTS_GINGERBREAD_MR1 =  android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.GINGERBREAD_MR1;
    public static final boolean SUPPORTS_GINGERBREAD =      android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.GINGERBREAD;
	public static final boolean SUPPORTS_FROYO =            android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.FROYO;
    public static final boolean SUPPORTS_ECLAIR_MR_1 =      android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.ECLAIR_MR1;
    public static final boolean SUPPORTS_ECLAIR_0_1 =       android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.ECLAIR_0_1;
	public static final boolean SUPPORTS_ECLAIR =           android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.ECLAIR;
    public static final boolean SUPPORTS_DONUT =            android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.DONUT;
    public static final boolean SUPPORTS_CUPCAKE =          android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.CUPCAKE;
    public static final boolean SUPPORTS_BASE_1_1 =         android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.BASE_1_1;
    public static final boolean SUPPORTS_BASE =             android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.BASE;

    public static final String PROFILE = getContext().getResources().getString(
			R.string.build_profile);

	public static final String HOCKEYKEY = getContext().getResources()
			.getString(R.string.build_hockeykey);

	public static final String TRAPSTERKEY = getContext().getResources()
			.getString(R.string.build_trapsterkey).equals("${build.trapsterkey}")?"b9f50e2b4da4e74a7fbb7e4ec9c9d1d7" : getContext().getResources()
					.getString(R.string.build_trapsterkey);

	public static final String FLURRYKEY = getContext().getResources()
			.getString(R.string.build_flurrykey);

	public static final String BUILD = getContext().getResources().getString(
			R.string.build_version);

	public static final String MARKET = getContext().getResources().getString(
			R.string.build_market);

	public static final EnumSet<Feature> FEATURESET = getDefaultFeatureSet();

	public static int VERSIONCODE;

	public static String VERSIONNAME;

	static {
		try {
			PackageInfo pInfo = TrapsterApplication.appInstance
					.getPackageManager().getPackageInfo(
							TrapsterApplication.appInstance.getPackageName(),
							PackageManager.GET_META_DATA);
			VERSIONNAME = pInfo.versionName;
			VERSIONCODE = pInfo.versionCode;
		} catch (NameNotFoundException e) {
			VERSIONNAME = "";
			VERSIONCODE = -1;
		}
	}

	
	private BuildFeatures() {

	}

	private static EnumSet<Feature> getDefaultFeatureSet() {

		if (PROFILE.equalsIgnoreCase("production"))
			return PRODUCTION;
		else if (PROFILE.equalsIgnoreCase("moderator"))
			return MODERATOR;
		else if (PROFILE.equalsIgnoreCase("dogfood"))
			return DOGFOOD;
		else
			// includes Amazon, unsigned
			return DEBUG;
	}

	public static String getFeatureSet() {
		StringBuilder sb = new StringBuilder();
		for (Feature feature : FEATURESET)
			sb.append(feature.toString() + ",");

		return sb.toString();
	}

	public static boolean hasFeature(Feature feature) {
		return FEATURESET.contains(feature);
	}

	private static Context getContext() {
		return TrapsterApplication.getAppInstance();
	}

	public static final LEVEL getLoggingLevel() {
		if (PROFILE.equalsIgnoreCase("debug"))
			return LEVEL.all;
		else
			return LEVEL.none;
	}

	public static String getDeviceId() {
		// Get unique device information
		TelephonyManager mTelephonyMgr = (TelephonyManager) getContext() .getSystemService(Context.TELEPHONY_SERVICE);
		String deviceId = mTelephonyMgr.getDeviceId();

        if (deviceId == null)
        {
            deviceId = Settings.Secure.getString(TrapsterApplication.getAppInstance().getContentResolver(), Settings.Secure.ANDROID_ID);
        }

		if ("amazon".equals(MARKET))
			try {
				// encrypt for Amazon Market
				deviceId = SecureHash.secureHash("MD5", deviceId);
			} catch (Exception e) {
{/* 				//Log.e(LOGNAME, "Unable to create secure ID:" + e.getMessage()); */}
			}

		return deviceId;
	}

	public static String getDeviceType() {


		return Build.MODEL;

	}

}
