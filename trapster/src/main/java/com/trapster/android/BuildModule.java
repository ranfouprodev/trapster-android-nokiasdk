package com.trapster.android;

import com.google.inject.AbstractModule;
import com.trapster.android.activity.fragment.LinkMatchingMapLayer;
import com.trapster.android.activity.fragment.PatrolMapLayer;
import com.trapster.android.activity.fragment.TrapMapLayer;
import com.trapster.android.comms.CommunicationManager;
import com.trapster.android.comms.INLPAPI;
import com.trapster.android.comms.ITrapsterAPI;
import com.trapster.android.comms.TrapsterAPIBridge;
import com.trapster.android.comms.nlp.NlpAPI;
import com.trapster.android.comms.rest.IUserAPI;
import com.trapster.android.comms.rest.JSONWriter;
import com.trapster.android.comms.rest.RESTUserAPI;
import com.trapster.android.manager.*;
import com.trapster.android.model.MapTileUpdateResource;
import com.trapster.android.model.dao.*;
import roboguice.inject.ContextSingleton;
import roboguice.inject.SharedPreferencesName;

public class BuildModule extends AbstractModule {

	@Override
	protected void configure() {

		/*
		 * Bind Default Shared Preferences to Defaults.PREF_SETTINGS
		 * 
		 * 
		 * @Inject SharedPreferences sharedPreferences;
		 */
		bindConstant().annotatedWith(SharedPreferencesName.class).to(
				Defaults.PREF_SETTINGS);

        /*
         *
         * Bind the global map event listener, who dispatches touch and map events to a background thread
         *
         */
        bind(GlobalMapEventManager.class).asEagerSingleton();


		/*
		 * Bind MapController as singleton across app.
		 * 
		 * @Inject MapController mapController to access it.
		 */
		bind(MapController.class).asEagerSingleton();

		/*
		 * Bind CommunicationManager as singleton across app.
		 * 
		 * @Inject CommunicationManager communicationManager to access it.
		 */
		bind(CommunicationManager.class).asEagerSingleton();

		/*
		 * Bind SessionManager as singleton across app.
		 * 
		 * @Inject SessionManager sessionManager to access it.
		 */
		bind(SessionManager.class).asEagerSingleton();

        /*
           * Bind UpdatedRouteLinkManager as singleton across app.
           *
           * @Inject UpdatedRouteLinkManager updatedRouteLinkManager to access it.
           */
        bind(UpdatedRouteLinkManager.class).asEagerSingleton();
		
		
		/*
		 * Bind PositionManager as singleton across app. PositionManager handles all the probe data 
		 * and background position logging
		 * 
		 * @Inject PositionManager positionManager to access it.
		 */
		bind(PositionManager.class).asEagerSingleton();
		
		
		/*
		 * Binds the PhpTrapsterAPI to the ITrapsterAPI interface
		 * 
		 * This allows switching between the server APIs
		 */
		bind(ITrapsterAPI.class).to(TrapsterAPIBridge.class);

                        /*
		 * Binds the NlpAPI to the INLPAPI interface
		 *
		 * This allows switching between the server APIs
		 */
        bind(INLPAPI.class).to(NlpAPI.class);


        /**
         * Binds the RESTUserAPI to the IUserAPI model. This provides the facebook and
         * other oauth login information
         */
        bind(IUserAPI.class).to(RESTUserAPI.class);

		/*
		 * DAO objects are tied to the activity. One instance per activity.
		 * State is not maintained across the activities
		 * 
		 * @Inject TrapDAO trapDao to access it.
		 */
		bind(TrapDAO.class).in(ContextSingleton.class);

		/*
		 * DAO objects are tied to the activity. One instance per activity.
		 * State is not maintained across the activities
		 * 
		 * @Inject ImageResourceDAO imageResourceDAO to access it.
		 */
		bind(ImageResourceDAO.class).in(ContextSingleton.class);
		
		/*
		 * DAO objects are tied to the activity. One instance per activity.
		 * State is not maintained across the activities
		 * 
		 * @Inject StringResourceDAO stringResourceDAO to access it.
		 */
		bind(StringResourceDAO.class).in(ContextSingleton.class);

        /*
           * DAO objects are tied to the activity. One instance per activity.
           * State is not maintained across the activities
           *
           * @Inject UpdatedRouteLinkDAO updatedRouteLinkDAO to access it.
           */
        bind(UpdatedRouteLinkDAO.class).in(ContextSingleton.class);

		/*
		 * DAO objects are tied to the activity. One instance per activity.
		 * State is not maintained across the activities
		 * 
		 * @Inject CategoryDAO categoryDAO to access it.
		 */
		bind(CategoryDAO.class).in(ContextSingleton.class);
		
		/*
		 * DAO objects are tied to the activity. One instance per activity.
		 * State is not maintained across the activities
		 * 
		 * Stores the server updates in terms of maptiles
		 * 
		 * @Inject MapTileUpdateResourceDAO mapTileUpdateResourceDAO to access it.
		 */
		bind(MapTileUpdateResource.class).in(ContextSingleton.class);
		

		/*
		 * Bind BitmapCacheManager as singleton across app. It contains an
		 * in-memory/disk cache for bitmap images
		 * 
		 * @Inject BitmapCacheManager bitmapCacheManager to access it.
		 */
		bind(BitmapCacheManager.class).asEagerSingleton();

		bind(TrapMapLayer.class).asEagerSingleton();
		
		bind(PatrolMapLayer.class).asEagerSingleton();

        bind(LinkMatchingMapLayer.class).asEagerSingleton();
		
		//bind(PatrolMapLayer.class).in(ContextSingleton.class);
        bind(FlurryManager.class).asEagerSingleton();

        bind(CompositePositionIndicatorManager.class).asEagerSingleton();
        bind(SoundThemeManager.class).asEagerSingleton();
        bind(TrapUpdateManager.class).asEagerSingleton();

        bind(JSONWriter.class).asEagerSingleton();

		
	}

}
