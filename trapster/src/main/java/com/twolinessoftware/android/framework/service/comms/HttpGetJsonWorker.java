package com.twolinessoftware.android.framework.service.comms;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

import java.io.IOException;
import java.io.InputStream;

/**
 * Class for POST json data to a service
 * 
 * @author John
 *
 */
public class HttpGetJsonWorker extends Worker {

	private static final String LOGNAME = "Framework.HttpGetJsonWorker";

    private String url;

    public HttpGetJsonWorker(String url) {
		super();
		this.url = url;
	}

	public void run() {

		HttpClient conn = null;

           InputStream istream = null;
			
			try {

				HttpGet getMethod = new HttpGet(url);


                getMethod.addHeader("Content-Type",
						"application/json");
                getMethod.setHeader("Accept", "application/json");

                getMethod.setParams(createParamsForPosting());
	
				conn = HttpConnectionManager.getInstance().getClient();

				//Log.d(LOGNAME, "SAUCY - Posting To:" + url );
				
				sendOnStartCommunication();
				
				HttpResponse httpResponse = conn.execute(getMethod);
				HttpEntity responseEntity = httpResponse.getEntity();
				istream = responseEntity.getContent();
				
				// Add the headers
				if (getParser() != null)
					getParser().setHeaders(httpResponse.getAllHeaders());
			
				if (getParser() != null)
					getParser().setResponseCode(httpResponse.getStatusLine().getStatusCode());
				
				//Log.d(LOGNAME, "SAUCY - Response Code:"+httpResponse.getStatusLine().getStatusCode()+":"+httpResponse.getAllHeaders());


                sendOnStopCommunication();

				if (getParser() != null)
					getParser().process(istream);

				
			}catch (Throwable e) {
                {/* 			//Log.e(LOGNAME, "Error Posting:"+e.getMessage()); */}

                sendOnCommuicationError(e.getMessage());
            }finally {
				
				if (istream != null)
                    try {
                        istream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

            }



	}

	

	
	
	private static HttpParams createParamsForPosting() {
		final HttpParams params = new BasicHttpParams();
		HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
		HttpProtocolParams.setContentCharset(params, "UTF-8");
		HttpProtocolParams.setUseExpectContinue(params, false); // solves the
		// '417' issue
		
		return params;
	}
}