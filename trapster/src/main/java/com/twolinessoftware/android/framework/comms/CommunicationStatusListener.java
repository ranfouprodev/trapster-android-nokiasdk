package com.twolinessoftware.android.framework.comms;

public interface CommunicationStatusListener {

	public void onOpenConnection();
	
	public void onCloseConnection(); 
	
	public void onConnectionError(String errorMessage); 
	
	// Not implemented yet. public void onNetworkAvailableChange(boolean available);
	
}
