package com.twolinessoftware.android.framework.service.comms;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

import java.io.InputStream;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 5/28/13
 * Time: 2:35 PM
 * To change this template use File | Settings | File Templates.
 */

public class HttpDeleteJsonWorker extends Worker {

    private static final String LOGNAME = "Framework.HttpDeleteJsonWorker";

    private String url;

    public HttpDeleteJsonWorker(String url) {
        super();
        this.url = url;
    }


    public void run() {

        HttpClient conn = null;

        try {

            InputStream istream = null;

            try {

                HttpDelete deleteMethod = new HttpDelete(url);


                deleteMethod.addHeader("Content-Type","application/json");
                deleteMethod.setHeader("Accept", "application/json");

                deleteMethod.setParams(createParamsForPut());


                conn = HttpConnectionManager.getInstance().getClient();

                //Log.e(LOGNAME, "SAUCY - Puting To:" + url + " value:" + jsonString);

                sendOnStartCommunication();

                HttpResponse httpResponse = conn.execute(deleteMethod);
                HttpEntity responseEntity = httpResponse.getEntity();
                istream = responseEntity.getContent();

                // Add the headers
                if (getParser() != null)
                    getParser().setHeaders(httpResponse.getAllHeaders());

                if (getParser() != null)
                    getParser().setResponseCode(httpResponse.getStatusLine().getStatusCode());

                if (getParser() != null)
                    getParser().process(istream);

                sendOnStopCommunication();

            } finally {

                if (istream != null)
                    istream.close();

            }

        } catch (Exception e) {
            android.util.Log.e(LOGNAME, "Error Posting:"+e.getMessage());
            sendOnCommuicationError(e.getMessage());
        }

    }

    private static HttpParams createParamsForPut() {
        final HttpParams params = new BasicHttpParams();
        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(params, "UTF-8");
        HttpProtocolParams.setUseExpectContinue(params, false); // solves the

        // '417' issue

        return params;
    }

}
