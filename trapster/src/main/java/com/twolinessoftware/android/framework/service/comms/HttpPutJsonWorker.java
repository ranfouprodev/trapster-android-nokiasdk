package com.twolinessoftware.android.framework.service.comms;

import android.util.Log;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 5/28/13
 * Time: 2:35 PM
 * To change this template use File | Settings | File Templates.
 */

public class HttpPutJsonWorker extends Worker {

    private static final String LOGNAME = "Framework.HttpPutJsonWorker";

    private String url;
    private String jsonString;

    public HttpPutJsonWorker(String url, String jsonString) {
        super();
        this.url = url;
        this.jsonString = jsonString;
    }


    public void run() {

        HttpClient conn = null;

        try {

            InputStream istream = null;

            try {

                HttpPut putMethod = new HttpPut(url);


                putMethod.addHeader("Content-Type","application/json");
                putMethod.setHeader("Accept", "application/json");

                putMethod.setParams(createParamsForPut());


                StringEntity se = new StringEntity(jsonString.toString());

                putMethod.setEntity(se);

                conn = HttpConnectionManager.getInstance().getClient();

                //Log.e(LOGNAME, "SAUCY - Puting To:" + url + " value:" + jsonString);

                sendOnStartCommunication();

                HttpResponse httpResponse = conn.execute(putMethod);
                HttpEntity responseEntity = httpResponse.getEntity();
                istream = responseEntity.getContent();

                // Add the headers
                if (getParser() != null)
                    getParser().setHeaders(httpResponse.getAllHeaders());

                if (getParser() != null)
                    getParser().setResponseCode(httpResponse.getStatusLine().getStatusCode());

                //Log.e(LOGNAME, "SAUCY - Response Code:"+httpResponse.getStatusLine().getStatusCode()+":"+httpResponse.getAllHeaders());
                //Log.e(LOGNAME, "SAUCY - Response Stream: " + convertStreamToString(istream));


                if (getParser() != null)
                    getParser().process(istream);

                sendOnStopCommunication();

            } finally {

                if (istream != null)
                    istream.close();

            }

        } catch (Exception e) {
            {/* 			//Log.e(LOGNAME, "Error Posting:"+e.getMessage()); */}

            sendOnCommuicationError(e.getMessage());
        }

    }

    private static HttpParams createParamsForPut() {
        final HttpParams params = new BasicHttpParams();
        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(params, "UTF-8");
        HttpProtocolParams.setUseExpectContinue(params, false); // solves the

        // '417' issue

        return params;
    }

}
