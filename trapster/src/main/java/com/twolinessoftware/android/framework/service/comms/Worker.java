package com.twolinessoftware.android.framework.service.comms;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import android.content.Context;

import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;

public abstract class Worker implements Runnable{
	
	private int id = -1; 
	
	private Context context; 
	private Parser parser;
	private ArrayList<CommunicationStatusListener> listeners = new ArrayList<CommunicationStatusListener>(); 

	
	
	public Parser getParser() {
		return parser;
	}

	public void setParser(Parser parser) {
		this.parser = parser;
	}

	public void addStatusListener(CommunicationStatusListener listener) {
		listeners.add(listener);
	} 
	
	public void sendOnStartCommunication(){
		for(CommunicationStatusListener listener:listeners)
			listener.onOpenConnection();
	}

	public void sendOnStopCommunication(){
		for(CommunicationStatusListener listener:listeners)
			listener.onCloseConnection();
	}
	
	public void sendOnCommuicationError(String errorMessage){
		for(CommunicationStatusListener listener:listeners)
			listener.onConnectionError(errorMessage);
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public Context getContext() {
		return context;
	}

    static String convertStreamToString(InputStream in) throws IOException
    {
        ByteArrayOutputStream out= null;
        try
        {
            byte[] buffer= new byte[1024 * 8];
            out= new ByteArrayOutputStream();
            for (int i= in.read(buffer); i != -1; i= in.read(buffer))
                out.write(buffer, 0, i);
            return new String(out.toByteArray());
        }
        finally
        {
            try
            {
                in.close();
            }
            catch (IOException e)
            {
                throw e;
            }
            if (out != null)
                out.close();
        }
    }
}
