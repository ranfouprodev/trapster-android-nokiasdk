package com.twolinessoftware.android.framework.service.comms;


import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.SerializedName;
import com.trapster.android.RESTDefaults;

import org.json.JSONObject;

import java.io.InputStream;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;



public abstract class JsonParser extends Parser {

	private static final String LOGNAME = "JsonParser";
    private static final String STATUS_ERROR = "ERROR";
    private static final String JSON_SYNTAX_ERROR = "JSON Syntax Error";
    private static final String JSON_IO_ERROR = "JSON IO Error";
    private static final String JSON_REST_ERROR = "JSON HTTP Error";

    public GsonBuilder getBuilder() {
        return builder;
    }

    private final GsonBuilder builder;

    protected Gson gson = null;
    private Status status;

    public class HttpResponseException extends Exception{
        public HttpResponseException(String s) {
            super(s);
        }
    }


	public JsonParser(){
        builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.TRANSIENT);
        //See notes below. Once 2.3 is released this needs to be enabled
        // builder.registerTypeAdapter(boolean.class, new BooleanSerializer());
        gson = builder.create();
    }


    public boolean handleHttpStatusErrors(Status status){
        return false;
    }

	@Override
	public void process(InputStream stream) throws Exception {

        if(getResponseCode() == HttpURLConnection.HTTP_OK || getResponseCode() == HttpURLConnection.HTTP_CREATED
                || getResponseCode() == HttpURLConnection.HTTP_ACCEPTED)
        {
            try
            {


                String jsonString = convertStreamToString(stream);

                Log.v(LOGNAME,"Parsing:"+jsonString);

                process(getResponseCode(),jsonString);


            } catch (Exception e) {
                status = new Status();
                status.message = JSON_SYNTAX_ERROR;
                status.status = STATUS_ERROR;
                throw(e);

            }
        }
        else
        {

            status = new Status();
            status.message = JSON_REST_ERROR;
            status.status = STATUS_ERROR;
            status.statusCode = getResponseCode();

            if(!handleHttpStatusErrors(status))
                throw new HttpResponseException("Response:"+getResponseCode());
        }
    }

    /**
     * Used for preloading resources only.
     *
     *
     * @param responseCode
     * @param jsonString
     * @throws Exception
     */
    public void process(int responseCode, String jsonString) throws Exception{

        setResponseCode(responseCode);

        if(getResponseCode() == HttpURLConnection.HTTP_OK || getResponseCode() == HttpURLConnection.HTTP_CREATED
                || getResponseCode() == HttpURLConnection.HTTP_ACCEPTED)

        {
            try
            {



                JSONObject jsonResponseObject = new JSONObject(jsonString);
                String responseString = jsonResponseObject.get(RESTDefaults.REST_RESPONSE_RESPONSE).toString();
                checkErrors(responseString);

                JSONObject jsonDataObject = new JSONObject(responseString);
                String dataString =  null;
                if(jsonDataObject.has(RESTDefaults.REST_RESPONSE_DATA)){
                    dataString = jsonDataObject.get(RESTDefaults.REST_RESPONSE_DATA).toString();

                    // Convert "Y" and "N" text to true and false
                    dataString = replaceYNText(dataString);
                }

                processInternal(dataString);


            } catch (Exception e) {

                Log.e(LOGNAME,"Error parsing:"+Log.getStackTraceString(e));

                status = new Status();
                status.message = JSON_SYNTAX_ERROR;
                status.status = STATUS_ERROR;
                throw(e);

            }
        }
        else
        {

            status = new Status();
            status.message = JSON_REST_ERROR;
            status.status = STATUS_ERROR;
            status.statusCode = getResponseCode();

            throw new HttpResponseException("Response:"+getResponseCode());
        }

    }


    private void processInternal(String reader)  throws Exception
    {
         parseJson(status, reader);
    }

	public abstract void parseJson(Status status, String jsonString);

	protected boolean checkErrors(String reader) {



        try
        {
            status = gson.fromJson(reader, Status.class);
        }
        catch (JsonSyntaxException jsonSyntaxException)
        {
            status = new Status();
            status.message = JSON_SYNTAX_ERROR;
            status.status = STATUS_ERROR;
            return true;
        }
        catch (JsonIOException jsonIOException)
        {
            status = new Status();
            status.message = JSON_IO_ERROR;
            status.status = STATUS_ERROR;
            return true;
        }

		return false; 
	}

    public boolean hasErrors()
    {
        return status != null;
    }

    public Status getStatus()
    {
        return status;
    }

	public static class Status{
		@SerializedName(RESTDefaults.REST_RESPONSE_EPOCH_TIME)
		public long time;
		
		@SerializedName(RESTDefaults.REST_RESPONSE_STATUS)
		public String status;
		
		@SerializedName(RESTDefaults.REST_RESPONSE_STATUS_CODE)
		public int statusCode;
		
		@SerializedName(RESTDefaults.REST_RESPONSE_ERROR_MESSAGE)
		public String message;
		
	}

    protected String replaceYNText(String input){
        input = input.replace(":\"Y\"", ":true");
        input = input.replace(":\"N\"", ":false");
        return input;
    }


    /**
     * The Serializer will change "Y" and "N" values to true and false booleans. Unfortunately this won't work until 2.3 gson lib is released.
     * For now there is a string search and replace to convert the "Y" to true and "N" to false
     */
    public class BooleanSerializer implements JsonSerializer<Boolean>, JsonDeserializer<Boolean> {

        @Override
        public JsonElement serialize(Boolean arg0, Type arg1, JsonSerializationContext arg2) {
            return new JsonPrimitive(arg0 ? "Y" : "N");
        }

        @Override
        public Boolean deserialize(JsonElement je, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
            // Need to handle true/false and 1:0 conditions

            return je.getAsString().equalsIgnoreCase("Y") ? true : false;
        }
    }

}
