package com.twolinessoftware.android.framework.service.comms;

import java.io.InputStream;

import android.util.Log;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;


public class HttpGetWorker extends Worker {

	private static final String LOGNAME = "Framework.HttpGetWorker";

    private String url; 

    public HttpGetWorker(String url) {
		super();
		this.url = url;
	}
	public void run() {

	
		HttpClient conn = null; 
		
		try {
			
			InputStream istream = null;
			
			try {
				
				HttpGet postMethod = new HttpGet(url);
		
				conn = HttpConnectionManager.getInstance().getClient();

                //Log.e(LOGNAME, "SAUCY - Get To: " + url);
						
				HttpResponse httpResponse = conn.execute(postMethod);
				sendOnStartCommunication();
			
				// Add the headers
				if (getParser() != null)
					getParser().setHeaders(httpResponse.getAllHeaders());
				
				
				HttpEntity responseEntity = httpResponse.getEntity();
				istream = responseEntity.getContent();
	
				sendOnStopCommunication();

                if (getParser() != null)
                    getParser().setHeaders(httpResponse.getAllHeaders());

				if (getParser() != null)
					getParser().setResponseCode(httpResponse.getStatusLine().getStatusCode());

                //Log.e(LOGNAME, "SAUCY - Response Code:"+httpResponse.getStatusLine().getStatusCode()+":"+httpResponse.getAllHeaders());
                //Log.e(LOGNAME, "SAUCY - Response Stream: " + convertStreamToString(istream));
			
				
				if (getParser() != null)
					getParser().process(istream);
						
			} finally { 
				
				if (istream != null)
					istream.close();
				
			}
		} catch (Exception e) {
			
			sendOnCommuicationError(e.getMessage());
	
		}

	}


	
}
