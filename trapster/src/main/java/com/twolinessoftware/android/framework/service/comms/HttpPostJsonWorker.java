package com.twolinessoftware.android.framework.service.comms;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

import java.io.IOException;
import java.io.InputStream;


/**
 * Class for POST json data to a service
 * 
 * @author John
 *
 */
public class HttpPostJsonWorker extends Worker {

	private static final String LOGNAME = "Framework.HttpPostJsonWorker";

    private String url; 
    private String jsonString; 

    public HttpPostJsonWorker(String url, String jsonString) {
		super();
		this.url = url;
		this.jsonString = jsonString;
	}

    
	public void run() {

		HttpClient conn = null; 
		
		   InputStream istream = null;
			
			try {

				HttpPost postMethod = new HttpPost(url);
						
			
				postMethod.addHeader("Content-Type",
						"application/json");
				postMethod.setHeader("Accept", "application/json");
	
				postMethod.setParams(createParamsForPosting());
	
				if(jsonString != null){
                    StringEntity se = new StringEntity(jsonString);

                    postMethod.setEntity(se);
                }
	
				conn = HttpConnectionManager.getInstance().getClient();

				sendOnStartCommunication();
				
				HttpResponse httpResponse = conn.execute(postMethod);
				HttpEntity responseEntity = httpResponse.getEntity();

             //   android.util.Log.v(LOGNAME,"POST Response:"+ EntityUtils.toString(responseEntity));
                istream = responseEntity.getContent();

                // Add the headers
				if (getParser() != null)
					getParser().setHeaders(httpResponse.getAllHeaders());
			
				if (getParser() != null)
					getParser().setResponseCode(httpResponse.getStatusLine().getStatusCode());
				
                sendOnStopCommunication();

				if (getParser() != null)
					getParser().process(istream);

				
			}catch (Exception e) {
                android.util.Log.e(LOGNAME, "Error Posting:"+e.getMessage());

                sendOnCommuicationError(e.getMessage());
            }finally {
				
				if (istream != null)
                    try {
                        istream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

            }



	}

	

	
	
	private static HttpParams createParamsForPosting() {
		final HttpParams params = new BasicHttpParams();
		HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
		HttpProtocolParams.setContentCharset(params, "UTF-8");
		HttpProtocolParams.setUseExpectContinue(params, false); // solves the
		// '417' issue
		
		return params;
	}
}