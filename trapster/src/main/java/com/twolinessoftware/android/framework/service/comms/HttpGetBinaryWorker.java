package com.twolinessoftware.android.framework.service.comms;

import android.content.Context;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

public class HttpGetBinaryWorker extends Worker {
	private static final String LOGNAME = "Framework.HttpGetBinaryWorker";

	private String url;

	private HttpClient conn;

	private String path;

	private Context context; 

    public HttpGetBinaryWorker(String url, Context context, String path) {
		super();
		this.url = url;
		this.path = path; 
		this.context = context; 
	}
	
	public void run() {

		FileOutputStream fos = null;	
			
		try {

            //Log.v(LOGNAME, "Getting from url:" + url);
           // long startTime = System.currentTimeMillis();

			HttpGet postMethod = new HttpGet(url);
			
			conn = HttpConnectionManager.getInstance().getClient();
					
			HttpResponse httpResponse = conn.execute(postMethod);
			sendOnStartCommunication(); 
			
			// Add the headers
			if (getParser() != null)
				getParser().setHeaders(httpResponse.getAllHeaders());
		
			
			HttpEntity responseEntity = httpResponse.getEntity();
			InputStream is = responseEntity.getContent();

			// Write the binary 
			fos = context.openFileOutput(path,Context.MODE_WORLD_READABLE); 
					
			byte[] buffer = new byte[1024];
			int len = 0; 
			 while ( (len = is.read(buffer)) > 0 ) {
		         fos.write(buffer,0, len);
			 }
			
			is.close(); 
			fos.close();
			
			sendOnStopCommunication();
			
			if (getParser() != null)
				getParser().process(new ByteArrayInputStream(path.getBytes()));
				//getParser().parse(path);

            //Log.d(LOGNAME,"Total time to download and store:"+(System.currentTimeMillis() - startTime)+" ms");

		} catch (Exception e) {
			//Log.e(LOGNAME, "Error:"+Log.getStackTrace(e));
			sendOnCommuicationError(e.getMessage());

		}
	}

}
