package com.twolinessoftware.android.framework.service.comms;

import org.apache.http.Header;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.zip.GZIPInputStream;

public abstract class Parser {

    private static final String CONTENT_ENCODING = "Content-Encoding";
    private static final String ENCODING_TYPE_GZIP = "gzip";
	
	private Header[] headers;

	public void setHeaders(Header[] headers) {
		this.headers = headers;
	}

	public Header[] getHeaders() {
		return headers;
	}
	
	private int responseCode; 
	
	public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
  	}
	
	public String convertStreamToString(InputStream in)  {

        try {
            in = decompressIfGzip(in);
        BufferedReader r = new BufferedReader(new InputStreamReader(in,"UTF-8"));
		StringBuilder total = new StringBuilder();
		String line;

			while ((line = r.readLine()) != null) {
			    total.append(line);
			}

            return total.toString();
        } catch (Exception e) {

            return null;
		}

	}

    public static String convertReaderToString(Reader reader)
    {
        char[] arr = new char[1024 * 8];
        StringBuffer stringBuffer = new StringBuffer();
        int numChars;
        try {
            while ((numChars = reader.read(arr, 0, arr.length)) > 0) {
                stringBuffer.append(arr, 0, numChars);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuffer.toString();
    }

    protected InputStream decompressIfGzip(InputStream inputStream)
    {
        for (Header header : headers)
        {
            if (header.getName().equals(CONTENT_ENCODING))
            {
                if (header.getValue().equals(ENCODING_TYPE_GZIP))
                {
                    try
                    {
                        inputStream = new GZIPInputStream(inputStream);
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                    break;
                }
            }
        }

        return inputStream;
    }
		
	public abstract void process(InputStream stream) throws Exception;
	
		
	
	
	
}
