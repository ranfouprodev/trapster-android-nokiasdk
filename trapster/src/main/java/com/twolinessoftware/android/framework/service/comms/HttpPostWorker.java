package com.twolinessoftware.android.framework.service.comms;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;


public class HttpPostWorker extends Worker {

    private static final String LOGNAME = "Framework.HttpPostWorker";

    // Testing
    private static ArrayList<Long> responseTimes = new ArrayList<Long>();
    private static long responseTimeSums = 0;
    private ArrayList<NameValuePair> values;

    private String url;
    private String xmlPost;

    public HttpPostWorker(String url, String xmlPost) {
        super();
        this.url = url;
        this.xmlPost = xmlPost;
        values = new ArrayList<NameValuePair>();
    }

    public HttpPostWorker(String url, ArrayList<NameValuePair> values)
    {
        super();
        this.url = url;
        this.xmlPost = "";
        this.values = values;
    }

    public void run() {

        //OkApacheClient conn = new OkApacheClient();
        HttpClient conn = null;

        InputStream istream = null;

        try {

            HttpPost postMethod = new HttpPost(url);

            postMethod.addHeader("Content-Type",
                    "application/x-www-form-urlencoded");

            postMethod.setParams(createParamsForPosting());

            // set POST body
            values.add(new BasicNameValuePair("request", xmlPost));
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(values,
                    "UTF-8");

            postMethod.setEntity(entity);

            long startTime = System.currentTimeMillis();

            conn = HttpConnectionManager.getInstance().getClient();

            //Log.v(LOGNAME, "Posting To:" + url + " value:" + xmlPost);

            sendOnStartCommunication();

            HttpResponse httpResponse = conn.execute(postMethod);
            HttpEntity responseEntity = httpResponse.getEntity();
            istream = responseEntity.getContent();


            sendOnStopCommunication();

            // Add the headers
            if (getParser() != null)
                getParser().setHeaders(httpResponse.getAllHeaders());

            if (getParser() != null)
                getParser().setResponseCode(httpResponse.getStatusLine().getStatusCode());

            long commsTime = System.currentTimeMillis() - startTime;

            if (getParser() != null)
                getParser().process(istream);

            long deltaTime = System.currentTimeMillis() - startTime;


            //Log.d(LOGNAME, "Total time "+deltaTime+"ms  Comms Time:"+commsTime+"("+url+")");

            responseTimes.add(new Long(deltaTime));

            Collections.sort(responseTimes);

            responseTimeSums += deltaTime;

            //Log.d(LOGNAME,"Average:"+(responseTimeSums/responseTimes.size())+" ms Median:"+responseTimes.get((int)(responseTimes.size()/2))+" ms");
            //Log.d(LOGNAME,"Times["+responseTimes+"]");

        } catch (Exception e) {


            sendOnCommuicationError(e.getMessage());
        } finally {

            if (istream != null)
                try {
                    istream.close();
                } catch (IOException e) {
                   Log.e(LOGNAME,"nested exception of DOOM");
                }

        }

    }


    private static HttpParams createParamsForPosting() {
        final HttpParams params = new BasicHttpParams();
        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(params, "UTF-8");
        HttpProtocolParams.setUseExpectContinue(params, false); // solves the
        // '417' issue

        return params;
    }

}