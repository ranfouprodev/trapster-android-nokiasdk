package com.trapster.android.test;

import com.trapster.android.comms.PatrolListener;
import com.trapster.android.model.PatrolPath;
import com.trapster.android.model.PatrolPaths;
import com.trapster.android.model.PatrolPoint;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;
import com.xtremelabs.robolectric.Robolectric;

import org.junit.Test;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;

/**
 * Created by John on 5/28/13.
 */

public class PatrolParserTest extends JsonParserTestBase {

    private static final String LOGNAME = "PatrolParserTest";

    // From https://probe.api.trapster.com/2/patrol.json?deviceid=351746051938853&opsys=4.2.2&devicetype=Galaxy+Nexus&pwdhash=041f52e0ee51136d260cf695daf14305&login=jcarpenter&appcapabilities=3&appkey=b9f50e2b4da4e74a7fbb7e4ec9c9d1d7&appid=TPST-AND-EN-4.1.2.0&latitude=40.727096&longitude=-73.983221&radius=5.0
    //
    private static final String PATROL_JSON = "{\"request\":null,\"response\":{\"message\":null,\"data\":{\"id\":null,\"string\":[],\"info\":null,\"displayname\":null,\"color\":[],\"patrollines\":[{\"patrolpoints\":[{\"longitude\":-73.975249,\"latitude\":40.755741,\"color\":\"00AFF000\"},{\"longitude\":-73.975247,\"latitude\":40.755783,\"color\":\"00AFF000\"}]}],\"alert\":null,\"karma\":null,\"lastkarma\":null,\"loginfailures\":null,\"confirmmethod\":null,\"marker\":[],\"added\":[],\"removed\":[],\"emailaddr\":null,\"pwdusernamehash\":null,\"mobilepwdhash\":null,\"usercredlevel\":null,\"alertradius\":null,\"signupepochtime\":null,\"lastvoteepochtime\":null,\"totaluservotes\":null,\"tosagreed\":null,\"trustedgrouppref\":null,\"privmsgpref\":null,\"smsaddr\":null,\"moderatorlevel\":null,\"loggedontositeflag\":null,\"newsletterflag\":null,\"emailconfflag\":null,\"userstatus\":null,\"sipallowedflag\":null,\"modapplyepochtime\":null,\"lastapiepochtime\":null,\"lastwebepochtime\":null,\"confirmepochtime\":null,\"facebook\":null,\"twitter\":null,\"twitterenabled\":null,\"facebookenabled\":null,\"tripSessionId\":null,\"image\":[],\"trapcategory\":[],\"traptype\":[],\"addedtheme\":[],\"removedtheme\":[],\"addedpoi\":[],\"removedpoi\":[],\"poiTopics\":[]},\"status\":null,\"asofepochtime\":1374774795,\"statuscode\":200}}";

    // Used to check if the parsing was done successfully
    private boolean passed = false;

    @Test
    public void shouldFailOnInvalidHttpResponseCode() throws Exception {
        System.out.println("shouldFailOnInvalidHttpResponseCode");

        passed = false;

        Robolectric.addPendingHttpResponse(405, "Invalid JSON");


        api.getPath(0,0,0, new PatrolListener() {

                    @Override
                    public void onPatrolPath(PatrolPaths patrolPaths, double lat, double lon, double radius) {
                        fail("JSON parsing passed magically");
                        lock.countDown();
                    }

                    @Override
                    public void onError(TrapsterError error) {
                        System.out.println("System Error Thrown:" + error.getDetails());
                        passed = true;
                        lock.countDown();
                    }
                }, new CommunicationStatusListener() {

                    @Override
                    public void onOpenConnection() {

                    }

                    @Override
                    public void onCloseConnection() {

                    }

                    @Override
                    public void onConnectionError(String errorMessage) {
                        System.out.println("JSON Error Thrown:" + errorMessage);
                        passed = true;
                        lock.countDown();
                    }
                }
        );

        lock.await(5, TimeUnit.SECONDS);

        assertTrue(passed);

    }


    @Test
    public void shouldFailOnInvalidJson() throws Exception {
        System.out.println("shouldFailOnInvalidJson");

        passed = false;

        Robolectric.addPendingHttpResponse(200, "Invalid JSON");

        api.getPath(0, 0, 0, new PatrolListener() {

                    @Override
                    public void onPatrolPath(PatrolPaths patrolPaths, double lat, double lon, double radius) {
                        fail("JSON parsing passed magically");
                        lock.countDown();
                    }

                    @Override
                    public void onError(TrapsterError error) {
                        passed = true;
                        lock.countDown();
                    }
                }, new CommunicationStatusListener() {

                    @Override
                    public void onOpenConnection() {

                    }

                    @Override
                    public void onCloseConnection() {

                    }

                    @Override
                    public void onConnectionError(String errorMessage) {
                        System.out.println("JSON Error Thrown:" + errorMessage);
                        passed = true;
                        lock.countDown();
                    }
                }
        );

        lock.await(5, TimeUnit.SECONDS);

        assertTrue(passed);

    }


    @Test
    public void shouldParseResponseWithStatus() throws Exception {
        System.out.println("Running Test shouldParseResponseWithStatus");

        passed = false;

        Robolectric.addPendingHttpResponse(200, PATROL_JSON);

        api.getPath(1, 2, 3, new PatrolListener() {

            @Override
            public void onPatrolPath(PatrolPaths patrolPaths, double lat, double lon, double radius) {

                assertNotNull(patrolPaths);

                assertEquals(1.0, lat);
                assertEquals(2.0, lon);
                assertEquals(3.0, radius);

                ArrayList<PatrolPath> paths = patrolPaths.getPaths();
                assertNotNull(paths);
                assertEquals(1,paths.size());

                PatrolPath path = paths.get(0);
                assertNotNull(path);

                assertEquals(2,path.getPoints().size());

                PatrolPoint point = path.getPoints().get(0);
                assertNotNull(point);
                assertEquals(-73.975249,point.getLon());
                assertEquals(40.755741,point.getLat());
                assertEquals("00AFF000",point.getColor());



                passed = true;
                lock.countDown();
            }


            @Override
            public void onError(TrapsterError error) {
                fail("Trapster Error:" + error.getDetails());
                lock.countDown();
            }
        }, new TestCommunicationListener());

        lock.await(5, TimeUnit.SECONDS);

        assertTrue(passed);

    }


}
