package com.trapster.android.test;

import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.util.Modules;
import com.trapster.android.Defaults;
import com.trapster.android.activity.StartScreen;
import com.trapster.android.comms.CommunicationManager;
import com.trapster.android.comms.ITrapsterAPI;
import com.trapster.android.comms.TrapsterAPIBridge;
import com.trapster.android.manager.BitmapCacheManager;
import com.trapster.android.manager.SessionManager;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;
import com.xtremelabs.robolectric.Robolectric;
import com.xtremelabs.robolectric.shadows.ShadowLog;

import org.junit.Before;
import org.junit.runner.RunWith;

import java.util.concurrent.CountDownLatch;

import roboguice.RoboGuice;
import roboguice.inject.SharedPreferencesName;

import static com.xtremelabs.robolectric.Robolectric.application;

/**
 * Created by John on 6/21/13.
 */
@RunWith(TrapsterTestRunner.class)
public class JsonParserTestBase {

    @Inject
    protected ITrapsterAPI api;

    @Inject
    protected SessionManager sessionManager;

    protected StartScreen activity;

    /** Countdown latch */
    protected CountDownLatch lock = new CountDownLatch(1);

    @Before
    public void setUp() throws Exception {
        // Force the logs to System
        ShadowLog.stream = System.out;
        Robolectric.bindShadowClass(ShadowLog.class);

        // Override the default RoboGuice module
        RoboGuice.setBaseApplicationInjector(application, RoboGuice.DEFAULT_STAGE, Modules.override(RoboGuice.newDefaultRoboModule(application)).with(new CommsOnlyTestModule()));

       // activity = new StartScreen();
        //activity.onCreate(null);

        RoboGuice.injectMembers(application,this);

    }


    protected class TestCommunicationListener implements CommunicationStatusListener {

        long startTime = 0;
        @Override
        public void onOpenConnection() {
           System.out.println("Opened Connection");
            startTime = System.currentTimeMillis();
        }

        @Override
        public void onCloseConnection() {
           long time = System.currentTimeMillis() - startTime;
            System.out.println("Closed connection: Time: " + time + " ms");
        }

        @Override
        public void onConnectionError(String errorMessage) {
            System.out.println("Connection Error: "+errorMessage);
            lock.countDown();
        }
    }

    protected class CommsOnlyTestModule extends AbstractModule {
        @Override
        protected void configure() {
            bindConstant().annotatedWith(SharedPreferencesName.class).to(
                    Defaults.PREF_SETTINGS);

            bind(CommunicationManager.class).asEagerSingleton();

            /*
             * Binds the PhpTrapsterAPI to the ITrapsterAPI interface
             *
             * This allows switching between the server APIs. This will only pass if the json parser is set
             */
            bind(ITrapsterAPI.class).to(TrapsterAPIBridge.class);

            bind(BitmapCacheManager.class).asEagerSingleton();
        }
    }

}
