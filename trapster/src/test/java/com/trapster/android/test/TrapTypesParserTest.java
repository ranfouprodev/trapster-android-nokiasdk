package com.trapster.android.test;

import com.trapster.android.comms.TrapTypeListener;
import com.trapster.android.model.TrapIcon;
import com.trapster.android.model.TrapLevel;
import com.trapster.android.model.TrapType;
import com.trapster.android.model.TrapTypes;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;
import com.xtremelabs.robolectric.Robolectric;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;

/**
 * Created by John on 5/28/13.
 */

public class TrapTypesParserTest extends JsonParserTestBase{

    private static final String LOGNAME = "TrapTypesParserTest";

    // From http://attributes.trap-scourge.com/2/attributes/traptype.json?deviceid=355762051548184&opsys=4.1.2&devicetype=-C6603_1270-7689-11&pwdhash=a89dc2825c7fee3acd022d73b539350d&login=demo&appcapabilities=3&appkey=b9f50e2b4da4e74a7fbb7e4ec9c9d1d7&appid=TPST-AND-EN-4.0.1.1&modifiedaftertime=0
    //
    private static final String TRAPTYPES_JSON =  "{\"request\":null,\"response\":{\"message\":null,\"data\":{\"id\":null,\"string\":[],\"info\":null,\"displayname\":null,\"color\":[],\"image\":[],\"addedtheme\":[],\"addedpoi\":[],\"removedpoi\":[],\"poiTopics\":[],\"removed\":[],\"removedtheme\":[],\"trapcategory\":[],\"traptype\":[{\"name\":\"Live Police\",\"description\":\"Police speed trap currently observing traffic.\",\"category\":3,\"audioid\":\"lp\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"Y\",\"icons\":{\"alt2\":\"lp_red\",\"alt1\":\"lp_yellow\",\"base\":\"lp_green\"},\"lifetime\":3600,\"levels\":{\"level\":[{\"icon\":\"lp_red\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"lp_red\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"lp_green\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"lp_yellow\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"lp_red\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"0\"},{\"name\":\"Red Light Camera\",\"description\":\"Red light camera. Be careful of surveillance cameras that look similar!\",\"category\":1,\"audioid\":\"rlc\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"N\",\"icons\":{\"alt2\":\"rlc_red\",\"alt1\":\"rlc_yellow\",\"base\":\"rlc_green\"},\"lifetime\":0,\"levels\":{\"level\":[{\"icon\":\"rlc_red\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"rlc_red\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"rlc_green\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"rlc_yellow\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"rlc_red\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"1\"},{\"name\":\"Fixed Speed Camera\",\"description\":\"Speed camera. Typically roadside on highways/speedways.\",\"category\":1,\"audioid\":\"sc\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"N\",\"icons\":{\"alt2\":\"sc_red\",\"alt1\":\"sc_yellow\",\"base\":\"sc_green\"},\"lifetime\":0,\"levels\":{\"level\":[{\"icon\":\"sc_red\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"sc_red\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"sc_green\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"sc_yellow\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"sc_red\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"2\"},{\"name\":\"Known Enforcement Point\",\"description\":\"Where police are often seen enforcing traffic with laser or radar.\",\"category\":1,\"audioid\":\"hp\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"N\",\"notifiable\":\"N\",\"icons\":{\"alt2\":\"pohh_red\",\"alt1\":\"pohh_yellow\",\"base\":\"pohh_green\"},\"lifetime\":1814400,\"levels\":{\"level\":[{\"icon\":\"pohh_red\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"pohh_red\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"pohh_green\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"pohh_yellow\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"pohh_red\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"3\"},{\"name\":\"Combo Camera\",\"description\":\"A combined red light and speed photo enforcement camera.\",\"category\":1,\"audioid\":\"cb\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"N\",\"icons\":{\"alt2\":\"cb_red\",\"alt1\":\"cb_yellow\",\"base\":\"cb_green\"},\"lifetime\":0,\"levels\":{\"level\":[{\"icon\":\"cb_red\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"cb_red\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"cb_green\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"cb_yellow\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"cb_red\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"4\"},{\"name\":\"Mobile Speed Camera\",\"description\":\"A speed camera inside of a vehicle enforcing passing traffic.\",\"category\":3,\"audioid\":\"msc\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"Y\",\"icons\":{\"alt2\":\"msc_red\",\"alt1\":\"msc_yellow\",\"base\":\"msc_green\"},\"lifetime\":7200,\"levels\":{\"level\":[{\"icon\":\"msc_red\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"msc_red\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"msc_green\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"msc_yellow\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"msc_red\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"5\"},{\"name\":\"Children at Play\",\"description\":\"Children at Play!\",\"category\":2,\"audioid\":\"childrenatplay\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"N\",\"icons\":{\"alt2\":\"childrenatplay\",\"alt1\":\"childrenatplay\",\"base\":\"childrenatplay\"},\"lifetime\":7200,\"levels\":{\"level\":[{\"icon\":\"childrenatplay\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"childrenatplay\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"childrenatplay\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"childrenatplay\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"childrenatplay\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"20\"},{\"name\":\"Accident\",\"description\":\"Accident or other roadway incident.\",\"category\":3,\"audioid\":\"accident\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"Y\",\"icons\":{\"alt2\":\"accident\",\"alt1\":\"accident\",\"base\":\"accident\"},\"lifetime\":7200,\"levels\":{\"level\":[{\"icon\":\"accident\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"accident\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"accident\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"accident\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"accident\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"21\"},{\"name\":\"Brush Fire\",\"description\":\"Brush Fire.\",\"category\":2,\"audioid\":\"brushfire\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"Y\",\"icons\":{\"alt2\":\"brushfire\",\"alt1\":\"brushfire\",\"base\":\"brushfire\"},\"lifetime\":21600,\"levels\":{\"level\":[{\"icon\":\"brushfire\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"brushfire\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"brushfire\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"brushfire\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"brushfire\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"22\"},{\"name\":\"Dangerous Intersection\",\"description\":\"Dangerous intersection ahead.\",\"category\":2,\"audioid\":\"dangerintersect\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"N\",\"icons\":{\"alt2\":\"dangerintersect\",\"alt1\":\"dangerintersect\",\"base\":\"dangerintersect\"},\"lifetime\":0,\"levels\":{\"level\":[{\"icon\":\"dangerintersect\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"dangerintersect\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"dangerintersect\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"dangerintersect\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"dangerintersect\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"23\"},{\"name\":\"Dangerous Curve\",\"description\":\"Dangerous curve ahead.\",\"category\":2,\"audioid\":\"dangerouscurve\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"N\",\"icons\":{\"alt2\":\"dangerouscurve\",\"alt1\":\"dangerouscurve\",\"base\":\"dangerouscurve\"},\"lifetime\":0,\"levels\":{\"level\":[{\"icon\":\"dangerouscurve\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"dangerouscurve\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"dangerouscurve\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"dangerouscurve\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"dangerouscurve\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"24\"},{\"name\":\"Flooded Road\",\"description\":\"Flooded road ahead.\",\"category\":2,\"audioid\":\"flood\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"Y\",\"icons\":{\"alt2\":\"flood\",\"alt1\":\"flood\",\"base\":\"flood\"},\"lifetime\":21600,\"levels\":{\"level\":[{\"icon\":\"flood\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"flood\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"flood\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"flood\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"flood\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"25\"},{\"name\":\"Ice On Road\",\"description\":\"Ice on road ahead.\",\"category\":2,\"audioid\":\"ice\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"Y\",\"icons\":{\"alt2\":\"ice\",\"alt1\":\"ice\",\"base\":\"ice\"},\"lifetime\":14400,\"levels\":{\"level\":[{\"icon\":\"ice\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"ice\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"ice\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"ice\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"ice\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"26\"},{\"name\":\"Narrow Bridge Ahead\",\"description\":\"Narrow Bridge Ahead.\",\"category\":2,\"audioid\":\"narrowbridge\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"N\",\"icons\":{\"alt2\":\"narrowbridge\",\"alt1\":\"narrowbridge\",\"base\":\"narrowbridge\"},\"lifetime\":0,\"levels\":{\"level\":[{\"icon\":\"narrowbridge\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"narrowbridge\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"narrowbridge\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"narrowbridge\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"narrowbridge\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"27\"},{\"name\":\"Road Closed Ahead\",\"description\":\"Road closed ahead!\",\"category\":2,\"audioid\":\"roadclosed\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"Y\",\"icons\":{\"alt2\":\"roadclosed\",\"alt1\":\"roadclosed\",\"base\":\"roadclosed\"},\"lifetime\":21600,\"levels\":{\"level\":[{\"icon\":\"roadclosed\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"roadclosed\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"roadclosed\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"roadclosed\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"roadclosed\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"28\"},{\"name\":\"Road Kill\",\"description\":\"Dead animal on road ahead.\",\"category\":2,\"audioid\":\"roadkill\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"Y\",\"icons\":{\"alt2\":\"roadkill\",\"alt1\":\"roadkill\",\"base\":\"roadkill\"},\"lifetime\":21600,\"levels\":{\"level\":[{\"icon\":\"roadkill\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"roadkill\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"roadkill\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"roadkill\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"roadkill\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"29\"},{\"name\":\"Toll Booth\",\"description\":\"Toll booth or toll station requiring payment.\",\"category\":2,\"audioid\":\"tollbooth\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"N\",\"icons\":{\"alt2\":\"tollbooth\",\"alt1\":\"tollbooth\",\"base\":\"tollbooth\"},\"lifetime\":0,\"levels\":{\"level\":[{\"icon\":\"tollbooth\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"tollbooth\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"tollbooth\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"tollbooth\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"tollbooth\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"30\"},{\"name\":\"Construction Zone\",\"description\":\"Construction Zone.\",\"category\":3,\"audioid\":\"construction\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"Y\",\"icons\":{\"alt2\":\"construction\",\"alt1\":\"construction\",\"base\":\"construction\"},\"lifetime\":86400,\"levels\":{\"level\":[{\"icon\":\"construction\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"construction\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"construction\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"construction\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"construction\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"31\"},{\"name\":\"MSC Often Seen Here\",\"description\":\"Known location where mobile speed cameras are often seen.\",\"category\":1,\"audioid\":\"mscosh\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"N\",\"icons\":{\"alt2\":\"mscosh_red\",\"alt1\":\"mscosh_yellow\",\"base\":\"mscosh_green\"},\"lifetime\":1814400,\"levels\":{\"level\":[{\"icon\":\"mscosh_red\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"mscosh_red\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"mscosh_green\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"mscosh_yellow\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"mscosh_red\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"7\"},{\"name\":\"School Zone\",\"description\":\"School and/or campus where children are present.\",\"category\":2,\"audioid\":\"schoolzone\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"N\",\"icons\":{\"alt2\":\"schoolzone\",\"alt1\":\"schoolzone\",\"base\":\"schoolzone\"},\"lifetime\":0,\"levels\":{\"level\":[{\"icon\":\"schoolzone\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"schoolzone\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"schoolzone\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"schoolzone\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"schoolzone\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"32\"},{\"name\":\"Road Hazard\",\"description\":\"Objects or other obstructions on the road.\",\"category\":3,\"audioid\":\"roadhazard\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"Y\",\"icons\":{\"alt2\":\"roadhazard\",\"alt1\":\"roadhazard\",\"base\":\"roadhazard\"},\"lifetime\":3600,\"levels\":{\"level\":[{\"icon\":\"roadhazard\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"roadhazard\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"roadhazard\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"roadhazard\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"roadhazard\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"33\"},{\"name\":\"EV Charging Station\",\"description\":\"Electric Vehicle Charging Station.\",\"category\":5,\"audioid\":\"\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"N\",\"notifiable\":\"N\",\"icons\":{\"alt2\":\"ev_charging_station\",\"alt1\":\"ev_charging_station\",\"base\":\"ev_charging_station\"},\"lifetime\":0,\"levels\":{\"level\":[{\"icon\":\"ev_charging_station\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"ev_charging_station\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"ev_charging_station\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"ev_charging_station\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"ev_charging_station\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"40\"},{\"name\":\"Traffic Jam\",\"description\":\"Vehicles are moving slowly or stopped.\",\"category\":3,\"audioid\":\"trafficjam\",\"reportable\":\"Y\",\"alertable\":\"Y\",\"votable\":\"Y\",\"announces\":\"Y\",\"notifiable\":\"Y\",\"icons\":{\"alt2\":\"traffic_jam\",\"alt1\":\"traffic_jam\",\"base\":\"traffic_jam\"},\"lifetime\":3600,\"levels\":{\"level\":[{\"icon\":\"traffic_jam\",\"bit\":4,\"levelname\":\"Validated\"},{\"icon\":\"traffic_jam\",\"bit\":5,\"levelname\":\"Law enforcement\"},{\"icon\":\"traffic_jam\",\"bit\":1,\"levelname\":\"Low confidence\"},{\"icon\":\"traffic_jam\",\"bit\":2,\"levelname\":\"Medium confidence\"},{\"icon\":\"traffic_jam\",\"bit\":3,\"levelname\":\"High confidence\"}]},\"id\":\"34\"}],\"alert\":null,\"marker\":[],\"added\":[],\"emailaddr\":null,\"pwdusernamehash\":null,\"mobilepwdhash\":null,\"usercredlevel\":null,\"alertradius\":null,\"signupepochtime\":null,\"lastvoteepochtime\":null,\"totaluservotes\":null,\"karma\":null,\"tosagreed\":null,\"trustedgrouppref\":null,\"privmsgpref\":null,\"smsaddr\":null,\"moderatorlevel\":null,\"loggedontositeflag\":null,\"newsletterflag\":null,\"emailconfflag\":null,\"userstatus\":null,\"lastkarma\":null,\"sipallowedflag\":null,\"loginfailures\":null,\"modapplyepochtime\":null,\"lastapiepochtime\":null,\"lastwebepochtime\":null,\"confirmepochtime\":null,\"confirmmethod\":null,\"facebook\":null,\"twitter\":null,\"twitterenabled\":null,\"facebookenabled\":null,\"tripSessionId\":null,\"patrollines\":[]},\"status\":\"OK\",\"asofepochtime\":1371764208,\"statuscode\":200}}";

    // Used to check if the parsing was done successfully
    private boolean passed = false;

    @Test
    public void shouldFailOnInvalidHttpResponseCode() throws Exception {
        System.out.println("shouldFailOnInvalidHttpResponseCode");

        passed = false;

        Robolectric.addPendingHttpResponse(405,"Invalid JSON");

        api.getTrapTypes(0,new TrapTypeListener() {
                    @Override
                    public void onTrapTypes(TrapTypes types) {
                        fail("JSON parsing passed magically");
                        lock.countDown();
                    }

                    @Override
                    public void onError(TrapsterError error) {
                        System.out.println("System Error Thrown:"+error.getDetails());
                        passed = true;
                        lock.countDown();
                    }
                },new CommunicationStatusListener(){

                    @Override
                    public void onOpenConnection() {

                    }

                    @Override
                    public void onCloseConnection() {

                    }

                    @Override
                    public void onConnectionError(String errorMessage) {
                        System.out.println("JSON Error Thrown:"+errorMessage);
                        passed = true;
                        lock.countDown();
                    }
                });

        lock.await(30, TimeUnit.SECONDS);

        assertTrue(passed);

    }


    @Test
    public void shouldFailOnInvalidJson() throws Exception {
        System.out.println("shouldFailOnInvalidJson");

        passed = false;

        Robolectric.addPendingHttpResponse(200,"Invalid JSON");

        api.getTrapTypes(0,new TrapTypeListener() {
            @Override
            public void onTrapTypes(TrapTypes types) {
                fail("JSON parsing passed magically");
                lock.countDown();
            }

            @Override
            public void onError(TrapsterError error) {
                passed = true;
                lock.countDown();
            }
        },new CommunicationStatusListener(){

                    @Override
                    public void onOpenConnection() {

                    }

                    @Override
                    public void onCloseConnection() {

                    }

                    @Override
                    public void onConnectionError(String errorMessage) {
                        System.out.println("JSON Error Thrown:"+errorMessage);
                        passed = true;
                        lock.countDown();
                    }
                });

        lock.await(30, TimeUnit.SECONDS);

        assertTrue(passed);

    }


        @Test
    public void shouldParseResponseWithStatus() throws Exception {
        System.out.println("Running Test shouldParseResponseWithStatus");

        passed = false;

        Robolectric.addPendingHttpResponse(200,TRAPTYPES_JSON);

        api.getTrapTypes(0,new TrapTypeListener() {
            @Override
            public void onTrapTypes(TrapTypes types) {
                assertNotNull(types);
                System.out.println("Parsed TrapTypes:" + types.getTypes().size());

                assertEquals(23, types.getTypes().size());

                TrapType lpType = types.getTypes().get(0); // Live Police is tested

                assertEquals("Live Police",lpType.getName());
                assertEquals("Police speed trap currently observing traffic.",lpType.getDescription());
                assertEquals(3,lpType.getCategoryId());
                assertEquals("lp",lpType.getAudioid());
                assertTrue(lpType.isReportable());
                assertTrue(lpType.isVotable());
                assertTrue(lpType.isNotifiable());
                assertTrue(lpType.isAnnounces());
                assertEquals(3600,lpType.getLifetime());
                assertEquals(0,lpType.getId());

                // Icons
                TrapIcon icon = lpType.getIcon();
                assertNotNull(icon);

                assertEquals("lp_red",icon.getAlt2());
                assertEquals("lp_yellow",icon.getAlt1());
                assertEquals("lp_green",icon.getBase());

                // Levels
                assertNotNull(lpType.getLevels());
                assertEquals(5, lpType.getLevels().size());

                TrapLevel level = lpType.getLevels().get(0);

                assertEquals("lp_red",level.getIcon());
                assertEquals(4,level.getBit());
                assertEquals("Validated",level.getLevelName());


                passed = true;

                lock.countDown();
            }

            @Override
            public void onError(TrapsterError error) {
                fail("Trapster Error:" + error.getDetails());
                lock.countDown();
            }
        },new TestCommunicationListener());

        lock.await(5, TimeUnit.SECONDS);

        assertTrue(passed);

    }


}
