package com.trapster.android.test;

import android.app.Application;

import com.trapster.android.TrapsterApplication;
import com.xtremelabs.robolectric.Robolectric;
import com.xtremelabs.robolectric.RobolectricTestRunner;
import com.xtremelabs.robolectric.res.RobolectricPackageManager;
import com.xtremelabs.robolectric.shadows.ShadowContextWrapper;

import org.junit.runners.model.InitializationError;

/**
 * Created by John on 5/29/13.
 */
public class TrapsterTestRunner extends RobolectricTestRunner{


    private Object lock = new Object();

    public TrapsterTestRunner(Class<?> testClass) throws InitializationError {
        super(testClass);
    }

    @Override
    protected Application createApplication() {

        TrapsterApplication app = new TrapsterApplication();
        ShadowContextWrapper shadowApp = Robolectric.shadowOf(app);
        shadowApp.setPackageName( "com.trapster.android" );
        shadowApp.setPackageManager(new RobolectricPackageManager(app, robolectricConfig));
        app.onCreate();

        return app;
    }
}
