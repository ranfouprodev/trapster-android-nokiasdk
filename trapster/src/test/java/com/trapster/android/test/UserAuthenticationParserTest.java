package com.trapster.android.test;

import com.trapster.android.comms.LoginListener;
import com.trapster.android.comms.rest.StatusCode;
import com.trapster.android.model.Profile;
import com.trapster.android.model.User;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;
import com.xtremelabs.robolectric.Robolectric;

import org.junit.Test;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 6/24/13
 * Time: 10:38 AM
 * To change this template use File | Settings | File Templates.
 */
public class UserAuthenticationParserTest extends JsonParserTestBase {

    private static final String USER_AUTH_JSON =  "{\"request\":null,\"response\":{\"message\":null,\"data\":{\"id\":\"103726636\",\"moderatorlevel\":\"0\",\"totaluservotes\":\"38\",\"karma\":\"45\",\"signupepochtime\":\"1333027455\",\"displayname\":\"savons1\",\"emailaddr\":\"sam.nazi@navteq.com\",\"status\":\"OK\",\"asofepochtime\":1371764208,\"statuscode\":200}}";
  //  Posting to:http://user.trap-scourge.com/2/user/qwerty
  //          07-17 10:51:39.717    2162-2162/com.trapster.android V/RESTTrapsterAPI: Params:{"request":{"auth":{"appcapabilities":"3","appid":"TPST-AND-EN-4.0.1.1","appkey":"b9f50e2b4da4e74a7fbb7e4ec9c9d1d7","deviceid":"351746050721086","devicetype":"Roam Mobility@-yakju-01","pwdhash":"a89dc2825c7fee3acd022d73b539350d","login":"demo"},"meta":{"opsys":"4.2.2"},"params":{"tosagreed":"Y","pwdcnf":"qwerty","emailaddr":"test@2linessoftware.com","newsletterflag":"N","pwd":"qwerty","smsaddr":"7736918402","displayname":"qwerty"}}}
    // Used to check if the parsing was done successfully

    private static final String REQUIRES_CONF_CODE =  "{ \"request\": null, \"response\": { \"message\": \"Incorrect confirmation code.\", \"data\": null, \"status\": \"ERROR\", \"statuscode\": 801, \"asofepochtime\": 1374162662 } }";
    private static final String INVALID_CREDENTIALS =  "{ \"request\": null, \"response\": { \"message\": \"User Credentials are invalid\", \"data\": null, \"status\": \"ERROR\", \"statuscode\": 403, \"asofepochtime\": 1374169002 } }";



    private boolean passed = false;


    @Test
    public void shouldFailOnRequiresConfCode() throws Exception {
        System.out.println("shouldFailOnRequiresConfCode");

        passed = false;

        Robolectric.addPendingHttpResponse(200, REQUIRES_CONF_CODE);

        api.login(new User("savons1", "123qwe"),new LoginListener() {

                    @Override
                    public void onLoginSuccess(User user,String optionalMessage) {
                        fail("JSON parsing passed magically");
                        lock.countDown();
                    }

                    @Override
                    public void onLoginFail(User user, StatusCode code) {
                        fail("JSON parsing passed magically");
                        lock.countDown();
                    }

                    @Override
                    public void onError(TrapsterError error) {

                        assertEquals(801, error.getType());
                        passed = true;
                        lock.countDown();
                    }
                },new CommunicationStatusListener(){

                    @Override
                    public void onOpenConnection() {

                    }

                    @Override
                    public void onCloseConnection() {

                    }

                    @Override
                    public void onConnectionError(String errorMessage) {
                        System.out.println("JSON Error Thrown: "+errorMessage);
                        passed = true;
                        lock.countDown();
                    }
                });

        lock.await(30, TimeUnit.SECONDS);

        assertTrue(passed);
    }

    @Test
    public void shouldFailOnInvalidJson() throws Exception {
        System.out.println("shouldFailOnInvalidJson");

        passed = false;

        Robolectric.addPendingHttpResponse(200, "Invalid JSON");

        api.login(new User("savons1", "123qwe"),new LoginListener() {

                    @Override
                    public void onLoginSuccess(User user,String optionalMessage) {
                        fail("JSON parsing passed magically");
                        lock.countDown();
                    }

                    @Override
                    public void onLoginFail(User user, StatusCode code) {
                        fail("JSON parsing passed magically");
                        lock.countDown();
                    }

                    @Override
                    public void onError(TrapsterError error) {
                        passed = true;
                        lock.countDown();
                    }
                },new CommunicationStatusListener(){

                    @Override
                    public void onOpenConnection() {

                    }

                    @Override
                    public void onCloseConnection() {

                    }

                    @Override
                    public void onConnectionError(String errorMessage) {
                        System.out.println("JSON Error Thrown: "+errorMessage);
                        passed = true;
                        lock.countDown();
                    }
                });

        lock.await(30, TimeUnit.SECONDS);

        assertTrue(passed);
    }


    @Test
    public void shouldFailOnInvalidCredentials() throws Exception {
        System.out.println("shouldFailOnInvalidCredentials");

        passed = false;

        Robolectric.addPendingHttpResponse(200, INVALID_CREDENTIALS);

        api.login(new User("savons1", "123qwe"),new LoginListener() {

                    @Override
                    public void onLoginSuccess(User user, String optionalMessage) {
                        fail("JSON parsing passed magically");
                        lock.countDown();
                    }

                    @Override
                    public void onLoginFail(User user, StatusCode code) {
                        if(code == StatusCode.INVALID_USER_CREDENTIALS){
                            passed = true;
                        }else
                            fail("JSON parsing passed magically");
                        lock.countDown();
                    }

                    @Override
                    public void onError(TrapsterError error) {
                        assertEquals(403,error.getType());
                        passed = true;
                        lock.countDown();
                    }
                },new CommunicationStatusListener(){

                    @Override
                    public void onOpenConnection() {

                    }

                    @Override
                    public void onCloseConnection() {

                    }

                    @Override
                    public void onConnectionError(String errorMessage) {
                        System.out.println("JSON Error Thrown: "+errorMessage);
                        passed = true;
                        lock.countDown();
                    }
                });

        lock.await(5, TimeUnit.SECONDS);

        assertTrue(passed);
    }

    @Test
    public void shouldParseResponseWithStatus() throws Exception {
        System.out.println("Running Test shouldParseResponseWithStatus");

        passed = false;

        Robolectric.addPendingHttpResponse(200,USER_AUTH_JSON);

        api.login(new User("savons1", "123qwe"),new LoginListener() {

                    @Override
                    public void onLoginSuccess(User user,String optionalMessage) {
                        Profile profile = sessionManager.getUser().getProfile();

                        assertEquals("savons1", profile.getName());
                        assertEquals("sam.nazi@navteq.com", profile.getEmail());
                        assertEquals(new Date(1333027455), profile.getSignUp());
                        assertEquals(45, profile.getKarma());
                        assertEquals(38, profile.getNumVotes());
                        assertEquals(0, profile.getGlobalModerator());

                        passed = true;
                        lock.countDown();
                    }

                    @Override
                    public void onLoginFail(User user, StatusCode code) {
                        fail("Trapster Login Failed: " + code.toString());
                        lock.countDown();
                    }

                    @Override
                    public void onError(TrapsterError error) {
                        fail("Trapster Error: " + error.getDetails());
                        lock.countDown();
                    }
                },new CommunicationStatusListener(){

                    @Override
                    public void onOpenConnection() {

                    }

                    @Override
                    public void onCloseConnection() {

                    }

                    @Override
                    public void onConnectionError(String errorMessage) {
                        System.out.println("JSON Error Thrown:"+errorMessage);
                        passed = true;
                        lock.countDown();
                    }
                });

        lock.await(5, TimeUnit.SECONDS);

        assertTrue(passed);

    }

    @Test
    public void shouldPassWithConfirmationCode() throws Exception {
        System.out.println("Running Test shouldPassWithConfirmationCode");

        passed = false;

        Robolectric.addPendingHttpResponse(200,USER_AUTH_JSON);

        api.login(new User("savons1", "123qwe"),"123",new LoginListener() {

                    @Override
                    public void onLoginSuccess(User user,String optionalMessage) {
                        Profile profile = sessionManager.getUser().getProfile();

                        assertEquals("savons1", profile.getName());
                        assertEquals("sam.nazi@navteq.com", profile.getEmail());
                        assertEquals(new Date(1333027455), profile.getSignUp());
                        assertEquals(45, profile.getKarma());
                        assertEquals(38, profile.getNumVotes());
                        assertEquals(0, profile.getGlobalModerator());

                        passed = true;
                        lock.countDown();
                    }

                    @Override
                    public void onLoginFail(User user, StatusCode code) {
                        fail("Trapster Login Failed: " + code.toString());
                        lock.countDown();
                    }

                    @Override
                    public void onError(TrapsterError error) {
                        fail("Trapster Error: " + error.getDetails());
                        lock.countDown();
                    }
                },new CommunicationStatusListener(){

                    @Override
                    public void onOpenConnection() {

                    }

                    @Override
                    public void onCloseConnection() {

                    }

                    @Override
                    public void onConnectionError(String errorMessage) {
                        System.out.println("JSON Error Thrown:"+errorMessage);
                        passed = true;
                        lock.countDown();
                    }
                });

        lock.await(5, TimeUnit.SECONDS);

        assertTrue(passed);

    }


    @Test
    public void shouldPassOnResend() throws Exception {
        System.out.println("Running Test shouldPassOnResend");

        passed = false;

        Robolectric.addPendingHttpResponse(200,USER_AUTH_JSON);

        api.resendCode(new User("savons1", "123qwe"),new LoginListener() {

                    @Override
                    public void onLoginSuccess(User user,String optionalMessage) {

                        passed = true;
                        lock.countDown();
                    }

                    @Override
                    public void onLoginFail(User user, StatusCode code) {
                        fail("Trapster Login Failed: " + code.toString());
                        lock.countDown();
                    }

                    @Override
                    public void onError(TrapsterError error) {
                        fail("Trapster Error: " + error.getDetails());
                        lock.countDown();
                    }
                },new CommunicationStatusListener(){

                    @Override
                    public void onOpenConnection() {

                    }

                    @Override
                    public void onCloseConnection() {

                    }

                    @Override
                    public void onConnectionError(String errorMessage) {
                        System.out.println("JSON Error Thrown:"+errorMessage);
                        passed = true;
                        lock.countDown();
                    }
                });

        lock.await(5, TimeUnit.SECONDS);

        assertTrue(passed);

    }
}
