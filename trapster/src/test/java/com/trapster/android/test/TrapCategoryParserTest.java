package com.trapster.android.test;

import com.trapster.android.comms.CategoryListener;
import com.trapster.android.model.Categories;
import com.trapster.android.model.Category;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;
import com.xtremelabs.robolectric.Robolectric;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;

/**
 * Created by John on 5/28/13.
 */

public class TrapCategoryParserTest extends JsonParserTestBase{

    private static final String LOGNAME = "TrapCategoryParserTest";

    // From attributes.trap-scourge.com/2/attributes/trapcategory?deviceid=000000000000000&opsys=8&devicetype=Android-google_sdk-null&pwdhash=9ba99272e6118443e330c893fa129bb2&login=frankstrapster&appcapabilities=3&appkey=53c58e821bf3092e0e8863d473832115&appid=TPST-AND-EN-2.0.0&modifiedaftertime=0
    //
    private static final String TRAPCATEGORIES_JSON =  "{\"request\":null,\"response\":{\"message\":null,\"data\":{\"id\":null,\"string\":[],\"info\":null,\"displayname\":null,\"color\":[],\"image\":[],\"addedtheme\":[],\"addedpoi\":[],\"removedpoi\":[],\"poiTopics\":[],\"removed\":[],\"removedtheme\":[],\"trapcategory\":[{\"name\":\"Enforcement Points\",\"sequence\":2,\"id\":\"1\"},{\"name\":\"Road Hazards\",\"sequence\":3,\"id\":\"2\"},{\"name\":\"Most Popular\",\"sequence\":1,\"id\":\"3\"},{\"name\":\"EV Charging Stations\",\"sequence\":6,\"id\":\"5\"}],\"traptype\":[],\"alert\":null,\"marker\":[],\"added\":[],\"emailaddr\":null,\"pwdusernamehash\":null,\"mobilepwdhash\":null,\"usercredlevel\":null,\"alertradius\":null,\"signupepochtime\":null,\"lastvoteepochtime\":null,\"totaluservotes\":null,\"karma\":null,\"tosagreed\":null,\"trustedgrouppref\":null,\"privmsgpref\":null,\"smsaddr\":null,\"moderatorlevel\":null,\"loggedontositeflag\":null,\"newsletterflag\":null,\"emailconfflag\":null,\"userstatus\":null,\"lastkarma\":null,\"sipallowedflag\":null,\"loginfailures\":null,\"modapplyepochtime\":null,\"lastapiepochtime\":null,\"lastwebepochtime\":null,\"confirmepochtime\":null,\"confirmmethod\":null,\"facebook\":null,\"twitter\":null,\"twitterenabled\":null,\"facebookenabled\":null,\"tripSessionId\":null,\"patrollines\":[]},\"status\":\"OK\",\"asofepochtime\":1372258884,\"statuscode\":200}}";

    // Used to check if the parsing was done successfully
    private boolean passed = false;

    @Test
    public void shouldFailOnInvalidHttpResponseCode() throws Exception {
        System.out.println("shouldFailOnInvalidHttpResponseCode");

        passed = false;

        Robolectric.addPendingHttpResponse(405,"Invalid JSON");

        api.getCategories(0, new CategoryListener() {

                    @Override
                    public void onCategories(Categories categories) {
                        fail("JSON parsing passed magically");
                        lock.countDown();
                    }


                    @Override
                    public void onError(TrapsterError error) {
                        System.out.println("System Error Thrown:"+error.getDetails());
                        passed = true;
                        lock.countDown();
                    }

                },new CommunicationStatusListener(){

                    @Override
                    public void onOpenConnection() {

                    }

                    @Override
                    public void onCloseConnection() {

                    }

                    @Override
                    public void onConnectionError(String errorMessage) {
                        System.out.println("JSON Error Thrown:"+errorMessage);
                        passed = true;
                        lock.countDown();
                    }
                });

        lock.await(5, TimeUnit.SECONDS);

        assertTrue(passed);

    }


    @Test
    public void shouldFailOnInvalidJson() throws Exception {
        System.out.println("shouldFailOnInvalidJson");

        passed = false;

        Robolectric.addPendingHttpResponse(200,"Invalid JSON");

        api.getCategories(0, new CategoryListener() {

                    @Override
                    public void onCategories(Categories categories) {
                        fail("JSON parsing passed magically");
                        lock.countDown();
                    }


                    @Override
                    public void onError(TrapsterError error) {
                        passed = true;
                        lock.countDown();
                    }
                }, new CommunicationStatusListener() {

                    @Override
                    public void onOpenConnection() {

                    }

                    @Override
                    public void onCloseConnection() {

                    }

                    @Override
                    public void onConnectionError(String errorMessage) {
                        System.out.println("JSON Error Thrown:" + errorMessage);
                        passed = true;
                        lock.countDown();
                    }
                }
        );

        lock.await(5, TimeUnit.SECONDS);

        assertTrue(passed);

    }


        @Test
    public void shouldParseResponseWithStatus() throws Exception {
        System.out.println("Running Test shouldParseResponseWithStatus");

        passed = false;

        Robolectric.addPendingHttpResponse(200,TRAPCATEGORIES_JSON);

            api.getCategories(0, new CategoryListener() {

                @Override
                public void onCategories(Categories categories) {
                    assertNotNull(categories);
                    System.out.println("Parsed Trap Categories:" + categories.getCategories().size());

                    assertEquals(4, categories.getCategories().size());

                    Category tc = categories.getCategories().get(0);
                    assertNotNull(tc);

                    assertEquals("Enforcement Points",tc.getName());

                    assertEquals(2,tc.getSequence());

                    assertEquals(1,tc.getId());

                    passed = true;

                    lock.countDown();
                }


                @Override
                public void onError(TrapsterError error) {
                    fail("Trapster Error:" + error.getDetails());
                    lock.countDown();
                }
            }, new TestCommunicationListener());

        lock.await(5, TimeUnit.SECONDS);

        assertTrue(passed);

    }


}
