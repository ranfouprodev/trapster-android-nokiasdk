package com.trapster.android.test;

import com.trapster.android.comms.SoundThemeListener;
import com.trapster.android.model.SoundLink;
import com.trapster.android.model.SoundTheme;
import com.trapster.android.model.SoundThemes;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;
import com.xtremelabs.robolectric.Robolectric;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;

/**
 * Created by John on 5/28/13.
 */

public class SoundThemeParserTest extends JsonParserTestBase {

    private static final String LOGNAME = "SoundThemeParserTest";

    // From attributes.trap-scourge.com/2/attributes/theme?deviceid=000000000000000&opsys=8&devicetype=Android-google_sdk-null&pwdhash=9ba99272e6118443e330c893fa129bb2&login=frankstrapster&appcapabilities=3&appkey=53c58e821bf3092e0e8863d473832115&appid=TPST-AND-EN-2.0.0&modifiedaftertime=0
    //
    // Note string from server is too long. Clipped to include only the first two added themes
    private static final String SOUNDTHEMES_JSON = "{ \"request\": null, \"response\": { \"message\": null, \"data\": { \"id\": null, \"string\": [], \"info\": null, \"displayname\": null, \"color\": [], \"image\": [], \"addedtheme\": [ { \"name\": \"Arabic\", \"version\": \"1.0\", \"description\": \"ØªÙ†Ø¨ÙŠÙ‡Ø§Øª ØµÙˆØªÙŠØ© Ø¹Ø±Ø¨ÙŠØ©\", \"links\": { \"link\": [ { \"path\": \"http://www.trapster.com/storage/theme/159/lp.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/159/lp.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/159/lp.mp3\" }, \"lid\": \"lp\" }, { \"path\": \"http://www.trapster.com/storage/theme/159/rlc.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/159/rlc.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/159/rlc.mp3\" }, \"lid\": \"rlc\" }, { \"path\": \"http://www.trapster.com/storage/theme/159/sc.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/159/sc.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/159/sc.mp3\" }, \"lid\": \"sc\" }, { \"path\": \"http://www.trapster.com/storage/theme/159/hp.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/159/hp.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/159/hp.mp3\" }, \"lid\": \"hp\" }, { \"path\": \"http://www.trapster.com/storage/theme/159/cb.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/159/cb.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/159/cb.mp3\" }, \"lid\": \"cb\" }, { \"path\": \"http://www.trapster.com/storage/theme/159/msc.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/159/msc.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/159/msc.mp3\" }, \"lid\": \"msc\" }, { \"path\": \"http://www.trapster.com/storage/theme/159/childrenatplay.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/159/childrenatplay.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/159/childrenatplay.mp3\" }, \"lid\": \"childrenatplay\" }, { \"path\": \"http://www.trapster.com/storage/theme/159/accident.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/159/accident.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/159/accident.mp3\" }, \"lid\": \"accident\" }, { \"path\": \"http://www.trapster.com/storage/theme/159/brushfire.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/159/brushfire.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/159/brushfire.mp3\" }, \"lid\": \"brushfire\" }, { \"path\": \"http://www.trapster.com/storage/theme/159/dangerintersect.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/159/dangerintersect.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/159/dangerintersect.mp3\" }, \"lid\": \"dangerintersect\" }, { \"path\": \"http://www.trapster.com/storage/theme/159/dangerouscurve.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/159/dangerouscurve.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/159/dangerouscurve.mp3\" }, \"lid\": \"dangerouscurve\" }, { \"path\": \"http://www.trapster.com/storage/theme/159/flood.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/159/flood.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/159/flood.mp3\" }, \"lid\": \"flood\" }, { \"path\": \"http://www.trapster.com/storage/theme/159/ice.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/159/ice.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/159/ice.mp3\" }, \"lid\": \"ice\" }, { \"path\": \"http://www.trapster.com/storage/theme/159/narrowbridge.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/159/narrowbridge.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/159/narrowbridge.mp3\" }, \"lid\": \"narrowbridge\" }, { \"path\": \"http://www.trapster.com/storage/theme/159/roadclosed.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/159/roadclosed.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/159/roadclosed.mp3\" }, \"lid\": \"roadclosed\" }, { \"path\": \"http://www.trapster.com/storage/theme/159/roadkill.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/159/roadkill.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/159/roadkill.mp3\" }, \"lid\": \"roadkill\" }, { \"path\": \"http://www.trapster.com/storage/theme/159/tollbooth.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/159/tollbooth.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/159/tollbooth.mp3\" }, \"lid\": \"tollbooth\" }, { \"path\": \"http://www.trapster.com/storage/theme/159/construction.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/159/construction.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/159/construction.mp3\" }, \"lid\": \"construction\" }, { \"path\": \"http://www.trapster.com/storage/theme/159/mscosh.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/159/mscosh.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/159/mscosh.mp3\" }, \"lid\": \"mscosh\" }, { \"path\": \"http://www.trapster.com/storage/theme/159/schoolzone.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/159/schoolzone.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/159/schoolzone.mp3\" }, \"lid\": \"schoolzone\" }, { \"path\": \"http://www.trapster.com/storage/theme/159/roadhazard.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/159/roadhazard.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/159/roadhazard.mp3\" }, \"lid\": \"roadhazard\" }, { \"path\": \"http://www.trapster.com/storage/theme/159/trafficjam.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/159/trafficjam.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/159/trafficjam.mp3\" }, \"lid\": \"trafficjam\" } ] }, \"author\": \"Trapster\", \"created\": 1320844951, \"updated\": 1353448020, \"id\": \"159\" }, { \"name\": \"Arnold\", \"version\": \"1.0\", \"description\": \"Arnold parody by Jeremy Cage\", \"links\": { \"link\": [ { \"path\": \"http://www.trapster.com/storage/theme/120/lp.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/120/lp.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/120/lp.mp3\" }, \"lid\": \"lp\" }, { \"path\": \"http://www.trapster.com/storage/theme/120/rlc.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/120/rlc.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/120/rlc.mp3\" }, \"lid\": \"rlc\" }, { \"path\": \"http://www.trapster.com/storage/theme/120/sc.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/120/sc.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/120/sc.mp3\" }, \"lid\": \"sc\" }, { \"path\": \"http://www.trapster.com/storage/theme/120/hp.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/120/hp.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/120/hp.mp3\" }, \"lid\": \"hp\" }, { \"path\": \"http://www.trapster.com/storage/theme/120/cb.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/120/cb.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/120/cb.mp3\" }, \"lid\": \"cb\" }, { \"path\": \"http://www.trapster.com/storage/theme/120/msc.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/120/msc.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/120/msc.mp3\" }, \"lid\": \"msc\" }, { \"path\": \"http://www.trapster.com/storage/theme/120/childrenatplay.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/120/childrenatplay.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/120/childrenatplay.mp3\" }, \"lid\": \"childrenatplay\" }, { \"path\": \"http://www.trapster.com/storage/theme/120/accident.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/120/accident.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/120/accident.mp3\" }, \"lid\": \"accident\" }, { \"path\": \"http://www.trapster.com/storage/theme/120/brushfire.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/120/brushfire.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/120/brushfire.mp3\" }, \"lid\": \"brushfire\" }, { \"path\": \"http://www.trapster.com/storage/theme/120/dangerintersect.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/120/dangerintersect.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/120/dangerintersect.mp3\" }, \"lid\": \"dangerintersect\" }, { \"path\": \"http://www.trapster.com/storage/theme/120/dangerouscurve.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/120/dangerouscurve.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/120/dangerouscurve.mp3\" }, \"lid\": \"dangerouscurve\" }, { \"path\": \"http://www.trapster.com/storage/theme/120/flood.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/120/flood.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/120/flood.mp3\" }, \"lid\": \"flood\" }, { \"path\": \"http://www.trapster.com/storage/theme/120/ice.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/120/ice.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/120/ice.mp3\" }, \"lid\": \"ice\" }, { \"path\": \"http://www.trapster.com/storage/theme/120/narrowbridge.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/120/narrowbridge.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/120/narrowbridge.mp3\" }, \"lid\": \"narrowbridge\" }, { \"path\": \"http://www.trapster.com/storage/theme/120/roadclosed.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/120/roadclosed.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/120/roadclosed.mp3\" }, \"lid\": \"roadclosed\" }, { \"path\": \"http://www.trapster.com/storage/theme/120/roadkill.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/120/roadkill.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/120/roadkill.mp3\" }, \"lid\": \"roadkill\" }, { \"path\": \"http://www.trapster.com/storage/theme/120/tollbooth.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/120/tollbooth.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/120/tollbooth.mp3\" }, \"lid\": \"tollbooth\" }, { \"path\": \"http://www.trapster.com/storage/theme/120/construction.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/120/construction.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/120/construction.mp3\" }, \"lid\": \"construction\" }, { \"path\": \"http://www.trapster.com/storage/theme/120/mscosh.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/120/mscosh.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/120/mscosh.mp3\" }, \"lid\": \"mscosh\" }, { \"path\": \"http://www.trapster.com/storage/theme/120/schoolzone.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/120/schoolzone.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/120/schoolzone.mp3\" }, \"lid\": \"schoolzone\" }, { \"path\": \"http://www.trapster.com/storage/theme/120/roadhazard.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/120/roadhazard.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/120/roadhazard.mp3\" }, \"lid\": \"roadhazard\" }, { \"path\": \"http://www.trapster.com/storage/theme/120/trafficjam.mp3\", \"formats\": { \"aiff\": \"http://www.trapster.com/storage/theme/120/trafficjam.aiff\", \"mp3\": \"http://www.trapster.com/storage/theme/120/trafficjam.mp3\" }, \"lid\": \"trafficjam\" } ] }, \"author\": \"Jeremy Cage\", \"created\": 1257724800, \"updated\": 1353448020, \"id\": \"120\" } ], \"addedpoi\": [], \"removedpoi\": [], \"poiTopics\": [], \"removed\": [ \"146\", \"147\", \"161\", \"164\", \"165\" ], \"removedtheme\": [ \"145\", \"152\", \"153\", \"154\", \"155\" ], \"trapcategory\": [], \"traptype\": [], \"alert\": null, \"marker\": [], \"added\": [], \"emailaddr\": null, \"pwdusernamehash\": null, \"mobilepwdhash\": null, \"usercredlevel\": null, \"alertradius\": null, \"signupepochtime\": null, \"lastvoteepochtime\": null, \"totaluservotes\": null, \"karma\": null, \"tosagreed\": null, \"trustedgrouppref\": null, \"privmsgpref\": null, \"smsaddr\": null, \"moderatorlevel\": null, \"loggedontositeflag\": null, \"newsletterflag\": null, \"emailconfflag\": null, \"userstatus\": null, \"lastkarma\": null, \"sipallowedflag\": null, \"loginfailures\": null, \"modapplyepochtime\": null, \"lastapiepochtime\": null, \"lastwebepochtime\": null, \"confirmepochtime\": null, \"confirmmethod\": null, \"facebook\": null, \"twitter\": null, \"twitterenabled\": null, \"facebookenabled\": null, \"tripSessionId\": null, \"patrollines\": [] }, \"status\": \"OK\", \"asofepochtime\": 1372199968, \"statuscode\": 200 } }";

    // Used to check if the parsing was done successfully
    private boolean passed = false;

    @Test
    public void shouldFailOnInvalidHttpResponseCode() throws Exception {
        System.out.println("shouldFailOnInvalidHttpResponseCode");

        passed = false;

        Robolectric.addPendingHttpResponse(405, "Invalid JSON");

        api.listSounds(0, new SoundThemeListener() {


                    @Override
                    public void onError(TrapsterError error) {
                        System.out.println("System Error Thrown:" + error.getDetails());
                        passed = true;
                        lock.countDown();
                    }

                    @Override
                    public void onSoundThemeChanges(SoundThemes themes) {
                        fail("JSON parsing passed magically");
                        lock.countDown();
                    }
                }, new CommunicationStatusListener() {

                    @Override
                    public void onOpenConnection() {

                    }

                    @Override
                    public void onCloseConnection() {

                    }

                    @Override
                    public void onConnectionError(String errorMessage) {
                        System.out.println("JSON Error Thrown:" + errorMessage);
                        passed = true;
                        lock.countDown();
                    }
                }
        );

        lock.await(5, TimeUnit.SECONDS);

        assertTrue(passed);

    }


    @Test
    public void shouldFailOnInvalidJson() throws Exception {
        System.out.println("shouldFailOnInvalidJson");

        passed = false;

        Robolectric.addPendingHttpResponse(200, "Invalid JSON");

        api.listSounds(0, new SoundThemeListener() {
                    @Override
                    public void onSoundThemeChanges(SoundThemes themes) {
                        fail("JSON parsing passed magically");
                        lock.countDown();
                    }

                    @Override
                    public void onError(TrapsterError error) {
                        passed = true;
                        lock.countDown();
                    }
                }, new CommunicationStatusListener() {

                    @Override
                    public void onOpenConnection() {

                    }

                    @Override
                    public void onCloseConnection() {

                    }

                    @Override
                    public void onConnectionError(String errorMessage) {
                        System.out.println("JSON Error Thrown:" + errorMessage);
                        passed = true;
                        lock.countDown();
                    }
                }
        );

        lock.await(5, TimeUnit.SECONDS);

        assertTrue(passed);

    }


    @Test
    public void shouldParseResponseWithStatus() throws InterruptedException {
        System.out.println("Running Test shouldParseResponseWithStatus");

        passed = false;

        Robolectric.addPendingHttpResponse(200, SOUNDTHEMES_JSON);

        api.listSounds(0, new SoundThemeListener() {
            @Override
            public void onSoundThemeChanges(SoundThemes themes) {

                    assertNotNull(themes);

                    System.out.println("Parsed SoundThemes:" + themes.getAdded().size() + ":" + themes.getRemoved());

                    assertEquals(2, themes.getAdded().size());
                    SoundTheme arabic = themes.getAdded().get(0);

                    assertEquals("Arabic", arabic.getName());
                    assertEquals("1.0", arabic.getVersion());
                    // @TODO fix with utf encoding
                   // assertEquals("ØªÙ†Ø¨ÙŠÙ‡Ø§Øª ØµÙˆØªÙŠØ© Ø¹Ø±Ø¨ÙŠØ©", arabic.getDescription());

                    assertEquals("Trapster", arabic.getAuthor());
                    assertEquals(1320844951, arabic.getCreated());
                    assertEquals(1353448020, arabic.getUpdated());
                    assertEquals(159, arabic.getId());

                    assertNotNull(arabic.getLinks());
                    assertEquals(22, arabic.getLinks().size());

                    SoundLink link = arabic.getLinks().get(0);

                    assertEquals("http://www.trapster.com/storage/theme/159/lp.mp3", link.getPath());
                    assertEquals("lp", link.getLid());

                    assertNotNull(link.getFormats());
                    assertEquals("http://www.trapster.com/storage/theme/159/lp.aiff", link.getFormats().getAiff());
                    assertEquals("http://www.trapster.com/storage/theme/159/lp.mp3", link.getFormats().getMp3());


                    assertEquals(5, themes.getRemoved().size());

                    assertEquals(145, themes.getRemoved().get(0).intValue());

                    passed = true;
                    lock.countDown();

            }

            @Override
            public void onError(TrapsterError error) {
                fail("Trapster Error:" + error.getDetails());
                lock.countDown();
            }
        }, new TestCommunicationListener());

        assertTrue(lock.await(30, TimeUnit.SECONDS));

        assertTrue(passed);

    }


}
