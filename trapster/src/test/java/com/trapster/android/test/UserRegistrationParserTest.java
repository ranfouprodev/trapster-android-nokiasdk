package com.trapster.android.test;

import com.trapster.android.comms.LoginListener;
import com.trapster.android.comms.rest.StatusCode;
import com.trapster.android.model.User;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;
import com.xtremelabs.robolectric.Robolectric;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;

/**
 * Created with IntelliJ IDEA.
 * User: snazi
 * Date: 6/24/13
 * Time: 10:38 AM
 * To change this template use File | Settings | File Templates.
 */
public class UserRegistrationParserTest extends JsonParserTestBase {

    /*
    From
            Posting to:http://user.trap-scourge.com/2/user/qwerty
            Params:{"request":{"auth":{"appcapabilities":"3","appid":"TPST-AND-EN-4.0.1.1","appkey":"b9f50e2b4da4e74a7fbb7e4ec9c9d1d7","deviceid":"351746050721086","devicetype":"Roam Mobility@-yakju-01","pwdhash":"a89dc2825c7fee3acd022d73b539350d","login":"demo"},"meta":{"opsys":"4.2.2"},"params":{"tosagreed":"Y","pwdcnf":"qwerty","emailaddr":"test@2linessoftware.com","newsletterflag":"N","pwd":"qwerty","smsaddr":"7736918402","displayname":"qwerty"}}}

     */


    private static final String EMAIL_ALREADY_TAKEN =  "{ \"request\": null, \"response\": { \"message\": \"The email address test@test.com is already registered\", \"data\": null, \"status\": \"ERROR\", \"statuscode\": 609, \"asofepochtime\": 1374162662 } }";
    private static final String USERNAME_ALREADY_TAKEN =  "{ \"request\": null, \"response\": { \"message\": \"The username username is not available. Please try another\", \"data\": null, \"status\": \"ERROR\", \"statuscode\": 614, \"asofepochtime\": 1374162662 } }";
    private static final String USER_AUTH_JSON =  "{ \"request\": null, \"response\": { \"message\": null, \"data\": null, \"status\": \"OK\", \"statuscode\": 200, \"asofepochtime\": 1374168093 } }";

    // Used to check if the parsing was done successfully
    private boolean passed = false;



    @Test
    public void shouldFailOnEmailTaken() throws Exception {
        System.out.println("shouldFailOnEmailTaken");

        passed = false;

        Robolectric.addPendingHttpResponse(200, EMAIL_ALREADY_TAKEN);

        api.register("test@test.com", "username","password",true,true, "smsaddr", "carrier",new LoginListener() {

                    @Override
                    public void onLoginSuccess(User user,String optionalMessage) {
                        fail("JSON parsing passed magically");
                        lock.countDown();
                    }

                    @Override
                    public void onLoginFail(User user, StatusCode code) {
                        if(code == StatusCode.EMAIL_TAKEN){
                            passed = true;
                        }else
                            fail("JSON parsing passed magically");
                        lock.countDown();
                        lock.countDown();
                    }

                    @Override
                    public void onError(TrapsterError error) {
                        assertEquals(609, error.getType());
                        passed = true;
                        lock.countDown();
                    }
                },new CommunicationStatusListener(){

                    @Override
                    public void onOpenConnection() {

                    }

                    @Override
                    public void onCloseConnection() {

                    }

                    @Override
                    public void onConnectionError(String errorMessage) {
                        System.out.println("JSON Error Thrown: "+errorMessage);
                        passed = true;
                        lock.countDown();
                    }
                });

        lock.await(5, TimeUnit.SECONDS);

        assertTrue(passed);
    }


    @Test
    public void shouldFailOnUsernameTaken() throws Exception {
        System.out.println("shouldFailOnUsernameTaken");

        passed = false;

        Robolectric.addPendingHttpResponse(200, USERNAME_ALREADY_TAKEN);

        api.register("test@test.com", "username","password",true,true, "smsaddr", "carrier",new LoginListener() {

                    @Override
                    public void onLoginSuccess(User user, String optionalMessage) {
                        fail("JSON parsing passed magically");
                        lock.countDown();
                    }

                    @Override
                    public void onLoginFail(User user, StatusCode code) {
                        if(code == StatusCode.USERNAME_IN_USE){
                            passed = true;
                        }else
                            fail("JSON parsing passed magically");

                        lock.countDown();
                    }

                    @Override
                    public void onError(TrapsterError error) {
                        assertEquals(614,error.getType());
                        passed = true;
                        lock.countDown();
                    }
                },new CommunicationStatusListener(){

                    @Override
                    public void onOpenConnection() {

                    }

                    @Override
                    public void onCloseConnection() {

                    }

                    @Override
                    public void onConnectionError(String errorMessage) {
                        System.out.println("JSON Error Thrown: "+errorMessage);
                        passed = true;
                        lock.countDown();
                    }
                });

        lock.await(5, TimeUnit.SECONDS);

        assertTrue(passed);
    }

    @Test
    public void shouldFailOnInvalidJson() throws Exception {
        System.out.println("shouldFailOnInvalidJson");

        passed = false;

        Robolectric.addPendingHttpResponse(200, "Invalid JSON");

        api.register("test@test.com", "username","password",true,true, "smsaddr", "carrier",new LoginListener() {

                    @Override
                    public void onLoginSuccess(User user,String optionalMessage) {
                        fail("JSON parsing passed magically");
                        lock.countDown();
                    }

                    @Override
                    public void onLoginFail(User user, StatusCode code) {
                        fail("JSON parsing passed magically");
                        lock.countDown();
                    }

                    @Override
                    public void onError(TrapsterError error) {
                        passed = true;
                        lock.countDown();
                    }
                },new CommunicationStatusListener(){

                    @Override
                    public void onOpenConnection() {

                    }

                    @Override
                    public void onCloseConnection() {

                    }

                    @Override
                    public void onConnectionError(String errorMessage) {
                        System.out.println("JSON Error Thrown: "+errorMessage);
                        passed = true;
                        lock.countDown();
                    }
                });

        lock.await(30, TimeUnit.SECONDS);

        assertTrue(passed);
    }


    @Test
    public void shouldParseResponseWithStatus() throws Exception {
        System.out.println("Running Test shouldParseResponseWithStatus");

        passed = false;

        Robolectric.addPendingHttpResponse(200,USER_AUTH_JSON);

        api.register("test@test.com", "username","password",true,true, "smsaddr", "carrier",new LoginListener() {

                    @Override
                    public void onLoginSuccess(User user,String optionalMessage) {
                        passed = true;
                        lock.countDown();
                    }

                    @Override
                    public void onLoginFail(User user, StatusCode code) {
                        fail("Trapster Login Failed: " + code.toString());
                        lock.countDown();
                    }

                    @Override
                    public void onError(TrapsterError error) {
                        fail("Trapster Error: " + error.getDetails());
                        lock.countDown();
                    }
                },new CommunicationStatusListener(){

                    @Override
                    public void onOpenConnection() {

                    }

                    @Override
                    public void onCloseConnection() {

                    }

                    @Override
                    public void onConnectionError(String errorMessage) {
                        System.out.println("JSON Error Thrown:"+errorMessage);
                        passed = true;
                        lock.countDown();
                    }
                });

        lock.await(5, TimeUnit.SECONDS);

        assertTrue(passed);

    }


}
