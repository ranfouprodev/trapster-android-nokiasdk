package com.trapster.android.test;

import com.trapster.android.comms.TrapListener;
import com.trapster.android.model.Trap;
import com.trapster.android.model.Traps;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;
import com.xtremelabs.robolectric.Robolectric;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;

/**
 * Created by John on 5/28/13.
 */

public class TrapParserTest extends JsonParserTestBase {

    private static final String LOGNAME = "TrapParserTest";

    // From trap.trap-scourge.com/2/trap?deviceid=000000000000000&opsys=8&devicetype=Android-google_sdk-null&pwdhash=9ba99272e6118443e330c893fa129bb2&login=frankstrapster&appcapabilities=3&appkey=53c58e821bf3092e0e8863d473832115&appid=TPST-AND-EN-2.0.0&latitude=51.04&longitude=-114.01&radius=5
    //
    private static final String TRAP_JSON = "{\"request\":null,\"response\":{\"message\":null,\"data\":{\"id\":null,\"string\":[],\"info\":null,\"displayname\":null,\"karma\":null,\"removed\":[],\"added\":[],\"marker\":[{\"id\":42376,\"country\":\"CAN\",\"source\":null,\"speedlimit\":null,\"numvotes\":106,\"latitude\":51.067108297085,\"longitude\":-114.03904086905,\"reporteddirection\":null,\"linkid\":133188071,\"traptypeid\":3,\"origdisplayname\":\"aln\",\"lastupdatedisplayname\":\"doyle911\",\"lastvoteepochtime\":1355857003,\"trapaddress\":\"954 16 Ave NE, Calgary, AB T2E, Canada\",\"trapcredlevel\":16,\"lifetime\":0,\"badgekey\":\"badge_moderator\",\"affecteddirection\":null,\"camerafacingdirection\":null,\"fieldverified\":false},{\"id\":44457,\"country\":\"CAN\",\"source\":null,\"speedlimit\":null,\"numvotes\":31,\"latitude\":51.067398592871,\"longitude\":-114.023655653,\"reporteddirection\":null,\"linkid\":34802457,\"traptypeid\":3,\"origdisplayname\":\"dnsradio\",\"lastupdatedisplayname\":\"n00bsmonkeys\",\"lastvoteepochtime\":1360057659,\"trapaddress\":\"16 Ave NE, Calgary, AB T2E, Canada\",\"trapcredlevel\":16,\"lifetime\":0,\"badgekey\":\"badge_moderator\",\"affecteddirection\":null,\"camerafacingdirection\":null,\"fieldverified\":false},{\"id\":91423,\"country\":\"CAN\",\"source\":null,\"speedlimit\":null,\"numvotes\":37,\"latitude\":51.050783,\"longitude\":-113.993282,\"reporteddirection\":null,\"linkid\":87899304,\"traptypeid\":3,\"origdisplayname\":\"talon\",\"lastupdatedisplayname\":\"rkp4\",\"lastvoteepochtime\":1359546021,\"trapaddress\":\"201 28 St SE, Calgary, AB T2A, Canada\",\"trapcredlevel\":16,\"lifetime\":0,\"badgekey\":\"badge_moderator\",\"affecteddirection\":null,\"camerafacingdirection\":null,\"fieldverified\":false},{\"id\":252810,\"country\":\"CAN\",\"source\":null,\"speedlimit\":null,\"numvotes\":72,\"latitude\":51.0660316195,\"longitude\":-114.02617692947,\"reporteddirection\":null,\"linkid\":34801048,\"traptypeid\":4,\"origdisplayname\":\"edncda\",\"lastupdatedisplayname\":\"henryorve1990\",\"lastvoteepochtime\":1363978767,\"trapaddress\":\"HWY-1, Calgary, AB, Canada\",\"trapcredlevel\":16,\"lifetime\":0,\"badgekey\":\"badge_moderator\",\"affecteddirection\":null,\"camerafacingdirection\":null,\"fieldverified\":false},{\"id\":349874,\"country\":\"CAN\",\"source\":null,\"speedlimit\":null,\"numvotes\":63,\"latitude\":51.066528858628,\"longitude\":-114.02900934219,\"reporteddirection\":null,\"linkid\":34801038,\"traptypeid\":3,\"origdisplayname\":\"thatguy789\",\"lastupdatedisplayname\":\"djack14\",\"lastvoteepochtime\":1359051576,\"trapaddress\":\"HWY-1, Calgary, AB, Canada\",\"trapcredlevel\":16,\"lifetime\":0,\"badgekey\":\"badge_moderator\",\"affecteddirection\":null,\"camerafacingdirection\":null,\"fieldverified\":false},{\"id\":357001,\"country\":\"CAN\",\"source\":null,\"speedlimit\":null,\"numvotes\":58,\"latitude\":51.049638279327,\"longitude\":-113.99084687233,\"reporteddirection\":null,\"linkid\":87899382,\"traptypeid\":4,\"origdisplayname\":\"smokeonit\",\"lastupdatedisplayname\":\"odess\",\"lastvoteepochtime\":1364836009,\"trapaddress\":\"28 St SE, Calgary, AB, Canada\",\"trapcredlevel\":16,\"lifetime\":0,\"badgekey\":\"badge_moderator\",\"affecteddirection\":null,\"camerafacingdirection\":null,\"fieldverified\":false},{\"id\":357003,\"country\":\"CAN\",\"source\":null,\"speedlimit\":null,\"numvotes\":86,\"latitude\":51.051968497019,\"longitude\":-114.00138527155,\"reporteddirection\":null,\"linkid\":87899450,\"traptypeid\":4,\"origdisplayname\":\"smokeonit\",\"lastupdatedisplayname\":\"odess\",\"lastvoteepochtime\":1364834343,\"trapaddress\":\"Centre Ave SE, Calgary, AB, Canada\",\"trapcredlevel\":16,\"lifetime\":0,\"badgekey\":\"badge_moderator\",\"affecteddirection\":null,\"camerafacingdirection\":null,\"fieldverified\":false},{\"id\":478855,\"country\":\"CAN\",\"source\":null,\"speedlimit\":null,\"numvotes\":87,\"latitude\":51.049027498209,\"longitude\":-114.01885724621,\"reporteddirection\":null,\"linkid\":34801057,\"traptypeid\":4,\"origdisplayname\":\"edncda\",\"lastupdatedisplayname\":\"dryinsertdan\",\"lastvoteepochtime\":1365706973,\"trapaddress\":\"Hwy 2, Calgary, AB, Canada\",\"trapcredlevel\":16,\"lifetime\":0,\"badgekey\":\"badge_moderator\",\"affecteddirection\":null,\"camerafacingdirection\":null,\"fieldverified\":false},{\"id\":1069193,\"country\":\"CAN\",\"source\":null,\"speedlimit\":null,\"numvotes\":24,\"latitude\":51.04071256229,\"longitude\":-114.0020263195,\"reporteddirection\":null,\"linkid\":32981668,\"traptypeid\":3,\"origdisplayname\":\"cendersby\",\"lastupdatedisplayname\":\"djack14\",\"lastvoteepochtime\":1363287579,\"trapaddress\":\"Barlow Trail SE, Calgary, AB, Canada\",\"trapcredlevel\":16,\"lifetime\":0,\"badgekey\":\"badge_moderator\",\"affecteddirection\":null,\"camerafacingdirection\":null,\"fieldverified\":false},{\"id\":1916517,\"country\":\"CAN\",\"source\":null,\"speedlimit\":null,\"numvotes\":10,\"latitude\":51.066904,\"longitude\":-114.013449,\"reporteddirection\":null,\"linkid\":32980980,\"traptypeid\":23,\"origdisplayname\":\"lamsik\",\"lastupdatedisplayname\":\"henryorve1990\",\"lastvoteepochtime\":1363978804,\"trapaddress\":\"16 Ave NE, Calgary, AB, Canada\",\"trapcredlevel\":16,\"lifetime\":0,\"badgekey\":\"badge_moderator\",\"affecteddirection\":null,\"camerafacingdirection\":null,\"fieldverified\":false},{\"id\":2106911,\"country\":\"CAN\",\"source\":null,\"speedlimit\":null,\"numvotes\":7,\"latitude\":51.039114,\"longitude\":-114.028885,\"reporteddirection\":null,\"linkid\":87901441,\"traptypeid\":32,\"origdisplayname\":\"fj55\",\"lastupdatedisplayname\":\"dgh1\",\"lastvoteepochtime\":1362524652,\"trapaddress\":\"1501-1523 9 Ave SE, Calgary, AB T2G 0T7, Canada\",\"trapcredlevel\":8,\"lifetime\":0,\"badgekey\":null,\"affecteddirection\":null,\"camerafacingdirection\":null,\"fieldverified\":false},{\"id\":2106919,\"country\":\"CAN\",\"source\":null,\"speedlimit\":null,\"numvotes\":11,\"latitude\":51.034792,\"longitude\":-114.018732,\"reporteddirection\":null,\"linkid\":87902079,\"traptypeid\":32,\"origdisplayname\":\"fj55\",\"lastupdatedisplayname\":\"n00bsmonkeys\",\"lastvoteepochtime\":1359120741,\"trapaddress\":\"1921 9 Ave SE, Calgary, AB T2G 0V2, Canada\",\"trapcredlevel\":16,\"lifetime\":0,\"badgekey\":\"badge_moderator\",\"affecteddirection\":null,\"camerafacingdirection\":null,\"fieldverified\":false},{\"id\":2736548,\"country\":\"CAN\",\"source\":null,\"speedlimit\":null,\"numvotes\":6,\"latitude\":51.040182,\"longitude\":-114.041933,\"reporteddirection\":null,\"linkid\":87901164,\"traptypeid\":23,\"origdisplayname\":\"jesud\",\"lastupdatedisplayname\":\"dgh1\",\"lastvoteepochtime\":1362524681,\"trapaddress\":\"801-805 11 Ave SE, Calgary, AB T2G 2Z5, Canada\",\"trapcredlevel\":8,\"lifetime\":0,\"badgekey\":null,\"affecteddirection\":null,\"camerafacingdirection\":null,\"fieldverified\":false},{\"id\":2958916,\"country\":\"CAN\",\"source\":null,\"speedlimit\":null,\"numvotes\":10,\"latitude\":51.051026,\"longitude\":-113.99396,\"reporteddirection\":null,\"linkid\":87899185,\"traptypeid\":32,\"origdisplayname\":\"thecraw\",\"lastupdatedisplayname\":\"odess\",\"lastvoteepochtime\":1364835960,\"trapaddress\":\"115-225 28 St SE, Calgary, AB T2A 6J9, Canada\",\"trapcredlevel\":16,\"lifetime\":0,\"badgekey\":\"badge_moderator\",\"affecteddirection\":null,\"camerafacingdirection\":null,\"fieldverified\":false},{\"id\":3081843,\"country\":\"CAN\",\"source\":null,\"speedlimit\":null,\"numvotes\":13,\"latitude\":51.036772760585,\"longitude\":-114.02320504189,\"reporteddirection\":null,\"linkid\":87923328,\"traptypeid\":27,\"origdisplayname\":\"malolo162\",\"lastupdatedisplayname\":\"ajg45\",\"lastvoteepochtime\":1352651336,\"trapaddress\":\"2003 16 St SE, Calgary, AB T2G 0V1, Canada\",\"trapcredlevel\":16,\"lifetime\":0,\"badgekey\":\"badge_moderator\",\"affecteddirection\":null,\"camerafacingdirection\":null,\"fieldverified\":false},{\"id\":3113343,\"country\":\"CAN\",\"source\":null,\"speedlimit\":null,\"numvotes\":10,\"latitude\":51.035785,\"longitude\":-113.983034,\"reporteddirection\":null,\"linkid\":87901912,\"traptypeid\":23,\"origdisplayname\":\"malolo162\",\"lastupdatedisplayname\":\"geojr001\",\"lastvoteepochtime\":1361384737,\"trapaddress\":\"2008-2036 35 St SE, Calgary, AB T2B 0W9, Canada\",\"trapcredlevel\":16,\"lifetime\":0,\"badgekey\":\"badge_moderator\",\"affecteddirection\":null,\"camerafacingdirection\":null,\"fieldverified\":false},{\"id\":3482989,\"country\":\"CAN\",\"source\":null,\"speedlimit\":null,\"numvotes\":7,\"latitude\":51.069494940669,\"longitude\":-114.01032111602,\"reporteddirection\":null,\"linkid\":33009355,\"traptypeid\":32,\"origdisplayname\":\"edncda\",\"lastupdatedisplayname\":\"dgh1\",\"lastvoteepochtime\":1362524376,\"trapaddress\":\"2288 18 Ave NE, Calgary, AB T2E 6S5, Canada\",\"trapcredlevel\":16,\"lifetime\":0,\"badgekey\":\"badge_moderator\",\"affecteddirection\":null,\"camerafacingdirection\":null,\"fieldverified\":false},{\"id\":5005084,\"country\":\"CAN\",\"source\":null,\"speedlimit\":null,\"numvotes\":7,\"latitude\":51.063905686489,\"longitude\":-114.04098412948,\"reporteddirection\":null,\"linkid\":87897303,\"traptypeid\":32,\"origdisplayname\":\"g0ny\",\"lastupdatedisplayname\":\"jerkman74\",\"lastvoteepochtime\":1353774938,\"trapaddress\":\"1101-1145 Regal Crescent NE, Calgary, AB T2E 5H3, Canada\",\"trapcredlevel\":16,\"lifetime\":0,\"badgekey\":\"badge_moderator\",\"affecteddirection\":null,\"camerafacingdirection\":null,\"fieldverified\":false},{\"id\":5005090,\"country\":\"CAN\",\"source\":null,\"speedlimit\":null,\"numvotes\":5,\"latitude\":51.05661302123,\"longitude\":-114.03175196605,\"reporteddirection\":null,\"linkid\":87898562,\"traptypeid\":32,\"origdisplayname\":\"g0ny\",\"lastupdatedisplayname\":\"g0ny\",\"lastvoteepochtime\":1321487811,\"trapaddress\":\"1334-1338 Child Ave NE, Calgary, AB T2E 5E1, Canada\",\"trapcredlevel\":16,\"lifetime\":0,\"badgekey\":\"badge_moderator\",\"affecteddirection\":null,\"camerafacingdirection\":null,\"fieldverified\":false},{\"id\":5126142,\"country\":\"CAN\",\"source\":null,\"speedlimit\":null,\"numvotes\":7,\"latitude\":51.0588956849,\"longitude\":-114.00697371917,\"reporteddirection\":null,\"linkid\":87898082,\"traptypeid\":32,\"origdisplayname\":\"g0ny\",\"lastupdatedisplayname\":\"dgh1\",\"lastvoteepochtime\":1362524409,\"trapaddress\":\"2312-2324 Maunsell Dr NE, Calgary, AB T2E 6A2, Canada\",\"trapcredlevel\":16,\"lifetime\":0,\"badgekey\":\"badge_moderator\",\"affecteddirection\":null,\"camerafacingdirection\":null,\"fieldverified\":false},{\"id\":5126145,\"country\":\"CAN\",\"source\":null,\"speedlimit\":null,\"numvotes\":4,\"latitude\":51.062509953181,\"longitude\":-114.01877543883,\"reporteddirection\":null,\"linkid\":87919519,\"traptypeid\":32,\"origdisplayname\":\"g0ny\",\"lastupdatedisplayname\":\"pyro6314\",\"lastvoteepochtime\":1340071834,\"trapaddress\":\"1810 10 Ave NE, Calgary, AB T2E 0Y2, Canada\",\"trapcredlevel\":16,\"lifetime\":0,\"badgekey\":\"badge_moderator\",\"affecteddirection\":null,\"camerafacingdirection\":null,\"fieldverified\":false},{\"id\":5126146,\"country\":\"CAN\",\"source\":null,\"speedlimit\":null,\"numvotes\":3,\"latitude\":51.054229085423,\"longitude\":-114.00594375091,\"reporteddirection\":null,\"linkid\":87898643,\"traptypeid\":32,\"origdisplayname\":\"g0ny\",\"lastupdatedisplayname\":\"dgh1\",\"lastvoteepochtime\":1362524431,\"trapaddress\":\"2225 3 Ave NE, Calgary, AB T2E, Canada\",\"trapcredlevel\":16,\"lifetime\":0,\"badgekey\":\"badge_moderator\",\"affecteddirection\":null,\"camerafacingdirection\":null,\"fieldverified\":false},{\"id\":5634464,\"country\":\"CAN\",\"source\":null,\"speedlimit\":null,\"numvotes\":7,\"latitude\":51.038548,\"longitude\":-114.027466,\"reporteddirection\":null,\"linkid\":87901441,\"traptypeid\":3,\"origdisplayname\":\"napane\",\"lastupdatedisplayname\":\"slodki123123\",\"lastvoteepochtime\":1355247829,\"trapaddress\":\"1515 9 Ave SE, Calgary, AB T2G, Canada\",\"trapcredlevel\":16,\"lifetime\":0,\"badgekey\":\"badge_moderator\",\"affecteddirection\":null,\"camerafacingdirection\":null,\"fieldverified\":false},{\"id\":5900642,\"country\":\"CAN\",\"source\":null,\"speedlimit\":null,\"numvotes\":2,\"latitude\":51.061672,\"longitude\":-113.983513,\"reporteddirection\":null,\"linkid\":708361006,\"traptypeid\":32,\"origdisplayname\":\"davyhoalim\",\"lastupdatedisplayname\":\"lorrainemiller\",\"lastvoteepochtime\":1360092366,\"trapaddress\":\"999 36 St NE, Calgary, AB T2A 6T2, Canada\",\"trapcredlevel\":4,\"lifetime\":0,\"badgekey\":null,\"affecteddirection\":null,\"camerafacingdirection\":null,\"fieldverified\":false},{\"id\":6524713,\"country\":\"CAN\",\"source\":null,\"speedlimit\":null,\"numvotes\":1,\"latitude\":51.039362987503,\"longitude\":-113.9922014717,\"reporteddirection\":null,\"linkid\":87901108,\"traptypeid\":32,\"origdisplayname\":\"geojr001\",\"lastupdatedisplayname\":\"geojr001\",\"lastvoteepochtime\":1364202945,\"trapaddress\":\"2963 16 Ave SE, Calgary, AB T2A, Canada\",\"trapcredlevel\":2,\"lifetime\":0,\"badgekey\":null,\"affecteddirection\":null,\"camerafacingdirection\":null,\"fieldverified\":false}],\"alert\":null,\"lastkarma\":null,\"loginfailures\":null,\"confirmmethod\":null,\"lastvoteepochtime\":null,\"color\":[],\"emailaddr\":null,\"pwdusernamehash\":null,\"usercredlevel\":null,\"alertradius\":null,\"signupepochtime\":null,\"totaluservotes\":null,\"tosagreed\":null,\"trustedgrouppref\":null,\"privmsgpref\":null,\"smsaddr\":null,\"moderatorlevel\":null,\"loggedontositeflag\":null,\"newsletterflag\":null,\"emailconfflag\":null,\"userstatus\":null,\"sipallowedflag\":null,\"modapplyepochtime\":null,\"lastapiepochtime\":null,\"lastwebepochtime\":null,\"confirmepochtime\":null,\"mobilepwdhash\":null,\"facebook\":null,\"twitter\":null,\"twitterenabled\":null,\"facebookenabled\":null,\"tripSessionId\":null,\"image\":[],\"trapcategory\":[],\"traptype\":[],\"addedtheme\":[],\"removedtheme\":[],\"addedpoi\":[],\"removedpoi\":[],\"poiTopics\":[],\"patrollines\":[]},\"status\":\"OK\",\"asofepochtime\":1372277587,\"statuscode\":200}}";

    private static final String TRAP_JSON_EMPTY = "{\"request\":null,\"response\":{\"message\":null,\"data\":{\"id\":null,\"string\":[],\"info\":null,\"displayname\":null,\"karma\":null,\"removed\":[],\"added\":[],\"marker\":[],\"alert\":null,\"lastkarma\":null,\"loginfailures\":null,\"confirmmethod\":null,\"lastvoteepochtime\":null,\"color\":[],\"emailaddr\":null,\"pwdusernamehash\":null,\"usercredlevel\":null,\"alertradius\":null,\"signupepochtime\":null,\"totaluservotes\":null,\"tosagreed\":null,\"trustedgrouppref\":null,\"privmsgpref\":null,\"smsaddr\":null,\"moderatorlevel\":null,\"loggedontositeflag\":null,\"newsletterflag\":null,\"emailconfflag\":null,\"userstatus\":null,\"sipallowedflag\":null,\"modapplyepochtime\":null,\"lastapiepochtime\":null,\"lastwebepochtime\":null,\"confirmepochtime\":null,\"mobilepwdhash\":null,\"facebook\":null,\"twitter\":null,\"twitterenabled\":null,\"facebookenabled\":null,\"tripSessionId\":null,\"image\":[],\"trapcategory\":[],\"traptype\":[],\"addedtheme\":[],\"removedtheme\":[],\"addedpoi\":[],\"removedpoi\":[],\"poiTopics\":[],\"patrollines\":[]},\"status\":\"OK\",\"asofepochtime\":1372277587,\"statuscode\":200}}";


    // Used to check if the parsing was done successfully
    private boolean passed = false;

    @Test
    public void shouldFailOnInvalidHttpResponseCode() throws Exception {
        System.out.println("shouldFailOnInvalidHttpResponseCode");

        passed = false;

        Robolectric.addPendingHttpResponse(405, "Invalid JSON");


        api.updateAllTraps(0, 0, 0, new TrapListener() {

                    @Override
                    public void onTraps(Traps traps) {
                        fail("JSON parsing passed magically");
                        lock.countDown();
                    }

                    @Override
                    public void onError(TrapsterError error) {
                        System.out.println("System Error Thrown:" + error.getDetails());
                        passed = true;
                        lock.countDown();
                    }
                }, new CommunicationStatusListener() {

                    @Override
                    public void onOpenConnection() {

                    }

                    @Override
                    public void onCloseConnection() {

                    }

                    @Override
                    public void onConnectionError(String errorMessage) {
                        System.out.println("JSON Error Thrown:" + errorMessage);
                        passed = true;
                        lock.countDown();
                    }
                }
        );

        lock.await(30, TimeUnit.SECONDS);

        assertTrue(passed);

    }


    @Test
    public void shouldFailOnInvalidJson() throws Exception {
        System.out.println("shouldFailOnInvalidJson");

        passed = false;

        Robolectric.addPendingHttpResponse(200, "Invalid JSON");

        api.updateAllTraps(0, 0, 0, new TrapListener() {

                    @Override
                    public void onTraps(Traps traps) {
                        fail("JSON parsing passed magically");
                        lock.countDown();
                    }

                    @Override
                    public void onError(TrapsterError error) {
                        passed = true;
                        lock.countDown();
                    }
                }, new CommunicationStatusListener() {

                    @Override
                    public void onOpenConnection() {

                    }

                    @Override
                    public void onCloseConnection() {

                    }

                    @Override
                    public void onConnectionError(String errorMessage) {
                        System.out.println("JSON Error Thrown:" + errorMessage);
                        passed = true;
                        lock.countDown();
                    }
                }
        );

        lock.await(30, TimeUnit.SECONDS);

        assertTrue(passed);

    }


    @Test
    public void shouldParseResponseWithStatus() throws Exception {
        System.out.println("Running Test shouldParseResponseWithStatus");

        passed = false;

        Robolectric.addPendingHttpResponse(200, TRAP_JSON);

        api.updateAllTraps(0, 0, 0, new TrapListener() {

            @Override
            public void onTraps(Traps traps) {
                assertNotNull(traps);
                System.out.println("Parsed TrapTypes:" + traps.getTraps().size());

                assertEquals(25, traps.getTraps().size());

                Trap trap = traps.getTraps().get(0);

                assertEquals(42376, trap.getId());
                assertEquals(106,trap.getNum());
                assertEquals(51.067108297085, trap.getLatitude());
                assertEquals(-114.03904086905, trap.getLongitude());
                assertEquals("aln",trap.getOuname());
                assertEquals("doyle911",trap.getLuname());
                assertEquals(3, trap.getTypeid());
                assertEquals(1355857003, trap.getLvoteSec());
                assertEquals("954 16 Ave NE, Calgary, AB T2E, Canada",trap.getAddress());
                assertEquals(16,trap.getLevel());
                assertEquals("badge_moderator",trap.getBadgeKey());

                passed = true;

                lock.countDown();
            }

            @Override
            public void onError(TrapsterError error) {
                fail("Trapster Error:" + error.getDetails());
                lock.countDown();
            }
        }, new TestCommunicationListener());

        lock.await(5, TimeUnit.SECONDS);

        assertTrue(passed);

    }

    @Test
    public void shouldParseEmptyResponseWithStatus() throws Exception {
        System.out.println("Running Test shouldParseResponseWithStatus");

        passed = false;

        Robolectric.addPendingHttpResponse(200, TRAP_JSON_EMPTY);

        api.updateAllTraps(0, 0, 0, new TrapListener() {

            @Override
            public void onTraps(Traps traps) {

                System.out.println("Parsed TrapTypes:" + traps.getTraps().size());



                passed = true;

                lock.countDown();
            }

            @Override
            public void onError(TrapsterError error) {
                fail("Trapster Error:" + error.getDetails());
                lock.countDown();
            }
        }, new TestCommunicationListener());

        lock.await(10, TimeUnit.SECONDS);

        assertTrue(passed);

    }


}
