package com.trapster.android.test;

import com.trapster.android.comms.NewTrapListener;
import com.trapster.android.model.Trap;
import com.trapster.android.util.TrapsterError;
import com.twolinessoftware.android.framework.comms.CommunicationStatusListener;
import com.xtremelabs.robolectric.Robolectric;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;

/**
 * Created by John on 5/28/13.
 *
 * There are changes coming to the add trap that may include passing back the trap object or trap id.
 */
public class AddTrapParserTest extends JsonParserTestBase {

    private static final String LOGNAME = "TrapParserTest";

   /*
      POSTING TO:http://trap.trap-scourge.com/2/trap?deviceid=355762051548184&opsys=4.2.2&devicetype=-C6603_1270-6704-34&pwdhash=041f52e0ee51136d260cf695daf14305&login=jcarpenter&appcapabilities=3&appkey=b9f50e2b4da4e74a7fbb7e4ec9c9d1d7&appid=TPST-AND-EN-4.0.1.1&
      POST DATA:{"request":{"auth":{"appcapabilities":"3","appid":"TPST-AND-EN-4.0.1.1","appkey":"b9f50e2b4da4e74a7fbb7e4ec9c9d1d7","deviceid":"355762051548184","devicetype":"-C6603_1270-6704-34","pwdhash":"041f52e0ee51136d260cf695daf14305","login":"jcarpenter"},"meta":{"opsys":"4.2.2"},"params":{"longitude":-114.26777305081487,"latitude":51.09076467342675,"reporteddirection":0,"traptypeids":20}}}
    */
    private static final String TRAP_JSON = "{ \"request\": null, \"response\": { \"message\": \"Thank you for reporting\", \"data\": { \"id\": 6608630, \"string\": [], \"info\": null, \"displayname\": null, \"karma\": null, \"removed\": [], \"added\": [], \"marker\": [], \"alert\": null, \"lastkarma\": null, \"loginfailures\": null, \"confirmmethod\": null, \"lastvoteepochtime\": null, \"emailaddr\": null, \"pwdusernamehash\": null, \"mobilepwdhash\": null, \"usercredlevel\": null, \"alertradius\": null, \"signupepochtime\": null, \"totaluservotes\": null, \"tosagreed\": null, \"trustedgrouppref\": null, \"privmsgpref\": null, \"smsaddr\": null, \"moderatorlevel\": null, \"loggedontositeflag\": null, \"newsletterflag\": null, \"emailconfflag\": null, \"userstatus\": null, \"sipallowedflag\": null, \"modapplyepochtime\": null, \"lastapiepochtime\": null, \"lastwebepochtime\": null, \"confirmepochtime\": null, \"facebook\": null, \"twitter\": null, \"twitterenabled\": null, \"facebookenabled\": null, \"tripSessionId\": null, \"color\": [], \"image\": [], \"trapcategory\": [], \"traptype\": [], \"addedtheme\": [], \"removedtheme\": [], \"addedpoi\": [], \"removedpoi\": [], \"poiTopics\": [], \"patrollines\": [] }, \"status\": \"OK\", \"asofepochtime\": 1374077559, \"statuscode\": 200 } }";

    // Used to check if the parsing was done successfully
    private boolean passed = false;

    private final Trap trap = getSampleTrap();


    private Trap getSampleTrap(){
        Trap trap = new Trap();
        return trap;
    }


    @Test
    public void shouldFailOnInvalidHttpResponseCode() throws Exception {
        System.out.println("shouldFailOnInvalidHttpResponseCode");

        passed = false;

        Robolectric.addPendingHttpResponse(405, "Invalid JSON");

        api.addNewTrap(trap, new NewTrapListener() {

                    @Override
                    public void onNewTrapAdded(Trap trap, String optionalMessage) {
                        fail("JSON parsing passed magically");
                        lock.countDown();
                    }

                    @Override
                    public void onError(TrapsterError error) {
                        System.out.println("System Error Thrown:" + error.getDetails());
                        passed = true;
                        lock.countDown();
                    }


                }, new CommunicationStatusListener() {

                    @Override
                    public void onOpenConnection() {

                    }

                    @Override
                    public void onCloseConnection() {

                    }

                    @Override
                    public void onConnectionError(String errorMessage) {
                        System.out.println("JSON Error Thrown:" + errorMessage);
                        passed = true;
                        lock.countDown();
                    }
                }
        ,0);

        lock.await(30, TimeUnit.SECONDS);

        assertTrue(passed);

    }


    @Test
    public void shouldFailOnInvalidJson() throws Exception {
        System.out.println("shouldFailOnInvalidJson");

        passed = false;

        Robolectric.addPendingHttpResponse(200, "Invalid JSON");

        api.addNewTrap(trap, new NewTrapListener() {

                    @Override
                    public void onNewTrapAdded(Trap trap, String optionalMessage) {
                        fail("JSON parsing passed magically");
                        lock.countDown();
                    }

                    @Override
                    public void onError(TrapsterError error) {
                        passed = true;
                        lock.countDown();
                    }
                }, new CommunicationStatusListener() {

                    @Override
                    public void onOpenConnection() {

                    }

                    @Override
                    public void onCloseConnection() {

                    }

                    @Override
                    public void onConnectionError(String errorMessage) {
                        System.out.println("JSON Error Thrown:" + errorMessage);
                        passed = true;
                        lock.countDown();
                    }
                }
                , 0
        );

        lock.await(30, TimeUnit.SECONDS);

        assertTrue(passed);

    }


    @Test
    public void shouldParseResponseWithStatus() throws Exception {
        System.out.println("Running Test shouldParseResponseWithStatus");

        passed = false;

        Robolectric.addPendingHttpResponse(200, TRAP_JSON);

        api.addNewTrap(trap, new NewTrapListener() {

            @Override
            public void onNewTrapAdded(Trap trap, String optionalMessage) {
                assertNotNull(trap);
                assertEquals(6608630,trap.getId());
                assertEquals("Thank you for reporting",optionalMessage);

                passed = true;

                lock.countDown();
            }

            @Override
            public void onError(TrapsterError error) {
                fail("Trapster Error:" + error.getDetails());
                lock.countDown();
            }
        }, new TestCommunicationListener(), 0);

        lock.await(5, TimeUnit.SECONDS);

        assertTrue(passed);

    }


}
