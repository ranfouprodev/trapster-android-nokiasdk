package com.trapster.android.beta.activity;


import com.nokia.android.common.GeoCoordinate;
import com.nokia.android.mapping.Map;
import com.nokia.android.mapping.MapAnimation;
import com.nokia.android.mapping.MapFactory;
import com.trapster.android.beta.R;

public class MapScreenTest extends MapManipulationTestClass
{

    private final static String ERROR_MAP_LOCATION = "The map did not move at least 1.0 units.";

    public void testLogin()throws Exception
    {
        startActivity(LoginScreen.class, TIMEOUT_ACTIVITY_STANDARD);
        LoginScreenTest lst = new LoginScreenTest();
        lst.setParentTest(this);
        lst.testCorrectInfo();
    }

    public void testMapTeleport() throws Exception
    {
        disableCenterOnMe();
        Map m = getMap();

        GeoCoordinate center = mapController.getMap().getCenter();
        Thread.sleep(1000);
        GeoCoordinate currentLocation = MapFactory.getPositioningManager().getPosition().getCoordinate();
        currentLocation.setLatitude(currentLocation.getLatitude() + 0.5);
        currentLocation.setLongitude(currentLocation.getLongitude() + 0.5);
        m.setCenter(currentLocation, MapAnimation.ANIMATION_LINEAR);
        Thread.sleep(1000); // Give it some time to move the map
        GeoCoordinate newCenter = mapController.getMap().getCenter();


        assertTrue(ERROR_MAP_LOCATION, center.distanceTo(newCenter) > 1.0);
    }

    public void testNewTrap()
    {
        robot.clickLongOnView(getFragment(R.id.fragmentMap).getView());
    }

/*    public void testGeoCoordinatePan() throws Exception
    {
        GeoCoordinate coordinate = MapFactory.createGeoCoordinate(33.170606, -117.252274);
        panToGeoCoordinate(coordinate);
    }*/


    public void testCenterOnMe()
    {
        boolean isCentering = mapController.isCenterOnMe();
        toggleCenterOnMe();
        assertTrue(isCentering != mapController.isCenterOnMe());
    }
}
