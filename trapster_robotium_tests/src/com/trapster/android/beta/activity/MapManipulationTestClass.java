package com.trapster.android.beta.activity;

import com.google.inject.Inject;
import com.nokia.android.common.GeoCoordinate;
import com.nokia.android.mapping.Map;
import com.nokia.android.mapping.MapFactory;
import com.trapster.android.beta.R;
import com.trapster.android.beta.TestClass;
import com.trapster.android.beta.manager.MapController;

public abstract class MapManipulationTestClass extends TestClass
{
    @Inject protected MapController mapController;

    private final static String ERROR_NULL_MAP = "The map is null!";
    private final static String ERROR_MAP_PAN_TIMEOUT = "Too much time passed while trying to pan to geo coordinate.";
    private final static String ERROR_RECENTER_DID_NOT_TOGGLE = "Unable to toggle recentering.";

    protected MapScreen mapActivity;

    public MapManipulationTestClass()
    {
        super(MapScreen.class);
    }


    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        mapActivity = (MapScreen) getActivity();
        mapController = mapActivity.mapController;
    }

    protected void panToGeoCoordinate(double latitude, double longitude) throws Exception
    {
        checkGeoCoordinateLimits(latitude, longitude);

        panToGeoCoordinate(MapFactory.createGeoCoordinate(latitude, longitude));
    }

    protected void panToGeoCoordinate(GeoCoordinate coordinate) throws Exception
    {
        long timeOut = 1000 * 3; // 30 second timeout
        assertNotNull(coordinate);

        Map map = getMap();

        double destinationLongitude = coordinate.getLongitude();
        double destinationLatitude = coordinate.getLatitude();

        GeoCoordinate currentLocation = MapFactory.getPositioningManager().getPosition().getCoordinate();


        double currentLatitude = currentLocation.getLatitude();
        double currentLongitude = currentLocation.getLongitude();
        double angleInDegress = Math.toDegrees(Math.atan2(destinationLongitude - currentLongitude, destinationLatitude - currentLatitude));

        FragmentBounds bounds = findFragmentBounds(R.id.fragmentMap);


        // These represent the starting X and Y of the edge of the screen following the angle from the center of the screen toward the destination point.
        int screenEdgeXStart = 0;
        int screenEdgeYStart = 0;

        // These represent the ending X and Y on the screen to complete the swipe motion, causing the screen to move toward our destination.
        int screenEdgeXEnd = 0;
        int screenEdgeYEnd = 0;

        if (angleInDegress >= 0 && angleInDegress < 90)
        {
            screenEdgeXStart = bounds.maximumX;
            double n = (Math.toRadians(angleInDegress));
            screenEdgeYStart = bounds.maximumY + (int)(n * screenEdgeXStart);
            screenEdgeXEnd = screenEdgeXStart * -1;
            screenEdgeYEnd = bounds.maximumY - screenEdgeYStart;
        }

        long startTime = System.currentTimeMillis();
        long currentTime = System.currentTimeMillis();
        while (map.getCenter().distanceTo(coordinate) >= 0.5)
        {
            assertTrue(ERROR_MAP_PAN_TIMEOUT, currentTime - startTime <= timeOut);

            //scrollScreen((float)screenEdgeXStart, (float)screenEdgeYStart, (float)screenEdgeXEnd, (float)screenEdgeYEnd, R.id.fragmentMap);
            //scrollScreen((float)screenSizeX , (float)0, (float)screenSizeX - 1, (float)screenSizeY, R.id.fragmentMap);
            //scrollScreen(0, 0, 500, 500, R.id.fragmentMap);
            Thread.sleep(500L);
            currentTime = System.currentTimeMillis();
        }
    }

    protected void toggleCenterOnMe()
    {
        boolean isCentering = mapController.isCenterOnMe();
        clickOnButton(R.id.imageRecenter);
        assertTrue(ERROR_RECENTER_DID_NOT_TOGGLE, isCentering != mapController.isCenterOnMe());
    }

    protected void disableCenterOnMe()
    {
        boolean isCentering = mapController.isCenterOnMe();
        if (isCentering)
        {
            clickOnButton(R.id.imageRecenter);
            assertTrue(ERROR_RECENTER_DID_NOT_TOGGLE, isCentering != mapController.isCenterOnMe());
        }

    }

    protected Map getMap()
    {
        robot.waitForFragmentById(R.id.fragmentMap);
        MapScreen mapActivity = (MapScreen) testedActivity;
        mapController = mapActivity.mapController;
        Map map = mapActivity.mapController.getMap();
        assertNotNull(ERROR_NULL_MAP, map);

        return map;
    }

    protected void zoomIn() throws Exception
    {
    }

    protected double getZoomLevel()
    {
        return getMap().getZoomLevel();
    }
}
