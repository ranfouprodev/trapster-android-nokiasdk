package com.trapster.android.beta.activity;

import com.trapster.android.beta.R;
import com.trapster.android.beta.TestClass;

public class LoginScreenTest extends TestClass<LoginScreen>
{
    private final static String ERROR_LOGIN_FAILED = "Failed to login.";
    private final static String ERROR_LOGIN_SUCCEEDED = "Login succeeded with incorrect credentials.";

    private final static long LOGIN_TIMEOUT = 10 * 1000; // 10 seconds


    public LoginScreenTest()
    {
        super(LoginScreen.class);
    }

    @Override
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }


//Un-commment this function when we have a mechanism to delete/increment/etc. our users/emails, so we don't fail our login because they already exist
/*    public void testSignup() throws Exception
    {
        startActivity(SignUpScreen.class, TIMEOUT_ACTIVITY_STANDARD);

        typeText(R.id.editEmail, "bbqSandwich1@superawesome.com");
        typeText(R.id.editUsername, "signBBQupNAme1");
        typeText(R.id.editPasswd, "ABC1234achtr");
        typeText(R.id.editVerifyPasswd, "ABC1234achtr");

        clickOnCheckBox(R.id.newsletter_checkbox);

        clickOnButton(R.id.buttonSignup);

        waitForProgressDialog(getProgressDialog((SignUpScreen)testedActivity), 10000L);

        Thread.sleep(5000);

        waitForConfirmationDialog(getConfirmationDialog((LoginScreen)currentActivity), 10000L);

        typeText(R.id.editConf, "12345");
        ((SignUpScreen)testedActivity).sessionManager.logout();

        Thread.sleep(5000);
    }*/

    public void testCorrectInfo() throws Exception
    {
        String testUsername = "testing4will";
        String testPassword = "fossil";

        typeText(R.id.editName, testUsername);
        typeText(R.id.editPasswd, testPassword);

        clickOnButton(R.id.bottomBarPositiveButton);
        waitForLoginResponse(LOGIN_TIMEOUT);

        LoginScreen loginScreen = (LoginScreen)testedActivity;
        assertTrue(ERROR_LOGIN_FAILED, loginScreen.sessionManager.isLoggedIn());

        ((LoginScreen)testedActivity).sessionManager.logout();
    }

    public void testEmptyInfo() throws Exception
    {
        setOrientation(ORIENTATION.LANDSCAPE);
        typeText(R.id.editName, "");
        typeText(R.id.editPasswd, "");

        clickOnButton(R.id.bottomBarPositiveButton);

        checkForTextOnScreen(R.string.invalid_credentials_error);
    }

    private void waitForLoginResponse(long timeOut) throws Exception
    {
        waitForProgressDialog(getProgressDialog((LoginScreen)testedActivity), timeOut);
    }
}
