package com.trapster.android.beta.activity;

/**
 * Created with IntelliJ IDEA.
 * User: kmekala
 * Date: 10/26/12
 * Time: 10:57 AM
 * To change this template use File | Settings | File Templates.
 */

import android.app.ProgressDialog;
import com.trapster.android.beta.R;
import com.trapster.android.beta.TestClass;


public class SignUpScreenTest extends TestClass<SignUpScreen>
{

    public static final String ERROR_SIGNUP_SUCCEEDED = "Signup successful when it should have failed!";


    public SignUpScreenTest()
    {
        super (SignUpScreen.class);
    }


    public void testCorrectInfo() throws Exception
    {
        //Date d = new Date(System.currentTimeMillis());
        //d.toString();

        String testEmail = "trapkal100@mailinator.com";
        String testUsername = "trapkal100";
        String testPassword = "trapster";
        String testVpassword = "trapster";

        //EditText emailField = getEditText(R.id.editEmail);
        //EditText usernameField = getEditText(R.id.editUsername);
       // EditText passwordField = getEditText(R.id.editPasswd);
       // EditText vPasswordField = getEditText(R.id.editVerifyPasswd);

        typeText(R.id.editEmail, testEmail);
        typeText(R.id.editUsername, testUsername);
        typeText(R.id.editPasswd, testPassword);
        typeText(R.id.editVerifyPasswd, testVpassword);

       // assertEquals(emailField.getText().toString(), testEmail);
        //assertEquals(usernameField.getText().toString(), testUsername);
        //assertEquals(passwordField.getText().toString(), testPassword);
        //assertEquals(vPasswordField.getText().toString(), testVpassword);

        clickOnCheckBox(R.id.newsletter_checkbox);

        clickOnButton(R.id.buttonSignup);

        ProgressDialog dialog = getProgressDialog((SignUpScreen)getTestedActivity());
        waitForProgressDialog(dialog, TIMEOUT_ACTIVITY_STANDARD);

/*        Thread.sleep(2000);

        //view v = robot.getView(R.id.dlgConf);
        //assertNotNull(v);

        //robot.waitForView(v);
        //robot.getActivityMonitor().getLastActivity();
        clickOnButton(R.id.btnConfCancel);*/


        //robot.searchText("The email address is already registered");

    }


    /* Test for Incorrect Username    */

    public void testIncorrectUname() throws Exception
    {
        String testEmail = "test4kal@mailinator.com";
        String testUsername = "t";
        String testPassword = "trapster";
        String testVpassword = "trapster";

        typeText(R.id.editEmail, testEmail);
        typeText(R.id.editUsername, testUsername);
        typeText(R.id.editPasswd, testPassword);
        typeText(R.id.editVerifyPasswd, testVpassword);

        clickOnCheckBox(R.id.newsletter_checkbox);

        clickOnButton(R.id.buttonSignup);

        ProgressDialog dialog = getProgressDialog((SignUpScreen)getTestedActivity());
        waitForProgressDialog(dialog, TIMEOUT_ACTIVITY_STANDARD);

        Thread.sleep(2000);

        //checkForTextOnScreen(R.string.signup_username_error, R.id.editUsername);

        // We can't check for the text popup because it's generated server-side.  Instead, let's make sure the login failed
        assertFalse("ERROR_SIGNUP_SUCCEEDED", ((SignUpScreen) getTestedActivity()).sessionManager.isLoggedIn());


    }

    /* Test for INVALID USERNAME */

   /* public void testInvalidUname() throws Exception
    {
        String testEmail = "test4kal@mailinator.com";
        String testUsername = ".h^ello";
        String testPassword = "trapster";
        String testVpassword = "trapster";

        typeText(R.id.editEmail, testEmail);
        typeText(R.id.editUsername, testUsername);
        typeText(R.id.editPasswd, testPassword);
        typeText(R.id.editVerifyPasswd, testVpassword);

        clickOnCheckBox(R.id.newsletter_checkbox);

        clickOnButton(R.id.buttonSignup);

         ProgressDialog dialog = getProgressDialog((SignUpScreen)getTestedActivity());
         waitForProgressDialog(dialog, TIMEOUT_ACTIVITY_STANDARD);

        Thread.sleep(2000);

        robot.scrollToTop();

        Thread.sleep(2000);

        checkForTextOnScreen(R.string.signup_username_error, R.id.editUsername);

    }*/


    /* TEST FOR INVALID EMAIL ADDRESS */

    public void testIncorrectEmail() throws Exception

    {
        String testEmail = "/1224;[]@adfda.com";
        String testUsername = "t";
        String testPassword = "trapster";
        String testVpassword = "trapster";

        typeText(R.id.editEmail, testEmail);
        typeText(R.id.editUsername, testUsername);
        typeText(R.id.editPasswd, testPassword);
        typeText(R.id.editVerifyPasswd, testVpassword);

        clickOnCheckBox(R.id.newsletter_checkbox);

        clickOnButton(R.id.buttonSignup);

        //ProgressDialog dialog = getProgressDialog((SignUpScreen)getTestedActivity());
        //waitForProgressDialog(dialog, TIMEOUT_ACTIVITY_STANDARD);

        Thread.sleep(2000);

        //checkForTextOnScreen(" The specified Email address in not a valid Email address ");

        checkForTextOnScreen(R.string.signup_email_error);
    }

    /* TEST FOR REGISTERED EMAIL ADDRESS */

    public void testRegisteredEmail() throws Exception

    {

        String testEmail = "trapkal75@mailinator.com";
        String testUsername = "test4kal";
        String testPassword = "trapster";
        String testVpassword = "trapster";

        typeText(R.id.editEmail, testEmail);
        typeText(R.id.editUsername, testUsername);
        typeText(R.id.editPasswd, testPassword);
        typeText(R.id.editVerifyPasswd, testVpassword);

        clickOnCheckBox(R.id.newsletter_checkbox);

        clickOnButton(R.id.buttonSignup);

        ProgressDialog dialog = getProgressDialog((SignUpScreen)getTestedActivity());
        waitForProgressDialog(dialog, TIMEOUT_ACTIVITY_STANDARD);

        Thread.sleep(2000);

        //checkForTextOnScreen("The email address " + testEmail + " is already registered.");
        //robot.searchText("The email address " + testVpassword + " is already registered ");
        //robot.searchText("fasdfdsfasfsf");

    }

    /* TEST FOR INCORRECT PASSWORDS */

    public void testIncorrectPwd() throws Exception

    {
        String testEmail = "test4kal@mailinator.com";
        String testUsername = "test4kal";
        String testPassword = "trapster";
        String testVpassword = "trapsteradf";

        typeText(R.id.editEmail, testEmail);
        typeText(R.id.editUsername, testUsername);
        typeText(R.id.editPasswd, testPassword);
        typeText(R.id.editVerifyPasswd, testVpassword);

        clickOnCheckBox(R.id.newsletter_checkbox);

        clickOnButton(R.id.buttonSignup);

        //ProgressDialog dialog = getProgressDialog((SignUpScreen)getTestedActivity());
        //waitForProgressDialog(dialog, TIMEOUT_ACTIVITY_STANDARD);

        Thread.sleep(2000);

        //checkForTextOnScreen(" Passwords do not match ");
        checkForTextOnScreen(R.string.password_match_error);
    }


    /* TEST FOR PASSWORD LENGTHS */

    public void testPwdLength() throws Exception

    {
        String testEmail = "test4kal@mailinator.com";
        String testUsername = "test4kal";
        String testPassword = "tra";
        String testVpassword = "tra";

        typeText(R.id.editEmail, testEmail);
        typeText(R.id.editUsername, testUsername);
        typeText(R.id.editPasswd, testPassword);
        typeText(R.id.editVerifyPasswd, testVpassword);

        clickOnCheckBox(R.id.newsletter_checkbox);

        clickOnButton(R.id.buttonSignup);

        //ProgressDialog dialog = getProgressDialog((SignUpScreen)getTestedActivity());
        //waitForProgressDialog(dialog, TIMEOUT_ACTIVITY_STANDARD);

        Thread.sleep(2000);

        //checkForTextOnScreen(" Password length must be between 6-20 characters in length. ");
        checkForTextOnScreen(R.string.signup_password_error);


    }


}

