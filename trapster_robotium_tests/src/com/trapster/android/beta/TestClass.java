package com.trapster.android.beta;


import android.app.Activity;
import android.app.Instrumentation;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.test.ActivityInstrumentationTestCase2;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import com.jayway.android.robotium.solo.Solo;
import com.trapster.android.beta.activity.LoginScreen;
import com.trapster.android.beta.activity.SignUpScreen;
import com.trapster.android.beta.activity.component.ConfirmationCodeDialog;
import com.trapster.android.beta.activity.fragment.TrapsterDialogFragment;

import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

public abstract class TestClass<T extends Activity> extends ActivityInstrumentationTestCase2
{
    protected Solo robot;
    protected Activity currentActivity;
    protected Activity testedActivity;
    protected Instrumentation instrumentation;
    protected static long TIMEOUT_ACTIVITY_STANDARD = 1000L * 1L; // 5 Seconds
    private final static String ERROR_VIEW_NOT_IN_FRAGMENT = "The fragment does not contain the specified view.";
    private final static String ERROR_NULL_FRAGMENT = "The fragment requested is null";
    private final static String ERROR_FRAGMENT_INVISIBLE = "The fragment is not visible";
    private final static String ERROR_LATITUDE_OUT_OF_BOUNDS = "The latitude provided is outside of possible bounds.";
    private final static String ERROR_LONGITUDE_OUT_OF_BOUNDS = "The longitude provided is outside of possible bounds.";
    private final static String ERROR_TEXT_NOT_FOUND = "Unable to find text, expected: ";
    private final static String ERROR_NULL_CHECK_BOX = "Unable to find the specified check box.";
    private final static String ERROR_NULL_PROGRESS_DIALOG = "The ProgressDialog was null!";
    private final static String ERROR_PROGRESS_DIALOG_INVISIBLE = "The ProgressDialog is not already showing!";
    private final static String ERROR_PROGRESS_DIALOG_UNAVAILABLE = "Unable to locate the ProgressDialog from class ";
    private final static String ERROR_CONFIRMATION_DIALOG_UNAVAILABLE = "Unable to locate the ConfirmationDialog from class ";
    private final long PROGRESS_DIALOG_TIMEOUT = 2000L;

    private final String[] REGEX_CHARS = {"[", "^", "$", "|", "?", "*", "+", "(", ")", "/"};
    private final String[] REGEX_VALID_CHARS = {"\\\\[", "\\\\^", "\\\\$", "\\\\|", "\\\\?", "\\\\*", "\\\\+", "\\\\(", "\\\\)", "\\\\/"};

    protected enum ORIENTATION
    {
        PORTRAIT,
        LANDSCAPE
    }



    public TestClass(Class<T> activity)
    {
        super(activity);

    }

    public void setParentTest(TestClass parent) throws Exception
    {
        robot = parent.getRobot();
        currentActivity = parent.getActivity();
        testedActivity = parent.getTestedActivity();
        instrumentation = parent.getInstrumentation();

    }

    @Override
    protected void setUp() throws Exception
    {
        currentActivity = getActivity();
        testedActivity = currentActivity;
        instrumentation = getInstrumentation();
        robot = new Solo(instrumentation, currentActivity);
        super.setUp();

        Thread.sleep(500L);
        robot.scrollToTop();
        Thread.sleep(1500L);
    }



    protected final Fragment getFragment(int id)
    {
        Fragment fragment = ((FragmentActivity)testedActivity).getSupportFragmentManager().findFragmentById(id);
        //assertNotNull(fragment);
        if (fragment != null)
            robot.waitForFragmentById(id);
        return fragment;
    }

    protected final Fragment getFragment(String fragmentName)
    {
        Fragment fragment = ((FragmentActivity)testedActivity).getSupportFragmentManager().findFragmentByTag(fragmentName);
        if (fragment != null)
            robot.waitForFragmentByTag(fragmentName);
        //assertNotNull(fragment);

        return fragment;
    }


    @Override
    protected void tearDown() throws Exception
    {
        currentActivity.finish();
        if (testedActivity != currentActivity)
            testedActivity.finish();
        robot.finishOpenedActivities();
        super.tearDown();
    }

    /*
        Starts the activity specified by @param activityName, waiting up to @param timeOut milliseconds before terminating.
        If @param timeOut is less than 1, no timeout is set.
     */
    protected final void startActivity(Class activityName, long timeOut)
    {
        Intent i = new Intent(Intent.ACTION_MAIN);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.setClassName(instrumentation.getTargetContext(), activityName.getName());
        instrumentation.startActivitySync(i);

        Instrumentation.ActivityMonitor monitor = instrumentation.addMonitor(activityName.getName(), null, true);
        Activity a = null;
        if (timeOut > 0)
            a = instrumentation.waitForMonitorWithTimeout(monitor, timeOut);
        else
            a = instrumentation.waitForMonitor(monitor);

        instrumentation.removeMonitor(monitor);

        testedActivity = robot.getCurrentActivity();
    }

    private Solo getRobot()
    {
        return robot;
    }

    protected EditText getEditText(int resourceID)
    {
        EditText editText = (EditText)robot.getView(resourceID);
        assertNotNull(editText);

        return editText;
    }

    protected View findViewInFragment(Fragment fragment, int resourceID)
    {
        View v = fragment.getView().findViewById(resourceID);
        assertNotNull(ERROR_VIEW_NOT_IN_FRAGMENT, v);

        return v;
    }

    protected void clickOnButton(int resourceID)
    {
        robot.clickOnView(robot.getView(resourceID));
    }

    protected void checkForPopup(int resourceID) throws Exception
    {
        checkForPopup(testedActivity.getString(resourceID));
    }

    protected void checkForPopup(String popupText) throws Exception
    {
        TrapsterDialogFragment fragment = (TrapsterDialogFragment)getFragment("dialog");
        assertNotNull(ERROR_NULL_FRAGMENT, fragment);
        String descriptionText = fragment.getArguments().getString("EXTRA_DESCRIPTION");
        Thread.sleep(500L); // Give time for the fragment to show up
        assertTrue(ERROR_FRAGMENT_INVISIBLE, fragment.isVisible());
        assertEquals(popupText, descriptionText);
    }

    protected void checkForTextOnScreen(int resourceID, int ... textFieldResources) throws Exception
    {
        String[] strings = new String[textFieldResources.length];
        for (int i = 0; i < textFieldResources.length; i++)
        {
            strings[i] = getEditText(textFieldResources[i]).getText().toString();
        }

        String text;
        if (strings.length > 0)
            text = testedActivity.getString(resourceID, strings);
        else
            text = testedActivity.getString(resourceID);

        checkForTextOnScreen(text);
    }

    protected void checkForTextOnScreen(String text) throws Exception
    {
        Thread.sleep(100L); // Give time for the text to pop up
        //text = fixForRegex(text);

        new Thread()
        {
            public void run()
            {
                try
                {
                    robot.scrollToTop();
                    Thread.sleep(1500L);
                    robot.scrollToBottom();
                }
                catch (InterruptedException e)
                {

                }
            }
        }.start();
        boolean waitText = robot.waitForText(text);
        boolean findText = false;
        if (!waitText)
            findText = robot.searchText(text);
        assertTrue(ERROR_TEXT_NOT_FOUND + text, waitText || findText);
    }

    private String fixForRegex(String text)
    {

        for (int i = 0; i < REGEX_CHARS.length; i++)
        {
            text = text.replaceAll(Pattern.quote(REGEX_CHARS[i]), REGEX_VALID_CHARS[i]);
        }

        return text;


    }

    protected void scrollScreen(float startX, float startY, float endX, float endY, int resourceID) throws Exception
    {
        int numberSteps = 5;
        robot.waitForFragmentById(resourceID);
        robot.clickOnView(getFragment(resourceID).getView());


        //The documentation on the drag() function is incorrect, it calls for (X1, Y1) and (X2, Y2) despite indicating that it should be X1, X2, Y1, Y2
        //robot.drag(startX, startY, endX, endY, numberSteps);
        robot.drag(startX, endX, startY, endY, numberSteps);
    }

    protected void checkGeoCoordinateLimits(double latitude, double longitude)
    {
        assertTrue(ERROR_LATITUDE_OUT_OF_BOUNDS, latitude <= 90.0 && latitude >= -90.0);
        assertTrue(ERROR_LONGITUDE_OUT_OF_BOUNDS, longitude <= 180.0 && longitude >= -180.0);
    }

    public static class FragmentBounds
    {
        public int minimumX;
        public int minimumY;
        public int maximumX;
        public int maximumY;
    }

    protected FragmentBounds findFragmentBounds(int fragmentID)
    {
        FragmentBounds bounds = new FragmentBounds();

        int screenSizeX = testedActivity.getWindowManager().getDefaultDisplay().getWidth();
        int screenSizeY = testedActivity.getWindowManager().getDefaultDisplay().getHeight();

        int[] locs = new int[2];
        bounds.minimumX = locs[0];
        bounds.minimumY = locs[1];

        getFragment(R.id.fragmentMap).getView().getLocationOnScreen(locs);

        bounds.minimumX = getFragment(R.id.fragmentMap).getView().getWidth();
        if (bounds.minimumX > screenSizeX)
            bounds.minimumX = screenSizeX;

        bounds.minimumY = getFragment(R.id.fragmentMap).getView().getHeight();
        if (bounds.minimumY > screenSizeY)
            bounds.minimumY = screenSizeY;

        if (bounds.maximumY == 0 )
            bounds.maximumY += getStatusBarHeight();

        return bounds;
    }

    protected void clickOnCheckBox(int resourceID)
    {
        final CheckBox box = (CheckBox) robot.getView(resourceID);
        assertNotNull(ERROR_NULL_CHECK_BOX, box);
        testedActivity.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                box.performClick();
            }
        });
    }

    protected void setOrientation(ORIENTATION orientation) throws Exception
    {
        switch (orientation)
        {
            case LANDSCAPE:
            {
                robot.setActivityOrientation(Solo.LANDSCAPE);
                break;
            }
            case PORTRAIT:
            {
                robot.setActivityOrientation(Solo.PORTRAIT);
                break;
            }
        }

        Thread.sleep(500L); // Provide time for the screen to rotate
    }

    protected void typeText(int resourceID, String text)
    {
        EditText textField = getEditText(resourceID);
        robot.typeText(textField, text);
        assertEquals(textField.getText().toString(), text);
    }

    protected void waitForProgressDialog(ProgressDialog progressDialog, long timeOut) throws Exception
    {
        assertNotNull(ERROR_NULL_PROGRESS_DIALOG, progressDialog);
        assertTrue(ERROR_PROGRESS_DIALOG_INVISIBLE, progressDialog.isShowing());
        long startTime = System.currentTimeMillis();
        long currentTime = System.currentTimeMillis();

            while (progressDialog.isShowing() && currentTime - startTime <= timeOut)
            {
                //if (progressDialog != null && )
                    //break; // Break out of the while loop
                currentTime = System.currentTimeMillis();
            }
    }

    protected void waitForConfirmationDialog(ConfirmationCodeDialog conf, long timeOut)

    {
        assertNotNull(ERROR_NULL_PROGRESS_DIALOG, conf);
        //assertTrue(ERROR_PROGRESS_DIALOG_INVISIBLE, conf.isShowing());
        long startTime = System.currentTimeMillis();
        long currentTime = System.currentTimeMillis();


        // Wait until it starts showing
        while (!conf.isShowing() && currentTime - startTime <= timeOut)
        {
            //if (progressDialog != null && )
            //break; // Break out of the while loop
            currentTime = System.currentTimeMillis();
        }
    }

    protected Activity getTestedActivity()
    {
        return testedActivity;
    }

    protected ProgressDialog getProgressDialog(LoginScreen loginScreen)
    {
        ProgressDialog progressDialog = null;
        long startTime = System.currentTimeMillis();
        long currentTime = System.currentTimeMillis();

        while (progressDialog == null && currentTime - startTime <= PROGRESS_DIALOG_TIMEOUT)
        {
            progressDialog = loginScreen.getProgressDialog();
        }

        assertNotNull(ERROR_PROGRESS_DIALOG_UNAVAILABLE + LoginScreen.class.getCanonicalName(), progressDialog);
        return progressDialog;
    }

    protected ProgressDialog getProgressDialog(SignUpScreen signupScreen)
    {
        ProgressDialog progressDialog = null;
        long startTime = System.currentTimeMillis();
        long currentTime = System.currentTimeMillis();

        while (progressDialog == null && currentTime - startTime <= PROGRESS_DIALOG_TIMEOUT)
        {
            progressDialog = signupScreen.getProgressDialog();
            currentTime = System.currentTimeMillis();
        }

        assertNotNull(ERROR_PROGRESS_DIALOG_UNAVAILABLE + SignUpScreen.class.getCanonicalName(), progressDialog);
        return progressDialog;
    }

    protected ConfirmationCodeDialog getConfirmationDialog(LoginScreen loginScreen)
    {
        ConfirmationCodeDialog confirmationDialog = null;
        long startTime = System.currentTimeMillis();
        long currentTime = System.currentTimeMillis();

        while (confirmationDialog == null && currentTime - startTime <= PROGRESS_DIALOG_TIMEOUT)
        {
            confirmationDialog = loginScreen.getConfirmationDialog();
            currentTime = System.currentTimeMillis();
        }

        assertNotNull(ERROR_CONFIRMATION_DIALOG_UNAVAILABLE + LoginScreen.class.getCanonicalName(), confirmationDialog);
        return confirmationDialog;
    }

    public String convertToDate(Date d)
    {
        String date = "";
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);

        date += cal.get(Calendar.MONTH) + "/";
        date += cal.get(Calendar.DAY_OF_MONTH) + "/";
        date += cal.get(Calendar.YEAR);

        return date;
    }

    public String convertToTime(Date d)
    {
        String date = "";
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);

        date += cal.get(Calendar.HOUR) + "/";
        date += cal.get(Calendar.MINUTE) + "/";
        date += cal.get(Calendar.SECOND);

        return date;
    }

    protected int getStatusBarHeight()
    {
        View globalView = testedActivity.findViewById(android.R.id.content); // the main view of my activity/application

        DisplayMetrics dm = new DisplayMetrics();
        testedActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int topOffset = dm.heightPixels - globalView.getMeasuredHeight();

        return topOffset;
    }
}
